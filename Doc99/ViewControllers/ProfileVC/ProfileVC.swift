//
//  ProfileVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 18/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD


class ProfileVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var tableViewProfile:UITableView!
    @IBOutlet var loginView:UIView!
    var profileMenuData = NSMutableArray()
    @IBOutlet var lblUsername:UILabel!

    @IBOutlet var viewMapOptions: UIView!
    @IBOutlet weak var scrollViewExtraOptions: UIScrollView!
    @IBOutlet weak var labelCredit: UILabel!
    @IBOutlet weak var viewPharmacyExtraOptions: UIView!
    
    //LOGIN POPUP
    @IBOutlet var loginPopupLabel2:UILabel!
    @IBOutlet var loginPopupLabel3:UILabel!
    
    @IBOutlet var btnEditProfile:UIButton!
    @IBOutlet var btnSignOut:UIButton!

    let MapFormTag = 11
    let savedMapTag = 12
    let submittedMapTag = 13
    let creditsTag = 14
    let mapOrderHistoryTag = 15
    let userOrdersTag = 16
    let sellingHistoryTag = 17
    
    
    var isSmokeSet = false
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
//        profileMenuData = ["Weight Reduction Program","Intra Ocular Pressure","ARIA Stroke Risk","Patient","Address","Membership","Rate Application","Reset Password","About Doc99","Health Concerns for Store"]
        
        profileMenuData = ["Smoking Cessation Program",
                           "Weight Reduction Program",  //0
                           //"Patient",                   //1
                           "Address",                   //2
                           "Membership",                //3
                           "Manage Access Program (MAP)",     //4
                           "Reset Password",            //5
                           "Rate Application",          //6
                           "About Doc99 Chemist"]               //7
        
        /*
         Weight Reduction Program
         Patient
         Address
         Membership
         Manage Access Pogram
         Reset password
         Rate Application
         About Dov99
         
         
         MAP Form
         Saved Map
         Submitted MAP
         Credits
         MAP Order history

         */

        
        //"Intra Ocular Pressure","ARIA Stroke Risk",
        
        // Define identifier
        let notificationName1 = Notification.Name("updateProfileDataNotification")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.setProfileData), name: notificationName1, object: nil)

        
        // Define identifier
        let notificationName2 = Notification.Name("updateAfterMemberShip")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateAfterMembershipCompleted), name: notificationName2, object: nil)
        
        labelCredit.clipsToBounds = true
        labelCredit.layer.cornerRadius = 12.5
        
        self.setLocalizationText()
        
        scrollViewExtraOptions.isScrollEnabled = true

        if  (USERDEFAULT.value(forKey: "user_type") != nil) && USERDEFAULT.value(forKey: "user_type")as! String == "2"{
            viewPharmacyExtraOptions.isHidden = false
            scrollViewExtraOptions.contentSize = CGSize(width: scrollViewExtraOptions.frame.size.width, height: viewPharmacyExtraOptions.frame.origin.y + viewPharmacyExtraOptions.frame.size.height)
        }
        else{
            viewPharmacyExtraOptions.isHidden = true
            scrollViewExtraOptions.contentSize = CGSize(width: scrollViewExtraOptions.frame.size.width, height: viewPharmacyExtraOptions.frame.origin.y )
        }
        
        
        viewMapOptions.frame = CGRect(x: 0, y: tableViewProfile.frame.origin.y,
                                      width: viewMapOptions.frame.size.width, height: tableViewProfile.frame.size.height)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = false
        
        if USERDEFAULT.value(forKey: "userID") != nil{
            
        }
        else{
            loginPopupLabel3.text = "LOGIN"
            loginView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
            self.view.addSubview(loginView)
        }
        self.setProfileData()
        
        if Reachability.isConnectedToNetwork() {
            self.postDataCheckUserCredit()
            self.postDataGetUserDetail()
        }
        else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
    }

    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // TODO: - OTHER METHODS
    func setProfileData() {
        if let username = USERDEFAULT.value(forKey: "fullName") as? String{
            lblUsername.text = "\(username)"
        }
        else{
            lblUsername.text = ""
        }
        
    }
    
    func updateAfterMembershipCompleted() {
        loginView.removeFromSuperview()
    }
    
    func setLocalizationText(){
        
        loginPopupLabel2.text = NSLocalizedString("Just enrolled to Doc99 app to experience it amazing features.", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        loginPopupLabel3.text = NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
        btnEditProfile.setTitle(NSLocalizedString("Edit Profile", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        btnSignOut.setTitle(NSLocalizedString("Sign Out", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)

    }
    
    
    
    // TODO: - DELEGATE METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.profileMenuData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "profileCell"
        var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ProfileTableVC
        
        if cell == nil {
            let nib  = Bundle.main.loadNibNamed("ProfileTableVC", owner: self, options: nil)
            cell = nib?[0] as? ProfileTableVC
        }
        cell!.selectionStyle = .none;
        
        
        if let title =  (self.profileMenuData[indexPath.row] as AnyObject) as? String
        {
            cell?.lblTitle.text = "\(title)"
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0{
            if isSmokeSet{
                let vc = SmokingProgramResultVC(nibName: "SmokingProgramResultVC",bundle: nil)
                navigationController?.pushViewController(vc,animated: true)
            }
            else{
                let vc = SmokingCessationProgramVC(nibName: "SmokingCessationProgramVC",bundle: nil)
                navigationController?.pushViewController(vc,animated: true)
            }
        }
        else if indexPath.row == 1 {
            let weightReductionVC = WeightReductionVC(nibName: "WeightReductionVC", bundle: nil)
            self.navigationController?.pushViewController(weightReductionVC, animated: true)
        }
//        else if indexPath.row == 2 {
//            let patientInfoVC = PatientInfoVC(nibName: "PatientInfoVC", bundle: nil)
//            patientInfoVC.isFromSettingPage = true
//            self.navigationController?.pushViewController(patientInfoVC, animated: true)
//        }
        else if indexPath.row == 2 {
            let patientInfoVC = AddressListVC(nibName: "AddressListVC", bundle: nil)
            patientInfoVC.isFromSetting = true
            APPDELEGATE.myTabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(patientInfoVC, animated: true)
        }
        else if indexPath.row == 3 {
            let member = JoinMemberVC(nibName: "JoinMemberVC", bundle: nil)
            member.isFromOther = true
            self.navigationController?.pushViewController(member, animated: true)
        }
        else if indexPath.row == 4 {
            //Open Other MAP options
            self.viewMapOptions.frame = CGRect(x: 0, y: tableViewProfile.frame.origin.y, width: self.tableViewProfile.frame.size.width, height: self.tableViewProfile.frame.size.height)
            self.view .addSubview(self.viewMapOptions)
        }
        else if indexPath.row == 5 {
            let reset = ResetPasswordVC(nibName: "ResetPasswordVC", bundle: nil)
            self.navigationController?.pushViewController(reset, animated: true)
        }
        else if indexPath.row == 6 {
            //Open browser for rate application
        }
        else if indexPath.row == 7 {
            let about = AboutUsVC(nibName: "AboutUsVC", bundle: nil)
            self.navigationController?.pushViewController(about, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }

    
    // TODO: - ACTION METHODS

    @IBAction func btnLogoutClicked(_ sender:UIButton){
        APPDELEGATE.logOutUser()
        let welcomeVC = WelcomeVC(nibName: "WelcomeVC", bundle: nil)
        self.navigationController?.pushViewController(welcomeVC, animated: true)
    }
    
    @IBAction func btnLoginClicked(_ sender:UIButton){
        
        if USERDEFAULT.value(forKey: "userID") != nil{
            
            if let isMember = USERDEFAULT.value(forKey: "isMember") as? String{
                let ismem = "\(isMember)"
                if ismem == "1"{
                    
                }
                else{
                    let joinMemberVC = JoinMemberVC(nibName: "JoinMemberVC", bundle: nil)
                    joinMemberVC.isFromOther = true
                    joinMemberVC.isFromTab = "4"
                    self.navigationController?.pushViewController(joinMemberVC, animated: true)
                }
            }
            else{
                
            }
        }
        else{
            let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
            loginVC.isFromTab = "4"
            self.navigationController?.pushViewController(loginVC, animated: true)
        }

        
    }
    
    @IBAction func btnEditProfileClicked(_ sender:UIButton){
        let editProfileVC = EditProfileVC(nibName: "EditProfileVC", bundle: nil)
        self.navigationController?.pushViewController(editProfileVC, animated: true)

    }
    
    @IBAction func btnWeightReductionClicked(_ sender:UIButton){
        let weightReductionVC = WeightReductionVC(nibName: "WeightReductionVC", bundle: nil)
        self.navigationController?.pushViewController(weightReductionVC, animated: true)
    }
    
    @IBAction func btnCancelClicked(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
        
        //        homeVC = TabBarVC(nibName: "TabBarVC", bundle: nil)
        //        homeVC.selectedIndexOfMyTabbarController = 0
        //        self.navigationC  = UINavigationController(rootViewController: homeVC!)
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.viewMapOptions.removeFromSuperview()
        
    }
    @IBAction func btnMapOptionCilcked(_ sender: UIButton) {
        if sender.tag == MapFormTag {
            let mapForm = MapOrderVC(nibName: "MapOrderVC", bundle: nil)
            mapForm.isFromSettings = true
            APPDELEGATE.arrayMAPOredrDrug = NSMutableArray()
            APPDELEGATE.myTabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(mapForm, animated: true)
        }
        else if sender.tag == savedMapTag {
            let saveForm = SavedMapListVC(nibName: "SavedMapListVC", bundle: nil)
            APPDELEGATE.myTabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(saveForm, animated: true)
        }
        else if sender.tag == submittedMapTag {
            let saveForm = SubmittedMapListVC(nibName: "SubmittedMapListVC", bundle: nil)
            APPDELEGATE.myTabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(saveForm, animated: true)
        }
        else if sender.tag == creditsTag {
            let credit = UserCreditVC(nibName: "UserCreditVC", bundle: nil)
            credit.isFromSettings = true
            APPDELEGATE.myTabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(credit, animated: true)
        }
        else if sender.tag == mapOrderHistoryTag {
            let history = MapOrderHistoryListVC(nibName: "MapOrderHistoryListVC", bundle: nil)
            
//            let history = MapOrderDetailVC(nibName: "MapOrderDetailVC", bundle: nil)
            APPDELEGATE.myTabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(history, animated: true)
        }
        else if sender.tag == userOrdersTag {
            let orderVC = UserOrderListVC(nibName: "UserOrderListVC", bundle: nil)
            APPDELEGATE.myTabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(orderVC, animated: true)
        }
        else if sender.tag == sellingHistoryTag {
            let selling = HistoryListVC(nibName: "HistoryListVC", bundle: nil)
            APPDELEGATE.myTabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(selling, animated: true)
        }
        
    }
    func postDataGetUserDetail(){
        
        let completeURL = NSString(format:"%@%@", MainURL,getUserProfileURL) as String
        
        let finalParams:NSDictionary = [
            "data" :["lang_type":Language_Type,
                     "user_id":USERDEFAULT.value(forKey: "userID") as? String ?? "",
                     "token":USERDEFAULT.value(forKey: "token") as? String ?? ""
            ]
        ]
        
        print("getUserProfileURL URL :",completeURL)
        print("getUserProfileURL Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getUserProfileURLTag)
    }
    
    //MARK:- Web service call Methods
    func postDataCheckUserCredit(){
        
        let completeURL = NSString(format:"%@%@", MainURL,checkCreditUrl) as String
        
        let finalParams:NSDictionary = [
            "data" :["lang_type":Language_Type,
                     "user_id":USERDEFAULT.value(forKey: "userID") as? String ?? "",
                     "token":USERDEFAULT.value(forKey: "token") as? String ?? ""
            ]
        ]
        
        print("Check Credit URL :",completeURL)
        print("Check Credit Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: checkCreditUrlTag)
    }
    
    
    //MARK:- Web Service Callback Methods
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
            
        case checkCreditUrlTag:
            let resultDict = responseObject as! NSDictionary;
            
            SVProgressHUD.dismiss()
            print("Chcek Credit response -> \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                let temp = (resultDict.value(forKey: "data")as! NSDictionary).mutableCopy() as! NSMutableDictionary
                temp.setValue("xyz", forKey: "XYZ")
                labelCredit.text = "\(temp.value(forKey: "credit")as! Int) Cr."
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                if !(loginView.isDescendant(of: self.view)){
                    self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
                }
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                labelCredit.text = "0 Cr."
                //labelCreditCaption.text = resultDict.value(forKey: "message") as? String
                
            }
            
            break
            
        case getUserProfileURLTag :
            let resultDict = responseObject as! NSDictionary;
            
            SVProgressHUD.dismiss()
            print("getUserProfileURLTag response -> \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                let temp = (resultDict.value(forKey: "data")as! NSDictionary)
                let scp_start_date = temp["scp_start_date"]as? String ?? ""
                
                if scp_start_date == "1"{
                    isSmokeSet = true
                }
                else{
                    isSmokeSet = false
                }
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                if !(loginView.isDescendant(of: self.view)){
                    self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
                }
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                labelCredit.text = "0 Cr."
                //labelCreditCaption.text = resultDict.value(forKey: "message") as? String
                
            }
            break
            
        default:
            break
            
        }
    }
    
    
    
    
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
            
        case checkCreditUrlTag:
                labelCredit.isHidden = true
            break
        case getUserProfileURLTag:
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
//        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    //MARK:- Other
    
    
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let loginAlert = UIAlertController(title: messageT, message: title, preferredStyle:.alert)
        let actionOK = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        let actionLogin = UIAlertAction(title: "LOGIN", style: .default) { (actionLogin) in
            APPDELEGATE.logOutUserWithoutToken()
            let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        
        loginAlert.addAction(actionOK)
        loginAlert.addAction(actionLogin)
        
        if !(self.presentedViewController != nil) {
            self.present(loginAlert, animated: true, completion: nil)
        }
    }
}
