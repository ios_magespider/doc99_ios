//
//  ResetPasswordVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 14/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD
class ResetPasswordVC: UIViewController,UIAlertViewDelegate {
    // FIXME: - VARIABLE
    
    @IBOutlet var txtOldPassword:UITextField!
    @IBOutlet var txtNewPassword:UITextField!
    @IBOutlet var txtConfirmPassword:UITextField!
    
    @IBOutlet var lblHeader:UILabel!
    @IBOutlet var lblTitleCurrentPassword:UILabel!
    @IBOutlet var lblTitleNewPassword:UILabel!
    @IBOutlet var lblTitleReNewPassword:UILabel!
    @IBOutlet var btnResetPassword:UIButton!


//    @IBOutlet var scrMain:UIScrollView!

    // FIXME: - VIEW CONTROLLER METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        self.addTapGestureInOurView()
        self.setLocalizationText()
    
    }
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        if viewTouched!.isKind(of: UIButton.self){
            
        }
        else{
            self.view.endEditing(true)
            //scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        }
    }
    
    func setLocalizationText(){
        
        btnResetPassword.setTitle(NSLocalizedString("RESET PASSWORD", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        lblHeader.text = NSLocalizedString("Reset Password", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitleCurrentPassword.text = NSLocalizedString("Current Password", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitleNewPassword.text = NSLocalizedString("New Password", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitleReNewPassword.text = NSLocalizedString("Re-Type New Password", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
        
        
    }
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    
    //AlertDelegate Methods protocol
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }

    
    
    
    
    
    // TODO: - DELEGATE METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
           // scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
            
        }
        return false
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var YOffset:Int = 0
        
        if (textField.tag==4) {
            YOffset=10
        }
        else if (textField.tag==5){
            YOffset=12
        }
        else if (textField.tag==6){
            YOffset=12
        }
        
        if (DeviceType.IS_IPHONE_4_OR_LESS) {
            
            if (textField.tag==4) {
                YOffset=15
            }
            else if (textField.tag==5){
                YOffset=22
            }
            else if (textField.tag==6){
                YOffset=22
            }
        }
      //  scrMain.setContentOffset(CGPoint(x: 0, y: CGFloat(textField.tag * YOffset)), animated: true)
        return true
        
    }
    
    // TODO: - ACTION METHODS
    
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        self.view.endEditing(true)
      //  scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
        if txtOldPassword.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter current password", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if txtNewPassword.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter new password", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if txtConfirmPassword.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter Re-type password", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if txtNewPassword.text != txtConfirmPassword.text{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please doesn't matched", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else{
            if Reachability.isConnectedToNetwork() == true {
                SVProgressHUD.show(withStatus: "Loading..")
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForResetPassword), userInfo: nil, repeats: false)
            } else {
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }
    }
    
    @IBAction func btnBackClicked(_ sender:UIButton){
        self.navigationController?.popViewController(animated:  true)
    }
    
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForResetPassword(){
        let completeURL = NSString(format:"%@%@", MainURL,resetPasswordURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "oldpassword":txtOldPassword.text!,
            "newpassword":txtNewPassword.text!,
            "lang_type":Language_Type
        ]
        
        

        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("ResetPassword API Parameter :",finalParams)
        print("ResetPassword API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: resetPasswordURLTag)
    }
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case resetPasswordURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("ResetPassword Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
                self.navigationController?.popViewController(animated: true)
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }

            
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case resetPasswordURLTag:
            SVProgressHUD.dismiss()
            break
        default:
            break
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
}
