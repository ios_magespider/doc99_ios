//
//  AuthorDetailVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 09/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class AuthorDetailVC: UIViewController {
    // FIXME: - VIEW CONTROLLER METHODS

    @IBOutlet var scrMain:UIScrollView!
    @IBOutlet var lblDescription:UILabel!
    @IBOutlet var imageViewAuthor:UIImageView!
    @IBOutlet var viewDetail:UIView!
    @IBOutlet var lblHeader:UILabel!
    @IBOutlet var lblAuthorName:UILabel!

    
  //  var authorData:NSDictionary!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        viewDetail.frame = CGRect(x: 0, y: 0, width: scrMain.frame.size.width, height: viewDetail.frame.size.height)
        scrMain.addSubview(viewDetail)
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
       // self.setAuthorDetailData(authorData: authorData)
    }
    
    func setAuthorDetailData(authorData:NSDictionary){
        
        print("Drug Detail : ",authorData)
        
        if let drug_name =  authorData.value(forKey: "author_name") as? String
        {
            lblHeader.text = "\(drug_name)"
        }
        if let drug_name =  authorData.value(forKey: "author_name") as? String
        {
            lblAuthorName.text = "\(drug_name)"
        }
        
        lblDescription.text = authorData.value(forKey: "drug_description") as? String
        lblDescription = Utility.setlableFrame(lblDescription, fontSize: 13.0)
        
        if (CGFloat((lblDescription.frame.size.height)) > 20.0) {
            viewDetail.frame = CGRect(x: viewDetail.frame.origin.x, y: viewDetail.frame.origin.y , width: viewDetail.frame.size.width, height: lblDescription.frame.origin.y + lblDescription.frame.size.height + 10)
            scrMain.contentSize = CGSize(width: scrMain.frame.size.width, height: viewDetail.frame.size.height)
        }
    }
    
    // TODO: - DELEGATE METHODS
    
    
    // TODO: - ACTION METHODS
    @IBAction func btnBackClicked(_ sender:UIButton){
        _ =  self.navigationController?.popViewController(animated: true)
    }
    
    
    // TODO: - POST DATA METHODS
}
