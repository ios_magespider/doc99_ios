//
//  SearchDrugVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 18/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SDWebImage

class SearchDrugVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate {
    
    @IBOutlet var tableViewDrugList:UITableView!
    
    @IBOutlet var txtSearch:UITextField!

    
    //For Pasignation
    var pageNum:Int!
    var isLoading:Bool?
    
    var drugData = NSMutableArray()
    var drugGlobalData = NSMutableArray()
    
    
    var timer: Timer? = nil
    
    @IBOutlet var lblNoDrug:UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // TODO: - OTHER METHODS
    
    func generalViewControllerSetting(){
        
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.drugData = NSMutableArray()
        self.drugGlobalData = NSMutableArray()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetDrugList), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        self.addTapGestureInOurView()
        self.setLocalizationText()
    }
    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        if viewTouched!.isKind(of: UIButton.self) && viewTouched!.isKind(of:UITextView.self){
            
        }
        else{
            self.view.endEditing(true)
        }
        
    }
    
    
    
    func addLoadingIndicatiorOnFooterOnTableView(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableViewDrugList.tableFooterView = spinner
    }
    func removeLoadingIndicatiorOnFooterOnTableView(){
        tableViewDrugList.tableFooterView = nil
    }
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }
    
    func setLocalizationText(){
        lblNoDrug.text = NSLocalizedString("nodrug", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
    }
    
    // TODO: - Textfield DELEGATE METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        return false
    }

    
    // TODO: - DELEGATE ScrollView
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == tableViewDrugList {
            if isLoading == true{
                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                    pageNum = pageNum + 1
                    print(pageNum)
                    isLoading = false
                    
                    if Reachability.isConnectedToNetwork() == true {
                        //self.addLoadingIndicatiorOnFooterOnTableView()
 
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetDrugList), userInfo: nil, repeats: false)
                        
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
            }
        }
    }
    
    
    // TODO: - DELEGATE METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.drugData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let identifier = "drugListCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? DrugListTableVC
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("DrugListTableVC", owner: self, options: nil)
                cell = nib?[0] as? DrugListTableVC
            }
            cell!.selectionStyle = .none;
            
            
            if let drug_name =  (self.drugData[indexPath.row] as AnyObject).value(forKey: "drug_name") as? String
            {
                cell?.lblDrugName.text = "\(drug_name)"
            }
            
            if let medical_name =  (self.drugData[indexPath.row] as AnyObject).value(forKey: "medical_name") as? String
            {
                cell?.lblMedicalName.text = "\(medical_name)"
            }
            
            if let weight =  (self.drugData[indexPath.row] as AnyObject).value(forKey: "weight") as? String
            {
                cell?.lblSize.text = "\(weight)"
            }
            
            
            let imageUrl = (self.drugData[indexPath.row] as AnyObject).value(forKey: "front_image") as? String
            let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl!) as String
            let url : NSString = fullUrl as NSString
            let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
            let searchURL : NSURL = NSURL(string: urlStr as String)!
            
            
            cell?.activityIndicatorForDrugImage.isHidden = false
            cell?.activityIndicatorForDrugImage.startAnimating()
            
            cell?.imageViewDrug.sd_setImage(with: searchURL as URL, completed: { (image:UIImage?, error:Error?, cacheType:SDImageCacheType!, imageURL:URL?) in
                
                if ((error) != nil) {
                    cell?.imageViewDrug.image = UIImage.init(named: "image_placeholder.png")
                }
                
                cell?.activityIndicatorForDrugImage.isHidden = true
                
            })
            return cell!
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let drugDetailVC = DrugDetailVC(nibName: "DrugDetailVC", bundle: nil)
            drugDetailVC.drugID = (self.drugData[indexPath.row] as AnyObject).value(forKey: "drug_id") as! String
            self.navigationController?.pushViewController(drugDetailVC, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105.0

    }
    
    // TODO: - ACTION METHODS
    @IBAction func btnBackClicked(_ sender:UIButton){
        _ =  self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchFilter(){

        tableViewDrugList.isHidden = false
        lblNoDrug.isHidden = true
        
        self.drugData.removeAllObjects()
        tableViewDrugList.reloadData()
        
        if (txtSearch.text == "") {
            //drugData = drugGlobalData.mutableCopy() as! NSMutableArray
            
            pageNum = 1
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetDrugList), userInfo: nil, repeats: false)
            
        }else{
            
            if Reachability.isConnectedToNetwork() == true {
                
                pageNum = 1
                
                self.addLoadingIndicatiorOnFooterOnTableView()
                //Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetDrugList), userInfo: nil, repeats: false)
                
                timer?.invalidate()
                timer = Timer.scheduledTimer(
                    timeInterval: 2,
                    target: self,
                    selector: #selector(self.postDataOnWebserviceForGetDrugList),
                    userInfo: nil,
                    repeats: false)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
//            predicate = String(format: "SELF['drug_name'] contains[c] '%@'", txtSearch.text!)
//            print(predicate)
//            
//            let myPredicate:NSPredicate = NSPredicate(format:predicate)
//            let temp = self.drugGlobalData.mutableCopy() as! NSArray
//            self.drugData = (temp.filtered(using: myPredicate) as NSArray).mutableCopy() as! NSMutableArray
            
        }
        
//        if self.drugData.count == 0 {
//            lblNoDrug.isHidden = false
//            tableViewDrugList.isHidden = true
//         }
//         else{
//             tableViewDrugList.isHidden = false
//             lblNoDrug.isHidden = true
//         }
       // tableViewDrugList.reloadData()
        
        
    }
    //MARK:
    
//    func getHints(timer: Timer) {
//        let userInfo = timer.userInfo as! String
//        
//        print("Hints for textField: \(userInfo)")
//    }
    
    //MARK: - POST DATA METHODS
    func postDataOnWebserviceForGetDrugList(){
        self.addLoadingIndicatiorOnFooterOnTableView()
    
        let completeURL = NSString(format:"%@%@", MainURL,getDrugListURL) as String
        
        let pageNumber = "\(pageNum!)"
        
        var params:NSDictionary!
        
        let parameter = NSMutableDictionary()
        parameter.setValue(USERDEFAULT.value(forKey: "userID") as? String ?? "", forKey: "user_id")
        parameter.setValue(USERDEFAULT.value(forKey: "token") as? String ?? "", forKey: "token")
        parameter.setValue(Language_Type, forKey: "lang_type")
        parameter.setValue(pageNumber, forKey: "page")
        parameter.setValue(PAGINATION_LIMITE, forKey: "limit")
        
        
//        let userInfo = timer.userInfo
//        print("Hints for textField: \(userInfo)")
        
        if (!(txtSearch.text!) .isEmpty){
            parameter.setValue(txtSearch.text!, forKey: "drug_name")
        }
        
        params = parameter.mutableCopy() as! NSDictionary

        
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetDrugList API Parameter :",finalParams)
        print("GetDrugList API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getDrugListURLTag)
        
    }
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case getDrugListURLTag:
            let resultDict = responseObject as! NSDictionary;
            //print("GetDrugList List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum == 1{
                    self.drugData = NSMutableArray()
                    self.drugGlobalData = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        self.drugGlobalData.add(myData[i])
                    }
                    
                    for i in 0...myData.count - 1 {
                        self.drugData.add(myData[i])
                    }
                    
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum > 0) {
                            self.pageNum = self.pageNum - 1
                        }
                        self.isLoading = false
                    }else{
                        self.isLoading = true
                    }
                }
                else{
                    self.isLoading = false
                    if (self.pageNum > 0) {
                        self.pageNum = self.pageNum - 1
                    }
                    
                    if self.drugData.count == 0{
                        self.tableViewDrugList.isHidden = true
                        self.lblNoDrug.isHidden = false
                    }
                    else{
                        self.tableViewDrugList.isHidden = false
                        self.lblNoDrug.isHidden = true
                    }
                }
                self.tableViewDrugList.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case getDrugListURLTag:
            self.isLoading = false
            if (self.pageNum > 0) {
                self.pageNum = self.pageNum - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    
    
}
