//
//  DrugFilterTableVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 15/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class DrugFilterTableVC: UITableViewCell {

    
    @IBOutlet var imageViewSelect:UIImageView!
    @IBOutlet var lblCategoryName:UILabel!
    @IBOutlet var btnCategory:UIButton!
    
    @IBOutlet var imageViewDisplay:UIImageView!
    @IBOutlet var activityIndicatorForDisplay:UIActivityIndicatorView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
