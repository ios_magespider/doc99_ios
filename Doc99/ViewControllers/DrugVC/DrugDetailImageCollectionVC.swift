//
//  DrugDetailImageCollectionVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 15/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class DrugDetailImageCollectionVC: UICollectionViewCell {

    @IBOutlet var imageViewDrug:UIImageView!
    @IBOutlet var activityIndicatorForDrugImage:UIActivityIndicatorView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
