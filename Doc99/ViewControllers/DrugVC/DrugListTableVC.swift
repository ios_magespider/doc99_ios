//
//  DrugListTableVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 13/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class DrugListTableVC: UITableViewCell {

    
    @IBOutlet var lblDrugName:UILabel!
    @IBOutlet var lblMedicalName:UILabel!
    @IBOutlet var lblSize:UILabel!
    @IBOutlet var imageViewDrug:UIImageView!
    @IBOutlet var activityIndicatorForDrugImage:UIActivityIndicatorView!

//    @IBOutlet weak var viewBtnPlusMinus: UIView!
//    @IBOutlet weak var btnMinus: UIButton!
//    @IBOutlet weak var labelItemNo: UILabel!
//    @IBOutlet weak var btnPlus: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
