//
//  DrugDetailVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 15/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD

class DrugDetailVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,UIAlertViewDelegate,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var scrMain:UIScrollView!
    @IBOutlet var lblHeaderDrugName:UILabel!
    @IBOutlet var lblDrugName:UILabel!
    @IBOutlet var lblMedicalName:UILabel!
    @IBOutlet var lblSize:UILabel!
    @IBOutlet var lblDescription:UILabel!
    
    @IBOutlet var collectionViewDrugImages:UICollectionView!

    
    var drugID:String!
    
    var drugImageGalleryData:NSMutableArray!

    var drugCategoryData = NSMutableArray()
    var drugCategoryGlobalData = NSMutableArray()

    var arrayDrugDetail = NSDictionary()
    
    let reuseIdentifier = "drugImagesCell"
    
    @IBOutlet var pageControl : UIPageControl!

    
    @IBOutlet var viewDescription:UIView!
    @IBOutlet var viewIngridentTitle:UIView!
    @IBOutlet var viewIngrident:UIView!
    @IBOutlet var viewDrugCategory:UIView!
    @IBOutlet var tableViewDrugCategory:UITableView!

    @IBOutlet var viewDetail:UIView!
    
    @IBOutlet var lblIngridient:UILabel!


    // MARK: - VIEW CONTROLLER METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionViewDrugImages.register(UINib(nibName: "DrugDetailImageCollectionVC", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)

        self.generalViewControllerSetting()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - OTHER METHODS
    func generalViewControllerSetting(){
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetDrugDetail), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        
        viewDetail.frame =  CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: scrMain.frame.size.height)
        scrMain.addSubview(viewDetail)
        
    }
    
    func setDrugDetailData(drugData:NSDictionary){
        
        print("Drug Detail : ",drugData)
        
        arrayDrugDetail = drugData
        
        
        if let drug_name =  drugData.value(forKey: "drug_name") as? String
        {
            lblDrugName.text = "\(drug_name)"
        }
        if let drug_name =  drugData.value(forKey: "drug_name") as? String
        {
            lblHeaderDrugName.text = "\(drug_name)"
        }
      
        if let medical_name =  drugData.value(forKey: "certificate_holder") as? String
        {
            lblMedicalName.text = "\(medical_name)"
        }
        
        if let weight =  drugData.value(forKey: "weight") as? String
        {
            let wei = "\(weight)"
            if wei == ""{
                lblSize.text = ""
            }
            else{
                lblSize.text = "\(wei)"
            }
            
        }

        if let ingredient =  drugData.value(forKey: "ingredient") as? String
        {
            lblIngridient.text = "\(ingredient)"
        }
        
     
        lblDescription.text = drugData.value(forKey: "drug_description") as? String
        lblDescription = Utility.setlableFrame(lblDescription, fontSize: 13.0)
        
        if (CGFloat((lblDescription.frame.size.height)) > 20.0) {
             viewDescription.frame = CGRect(x: viewDescription.frame.origin.x, y: viewDescription.frame.origin.y, width: viewDescription.frame.size.width, height: lblDescription.frame.size.height + lblDescription.frame.origin.y + 10.0)
            viewIngridentTitle.frame = CGRect(x: viewIngridentTitle.frame.origin.x, y: viewDescription.frame.origin.y + viewDescription.frame.size.height, width: viewIngridentTitle.frame.size.width, height: viewIngridentTitle.frame.size.height )
            viewIngrident.frame = CGRect(x: viewIngrident.frame.origin.x, y: viewIngridentTitle.frame.origin.y + viewIngridentTitle.frame.size.height, width: viewIngrident.frame.size.width, height: viewIngrident.frame.size.height )
            viewDrugCategory.frame = CGRect(x: viewIngrident.frame.origin.x, y: viewIngrident.frame.origin.y + viewIngrident.frame.size.height, width: viewDrugCategory.frame.size.width, height: viewDrugCategory.frame.size.height )
            
            if tableViewDrugCategory.contentSize.height > tableViewDrugCategory.frame.size.height{
                
                tableViewDrugCategory.frame = CGRect(x: tableViewDrugCategory.frame.origin.x, y: viewDrugCategory.frame.origin.y + viewDrugCategory.frame.size.height, width: tableViewDrugCategory.frame.size.width, height: tableViewDrugCategory.contentSize.height)
            }
            else{
                tableViewDrugCategory.frame = CGRect(x: tableViewDrugCategory.frame.origin.x, y: viewDrugCategory.frame.origin.y + viewDrugCategory.frame.size.height, width: tableViewDrugCategory.frame.size.width, height: tableViewDrugCategory.frame.size.height)
            }

            viewDetail.frame = CGRect(x: viewDetail.frame.origin.x, y: viewDetail.frame.origin.y , width: viewDetail.frame.size.width, height: tableViewDrugCategory.frame.origin.y + tableViewDrugCategory.frame.size.height)

            scrMain.contentSize = CGSize(width: scrMain.frame.size.width, height: viewDetail.frame.size.height)
        }
        else{
            
            viewDescription.frame = CGRect(x: viewDescription.frame.origin.x, y: viewDescription.frame.origin.y, width: viewDescription.frame.size.width, height: viewDescription.frame.size.height)
            viewIngridentTitle.frame = CGRect(x: viewIngridentTitle.frame.origin.x, y: viewDescription.frame.origin.y + viewDescription.frame.size.height, width: viewIngridentTitle.frame.size.width, height: viewIngridentTitle.frame.size.height )
            viewIngrident.frame = CGRect(x: viewIngrident.frame.origin.x, y: viewIngridentTitle.frame.origin.y + viewIngridentTitle.frame.size.height, width: viewIngrident.frame.size.width, height: viewIngrident.frame.size.height )
            viewDrugCategory.frame = CGRect(x: viewIngrident.frame.origin.x, y: viewIngrident.frame.origin.y + viewIngrident.frame.size.height, width: viewDrugCategory.frame.size.width, height: viewDrugCategory.frame.size.height )
            
            print("Tableview Content Size :",tableViewDrugCategory.contentSize.height)
            
            if tableViewDrugCategory.contentSize.height > tableViewDrugCategory.frame.size.height{
                
            tableViewDrugCategory.frame = CGRect(x: tableViewDrugCategory.frame.origin.x, y: viewDrugCategory.frame.origin.y + viewDrugCategory.frame.size.height, width: tableViewDrugCategory.frame.size.width, height: tableViewDrugCategory.contentSize.height)
            }
            else{
             tableViewDrugCategory.frame = CGRect(x: tableViewDrugCategory.frame.origin.x, y: viewDrugCategory.frame.origin.y + viewDrugCategory.frame.size.height, width: tableViewDrugCategory.frame.size.width, height: tableViewDrugCategory.frame.size.height)
            }
            
            viewDetail.frame = CGRect(x: viewDetail.frame.origin.x, y: viewDetail.frame.origin.y , width: viewDetail.frame.size.width, height: tableViewDrugCategory.frame.origin.y + tableViewDrugCategory.frame.size.height)
            
            scrMain.contentSize = CGSize(width: scrMain.frame.size.width, height: viewDetail.frame.size.height)
        }
        
        drugImageGalleryData = NSMutableArray(array: drugData.value(forKey: "gallery") as! NSArray)
        pageControl.numberOfPages = drugImageGalleryData.count;

        collectionViewDrugImages.reloadData()
    }
 
    // MARK: - DELEGATE METHODS
    // MARK: - UICollectionViewDataSource protocol
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let drugDetailData = drugImageGalleryData {
            if drugDetailData.count == 0{
                return 1
            }
            else{
                return drugDetailData.count
            }
        
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! DrugDetailImageCollectionVC
        
        if let drugDetailData = drugImageGalleryData {
            if drugDetailData.count > 0{
                if ((drugDetailData[indexPath.row] as AnyObject).value(forKey: "image") as? String == ""){
                    cell.imageViewDrug.image = UIImage.init(named: "image_placeholder.png")
                    cell.activityIndicatorForDrugImage.isHidden = true
                    
                }
                else{
                    let imageUrl = (drugDetailData[indexPath.row] as AnyObject).value(forKey: "image") as? String
                    let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl!) as String
                    let url : NSString = fullUrl as NSString
                    let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
                    let searchURL : NSURL = NSURL(string: urlStr as String)!
                    
                    cell.activityIndicatorForDrugImage.isHidden = false
                    cell.activityIndicatorForDrugImage.startAnimating()
                    
                    
                    cell.imageViewDrug.sd_setImage(with: searchURL as URL, completed: { (image:UIImage?, error:Error?, cacheType:SDImageCacheType!, imageURL:URL?) in
                        
                        if ((error) != nil) {
                            cell.imageViewDrug.image = UIImage.init(named: "image_placeholder.png")
                        }
                        
                        cell.activityIndicatorForDrugImage.isHidden = true
                        
                    })
                }
                
            }
            else{
                cell.imageViewDrug.image = UIImage.init(named: "image_placeholder.png")
                cell.activityIndicatorForDrugImage.isHidden = true
            }
            
        }
        else{
            cell.imageViewDrug.image = UIImage.init(named: "image_placeholder.png")
            cell.activityIndicatorForDrugImage.isHidden = true
        }

        
        return cell

    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        pageControl.currentPage = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionViewDrugImages.frame.size.width, height: collectionViewDrugImages.frame.size.height)
    }

    
    
    // TODO: - DELEGATE METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.drugCategoryData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "healthCategory"
        var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? HealthCategoryTableVC
        
        if cell == nil {
            let nib  = Bundle.main.loadNibNamed("HealthCategoryTableVC", owner: self, options: nil)
            cell = nib?[0] as? HealthCategoryTableVC
        }
        cell!.selectionStyle = .none;
        
        
        if let articalCategoryName =  (self.drugCategoryData[indexPath.row] as AnyObject).value(forKey: "ac_name") as? String
        {
            cell?.lblTitle.text = "\(articalCategoryName)"
        }
        
        if let articalCategoryName =  (self.drugCategoryData[indexPath.row] as AnyObject).value(forKey: "ac_name") as? String
        {
            cell?.lblSubTitle.text = "\(articalCategoryName)"
        }
        
        cell?.imageViewSelect.isHidden = true
        cell?.btnSelect.isHidden = true
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    
    // TODO: - ACTION METHODS
    @IBAction func btnBackClicked(_ sender:UIButton){
        _ =  self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSearchClicked(_ sender:UIButton){
        //        txtSearch.becomeFirstResponder()
        //
        //        UIView.animate(withDuration: 0.3) {
        //
        //            self.viewSearchDrug.frame = CGRect(x: self.headerView.frame.origin.x, y: self.headerView.frame.origin.y, width: self.viewSearchDrug.frame.size.width, height: self.viewSearchDrug.frame.size.height)
        //
        //        }
        
        let searchVC = SearchDrugVC(nibName: "SearchDrugVC", bundle: nil)
        self.navigationController?.pushViewController(searchVC, animated: true)
        
    }
    @IBAction func btnAddToCartClicked(_ sender: Any) {
        if APPDELEGATE.arrayMAPOredrDrug.count > 0 {
            
            var index = 0
            var found = false
            var i = 0
            
            for temp in APPDELEGATE.arrayMAPOredrDrug{
                if ((temp as! NSDictionary).value(forKey: "drug_id") as? String) == arrayDrugDetail.value(forKey: "drug_id") as? String{
                    found = true
                    index = i
                    //i = i+1
                    break
                }
                else{
                    i = i+1
                    found = false
                }
            }
            
            
            if found == true{
                //replace at index
                
                print("replacing at index .. \(index)")
                
                var qty = Int(((APPDELEGATE.arrayMAPOredrDrug[index] as! NSDictionary).value(forKey: "qty") as? String)!)
                qty = qty! + 1
                
                let x = (APPDELEGATE.arrayMAPOredrDrug[index] as! NSDictionary).mutableCopy() as! NSMutableDictionary
                x.setValue(String(qty!), forKey: "qty")
                
                APPDELEGATE.arrayMAPOredrDrug.replaceObject(at: i, with: x)
                
            }
            else{
                print("Created new entry for non-empty array..")
                
                let arr : NSDictionary = [
                    "drug_id" : arrayDrugDetail.value(forKey: "drug_id") as? String ?? "",
                    "product_id": "1",
                    "product_name": arrayDrugDetail.value(forKey: "drug_name") as? String ?? "",
                    "product_desc" : arrayDrugDetail.value(forKey: "drug_description") as? String ?? "",
                    "price" : "0",
                    "qty" : "1",
                    "packaging_unit" : "",
                    "m_country" : "",
                    "weight" : arrayDrugDetail.value(forKey: "weight") as? String ?? "",
                    ]
                APPDELEGATE.arrayMAPOredrDrug.add(arr)
            }
        }
        else{
            print("Created new entry for blank array..")
            
            let arr : NSDictionary = [
                "drug_id" : arrayDrugDetail.value(forKey: "drug_id") as? String ?? "",
                "product_id": "1",
                "product_name": arrayDrugDetail.value(forKey: "drug_name") as? String ?? "",
                "product_desc" : arrayDrugDetail.value(forKey: "drug_description") as? String ?? "",
                "price" : "0",
                "qty" : "1",
                "packaging_unit" : "",
                "m_country" : "",
                "weight" : arrayDrugDetail.value(forKey: "weight") as? String ?? "",
                ]
            APPDELEGATE.arrayMAPOredrDrug.add(arr)
        }
        
        print("arrayMAPOredrDrug --> \(APPDELEGATE.arrayMAPOredrDrug)")

        let mapOrder = MapOrderVC(nibName: "MapOrderVC", bundle: nil)
        self.navigationController?.pushViewController(mapOrder, animated: true)
    }
    
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForGetDrugDetail(){
        let completeURL = NSString(format:"%@%@", MainURL,drugDetailURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as? String ?? "",
            "token": USERDEFAULT.value(forKey: "token") as? String ?? "",
            "lang_type":Language_Type,
            "drug_id":drugID
        ]

        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetDrugDetail API Parameter :",finalParams)
        print("GetDrugDetail API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: drugDetailURLTag)
        
    }
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case drugDetailURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("GetDrugDetail List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                self.setDrugDetailData(drugData: resultDict.value(forKey: "data") as! NSDictionary)
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case drugDetailURLTag:
            SVProgressHUD.dismiss()

            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    

    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }
    
    
}
