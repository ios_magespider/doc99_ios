//
//  DrugFilterVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 15/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SDWebImage
class DrugFilterVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UIAlertViewDelegate {
    
    @IBOutlet var tableViewFilterDrug:UITableView!
    @IBOutlet var tableViewDosage:UITableView!

    @IBOutlet var collectionViewFilterDrug:UICollectionView!

    @IBOutlet var lblCategoryCount:UILabel!
    @IBOutlet var lblDoseCategoryCount:UILabel!

    
    let reuseIdentifier = "drugFilterCollectionCell"

    @IBOutlet var scrMain:UIScrollView!
    @IBOutlet var viewMedicineForm:UIView!
    @IBOutlet var viewDiesesType:UIView!

    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    
    //For Pasignation
    var pageNum:Int!
    var isLoading:Bool?
    
    var diseaseData = NSMutableArray()
    var diseaseGlobalData = NSMutableArray()
    
    var boolArray:NSMutableArray!
    var categoryCount:Int!
    var categoryDoseCount:Int!

    var diseaseTypeData = NSMutableArray()
    var diseaseCategoryData = NSMutableArray()

    
    var drugCategoryData = NSMutableArray()
    var drugCategoryGlobalData = NSMutableArray()
    
    
    var finalFilterData = NSMutableDictionary()
    
    var boolArrayForCategory:NSMutableArray!
    var zeroTempDataArray:NSMutableArray!

    

    var alreadyFilterData = NSDictionary()


    @IBOutlet var lblFilterHeader:UILabel!
    @IBOutlet var lblMedicineForm:UILabel!
    @IBOutlet var lblDieseaseType:UILabel!
    @IBOutlet var btnReset:UIButton!
    @IBOutlet var btnApply:UIButton!
    
    @IBOutlet var viewIngredient:UIView!
    @IBOutlet var viewCertificateHolder:UIView!

    @IBOutlet var txtIngredient:UITextField!
    @IBOutlet var txtCertificateHolder:UITextField!

    var totalFilterType:Int!
    
    var activeTextField:UITextField?

    
    // FIXME: - VIEW CONTROLLER METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        
//        self.collectionViewFilterDrug.register(UINib(nibName: "DrugFIlterCollectionVC", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow), name: .UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden), name: .UIKeyboardWillHide, object: nil)

        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.diseaseData = NSMutableArray()
        self.diseaseGlobalData = NSMutableArray()
        
//        if Reachability.isConnectedToNetwork() == true {
//            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetDrugCategory), userInfo: nil, repeats: false)
//        }else{
//            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
//        }
        
        

        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.postDataOnWebserviceForGetDiseaseTye), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
      self.setLocalizationText()
        self.addTapGestureInOurView()

    }
    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        if viewTouched!.isKind(of: UIButton.self){
            
        }
        else{
            self.view.endEditing(true)
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        }
    }
    
    func addLoadingIndicatiorOnFooterOnTableView(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableViewFilterDrug.tableFooterView = spinner
    }
    func removeLoadingIndicatiorOnFooterOnTableView(){
        tableViewFilterDrug.tableFooterView = nil
    }
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }

    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }
    
    func setLocalizationText(){
        
        lblFilterHeader.text = NSLocalizedString("filter", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
//        lblMedicineForm.text = NSLocalizedString("medicineform", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
//        lblDieseaseType.text = NSLocalizedString("diseasetype", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

        btnReset.setTitle(NSLocalizedString("reset", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        btnApply.setTitle(NSLocalizedString("apply", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        
    }
    
    

    
    // MARK: - DELEGATE ScrollView
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        //tableViewFilterDrug.frame = CGRect(x: 0, y: tableViewFilterDrug.frame.origin.y, width: scrMain.frame.size.width, height: tableHeight.constant)
        
    }
    
    // MARK: - DELEGATE METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }

    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        activeTextField = nil
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        activeTextField = textField
    }
    
    
    // TODO: - DELEGATE METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 100{
            return self.diseaseData.count
        }
        else{
            return self.drugCategoryData.count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 100{
            let identifier = "drugFilterCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? DrugFilterTableVC
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("DrugFilterTableVC", owner: self, options: nil)
                cell = nib?[0] as? DrugFilterTableVC
            }
            cell!.selectionStyle = .none;
            
            
            if let articalCategoryName =  (self.diseaseData[indexPath.row] as AnyObject).value(forKey: "hc_name") as? String
            {
                cell?.lblCategoryName.text = "\(articalCategoryName)"
            }
            
            if boolArray.object(at: indexPath.row) as! String == "1"{
                cell?.imageViewSelect.image = UIImage.init(named: "check_blue.png")
                cell?.lblCategoryName.textColor = UIColor.black
                cell?.lblCategoryName.font = UIFont(name:"Lato-Bold", size: 14.0)
            }
            else{
                cell?.imageViewSelect.image = UIImage.init(named: "check_gray.png")
                cell?.lblCategoryName.textColor = UIColor.black
                cell?.lblCategoryName.font = UIFont(name:"Lato-Regular", size: 14.0)

            }
            
            
            
            
            if ((self.diseaseData.object(at: indexPath.row) as AnyObject).value(forKey: "image") as? String == "")
            {
                cell?.imageViewDisplay.image = UIImage.init(named: "image_placeholder.png")
                cell?.activityIndicatorForDisplay.isHidden = true
                
            }
            else{
                let imageUrl = (self.diseaseData.object(at: indexPath.row) as AnyObject).value(forKey: "image") as? String
                
                let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl!) as String
                let url : NSString = fullUrl as NSString
                let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
                let searchURL : NSURL = NSURL(string: urlStr as String)!
                
                cell?.activityIndicatorForDisplay.isHidden = false
                cell?.activityIndicatorForDisplay.startAnimating()
                
                
                cell?.imageViewDisplay.sd_setImage(with: searchURL as URL, completed: { (image:UIImage?, error:Error?, cacheType:SDImageCacheType!, imageURL:URL?) in
                    
                    if ((error) != nil) {
                        cell?.imageViewDisplay.image = UIImage.init(named: "image_placeholder.png")
                    }
                    cell?.activityIndicatorForDisplay.isHidden = true
                })
                
            }
            
            
            cell?.btnCategory.tag = indexPath.row
            cell?.btnCategory.addTarget(self, action: #selector(self.btnCategorySelectionClicked(_:)), for: .touchUpInside)
            
            return cell!
        }
        else{
            
            let identifier = "drugFilterCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? DrugFilterTableVC
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("DrugFilterTableVC", owner: self, options: nil)
                cell = nib?[0] as? DrugFilterTableVC
            }
            cell!.selectionStyle = .none;
            
            
            if let articalCategoryName =  (self.drugCategoryData.object(at: indexPath.row) as AnyObject).value(forKey: "df_form") as? String
            {
                cell?.lblCategoryName.text = "\(articalCategoryName)"
            }
            
            if boolArrayForCategory.object(at: indexPath.row) as! String == "1"{
                cell?.imageViewSelect.image = UIImage.init(named: "check_blue.png")
                cell?.lblCategoryName.textColor = UIColor.black
                cell?.lblCategoryName.font = UIFont(name:"Lato-Bold", size: 14.0)

            }
            else{
                cell?.imageViewSelect.image = UIImage.init(named: "check_gray.png")
                cell?.lblCategoryName.textColor = UIColor.black
                cell?.lblCategoryName.font = UIFont(name:"Lato-Regular", size: 14.0)

            }
            
            if ((self.drugCategoryData.object(at: indexPath.row) as AnyObject).value(forKey: "image") as? String == "")
            {
                cell?.imageViewDisplay.image = UIImage.init(named: "image_placeholder.png")
                cell?.activityIndicatorForDisplay.isHidden = true
                
            }
            else{
                let imageUrl = (self.drugCategoryData.object(at: indexPath.row) as AnyObject).value(forKey: "image") as? String
                
                let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl!) as String
                let url : NSString = fullUrl as NSString
                let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
                let searchURL : NSURL = NSURL(string: urlStr as String)!
                
                cell?.activityIndicatorForDisplay.isHidden = false
                cell?.activityIndicatorForDisplay.startAnimating()
                
                
                cell?.imageViewDisplay.sd_setImage(with: searchURL as URL, completed: { (image:UIImage?, error:Error?, cacheType:SDImageCacheType!, imageURL:URL?) in
                    
                    if ((error) != nil) {
                        cell?.imageViewDisplay.image = UIImage.init(named: "image_placeholder.png")
                    }
                    cell?.activityIndicatorForDisplay.isHidden = true
                })
                
            }

            
            cell?.btnCategory.tag = indexPath.row
            cell?.btnCategory.addTarget(self, action: #selector(self.btnCategoryDoseSelectionClicked(_:)), for: .touchUpInside)
            
            return cell!
        }
        
        
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.drugCategoryData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! DrugFIlterCollectionVC
        cell.lblName.text = (self.drugCategoryData.object(at: indexPath.row) as AnyObject).value(forKey: "df_form") as? String
        
       

//        if boolArrayForCategory.object(at: indexPath.row) as! String == "1"{
//            cell.lblName.textColor = UIColor.init(colorLiteralRed: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
//            
//            if ((self.drugCategoryData.object(at: indexPath.row) as AnyObject).value(forKey: "dc_image_selected") as? String == "")
//            {
//                cell.imageViewMedicine.image = UIImage.init(named: "image_placeholder.png")
//                cell.activityIndicatorForCategory.isHidden = true
//                
//            }
//            else{
//                let imageUrl = (self.drugCategoryData.object(at: indexPath.row) as AnyObject).value(forKey: "dc_image_selected") as? String
//                
//                let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl!) as String
//                let url : NSString = fullUrl as NSString
//                let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
//                let searchURL : NSURL = NSURL(string: urlStr as String)!
//                
//                cell.activityIndicatorForCategory.isHidden = false
//                cell.activityIndicatorForCategory.startAnimating()
//                
//                
//                cell.imageViewMedicine.sd_setImage(with: searchURL as URL, completed: { (image:UIImage?, error:Error?, cacheType:SDImageCacheType!, imageURL:URL?) in
//                    
//                    if ((error) != nil) {
//                        cell.imageViewMedicine.image = UIImage.init(named: "image_placeholder.png")
//                    }
//                    cell.activityIndicatorForCategory.isHidden = true
//                })
//                
//            }
//            
//        }
//        else{
//            cell.lblName.textColor = UIColor.init(colorLiteralRed: 108.0/255.0, green: 110.0/255.0, blue: 112.0/255.0, alpha: 0.5)
//            
//            if ((self.drugCategoryData.object(at: indexPath.row) as AnyObject).value(forKey: "image") as? String == "")
//            {
//                cell.imageViewMedicine.image = UIImage.init(named: "image_placeholder.png")
//                cell.activityIndicatorForCategory.isHidden = true
//                
//            }
//            else{
//                let imageUrl = (self.drugCategoryData.object(at: indexPath.row) as AnyObject).value(forKey: "image") as? String
//                
//                let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl!) as String
//                let url : NSString = fullUrl as NSString
//                let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
//                let searchURL : NSURL = NSURL(string: urlStr as String)!
//                
//                cell.activityIndicatorForCategory.isHidden = false
//                cell.activityIndicatorForCategory.startAnimating()
//                
//                
//                cell.imageViewMedicine.sd_setImage(with: searchURL as URL, completed: { (image:UIImage?, error:Error?, cacheType:SDImageCacheType!, imageURL:URL?) in
//                    
//                    if ((error) != nil) {
//                        cell.imageViewMedicine.image = UIImage.init(named: "image_placeholder.png")
//                    }
//                    cell.activityIndicatorForCategory.isHidden = true
//                })
//                
//            }
//            
//        }
//        cell.btnCategory.tag = indexPath.row
//        cell.btnCategory.addTarget(self, action: #selector(self.btnDrugCategorySelectionClicked(_:)), for: .touchUpInside)

        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }

    
    
    
    // TODO: - ACTION METHODS
    
    @IBAction func btnCategorySelectionClicked(_ sender:UIButton){
        
        if boolArray.count>0{
            boolArray.removeAllObjects()
            for _ in 0 ... diseaseData.count-1{
                boolArray.add("0")
            }
        }
        
        
        if boolArray.object(at: sender.tag) as! String == "0"{
            boolArray.replaceObject(at: sender.tag, with: "1")
            categoryCount = categoryCount + 1
        }
        else{
            boolArray.replaceObject(at: sender.tag, with: "0")
            categoryCount = categoryCount - 1
        }
        
        if categoryCount > 0 {
            lblCategoryCount.text = "\(categoryCount!) Selected"
        }
        else{
            lblCategoryCount.text = ""
        }
        
        
        let indexPath = NSIndexPath(row: sender.tag, section: 0)
        tableViewFilterDrug.reloadRows(at: [indexPath as IndexPath], with: .none)
        
        diseaseTypeData = NSMutableArray.init()
        
        for i in 0...diseaseData.count - 1 {
            if boolArray.object(at: i) as! String == "1"{
                diseaseTypeData.add((self.diseaseData[i] as AnyObject).value(forKey: "hc_id") as! String)
            }
        }
        
        print("diseaseTypeData ID",diseaseTypeData)
        
        finalFilterData.setValue(diseaseTypeData, forKey: "ddIDData")
        finalFilterData.setValue(categoryCount, forKey: "catCount")

        tableViewFilterDrug.reloadData()
        
    }
    
    @IBAction func btnCategoryDoseSelectionClicked(_ sender:UIButton){
        
        
        if boolArrayForCategory.object(at: sender.tag) as! String == "0"{
            boolArrayForCategory.replaceObject(at: sender.tag, with: "1")
            categoryDoseCount = categoryDoseCount + 1
        }
        else{
            boolArrayForCategory.replaceObject(at: sender.tag, with: "0")
            categoryDoseCount = categoryDoseCount - 1
        }
        
        if categoryDoseCount > 0 {
            lblDoseCategoryCount.text = "\(categoryDoseCount!) Selected"
        }
        else{
            lblDoseCategoryCount.text = ""
        }
        
        
        let indexPath = NSIndexPath(row: sender.tag, section: 0)
        tableViewDosage.reloadRows(at: [indexPath as IndexPath], with: .none)
        
        
        diseaseCategoryData = NSMutableArray.init()
        
        for i in 0...drugCategoryData.count - 1 {
            if boolArrayForCategory.object(at: i) as! String == "1"{
                diseaseCategoryData.add((self.drugCategoryData[i] as AnyObject).value(forKey: "df_id") as! String)
            }
        }
        
        print("diseaseCategoryData ID",diseaseCategoryData)
        
        finalFilterData.setValue(diseaseCategoryData, forKey: "dcIDData")
        finalFilterData.setValue(categoryDoseCount, forKey: "catDoseCount")
        
    }
    
    
    
    @IBAction func btnDrugCategorySelectionClicked(_ sender:UIButton){
        
        //Single Selection 
        
       // boolArrayForCategory.replaceObject(at: sender.tag, with: "1")
        let indexPath = NSIndexPath(row: sender.tag, section: 0)
        
        let myIndexValue:Int = boolArrayForCategory.index(of: "1")
        
        boolArrayForCategory = zeroTempDataArray.mutableCopy() as! NSMutableArray
        
        let oneDataArray = zeroTempDataArray.mutableCopy() as! NSMutableArray
        
        oneDataArray.replaceObject(at: indexPath.row, with: "1")
        boolArrayForCategory.replaceObject(at: indexPath.row, with: oneDataArray.object(at: indexPath.row))
        
        UIView.animate(withDuration: 0, animations: {
            self.collectionViewFilterDrug.performBatchUpdates({
                let indexPath1 = IndexPath(row: indexPath.row, section: 0)
                print("Indexpath1 \(indexPath1.row)")
                self.collectionViewFilterDrug.reloadItems(at: [indexPath1])
            }, completion: nil)
        })
        
        UIView.animate(withDuration: 0, animations: {
            self.collectionViewFilterDrug.performBatchUpdates({
                if (myIndexValue<self.boolArrayForCategory.count) {
                    let indexPath2 = IndexPath(row: myIndexValue, section: 0)
                    print("Indexpath2 \(indexPath2.row)")
                    self.collectionViewFilterDrug.reloadItems(at: [indexPath2])
                }
            }, completion: nil)
        })
        

        diseaseCategoryData = NSMutableArray.init()

        for i in 0...drugCategoryData.count - 1 {
            if boolArrayForCategory.object(at: i) as! String == "1"{
                diseaseCategoryData.add((self.drugCategoryData[i] as AnyObject).value(forKey: "df_id") as! String)
            }
        }

        print("diseaseCategoryData ID",diseaseCategoryData)

        finalFilterData.setValue(diseaseCategoryData, forKey: "dcIDData")

        
        
        
//        if boolArrayForCategory.object(at: sender.tag) as! String == "0"{
//            boolArrayForCategory.replaceObject(at: sender.tag, with: "1")
//        }
//        else{
//            boolArrayForCategory.replaceObject(at: sender.tag, with: "0")
//        }
//        
//        let indexPath = NSIndexPath(row: sender.tag, section: 0)
//        collectionViewFilterDrug.reloadItems(at: [indexPath as IndexPath])
//        
//        
//        diseaseCategoryData = NSMutableArray.init()
//        
//        for i in 0...drugCategoryData.count - 1 {
//            if boolArrayForCategory.object(at: i) as! String == "1"{
//                diseaseCategoryData.add((self.drugCategoryData[i] as AnyObject).value(forKey: "dc_id") as! String)
//            }
//        }
//        
//        print("diseaseCategoryData ID",diseaseCategoryData)
//        
//        finalFilterData.setValue(diseaseCategoryData, forKey: "dcIDData")
        
    }

   

    
    @IBAction func btnCloseClicked(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnResetClicked(_ sender:UIButton){
        boolArray = NSMutableArray.init()
        categoryCount = 0
        categoryDoseCount = 0

        if diseaseData.count > 0{
            for i in 0...diseaseData.count - 1 {
                boolArray.add("0")
            }
        }
        
        
        boolArrayForCategory = NSMutableArray.init()
        if drugCategoryData.count > 0{
            for _ in 0...drugCategoryData.count - 1 {
                boolArrayForCategory.add("0")
            }
        }
//        collectionViewFilterDrug.reloadData()
//        tableViewDosage.reloadData()
        
        tableViewFilterDrug.reloadData()
        lblCategoryCount.text = ""
//        lblDoseCategoryCount.text = ""

        
        USERDEFAULT.removeObject(forKey: "filterDrugData")
        USERDEFAULT.synchronize()
        
        let notificationName = Notification.Name("filterDrugList")
        NotificationCenter.default.post(name: notificationName, object: nil)
        
        _ = self.navigationController?.popViewController(animated: true)

        
    }
    
    @IBAction func btnApplyClicked(_ sender:UIButton){
        
        if txtIngredient.text != ""{
            finalFilterData.setValue(txtIngredient.text!, forKey: "ingredient")
        }
        else{
            finalFilterData.setValue("", forKey: "ingredient")
        }
        
        if txtCertificateHolder.text != ""{
            finalFilterData.setValue(txtCertificateHolder.text!, forKey: "certificateHolder")
        }
        else{
            finalFilterData.setValue("", forKey: "certificateHolder")
        }
        
        
        let arrtdft = finalFilterData
        
        var dcID:Int!
        if let dcIDData = finalFilterData.value(forKey: "dcIDData") as? NSArray{
            dcID = 0
            if dcIDData.count > 0{
                dcID = dcID + 1
            }
            else{
                dcID = 0
            }
        }
        else{
            dcID = 0
        }
        
        var ddID:Int!
        if let ddIDData = finalFilterData.value(forKey: "ddIDData") as? NSArray{
            ddID = 0
            if ddIDData.count > 0{
                ddID = ddID + 1
            }
            else{
                ddID = 0
            }
        }
        else{
            ddID = 0
        }
        
        var ingredientCount:Int!
        if let ingredient = finalFilterData.value(forKey: "ingredient") as? String{
            let ingri = "\(ingredient)"
            ingredientCount = 0
            if ingri != ""{
                ingredientCount = ingredientCount + 1
            }
            else{
                ingredientCount = 0
            }
        }
        else{
           ingredientCount = 0
        }
        
        var certificateHolderCount:Int!

        if let certificateHolder = finalFilterData.value(forKey: "certificateHolder") as? String{
            certificateHolderCount = 0
            let certiHolder = "\(certificateHolder)"
            if certiHolder != ""{
                certificateHolderCount = certificateHolderCount + 1
            }
            else{
                certificateHolderCount = 0
            }
        }
        else{
            certificateHolderCount = 0
        }

        
        
//        totalFilterType = dcID + ddID + ingredientCount + certificateHolderCount
        
        let isThe1 = dcID + ddID
        let isThe2 = ingredientCount + certificateHolderCount

        let totalFilterType:Int! = isThe1 + isThe2
        
        
        let strVal = "\(totalFilterType!)"
        finalFilterData.setValue(strVal, forKey: "totalFilterType")

        
        if diseaseTypeData.count == 0{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please select health concerns", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else{
            let notificationName = Notification.Name("filterDrugList")
            USERDEFAULT.setValue(finalFilterData, forKey: "filterDrugData")
            USERDEFAULT.synchronize()
            
           // NotificationCenter.default.post(name: notificationName, object: diseaseTypeData, userInfo: nil)
            NotificationCenter.default.post(name: notificationName, object: nil)
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForGetDiseaseTye(){
        let completeURL = NSString(format:"%@%@", MainURL,healthConcernURL) as String
        
       // let pageNumber = "\(pageNum!)"
       // "page":pageNumber,
       // "limit":PAGINATION_LIMITE
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as? String ?? "",
            "token": USERDEFAULT.value(forKey: "token") as? String ?? "",
            "lang_type":Language_Type
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetArticalCategory API Parameter :",finalParams)
        print("GetArticalCategory API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: healthConcernURLTag)
        
    }
    
    func postDataOnWebserviceForGetDrugCategory(){
        let completeURL = NSString(format:"%@%@", MainURL,dosesDataURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as? String ?? "",
            "token": USERDEFAULT.value(forKey: "token") as? String ?? "",
            "lang_type":Language_Type
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetDrugCategory API Parameter :",finalParams)
        print("GetDrugCategory API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: dosesDataURLTag)
        
    }

    
    
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case healthConcernURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("HealthConcern List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum == 1{
                    self.diseaseData = NSMutableArray()
                    self.diseaseGlobalData = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        self.diseaseGlobalData.add(myData[i])
                    }
                    
                    for i in 0...myData.count - 1 {
                        self.diseaseData.add(myData[i])
                    }
                    
                    boolArray = NSMutableArray.init()
                    categoryCount = 0
                    
                    for _ in 0...diseaseData.count - 1 {
                        boolArray.add("0")
                    }
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum > 0) {
                            self.pageNum = self.pageNum - 1
                        }
                        self.isLoading = false
                    }else{
                        self.isLoading = true
                    }
                }
                else{
                    self.isLoading = false
                    if (self.pageNum > 0) {
                        self.pageNum = self.pageNum - 1
                    }
                    
                    if self.diseaseData.count == 0{
                        self.tableViewFilterDrug.isHidden = true
                        //self.lblNoArtical.isHidden = false
                    }
                    else{
                        self.tableViewFilterDrug.isHidden = false
                        //self.lblNoArtical.isHidden = true
                    }
                }
                
                self.tableViewFilterDrug.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
                
                if USERDEFAULT.value(forKey: "filterDrugData") != nil{
                    print("Already FilterData :",USERDEFAULT.value(forKey: "filterDrugData") ?? "NOOO")
                    alreadyFilterData = USERDEFAULT.value(forKey: "filterDrugData") as! NSDictionary


                    let temp = NSMutableArray()

                    for i in 0..<self.diseaseData.count{
                        let id = (self.diseaseData[i] as AnyObject).value(forKey: "hc_id") as! String
                        temp.add("\(id)")
                    }
                    print(temp)

                   if let abc = alreadyFilterData.value(forKey: "ddIDData") as? NSArray
                   {
                        for i in 0..<abc.count{
                            let myIndexValue:Int = temp.index(of: "\(abc[i])")
                            if self.diseaseData.count > myIndexValue{
                                self.boolArray.replaceObject(at: myIndexValue, with: "1")
                                let path = NSIndexPath(row: myIndexValue, section: 0)
                                tableViewFilterDrug.reloadRows(at: [path as IndexPath], with:.none)
                            }
                        }
                    }
                    
                    if let catCount = alreadyFilterData.value(forKey: "catCount") as? Int{
                        categoryCount = catCount
                        
                        if categoryCount > 0 {
                            lblCategoryCount.text = "\(categoryCount!) Selected"
                        }
                        else{
                            lblCategoryCount.text = ""
                        }
                        finalFilterData.setValue(categoryCount, forKey: "catCount")

                    }
                    
                    diseaseTypeData = NSMutableArray.init()
                    
                    for i in 0...diseaseData.count - 1 {
                        if boolArray.object(at: i) as! String == "1"{
                            diseaseTypeData.add((self.diseaseData[i] as AnyObject).value(forKey: "hc_id") as! String)
                        }
                    }
                    print("diseaseTypeData ID",diseaseTypeData)
                    finalFilterData.setValue(diseaseTypeData, forKey: "ddIDData")

                    
                    if let ingredient = alreadyFilterData.value(forKey: "ingredient") as? String
                    {
                        let ingri = "\(ingredient)"
                        txtIngredient.text = ingri
                        finalFilterData.setValue(ingri, forKey: "ingredient")
                    }
                    
                    if let certificateHolder = alreadyFilterData.value(forKey: "certificateHolder") as? String
                    {
                        let CertiHolder = "\(certificateHolder)"
                        txtCertificateHolder.text = CertiHolder
                        finalFilterData.setValue(CertiHolder, forKey: "certificateHolder")
                    }


                    
                }
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            tableViewFilterDrug.reloadData()
            self.setupScrollForMainScrollView()
            break
            
        case dosesDataURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("Doses Data Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                drugCategoryData = NSMutableArray(array: resultDict.value(forKey: "data") as! NSArray)
                drugCategoryGlobalData = NSMutableArray(array: resultDict.value(forKey: "data") as! NSArray)
                
                boolArrayForCategory = NSMutableArray.init()
                for _ in 0...drugCategoryData.count - 1 {
                    boolArrayForCategory.add("0")
                }
                
                zeroTempDataArray = NSMutableArray.init()
                for _ in 0...drugCategoryData.count - 1 {
                    zeroTempDataArray.add("0")
                }
                
                categoryDoseCount = 0

                
                
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
//            collectionViewFilterDrug.reloadData()
//            tableViewDosage.reloadData()
            self.setupScrollForMainScrollView()

            
            if USERDEFAULT.value(forKey: "filterDrugData") != nil{
                print("Already FilterData :",USERDEFAULT.value(forKey: "filterDrugData") ?? "NOOO")
                alreadyFilterData = USERDEFAULT.value(forKey: "filterDrugData") as! NSDictionary
                
                
                let temp = NSMutableArray()
                
                for i in 0..<self.drugCategoryData.count{
                    let id = (self.drugCategoryData[i] as AnyObject).value(forKey: "df_id") as! String
                    temp.add("\(id)")
                }
                print(temp)
                
                if let abc = alreadyFilterData.value(forKey: "dcIDData") as? NSArray
                {
                    print(abc)
                    for i in 0..<abc.count{
                        let myIndexValue:Int = temp.index(of: "\(abc[i])")
                        if self.drugCategoryData.count > myIndexValue{
                            self.boolArrayForCategory.replaceObject(at: myIndexValue, with: "1")
                            let path = NSIndexPath(row: myIndexValue, section: 0)
                            //collectionViewFilterDrug.reloadItems(at: [path as IndexPath])
                            tableViewDosage.reloadRows(at: [path as IndexPath], with: .none)
                            
                        }
                        
                    }
                    
                }
                
                if let catCount = alreadyFilterData.value(forKey: "catDoseCount") as? Int{
                    categoryDoseCount = catCount
                    
                    if categoryDoseCount > 0 {
                        lblDoseCategoryCount.text = "\(categoryDoseCount!) Selected"
                    }
                    else{
                        lblDoseCategoryCount.text = ""
                    }
                    finalFilterData.setValue(categoryDoseCount, forKey: "catCount")
                    
                }

                
                
                
                diseaseCategoryData = NSMutableArray.init()
                
                if drugCategoryData.count > 0{
                    for i in 0...drugCategoryData.count - 1 {
                        if boolArrayForCategory.object(at: i) as! String == "1"{
                            diseaseCategoryData.add((self.drugCategoryData[i] as AnyObject).value(forKey: "df_id") as! String)
                        }
                    }
                }
                
                print("diseaseCategoryData ID",diseaseCategoryData)
                finalFilterData.setValue(diseaseCategoryData, forKey: "dcIDData")

                
                if let ingredient = alreadyFilterData.value(forKey: "ingredient") as? String
                {
                    let ingri = "\(ingredient)"
                    txtIngredient.text = ingri
                    finalFilterData.setValue(ingri, forKey: "ingredient")
                }
                
                if let certificateHolder = alreadyFilterData.value(forKey: "certificateHolder") as? String
                {
                    let CertiHolder = "\(certificateHolder)"
                    txtCertificateHolder.text = CertiHolder
                    finalFilterData.setValue(CertiHolder, forKey: "certificateHolder")
                }
                
            }
            
            break

            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case healthConcernURLTag:
            self.isLoading = false
            if (self.pageNum > 0) {
                self.pageNum = self.pageNum - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
        case dosesDataURLTag:
            
            break
    
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    // TODO: - SET FRAME METHOD
    func setupScrollForMainScrollView(){
//        self.viewMedicineForm.frame = CGRect(x: self.viewMedicineForm.frame.origin.x, y: self.viewMedicineForm.frame.origin.y, width: self.viewMedicineForm.frame.size.width, height: self.viewMedicineForm.frame.size.height)
//        self.tableViewDosage.frame = CGRect(x: self.tableViewDosage.frame.origin.x, y: self.viewMedicineForm.frame.origin.y + self.viewMedicineForm.frame.size.height , width: self.viewMedicineForm.frame.size.width, height: self.tableViewDosage.contentSize.height)
        
        tableHeight.constant = tableViewFilterDrug.contentSize.height
         
        self.viewDiesesType.frame = CGRect(x: self.viewDiesesType.frame.origin.x, y: self.viewDiesesType.frame.origin.y, width: self.viewDiesesType.frame.size.width, height: self.viewDiesesType.frame.size.height)

        tableViewFilterDrug.frame = CGRect(x: tableViewFilterDrug.frame.origin.x, y: tableViewFilterDrug.frame.origin.y, width: tableViewFilterDrug.frame.size.width, height: tableHeight.constant)

        self.viewIngredient.frame = CGRect(x: self.viewIngredient.frame.origin.x, y: self.tableViewFilterDrug.frame.origin.y + self.tableViewFilterDrug.frame.size.height , width: self.viewIngredient.frame.size.width, height: self.viewIngredient.frame.size.height)

        self.txtIngredient.frame = CGRect(x: self.txtIngredient.frame.origin.x, y: self.viewIngredient.frame.origin.y + self.viewIngredient.frame.size.height , width: self.txtIngredient.frame.size.width, height: self.txtIngredient.frame.size.height)

        self.viewCertificateHolder.frame = CGRect(x: self.viewCertificateHolder.frame.origin.x, y: self.txtIngredient.frame.origin.y + self.txtIngredient.frame.size.height , width: self.viewCertificateHolder.frame.size.width, height: self.viewCertificateHolder.frame.size.height)

        self.txtCertificateHolder.frame = CGRect(x: self.txtCertificateHolder.frame.origin.x, y: self.viewCertificateHolder.frame.origin.y + self.viewCertificateHolder.frame.size.height , width: self.txtCertificateHolder.frame.size.width, height: self.txtCertificateHolder.frame.size.height)

        self.scrMain.contentSize = CGSize(width: self.scrMain.frame.size.width, height:self.txtCertificateHolder.frame.origin.y+self.txtCertificateHolder.frame.size.height + 20)
        
    }

    //MARK:- Notification Methods
    
    func keyboardDidShow(notification:NSNotification)
    {
        print("keyboard did show \(notification)")
        
        guard let dic = notification.userInfo else
        {
            print("user info is nil")
            return
        }
        
        let kbRect:CGRect = dic[UIKeyboardFrameEndUserInfoKey] as! CGRect
        
        let contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbRect.size.height, 0.0)
        scrMain.contentInset = contentInsets
        scrMain.scrollIndicatorInsets = contentInsets
        
        var aRect = self.view.frame
        aRect.size.height -= kbRect.size.height
        
        if activeTextField != nil
        {
            if (!aRect.contains(activeTextField!.frame.origin))
            {
                scrMain.scrollRectToVisible((activeTextField!.frame), animated: true)
            }
        }
    }
    
    func keyboardWillBeHidden(notification:NSNotification)
    {
        print("keyboardWillBeHidden \(notification) )")
        
        let contentInsets = UIEdgeInsets.zero
        scrMain.contentInset = contentInsets
        scrMain.scrollIndicatorInsets = contentInsets
    }

    
    
}
