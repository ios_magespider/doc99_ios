//
//  DrugGridTableVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 15/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class DrugGridTableVC: UITableViewCell {

    @IBOutlet var lblDrugName:UILabel!
    @IBOutlet var lblMedicalName:UILabel!
    @IBOutlet var lblSize:UILabel!
    @IBOutlet var imageViewDrug:UIImageView!
    @IBOutlet var activityIndicatorForDrugImage:UIActivityIndicatorView!
    @IBOutlet var viewLeft:UIView!
    @IBOutlet var btnLeftSelect:UIButton!

    
    @IBOutlet var lblDrugName1:UILabel!
    @IBOutlet var lblMedicalName1:UILabel!
    @IBOutlet var lblSize1:UILabel!
    @IBOutlet var imageViewDrug1:UIImageView!
    @IBOutlet var activityIndicatorForDrugImage1:UIActivityIndicatorView!
    @IBOutlet var viewRight:UIView!
    @IBOutlet var btnRightSelect:UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
