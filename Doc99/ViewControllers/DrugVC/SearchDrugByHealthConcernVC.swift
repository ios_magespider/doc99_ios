//
//  SearchDrugByHealthConcernVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 08/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SDWebImage

class SearchDrugByHealthConcernVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate {
    
    @IBOutlet var tableViewDrugList:UITableView!
    
    @IBOutlet var txtSearch:UITextField!
    
    var refreshControl: UIRefreshControl!

    //For Pasignation
    var pageNum:Int!
    var isLoading:Bool?
    
    var drugData = NSMutableArray()
    var drugGlobalData = NSMutableArray()
    
    @IBOutlet var lblNoDrug:UILabel!
    
    var diseaseTypeData = NSMutableArray()
    var finalFilterData = NSMutableDictionary()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // TODO: - OTHER METHODS
    
    func generalViewControllerSetting(){
        
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.drugData = NSMutableArray()
        self.drugGlobalData = NSMutableArray()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetHealthConcerns), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        self.addTapGestureInOurView()
        self.setLocalizationText()
        self.addPullRefreshDrugConcern()
    }
    
    func addPullRefreshDrugConcern(){
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.gray
        refreshControl.addTarget(self, action: #selector(self.refreshDrugConcern(sender:)), for: .valueChanged)
        tableViewDrugList.addSubview(refreshControl)
    }
    
    func refreshDrugConcern(sender:AnyObject) {
        // Code to refresh table view
       
        
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.drugData = NSMutableArray()
        self.drugGlobalData = NSMutableArray()
        self.tableViewDrugList.reloadData()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetHealthConcerns), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        refreshControl.endRefreshing()
    }

    
    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        if viewTouched!.isKind(of: UIButton.self) && viewTouched!.isKind(of:UITextView.self){
            
        }
        else{
            self.view.endEditing(true)
        }
        
    }
    
    
    
    func addLoadingIndicatiorOnFooterOnTableView(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableViewDrugList.tableFooterView = spinner
    }
    func removeLoadingIndicatiorOnFooterOnTableView(){
        tableViewDrugList.tableFooterView = nil
    }
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }
    
    func setLocalizationText(){
        lblNoDrug.text = NSLocalizedString("nodrug", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
    }
    
    // TODO: - Textfield DELEGATE METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        return false
    }
    
    
    // TODO: - DELEGATE ScrollView
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
//        if scrollView == tableViewDrugList {
//            if isLoading == true{
//                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
//                    pageNum = pageNum + 1
//                    print(pageNum)
//                    isLoading = false
//                    
//                    if Reachability.isConnectedToNetwork() == true {
//                        self.addLoadingIndicatiorOnFooterOnTableView()
//                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetHealthConcerns), userInfo: nil, repeats: false)
//                    } else {
//                        showAlert("Check Connection", title: "Internet is not available.")
//                    }
//                }
//            }
//            
//        }
        
        
    }
    
    
    // TODO: - DELEGATE METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.drugData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let identifier = "healthCategory"
        var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? HealthCategoryTableVC
        
        if cell == nil {
            let nib  = Bundle.main.loadNibNamed("HealthCategoryTableVC", owner: self, options: nil)
            cell = nib?[0] as? HealthCategoryTableVC
        }
        cell!.selectionStyle = .none;
        
        
        if let articalCategoryName =  (self.drugData[indexPath.row] as AnyObject).value(forKey: "hc_name") as? String
        {
            cell?.lblTitle.text = "\(articalCategoryName)"
        }
        
        if let articalCategoryName =  (self.drugData[indexPath.row] as AnyObject).value(forKey: "bodyorgan") as? String
        {
            cell?.lblSubTitle.text = "\(articalCategoryName)"
        }
        
        

            cell?.imageViewSelect.isHidden = true
        
        
        if ((self.drugData.object(at: indexPath.row) as AnyObject).value(forKey: "image") as? String == "")
        {
            cell?.imageViewHealthCategory.image = UIImage.init(named: "image_placeholder.png")
            cell?.activityIndicatorForHealth.isHidden = true
            
        }
        else{
            let imageUrl = (self.drugData.object(at: indexPath.row) as AnyObject).value(forKey: "image") as? String
            
            let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl!) as String
            let url : NSString = fullUrl as NSString
            let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
            let searchURL : NSURL = NSURL(string: urlStr as String)!
            
            cell?.activityIndicatorForHealth.isHidden = false
            cell?.activityIndicatorForHealth.startAnimating()
            
            
            cell?.imageViewHealthCategory.sd_setImage(with: searchURL as URL, completed: { (image:UIImage?, error:Error?, cacheType:SDImageCacheType!, imageURL:URL?) in
                
                if ((error) != nil) {
                    cell?.imageViewHealthCategory.image = UIImage.init(named: "image_placeholder.png")
                }
                cell?.activityIndicatorForHealth.isHidden = true
            })
            
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let id =  (self.drugData[indexPath.row] as AnyObject).value(forKey: "hc_id") as? String
        {
            let drugConcernID = "\(id)"
//            USERDEFAULT.setValue(drugConcernID, forKey: "searchConcernID")
//            USERDEFAULT.synchronize()
//            let notificationName = Notification.Name("searchDataNotification")
//            NotificationCenter.default.post(name: notificationName, object: nil)
//            self.navigationController?.popViewController(animated: true)
            
            diseaseTypeData = NSMutableArray.init()
            diseaseTypeData.add(drugConcernID)
            print("diseaseTypeData ID",diseaseTypeData)
            
            finalFilterData.setValue(diseaseTypeData, forKey: "ddIDData")
            finalFilterData.setValue("1", forKey: "catCount")

            let notificationName = Notification.Name("filterDrugList")
            USERDEFAULT.setValue(finalFilterData, forKey: "filterDrugData")
            USERDEFAULT.synchronize()
            
            // NotificationCenter.default.post(name: notificationName, object: diseaseTypeData, userInfo: nil)
            NotificationCenter.default.post(name: notificationName, object: nil)
            _ = self.navigationController?.popViewController(animated: true)
            
            

        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    // TODO: - ACTION METHODS
    @IBAction func btnBackClicked(_ sender:UIButton){
        _ =  self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchFilter(){
        var predicate = String()
        
        
        if (txtSearch.text == "") {
            drugData = drugGlobalData.mutableCopy() as! NSMutableArray
        }else{
            predicate = String(format: "SELF['hc_name'] contains[c] '%@'", txtSearch.text!)
            print(predicate)
            
            let myPredicate:NSPredicate = NSPredicate(format:predicate)
            let temp = self.drugGlobalData.mutableCopy() as! NSArray
            self.drugData = (temp.filtered(using: myPredicate) as NSArray).mutableCopy() as! NSMutableArray
            
        }
        
        if self.drugData.count == 0 {
            lblNoDrug.isHidden = false
            tableViewDrugList.isHidden = true
        }
        else{
            tableViewDrugList.isHidden = false
            lblNoDrug.isHidden = true
        }
        tableViewDrugList.reloadData()
        
        
    }
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForGetHealthConcerns(){
        
        let completeURL = NSString(format:"%@%@", MainURL,healthConcernURL) as String
        
        // let pageNumber = "\(pageNum!)"
        // "page":pageNumber,
        // "limit":PAGINATION_LIMITE
        
        let params:NSDictionary = [
            // "user_id" : "49",
            // "token": "ICp48GEVMOBwzHOUu1KJN2xJ2PxsmCSa1jDfTGpERXP6s7pbFH2ZpPvg4PpuJSWX5uCYy701DfKNHHeacneTpCMxU3C0JEvlT5CFjVqbv63FXjlOUZoQ6jsW",
            "lang_type":Language_Type
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetArticalCategory API Parameter :",finalParams)
        print("GetArticalCategory API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: healthConcernURLTag)
        
        
        
    }
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case healthConcernURLTag:
            let resultDict = responseObject as! NSDictionary;
//            print("getDrugDiseaseURLTag List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum == 1{
                    self.drugData = NSMutableArray()
                    self.drugGlobalData = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        self.drugGlobalData.add(myData[i])
                    }
                    
                    for i in 0...myData.count - 1 {
                        self.drugData.add(myData[i])
                    }
                    
                    
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum > 0) {
                            self.pageNum = self.pageNum - 1
                        }
                        self.isLoading = false
                    }else{
                        self.isLoading = true
                    }
                }
                else{
                    self.isLoading = false
                    if (self.pageNum > 0) {
                        self.pageNum = self.pageNum - 1
                    }
                    
                    if self.drugData.count == 0{
                        self.tableViewDrugList.isHidden = true
                        //self.lblNoArtical.isHidden = false
                    }
                    else{
                        self.tableViewDrugList.isHidden = false
                        //self.lblNoArtical.isHidden = true
                    }
                }
                self.tableViewDrugList.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case healthConcernURLTag:
            self.isLoading = false
            if (self.pageNum > 0) {
                self.pageNum = self.pageNum - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    
    
}
