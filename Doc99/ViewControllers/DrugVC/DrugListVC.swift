//
//  DrugListVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 13/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD

class DrugListVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIAlertViewDelegate {
    
    @IBOutlet var tableViewDrugList:UITableView!
    @IBOutlet var tableViewDrugGrid:UITableView!
    
    @IBOutlet var imageViewListGrid:UIImageView!
    @IBOutlet var lblListGrid:UILabel!
    
    var isList:Bool?

    //For Pasignation
    var pageNum:Int!
    var isLoading:Bool?
    
    var drugData = NSMutableArray()
    var drugGlobalData = NSMutableArray()
    
    @IBOutlet var lblNoDrug:UILabel!
    
    var isFilterData:Bool?

    var filterData = NSDictionary()

    @IBOutlet var viewSearchDrug:UIView!
    @IBOutlet var txtSearch:UITextField!
    @IBOutlet var headerView:UIView!

    
    
    @IBOutlet var viewPicker:UIView!
    @IBOutlet var pickerView:UIPickerView!
    var pickerSelIndx:Int!
    var pickOption:NSMutableArray!
    var pickerSelectedValue:String!

    @IBOutlet var lblFilterCount:UILabel!
    @IBOutlet var lblFilterBy:UILabel!
    @IBOutlet var imageViewFilter:UIImageView!

    @IBOutlet var lblDugHeader:UILabel!
    @IBOutlet var lblSortBy:UILabel!
    @IBOutlet var btnCancel:UIButton!
    @IBOutlet var btnDone:UIButton!
    
    @IBOutlet var loginView:UIView!

    var healthConcernData = NSArray()

    
    //HealthConcernView
    @IBOutlet var lblHealthConcernHeader:UILabel!
    @IBOutlet var lblNext:UILabel!

    
    // Health Concern
    //For Pasignation
    var pageNum1:Int!
    var isLoading1:Bool?
    var healthData = NSMutableArray()
    var healthGlobalData = NSMutableArray()
    var boolArray:NSMutableArray!
    var healthCategory = NSMutableArray()
    @IBOutlet var tableViewHealthCategory:UITableView!
    @IBOutlet var viewHealthConcern:UIView!
    var finalFilterData = NSMutableDictionary()
    var categoryCount:Int!
    var alreadyFilterData = NSDictionary()

    var refreshControl: UIRefreshControl!
    var refreshControl1: UIRefreshControl!

    
    //LOGIN POPUP
    @IBOutlet var loginPopupLabel2:UILabel!
    @IBOutlet var loginPopupLabel3:UILabel!
    
    
    
    var isSortData:Bool?

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //if USERDEFAULT.value(forKey: "userID") != nil{
            self.generalViewControllerSetting()
        //}
        
        // Define identifier
        let notificationName1 = Notification.Name("filterDrugList")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(filterDrugDataCalled(notification:)), name: notificationName1, object: nil)
        

        // Define identifier
        let notificationName5 = Notification.Name("updateHealthConcernDataNotification")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateHealthConcernData), name: notificationName5, object: nil)

        // Define identifier
        let notificationName6 = Notification.Name("searchDataNotification")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.searchDrugDataNotification), name: notificationName6, object: nil)
        
        
        // Define identifier
        let notificationName7 = Notification.Name("updateAfterMemberShip")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateAfterMembershipCompleted), name: notificationName7, object: nil)
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = false
        
        self.viewSearchDrug.frame = CGRect(x: self.headerView.frame.origin.x+self.headerView.frame.size.width, y: self.headerView.frame.origin.y, width: self.viewSearchDrug.frame.size.width, height: self.viewSearchDrug.frame.size.height)

        pickerSelIndx=0
        pickOption = NSMutableArray()
        
        pickOption = ["Ascending By Name","Descending By Name"]
        
        viewPicker.frame = CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: viewPicker.frame.size.width, height: viewPicker.frame.size.height)

        if USERDEFAULT.value(forKey: "userID") != nil{  //comment removed by hitesh.
            loginView.removeFromSuperview()
            if let isMember = USERDEFAULT.value(forKey: "isMember") as? String{
                print("isMember as string--> \(isMember)")
                
                let ismem = "\(isMember)"
                if ismem == "1"{
                    if let firstTIme = USERDEFAULT.value(forKey: "isDrugSearch") as? String{
                        let first = "\(firstTIme)"
                        if first == "NO"{
                            viewHealthConcern.removeFromSuperview()
                        }
                        else{
                            viewHealthConcern.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
                            self.view.addSubview(viewHealthConcern)
                        }
                    }
                    else{
                        viewHealthConcern.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
                        self.view.addSubview(viewHealthConcern)
                    }
                }
                else{
                    
                    //removed from last change
//                    loginPopupLabel3.text = "MEMBERSHIP"
//                    self.view.addSubview(loginView)
                    //--------
                    
                    //Hitesh
                    if let firstTIme = USERDEFAULT.value(forKey: "isDrugSearch") as? String{
                        let first = "\(firstTIme)"
                        if first == "NO"{
                            viewHealthConcern.removeFromSuperview()
                        }
                        else{
                            viewHealthConcern.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
                            self.view.addSubview(viewHealthConcern)
                        }
                    }
                    else{
                        viewHealthConcern.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
                        self.view.addSubview(viewHealthConcern)
                    }
                }
            }
            else if let isMember = USERDEFAULT.value(forKey: "isMember") as? NSNumber{
                print("isMember as number--> \(isMember)")
                
                let ismem = "\(isMember)"
                if ismem == "1"{
                    if let firstTIme = USERDEFAULT.value(forKey: "isDrugSearch") as? String{
                        let first = "\(firstTIme)"
                        if first == "NO"{
                            viewHealthConcern.removeFromSuperview()
                        }
                        else{
                            viewHealthConcern.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
                            self.view.addSubview(viewHealthConcern)
                        }
                    }
                    else{
                        viewHealthConcern.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
                        self.view.addSubview(viewHealthConcern)
                    }
                    
                }
                else{
                    //added by Hitesh
                    if let firstTIme = USERDEFAULT.value(forKey: "isDrugSearch") as? String{
                        let first = "\(firstTIme)"
                        if first == "NO"{
                            viewHealthConcern.removeFromSuperview()
                        }
                        else{
                            viewHealthConcern.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
                            self.view.addSubview(viewHealthConcern)
                        }
                    }
                    else{
                        viewHealthConcern.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
                        self.view.addSubview(viewHealthConcern)
                    }
                }
            }
            else{
                if let firstTIme = USERDEFAULT.value(forKey: "isDrugSearch") as? String{
                    let first = "\(firstTIme)"
                    if first == "NO"{
                        viewHealthConcern.removeFromSuperview()
                    }
                    else{
                        viewHealthConcern.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
                        self.view.addSubview(viewHealthConcern)
                    }
                }
                else{
                    viewHealthConcern.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
                    self.view.addSubview(viewHealthConcern)
                }
            }
        
            
        //else part removed from comment by hitesh.
            
        }
        else{
            loginView.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
            loginPopupLabel3.text = "LOGIN"
            self.view.addSubview(loginView)
        }
        
        isSortData = false

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // TODO: - OTHER METHODS
    
    func updateAfterMembershipCompleted() {
        self.generalViewControllerSetting()
        loginView.removeFromSuperview()
    }
    
    func generalViewControllerSetting(){
        
        //headerView.addSubview(viewSearchDrug)
        
        lblFilterCount.layer.cornerRadius = lblFilterCount.frame.size.width / 2
        lblFilterCount.clipsToBounds = true

        if USERDEFAULT.value(forKey: "filterDrugData") != nil{
            
            print("FilterData :",USERDEFAULT.value(forKey: "filterDrugData") ?? "NOOO")
            filterData = USERDEFAULT.value(forKey: "filterDrugData") as! NSDictionary
            
            if let totalFilterType = filterData.value(forKey: "totalFilterType") as? String{
                let totFilter = "\(totalFilterType)"
                print("totFilterType :",totFilter)
                lblFilterCount.isHidden = false
                lblFilterCount.text = totFilter
            }
            else{
                lblFilterCount.isHidden = true
                lblFilterCount.text = ""
            }
            
            lblFilterBy.textColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
            imageViewFilter.image = UIImage.init(named: "filter_blue.png")
            isFilterData = true
        }
        else{
            lblFilterCount.isHidden = true
            lblFilterCount.text = ""
            
            lblFilterBy.textColor = UIColor.init(red: 108.0/255.0, green: 110.0/255.0, blue: 112.0/255.0, alpha: 1.0)
            imageViewFilter.image = UIImage.init(named: "filter_gray.png")
            isFilterData = false
        }
        
        
//        isFilterData = false
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.drugData = NSMutableArray()
        self.drugGlobalData = NSMutableArray()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            self.addLoadingIndicatiorOnFooterOnTableViewGrid()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetDrugList), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        self.addTapGestureInOurView()
        self.setLocalizationText()
        
        
        // Health Concern
        pageNum1=1;
        let indicator2 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator2.startAnimating()
        
        self.healthData = NSMutableArray()
        self.healthGlobalData = NSMutableArray()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableViewHealth()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetHealthConcerns), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        if Reachability.isConnectedToNetwork() == true {
            self.performSelector(inBackground: #selector(self.postDataOnWebserviceForGetProfile), with: nil)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        
        
        self.addPullRefreshHealthConcern()
        //self.addPullRefreshDrugGrid()
        self.addPullRefreshDrugList()
        self.setiPhone4sDeviceSupport()
        
    }
    
    func setiPhone4sDeviceSupport() {
        if DeviceType.IS_IPHONE_4_OR_LESS{
            viewHealthConcern.frame = CGRect(x: viewHealthConcern.frame.origin.x, y: viewHealthConcern.frame.origin.y, width: viewHealthConcern.frame.size.width, height: viewHealthConcern.frame.size.height - 88 )
        }

    }
    
    
    func searchDrugDataNotification() {
        
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.drugData = NSMutableArray()
        self.drugGlobalData = NSMutableArray()
        self.tableViewDrugList.reloadData()
        self.tableViewDrugGrid.reloadData()
        
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            self.addLoadingIndicatiorOnFooterOnTableViewGrid()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetDrugListForSearch), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        self.addTapGestureInOurView()
        self.setLocalizationText()
        
    }
    
    
    func addPullRefreshHealthConcern(){
        refreshControl1 = UIRefreshControl()
        refreshControl1.backgroundColor = UIColor.clear
        refreshControl1.tintColor = UIColor.gray
        refreshControl1.addTarget(self, action: #selector(self.refreshHealthConcern(sender:)), for: .valueChanged)
        tableViewHealthCategory.addSubview(refreshControl1)
    }
    
    func addPullRefreshDrugList(){
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.gray
        refreshControl.addTarget(self, action: #selector(self.refreshDrug(sender:)), for: .valueChanged)
        tableViewDrugList.addSubview(refreshControl)
    }

    func addPullRefreshDrugGrid(){
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.gray
        refreshControl.addTarget(self, action: #selector(self.refreshDrug(sender:)), for: .valueChanged)
        tableViewDrugGrid.addSubview(refreshControl)
    }
    

    
    
    func refreshHealthConcern(sender:AnyObject) {
        // Code to refresh table view
      
        // Health Concern
        pageNum1=1;
        let indicator2 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator2.startAnimating()
        
        self.healthData = NSMutableArray()
        self.healthGlobalData = NSMutableArray()
        self.tableViewHealthCategory.reloadData()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableViewHealth()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetHealthConcerns), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        refreshControl1.endRefreshing()
        if Reachability.isConnectedToNetwork() == true {
            self.performSelector(inBackground: #selector(self.postDataOnWebserviceForGetProfile), with: nil)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
    }
    
    func refreshDrug(sender:AnyObject) {
        // Code to refresh table view
        
        if USERDEFAULT.value(forKey: "filterDrugData") != nil{
            
            print("FilterData :",USERDEFAULT.value(forKey: "filterDrugData") ?? "NOOO")
            filterData = USERDEFAULT.value(forKey: "filterDrugData") as! NSDictionary
            
            
            if let totalFilterType = filterData.value(forKey: "totalFilterType") as? String{
                let totFilter = "\(totalFilterType)"
                print("totFilterType :",totFilter)
                lblFilterCount.isHidden = false
                lblFilterCount.text = totFilter
            }
            else{
                lblFilterCount.isHidden = true
                lblFilterCount.text = ""
            }
            
            
            lblFilterBy.textColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
            imageViewFilter.image = UIImage.init(named: "filter_blue.png")
            isFilterData = true
        }
        else{
            lblFilterCount.isHidden = true
            lblFilterCount.text = ""
            
            lblFilterBy.textColor = UIColor.init(red: 108.0/255.0, green: 110.0/255.0, blue: 112.0/255.0, alpha: 1.0)
            imageViewFilter.image = UIImage.init(named: "filter_gray.png")
            
            
            isFilterData = false
        }
        
        
        
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.drugData = NSMutableArray()
        self.drugGlobalData = NSMutableArray()
        self.tableViewDrugList.reloadData()
        self.tableViewDrugGrid.reloadData()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            self.addLoadingIndicatiorOnFooterOnTableViewGrid()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetDrugList), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        refreshControl.endRefreshing()
    }
    
    
    
    
    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        
        if viewTouched!.isKind(of: UIButton.self) && viewTouched!.isKind(of:UITextView.self){
            
        }
        else{
            self.view.endEditing(true)
            //            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
            
            
            if (viewTouched!.superview!.superview == viewPicker) {
            }else if(viewTouched!.superview == viewPicker){
            }else if(viewTouched == viewPicker){
            }else{
                UIView.animate(withDuration: 0.3) {
                    
                    self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
                    
                    
                    
                }
                
            }
            
            
            
            
        }
        
        
        
    }

    
    
    func addLoadingIndicatiorOnFooterOnTableView(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableViewDrugList.tableFooterView = spinner
    }
    func removeLoadingIndicatiorOnFooterOnTableView(){
        tableViewDrugList.tableFooterView = nil
    }
    
    func addLoadingIndicatiorOnFooterOnTableViewGrid(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableViewDrugGrid.tableFooterView = spinner
    }
    func removeLoadingIndicatiorOnFooterOnTableViewGrid(){
        tableViewDrugGrid.tableFooterView = nil
    }

    func addLoadingIndicatiorOnFooterOnTableViewHealth(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableViewHealthCategory.tableFooterView = spinner
    }
    func removeLoadingIndicatiorOnFooterOnTableViewHealth(){
        tableViewHealthCategory.tableFooterView = nil
    }

    
    func filterDrugDataCalled(notification: NSNotification) {
        
        
        if USERDEFAULT.value(forKey: "filterDrugData") != nil{
            
            print("FilterData :",USERDEFAULT.value(forKey: "filterDrugData") ?? "NOOO")
            filterData = USERDEFAULT.value(forKey: "filterDrugData") as! NSDictionary
            
//            if let DeseaseCatData = filterData.value(forKey: "dcIDData") as? NSArray{
//                let atrticalCatData1 = DeseaseCatData
//                print("atrtical Category Data :",atrticalCatData1)
//                lblFilterCount.isHidden = false
//                lblFilterCount.text = "1"
//                if let DeseaseCatData = filterData.value(forKey: "ddIDData") as? NSArray{
//                    let atrticalCatData1 = DeseaseCatData
//                    print("atrtical Category Data :",atrticalCatData1)
//                    lblFilterCount.isHidden = false
//                    lblFilterCount.text = "2"
//                }
//                if let ingredient = filterData.value(forKey: "ingredient") as? String{
//                    let ingredient = "\(ingredient)"
//                    print("Ingredient :",ingredient)
//
//                    lblFilterCount.isHidden = false
//                    lblFilterCount.text = "3"
//                    
//                }
//                if let certificateHolder = filterData.value(forKey: "certificateHolder") as? String{
//                    let certificateHolder = "\(certificateHolder)"
//                    print("certificateHolder :",certificateHolder)
//                    lblFilterCount.isHidden = false
//                    lblFilterCount.text = "4"
//                }
//                else{
//                    lblFilterCount.isHidden = false
//                    lblFilterCount.text = "1"
//                }
//                
//            }
//            else if let DDID = filterData.value(forKey: "ddIDData") as? NSArray{
//                    let atrticalCatData1 = DDID
//                    print("atrtical Category Data :",atrticalCatData1)
//                    lblFilterCount.isHidden = false
//                    lblFilterCount.text = "1"
//            }
//            else if let ingredient = filterData.value(forKey: "ingredient") as? String{
//                let ingredient = "\(ingredient)"
//                print("Ingredient :",ingredient)
//               
//                lblFilterCount.isHidden = false
//                lblFilterCount.text = "1"
//
//            }
//            else if let certificateHolder = filterData.value(forKey: "certificateHolder") as? String{
//                let certificateHolder = "\(certificateHolder)"
//                print("certificateHolder :",certificateHolder)
//                lblFilterCount.isHidden = false
//                lblFilterCount.text = "1"
//            }
//            else{
//                lblFilterCount.isHidden = true
//                lblFilterCount.text = ""
//            }
            
            
            if let totalFilterType = filterData.value(forKey: "totalFilterType") as? String{
                let totFilter = "\(totalFilterType)"
                print("totFilterType :",totFilter)
                lblFilterCount.isHidden = false
                lblFilterCount.text = totFilter
            }
            else{
                lblFilterCount.isHidden = true
                lblFilterCount.text = ""
            }

            
            
            
            lblFilterBy.textColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
            imageViewFilter.image = UIImage.init(named: "filter_blue.png")
            isFilterData = true
        }
        else{
            lblFilterCount.isHidden = true
            lblFilterCount.text = ""
            
            lblFilterBy.textColor = UIColor.init(red: 108.0/255.0, green: 110.0/255.0, blue: 112.0/255.0, alpha: 1.0)
            imageViewFilter.image = UIImage.init(named: "filter_gray.png")

            
            isFilterData = false
        }
        
            self.drugData = NSMutableArray()
            self.drugGlobalData = NSMutableArray()
            self.tableViewDrugList.isHidden = false

            self.tableViewDrugList.reloadData()
            self.tableViewDrugGrid.reloadData()

        
            pageNum=1;
            let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            indicator1.startAnimating()
        
            if Reachability.isConnectedToNetwork() == true {
                self.addLoadingIndicatiorOnFooterOnTableView()
                self.addLoadingIndicatiorOnFooterOnTableViewGrid()

                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetDrugList), userInfo: nil, repeats: false)
            } else {
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
            
            
       
        
        
    }
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }

    func setLocalizationText(){
        
        lblDugHeader.text = NSLocalizedString("drugs", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblListGrid.text = NSLocalizedString("grid", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblSortBy.text = NSLocalizedString("sortby", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblFilterBy.text = NSLocalizedString("filterby", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblNoDrug.text = NSLocalizedString("nodrug", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
        btnCancel.setTitle(NSLocalizedString("cancel", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        btnDone.setTitle(NSLocalizedString("done", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        
        loginPopupLabel2.text = NSLocalizedString("Just enrolled to Doc99 app to experience it amazing features.", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        loginPopupLabel3.text = NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
        
        lblHealthConcernHeader.text = NSLocalizedString("Select minimum 1 categories of health concerns", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblNext.text = NSLocalizedString("NEXT", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

        
    }
    func updateHealthConcernData(){
       
        pageNum1=1;
        let indicator2 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator2.startAnimating()
        
        self.healthData = NSMutableArray()
        self.healthGlobalData = NSMutableArray()
        self.tableViewHealthCategory.reloadData()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableViewHealth()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetHealthConcerns), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        refreshControl1.endRefreshing()
        if Reachability.isConnectedToNetwork() == true {
            self.performSelector(inBackground: #selector(self.postDataOnWebserviceForGetProfile), with: nil)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }

        
        
    }
    
    
    // TODO: - DELEGATE ScrollView
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
//        print("Offset Y",scrollView.contentOffset.y)
//        print("scrollView Height",scrollView.frame.size.height)
//        print("scrollView Content Size Height",scrollView.contentSize.height)
        
        if scrollView == tableViewDrugList {
            if isLoading == true{
                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                    pageNum = pageNum + 1
                    print(pageNum)
                    isLoading = false
                    
                    if Reachability.isConnectedToNetwork() == true {
                        self.addLoadingIndicatiorOnFooterOnTableView()
                        self.addLoadingIndicatiorOnFooterOnTableViewGrid()

                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetDrugList), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
            }
            
        }
       else if scrollView == tableViewDrugGrid {
            
            if isLoading == true{
                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                    pageNum = pageNum + 1
                    print(pageNum)
                    isLoading = false
                    
                    if Reachability.isConnectedToNetwork() == true {
                        self.addLoadingIndicatiorOnFooterOnTableView()
                        self.addLoadingIndicatiorOnFooterOnTableViewGrid()
                        
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetDrugList), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
            }
            
        }
        else if scrollView == tableViewHealthCategory {
//            if isLoading1 == true{
//                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
//                    pageNum1 = pageNum1 + 1
//                    print(pageNum1)
//                    isLoading1 = false
//                    
//                    if Reachability.isConnectedToNetwork() == true {
//                        self.addLoadingIndicatiorOnFooterOnTableViewHealth()
//                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetHealthConcerns), userInfo: nil, repeats: false)
//                    } else {
//                        showAlert("Check Connection", title: "Internet is not available.")
//                    }
//                    
//                }
//            }
            
        }
        
        
    }
    
    
    // TODO: - DELEGATE METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewDrugList{
            return self.drugData.count
        }
        else if tableView == tableViewDrugGrid{
            if (self.drugData.count%2 != 0) {
                return (self.drugData.count) / 2 + 1
            }
            return (self.drugData.count) / 2
        }
        else{
            return self.healthData.count
        }
        
        

        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableViewDrugList{
            let identifier = "drugListCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? DrugListTableVC
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("DrugListTableVC", owner: self, options: nil)
                cell = nib?[0] as? DrugListTableVC
            }
            cell!.selectionStyle = .none;
            
            
            if let drug_name =  (self.drugData[indexPath.row] as AnyObject).value(forKey: "drug_name") as? String
            {
                cell?.lblDrugName.text = "\(drug_name)"
            }
            
            if let medical_name =  (self.drugData[indexPath.row] as AnyObject).value(forKey: "medical_name") as? String
            {
                cell?.lblMedicalName.text = "\(medical_name)"
            }

            if let weight =  (self.drugData[indexPath.row] as AnyObject).value(forKey: "weight") as? String
            {
                cell?.lblSize.text = "\(weight)"
            }
            
            
            let imageUrl = (self.drugData[indexPath.row] as AnyObject).value(forKey: "front_image") as? String
            let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl!) as String
            let url : NSString = fullUrl as NSString
            let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
            let searchURL : NSURL = NSURL(string: urlStr as String)!
            
            
            cell?.activityIndicatorForDrugImage.isHidden = false
            cell?.activityIndicatorForDrugImage.startAnimating()
            
            cell?.imageViewDrug.sd_setImage(with: searchURL as URL, completed: { (image:UIImage?, error:Error?, cacheType:SDImageCacheType!, imageURL:URL?) in
                
                if ((error) != nil) {
                    cell?.imageViewDrug.image = UIImage.init(named: "image_placeholder.png")
                }
                
                cell?.activityIndicatorForDrugImage.isHidden = true
                
            })
            return cell!
        }
        else if tableView == tableViewDrugGrid{

            let identifier = "drugGridCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? DrugGridTableVC
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("DrugGridTableVC", owner: self, options: nil)
                cell = nib?[0] as? DrugGridTableVC
            }
            cell!.selectionStyle = .none;
        
            let rowLeft = (indexPath.row * 2 )
            let rowRight = (indexPath.row * 2 ) + 1
            
            var temp:Bool = false
            
            
            
            if (self.drugData.count > rowRight ) {
                temp = true;
                cell?.viewRight.isHidden = false;
            }
            else{
                cell?.viewRight.isHidden = true;
                temp = false;
            }

            if let drug_name =  (self.drugData[rowLeft] as AnyObject).value(forKey: "drug_name") as? String
            {
                cell?.lblDrugName.text = "\(drug_name)"
            }
            
            if let medical_name =  (self.drugData[rowLeft] as AnyObject).value(forKey: "medical_name") as? String
            {
                cell?.lblMedicalName.text = "\(medical_name)"
            }
            
            if let weight =  (self.drugData[rowLeft] as AnyObject).value(forKey: "weight") as? String
            {
                cell?.lblSize.text = "\(weight)"
            }
            
            
            let imageUrl = (self.drugData[rowLeft] as AnyObject).value(forKey: "front_image") as? String
            let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl!) as String
            let url : NSString = fullUrl as NSString
            let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
            let searchURL : NSURL = NSURL(string: urlStr as String)!
            
            
            cell?.activityIndicatorForDrugImage.isHidden = false
            cell?.activityIndicatorForDrugImage.startAnimating()
            
            cell?.imageViewDrug.sd_setImage(with: searchURL as URL, completed: { (image:UIImage?, error:Error?, cacheType:SDImageCacheType!, imageURL:URL?) in
                
                if ((error) != nil) {
                    cell?.imageViewDrug.image = UIImage.init(named: "image_placeholder.png")
                }
                
                cell?.activityIndicatorForDrugImage.isHidden = true
                
            })
            
            cell?.btnLeftSelect.tag = rowLeft
            cell?.btnLeftSelect.addTarget(self, action: #selector(self.btnLeftSelectClicked(_:)), for: .touchUpInside)
        
            if (temp){
                
                if let drug_name =  (self.drugData[rowRight] as AnyObject).value(forKey: "drug_name") as? String
                {
                    cell?.lblDrugName1.text = "\(drug_name)"
                }
                
                if let medical_name =  (self.drugData[rowRight] as AnyObject).value(forKey: "medical_name") as? String
                {
                    cell?.lblMedicalName1.text = "\(medical_name)"
                }
                
                if let weight =  (self.drugData[rowRight] as AnyObject).value(forKey: "weight") as? String
                {
                    cell?.lblSize1.text = "\(weight)"
                }
                
                
                let imageUrl = (self.drugData[rowRight] as AnyObject).value(forKey: "front_image") as? String
                let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl!) as String
                let url : NSString = fullUrl as NSString
                let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
                let searchURL : NSURL = NSURL(string: urlStr as String)!
                
                
                cell?.activityIndicatorForDrugImage1.isHidden = false
                cell?.activityIndicatorForDrugImage1.startAnimating()
                
                cell?.imageViewDrug1.sd_setImage(with: searchURL as URL, completed: { (image:UIImage?, error:Error?, cacheType:SDImageCacheType!, imageURL:URL?) in
                    
                    if ((error) != nil) {
                        cell?.imageViewDrug1.image = UIImage.init(named: "image_placeholder.png")
                    }
                    
                    cell?.activityIndicatorForDrugImage1.isHidden = true
                    
                })

                cell?.btnRightSelect.tag = rowRight
                cell?.btnRightSelect.addTarget(self, action: #selector(self.btnRightSelectClicked(_:)), for: .touchUpInside)

            }
            return cell!
        }
        else{
            let identifier = "healthCategory"
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? HealthCategoryTableVC
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("HealthCategoryTableVC", owner: self, options: nil)
                cell = nib?[0] as? HealthCategoryTableVC
            }
            cell!.selectionStyle = .none;
            
            
            if let articalCategoryName =  (self.healthData[indexPath.row] as AnyObject).value(forKey: "hc_name") as? String
            {
                cell?.lblTitle.text = "\(articalCategoryName)"
            }
            
            if let articalCategoryName =  (self.healthData[indexPath.row] as AnyObject).value(forKey: "bodyorgan") as? String
            {
                cell?.lblSubTitle.text = "\(articalCategoryName)"
            }
            
            
            if boolArray.object(at: indexPath.row) as! String == "1"{
                cell?.imageViewSelect.image = UIImage.init(named: "check_blue.png")
            }
            else{
                cell?.imageViewSelect.image = UIImage.init(named: "check_gray.png")
            }
            
            
            if ((self.healthData.object(at: indexPath.row) as AnyObject).value(forKey: "image") as? String == "")
            {
                cell?.imageViewHealthCategory.image = UIImage.init(named: "image_placeholder.png")
                cell?.activityIndicatorForHealth.isHidden = true
                
            }
            else{
                let imageUrl = (self.healthData.object(at: indexPath.row) as AnyObject).value(forKey: "image") as? String
                
                let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl!) as String
                let url : NSString = fullUrl as NSString
                let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
                let searchURL : NSURL = NSURL(string: urlStr as String)!
                
                cell?.activityIndicatorForHealth.isHidden = false
                cell?.activityIndicatorForHealth.startAnimating()
                
                
                cell?.imageViewHealthCategory.sd_setImage(with: searchURL as URL, completed: { (image:UIImage?, error:Error?, cacheType:SDImageCacheType!, imageURL:URL?) in
                    
                    if ((error) != nil) {
                        cell?.imageViewHealthCategory.image = UIImage.init(named: "image_placeholder.png")
                    }
                    cell?.activityIndicatorForHealth.isHidden = true
                })
                
            }
            
            
            
            cell?.btnSelect.tag = indexPath.row
            cell?.btnSelect.addTarget(self, action: #selector(self.btnCategorySelectionClicked(_:)), for: .touchUpInside)
            
            return cell!
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableViewDrugList{
            let drugDetailVC = DrugDetailVC(nibName: "DrugDetailVC", bundle: nil)
            drugDetailVC.drugID = (self.drugData[indexPath.row] as AnyObject).value(forKey: "drug_id") as! String
            self.navigationController?.pushViewController(drugDetailVC, animated: true)
        }
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableViewDrugList{
            return 105.0
        }
        else if tableView == tableViewDrugGrid{
            return 180.0
        }
        else{
            return 60.0
        }
    }
    
    
    // MARK: - PICKERVIEW DELEGATE
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickOption.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       // return (self.pickOption[row] as AnyObject).value(forKey: "title") as? String
          return self.pickOption[row] as? String
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50.0
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let label = UILabel()
        label.textAlignment = NSTextAlignment.center
        
        label.font = UIFont.systemFont(ofSize: 20.0)
        label.text = self.pickOption[row] as? String
        return label
    }

    
    
    
    // TODO: - ACTION METHODS
    @IBAction func btnLeftSelectClicked(_ sender:UIButton){
        let drugDetailVC = DrugDetailVC(nibName: "DrugDetailVC", bundle: nil)
        drugDetailVC.drugID = (self.drugData[sender.tag] as AnyObject).value(forKey: "drug_id") as! String
        self.navigationController?.pushViewController(drugDetailVC, animated: true)

    }
    @IBAction func btnRightSelectClicked(_ sender:UIButton){
        let drugDetailVC = DrugDetailVC(nibName: "DrugDetailVC", bundle: nil)
        drugDetailVC.drugID = (self.drugData[sender.tag] as AnyObject).value(forKey: "drug_id") as! String
        self.navigationController?.pushViewController(drugDetailVC, animated: true)
        
    }
    
    @IBAction func btnBackClicked(_ sender:UIButton){
        _ =  self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSearchClicked(_ sender:UIButton){
//        txtSearch.becomeFirstResponder()
//        
//        UIView.animate(withDuration: 0.3) {
//            
//            self.viewSearchDrug.frame = CGRect(x: self.headerView.frame.origin.x, y: self.headerView.frame.origin.y, width: self.viewSearchDrug.frame.size.width, height: self.viewSearchDrug.frame.size.height)
//            
//        }
        
        let searchVC = SearchDrugVC(nibName: "SearchDrugVC", bundle: nil)
        self.navigationController?.pushViewController(searchVC, animated: true)
        
//        let searchVC = SearchDrugByHealthConcernVC(nibName: "SearchDrugByHealthConcernVC", bundle: nil)
//        self.navigationController?.pushViewController(searchVC, animated: true)
        
    }
    
    @IBAction func btnSearchCancelClicked(_ sender:UIButton){
        txtSearch.text = ""
        txtSearch.resignFirstResponder()
        
        UIView.animate(withDuration: 0.3) {
            
            self.viewSearchDrug.frame = CGRect(x: self.headerView.frame.origin.x+self.headerView.frame.size.width, y: self.headerView.frame.origin.y, width: self.viewSearchDrug.frame.size.width, height: self.viewSearchDrug.frame.size.height)
            
        }

        
        
        drugData = drugGlobalData.mutableCopy() as! NSMutableArray
    /*    if drugData.count == 0 {
            //lblNoData.hidden =FALSE;
            tableViewDrugList.isHidden = true
            tableViewDrugGrid.isHidden = true
        }
        else{
            tableViewDrugList.isHidden = false
            tableViewDrugGrid.isHidden = false
            //lblNoData.hidden =TRUE;
        }*/
        
        tableViewDrugList.reloadData()

    }

    
    
    @IBAction func btnHeaderClicked(_ sender:UIButton){
        
        if sender.tag == 10{
            
            if isList == true{
                isList = false
                tableViewDrugGrid.isHidden = true
                tableViewDrugList.isHidden = false
                
                lblListGrid.text =  NSLocalizedString("grid", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
                imageViewListGrid.image = UIImage.init(named: "Grid-gray.png")
            }
            else{
                isList = true
                tableViewDrugGrid.isHidden = false
                tableViewDrugList.isHidden = true
                lblListGrid.text =  NSLocalizedString("list", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
                imageViewListGrid.image = UIImage.init(named: "List-gray.png")
            }
            tableViewDrugList.reloadData()
            tableViewDrugGrid.reloadData()
        }
        else if sender.tag == 11{
            UIView.animate(withDuration: 0.3, animations: {
                
                self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y - self.viewPicker.frame.size.height - 49, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
                
                
            })

        }
        else if sender.tag == 12{
            let drugFilterVC = DrugFilterVC(nibName: "DrugFilterVC", bundle: nil)
            self.navigationController?.pushViewController(drugFilterVC, animated: true)

        }
        
    }
    
    @IBAction func searchFilter(){
        var predicate = String()
        
        
        if (txtSearch.text == "") {
            drugData = drugGlobalData.mutableCopy() as! NSMutableArray
        }else{
            predicate = String(format: "SELF['drug_name'] contains[c] '%@'", txtSearch.text!)
            print(predicate)
            
            let myPredicate:NSPredicate = NSPredicate(format:predicate)
            let temp = self.drugGlobalData.mutableCopy() as! NSArray
            self.drugData = (temp.filtered(using: myPredicate) as NSArray).mutableCopy() as! NSMutableArray
            
        }
        
        //if self.drugData.count == 0 {
        //    lblNoDrug.isHidden = false
        //    tableViewDrugGrid.isHidden = true
        //    tableViewDrugList.isHidden = true
       // }
       // else{
       //     tableViewDrugGrid.isHidden = false
       //     tableViewDrugList.isHidden = false
       //     lblNoDrug.isHidden = true
       // }
        tableViewDrugList.reloadData()
        tableViewDrugGrid.reloadData()
        
    }

    @IBAction func cancelPicker(sender:AnyObject){
        UIView.animate(withDuration: 0.3) {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
        }
    }
    
    
    @IBAction func donePicker(sender:UIButton){
        isSortData = true
        pickerSelIndx = pickerView.selectedRow(inComponent: 0)
        
        if pickerSelIndx == 0{
            pickerSelectedValue = "asc"
        }
        else if pickerSelIndx == 1{
            pickerSelectedValue = "desc"
        }
        
        print("Picker Index is  :-> \(pickerSelIndx) and value is \(pickOption[pickerSelIndx])")
       // txtDish.text = (self.dishData[pickerSelIndx] as AnyObject).value(forKey: "dishname") as? String
        //dishID = ((self.dishData[pickerSelIndx] as AnyObject).value(forKey: "_id") as? String)!
        
        
        
        self.drugData = NSMutableArray()
        self.drugGlobalData = NSMutableArray()
        self.tableViewDrugList.reloadData()
        self.tableViewDrugGrid.reloadData()
        
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            self.addLoadingIndicatiorOnFooterOnTableViewGrid()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetDrugList), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
        })
    }
    
    @IBAction func btnLoginClicked(_ sender:UIButton){
        
        if USERDEFAULT.value(forKey: "userID") != nil{
            
            if let isMember = USERDEFAULT.value(forKey: "isMember") as? String{
                let ismem = "\(isMember)"
                if ismem == "1"{
                    
                }
                else{
                    let joinMemberVC = JoinMemberVC(nibName: "JoinMemberVC", bundle: nil)
                    joinMemberVC.isFromOther = true
                    joinMemberVC.isFromTab = "0"
                    self.navigationController?.pushViewController(joinMemberVC, animated: true)
                }
            }
            else{
                
            }
        }
        else{
            let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
            loginVC.isFromTab = "0"
            self.navigationController?.pushViewController(loginVC, animated: true)
        }

        
    }
    @IBAction func btnCancelClicked(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNextHealthClicked(_ sender:UIButton){
        
        if healthCategory.count > 0{
            
            viewHealthConcern.removeFromSuperview()
            
            let strVal = "1"
            finalFilterData.setValue(strVal, forKey: "totalFilterType")
            
            
            let notificationName = Notification.Name("filterDrugList")
            USERDEFAULT.setValue(finalFilterData, forKey: "filterDrugData")
            USERDEFAULT.synchronize()
            
            // NotificationCenter.default.post(name: notificationName, object: diseaseTypeData, userInfo: nil)
            NotificationCenter.default.post(name: notificationName, object: nil)
            _ = self.navigationController?.popViewController(animated: true)
            
            if Reachability.isConnectedToNetwork() == true {
                SVProgressHUD.show(withStatus: "Loading..")
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForUpdateProfile), userInfo: nil, repeats: false)
            } else {
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
            
            USERDEFAULT.set("NO", forKey: "isDrugSearch")
            USERDEFAULT.synchronize()
        }
        else{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please select health concerns.", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
    }
    @IBAction func btnBackHealthClicked(_ sender:UIButton){
        viewHealthConcern.removeFromSuperview()
    }
    
    @IBAction func btnCategorySelectionClicked(_ sender:UIButton){
        
        boolArray = NSMutableArray()
        for _ in 0...healthData.count - 1 {
            boolArray.add("0")
        }
        boolArray.replaceObject(at: sender.tag, with: "1")
        categoryCount = 1
        tableViewHealthCategory.reloadData()
        
        /*
        if boolArray.object(at: sender.tag) as! String == "0"{
            boolArray.replaceObject(at: sender.tag, with: "1")
            categoryCount = categoryCount + 1

        }
        else{
            boolArray.replaceObject(at: sender.tag, with: "0")
            categoryCount = categoryCount - 1

        }
        
        
        let indexPath = NSIndexPath(row: sender.tag, section: 0)
        tableViewHealthCategory.reloadRows(at: [indexPath as IndexPath], with: .none)
        */
        healthCategory = NSMutableArray.init()
        for i in 0...healthData.count - 1 {
            if boolArray.object(at: i) as! String == "1"{
                healthCategory.add((self.healthData[i] as AnyObject).value(forKey: "hc_id") as! String)
            }
        }
        
        print("diseaseTypeData ID",healthCategory)
        
        finalFilterData.setValue(healthCategory, forKey: "ddIDData")
        finalFilterData.setValue(categoryCount, forKey: "catCount")

        
    }
    
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForGetDrugList(){
        let completeURL = NSString(format:"%@%@", MainURL,getDrugListURL) as String
        
        let pageNumber = "\(pageNum!)"
        
        var params:NSDictionary!
        
        if self.isFilterData == true{
            
            
           // params = [
           //     "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
           //     "token": USERDEFAULT.value(forKey: "token") as! String,
          ///      "lang_type":"1",
           //     "page":pageNumber,
           //     "limit":PAGINATION_LIMITE
           // ]
            
            let parameter = NSMutableDictionary()
            parameter.setValue(USERDEFAULT.value(forKey: "userID") as? String ?? "", forKey: "user_id")
            parameter.setValue(USERDEFAULT.value(forKey: "token") as? String ?? "", forKey: "token")
            parameter.setValue(Language_Type, forKey: "lang_type")
            parameter.setValue(pageNumber, forKey: "page")
            parameter.setValue(PAGINATION_LIMITE, forKey: "limit")
            
            var filterIDDataString:String = ""
            if let abc = filterData.value(forKey: "ddIDData") as? NSArray{

                let filterIDDataArray = abc
                if filterIDDataArray.count > 0 {
                    filterIDDataString = filterIDDataArray.componentsJoined(by: ",")
                }
                
                if filterIDDataString != "" {
                    parameter.setValue(filterIDDataString, forKey: "hc_id")
                }
                
            }
            var filterCatIDString:String = ""
            
            if let abc = filterData.value(forKey: "dcIDData") as? NSArray{
                let filterCatIDDataArray = abc
                if filterCatIDDataArray.count > 0 {
                    filterCatIDString = filterCatIDDataArray.componentsJoined(by: ",")
                }
                if filterCatIDString != "" {
                    parameter.setValue(filterCatIDString, forKey: "df_id")
                }

            }
            
            //Filter By Ingrendient,Cirtificate Holder
            if let ingredient = filterData.value(forKey: "ingredient") as? String{
                let ingre = "\(ingredient)"
                if ingre != ""{
                    parameter.setValue("\(ingredient)", forKey: "ingredient")
                }
            }
            if let certificateHolder = filterData.value(forKey: "certificateHolder") as? String{
                
                let certificate = "\(certificateHolder)"
                if certificate != ""{
                    parameter.setValue("\(certificate)", forKey: "certificate_holder")
                }
                

            }
            
            
            
            
            if self.isSortData == true{
                if  pickerSelectedValue != ""{
                    parameter.setValue(pickerSelectedValue, forKey: "order_by")
                }
            }
            
            params = parameter.mutableCopy() as! NSDictionary

        }
        else{
          
            let parameter = NSMutableDictionary()
            parameter.setValue(USERDEFAULT.value(forKey: "userID") as? String ?? "", forKey: "user_id")
            parameter.setValue(USERDEFAULT.value(forKey: "token") as? String ?? "", forKey: "token")
            parameter.setValue(Language_Type, forKey: "lang_type")
            parameter.setValue(pageNumber, forKey: "page")
            parameter.setValue(PAGINATION_LIMITE, forKey: "limit")

            if self.isSortData == true{
                if  pickerSelectedValue != ""{
                    parameter.setValue(pickerSelectedValue, forKey: "order_by")
                }
            }
            
            params = parameter.mutableCopy() as! NSDictionary

        }
        

        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetDrugList API Parameter :",finalParams)
        print("GetDrugList API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getDrugListURLTag)
        
    }
    
    func postDataOnWebserviceForGetDrugListForSearch(){
        let completeURL = NSString(format:"%@%@", MainURL,getDrugListURL) as String
        
        let pageNumber = "\(pageNum!)"
        
        var params:NSDictionary!
        
        let searchID = USERDEFAULT.value(forKey: "searchConcernID") as! String
        
        let parameter = NSMutableDictionary()
        parameter.setValue(USERDEFAULT.value(forKey: "userID") as? String ?? "", forKey: "user_id")
        parameter.setValue(USERDEFAULT.value(forKey: "token") as? String ?? "", forKey: "token")
        parameter.setValue(Language_Type, forKey: "lang_type")
        parameter.setValue(pageNumber, forKey: "page")
        parameter.setValue(PAGINATION_LIMITE, forKey: "limit")
        parameter.setValue(searchID, forKey: "hc_id")
        
        
 
        params = parameter.mutableCopy() as! NSDictionary

        
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetDrugListForSearch API Parameter :",finalParams)
        print("GetDrugListForSearch API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getDrugListURLTag)
        
    }
    
    
    
    func postDataOnWebserviceForGetHealthConcerns(){
        
        let completeURL = NSString(format:"%@%@", MainURL,healthConcernURL) as String
        
//         let pageNumber = "\(pageNum1!)"
        // "page":pageNumber,
        // "limit":PAGINATION_LIMITE
        
        let params:NSDictionary = [
            // "user_id" : "49",
            // "token": "ICp48GEVMOBwzHOUu1KJN2xJ2PxsmCSa1jDfTGpERXP6s7pbFH2ZpPvg4PpuJSWX5uCYy701DfKNHHeacneTpCMxU3C0JEvlT5CFjVqbv63FXjlOUZoQ6jsW",
//             "page":pageNumber,
//             "limit":PAGINATION_LIMITE,
            "lang_type":Language_Type
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetArticalCategory API Parameter :",finalParams)
        print("GetArticalCategory API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: healthConcernURLTag)
        
        
    }
    
    func postDataOnWebserviceForGetProfile(){
        let completeURL = NSString(format:"%@%@", MainURL,getUserProfileURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as? String ?? "",
            "token": USERDEFAULT.value(forKey: "token") as? String ?? "",
            "lang_type":Language_Type
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetProfile API Parameter :",finalParams)
        print("GetProfile API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getUserProfileURLTag)
    }

    

    func postDataOnWebserviceForUpdateProfile(){
        let completeURL = NSString(format:"%@%@", MainURL,editUserProfileURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as? String ?? "",
            "token": USERDEFAULT.value(forKey: "token") as? String ?? "",
            "lang_type":Language_Type,
            "health_concern":healthCategory,
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("UpdateProfile API Parameter :",finalParams)
        print("UpdateProfile API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: editUserProfileURLTag)
    }
    
    
    
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case getDrugListURLTag:
            let resultDict = responseObject as! NSDictionary;
            //print("GetDrugList List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum == 1{
                    self.drugData = NSMutableArray()
                    self.drugGlobalData = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        self.drugGlobalData.add(myData[i])
                    }
                    
                    for i in 0...myData.count - 1 {
                        self.drugData.add(myData[i])
                    }
                    
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum > 0) {
                            self.pageNum = self.pageNum - 1
                        }
                        self.isLoading = false
                    }else{
                        self.isLoading = true
                    }
                }
                else{
                    self.isLoading = false
                    if (self.pageNum > 0) {
                        self.pageNum = self.pageNum - 1
                    }
                    
                }
                
                if self.drugData.count == 0{
                    self.tableViewDrugList.isHidden = true
                    self.tableViewDrugGrid.isHidden = true
                    self.lblNoDrug.isHidden = false
                }
                else{
//                    if isList == true{
//                        self.tableViewDrugList.isHidden = false
//                    }
//                    else{
//                        self.tableViewDrugGrid.isHidden = false
//                    }
                    self.lblNoDrug.isHidden = true
                }

                
                self.tableViewDrugList.reloadData()
                self.tableViewDrugGrid.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
                self.removeLoadingIndicatiorOnFooterOnTableViewGrid()

            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        case healthConcernURLTag:
            let resultDict = responseObject as! NSDictionary;
//            print("getDrugDiseaseURLTag List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum1 == 1{
                    self.healthData = NSMutableArray()
                    self.healthGlobalData = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        self.healthGlobalData.add(myData[i])
                    }
                    
                    for i in 0...myData.count - 1 {
                        self.healthData.add(myData[i])
                    }
                    
                    boolArray = NSMutableArray.init()
                    categoryCount = 0
                    for _ in 0...healthData.count - 1 {
                        boolArray.add("0")
                    }
                    
                    
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum1 > 0) {
                            self.pageNum1 = self.pageNum1 - 1
                        }
                        self.isLoading1 = false
                    }else{
                        self.isLoading1 = true
                    }
                }
                else{
                    self.isLoading1 = false
                    if (self.pageNum1 > 0) {
                        self.pageNum1 = self.pageNum1 - 1
                    }
                    
                    if self.healthData.count == 0{
                        self.tableViewHealthCategory.isHidden = true
                        //self.lblNoArtical.isHidden = false
                    }
                    else{
                        self.tableViewHealthCategory.isHidden = false
                        //self.lblNoArtical.isHidden = true
                    }
                }
                self.tableViewHealthCategory.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableViewHealth()
                
                self.setHealthConcernData()

                
                
                if USERDEFAULT.value(forKey: "filterDrugData") != nil{
                    print("Already FilterData :",USERDEFAULT.value(forKey: "filterDrugData") ?? "NOOO")
                    alreadyFilterData = USERDEFAULT.value(forKey: "filterDrugData") as! NSDictionary
                    
                    
                    let temp = NSMutableArray()
                    
                    for i in 0..<self.healthData.count{
                        let id = (self.healthData[i] as AnyObject).value(forKey: "hc_id") as! String
                        temp.add("\(id)")
                    }
                    print(temp)
                    
                    if let abc = alreadyFilterData.value(forKey: "ddIDData") as? NSArray
                    {
                        for i in 0..<abc.count{
                            let myIndexValue:Int = temp.index(of: "\(abc[i])")
                            if self.healthData.count > myIndexValue{
                                self.boolArray.replaceObject(at: myIndexValue, with: "1")
                                let path = NSIndexPath(row: myIndexValue, section: 0)
                                tableViewHealthCategory.reloadRows(at: [path as IndexPath], with:.none)
                            }
                        }
                    }
                    
                    if let catCount = alreadyFilterData.value(forKey: "catCount") as? Int{
                        categoryCount = catCount
                        finalFilterData.setValue(categoryCount, forKey: "catCount")
                        
                    }
                    
                    healthCategory = NSMutableArray.init()
                    
                    for i in 0...healthData.count - 1 {
                        if boolArray.object(at: i) as! String == "1"{
                            healthCategory.add((self.healthData[i] as AnyObject).value(forKey: "hc_id") as! String)
                        }
                    }
                    print("diseaseTypeData ID",healthCategory)
                    finalFilterData.setValue(healthCategory, forKey: "ddIDData")
                    
                    
                }
                
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        case getUserProfileURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("GETProfile Response  : \(resultDict)")
            SVProgressHUD.dismiss()
            if resultDict.value(forKey: "status") as! String == "1"{
                
                //comment by hitesh. must remove this comment. dont forget
                //healthConcernData = (resultDict.value(forKey: "data") as! NSDictionary).value(forKey: "userhealth_concern") as! NSArray
                self.setHealthConcernData()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                if !(loginView.isDescendant(of: self.view)){
                    self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
                }
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
            }
            //   SVProgressHUD.dismiss()
            break
            
            
        case editUserProfileURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("UpdateProfile Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                if let fullname = (resultDict.value(forKey: "data") as! NSDictionary).value(forKey: "fullname") as? String{
                    let name = "\(fullname)"
                    USERDEFAULT.set(name, forKey: "fullName")
                    USERDEFAULT.synchronize()
                    let notificationName = Notification.Name("updateProfileDataNotification")
                    NotificationCenter.default.post(name: notificationName, object: nil)
                    
                }
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
            }
            
            SVProgressHUD.dismiss()
            break
            
            
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case getDrugListURLTag:
            self.isLoading = false
            if (self.pageNum > 0) {
                self.pageNum = self.pageNum - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            self.removeLoadingIndicatiorOnFooterOnTableViewGrid()

            break
            
        case healthConcernURLTag:
            self.isLoading1 = false
            if (self.pageNum1 > 0) {
                self.pageNum1 = self.pageNum1 - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableViewHealth()
            break
        case getUserProfileURLTag:
            //  SVProgressHUD.dismiss()
            break

            
            
        case editUserProfileURLTag:
            SVProgressHUD.dismiss()
            break

            
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    
    // TODO: - OTHERS METHODS
    func setHealthConcernData() {
        let temp = NSMutableArray()
        
        if healthData.count > 0{
            for i in 0..<self.healthData.count{
                let id = (self.healthData[i] as AnyObject).value(forKey: "hc_id") as! String
                temp.add("\(id)")
            }
            print(temp)
        }
        
        if healthConcernData.count > 0{
            if let abc = healthConcernData as? NSArray
            {
                for i in 0..<abc.count{
                    let myIndexValue:Int = temp.index(of: "\(abc[i])")
                    if self.healthData.count > myIndexValue{
                        self.boolArray.replaceObject(at: myIndexValue, with: "1")
                        let path = NSIndexPath(row: myIndexValue, section: 0)
                        tableViewHealthCategory.reloadRows(at: [path as IndexPath], with:.none)
                    }
                }
            }
            USERDEFAULT.setValue(healthConcernData, forKey: "userHealthConcernData")
            USERDEFAULT.synchronize()
        }
        
        
        if healthData.count > 0{
            healthCategory = NSMutableArray.init()
            for i in 0...healthData.count - 1 {
                if boolArray.object(at: i) as! String == "1"{
                    healthCategory.add((self.healthData[i] as AnyObject).value(forKey: "hc_id") as! String)
                }
            }
            
            print("Selected Health Concern Data : ",healthCategory)
            USERDEFAULT.setValue(healthCategory, forKey: "userHealthCategoryData")
            USERDEFAULT.synchronize()

        }

        categoryCount = healthCategory.count
        
        finalFilterData.setValue(healthCategory, forKey: "ddIDData")
        finalFilterData.setValue(categoryCount, forKey: "catCount")

        
    }

    
    
    
    
}
