//
//  HealthIndexTestVC.swift
//  Doc99
//
//  Created by MS-SUB-02 on 29/09/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class HealthIndexTestVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackClicked(_ sender: UIButton)
    {
        _  = self.navigationController?.popViewController(animated: true)
        
    }

    @IBAction func btnIndexClicked(_ sender: UIButton)
    {
        
        let bloodPressureVC = AgeFormVC(nibName: "AgeFormVC", bundle: nil)
        self.navigationController?.pushViewController(bloodPressureVC, animated: true)
        
    }

    @IBAction func btnReportTapped(_ sender: UIButton) {
        let report  = ReportListVC(nibName: "ReportListVC", bundle: nil)
        self.navigationController?.pushViewController(report, animated: true)
    }
}
