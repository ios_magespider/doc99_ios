//
//  PharmacistAdviceVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 02/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class PharmacistAdviceVC: UIViewController {
    
    // FIXME: - VIEW CONTROLLER METHODS
    @IBOutlet var btnSelect1:UIButton!
    @IBOutlet var btnSelect2:UIButton!
    @IBOutlet var btnSelect3:UIButton!
    @IBOutlet var btnSelect4:UIButton!
    
    @IBOutlet var imageViewSelect1:UIImageView!
    @IBOutlet var imageViewSelect2:UIImageView!
    @IBOutlet var imageViewSelect3:UIImageView!
    @IBOutlet var imageViewSelect4:UIImageView!
    
    @IBOutlet var lblSelect1:UILabel!
    @IBOutlet var lblSelect2:UILabel!
    @IBOutlet var lblSelect3:UILabel!
    @IBOutlet var lblSelect4:UILabel!
    
    
    var healthCalculateData = NSMutableArray()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
    }
    func setSelectedButton(radioButton:UIButton) {
        let image = UIImage.init(named: "health_check.png")
        let image1 = UIImage.init(named: "health_Uncheck.png")
        
        if radioButton == btnSelect1{
            imageViewSelect1.image = image
            imageViewSelect2.image = image1
            imageViewSelect3.image = image1
            imageViewSelect4.image = image1
            print("Selected : ",lblSelect1.text ?? "Nothing")
            
            APPDELEGATE.healthIndexData.setValue("30", forKey: "pharmaAdvice")
            
        }
        else if radioButton == btnSelect2{
            imageViewSelect2.image = image
            imageViewSelect3.image = image1
            imageViewSelect4.image = image1
            imageViewSelect1.image = image1
            print("Selected : ",lblSelect2.text ?? "Nothing")
            
            APPDELEGATE.healthIndexData.setValue("31", forKey: "pharmaAdvice")
            
        }
        else if radioButton == btnSelect3{
            imageViewSelect3.image = image
            imageViewSelect4.image = image1
            imageViewSelect1.image = image1
            imageViewSelect2.image = image1
            print("Selected : ",lblSelect3.text ?? "Nothing")
            
            APPDELEGATE.healthIndexData.setValue("32", forKey: "pharmaAdvice")
        }
        else if radioButton == btnSelect4{
            imageViewSelect4.image = image
            imageViewSelect1.image = image1
            imageViewSelect2.image = image1
            imageViewSelect3.image = image1
            print("Selected : ",lblSelect4.text ?? "Nothing")
            
            APPDELEGATE.healthIndexData.setValue("33", forKey: "pharmaAdvice")
            
        }
        
    }
   

    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForHealthCalculateData(){
        let completeURL = NSString(format:"%@%@", MainURL,healthCalculateURL) as String
        
        //        "intra_ocular":APPDELEGATE.healthIndexData.value(forKey: "intraOcular") as! String,
        //        "aria_stroke":APPDELEGATE.healthIndexData.value(forKey: "ariaStroke") as! String,
        
        let currentDate = getStringDateFromDate(dateFormat: "yyyy-MM-dd HH:mm:ss", enterDate: Date())
        print("Current Date :",currentDate)
        
        var Gender:String
        
        if APPDELEGATE.healthIndexData.value(forKey: "genderIs")  == nil
        {
            Gender = ""
        }
        else
        {
            Gender = APPDELEGATE.healthIndexData.value(forKey: "genderIs") as! String

        }
//        var asdf:Int
//        if APPDELEGATE.healthIndexData.value(forKey: "birthDateIs")  == nil
//        {
//        }
//        else
//        {
//             asdf = self.calculateAge(dob: APPDELEGATE.healthIndexData.value(forKey: "birthDateIs") as! String)
//        }
//        
//        let strAge: String = String(asdf)
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "blood_pressure":APPDELEGATE.healthIndexData.value(forKey: "bloodPressure") as! String,
            "blood_glucose":APPDELEGATE.healthIndexData.value(forKey: "bloodGlocose") as! String,
            "uric_acid":APPDELEGATE.healthIndexData.value(forKey: "uricAcid") as! String,
            "pharmacist_advice":APPDELEGATE.healthIndexData.value(forKey: "pharmaAdvice") as! String,
            "sex":Gender,
            "age":APPDELEGATE.healthIndexData.value(forKey: "pharmaAdvice") as! String,
            "height":"",
            "weight":"",
            "bmi":"",
            "ost":"",
            "health_date":currentDate
        ]
        

        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("HealthCalculateData API Parameter :",finalParams)
        print("HealthCalculateData API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: healthCalculateURLTag)
        
    }
    
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case healthCalculateURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("HealthCalculate Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                let result = resultDict.value(forKey: "data") as! NSDictionary
                healthCalculateData = NSMutableArray()
                
                if let diastolic_presssure = NumberFormatter().number(from: result.value(forKey: "diastolic_presssure") as! String) {
                    healthCalculateData.add(CGFloat(diastolic_presssure))
                }
                
                if let nutrition = NumberFormatter().number(from: result.value(forKey: "nutrition") as! String) {
                    healthCalculateData.add(CGFloat(nutrition))
                }
                
                if let excercise = NumberFormatter().number(from: result.value(forKey: "excercise") as! String) {
                    healthCalculateData.add(CGFloat(excercise))
                }
                
                if let advice = NumberFormatter().number(from: result.value(forKey: "advice") as! String) {
                    healthCalculateData.add(CGFloat(advice))
                }
                
                if let lasttotalscore = result.value(forKey: "lasttotalscore") as? String{
                    let lastTot = "\(lasttotalscore)"
                    if let lasttotalscore = NumberFormatter().number(from: lastTot) {
                        healthCalculateData.add(CGFloat(lasttotalscore))
                    }
                    
                }
                else if let lasttotalscore = result.value(forKey: "lasttotalscore") as? NSNumber{
                    healthCalculateData.add(CGFloat(lasttotalscore))
                }
                
                
                
                
                print("HealthCalculated Data",healthCalculateData)
                
                
                
                print("Health IndexData : %@",APPDELEGATE.healthIndexData)
                let reportVC = ReportVC(nibName: "ReportVC", bundle: nil)
               // reportVC.bmiValue = lblBmi.text
               // reportVC.ostValue = lblOst.text
                if let lasttotalscore = result.value(forKey: "lasttotalscore") as? NSArray
                {
                    reportVC.arrLineChartData = lasttotalscore
                }
                reportVC.barChartYValuesData = healthCalculateData.mutableCopy() as! NSArray
                self.navigationController?.pushViewController(reportVC, animated: true)
                
                
                
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case healthCalculateURLTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    
    //AlertDelegate Methods protocol
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }

    // TODO: - ACTION METHODS
    @IBAction func btnNextClicked(_ sender: UIButton) {
//        let intraOcularPressureVC = IntraOcularPressureVC(nibName: "IntraOcularPressureVC", bundle: nil)
//        self.navigationController?.pushViewController(intraOcularPressureVC, animated: true)
        
        if APPDELEGATE.healthIndexData.value(forKey: "pharmaAdvice") != nil{
            if Reachability.isConnectedToNetwork() == true {
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForHealthCalculateData), userInfo: nil, repeats: false)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
        }
        else{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please select PharmaAdvice", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }

        

    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
        APPDELEGATE.healthIndexData.setValue(nil, forKey: "pharmaAdvice")

    }
    
    @IBAction func btnPharmacistSelectionClicked(_ sender: UIButton) {
        
        if sender == btnSelect1{
            self.setSelectedButton(radioButton: btnSelect1)
        }
        else if sender == btnSelect2{
            self.setSelectedButton(radioButton: btnSelect2)
        }
        else if sender == btnSelect3{
            self.setSelectedButton(radioButton: btnSelect3)
        }
        else if sender == btnSelect4{
            self.setSelectedButton(radioButton: btnSelect4)
        }
        
    }

    
    // TODO: - POST DATA METHODS
    
    func calculateAge(dob : String) -> Int{
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let date = df.date(from: dob)
        NSLog("%@", dob)
        guard let val = date else{
            return (0)
        }
        var years = 0
        var months = 0
        var days = 0
        
        let cal = NSCalendar.current
        years = cal.component(.year, from: NSDate() as Date) -  cal.component(.year, from: val)
        
        let currMonth = cal.component(.month, from: NSDate() as Date)
        let birthMonth = cal.component(.month, from: val)
        
        //get difference between current month and birthMonth
        months = currMonth - birthMonth
        //if month difference is in negative then reduce years by one and calculate the number of months.
        if months < 0
        {
            years = years - 1
            months = 12 - birthMonth + currMonth
            if cal.component(.day, from: NSDate() as Date) < cal.component(.day, from: val){
                months = months - 1
            }
        } else if months == 0 && cal.component(.day, from: NSDate() as Date) < cal.component(.day, from: val)
        {
            years = years - 1
            months = 11
        }
        
        //Calculate the days
        if cal.component(.day, from: NSDate() as Date) > cal.component(.day, from: val){
            days = cal.component(.day, from: NSDate() as Date) - cal.component(.day, from: val)
        }
        else if cal.component(.day, from: NSDate() as Date) < cal.component(.day, from: val)
        {
            let today = cal.component(.day, from: NSDate() as Date)
            let date = cal.date(byAdding:.month, value:  -1, to: NSDate() as Date)
            
            //let date = cal.dateByAddingUnit(.Month, value: -1, toDate: NSDate(), options: [])
            
            days = (cal.component(.day, from: date!) - cal.component(.day, from: val)) + today
        } else
        {
            days = 0
            if months == 12
            {
                years = years + 1
                months = 0
            }
        }
        
        return years
    }
    

}

