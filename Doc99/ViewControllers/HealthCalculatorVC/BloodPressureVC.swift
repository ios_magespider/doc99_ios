//
//  BloodPressureVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 02/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class BloodPressureVC: UIViewController {

    // FIXME: - VIEW CONTROLLER METHODS
    @IBOutlet var btnBelow85:UIButton!
    @IBOutlet var btn85to90:UIButton!
    @IBOutlet var btn90to95:UIButton!
    @IBOutlet var btnUp95:UIButton!
    
    @IBOutlet var imageViewBelow85:UIImageView!
    @IBOutlet var imageView85to90:UIImageView!
    @IBOutlet var imageView90to95:UIImageView!
    @IBOutlet var imageViewUp95:UIImageView!

    @IBOutlet var lblBelow85:UILabel!
    @IBOutlet var lbl85to90:UILabel!
    @IBOutlet var lbl90to95:UILabel!
    @IBOutlet var lblUp95:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
    }
    func setSelectedButton(radioButton:UIButton) {
        let image = UIImage.init(named: "health_check.png")
        let image1 = UIImage.init(named: "health_Uncheck.png")
        
        if radioButton == btnBelow85{
            imageViewBelow85.image = image
            imageView85to90.image = image1
            imageView90to95.image = image1
            imageViewUp95.image = image1
            print("Selected : ",lblBelow85.text ?? "Nothing")
            APPDELEGATE.healthIndexData.setValue("80", forKey: "bloodPressure")
        }
        else if radioButton == btn85to90{
            imageView85to90.image = image
            imageViewBelow85.image = image1
            imageView90to95.image = image1
            imageViewUp95.image = image1
            print("Selected : ",lbl85to90.text ?? "Nothing")
            APPDELEGATE.healthIndexData.setValue("85", forKey: "bloodPressure")
            
        }
        else if radioButton == btn90to95{
            imageView90to95.image = image
            imageView85to90.image = image1
            imageViewBelow85.image = image1
            imageViewUp95.image = image1
            print("Selected : ",lbl90to95.text ?? "Nothing")
            APPDELEGATE.healthIndexData.setValue("90", forKey: "bloodPressure")
            
            
        }
        else if radioButton == btnUp95{
            imageViewUp95.image = image
            imageView85to90.image = image1
            imageView90to95.image = image1
            imageViewBelow85.image = image1
            print("Selected : ",lblUp95.text ?? "Nothing")
            APPDELEGATE.healthIndexData.setValue("95", forKey: "bloodPressure")
            
        }
        
    }
    // TODO: - DELEGATE METHODS
    
    
    // TODO: - ACTION METHODS
    @IBAction func btnNextClicked(_ sender: UIButton) {
        
        if APPDELEGATE.healthIndexData.value(forKey: "bloodPressure") != nil{
            let bloodGlocoseTestVC = BloodGlocoseTestVC(nibName: "BloodGlocoseTestVC", bundle: nil)
            self.navigationController?.pushViewController(bloodGlocoseTestVC, animated: true)
        }
        else{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: "Please select BloodPressure")
        }
        
        
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
        APPDELEGATE.healthIndexData.setValue(nil, forKey: "bloodPressure")

    }
    
    @IBAction func btnBloodPressureSelectionClicked(_ sender: UIButton) {
        
        if sender == btnBelow85{
            self.setSelectedButton(radioButton: btnBelow85)
        }
        else if sender == btn85to90{
            self.setSelectedButton(radioButton: btn85to90)
        }
        else if sender == btn90to95{
            self.setSelectedButton(radioButton: btn90to95)
        }
        else if sender == btnUp95{
            self.setSelectedButton(radioButton: btnUp95)
        }
        
    }
    
    // TODO: - POST DATA METHODS

}
