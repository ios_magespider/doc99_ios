//
//  BMICountVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 02/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD
class BMICountVC: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UIAlertViewDelegate {
    
    // FIXME: - VIEW CONTROLLER METHODS
    @IBOutlet var txtAge:UITextField!
    @IBOutlet var txtHeight:UITextField!
    @IBOutlet var txtWeight:UITextField!
    @IBOutlet var txtGender:UITextField!

    @IBOutlet var lblBmi:UILabel!
    @IBOutlet var lblOst:UILabel!

    
    @IBOutlet var viewPicker:UIView!
    @IBOutlet var pickerView:UIPickerView!
    var pickerSelIndx:Int!
    var pickOption:NSMutableArray!
    
    @IBOutlet var scrMain:UIScrollView!

    @IBOutlet var lblHeader:UILabel!
    @IBOutlet var lblTitleSex:UILabel!
    @IBOutlet var lblTitleAge:UILabel!
    @IBOutlet var lblTitleHeight:UILabel!
    @IBOutlet var lblTitleWeight:UILabel!
    @IBOutlet var lblTitleBmi:UILabel!
    @IBOutlet var lblTitleOst:UILabel!
    @IBOutlet var lblTitleSubmit:UILabel!

    var healthCalculateData = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        
        pickerSelIndx=0
        pickOption = NSMutableArray()
        pickOption = ["Male","Female"]
        pickerView.reloadAllComponents()
        viewPicker.frame = CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: viewPicker.frame.size.width, height: viewPicker.frame.size.height)

        self.addTapGestureInOurView()
        self.NavBarNumberPad()
        self.setLocalizationText()
    }
    
    func setLocalizationText(){
        lblHeader.text = NSLocalizedString("BMI & OST Count", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitleSex.text = NSLocalizedString("Sex", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitleAge.text = NSLocalizedString("Age", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitleHeight.text = NSLocalizedString("Height (cm)", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitleWeight.text = NSLocalizedString("Weight (Kg)", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitleBmi.text = NSLocalizedString("BMI", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitleOst.text = NSLocalizedString("OST", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitleSubmit.text = NSLocalizedString("SUBMIT", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

    }
    

    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        
        if viewTouched!.isKind(of: UIButton.self) && viewTouched!.isKind(of:UITextView.self){
            
        }
        else{
            self.view.endEditing(true)
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
            
            
            if (viewTouched!.superview!.superview == viewPicker) {
            }else if(viewTouched!.superview == viewPicker){
            }else if(viewTouched == viewPicker){
            }else{
                UIView.animate(withDuration: 0.3) {
                    
                    self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
                    
                    
                    
                }
                
            }
        }
    }
    
    // Numeric Keyboard Hide Method
    func NavBarNumberPad(){
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        numberToolbar.barStyle = .default
        
        let viewBack = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        viewBack.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        numberToolbar.addSubview(viewBack)
        
        let btnCancel = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelNumberPad))
        btnCancel.tintColor = UIColor.white
        
        let btnDone = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.doneWithNumberPad))
        btnDone.tintColor = UIColor.white
        
        numberToolbar.items = NSArray(objects: btnCancel,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),btnDone) as? [UIBarButtonItem]
        
        
        numberToolbar.sizeToFit()
        txtAge.inputAccessoryView = numberToolbar
        txtWeight.inputAccessoryView = numberToolbar
        txtHeight.inputAccessoryView = numberToolbar

    }
    
    func cancelNumberPad(){
        txtAge.resignFirstResponder()
        txtWeight.resignFirstResponder()
        txtHeight.resignFirstResponder()
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)

    }
    
    func doneWithNumberPad(){
        txtAge.resignFirstResponder()
        txtWeight.resignFirstResponder()
        txtHeight.resignFirstResponder()
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)

    }
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    
    //AlertDelegate Methods protocol
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }
    

    
    
    
    // TODO: - DELEGATE METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
            
        }
        return false
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var YOffset:Int = 0
        
        if (textField.tag==4) {
            YOffset=0
        }
        else if (textField.tag==5){
            YOffset=0
        }
        else if (textField.tag==6){
            YOffset=0
        }
        
        if (DeviceType.IS_IPHONE_4_OR_LESS) {
            
            if (textField.tag==4) {
                YOffset=25
            }
            else if (textField.tag==5){
                YOffset=30
            }
            else if (textField.tag==6){
                YOffset=30
            }
        }
        scrMain.setContentOffset(CGPoint(x: 0, y: CGFloat(textField.tag * YOffset)), animated: true)
        return true
        
    }

    
    
    // MARK: - PICKERVIEW DELEGATE
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickOption.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        // return (self.pickOption[row] as AnyObject).value(forKey: "title") as? String
        return self.pickOption[row] as? String
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50.0
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let label = UILabel()
        label.textAlignment = NSTextAlignment.center
        
        label.font = UIFont.systemFont(ofSize: 20.0)
        label.text = self.pickOption[row] as? String
        return label
    }

    
    
    
    
    // TODO: - ACTION METHODS
    @IBAction func bmiOstCalculate(){
        
        if txtWeight.text == "" || txtHeight.text == "" || txtAge.text == ""{
            return;
        }
        else{
            let weight:Double = Double(txtWeight.text!)!
            let height:Double = Double(txtHeight.text!)!
            let age:Double = Double(txtAge.text!)!
            
            let heightMeter = (height / 100)


            let BMI = (weight) / (heightMeter * heightMeter)
            //let BMI = (weight/2.205) / (height/39.37)
            lblBmi.text = String(BMI)
            
            let str = lblBmi.text!
            let nsString = str as NSString
            if nsString.length > 0
            {
                lblBmi.text = nsString.substring(with: NSRange(location: 0, length: nsString.length > 5 ? 5 : nsString.length))
            }
            
            
            let OST = 0.2 * (weight - age)
            lblOst.text = String(OST)
            
            let str1 = lblOst.text!
            let nsString1 = str1 as NSString
            if nsString1.length > 0
            {
                lblOst.text = nsString1.substring(with: NSRange(location: 0, length: nsString1.length > 5 ? 5 : nsString1.length))
            }

        }
        
    }
    
    @IBAction func btnNextClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        

        if txtGender.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please select gender", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if txtAge.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter age", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if txtHeight.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter height", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if txtWeight.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter weight", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else{
            APPDELEGATE.healthIndexData.setValue(txtGender.text, forKey: "gender")
            APPDELEGATE.healthIndexData.setValue(txtAge.text, forKey: "age")
            APPDELEGATE.healthIndexData.setValue(txtHeight.text, forKey: "height")
            APPDELEGATE.healthIndexData.setValue(txtWeight.text, forKey: "weight")
            APPDELEGATE.healthIndexData.setValue(lblBmi.text, forKey: "bmi")
            APPDELEGATE.healthIndexData.setValue(lblOst.text, forKey: "ost")

            
            
            
            if Reachability.isConnectedToNetwork() == true {
                SVProgressHUD.show(withStatus: "Loading..")
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForHealthCalculateData), userInfo: nil, repeats: false)
            } else {
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
        }
        
        
        
        
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnGenderClicked(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y - self.viewPicker.frame.size.height, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
            
        })
    }
    @IBAction func cancelPicker(sender:AnyObject){
        UIView.animate(withDuration: 0.3) {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
        }
        
    }
    
    @IBAction func donePicker(sender:UIButton){
        
        pickerSelIndx = pickerView.selectedRow(inComponent: 0)
        
        print("Picker Index is  :-> \(pickerSelIndx) and value is \(pickOption[pickerSelIndx])")
        txtGender.text = self.pickOption[pickerSelIndx] as? String
        //dishID = ((self.dishData[pickerSelIndx] as AnyObject).value(forKey: "_id") as? String)!
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
        })
    }

    
    
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForHealthCalculateData(){
        let completeURL = NSString(format:"%@%@", MainURL,healthCalculateURL) as String
        
//        "intra_ocular":APPDELEGATE.healthIndexData.value(forKey: "intraOcular") as! String,
//        "aria_stroke":APPDELEGATE.healthIndexData.value(forKey: "ariaStroke") as! String,
        
        var weightIs : String = ""
        
        if APPDELEGATE.healthIndexData.value(forKey: "weight") as? String == nil
        {
            weightIs = "";
        }
        else
        {
            weightIs = APPDELEGATE.healthIndexData.value(forKey: "weight") as! String
        }
        
        
        var heightIs : String = ""
        
        if APPDELEGATE.healthIndexData.value(forKey: "height") as? String == nil
        {
            heightIs = "";
        }
        else
        {
            heightIs = APPDELEGATE.healthIndexData.value(forKey: "height") as! String
        }
        
        var ageIs : String = ""
        
        if APPDELEGATE.healthIndexData.value(forKey: "age") as? String == nil
        {
            ageIs = "";
        }
        else
        {
            ageIs = APPDELEGATE.healthIndexData.value(forKey: "age") as! String
        }
        
        var genderIs : String = ""
        
        if APPDELEGATE.healthIndexData.value(forKey: "gender") as? String == nil
        {
            genderIs = "";
        }
        else
        {
            genderIs = APPDELEGATE.healthIndexData.value(forKey: "gender") as! String
        }
        
        
        
        var bmiIs : String = ""
        
        if APPDELEGATE.healthIndexData.value(forKey: "bmi") as? String == nil
        {
            bmiIs = "";
        }
        else
        {
            bmiIs = APPDELEGATE.healthIndexData.value(forKey: "bmi") as! String
        }

        
        var ostIs : String = ""
        
        if APPDELEGATE.healthIndexData.value(forKey: "ost") as? String == nil
        {
            ostIs = "";
        }
        else
        {
            ostIs = APPDELEGATE.healthIndexData.value(forKey: "ost") as! String
        }

        
        let currentDate = getStringDateFromDate(dateFormat: "yyyy-MM-dd HH:mm:ss", enterDate: Date())
        print("Current Date :",currentDate)
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "blood_pressure":APPDELEGATE.healthIndexData.value(forKey: "bloodPressure") as! String,
            "blood_glucose":APPDELEGATE.healthIndexData.value(forKey: "bloodGlocose") as! String,
            "uric_acid":APPDELEGATE.healthIndexData.value(forKey: "uricAcid") as! String,
            "pharmacist_advice":APPDELEGATE.healthIndexData.value(forKey: "pharmaAdvice") as! String,
            "sex":genderIs,
            "age":ageIs,
            "height":heightIs,
            "weight":weightIs,
            "bmi":bmiIs,
            "ost":ostIs,
            "health_date":currentDate
        ]
        
       
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("HealthCalculateData API Parameter :",finalParams)
        print("HealthCalculateData API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: healthCalculateURLTag)
        
    }
   
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case healthCalculateURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("HealthCalculate Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                let result = resultDict.value(forKey: "data") as! NSDictionary
               healthCalculateData = NSMutableArray()
                
                if let diastolic_presssure = NumberFormatter().number(from: result.value(forKey: "diastolic_presssure") as! String) {
                    healthCalculateData.add(CGFloat(diastolic_presssure))
                }
                
                if let nutrition = NumberFormatter().number(from: result.value(forKey: "nutrition") as! String) {
                    healthCalculateData.add(CGFloat(nutrition))
                }
                
                if let excercise = NumberFormatter().number(from: result.value(forKey: "excercise") as! String) {
                    healthCalculateData.add(CGFloat(excercise))
                }

                if let advice = NumberFormatter().number(from: result.value(forKey: "advice") as! String) {
                    healthCalculateData.add(CGFloat(advice))
                }
                
                if let lasttotalscore = result.value(forKey: "lasttotalscore") as? String{
                    let lastTot = "\(lasttotalscore)"
                    if let lasttotalscore = NumberFormatter().number(from: lastTot) {
                        healthCalculateData.add(CGFloat(lasttotalscore))
                    }

                }
                else if let lasttotalscore = result.value(forKey: "lasttotalscore") as? NSNumber{
                    healthCalculateData.add(CGFloat(lasttotalscore))
                }
                
               
                
                print("HealthCalculated Data",healthCalculateData)

                
                
                print("Health IndexData : %@",APPDELEGATE.healthIndexData)
                let reportVC = ReportVC(nibName: "ReportVC", bundle: nil)
                reportVC.bmiValue = lblBmi.text
                reportVC.ostValue = lblOst.text
                if let lasttotalscore = result.value(forKey: "lasttotalscore") as? NSArray
                {
                    reportVC.arrLineChartData = lasttotalscore
                }
                reportVC.barChartYValuesData = healthCalculateData.mutableCopy() as! NSArray
                self.navigationController?.pushViewController(reportVC, animated: true)

                
                
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case healthCalculateURLTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }

}
