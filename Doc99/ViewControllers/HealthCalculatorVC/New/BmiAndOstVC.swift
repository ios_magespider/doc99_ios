//
//  BmiAndOstVC.swift
//  Doc99
//
//  Created by MS-SUB-02 on 06/10/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class BmiAndOstVC: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UIAlertViewDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var txtTScore: UITextField!
    @IBOutlet var txtWeight:UITextField!
    @IBOutlet var txtUnit:UITextField!
    @IBOutlet var scrMain:UIScrollView!
    
    @IBOutlet weak var txtAge: UITextField!
    @IBOutlet weak var txtGender: UITextField!
    
    @IBOutlet var viewPicker:UIView!
    @IBOutlet var pickerView:UIPickerView!
    var pickerSelIndx:Int!
    var pickOption = NSArray()
    var genderArray = NSArray()
    
    
    var selection = "weight"
    
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.generalViewControllerSetting()
        
        //        viewBg.layer.borderWidth=0.5
        //        viewBg.layer.borderColor=UIColor.lightGray.cgColor
        // Do any additional setup after loading the view.
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        
        pickerSelIndx=0
        
        pickOption = ["kg", "gram"]
        genderArray = ["Male", "Female"];
        
        pickerView.reloadAllComponents()
        viewPicker.frame = CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: viewPicker.frame.size.width, height: viewPicker.frame.size.height)
        
        self.addTapGestureInOurView()
        self.NavBarNumberPad()
    }
    
    
        func addTapGestureInOurView(){
            let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
            tapRecognizer.cancelsTouchesInView = false
            self.scrMain.addGestureRecognizer(tapRecognizer)
        }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        
        if viewTouched!.isKind(of: UIButton.self) && viewTouched!.isKind(of:UITextView.self){
            
        }
        else{
            self.view.endEditing(true)
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
            
            
            if (viewTouched!.superview!.superview == viewPicker) {
            }else if(viewTouched!.superview == viewPicker){
            }else if(viewTouched == viewPicker){
            }else{
                UIView.animate(withDuration: 0.3) {
                    
                    self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
                    
                    
                }
                
            }
        }
    }
    
    
    
    // MARK: - PICKERVIEW DELEGATE
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if selection == "weight"
        {
            return self.pickOption.count
        }
        else{
            return self.genderArray.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if selection == "weight"
        {
            return self.pickOption[row] as? String
        }
        else{
            return self.genderArray[row] as? String
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50.0
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let label = UILabel()
        label.textAlignment = NSTextAlignment.center
        
        label.font = UIFont.systemFont(ofSize: 20.0)
        if selection == "weight"{
            label.text = self.pickOption[row] as? String
        }else{
           label.text = self.genderArray[row] as? String
        }
        return label
    }
    
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSaveRecordClicked(_ sender: UIButton) {
        
       
        if Utility.isEmpty(txtWeight.text) && Utility.isEmpty(txtTScore.text){
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter Weight", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if Utility.isEmpty(txtUnit.text) && Utility.isEmpty(txtTScore.text){
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please select unit", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if Utility.isEmpty(txtAge.text) && Utility.isEmpty(txtTScore.text){
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter age", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if Utility.isEmpty(txtGender.text) && Utility.isEmpty(txtTScore.text){
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please select gender", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if(Utility.isEmpty(txtTScore.text) && Utility.isEmpty(txtWeight.text) && Utility.isEmpty(txtUnit.text))
        {
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter T-Score", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else{
            
            let bloodPressureVC = NewResultVC(nibName: "NewResultVC", bundle: nil)
            bloodPressureVC.isFromTwoValue = false
            bloodPressureVC.isFromUricAcid = false
            bloodPressureVC.isFromIntraOcular = false
            bloodPressureVC.strFirst = "asdf"
            
            bloodPressureVC.gen = txtGender.text!
            bloodPressureVC.age = txtAge.text!
            bloodPressureVC.tScore = txtTScore.text!
            bloodPressureVC.strSecound = txtWeight.text! as String as NSString?
            bloodPressureVC.strThird = txtUnit.text! as String as NSString?

            self.navigationController?.pushViewController(bloodPressureVC, animated: true)

            
        }
        
        
    }
    @IBAction func btnSelectOptionClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        selection = "weight"
        pickerView.reloadAllComponents()
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y - self.viewPicker.frame.size.height, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
            
        })
    }
    @IBAction func btnGenderClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        selection = "gender"
        pickerView.reloadAllComponents()
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y - self.viewPicker.frame.size.height, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
            
        })
    }
    @IBAction func cancelPicker(sender:AnyObject){
        UIView.animate(withDuration: 0.3) {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
        }
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
        
    }
    
    @IBAction func donePicker(sender:UIButton){
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        if selection == "weight"
        {
            pickerSelIndx = pickerView.selectedRow(inComponent: 0)
            print("Picker Index is  :-> \(pickerSelIndx) and value is \(pickOption[pickerSelIndx])")
            txtUnit.text = self.pickOption[pickerSelIndx] as? String
        }
        else{
            pickerSelIndx = pickerView.selectedRow(inComponent: 0)
            print("Picker Index is  :-> \(pickerSelIndx) and value is \(genderArray[pickerSelIndx])")
            txtGender.text = self.genderArray[pickerSelIndx] as? String
        }
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
        })
    }
    
        // TODO: - DELEGATE METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder?
        if (nextResponder != nil){
            
            
            let nextResponder1 = UIButton.superclass()
            if (nextResponder1 != nil)
            {
                if (textField.tag==1){
                    
                    UIView.animate(withDuration: 0.3, animations: {
                        
                        self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y - self.viewPicker.frame.size.height, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
                        
                        
                    })
                    
                }
            }
            else
            {
                
                nextResponder?.becomeFirstResponder()
            }
        }
        else
        {
            textField.resignFirstResponder()
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
            
        }
        return false
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(txtWeight == textField || txtAge == textField)
        {
            txtTScore.text = ""
        }
        else if(txtTScore == textField)
        {
            txtWeight.text = ""
            txtAge.text = ""
            
            let aSet = NSCharacterSet(charactersIn:"0123456789-+.").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var YOffset:Int = 0
        
//        if (textField.tag==1) {
//            YOffset=130
//        }else if (textField.tag==2){
//            YOffset=95
//        }else if (textField.tag==3){
//            YOffset=85
//        }
        
        if (DeviceType.IS_IPHONE_4_OR_LESS) {
            
            if (textField.tag==1) {
                YOffset=0
            }else if (textField.tag==2){
                YOffset=0
            }else if (textField.tag==3){
                YOffset=5
            }
            
        }
        scrMain.setContentOffset(CGPoint(x: 0, y: CGFloat(textField.tag * YOffset)), animated: true)
        return true
        
    }
    
    
    // Numeric Keyboard Hide Method
    func NavBarNumberPad(){
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        numberToolbar.barStyle = .default
        
        let viewBack = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        viewBack.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        numberToolbar.addSubview(viewBack)
        
        let btnCancel = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelNumberPad))
        btnCancel.tintColor = UIColor.white
        
        let btnDone = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.doneWithNumberPad))
        btnDone.tintColor = UIColor.white
        
        numberToolbar.items = NSArray(objects: btnCancel,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),btnDone) as? [UIBarButtonItem]
        numberToolbar.sizeToFit()

        txtWeight.inputAccessoryView = numberToolbar
        txtAge.inputAccessoryView = numberToolbar
        
    }
    
    func cancelNumberPad(){
        self.view.endEditing(true)
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
        
    }
    
    func doneWithNumberPad(){
        self.view.endEditing(true)
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
    }
    
    
    
    
}
