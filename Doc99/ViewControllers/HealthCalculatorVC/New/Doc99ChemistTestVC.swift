//
//  Doc99ChemistTestVC.swift
//  Doc99
//
//  Created by MS-SUB-02 on 29/09/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class Doc99ChemistTestVC: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource  {
    @IBOutlet var txtName:UITextField!
    @IBOutlet var txtBirthDate:UITextField!
    @IBOutlet var txtGender:UITextField!
    
    
    @IBOutlet var datePicker:UIDatePicker!
    @IBOutlet var viewDatePicker:UIView!
    
    
    @IBOutlet var viewPicker:UIView!
    @IBOutlet var pickerView:UIPickerView!
    var pickerSelIndx:Int!
    var pickOption:NSMutableArray!
    var profileData = NSMutableDictionary()
    
 

    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        APPDELEGATE.myTabBarController?.tabBar.isHidden=true

        if USERDEFAULT.value(forKey: "userID") != nil{
            if USERDEFAULT.value(forKey: "gender") as? String == "" {
                if Reachability.isConnectedToNetwork() {
                    SVProgressHUD.show(withStatus: "Loading...")
                    self.postDataOnWebserviceForGetProfile()
                }
                else{
                    showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        self.addTapGestureInOurView()
        
        pickerSelIndx=0
        pickOption = NSMutableArray()
        pickOption = ["Male","Female"]
        pickerView.reloadAllComponents()
        
        viewPicker.frame = CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: viewPicker.frame.size.width, height: viewPicker.frame.size.height)

    }
    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        if viewTouched!.isKind(of: UIButton.self){
            
        }
        else{
            self.view.endEditing(true)
            //scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        }
        
        if (viewTouched!.superview!.superview == viewDatePicker) {
        }else if(viewTouched!.superview == viewDatePicker){
        }else if(viewTouched == viewDatePicker){
        }else{
            UIView.animate(withDuration: 0.3) {
                
                self.viewDatePicker.frame=CGRect(x: self.view.frame.size.width - self.viewDatePicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewDatePicker.frame.size.width, height: self.viewDatePicker.frame.size.height)
                
            }
        }
        
        
        
    }
        
    // MARK: - PICKERVIEW DELEGATE
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickOption.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        // return (self.pickOption[row] as AnyObject).value(forKey: "title") as? String
        return self.pickOption[row] as? String
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50.0
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let label = UILabel()
        label.textAlignment = NSTextAlignment.center
        
        label.font = UIFont.systemFont(ofSize: 20.0)
        label.text = self.pickOption[row] as? String
        return label
    }
    
    // TODO: - ACTION METHODS
    @IBAction func btnGenderClicked(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y - self.viewPicker.frame.size.height, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
            
        })
    }
    @IBAction func cancelPickerForGender(sender:AnyObject){
        UIView.animate(withDuration: 0.3) {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
        }
        
    }
    
    @IBAction func donePicker(sender:UIButton){
        
        pickerSelIndx = pickerView.selectedRow(inComponent: 0)
        
        
        
        print("Picker Index is  :-> \(pickerSelIndx) and value is \(pickOption[pickerSelIndx])")
        txtGender.text = self.pickOption[pickerSelIndx] as? String
        //dishID = ((self.dishData[pickerSelIndx] as AnyObject).value(forKey: "_id") as? String)!
        
        
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
        })
    }


    @IBAction func btnBackClicked(_ sender: UIButton)
    {
        _  = self.navigationController?.popViewController(animated: true)
        
    }
 
    @IBAction func btnStartClicked(_ sender: UIButton)
    {
        if txtName.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter Name", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if txtBirthDate.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter BirthDate", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if txtGender.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter Gender", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else{
            if USERDEFAULT.value(forKey: "userID") != nil{
                if USERDEFAULT.value(forKey: "gender") as? String == "" {
                    if Reachability.isConnectedToNetwork() {
                        SVProgressHUD.show(withStatus: "Loading...")
                        self.postDataOnWebserviceForUpdateProfile()
                    }
                    else{
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
            }
            else{
                APPDELEGATE.healthIndexData.setValue(txtName.text, forKey: "nameIs")
                APPDELEGATE.healthIndexData.setValue(txtBirthDate.text, forKey: "birthDateIs")
                APPDELEGATE.healthIndexData.setValue(txtGender.text, forKey: "genderIs")
                let bloodPressureVC = HealthCalculatorHomeVC(nibName: "HealthCalculatorHomeVC", bundle: nil)
                self.navigationController?.pushViewController(bloodPressureVC, animated: true)
            }
            
        }
    }
    
    @IBAction func btnDatePickerSelectedClicked(_ sender:UIButton){
        self.view.endEditing(true)
        
        if txtBirthDate.text != "" {
            let str = txtBirthDate.text!
            if str != "0000-00-00"{
                let dateFormater = DateFormatter()
                dateFormater.dateFormat = "yyyy-MM-dd"
                var dateFromString = Date.init()
                dateFromString = dateFormater.date(from: str as String)!
                datePicker.setDate(dateFromString as Date, animated: true)
            }
        }
        
        datePicker.maximumDate = NSDate() as Date
        datePicker.datePickerMode = .date;
        
        
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewDatePicker.frame=CGRect(x: self.view.frame.size.width - self.viewDatePicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y - self.viewDatePicker.frame.size.height, width: self.viewDatePicker.frame.size.width, height: self.viewDatePicker.frame.size.height)
            
            
        })
        
        
    }
    
    @IBAction func cancelPicker(sender:AnyObject){
        UIView.animate(withDuration: 0.3) {
            self.viewDatePicker.frame=CGRect(x: self.view.frame.size.width - self.viewDatePicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewDatePicker.frame.size.width, height: self.viewDatePicker.frame.size.height)
        }
    }
    @IBAction func doneDatePicker(sender:UIButton){
        
        txtBirthDate.text = getStringDateFromDate(dateFormat: "yyyy-MM-dd", enterDate: datePicker.date)
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewDatePicker.frame=CGRect(x: self.view.frame.size.width - self.viewDatePicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewDatePicker.frame.size.width, height: self.viewDatePicker.frame.size.height)
        })
    }
    
    // TODO: - DELEGATE METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
            self.view.endEditing(true)
        }
        else
        {
            textField.resignFirstResponder()
        }
        return false
    }


    
    //MARK:-
    func setProfileData(profileData:NSMutableDictionary){
        print("Profile Data : " , profileData)
        
        if let fullname  = profileData.value(forKey: "fullname") as? String{
            txtName.text = "\(fullname)"
        }
        
        if let dateofBirth  = profileData.value(forKey: "dob") as? String{
            if dateofBirth.contains("0000") {
                txtBirthDate.text = ""
            }
            else{
            txtBirthDate.text = "\(dateofBirth)"
            }
        }
        
    }
    
   
    // MARK:- POST DATA METHODS
    func postDataOnWebserviceForGetProfile(){
        let completeURL = NSString(format:"%@%@", MainURL,getUserProfileURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetProfile API Parameter :",finalParams)
        print("GetProfile API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getUserProfileURLTag)
    }
    
    func postDataOnWebserviceForUpdateProfile(){
        let completeURL = NSString(format:"%@%@", MainURL,editUserProfileURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "gender":txtGender.text!,
            "fullname" : txtName.text!,
            "dob" : txtBirthDate.text!
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("UpdateProfile API Parameter :",finalParams)
        print("UpdateProfile API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: editUserProfileURLTag)
    }
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case getUserProfileURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("GETProfile Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                profileData = NSMutableDictionary(dictionary: resultDict.value(forKey: "data") as! NSDictionary)
                self.setProfileData(profileData: profileData)
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            SVProgressHUD.dismiss()
            break
            
        case editUserProfileURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("UpdateProfile Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                let dataDictionary = resultDict.value(forKey: "data") as! NSDictionary
                
                USERDEFAULT.setValue(dataDictionary.value(forKey: "fullname") as! String, forKey: "fullName")
                USERDEFAULT.synchronize()
                
                USERDEFAULT.setValue(dataDictionary.value(forKey: "dob") as! String, forKey: "dob")
                USERDEFAULT.synchronize()
                
                USERDEFAULT.setValue(txtGender.text, forKey: "gender")
                USERDEFAULT.synchronize()
                
                let bloodPressureVC = HealthCalculatorHomeVC(nibName: "HealthCalculatorHomeVC", bundle: nil)
                self.navigationController?.pushViewController(bloodPressureVC, animated: true)
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            
            SVProgressHUD.dismiss()
            break
            
            
        default:
            break
            
        }
    }
    
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case getUserProfileURLTag:
            SVProgressHUD.dismiss()
            break
            
        case editUserProfileURLTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    
    //MARK:- Alerts
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    
    //AlertDelegate Methods protocol
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }
    
}
