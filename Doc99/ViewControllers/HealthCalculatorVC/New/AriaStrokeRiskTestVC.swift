//
//  AriaStrokeRiskTestVC.swift
//  Doc99
//
//  Created by MS-SUB-02 on 21/09/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class AriaStrokeRiskTestVC: UIViewController {
    @IBOutlet var txtLeftPrameter:UITextField!
    @IBOutlet var txtRightParaMeter:UITextField!
    @IBOutlet var scrMain:UIScrollView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.NavBarNumberPad()
        self.addTapGestureInOurView()
        //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        //         NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //        viewBg.layer.borderWidth=0.5
        //        viewBg.layer.borderColor=UIColor.lightGray.cgColor
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.scrMain.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        if viewTouched!.isKind(of: UIButton.self){
            
        }
        else{
            self.view.endEditing(true)
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        }
    }
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSaveRecordClicked(_ sender: UIButton) {
        
        if txtLeftPrameter.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter left eye parameters", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if txtRightParaMeter.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter right eye parameters", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
            
        else{
            
            let bloodPressureVC = NewResultVC(nibName: "NewResultVC", bundle: nil)
            bloodPressureVC.isFromTwoValue = true
            bloodPressureVC.isFromAreaStoke=true
            bloodPressureVC.isFromBloodPressure = false
            bloodPressureVC.strFirst = String(format: "%@", txtLeftPrameter.text!) as NSString!
            bloodPressureVC.strSecound = txtRightParaMeter.text! as String as NSString!
            
            self.navigationController?.pushViewController(bloodPressureVC, animated: true)

//            if Reachability.isConnectedToNetwork() == true {
//                SVProgressHUD.show(withStatus: "Loading..")
//                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForBloodPresureTest), userInfo: nil, repeats: false)
//            } else {
//                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
//            }
        }
        
        
    }
    
    @IBAction func btnAriaStrokeTestInfo(_ sender: UIButton) {
        let ariaStrokeRiskTestInfoVC = AriaStrokeRiskTestInfoVC(nibName: "AriaStrokeRiskTestInfoVC", bundle: nil)
        self.navigationController?.pushViewController(ariaStrokeRiskTestInfoVC, animated: true)
    }
       
    
    //    //MARK:- Notification Methods
    //
    //    func keyboardDidShow(notification:NSNotification)
    //    {
    //        print("keyboard did show \(notification)")
    //
    //        guard let dic = notification.userInfo else
    //        {
    //            print("user info is nil")
    //            return
    //        }
    //
    //        let kbRect:CGRect = dic[UIKeyboardFrameEndUserInfoKey] as! CGRect
    //
    //        let contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbRect.size.height, 0.0)
    //        scrMain.contentInset = contentInsets
    //        scrMain.scrollIndicatorInsets = contentInsets
    //
    //        var aRect = self.view.frame
    //        aRect.size.height -= kbRect.size.height
    //
    //        if activeField != nil
    //        {
    //            if (!aRect.contains(activeField!.frame.origin))
    //            {
    //                scrMain.scrollRectToVisible((activeField!.frame), animated: true)
    //            }
    //        }
    //
    //    }
    //
    //    func keyboardWillBeHidden(notification:NSNotification)
    //    {
    //        print("keyboardWillBeHidden \(notification) )")
    //
    //        let contentInsets = UIEdgeInsets.zero
    //        scrMain.contentInset = contentInsets
    //        scrMain.scrollIndicatorInsets = contentInsets
    //    }
    // TODO: - DELEGATE METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
            
        }
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var YOffset:Int = 0
        
//        if (textField.tag==1) {
//            YOffset=130
//
//        }else if (textField.tag==2){
//            YOffset=95
//        }
        
        if (DeviceType.IS_IPHONE_4_OR_LESS) {
            
            if (textField.tag==1) {
                YOffset=0
            }else if (textField.tag==2){
                YOffset=0
            }else if (textField.tag==3){
                YOffset=5
            }
            
        }
        scrMain.setContentOffset(CGPoint(x: 0, y: CGFloat(textField.tag * YOffset)), animated: true)
        return true
        
    }
    
    
    
    
    // Numeric Keyboard Hide Method
    func NavBarNumberPad(){
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        numberToolbar.barStyle = .default
        
        let viewBack = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        viewBack.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        numberToolbar.addSubview(viewBack)
        
        let btnCancel = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelNumberPad))
        btnCancel.tintColor = UIColor.white
        
        let btnDone = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.doneWithNumberPad))
        btnDone.tintColor = UIColor.white
        
        numberToolbar.items = NSArray(objects: btnCancel,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),btnDone) as? [UIBarButtonItem]
        
        
        numberToolbar.sizeToFit()
        txtLeftPrameter.inputAccessoryView = numberToolbar
        txtRightParaMeter.inputAccessoryView = numberToolbar
        
    }
    
    func cancelNumberPad(){
        txtLeftPrameter.resignFirstResponder()
        txtRightParaMeter.resignFirstResponder()
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
        
    }
    
    func doneWithNumberPad(){
        txtLeftPrameter.resignFirstResponder()
        txtRightParaMeter.resignFirstResponder()
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
    }
    
    
    
    
}
