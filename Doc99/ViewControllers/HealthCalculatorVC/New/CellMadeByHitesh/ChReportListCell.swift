//
//  ChReportListCell.swift
//  Doc99
//
//  Created by Hitesh Prajapati on 28/11/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class ChReportListCell: UITableViewCell {

    
    @IBOutlet var lblDate: UILabel!
    
    @IBOutlet var lblReport1: UILabel!
    @IBOutlet var lblReport2: UILabel!
    @IBOutlet var lblReport3: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
