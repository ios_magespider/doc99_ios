//
//  BloodGlucoseTestVC.swift
//  Doc99
//
//  Created by MS-SUB-02 on 27/09/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class BloodGlucoseTestVC: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UIAlertViewDelegate,UITextFieldDelegate {

    @IBOutlet var viewBg:UIView!
    @IBOutlet var txtParameter:UITextField!
    @IBOutlet var txtHbA1C:UITextField!
    @IBOutlet var txtOption:UITextField!
    @IBOutlet var scrMain:UIScrollView!

    
    @IBOutlet var viewPicker:UIView!
    @IBOutlet var pickerView:UIPickerView!
    var pickerSelIndx:Int!
    var pickOption:NSMutableArray!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.generalViewControllerSetting()
        
        //        viewBg.layer.borderWidth=0.5
        //        viewBg.layer.borderColor=UIColor.lightGray.cgColor
        // Do any additional setup after loading the view.
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        
        pickerSelIndx=0
        pickOption = NSMutableArray()
        pickOption = ["Fasting Blood Glucose","Postprandial 1 Hour","Postprandial 2 Hour"]
        pickerView.reloadAllComponents()
        viewPicker.frame = CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: viewPicker.frame.size.width, height: viewPicker.frame.size.height)
        
      //  self.addTapGestureInOurView()
        self.NavBarNumberPad()
    }
    
    
//    func addTapGestureInOurView(){
//        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
//        tapRecognizer.cancelsTouchesInView = false
//        self.scrMain.addGestureRecognizer(tapRecognizer)
//    }
//    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
//        let point:CGPoint = sender.location(in: sender.view)
//        let viewTouched = view.hitTest(point, with: nil)
//        
//        if viewTouched!.isKind(of: UIButton.self){
//            
//        }
//        else{
//            self.view.endEditing(true)
//            
//            let nextResponder1 = UIButton.superclass()
//            if (nextResponder1 != nil)
//            {
//                                                       UIView.animate(withDuration: 0.3, animations: {
//                            
//                            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y - self.viewPicker.frame.size.height, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
//                            
//                            
//                        })
//            }
//            else
//            {
//
//            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
//            }
//        }
//    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(txtHbA1C == textField)
        {
            let aSet = NSCharacterSet(charactersIn:"0123456789-+.").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }
        return true
    }
    
    // MARK: - PICKERVIEW DELEGATE
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickOption.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        // return (self.pickOption[row] as AnyObject).value(forKey: "title") as? String
        return self.pickOption[row] as? String
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50.0
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let label = UILabel()
        label.textAlignment = NSTextAlignment.center
        
        label.font = UIFont.systemFont(ofSize: 20.0)
        label.text = self.pickOption[row] as? String
        return label
    }
    
    
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSaveRecordClicked(_ sender: UIButton) {
        
        /*if txtHbA1C.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter HbA1C", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else*/ if txtOption.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter option", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if txtParameter.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter parameter", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
            
        else{
            
            
            let bloodPressureVC = NewResultVC(nibName: "NewResultVC", bundle: nil)
            bloodPressureVC.isFromTwoValue = true
            bloodPressureVC.isFromBloodPressure = false
            bloodPressureVC.isFromAreaStoke = false
            bloodPressureVC.strFirst = String(format: "%@", txtHbA1C.text!) as NSString!
            bloodPressureVC.strSecound = txtParameter.text! as String as NSString!
            bloodPressureVC.strThird = txtOption.text! as String as NSString!

            self.navigationController?.pushViewController(bloodPressureVC, animated: true)

//            if Reachability.isConnectedToNetwork() == true {
//                SVProgressHUD.show(withStatus: "Loading..")
//                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForBloodPresureTest), userInfo: nil, repeats: false)
//            } else {
//                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
//            }
        }

        
    }
    @IBAction func btnSelectOptionClicked(_ sender: UIButton) {
                   self.view.endEditing(true)

//        NSLog("%@" , scrMain.contentOffset.y)
//        if scrMain.contentOffset.y < 75
//        {
//            scrMain.setContentOffset(CGPoint(x: 0, y: CGFloat(1 * 75)), animated: true)

        //}
//        else
//        {
//        scrMain.setContentOffset(CGPoint(x: 0, y:0), animated: true)
//        }

        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y - self.viewPicker.frame.size.height, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
            
        })
    }
    @IBAction func cancelPicker(sender:AnyObject){
        UIView.animate(withDuration: 0.3) {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
        }
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)

        
    }
    
    @IBAction func donePicker(sender:UIButton){
        
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)

        pickerSelIndx = pickerView.selectedRow(inComponent: 0)
        
        print("Picker Index is  :-> \(pickerSelIndx) and value is \(pickOption[pickerSelIndx])")
        txtOption.text = self.pickOption[pickerSelIndx] as? String
        //dishID = ((self.dishData[pickerSelIndx] as AnyObject).value(forKey: "_id") as? String)!
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
        })
    }
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForBloodPresureTest(){
        let completeURL = NSString(format:"%@%@", MainURL,bloodGlucoseTestDataURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "hba1c":txtHbA1C.text as Any,
            "option":txtOption.text as Any,
            "param":txtParameter.text as Any,
            "hba1c_report":"20",
            "option_report":"30"
        ]
        
        
        
        
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("editWeightData API Parameter :",finalParams)
        print("editWeightData API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: bloodGlucoseTestURLTag)
        
    }
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case bloodGlucoseTestURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("editWeightData Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                
                let bloodPressureVC = ResultVC(nibName: "ResultVC", bundle: nil)
                bloodPressureVC.isFromTwoValue = true
                bloodPressureVC.isFromBloodPressure = false
                bloodPressureVC.strFirst = String(format: "%@", txtHbA1C.text!) as NSString!
                bloodPressureVC.strSecound = txtParameter.text! as String as NSString!

                self.navigationController?.pushViewController(bloodPressureVC, animated: true)
                
                //
                //                self.weightProgramID = (resultDict.value(forKey: "data") as AnyObject).value(forKey: "wp_id") as! String
                //
                //                let notificationName = Notification.Name("refreshMyWeightProgramData")
                //                NotificationCenter.default.post(name: notificationName, object: nil)
                //
                //                self.performSelector(inBackground: #selector(self.postDataOnWebserviceForGetSingleWeightProgram), with: nil)
                //
                //                self.navigationController?.popViewController(animated: true)
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
                
            else if resultDict.value(forKey: "status") as! String == "2"{
                //                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            
            SVProgressHUD.dismiss()
            break
            
            
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case bloodGlucoseTestURLTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    // TODO: - DELEGATE METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            
            
            let nextResponder1 = UIButton.superclass()
            if (nextResponder1 != nil) 
            {
                if (textField.tag==1){
                    
                    UIView.animate(withDuration: 0.3, animations: {
                        
                        self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y - self.viewPicker.frame.size.height, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
                        
                        
                    })

            }
            }
            else
            {
            
            nextResponder?.becomeFirstResponder()
            }
        }
        else
        {
            textField.resignFirstResponder()
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
            
        }
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var YOffset:Int = 0
        /*
        if (textField.tag==1) {
            YOffset=75
        }else if (textField.tag==2){
            YOffset=70
        }else if (textField.tag==3){
            YOffset=70
        }
        */
        
        if (DeviceType.IS_IPHONE_4_OR_LESS) {
            
            if (textField.tag==1) {
                YOffset=0
            }else if (textField.tag==2){
                YOffset=0
            }else if (textField.tag==3){
                YOffset=5
            }
    
        }
        scrMain.setContentOffset(CGPoint(x: 0, y: CGFloat(textField.tag * YOffset)), animated: true)
        return true
        
    }
    

    // Numeric Keyboard Hide Method
    func NavBarNumberPad(){
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        numberToolbar.barStyle = .default
        
        let viewBack = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        viewBack.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        numberToolbar.addSubview(viewBack)
        
        let btnCancel = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelNumberPad))
        btnCancel.tintColor = UIColor.white
        
        let btnDone = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.doneWithNumberPad))
        btnDone.tintColor = UIColor.white
        
        numberToolbar.items = NSArray(objects: btnCancel,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),btnDone) as? [UIBarButtonItem]
        
        
        numberToolbar.sizeToFit()
        txtHbA1C.inputAccessoryView = numberToolbar
        txtParameter.inputAccessoryView = numberToolbar
        
    }
    
    func cancelNumberPad(){
        txtHbA1C.resignFirstResponder()
        txtParameter.resignFirstResponder()
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
        
    }
    
    func doneWithNumberPad(){
        txtHbA1C.resignFirstResponder()
        txtParameter.resignFirstResponder()
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
    }
    
    
 
    
}
