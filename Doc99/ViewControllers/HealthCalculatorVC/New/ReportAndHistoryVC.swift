//
//  ReportAndHistoryVC.swift
//  Doc99
//
//  Created by MS-SUB-02 on 21/09/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit



class ReportAndHistoryVC: UIViewController , UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITableViewDataSource, UITableViewDelegate{
    @IBOutlet weak var objCollectionView: UICollectionView!
    
    
    var arrMenu: NSArray = [[String:String]]() as NSArray

    var _sizingCell: ReportHistoryCollectionCell!

    var pageNum1:Int!
    var isLoading1:Bool?
    var arrBpText: NSMutableArray!

    @IBOutlet var tableViewReport:UITableView!

    @IBOutlet var lblNoReport: UILabel!
    var strCollectionSelected: NSString!

    var selectedIndex = 0
    
    //var pageNum1:Int!
    //var isLoading1:Bool?

   // var arrBloodGlucoseTestData: NSMutableArray!

    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let nib = UINib(nibName: "ReportHistoryCollectionCell", bundle: nil)
        objCollectionView.register(nib, forCellWithReuseIdentifier: "reportcollectionMenuCell")
        _sizingCell = nib.instantiate(withOwner: nil, options: nil)[0] as! ReportHistoryCollectionCell
        

        strCollectionSelected = "BP Test"
        pageNum1=1;
        arrBpText = NSMutableArray()
        
        APPDELEGATE.myTabBarController?.tabBar.isHidden=true
        

        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetHealthReport), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }

        
        arrMenu = [["name": "BP Test", "image": "1.png", "isSelected": "YES"],
                   ["name": "Blood Glucose Test", "image": "2.png" , "isSelected": "NO"],
                   ["name": "Uric Acid Test", "image": "3.png", "isSelected": "NO"] ,
                   ["name": "Intra Ocular Pressure", "image": "4.png", "isSelected": "NO"],
                   ["name": "Aria Stroke Risk", "image": "5.png", "isSelected": "NO"],
                   ["name": "BMI and OST", "image": "6.png", "isSelected": "NO"],
                   ["name": "Cholesterol Test", "image": "9.png", "isSelected": "NO"],
                   ["name": "SOA", "image": "10.png", "isSelected": "NO"],
                   ["name": "Waist Hip Ratio", "image": "11.png", "isSelected": "NO"],
                   ["name": "Pulmonary", "image": "12.png", "isSelected": "NO"],
                   ["name": "Child Growth", "image": "12.png", "isSelected": "NO"]
        ]
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-
    
    func addLoadingIndicatiorOnFooterOnTableView(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableViewReport.tableFooterView = spinner
    }
    func removeLoadingIndicatiorOnFooterOnTableView(){
        tableViewReport.tableFooterView = nil
    }
    

    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width:100, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMenu.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ReportHistoryCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier:"reportcollectionMenuCell", for: indexPath as IndexPath) as! ReportHistoryCollectionCell
        
        
        //let arr1 = arrMenu[indexPath.row]
        cell.lblNameForSliderMenu.text = ((arrMenu.value(forKey: "name") as! NSArray).object(at: indexPath.row) as! NSString) as String
        
//        NSLog("%@",((arrMenu.value(forKey: "name") as! NSArray).object(at: indexPath.row) as! NSString) as String)
//        NSLog("%@", strCollectionSelected)
//        NSLog("%@", ((arrMenu.value(forKey: "isSelected") as! NSArray).object(at: indexPath.row) as! NSString) as String)

        if ((arrMenu.value(forKey: "isSelected") as! NSArray).object(at: indexPath.row) as! NSString) as String == ("YES")

        {
            cell.lblNameForSliderMenu.textColor = UIColor.init(red: 85/255.0, green: 198/255.0, blue: 227/255.0, alpha: 1)
            
        }
        else
        {
            cell.lblNameForSliderMenu.textColor = UIColor.darkGray
        }
        return cell
    }
    
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedIndex = indexPath.item
        
        var arr = [[String:String]]()
        var arr1 = [[String:String]]()

        
        for index in 0 ... arrMenu.count-1
        {
            arr = arrMenu as! [[String : String]]
            var nameChange = arr[index]
            
            if index == indexPath.row
            {
                nameChange["isSelected"] = "YES"
            }
            else
            {
                nameChange["isSelected"] = "NO"
            }
            arr1.append(nameChange)
        }
        arrMenu = arr1 as NSArray

        strCollectionSelected = (arrMenu.value(forKey: "name") as! NSArray).object(at: indexPath.row) as! NSString
        
        
        arrBpText = NSMutableArray()
        self.tableViewReport.reloadData()
        if indexPath.row == 0
        {
            pageNum1=1;
            
            if Reachability.isConnectedToNetwork() == true {
                self.addLoadingIndicatiorOnFooterOnTableView()
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetHealthReport
                    ), userInfo: nil, repeats: false)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }

        }
        else if indexPath.row == 1
        {
            
            pageNum1=1;
          //  arrBpText = NSMutableArray()

            if Reachability.isConnectedToNetwork() == true {
                self.addLoadingIndicatiorOnFooterOnTableView()
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForBloodGlucoseTestReport), userInfo: nil, repeats: false)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            

        }
        else if indexPath.row == 2
        {
            pageNum1=1;
            //  arrBpText = NSMutableArray()
            
            if Reachability.isConnectedToNetwork() == true {
                self.addLoadingIndicatiorOnFooterOnTableView()
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForUricAcidTestReport), userInfo: nil, repeats: false)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }

            
        }
        else if indexPath.row == 3
        {
            pageNum1=1;
            //  arrBpText = NSMutableArray()
            
            if Reachability.isConnectedToNetwork() == true {
                self.addLoadingIndicatiorOnFooterOnTableView()
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForIntraOcularPressureTestReportReport), userInfo: nil, repeats: false)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }

        }
        else if indexPath.row == 4
        {
            pageNum1=1;
            //  arrBpText = NSMutableArray()
            
            if Reachability.isConnectedToNetwork() == true {
                self.addLoadingIndicatiorOnFooterOnTableView()
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForAriaStrokeDataReport), userInfo: nil, repeats: false)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }
        else if indexPath.row == 5
        {
            pageNum1=1;
            //  arrBpText = NSMutableArray()
            
            if Reachability.isConnectedToNetwork() == true {
                self.addLoadingIndicatiorOnFooterOnTableView()
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForOsteoporosisAssetmentToolTestReport), userInfo: nil, repeats: false)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }
        else if indexPath.row == 6
        {
            //Cholesterol
            
            pageNum1=1;
            //  arrBpText = NSMutableArray()
            
            if Reachability.isConnectedToNetwork() == true {
                self.addLoadingIndicatiorOnFooterOnTableView()
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataGetCholesterolTestReport), userInfo: nil, repeats: false)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }
        else if indexPath.row == 7
        {
            //SOA
            if Reachability.isConnectedToNetwork() == true{
                pageNum1 = 1
                self.addLoadingIndicatiorOnFooterOnTableView()
                Timer.scheduledTimer(timeInterval: 0.1, target: self , selector: #selector(self.postDataGetSOATestReport), userInfo: nil, repeats: false)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }

        }
        else if indexPath.row == 8
        {
            //Waist Hip Ratio
            if Reachability.isConnectedToNetwork() == true{
                pageNum1 = 1
                self.addLoadingIndicatiorOnFooterOnTableView()
                Timer.scheduledTimer(timeInterval: 0.1, target: self , selector: #selector(self.postDataGetWaistHipTestReport), userInfo: nil, repeats: false)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }
        else if indexPath.row == 9
        {
            //Pulmonary
            
            if Reachability.isConnectedToNetwork() == true{
                pageNum1 = 1
                self.addLoadingIndicatiorOnFooterOnTableView()
                Timer.scheduledTimer(timeInterval: 0.1, target: self , selector: #selector(self.postDataGetPulmonaryTestReport), userInfo: nil, repeats: false)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }
        else if indexPath.row == 10{
            //Child Growth Curve
            if Reachability.isConnectedToNetwork() == true{
                pageNum1 = 1
                self.addLoadingIndicatiorOnFooterOnTableView()
                Timer.scheduledTimer(timeInterval: 0.1, target: self , selector: #selector(self.postDataGetChildGrowthTestReport), userInfo: nil, repeats: false)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }
        collectionView.reloadData()

    }
    
    

    // TODO: - DELEGATE METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrBpText.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if strCollectionSelected == "BP Test"{
            //----------------------
            let identifier = "reportListCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ReportListCell
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("ReportListCell", owner: self, options: nil)
                cell = nib?[0] as? ReportListCell
            }
            cell!.selectionStyle = .none;
            //----------------------
            if let reportDate =  (arrBpText[indexPath.row] as AnyObject).value(forKey: "created_at") as? String
            {
                
                let report = "\(reportDate)"
                let a =  getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: report, newFormat: "dd MMM yyyy")
                cell?.lblDate.text = "\(a)"


                
            }
            if let report1 =  (arrBpText[indexPath.row] as AnyObject).value(forKey: "diastolic") as? String
            {
                
                cell?.lblReport1.text = "\(report1) mmHg"
            }
            if let report2 =  (arrBpText[indexPath.row] as AnyObject).value(forKey: "systolic") as? String
            {
                
                cell?.lblReport2.text = "\(report2) mmHg"
            }
            return cell!
        }
        else if strCollectionSelected == "Blood Glucose Test"{
            //----------------------
            let identifier = "reportListCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ReportListCell
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("ReportListCell", owner: self, options: nil)
                cell = nib?[0] as? ReportListCell
            }
            cell!.selectionStyle = .none;
            //----------------------
            if let reportDate =  (arrBpText[indexPath.row] as AnyObject).value(forKey: "created_at") as? String
            {
                let report = "\(reportDate)"
                let a =  getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: report, newFormat: "dd MMM yyyy")
                cell?.lblDate.text = "\(a)"
               // cell?.lblDate.text = "\(report)"
            }
            if let report1 =  (arrBpText[indexPath.row] as AnyObject).value(forKey: "hba1c_report") as? String
            {
                cell?.lblReport2.text = "\(report1) mg/dL"
            }
            return cell!
        }
        else if strCollectionSelected == "Uric Acid Test"{
            //----------------------
            let identifier = "reportListCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ReportListCell
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("ReportListCell", owner: self, options: nil)
                cell = nib?[0] as? ReportListCell
            }
            cell!.selectionStyle = .none;
            //----------------------
            if let reportDate =  (arrBpText[indexPath.row] as AnyObject).value(forKey: "created_at") as? String
            {
                let report = "\(reportDate)"
                let a =  getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: report, newFormat: "dd MMM yyyy")
                cell?.lblDate.text = "\(a)"
            }
            if let report1 =  (arrBpText[indexPath.row] as AnyObject).value(forKey: "uat_param") as? String
            {
                cell?.lblReport2.text = "\(report1)"
            }
            return cell!
        }
        else if strCollectionSelected == "Intra Ocular Pressure"{
            //----------------------
            let identifier = "reportListCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ReportListCell
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("ReportListCell", owner: self, options: nil)
                cell = nib?[0] as? ReportListCell
            }
            cell!.selectionStyle = .none;
            //----------------------
            if let reportDate =  (arrBpText[indexPath.row] as AnyObject).value(forKey: "created_at") as? String
            {
                let report = "\(reportDate)"
                let a =  getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: report, newFormat: "dd MMM yyyy")
                cell?.lblDate.text = "\(a)"
                
                
                // cell?.lblDate.text = "\(report)"
            }
            if let report1 =  (arrBpText[indexPath.row] as AnyObject).value(forKey: "iop") as? String
            {
                cell?.lblReport2.text = "\(report1) mmHg"
            }
            return cell!
        }
        else if strCollectionSelected == "Aria Stroke Risk"{
            //----------------------
            let identifier = "reportListCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ReportListCell
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("ReportListCell", owner: self, options: nil)
                cell = nib?[0] as? ReportListCell
            }
            cell!.selectionStyle = .none;
            //----------------------
            if let reportDate =  (arrBpText[indexPath.row] as AnyObject).value(forKey: "created_at") as? String
            {
                let report = "\(reportDate)"
                let a =  getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: report, newFormat: "dd MMM yyyy")
                cell?.lblDate.text = "\(a)"
                
                
                // cell?.lblDate.text = "\(report)"
            }
            if let report1 =  (arrBpText[indexPath.row] as AnyObject).value(forKey: "left_eye") as? String
            {
                cell?.lblReport2.text = "\(report1)"
            }
            if let report2 =  (arrBpText[indexPath.row] as AnyObject).value(forKey: "right_eye") as? String
            {
                cell?.lblReport1.text = "\(report2)"
            }
            return cell!
        }
        else if strCollectionSelected == "BMI and OST"{
            //----------------------
            let identifier = "reportListCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ReportListCell
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("ReportListCell", owner: self, options: nil)
                cell = nib?[0] as? ReportListCell
            }
            cell!.selectionStyle = .none;
            //----------------------
            if let reportDate =  (arrBpText[indexPath.row] as AnyObject).value(forKey: "created_at") as? String
            {
                let report = "\(reportDate)"
                let a =  getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: report, newFormat: "dd MMM yyyy")
                cell?.lblDate.text = "\(a)"
                
                
                // cell?.lblDate.text = "\(report)"
            }
            if let report1 =  (arrBpText[indexPath.row] as AnyObject).value(forKey: "weight") as? String
            {
                
                let WeightInUnit = (arrBpText[indexPath.row] as AnyObject).value(forKey: "weight_in") as! String
                cell?.lblReport2.text = "\(report1) \(WeightInUnit)"
                
            }
            return cell!
        }
        else if strCollectionSelected == "Cholesterol Test"
        {
            let identifier = "chReportListCell"     //chReportListCell
            
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ChReportListCell
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("ChReportListCell", owner: self, options: nil)
                cell = nib?[0] as? ChReportListCell
            }
            cell!.selectionStyle = .none;
            
            
            if let reportDate =  (arrBpText[indexPath.row] as AnyObject).value(forKey: "created_at") as? String
            {
                let report = "\(reportDate)"
                let a =  getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: report, newFormat: "dd MMM yyyy")
                cell?.lblDate.text = "\(a)"
                
                
                // cell?.lblDate.text = "\(report)"
            }
            
            cell?.lblReport1.text = (arrBpText[indexPath.row] as AnyObject).value(forKey: "triglyceride_param") as? String
            cell?.lblReport2.text = (arrBpText[indexPath.row] as AnyObject).value(forKey: "hdl_result") as? String
            cell?.lblReport3.text = (arrBpText[indexPath.row] as AnyObject).value(forKey: "ldl_param") as? String
            
            return cell!
        }
        else if strCollectionSelected == "SOA"
        {
            //----------------------
            let identifier = "reportListCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ReportListCell
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("ReportListCell", owner: self, options: nil)
                cell = nib?[0] as? ReportListCell
            }
            cell!.selectionStyle = .none;
            //----------------------
            if let reportDate =  (arrBpText[indexPath.row] as AnyObject).value(forKey: "created_at") as? String
            {
                let report = "\(reportDate)"
                let a =  getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: report, newFormat: "dd MMM yyyy")
                cell?.lblDate.text = "\(a)"
            }
            if let report1 =  (arrBpText[indexPath.row] as AnyObject).value(forKey: "pulse_oximetry_param") as? String
            {
                cell?.lblReport2.text = "\(report1)"
            }
            return cell!
        }
        else if strCollectionSelected == "Waist Hip Ratio"
        {
            //----------------------
            let identifier = "reportListCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ReportListCell
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("ReportListCell", owner: self, options: nil)
                cell = nib?[0] as? ReportListCell
            }
            cell!.selectionStyle = .none;
            //----------------------
            if let reportDate =  (arrBpText[indexPath.row] as AnyObject).value(forKey: "created_at") as? String
            {
                let report = "\(reportDate)"
                let a =  getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: report, newFormat: "dd MMM yyyy")
                cell?.lblDate.text = "\(a)"
            }
//            if let report1 =  (arrBpText[indexPath.row] as AnyObject).value(forKey: "waist_hip_result") as? String
//            {
//                cell?.lblReport2.text = "\(report1)"
//            }
            cell?.lblReport1.text = (arrBpText[indexPath.row] as AnyObject).value(forKey: "hip_circumference") as? String
            cell?.lblReport2.text = (arrBpText[indexPath.row] as AnyObject).value(forKey: "waist_circumference") as? String
            
            return cell!
        }
        else if strCollectionSelected == "Pulmonary"
        {
            let identifier = "chReportListCell"     //chReportListCell
            
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ChReportListCell
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("ChReportListCell", owner: self, options: nil)
                cell = nib?[0] as? ChReportListCell
            }
            cell!.selectionStyle = .none;
            
            
            if let reportDate =  (arrBpText[indexPath.row] as AnyObject).value(forKey: "created_at") as? String
            {
                let report = "\(reportDate)"
                let a =  getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: report, newFormat: "dd MMM yyyy")
                cell?.lblDate.text = "\(a)"
                
                
                // cell?.lblDate.text = "\(report)"
            }
            
            cell?.lblReport1.text = (arrBpText[indexPath.row] as AnyObject).value(forKey: "fev1") as? String
            cell?.lblReport2.text = (arrBpText[indexPath.row] as AnyObject).value(forKey: "fvc") as? String
            cell?.lblReport3.text = (arrBpText[indexPath.row] as AnyObject).value(forKey: "pefr") as? String
            
            return cell!
        }
        else if strCollectionSelected == "Child Growth"{
            //----------------------
            let identifier = "reportListCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ReportListCell
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("ReportListCell", owner: self, options: nil)
                cell = nib?[0] as? ReportListCell
            }
            cell!.selectionStyle = .none;
            //----------------------
            if let reportDate =  (arrBpText[indexPath.row] as AnyObject).value(forKey: "created_at") as? String
            {
                let report = "\(reportDate)"
                let a =  getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: report, newFormat: "dd MMM yyyy")
                cell?.lblDate.text = "\(a)"
                // cell?.lblDate.text = "\(report)"
            }
            if let report1 =  (arrBpText[indexPath.row] as AnyObject).value(forKey: "report") as? String
            {
                cell?.lblReport2.text = "\(report1) cm"
            }
            
            return cell!
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier")! as UITableViewCell
            return cell
        }
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        if selectedIndex == 0{
            //BP Test
            
            let bloodPressureVC = NewResultVC(nibName: "NewResultVC", bundle: nil)
            bloodPressureVC.isFromReport = true
            bloodPressureVC.isFromTwoValue = true
            bloodPressureVC.isFromBloodPressure = true
            bloodPressureVC.strFirst = (arrBpText[indexPath.row] as AnyObject).value(forKey: "systolic") as? NSString
            bloodPressureVC.strSecound = (arrBpText[indexPath.row] as AnyObject).value(forKey: "diastolic") as? NSString
            bloodPressureVC.strThird = (arrBpText[indexPath.row] as AnyObject).value(forKey: "heart_rate") as? NSString
            let date = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "created_at")as? String ?? "", newFormat: "yyyy/MM/dd")
            bloodPressureVC.reportDate = date
            
            self.navigationController?.pushViewController(bloodPressureVC, animated: true)
            
        }
        else if selectedIndex == 1{
            //Blood glucose test
            
            let bloodPressureVC = NewResultVC(nibName: "NewResultVC", bundle: nil)
            bloodPressureVC.isFromReport = true
            bloodPressureVC.isFromTwoValue = true
            bloodPressureVC.isFromBloodPressure = false
            bloodPressureVC.isFromAreaStoke = false
            bloodPressureVC.strFirst = (arrBpText[indexPath.row] as AnyObject).value(forKey: "hba1c") as? NSString
            bloodPressureVC.strSecound = (arrBpText[indexPath.row] as AnyObject).value(forKey: "param") as? NSString
            
            let opt = (arrBpText[indexPath.row] as AnyObject).value(forKey: "option") as? String ?? ""
            var option = ""
            if opt == "" || opt == "0" || opt == "1"{
                option = "Fasting Blood Glucose"
            }
            else if opt == "2"{
                option = "Postprandial 1 Hour"
            }
            else if opt == "3"{
                option = "Postprandial 2 Hour"
            }
            bloodPressureVC.strThird = option as NSString
            
            
            let date = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "created_at")as? String ?? "", newFormat: "yyyy/MM/dd")
            bloodPressureVC.reportDate = date
            
            
            self.navigationController?.pushViewController(bloodPressureVC, animated: true)
            
        }
        else if selectedIndex == 2{
            //Uric acid test
            
            let bloodPressureVC = NewResultVC(nibName: "NewResultVC", bundle: nil)
            bloodPressureVC.isFromReport = true
            bloodPressureVC.isFromTwoValue = false
            bloodPressureVC.strFirst = (arrBpText[indexPath.row] as AnyObject).value(forKey: "uat_param") as? NSString
            bloodPressureVC.isFromIntraOcular = false
            bloodPressureVC.isFromUricAcid = true
            
            let date = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "created_at")as? String ?? "", newFormat: "yyyy/MM/dd")
            bloodPressureVC.reportDate = date
            
            self.navigationController?.pushViewController(bloodPressureVC, animated: true)
        }
        else if selectedIndex == 3{
            //Intra ocular pressure
            
            let bloodPressureVC = NewResultVC(nibName: "NewResultVC", bundle: nil)
            bloodPressureVC.isFromReport = true
            bloodPressureVC.isFromTwoValue = false
            bloodPressureVC.strFirst = (arrBpText[indexPath.row] as AnyObject).value(forKey: "iop") as? NSString
            bloodPressureVC.isFromIntraOcular = true
            
            let date = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "created_at")as? String ?? "", newFormat: "yyyy/MM/dd")
            bloodPressureVC.reportDate = date
            
            self.navigationController?.pushViewController(bloodPressureVC, animated: true)
        }
        else if selectedIndex == 4{
            //Aria stroke
            
            let bloodPressureVC = NewResultVC(nibName: "NewResultVC", bundle: nil)
            bloodPressureVC.isFromReport = true
            bloodPressureVC.isFromTwoValue = true
            bloodPressureVC.isFromAreaStoke=true
            bloodPressureVC.isFromBloodPressure = false
            bloodPressureVC.strFirst = (arrBpText[indexPath.row] as AnyObject).value(forKey: "left_eye") as? NSString
            bloodPressureVC.strSecound = (arrBpText[indexPath.row] as AnyObject).value(forKey: "right_eye") as? NSString
            
            
            let date = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "created_at")as? String ?? "", newFormat: "yyyy/MM/dd")
            bloodPressureVC.reportDate = date
            
            
            self.navigationController?.pushViewController(bloodPressureVC, animated: true)
        }
        else if selectedIndex == 5{
            let bloodPressureVC = NewResultVC(nibName: "NewResultVC", bundle: nil)
            bloodPressureVC.isFromReport = true
            bloodPressureVC.isFromTwoValue = false
            bloodPressureVC.isFromUricAcid = false
            bloodPressureVC.isFromIntraOcular = false
            bloodPressureVC.strFirst = "asdf"
            
            bloodPressureVC.strSecound = (arrBpText[indexPath.row] as AnyObject).value(forKey: "weight") as? NSString
            bloodPressureVC.strThird = (arrBpText[indexPath.row] as AnyObject).value(forKey: "ost") as? NSString
            
            let date = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "created_at")as? String ?? "", newFormat: "yyyy/MM/dd")
            bloodPressureVC.reportDate = date
            
            self.navigationController?.pushViewController(bloodPressureVC, animated: true)
        }
        else if selectedIndex == 6{
            let chResult = CholesterolTestResultVC(nibName: "CholesterolTestResultVC", bundle: nil)
            chResult.isFromReport = true
            chResult.strTriglyceride = (arrBpText[indexPath.row] as AnyObject).value(forKey: "triglyceride_param") as? NSString
            chResult.strTotalCholesterol = (arrBpText[indexPath.row] as AnyObject).value(forKey: "total_param") as? NSString
            chResult.strHdlCholesterol = (arrBpText[indexPath.row] as AnyObject).value(forKey: "hdl_param") as? NSString
            chResult.strLdlCholesterol = (arrBpText[indexPath.row] as AnyObject).value(forKey: "ldl_param") as? NSString
            let date = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "created_at")as? String ?? "", newFormat: "yyyy/MM/dd")
            chResult.reportDate = date
            self.navigationController?.pushViewController(chResult, animated: true)
        }
        else if selectedIndex == 7{
            //SOA
            let chResult = SoaTestResultVC(nibName: "SoaTestResultVC", bundle: nil)
            chResult.isFromReport = true
            chResult.strSoaParam = (arrBpText[indexPath.row] as AnyObject).value(forKey: "pulse_oximetry_param") as? NSString
            let date = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "created_at")as? String ?? "", newFormat: "yyyy/MM/dd")
            chResult.reportDate = date
            self.navigationController?.pushViewController(chResult, animated: true)
        }
        else if selectedIndex == 8{
            //Waist hip
            
            let chResult = WaistTestResultVC(nibName: "WaistTestResultVC", bundle: nil)
            chResult.isFromReport = true
            chResult.strWaistParam = (arrBpText[indexPath.row] as AnyObject).value(forKey: "waist_circumference") as? NSString
            chResult.strHipParam = (arrBpText[indexPath.row] as AnyObject).value(forKey: "hip_circumference") as? NSString
            let date = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "created_at")as? String ?? "", newFormat: "yyyy/MM/dd")
            chResult.reportDate = date
            self.navigationController?.pushViewController(chResult, animated: true)
        }
        else if selectedIndex == 9{
            //Pulmonary
            var exFvc: Double = 0
            var exFev1: Double = 0
            var exPef: Double = 0
            
            let height = (arrBpText[indexPath.row] as AnyObject).value(forKey: "height") as? String
            
            if APPDELEGATE.healthIndexData.value(forKey: "genderIs")  != nil
            {
                let age:Double = Double(self.calculateAge(dob: APPDELEGATE.healthIndexData.value(forKey: "birthDateIs") as! String))
                let strGender = APPDELEGATE.healthIndexData.value(forKey: "genderIs") as! String
                
                let ht = Double(height!)
                
                
                if  strGender == "Male"
                {
                    if (age < 25)
                    {
                        exFvc = -5.508+0.05*ht!+0.078*age;
                        exFev1 = -4.808+0.046*ht!+0.045*age;
                    }
                    else   // Knudson-males-age>= 25
                    {
                        exFvc = -5.459+0.065*ht!-0.029*age;
                        exFev1 = -4.203+0.052*ht!-0.027*age;
                    }
                    
                    if (age < 15)
                    {
                        exPef=455 * (ht!/100) - 332
                        
                    }
                    else   //males age>= 15
                    {
                        exPef = exp((0.544 * log(age))-(0.0151*age)-(74.7 / ht!)+5.48);  // For Male
                    }
                    print("PEF --> \(exPef)")
                }
                else  if strGender == "Female"
                {
                    if (age < 20)
                    {
                        exFvc = -3.469+ht!*0.033+age*0.092;
                        exFev1 = -2.703+ht!*0.027+age*0.085;
                    }
                    else
                    {
                        exFvc = -1.774+ht!*0.037+age*(-0.022);
                        exFev1 = -0.794+ht!*0.027+age*(-0.021);
                    }
                    
                    
                    if (age < 15)
                    {
                        exPef=455 * (ht!/100) - 332
                        
                    }
                    else   //Females age >= 15
                    {
                        exPef = exp((0.5376 * log(age))-(0.0120*age)-(58.8 / ht!)+5.63)
                    }
                }
            }
            else if USERDEFAULT.value(forKey: "gender") != nil{
                //print("Counting from USERDEFAULT gender")
                
                let age:Double = Double(self.calculateAge(dob: USERDEFAULT.value(forKey: "dob") as! String))
                let strGender = USERDEFAULT.value(forKey: "gender") as! String
                
                let ht = Double(height!)
                
                
                if  strGender == "Male"
                {
                    if (age < 25)
                    {
                        exFvc = -5.508+0.05*ht!+0.078*age;
                        exFev1 = -4.808+0.046*ht!+0.045*age;
                    }
                    else   // Knudson-males-age>= 25
                    {
                        exFvc = -5.459+0.065*ht!-0.029*age;
                        exFev1 = -4.203+0.052*ht!-0.027*age;
                    }
                    
                    if (age < 15)
                    {
                        exPef=455 * (ht!/100) - 332
                        
                    }
                    else   //males age>= 15
                    {
                        exPef = exp((0.544 * log(age))-(0.0151*age)-(74.7 / ht!)+5.48);  // For Male
                    }
                    //print("PEF --> \(exPef)")
                }
                else  if strGender == "Female"
                {
                    if (age < 20)
                    {
                        exFvc = -3.469+ht!*0.033+age*0.092;
                        exFev1 = -2.703+ht!*0.027+age*0.085;
                    }
                    else
                    {
                        exFvc = -1.774+ht!*0.037+age*(-0.022);
                        exFev1 = -0.794+ht!*0.027+age*(-0.021);
                    }
                    
                    
                    if (age < 15)
                    {
                        exPef=455 * (ht!/100) - 332
                        
                    }
                    else   //Females age >= 15
                    {
                        exPef = exp((0.5376 * log(age))-(0.0120*age)-(58.8 / ht!)+5.63)
                    }
                }
            }
            
            
             let chResult = PulmonaryTestResultVC(nibName: "PulmonaryTestResultVC", bundle: nil)
             
             chResult.userFEV = (arrBpText[indexPath.row] as AnyObject).value(forKey: "fev1") as? NSString
             chResult.userFVC = (arrBpText[indexPath.row] as AnyObject).value(forKey: "fvc") as? NSString
             chResult.userPEFR = (arrBpText[indexPath.row] as AnyObject).value(forKey: "pefr") as? NSString
             chResult.strHeight = (arrBpText[indexPath.row] as AnyObject).value(forKey: "height") as? NSString
             
             chResult.strFEV = exFev1
             chResult.strFVC = exFvc
             chResult.strPEFR = exPef
            
            let date = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "created_at")as? String ?? "", newFormat: "yyyy/MM/dd")
            chResult.reportDate = date
             self.navigationController?.pushViewController(chResult, animated: true)
        }
        else if selectedIndex == 10{
            
            //Child Feet
            var childInch = "00"
            if !Utility.isEmpty((arrBpText[indexPath.row] as! NSDictionary).value(forKey: "child_height_inches") as? String ?? ""){
                childInch = (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "child_height_inches") as? String ?? ""
            }
            let childFeet = (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "child_height_feet") as? String ?? ""
            let finalChildHeightFeet = childFeet+"."+childInch
            //print("Child = \(finalChildHeightFeet)")
            
            //Mother Feet
            var motherInch = "00"
            if !Utility.isEmpty((arrBpText[indexPath.row] as! NSDictionary).value(forKey: "mother_height_inches") as? String ?? ""){
                motherInch = (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "mother_height_inches") as? String ?? ""
            }
            let motherFeet = (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "mother_height_feet") as? String ?? ""
            let finalMotherHeightFeet = motherFeet+"."+motherInch
            //print("mother = \(finalMotherHeightFeet)")
            
            //Father Feet
            var fatherInch = "00"
            if !Utility.isEmpty((arrBpText[indexPath.row] as! NSDictionary).value(forKey: "father_height_inches") as? String ?? ""){
                fatherInch = (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "father_height_inches") as? String ?? ""
            }
            let fatherFeet = (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "father_height_feet") as? String ?? ""
            let finalFatherHeightFeet = fatherFeet+"."+fatherInch
            //print("Father = \(finalFatherHeightFeet)")
            
            let chResult = ChildGrowthTestResultVC(nibName: "ChildGrowthTestResultVC", bundle: nil)
            chResult.isFromReport = true
            chResult.gender = (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "child_sex") as? String ?? ""
            chResult.age = Double((arrBpText[indexPath.row] as! NSDictionary).value(forKey: "age_month") as? String ?? "0")!
            chResult.gender = (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "child_sex") as? String ?? ""
            chResult.childFeet = (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "child_height_feet") as? String ?? ""
            chResult.childInches = (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "child_height_inches") as? String ?? ""
            chResult.motherFeet = (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "mother_height_feet") as? String ?? ""
            chResult.motherInches = (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "mother_height_inches") as? String ?? ""
            chResult.fatherFeet = (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "father_height_feet") as? String ?? ""
            chResult.fatherInches = (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "father_height_inches") as? String ?? ""
            let date = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: (arrBpText[indexPath.row] as! NSDictionary).value(forKey: "created_at")as? String ?? "", newFormat: "yyyy/MM/dd")
            chResult.reportDate = date
            chResult.childHeightFeet = Double(finalChildHeightFeet)!
            chResult.motherHeightFeet = Double(finalMotherHeightFeet)!
            chResult.fatherHeightFeet = Double(finalFatherHeightFeet)!
            self.navigationController?.pushViewController(chResult, animated: true)
        }
    }
    
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForGetHealthReport(){
        
        let completeURL = NSString(format:"%@%@", MainURL,bloodPressureTestReportURL) as String
        
        let pageNumber = "\(pageNum1!)"
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "page":pageNumber,
            "limit":PAGINATION_LIMITE,
            "lang_type":Language_Type
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        //print("GetHealthReport API Parameter :",finalParams)
        print("GetHealthReport API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: bloodPressureTestReportURLTag)
    }
    
    func postDataOnWebserviceForBloodGlucoseTestReport(){
        
        let completeURL = NSString(format:"%@%@", MainURL,bloodGlucoseTestReportURL) as String
        
        let pageNumber = "\(pageNum1!)"
        // "page":pageNumber,
        // "limit":PAGINATION_LIMITE
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "page":pageNumber,
            "limit":PAGINATION_LIMITE,
            "lang_type":Language_Type
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
//        print("bloodGlucose API Parameter :",finalParams)
        print("bloodGlucoseTest API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: bloodGlucoseTestReportURLTag)
        
        
    }
    
    func postDataOnWebserviceForUricAcidTestReport(){
        
        let completeURL = NSString(format:"%@%@", MainURL,uricAcidReportURL) as String
        
        let pageNumber = "\(pageNum1!)"
        // "page":pageNumber,
        // "limit":PAGINATION_LIMITE
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "page":pageNumber,
            "limit":PAGINATION_LIMITE,
            "lang_type":Language_Type
        ]
        
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
//        print("GetHealthReport API Parameter :",finalParams)
        print("GetHealthReport API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: uricAcidReportURLTag)
        
        
    }
    func postDataOnWebserviceForIntraOcularPressureTestReportReport(){
        
        let completeURL = NSString(format:"%@%@", MainURL,intraOcularPressureTestReportURL) as String
        
        let pageNumber = "\(pageNum1!)"
        // "page":pageNumber,
        // "limit":PAGINATION_LIMITE
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "page":pageNumber,
            "limit":PAGINATION_LIMITE,
            "lang_type":Language_Type
        ]
        
        
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
//        print("GetHealthReport API Parameter :",finalParams)
        print("GetHealthReport API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: intraOcularPressureTestReportURLTag)
        
        
    }
    func postDataOnWebserviceForAriaStrokeDataReport(){
        
        let completeURL = NSString(format:"%@%@", MainURL,ariaStrokeReportURL) as String
        
        let pageNumber = "\(pageNum1!)"
        // "page":pageNumber,
        // "limit":PAGINATION_LIMITE
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "page":pageNumber,
            "limit":PAGINATION_LIMITE,
            "lang_type":Language_Type
        ]
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
//        print("GetHealthReport API Parameter :",finalParams)
        print("GetHealthReport API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: ariaStrokeReportURLTag)
        
        
    }
    func postDataOnWebserviceForOsteoporosisAssetmentToolTestReport(){
        
        let completeURL = NSString(format:"%@%@", MainURL,osteoporosisAssetmentToolReportURL) as String
        
        let pageNumber = "\(pageNum1!)"
        // "page":pageNumber,
        // "limit":PAGINATION_LIMITE
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "page":pageNumber,
            "limit":PAGINATION_LIMITE,
            "lang_type":Language_Type
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
//        print("GetHealthReport API Parameter :",finalParams)
        print("GetHealthReport API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: osteoporosisAssetmentToolReportURLTag)
        
        
    }
    
    func postDataGetCholesterolTestReport(){
        let completeURL = NSString(format:"%@%@", MainURL,cholesterolHistoryReportUrl) as String
        
        let pageNumber = "\(pageNum1!)"
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "page":pageNumber,
            "limit":PAGINATION_LIMITE,
            "lang_type":Language_Type
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
//        print("cholesterolHistoryReportUrlTag API Parameter :",finalParams)
        print("cholesterolHistoryReportUrlTag API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: cholesterolHistoryReportUrlTag)
        
    }
    
    func postDataGetSOATestReport(){
        let completeURL = NSString(format:"%@%@", MainURL,soaHistoryReportUrl) as String
        
        let pageNumber = "\(pageNum1!)"
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "page":pageNumber,
            "limit":PAGINATION_LIMITE,
            "lang_type":Language_Type
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
//        print("SOA HistoryReportUrlTag API Parameter :",finalParams)
        print("SOA HistoryReportUrlTag API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: soaHistoryReportUrlTag)
    }
    
    func postDataGetWaistHipTestReport(){
        let completeURL = NSString(format:"%@%@", MainURL,waistHipRatioHistoryReportUrl) as String
        
        let pageNumber = "\(pageNum1!)"
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "page":pageNumber,
            "limit":PAGINATION_LIMITE,
            "lang_type":Language_Type
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
//        print("WaistHip HistoryReportUrlTag API Parameter :",finalParams)
        print("WaistHip HistoryReportUrlTag API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: waistHipRatioHistoryReportUrlTag)
    }
    
    
    func postDataGetPulmonaryTestReport(){
        let completeURL = NSString(format:"%@%@", MainURL,pulmonaryHistoryReportUrl) as String
        
        let pageNumber = "\(pageNum1!)"
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "page":pageNumber,
            "limit":PAGINATION_LIMITE,
            "lang_type":Language_Type
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
//        print("Pulmonary HistoryReportUrlTag API Parameter :",finalParams)
        print("Pulmonary HistoryReportUrlTag API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: pulmonaryHistoryReportUrlTag)
    }
    

    func postDataGetChildGrowthTestReport(){
        let completeURL = NSString(format:"%@%@", MainURL,childGrowthTestHistoryUrl) as String
        
        let pageNumber = "\(pageNum1!)"
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "page":pageNumber,
            "limit":PAGINATION_LIMITE,
            "lang_type":Language_Type
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("childGrowthTestHistoryUrl API URL :",completeURL)
        print("childGrowthTestHistoryUrl API Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: childGrowthTestHistoryUrlTag)
    }
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
            
        case bloodPressureTestReportURLTag:
            let resultDict = responseObject as! NSDictionary;
            //print("bloodPressureTestReportURLTag List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum1 == 1{
                    arrBpText = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        arrBpText.add(myData[i])
                    }
                                      
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum1 > 0) {
                            self.pageNum1 = self.pageNum1 - 1
                        }
                        self.isLoading1 = false
                    }else{
                        self.isLoading1 = true
                    }
                }
                else{
                    self.isLoading1 = false
                    if (self.pageNum1 > 0) {
                        self.pageNum1 = self.pageNum1 - 1
                    }
                    
                }
                
                if arrBpText.count == 0{
                    self.tableViewReport.isHidden = true
                    self.lblNoReport.isHidden = false
                }
                else{
                    self.tableViewReport.isHidden = false
                    self.lblNoReport.isHidden = true
                }
                
                
                self.tableViewReport.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        case bloodGlucoseTestReportURLTag:
            let resultDict = responseObject as! NSDictionary;
            //print("bloodGlucoseTestReportURLTag List Response  : \(resultDict)")
            self.isLoading1 = false

            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum1 == 1{
                    arrBpText = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        arrBpText.add(myData[i])
                    }
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum1 > 0) {
                            self.pageNum1 = self.pageNum1 - 1
                        }
                        self.isLoading1 = false
                    }else{
                        self.isLoading1 = true
                    }
                }
                else{
                    self.isLoading1 = false
                    if (self.pageNum1 > 0) {
                        self.pageNum1 = self.pageNum1 - 1
                    }
                    
                }
                
                if arrBpText.count == 0{
                    self.tableViewReport.isHidden = true
                    self.lblNoReport.isHidden = false
                }
                else{
                    self.tableViewReport.isHidden = false
                    self.lblNoReport.isHidden = true
                }
                
                
                self.tableViewReport.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
        case uricAcidReportURLTag:
            let resultDict = responseObject as! NSDictionary;
            //print("uricAcidReportURLTag List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum1 == 1{
                    arrBpText = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        arrBpText.add(myData[i])
                    }
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum1 > 0) {
                            self.pageNum1 = self.pageNum1 - 1
                        }
                        self.isLoading1 = false
                    }else{
                        self.isLoading1 = true
                    }
                }
                else{
                    self.isLoading1 = false
                    if (self.pageNum1 > 0) {
                        self.pageNum1 = self.pageNum1 - 1
                    }
                    
                }
                
                if arrBpText.count == 0{
                    self.tableViewReport.isHidden = true
                    self.lblNoReport.isHidden = false
                }
                else{
                    self.tableViewReport.isHidden = false
                    self.lblNoReport.isHidden = true
                }
                
                
                self.tableViewReport.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
        case intraOcularPressureTestReportURLTag:
            let resultDict = responseObject as! NSDictionary;
            //print("intraOcularPressureTestReportURLTag List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum1 == 1{
                    arrBpText = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        arrBpText.add(myData[i])
                    }
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum1 > 0) {
                            self.pageNum1 = self.pageNum1 - 1
                        }
                        self.isLoading1 = false
                    }else{
                        self.isLoading1 = true
                    }
                }
                else{
                    self.isLoading1 = false
                    if (self.pageNum1 > 0) {
                        self.pageNum1 = self.pageNum1 - 1
                    }
                    
                }
                
                if arrBpText.count == 0{
                    self.tableViewReport.isHidden = true
                    self.lblNoReport.isHidden = false
                }
                else{
                    self.tableViewReport.isHidden = false
                    self.lblNoReport.isHidden = true
                }
                
                
                self.tableViewReport.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break

        case ariaStrokeReportURLTag:
            let resultDict = responseObject as! NSDictionary;
//            print("ariaStrokeReportURLTag List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum1 == 1{
                    arrBpText = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        arrBpText.add(myData[i])
                    }
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum1 > 0) {
                            self.pageNum1 = self.pageNum1 - 1
                        }
                        self.isLoading1 = false
                    }else{
                        self.isLoading1 = true
                    }
                }
                else{
                    self.isLoading1 = false
                    if (self.pageNum1 > 0) {
                        self.pageNum1 = self.pageNum1 - 1
                    }
                    
                }
                
                if arrBpText.count == 0{
                    self.tableViewReport.isHidden = true
                    self.lblNoReport.isHidden = false
                }
                else{
                    self.tableViewReport.isHidden = false
                    self.lblNoReport.isHidden = true
                }
                
                
                self.tableViewReport.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
        case osteoporosisAssetmentToolReportURLTag:
            let resultDict = responseObject as! NSDictionary;
//            print("osteoporosisAssetmentToolReportURLTag List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum1 == 1{
                    arrBpText = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        arrBpText.add(myData[i])
                    }
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum1 > 0) {
                            self.pageNum1 = self.pageNum1 - 1
                        }
                        self.isLoading1 = false
                    }else{
                        self.isLoading1 = true
                    }
                }
                else{
                    self.isLoading1 = false
                    if (self.pageNum1 > 0) {
                        self.pageNum1 = self.pageNum1 - 1
                    }
                    
                }
                
                if arrBpText.count == 0{
                    self.tableViewReport.isHidden = true
                    self.lblNoReport.isHidden = false
                }
                else{
                    self.tableViewReport.isHidden = false
                    self.lblNoReport.isHidden = true
                }
                
                
                self.tableViewReport.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        case cholesterolHistoryReportUrlTag:
            let resultDict = responseObject as! NSDictionary;
//            print("cholesterolHistoryReportUrlTag Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum1 == 1{
                    arrBpText = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        arrBpText.add(myData[i])
                    }
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum1 > 0) {
                            self.pageNum1 = self.pageNum1 - 1
                        }
                        self.isLoading1 = false
                    }else{
                        self.isLoading1 = true
                    }
                }
                else{
                    self.isLoading1 = false
                    if (self.pageNum1 > 0) {
                        self.pageNum1 = self.pageNum1 - 1
                    }
                    
                }
                
                if arrBpText.count == 0{
                    self.tableViewReport.isHidden = true
                    self.lblNoReport.isHidden = false
                }
                else{
                    self.tableViewReport.isHidden = false
                    self.lblNoReport.isHidden = true
                }
                
                
                self.tableViewReport.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        case soaHistoryReportUrlTag:
            let resultDict = responseObject as! NSDictionary;
            print("soaHistoryReportUrlTag Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum1 == 1{
                    arrBpText = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        arrBpText.add(myData[i])
                    }
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum1 > 0) {
                            self.pageNum1 = self.pageNum1 - 1
                        }
                        self.isLoading1 = false
                    }else{
                        self.isLoading1 = true
                    }
                }
                else{
                    self.isLoading1 = false
                    if (self.pageNum1 > 0) {
                        self.pageNum1 = self.pageNum1 - 1
                    }
                    
                }
                
                if arrBpText.count == 0{
                    self.tableViewReport.isHidden = true
                    self.lblNoReport.isHidden = false
                }
                else{
                    self.tableViewReport.isHidden = false
                    self.lblNoReport.isHidden = true
                }
                
                self.tableViewReport.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            
            break
            
        case waistHipRatioHistoryReportUrlTag:
            let resultDict = responseObject as! NSDictionary;
            print("waistHipRatioHistoryReportUrlTag Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum1 == 1{
                    arrBpText = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        arrBpText.add(myData[i])
                    }
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum1 > 0) {
                            self.pageNum1 = self.pageNum1 - 1
                        }
                        self.isLoading1 = false
                    }else{
                        self.isLoading1 = true
                    }
                }
                else{
                    self.isLoading1 = false
                    if (self.pageNum1 > 0) {
                        self.pageNum1 = self.pageNum1 - 1
                    }
                    
                }
                
                if arrBpText.count == 0{
                    self.tableViewReport.isHidden = true
                    self.lblNoReport.isHidden = false
                }
                else{
                    self.tableViewReport.isHidden = false
                    self.lblNoReport.isHidden = true
                }
                
                self.tableViewReport.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            
            break
        
        case pulmonaryHistoryReportUrlTag:
            let resultDict = responseObject as! NSDictionary;
            print("pulmonaryHistoryReportUrlTag Response  : \(resultDict)")
            
            self.removeLoadingIndicatiorOnFooterOnTableView()
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum1 == 1{
                    arrBpText = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        arrBpText.add(myData[i])
                    }
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum1 > 0) {
                            self.pageNum1 = self.pageNum1 - 1
                        }
                        self.isLoading1 = false
                    }else{
                        self.isLoading1 = true
                    }
                }
                else{
                    self.isLoading1 = false
                    if (self.pageNum1 > 0) {
                        self.pageNum1 = self.pageNum1 - 1
                    }
                    
                }
                
                if arrBpText.count == 0{
                    self.tableViewReport.isHidden = true
                    self.lblNoReport.isHidden = false
                }
                else{
                    self.tableViewReport.isHidden = false
                    self.lblNoReport.isHidden = true
                }
                
                self.tableViewReport.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
        
        case childGrowthTestHistoryUrlTag:
            let resultDict = responseObject as! NSDictionary;
            print("childGrowthTestHistoryUrlTag Response  : \(resultDict)")
            
            self.removeLoadingIndicatiorOnFooterOnTableView()
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum1 == 1{
                    arrBpText = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        arrBpText.add(myData[i])
                    }
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum1 > 0) {
                            self.pageNum1 = self.pageNum1 - 1
                        }
                        self.isLoading1 = false
                    }else{
                        self.isLoading1 = true
                    }
                }
                else{
                    self.isLoading1 = false
                    if (self.pageNum1 > 0) {
                        self.pageNum1 = self.pageNum1 - 1
                    }
                    
                }
                
                if arrBpText.count == 0{
                    self.tableViewReport.isHidden = true
                    self.lblNoReport.isHidden = false
                }
                else{
                    self.tableViewReport.isHidden = false
                    self.lblNoReport.isHidden = true
                }
                
                self.tableViewReport.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
            
        case bloodPressureTestReportURLTag:
            self.isLoading1 = false
            if (self.pageNum1 > 1) {
                self.pageNum1 = self.pageNum1 - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
            
        case bloodGlucoseTestReportURLTag:
            self.isLoading1 = false
            if (self.pageNum1 > 1) {
                self.pageNum1 = self.pageNum1 - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
            
        case uricAcidReportURLTag:
            self.isLoading1 = false
            if (self.pageNum1 > 1) {
                self.pageNum1 = self.pageNum1 - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
        case intraOcularPressureTestReportURLTag:
            self.isLoading1 = false
            if (self.pageNum1 > 1) {
                self.pageNum1 = self.pageNum1 - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
        case ariaStrokeReportURLTag:
            self.isLoading1 = false
            if (self.pageNum1 > 1) {
                self.pageNum1 = self.pageNum1 - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
            
        case osteoporosisAssetmentToolReportURLTag:
            self.isLoading1 = false
            if (self.pageNum1 > 1) {
                self.pageNum1 = self.pageNum1 - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break

        case cholesterolHistoryReportUrlTag:
            self.isLoading1 = false
            if (self.pageNum1 > 1) {
                self.pageNum1 = self.pageNum1 - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
            
        case soaHistoryReportUrlTag:
            self.isLoading1 = false
            if (self.pageNum1 > 1) {
                self.pageNum1 = self.pageNum1 - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
            
        case waistHipRatioHistoryReportUrlTag:
            self.isLoading1 = false
            if (self.pageNum1 > 1) {
                self.pageNum1 = self.pageNum1 - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
            
        case pulmonaryHistoryReportUrlTag:
            self.isLoading1 = false
            if (self.pageNum1 > 1) {
                self.pageNum1 = self.pageNum1 - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
        
        case childGrowthTestHistoryUrlTag:
            self.isLoading1 = false
            if (self.pageNum1 > 1) {
                self.pageNum1 = self.pageNum1 - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    // TODO: - GOING FOR LOGIN
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    // TODO: - DELEGATE ScrollView
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == tableViewReport {
        if isLoading1 == true {
            if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                if Reachability.isConnectedToNetwork() == true {
                    pageNum1 = pageNum1 + 1
                    print(pageNum1)
                    isLoading1 = false
                    self.addLoadingIndicatiorOnFooterOnTableView()
                    
                    if strCollectionSelected == "BP Test"{
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetHealthReport), userInfo: nil, repeats: false)
                    }
                    else if strCollectionSelected == "Blood Glucose Test"{
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForBloodGlucoseTestReport), userInfo: nil, repeats: false)
                    }
                    else if strCollectionSelected == "Uric Acid Test"{
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForUricAcidTestReport), userInfo: nil, repeats: false)
                    }
                    else if strCollectionSelected == "Intra Ocular Pressure"{
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForIntraOcularPressureTestReportReport), userInfo: nil, repeats: false)
                    }
                    else if strCollectionSelected == "Aria Stroke Risk"{
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForAriaStrokeDataReport), userInfo: nil, repeats: false)
                    }
                    else if strCollectionSelected == "BMI and OST"{
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForOsteoporosisAssetmentToolTestReport), userInfo: nil, repeats: false)
                    }
                    else if strCollectionSelected == "Cholesterol Test"{
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataGetCholesterolTestReport), userInfo: nil, repeats: false)
                    }
                    else if strCollectionSelected == "SOA" {
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataGetSOATestReport), userInfo: nil, repeats: false)
                    }
                    else if strCollectionSelected == "Waist Hip Ratio" {
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataGetWaistHipTestReport), userInfo: nil, repeats: false)
                    }
                    else if strCollectionSelected == "Pulmonary" {
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataGetPulmonaryTestReport), userInfo: nil, repeats: false)
                    }
                }
            }
        }
        }
        /*
        if scrollView == tableViewReport {
            if isLoading1 == true{
                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                    pageNum1 = pageNum1 + 1
                    print(pageNum1)
                    isLoading1 = false
                    
                    if Reachability.isConnectedToNetwork() == true {
                        self.addLoadingIndicatiorOnFooterOnTableView()
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetHealthReport), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                    
                }
                
            }
            else if isLoading1 == true{
                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                    pageNum1 = pageNum1 + 1
                    print(pageNum1)
                    //isLoading1 = false
                    
                    if strCollectionSelected == "Blood Glucose Test"{
                        if Reachability.isConnectedToNetwork() == true {
                            self.addLoadingIndicatiorOnFooterOnTableView()
                            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForBloodGlucoseTestReport), userInfo: nil, repeats: false)
                        } else {
                            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                        }
                        
                        
                    }
                    else if strCollectionSelected == "Uric Acid Test"{
                        if Reachability.isConnectedToNetwork() == true {
                            self.addLoadingIndicatiorOnFooterOnTableView()
                            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForUricAcidTestReport), userInfo: nil, repeats: false)
                        } else {
                            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                        }
                        
                        
                    }
                    else if strCollectionSelected == "Intra Ocular Pressure"{
                        if Reachability.isConnectedToNetwork() == true {
                            self.addLoadingIndicatiorOnFooterOnTableView()
                            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForIntraOcularPressureTestReportReport), userInfo: nil, repeats: false)
                        } else {
                            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                        }
                        
                        
                    }
                    else if strCollectionSelected == "Aria Stroke Risk"{
                        if Reachability.isConnectedToNetwork() == true {
                            self.addLoadingIndicatiorOnFooterOnTableView()
                            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForAriaStrokeDataReport), userInfo: nil, repeats: false)
                        } else {
                            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                        }
                        
                        
                                            }
                    else if strCollectionSelected == "BMI and OST"{
                        if Reachability.isConnectedToNetwork() == true {
                            self.addLoadingIndicatiorOnFooterOnTableView()
                            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForOsteoporosisAssetmentToolTestReport), userInfo: nil, repeats: false)
                        } else {
                            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                        }
                        
                        
                    }
                    else if strCollectionSelected == "Cholesterol Test"{
                        if Reachability.isConnectedToNetwork() == true {
                            self.addLoadingIndicatiorOnFooterOnTableView()
                            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataGetCholesterolTestReport), userInfo: nil, repeats: false)
                        } else {
                            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                        }
                        
                        
                    }

                }
            }
        }
        */
        
    }

    // TODO: - ACTION METODE

    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnReportAndHistoryClicked(_ sender: UIButton) {
      //  let bloodPressureVC = ReportAndHistoryVC(nibName: "ReportAndHistoryVC", bundle: nil)
      //  self.navigationController?.pushViewController(bloodPressureVC, animated: true)
        
        
    }
    
    
    func calculateAge(dob : String) -> Int{
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let date = df.date(from: dob)
        NSLog("%@", dob)
        guard let val = date else{
            return (0)
        }
        var years = 0
        var months = 0
        var days = 0
        
        let cal = NSCalendar.current
        years = cal.component(.year, from: NSDate() as Date) -  cal.component(.year, from: val)
        
        let currMonth = cal.component(.month, from: NSDate() as Date)
        let birthMonth = cal.component(.month, from: val)
        
        //get difference between current month and birthMonth
        months = currMonth - birthMonth
        //if month difference is in negative then reduce years by one and calculate the number of months.
        if months < 0
        {
            years = years - 1
            months = 12 - birthMonth + currMonth
            if cal.component(.day, from: NSDate() as Date) < cal.component(.day, from: val){
                months = months - 1
            }
        } else if months == 0 && cal.component(.day, from: NSDate() as Date) < cal.component(.day, from: val)
        {
            years = years - 1
            months = 11
        }
        
        //Calculate the days
        if cal.component(.day, from: NSDate() as Date) > cal.component(.day, from: val){
            days = cal.component(.day, from: NSDate() as Date) - cal.component(.day, from: val)
        }
        else if cal.component(.day, from: NSDate() as Date) < cal.component(.day, from: val)
        {
            let today = cal.component(.day, from: NSDate() as Date)
            let date = cal.date(byAdding:.month, value:  -1, to: NSDate() as Date)
            
            //let date = cal.dateByAddingUnit(.Month, value: -1, toDate: NSDate(), options: [])
            
            days = (cal.component(.day, from: date!) - cal.component(.day, from: val)) + today
        } else
        {
            days = 0
            if months == 12
            {
                years = years + 1
                months = 0
            }
        }
        
        return years
    }

}
