//
//  IntraOcularPressureTestVC.swift
//  Doc99
//
//  Created by MS-SUB-02 on 21/09/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class IntraOcularPressureTestVC: UIViewController,UITextFieldDelegate {
    @IBOutlet var viewBg:UIView!
    @IBOutlet weak var txtLeftEye: UITextField!
    @IBOutlet weak var txtRightEye: UITextField!
    @IBOutlet var txtIop:UITextField!
    @IBOutlet var scrMain:UIScrollView!

    var activeField: UITextField?
    
    //MARK:-
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.NavBarNumberPad()
        self.addTapGestureInOurView()
        
        
//        if #available(iOS 10.0, *)
//        {
//            txtLeftEye.keyboardType = .asciiCapableNumberPad
//            txtRightEye.keyboardType = .asciiCapableNumberPad
//            txtIop.keyboardType = .asciiCapableNumberPad
//        }
//        else{
//            txtMobile.keyboardType = .numberPad
//        }
        
             //viewBg.layer.borderWidth=0.5
        //viewBg.layer.borderColor=UIColor.lightGray.cgColor
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.scrMain.addGestureRecognizer(tapRecognizer)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let aSet = NSCharacterSet(charactersIn:"0123456789-+.").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        return string == numberFiltered
    }
    
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        if viewTouched!.isKind(of: UIButton.self){
            
        }
        else{
            self.view.endEditing(true)
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        }
    }
    

    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSaveRecordClicked(_ sender: UIButton) {
        
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
        if Utility.isEmpty(txtLeftEye.text!){
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter left eye parameters", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if Utility.isEmpty(txtRightEye.text!){
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter right eye parameters", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if Utility.isEmpty(txtIop.text!){
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter IOP parameters", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else{
            
            let eye = EyeTestResultVC(nibName: "EyeTestResultVC", bundle: nil)
            eye.isFromReport = false
            eye.leftEyeSight = txtLeftEye.text!
            eye.rightEyeSight = txtRightEye.text!
            eye.iOPValue = txtIop.text!
            
            self.navigationController?.pushViewController(eye, animated: true)

//            if Reachability.isConnectedToNetwork() == true {
//                SVProgressHUD.show(withStatus: "Loading..")
//                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForIntraOcularPressureTest), userInfo: nil, repeats: false)
//            } else {
//                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
//            }
        }

        
    
        
        
    }
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForIntraOcularPressureTest(){
        let completeURL = NSString(format:"%@%@", MainURL,intraOcularPressureTestDataURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "iop":txtIop.text as Any,
           
        ]
        
        
        
        
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("editWeightData API Parameter :",finalParams)
        print("editWeightData API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: intraOcularPressureTestURLTag)
        
    }
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case intraOcularPressureTestURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("editWeightData Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                
                let bloodPressureVC = ResultVC(nibName: "ResultVC", bundle: nil)
                bloodPressureVC.isFromTwoValue = false
                bloodPressureVC.strFirst = String(format: "%@", txtIop.text!) as NSString!
                

                self.navigationController?.pushViewController(bloodPressureVC, animated: true)
                //
                //                self.weightProgramID = (resultDict.value(forKey: "data") as AnyObject).value(forKey: "wp_id") as! String
                //
                //                let notificationName = Notification.Name("refreshMyWeightProgramData")
                //                NotificationCenter.default.post(name: notificationName, object: nil)
                //
                //                self.performSelector(inBackground: #selector(self.postDataOnWebserviceForGetSingleWeightProgram), with: nil)
                //
                //                self.navigationController?.popViewController(animated: true)
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                //                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            
            SVProgressHUD.dismiss()
            break
            
            
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case intraOcularPressureTestURLTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder?
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
            
        }
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var YOffset:Int = 0
        
//        if (textField.tag==1) {
//            YOffset=200
//        }
        
        if (DeviceType.IS_IPHONE_4_OR_LESS) {
            if (textField.tag==1) {
                YOffset=0
            }
        }
        scrMain.setContentOffset(CGPoint(x: 0, y: CGFloat(textField.tag * YOffset)), animated: true)
        return true
        
    }
    
    
    
    
    // Numeric Keyboard Hide Method
    func NavBarNumberPad(){
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        numberToolbar.barStyle = .default
        
        let viewBack = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        viewBack.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        numberToolbar.addSubview(viewBack)
        
        let btnCancel = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelNumberPad))
        btnCancel.tintColor = UIColor.white
        
        let btnDone = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.doneWithNumberPad))
        btnDone.tintColor = UIColor.white
        
        numberToolbar.items = NSArray(objects: btnCancel,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),btnDone) as? [UIBarButtonItem]
        
        
        numberToolbar.sizeToFit()
        txtIop.inputAccessoryView = numberToolbar
        txtLeftEye.inputAccessoryView = numberToolbar
        txtRightEye.inputAccessoryView = numberToolbar
        
    }
    
    func cancelNumberPad(){
        self.view.endEditing(true)
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
        
    }
    
    func doneWithNumberPad(){
        self.view.endEditing(true)
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
    }
    

}
