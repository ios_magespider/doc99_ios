//
//  ResultVC.swift
//  Doc99
//
//  Created by MS-SUB-02 on 21/09/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import  SVProgressHUD
//import FBSDKShareKit

class ResultVC: UIViewController{
    
    @IBOutlet var viewBg:UIView!
    @IBOutlet var viewTwoValue:UIView!
    @IBOutlet var lblFirstOne:UILabel!
    @IBOutlet var lblSecoundOne:UILabel!
    @IBOutlet var lblOnlyOne:UILabel!
    @IBOutlet var lblFirst:UILabel!
    @IBOutlet var lblSecound:UILabel!
    @IBOutlet var lblFinalOne:UILabel!
    @IBOutlet var lblTitle:UILabel!
    @IBOutlet var lblSingalTitle:UILabel!

    @IBOutlet var lblSingalBottom:UILabel!

    @IBOutlet var lblTitleSecoundFoeBloodGlucose:UILabel!

    @IBOutlet var lblFirstBottom:UILabel!
    @IBOutlet var lblSecoundBottom:UILabel!

    
    //MARK:-
    
    var isFromReport = false
    var strFirst:NSString?
    var strSecound:NSString?
    var strThird:NSString?
    
     var isFromTwoValue:Bool!
    
    var isFromBloodPressure:Bool!
    var isFromAreaStoke:Bool!

    var isFromIntraOcular:Bool!
    var isFromAriaStroke:Bool!
    var isFromUricAcid:Bool!
    
    
    var isFirstCall:Bool!
    var fromHistory : Bool = false
    
    
    var username: String = ""
    var secound: String = ""
    var textSharing: String = ""
    
    var userName : String = ""
    var gender : String = ""
    var date : String = ""
    
    var ssImage : UIImage!
    
    @IBOutlet var loginView:UIView!
    
    //LOGIN POPUP
    @IBOutlet var loginPopupLabel2:UILabel!
    @IBOutlet var loginPopupLabel3:UILabel!
    
    //MARK:-
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBg.layer.borderWidth=0.5
        viewBg.layer.borderColor=UIColor.lightGray.cgColor
        
        isFirstCall = true

        loginView.setNeedsLayout()
        loginView.layoutIfNeeded()
        if USERDEFAULT.value(forKey: "userID") != nil{
            
            if let isMember = USERDEFAULT.value(forKey: "isMember") as? String{
                let ismem = "\(isMember)"
                if ismem == "1"{
                }
                else{
                    loginPopupLabel3.text = "MEMBERSHIP"
                    self.view.addSubview(loginView)
                    self.addtopLeftViewConstraints()
                    
                    loginView.setNeedsLayout()
                    loginView.layoutIfNeeded()
                    
                }
            }
            else{
                
            }
        }
        else{
            
            loginPopupLabel3.text = "LOGIN"
            self.view.addSubview(loginView)
            self.addtopLeftViewConstraints()
            
            loginView.setNeedsLayout()
            loginView.layoutIfNeeded()
            
        }
        
        
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
      
        loginView.frame = self.view.frame
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.addSubview(loginView)
        loginView.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        loginView.frame = self.view.frame
        if USERDEFAULT.value(forKey: "userID") != nil{
            self.setData()
            loginView.removeFromSuperview()
        }
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-
    
    func setData()
    {
        
        if isFromTwoValue == true
        {
            viewTwoValue.isHidden  = false
            
            if isFromBloodPressure == true
            {
                if !isFromReport{
                    if Reachability.isConnectedToNetwork() == true{
                        if isFirstCall == true {
//                            SVProgressHUD.show(withStatus: "Loading..")
//                            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForBloodPresureTest), userInfo: nil, repeats: false)
                        }
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
                lblTitle.text = "Blood Pressure"
                lblTitleSecoundFoeBloodGlucose.text = "(Distolic pressure Test)"
                
                if let first = strFirst as String?
                {
                    
                    lblFirst.text = "Systolic"
                    lblFirstOne.text = "\(first)"
                    
                    lblFirstBottom.text = "mmHg"

                    let a = Int(first)
                    switch  a! {
                    case 90 ... 140:
                        lblFirstOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                    default:
                        lblFirstOne.textColor=UIColor.red
                    }
                    
                }
                if let  second = strSecound as String?
                {
                    
                    lblSecound.text = "Diastolic"
                    
                    lblSecoundOne.text = "\(second)"
                    
                    lblSecoundBottom.text = "mmHg"

                    
                    let b = Int(second)
                    switch  b! {
                    case 60 ... 90:
                        lblSecoundOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                    default:
                        lblSecoundOne.textColor=UIColor.red
                    }
                    
                    
                }
                
                lblFinalOne.text = "Your result is above the standard.Please consult the specialist."

                
                
            }
            else if isFromAreaStoke == true
            {

                lblTitle.text = "ARIA Stroke Test"
                lblTitleSecoundFoeBloodGlucose.text = ""

                if !isFromReport{
                    if Reachability.isConnectedToNetwork() == true {
                        SVProgressHUD.show(withStatus: "Loading..")
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceAriaStrokeRiskTest), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
                
                
                if let first = strFirst as String?
                {
                    
                    lblFirst.text = "Left Eye"
                    lblFirstOne.text = "\(first)"
                    lblFirstBottom.text = ""
                    
                    
                    let a = Float(first)
                    switch  a! {
                    case 0.3857 ... 0.4263:
                        lblFirstOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                    default:
                        lblFirstOne.textColor=UIColor.red
                    }
                    
                }
                if let  second = strSecound as String?
                {
                    
                    lblSecound.text = "Right Eye"
                    
                    lblSecoundOne.text = "\(second)"
                    
                    lblSecoundBottom.text = ""
                    
                    let b = Float(second)
                    switch  b! {
                    case 0.4932 ... 0.5754:
                        lblSecoundOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                    default:
                        lblSecoundOne.textColor=UIColor.red
                    }
                    
                    
                }
                lblFinalOne.text = "Your result is above the standard.Please consult the specialist."

                
                
            }
            else
            {
                
                lblTitle.text = "Blood Glucose Test"
                lblTitleSecoundFoeBloodGlucose.text = ""

                if !isFromReport{
                    if Reachability.isConnectedToNetwork() == true {
                        SVProgressHUD.show(withStatus: "Loading..")
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForBloodGlucoseTest), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
                
                if let first = strFirst as String?
                {
                    
                    lblFirst.text = "HbA1C"
                    lblFirstOne.text = "\(first)"
                    lblFirstBottom.text = "percent"

                    
                    let a = Float(first)
                    switch  a! {
                    case 0 ... 6.5:
                        lblFirstOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                    default:
                        lblFirstOne.textColor=UIColor.red
                    }
                    
                }
                if let  second = strSecound as String?
                {
                    
                    lblSecound.text = strThird! as String
                    
                    lblSecoundOne.text = "\(second)"
                    
                    lblSecoundBottom.text = "mmol/L"

                    if strThird == "Fasting Blood Glucose"
                    {
                        let b = Float(second)
                        switch  b! {
                        case 3.9 ... 6.1:
                            lblSecoundOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                        default:
                            lblSecoundOne.textColor=UIColor.red
                        }
                    }
                    else if strThird == "Postprandial 1 Hour"
                    {
                        let b = Float(second)
                        switch  b! {
                        case 7.8 ... 9.0:
                            lblSecoundOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                        default:
                            lblSecoundOne.textColor=UIColor.red
                        }
                    }
                    
                    else if strThird == "Postprandial 2 Hour"
                    {
                        let b = Float(second)
                        switch  b! {
                        case 0 ... 7.8:
                            lblSecoundOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                        default:
                            lblSecoundOne.textColor=UIColor.red
                        }
                    }
                    else
                    {
                        
                    }
                }
            }
            
            viewBg.addSubview(viewTwoValue)
            viewBg .bringSubview(toFront: viewTwoValue)
        }
        else
        {
            lblTitleSecoundFoeBloodGlucose.text = ""

            viewTwoValue.isHidden  = true
            if let first = strFirst as String?
            {
                if isFromIntraOcular == true
                {
                    lblTitle.text = "Intra-Ocular Pressure"
                    
                    if !isFromReport{
                        if Reachability.isConnectedToNetwork() == true {
                            SVProgressHUD.show(withStatus: "Loading..")
                            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForIntraOcularPressureTest), userInfo: nil, repeats: false)
                        } else {
                            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                        }
                    }
                    
                    
                    lblSingalTitle.text = "IOP"
                    lblOnlyOne.text = "\(first)"
                    lblSingalBottom.text = "mmHg"

                    // let a = Float(first)
                    switch  Int(first)! {
                    case 12 ... 20:
                           switch  Int(first)!
                           {
                           case 16 ... 20:
                            lblFinalOne.text="Your Result is between the Medium Risk"

                            break
                            
                           default:
                            lblFinalOne.text="Your Result is between the standard.Keep Smiling"
                            break
                      
                           }
                        
                        lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                        
                        
                    case 21 ... Int.max:
                        
                        
                        lblFinalOne.text="Your Result is between the High Risk"
                        
                        lblOnlyOne.textColor=UIColor.red
                        
                    case 0...15:
                        lblFinalOne.text="Your Result is between the Low Risk"
                        
                        lblOnlyOne.textColor=UIColor.red
                        
                        
                    default:
                        lblOnlyOne.textColor=UIColor.red
                    }
                    
                }
                else if isFromUricAcid == true
                {
                    
                    lblTitle.text = "Uric Acid Test"
                    if !isFromReport{
                        if Reachability.isConnectedToNetwork() == true {
                            SVProgressHUD.show(withStatus: "Loading..")
                            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForUricAcidTest), userInfo: nil, repeats: false)
                        } else {
                            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                        }
                    }
                    lblSingalTitle.text = "Parameter"
                    lblOnlyOne.text = "\(first)"
                    lblSingalBottom.text = ""
                    
                    
                    
                    if USERDEFAULT.value(forKey: "gender")  != nil
                    {
                        let strGender = USERDEFAULT.value(forKey: "gender") as! String
                        
                        if  strGender == "Male"
                        {
                            switch  Float(first)! {
                            case 0.15 ... 0.42:
                                lblFinalOne.text="Your Result is between the standard.Keep Smiling"
                                lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                                
                            default:
                                lblOnlyOne.textColor=UIColor.red
                            }
                        }
                        else  if strGender == "Female"
                        {
                            switch  Float(first)! {
                            case 0.09 ... 0.36:
                                lblFinalOne.text="Your Result is between the standard.Keep Smiling"
                                lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                                
                            default:
                                lblOnlyOne.textColor=UIColor.red
                            }
                            
                        }
                        
                    }
                }
                else
                {
                

                    lblTitle.text = "Osteoporosis Self Assessment Tool"
                    
                    let Weight:String = strSecound! as String
                    let Weight1:Double = Double(Weight)!
                    
                    if APPDELEGATE.healthIndexData.value(forKey: "birthDateIs")  == nil
                    {
                        let age:Double = Double(self.calculateAge(dob: USERDEFAULT.value(forKey: "dob") as! String))
                        //EmailId
                        
                        let OST = 0.2 * (Weight1 - age)
                        lblOnlyOne.text = "\(OST)"
                        
                        if OST >= 4 && OST <= 16{
                            lblFinalOne.text="Your Result is between the Low Risk"
                            lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                        }
                        else if OST >= -1 && OST <= -3{
                            lblFinalOne.text="Your Result is between the Medium Risk"
                            lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                        }
                        else if OST >= -6 && OST <= -1{
                            lblFinalOne.text="Your Result is between the High Risk"
                            lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                        }
                        else{
                            lblOnlyOne.textColor=UIColor.red
                        }
                        
                        //                                switch  Double(OST) {
                        //                                case 4 ... 16:
                        //                                    lblFinalOne.text="Your Result is between the Low Risk"
                        //                                    lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                        //
                        //                                case -1 ... -3:
                        //                                    lblFinalOne.text="Your Result is between the Medium Risk"
                        //                                    lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                        //
                        //                                case -6 ... -1:
                        //                                    lblFinalOne.text="Your Result is between the High Risk"
                        //                                    lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                        //
                        //                                default:
                        //                                    lblOnlyOne.textColor=UIColor.red
                        //                                }
                    }
                    else
                    {
                        let age:Double = Double(self.calculateAge(dob: APPDELEGATE.healthIndexData.value(forKey: "birthDateIs") as! String))
                        
                        let OST = 0.2 * (Weight1 - age)
                        lblOnlyOne.text = "\(OST)"
                        
                        switch  Double(OST) {
                        case 4 ... 20:
                            lblFinalOne.text="Your Result is between the Low Risk"
                            lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                            
                        case -1 ... 3:
                            lblFinalOne.text="Your Result is between the Medium Risk"
                            lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                            
                        case -20 ... 2:
                            lblFinalOne.text="Your Result is between the High Risk"
                            lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                            
                            
                        default:
                            lblOnlyOne.textColor=UIColor.red
                        }
                        
                        
                    }
                    
                    
                    
                    
                  //  EmailId
                    
                    if !isFromReport{
                        if Reachability.isConnectedToNetwork() == true {
                            SVProgressHUD.show(withStatus: "Loading..")
                            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForBMIAndOSTTest), userInfo: nil, repeats: false)
                        } else {
                            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                        }
                    }
                    lblSingalTitle.text = "OST"
                    lblSingalBottom.text = ""
                    
                }
                
            }
            
            
        }

    }
    @IBAction func btnHomeClicked(_ sender: UIButton) {
        
        
        
        var viewControllers = navigationController?.viewControllers
        viewControllers?.removeLast(2) // views to pop
        navigationController?.setViewControllers(viewControllers!, animated: true)
        
//        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-3] animated:YES];

       // let bloodPressureVC = ResultVC(nibName: "ResultVC", bundle: nil)
        //self.navigationController?.pushViewController(bloodPressureVC, animated: true)
        
        
    }
    @IBAction func btnShareReport(_ sender: UIButton)
    {
        
        UIGraphicsBeginImageContextWithOptions(viewBg.bounds.size, viewBg.isOpaque, 0.0)
        viewBg.drawHierarchy(in: viewBg.bounds, afterScreenUpdates: false)
        let snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        ssImage = snapshotImageFromMyView!

        userName = (USERDEFAULT.value(forKey: "fullName") as? String)!
        gender = (USERDEFAULT.value(forKey: "gender") as? String)!
        
        if !fromHistory {
            let dd = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM/dd"
            date = formatter.string(from: dd)
        }
        else{
            date = ""
        }
        
        
        if lblTitle.text == "Blood Glucose Test"
        {
            username = strFirst! as String //measure
            secound = lblSecound.text! as String //
            textSharing = "Name : \(userName)\nSex : \(gender)\nReport Date : \(date)"
            //\n\nBlood Glucose Test :\nHbA1C : \(lblFirstOne.text!), \(secound)
        }
       else if lblTitle.text == "ARIA Stroke Test"
        {
            
            username = strFirst! as String
            secound = lblSecound.text! as String
            textSharing = "Name : \(userName)\nSex : \(gender)\nReport Date : \(date)"
            //\n\nARIA Stroke Test :\nLeft Eye : \(lblFirstOne.text!), Right Eye : \(lblSecoundOne.text!)

        }
        else if lblTitle.text == "Blood Pressure"
        {
            
            username = strFirst! as String
            secound = lblSecound.text! as String
            textSharing = "Name : \(userName)\nSex : \(gender)\nReport Date : \(date)"
            //\n\nBlood Pressure Report :\nSystolic Parameter : \(lblFirstOne.text!), Diastolic Parameter : \(lblSecoundOne.text!)
            
        }
        else if lblTitle.text == "Uric Acid Test"
        {
            
            username = strFirst! as String
            secound = lblSecound.text! as String
            textSharing = "Name : \(userName)\nSex : \(gender)\nReport Date : \(date)"
            //\n\nUric Acid Test : \(username) \(secound)
        }
        else if lblTitle.text == "Osteoporosis Self Assessment Tool"
        {
            
            username = strFirst! as String
            secound = lblSecound.text! as String
            textSharing = "Name : \(userName)\nSex : \(gender)\nReport Date : \(date)"
            //\n\nOsteoporosis Self Assessment Tool :\nOST Result : \(lblOnlyOne.text!) \(secound)
        }
        else if lblTitle.text == "Intra-Ocular Pressure"
        {
            username = strFirst! as String
            secound = lblSecound.text! as String
            textSharing = "Name : \(userName)\nSex : \(gender)\nReport Date : \(date)"
            //\n\nIntra-Ocular Pressure :\nIOP Parameter : \(username) \(secound)
        }
        
        
        
        //self,"Sharing From Doc99",
        
        let  activityViewController = UIActivityViewController(activityItems: [self,ssImage], applicationActivities: [])
        
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop ]
        
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
  
    
    @IBAction func btnLoginClicked(_ sender:UIButton){
        
        if USERDEFAULT.value(forKey: "userID") as? String != nil{
            
            if let isMember = USERDEFAULT.value(forKey: "isMember") as? String{
                let ismem = "\(isMember)"
                if ismem == "1"{
                    
                }
                else{
                    let joinMemberVC = JoinMemberVC(nibName: "JoinMemberVC", bundle: nil)
                    joinMemberVC.isFromOther = true
                    joinMemberVC.isFromTab = "6"
                    self.navigationController?.pushViewController(joinMemberVC, animated: true)
                }
            }
            else{
                
            }
        }
        else{
            let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
            loginVC.isFromTab = "6"
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        
        
    }

    
   // TODO: - POST DATA METHODS
func postDataOnWebserviceForBloodPresureTest(){
    let completeURL = NSString(format:"%@%@", MainURL,bloodPressureTestDataURL) as String
    
    let params:NSDictionary = [
        "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
        "token": USERDEFAULT.value(forKey: "token") as! String,
        "lang_type":Language_Type,
        "heart_rate":strThird!,
        "systolic":strFirst!,
        "diastolic":strSecound!,
        "systolic_report":"20",
        "diastolic_report":"30"
    ]
    
    
    
    
    let finalParams:NSDictionary = [
        "data" : params
    ]
    
    
    
    print("editWeightData API Parameter :",finalParams)
    print("editWeightData API URL :",completeURL)
    
    let sampleProtocol = SyncManager()
    sampleProtocol.delegate = self
    sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: bloodPressureTestURLTag)
    
}

    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForIntraOcularPressureTest(){
        let completeURL = NSString(format:"%@%@", MainURL,intraOcularPressureTestDataURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "iop":strFirst!,
            
            ]
        
        
        
        
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("editWeightData API Parameter :",finalParams)
        print("editWeightData API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: intraOcularPressureTestURLTag)
        
    }
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceAriaStrokeRiskTest(){
        let completeURL = NSString(format:"%@%@", MainURL,ariaStrokeDataURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "left_eye":strFirst!,
            "right_eye":strSecound!,
            "left_eye_report":"20",
            "right_eye_report":"30",
        ]
        
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("editWeightData API Parameter :",finalParams)
        print("editWeightData API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: ariaStrokeDataURLTag)
        
    }
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForBloodGlucoseTest(){
        let completeURL = NSString(format:"%@%@", MainURL,bloodGlucoseTestDataURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "hba1c":strFirst!,
            "option":strThird!,
            "param":strSecound!,
            "hba1c_report":"20",
            "option_report":"30"
        ]
        
        
       
        
        
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("editWeightData API Parameter :",finalParams)
        print("editWeightData API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: bloodGlucoseTestURLTag)
        
    }
    func postDataOnWebserviceForBMIAndOSTTest(){
        let completeURL = NSString(format:"%@%@", MainURL,osteoporosisAssetmentToolDataURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "ost":lblOnlyOne.text!,
            "weight":strSecound!,
            "weight_in":strThird!,
            "ost_report":"20",
        ]
        
        
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("editWeightData API Parameter :",finalParams)
        print("editWeightData API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: osteoporosisAssetmentToolURLTag)
        
    }
    
    func postDataOnWebserviceForUricAcidTest(){
        let completeURL = NSString(format:"%@%@", MainURL,uricAcidDataURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "uat_param":strFirst!,
            "uat_param_report":"0.432"

            
            ]
        
        
        
        
        
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("editWeightData API Parameter :",finalParams)
        print("editWeightData API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: uricAcidDataURLTag)
        
    }
    

    
    // MARK:-
func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
    switch tag {
    case bloodPressureTestURLTag:
        let resultDict = responseObject as! NSDictionary;
        print("editWeightData Response  : \(resultDict)")
        
        if resultDict.value(forKey: "status") as! String == "1"{
            
            isFirstCall = false
            
            //
            //                self.weightProgramID = (resultDict.value(forKey: "data") as AnyObject).value(forKey: "wp_id") as! String
            //
            //                let notificationName = Notification.Name("refreshMyWeightProgramData")
            //                NotificationCenter.default.post(name: notificationName, object: nil)
            //
            //                self.performSelector(inBackground: #selector(self.postDataOnWebserviceForGetSingleWeightProgram), with: nil)
            //
            //                self.navigationController?.popViewController(animated: true)
        }
        else if resultDict.value(forKey: "status") as! String == "0"{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
        }
        else if resultDict.value(forKey: "status") as! String == "3"{
            
        }
            
        else if resultDict.value(forKey: "status") as! String == "2"{
            //                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
        }
        
        
        SVProgressHUD.dismiss()
        break
    case intraOcularPressureTestURLTag:
        let resultDict = responseObject as! NSDictionary;
        print("editWeightData Response  : \(resultDict)")
        
        if resultDict.value(forKey: "status") as! String == "1"{
            
            
           
        }
        else if resultDict.value(forKey: "status") as! String == "0"{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
        }
        else if resultDict.value(forKey: "status") as! String == "3"{
            
        }
            
        else if resultDict.value(forKey: "status") as! String == "2"{
            //                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
        }
        
        
        SVProgressHUD.dismiss()
        break

    case ariaStrokeDataURLTag:
        let resultDict = responseObject as! NSDictionary;
        print("editWeightData Response  : \(resultDict)")
        
        if resultDict.value(forKey: "status") as! String == "1"{
            
        
        }
        else if resultDict.value(forKey: "status") as! String == "0"{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
        }
        else if resultDict.value(forKey: "status") as! String == "3"{
            
        }
            
        else if resultDict.value(forKey: "status") as! String == "2"{
            //                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
        }
        
        
        SVProgressHUD.dismiss()
        break
        
    case bloodGlucoseTestURLTag:
        let resultDict = responseObject as! NSDictionary;
        print("editWeightData Response  : \(resultDict)")
        
        if resultDict.value(forKey: "status") as! String == "1"{
            
            
          
        }
        else if resultDict.value(forKey: "status") as! String == "0"{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
        }
        else if resultDict.value(forKey: "status") as! String == "3"{
            
        }
            
        else if resultDict.value(forKey: "status") as! String == "2"{
            //                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
        }
        
        
        SVProgressHUD.dismiss()
        break
    case osteoporosisAssetmentToolURLTag:
        let resultDict = responseObject as! NSDictionary;
        print("editWeightData Response  : \(resultDict)")
        
        if resultDict.value(forKey: "status") as! String == "1"{
            
            
            
        }
        else if resultDict.value(forKey: "status") as! String == "0"{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
        }
        else if resultDict.value(forKey: "status") as! String == "3"{
            
        }
            
        else if resultDict.value(forKey: "status") as! String == "2"{
            //                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
        }
        
        
        SVProgressHUD.dismiss()
        break
    case uricAcidDataURLTag:
        let resultDict = responseObject as! NSDictionary;
        print("editWeightData Response  : \(resultDict)")
        
        if resultDict.value(forKey: "status") as! String == "1"{
            
            
            
        }
        else if resultDict.value(forKey: "status") as! String == "0"{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
        }
        else if resultDict.value(forKey: "status") as! String == "3"{
            
        }
            
        else if resultDict.value(forKey: "status") as! String == "2"{
            //                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
        }
        
        
        SVProgressHUD.dismiss()
        break
        

        
    default:
        break
        
    }
    
}
func syncFailure(_ error: Error!, withTag tag: Int) {
    switch tag {
    case bloodPressureTestURLTag:
        SVProgressHUD.dismiss()
        break
    case intraOcularPressureTestURLTag:
        SVProgressHUD.dismiss()
        break
    case ariaStrokeDataURLTag:
        SVProgressHUD.dismiss()
        break
    case bloodGlucoseTestURLTag:
        SVProgressHUD.dismiss()
        break
    case osteoporosisAssetmentToolURLTag:
        SVProgressHUD.dismiss()
        break
    case uricAcidDataURLTag:
        SVProgressHUD.dismiss()
        break

    default:
        break
        
    }
    print("syncFailure Error : ",error.localizedDescription)
    showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
}

    
    func addtopLeftViewConstraints()
    {
        
        NSLayoutConstraint(item: loginView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: loginView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: loginView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: loginView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1.0, constant: 0.0).isActive = true

//        let topLeftViewLeadingConstraint = NSLayoutConstraint(item: loginView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal
//            , toItem:self.view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
//        
//        let topLeftViewTopConstraint = NSLayoutConstraint(item: loginView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal
//            , toItem: self.view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
//        
//        let topLeftViewTailingConstraint = NSLayoutConstraint(item: loginView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal
//            , toItem:self.view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
//        
//        let topLeftViewBottomConstraint = NSLayoutConstraint(item: loginView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal
//            , toItem:self.view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
//
////        let topLeftViewHeightConstraint = NSLayoutConstraint(item: loginView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal
////            , toItem:self.view, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0)
////        
////        
////        let topLeftViewWidthConstraint = NSLayoutConstraint(item: loginView, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal
////            , toItem:self.view, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0)
//        
//
//
//        NSLayoutConstraint.activate([topLeftViewLeadingConstraint, topLeftViewTailingConstraint, topLeftViewTopConstraint,topLeftViewBottomConstraint])
    }
 
    
    
    func calculateAge(dob : String) -> Int{
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let date = df.date(from: dob)
        NSLog("%@", dob)
        guard let val = date else{
            return (0)
        }
        var years = 0
        var months = 0
        var days = 0
        
        let cal = NSCalendar.current
        years = cal.component(.year, from: NSDate() as Date) -  cal.component(.year, from: val)
        
        let currMonth = cal.component(.month, from: NSDate() as Date)
        let birthMonth = cal.component(.month, from: val)
        
        //get difference between current month and birthMonth
        months = currMonth - birthMonth
        //if month difference is in negative then reduce years by one and calculate the number of months.
        if months < 0
        {
            years = years - 1
            months = 12 - birthMonth + currMonth
            if cal.component(.day, from: NSDate() as Date) < cal.component(.day, from: val){
                months = months - 1
            }
        } else if months == 0 && cal.component(.day, from: NSDate() as Date) < cal.component(.day, from: val)
        {
            years = years - 1
            months = 11
        }
        
        //Calculate the days
        if cal.component(.day, from: NSDate() as Date) > cal.component(.day, from: val){
            days = cal.component(.day, from: NSDate() as Date) - cal.component(.day, from: val)
        }
        else if cal.component(.day, from: NSDate() as Date) < cal.component(.day, from: val)
        {
            let today = cal.component(.day, from: NSDate() as Date)
            let date = cal.date(byAdding:.month, value:  -1, to: NSDate() as Date)
            
            //let date = cal.dateByAddingUnit(.Month, value: -1, toDate: NSDate(), options: [])
            
            days = (cal.component(.day, from: date!) - cal.component(.day, from: val)) + today
        } else
        {
            days = 0
            if months == 12
            {
                years = years + 1
                months = 0
            }
        }
        
        return years
    }
    

}
