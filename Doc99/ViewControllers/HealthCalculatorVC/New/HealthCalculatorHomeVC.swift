//
//  HealthCalculatorHomeVC.swift
//  Doc99
//
//  Created by MS-SUB-02 on 20/09/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class HealthCalculatorHomeVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout  {
    @IBOutlet weak var objCollectionView: UICollectionView!
    var arr = [[String: String]]()
    @IBOutlet weak var lblLastLogin: UILabel!
    @IBOutlet var btnHelthIndex:UIButton!
    @IBOutlet var btnReportAndHistory:UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        lblLastLogin.layer.cornerRadius=5.0

        let nib = UINib(nibName: "MenuCollectionCell", bundle: nil)
        self.objCollectionView.register(nib, forCellWithReuseIdentifier: "collectionMenuCell")
        
        APPDELEGATE.myTabBarController?.tabBar.isHidden=true
        
        
        arr = [["name": "BP Test", "image": "6.png"], ["name": "Blood Glucose Test", "image": "2.png"], ["name": "Uric Acid Test", "image": "3.png"] ,
               ["name": "Eye Test", "image": "7.png"],["name": "Aria Stroke Risk", "image": "5.png"],["name": "BMI and OST", "image": "1.png"],
               ["name": "Cholesterol", "image": "9.png"],["name": "SOA", "image": "12.png"],["name": "Waist hip Ratio", "image": "8.png"],
               ["name": "Pulmonary Test", "image": "10.png"],["name": "Child Growth Curve", "image": "11.png"]]

        //,["name": "Child Growth Curve", "image": "11.png"]
        
        
        if USERDEFAULT.value(forKey: "userID") != nil
        {
            btnReportAndHistory.isHidden = false
        }
        else
        {
            btnReportAndHistory.isHidden = true
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
         //For Survay button
        if USERDEFAULT.value(forKey: "userID") != nil{
            btnHelthIndex.isHidden = false
        } else {
            btnHelthIndex.isHidden = true
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // NSLog("%@", arr.count)
        return arr.count
        
    }
    
    // func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {}
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MenuCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier:"collectionMenuCell", for: indexPath as IndexPath) as! MenuCollectionCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        
        let arr1 = arr[indexPath.row]
        
        // let image: UIImage = UIImage(named: "image")
        
        cell.lblName.text = arr1["name"]
        cell.img.image = UIImage (named: (arr1["image"]! as String) )
        
        //NSLog("%@", cell.lblName.text as Any)
        
        //  cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
        // let strTitle : NSString=arrDict[indexPath.row] .valueForKey("TITLE") as NSString
        
        return cell
    }
    
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        
        if indexPath.row == 0
        {
            let vc = BloodPressureTestVC(
                nibName: "BloodPressureTestVC",
                bundle: nil)
            navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 1
        {
            let vc = BloodGlucoseTestVC(
                nibName: "BloodGlucoseTestVC",
                bundle: nil)
            navigationController?.pushViewController(vc,
                                                     animated: true)
        }
        else if indexPath.row == 2
        {
            let vc = UricAcidViewController(
                nibName: "UricAcidViewController",
                bundle: nil)
            navigationController?.pushViewController(vc,
                                                     animated: true)
        }
        else if indexPath.row == 3
        {
            let vc = IntraOcularPressureTestVC(nibName: "IntraOcularPressureTestVC",bundle: nil)
            navigationController?.pushViewController(vc,animated: true)
            
        }
        else if indexPath.row == 4
        {
            if USERDEFAULT.value(forKey: "ariaStrokeRiskTestInfoVC") != nil {
                let vc = AriaStrokeRiskTestVC(nibName: "AriaStrokeRiskTestVC", bundle: nil)
                navigationController?.pushViewController(vc, animated: true)
            } else {
                // one time display Aria Stroke Risk Test Info VC
                let ariaStrokeRiskTestInfoVC = AriaStrokeRiskTestInfoVC(nibName: "AriaStrokeRiskTestInfoVC", bundle: nil)
                ariaStrokeRiskTestInfoVC.btnSkipIsHidden = false
                self.navigationController?.pushViewController(ariaStrokeRiskTestInfoVC, animated: true)
            }
         }
        else if indexPath.row == 5
        {
            let vc = BmiAndOstVC(
                nibName: "BmiAndOstVC",
                bundle: nil)
            navigationController?.pushViewController(vc,animated: true)
            
        }
        else if indexPath.row == 6
        {
            let vc = CholesterolTestVC(
                nibName: "CholesterolTestVC",
                bundle: nil)
            navigationController?.pushViewController(vc,animated: true)
        }
        else if indexPath.row == 7
        {
            let vc = SoaTestVC(
                nibName: "SoaTestVC",
                bundle: nil)
            navigationController?.pushViewController(vc,animated: true)
        }
        else if indexPath.row == 8
        {
            let vc = WaistTestVC(
                nibName: "WaistTestVC",
                bundle: nil)
            navigationController?.pushViewController(vc,animated: true)
        }
        else if indexPath.row == 9
        {
            let vc = PulmonaryTestVC(nibName: "PulmonaryTestVC",bundle: nil)
            navigationController?.pushViewController(vc,animated: true)
        }
        else if indexPath.row == 10
        {
            let vc = ChildGrowthTestVC(nibName: "ChildGrowthTestVC",bundle: nil)
            navigationController?.pushViewController(vc,animated: true)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let hw = objCollectionView.frame.size.width/3 - 20
        return CGSize(width: hw, height: hw)
    }

    //MARK:-
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnReportListClicked(_ sender: UIButton) {
        let report  = ReportListVC(nibName: "ReportListVC", bundle: nil)
        self.navigationController?.pushViewController(report, animated: true)
    }
    
    @IBAction func btnReportAndHistoryClicked(_ sender: UIButton) {
        let bloodPressureVC = ReportAndHistoryVC(nibName: "ReportAndHistoryVC", bundle: nil)
        
        self.navigationController?.pushViewController(bloodPressureVC, animated: true)
    }
    
    @IBAction func btnIndexClicked(_ sender: UIButton)
    {
        
        let bloodPressureVC = HealthIndexTestVC(nibName: "HealthIndexTestVC", bundle: nil)
        self.navigationController?.pushViewController(bloodPressureVC, animated: true)

    }
   }
