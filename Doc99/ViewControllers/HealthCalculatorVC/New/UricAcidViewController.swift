//
//  UricAcidViewController.swift
//  Doc99
//
//  Created by MS-SUB-02 on 11/10/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class UricAcidViewController: UIViewController {
    @IBOutlet var viewBg:UIView!
    @IBOutlet var txtParameter:UITextField!
    @IBOutlet var scrMain:UIScrollView!
    
    var activeField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.NavBarNumberPad()
        self.addTapGestureInOurView()
        
        
        //viewBg.layer.borderWidth=0.5
        //viewBg.layer.borderColor=UIColor.lightGray.cgColor
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.scrMain.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        if viewTouched!.isKind(of: UIButton.self){
            
        }
        else{
            self.view.endEditing(true)
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        }
    }
    
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSaveRecordClicked(_ sender: UIButton) {
        
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
        if txtParameter.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter Parameters", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else{
            
            let bloodPressureVC = NewResultVC(nibName: "NewResultVC", bundle: nil)
            bloodPressureVC.isFromTwoValue = false
            bloodPressureVC.strFirst = String(format: "%@", txtParameter.text!) as NSString!
            bloodPressureVC.isFromIntraOcular = false
            bloodPressureVC.isFromUricAcid = true

            self.navigationController?.pushViewController(bloodPressureVC, animated: true)
            
            //            if Reachability.isConnectedToNetwork() == true {
            //                SVProgressHUD.show(withStatus: "Loading..")
            //                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForIntraOcularPressureTest), userInfo: nil, repeats: false)
            //            } else {
            //                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            //            }
        }
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
            
        }
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var YOffset:Int = 0
        
//        if (textField.tag==1) {
//            YOffset=200
//        }
        
        if (DeviceType.IS_IPHONE_4_OR_LESS) {
            
            if (textField.tag==1) {
                YOffset=0
            }
            
        }
        scrMain.setContentOffset(CGPoint(x: 0, y: CGFloat(textField.tag * YOffset)), animated: true)
        return true
        
    }
    
    
    
    
    // Numeric Keyboard Hide Method
    func NavBarNumberPad(){
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        numberToolbar.barStyle = .default
        
        let viewBack = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        viewBack.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        numberToolbar.addSubview(viewBack)
        
        let btnCancel = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelNumberPad))
        btnCancel.tintColor = UIColor.white
        
        let btnDone = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.doneWithNumberPad))
        btnDone.tintColor = UIColor.white
        
        numberToolbar.items = NSArray(objects: btnCancel,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),btnDone) as? [UIBarButtonItem]
        
        
        numberToolbar.sizeToFit()
        txtParameter.inputAccessoryView = numberToolbar
        
    }
    
    func cancelNumberPad(){
        txtParameter.resignFirstResponder()
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
        
    }
    
    func doneWithNumberPad(){
        txtParameter.resignFirstResponder()
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
    }
    
    
}
