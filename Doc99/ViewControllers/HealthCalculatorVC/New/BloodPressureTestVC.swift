//
//  BloodPressureTestVC.swift
//  Doc99
//
//  Created by MS-SUB-02 on 21/09/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD


class BloodPressureTestVC: UIViewController {
    @IBOutlet var viewBg:UIView!
    @IBOutlet var txtRate:UITextField!
    @IBOutlet var txtSystolic:UITextField!
    @IBOutlet var txtDiastolic:UITextField!
    @IBOutlet var scrMain:UIScrollView!


    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.NavBarNumberPad()
        self.addTapGestureInOurView()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //        viewBg.layer.borderWidth=0.5
        //        viewBg.layer.borderColor=UIColor.lightGray.cgColor
        // Do any additional setup after loading the view.
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.scrMain.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        if viewTouched!.isKind(of: UIButton.self){
            
        }
        else{
            self.view.endEditing(true)
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        }
    }
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnSaveRecordClicked(_ sender: UIButton) {
        
        if txtRate.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter heart rate", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if txtSystolic.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter systolic parameters", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if txtDiastolic.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter diastolic parameters", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
            
        else{
            
            let bloodPressureVC = NewResultVC(nibName: "NewResultVC", bundle: nil)
            bloodPressureVC.isFromTwoValue = true
            bloodPressureVC.isFromBloodPressure = true
            bloodPressureVC.strFirst = String(format: "%@", txtSystolic.text!) as NSString!
            bloodPressureVC.strSecound = txtDiastolic.text! as String as NSString!
            bloodPressureVC.strThird = txtRate.text! as String as NSString!

            self.navigationController?.pushViewController(bloodPressureVC, animated: true)

            
//                if Reachability.isConnectedToNetwork() == true {
//                    SVProgressHUD.show(withStatus: "Loading..")
//                    Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForBloodPresureTest), userInfo: nil, repeats: false)
//                } else {
//                    showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
//                }
            
        }

        
    }
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForBloodPresureTest(){
        let completeURL = NSString(format:"%@%@", MainURL,bloodPressureTestDataURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "heart_rate":txtRate.text as Any,
            "systolic":txtSystolic.text as Any,
            "diastolic":txtDiastolic.text as Any,
            "systolic_report":"20",
            "diastolic_report":"30"
        ]
        
        
        
    
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("editWeightData API Parameter :",finalParams)
        print("editWeightData API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: bloodPressureTestURLTag)
        
    }

    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case bloodPressureTestURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("editWeightData Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                
                let bloodPressureVC = ResultVC(nibName: "ResultVC", bundle: nil)
                bloodPressureVC.isFromTwoValue = true
                bloodPressureVC.isFromBloodPressure = true
                bloodPressureVC.strFirst = String(format: "%@", txtSystolic.text!) as NSString!
                bloodPressureVC.strSecound = txtDiastolic.text! as String as NSString!
                bloodPressureVC.strThird = txtRate.text! as String as NSString!

                self.navigationController?.pushViewController(bloodPressureVC, animated: true)

//                
//                self.weightProgramID = (resultDict.value(forKey: "data") as AnyObject).value(forKey: "wp_id") as! String
//                
//                let notificationName = Notification.Name("refreshMyWeightProgramData")
//                NotificationCenter.default.post(name: notificationName, object: nil)
//                
//                self.performSelector(inBackground: #selector(self.postDataOnWebserviceForGetSingleWeightProgram), with: nil)
//                
//                self.navigationController?.popViewController(animated: true)
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
                
            else if resultDict.value(forKey: "status") as! String == "2"{
//                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            
            SVProgressHUD.dismiss()
            break
            
            
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case bloodPressureTestURLTag:
            SVProgressHUD.dismiss()
            break
            
            default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
//    //MARK:- Notification Methods
//    
//    func keyboardDidShow(notification:NSNotification)
//    {
//        print("keyboard did show \(notification)")
//        
//        guard let dic = notification.userInfo else
//        {
//            print("user info is nil")
//            return
//        }
//        
//        let kbRect:CGRect = dic[UIKeyboardFrameEndUserInfoKey] as! CGRect
//        
//        let contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbRect.size.height, 0.0)
//        scrMain.contentInset = contentInsets
//        scrMain.scrollIndicatorInsets = contentInsets
//        
//        var aRect = self.view.frame
//        aRect.size.height -= kbRect.size.height
//        
//        if activeField != nil
//        {
//            if (!aRect.contains(activeField!.frame.origin))
//            {
//                scrMain.scrollRectToVisible((activeField!.frame), animated: true)
//            }
//        }
//        
//    }
//    
//    func keyboardWillBeHidden(notification:NSNotification)
//    {
//        print("keyboardWillBeHidden \(notification) )")
//        
//        let contentInsets = UIEdgeInsets.zero
//        scrMain.contentInset = contentInsets
//        scrMain.scrollIndicatorInsets = contentInsets
//    }
    // TODO: - DELEGATE METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
            
        }
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var YOffset:Int = 0
        /*
        if (textField.tag==1) {
            YOffset=75
        }else if (textField.tag==2){
            YOffset=70
        }else if (textField.tag==3){
            YOffset=70
        }
        */
        if (DeviceType.IS_IPHONE_4_OR_LESS) {
            
            if (textField.tag==1) {
                YOffset=0
            }else if (textField.tag==2){
                YOffset=14
            }else if (textField.tag==3){
                YOffset=30
            }
        }
        //scrMain.setContentOffset(CGPoint(x: 0, y: CGFloat(textField.tag * YOffset)), animated: true)
        return true
    }
    
    //MARK: - Keyboard Delegate Methods
    @objc func keyboardWillShow(notification:NSNotification){
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = scrMain.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scrMain.contentInset = contentInset
    }
    
    
    @objc func keyboardWillHide(notification:NSNotification){
        let contentInset:UIEdgeInsets = .zero
        scrMain.contentInset = contentInset
    }
    
    
    // Numeric Keyboard Hide Method
    func NavBarNumberPad(){
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        numberToolbar.barStyle = .default
        
        let viewBack = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        viewBack.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        numberToolbar.addSubview(viewBack)
        
        let btnCancel = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelNumberPad))
        btnCancel.tintColor = UIColor.white
        
        let btnDone = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.doneWithNumberPad))
        btnDone.tintColor = UIColor.white
        
        numberToolbar.items = NSArray(objects: btnCancel,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),btnDone) as? [UIBarButtonItem]
        
        
        numberToolbar.sizeToFit()
        txtRate.inputAccessoryView = numberToolbar
        txtSystolic.inputAccessoryView = numberToolbar
        txtDiastolic.inputAccessoryView = numberToolbar

    }
    
    func cancelNumberPad(){
        txtRate.resignFirstResponder()
        txtSystolic.resignFirstResponder()
        txtDiastolic.resignFirstResponder()
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)


    }
    
    func doneWithNumberPad(){
        txtRate.resignFirstResponder()
        txtSystolic.resignFirstResponder()
        txtDiastolic.resignFirstResponder()
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)

    }
    
    
    
    
}
