//
//  AreaStrokeRiskTestInfoVC.swift
//  Doc99
//
//  Created by Hardik on 01/12/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class AriaStrokeRiskTestInfoVC: UIViewController, UIWebViewDelegate {
    
    @IBOutlet var webViewAboutUS:UIWebView!
    @IBOutlet var lblHeaderAboutus:UILabel!
    
    @IBOutlet weak var viewSkip: UIView!
    @IBOutlet weak var constraintViewSkipHeight: NSLayoutConstraint!
    
    var btnSkipIsHidden: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewSkip.isHidden = btnSkipIsHidden
        if btnSkipIsHidden {
            constraintViewSkipHeight.constant = 0
        } else {
            constraintViewSkipHeight.constant = 45
        }
        generalViewControllerSetting()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSkip(_ sender: UIButton) {
        USERDEFAULT.set("false", forKey: "ariaStrokeRiskTestInfoVC")
        let vc = AriaStrokeRiskTestVC(nibName: "AriaStrokeRiskTestVC", bundle: nil)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func getDataFromAPI() {
        
    }
    
    func generalViewControllerSetting(){
        //webViewPrescriptionTC.loadRequest(URLRequest(url: URL(string: "http://hardwirestudios.com")!))
        
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetAboutUS), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        self.setLocalizationText()
    }
    
    
    func setLocalizationText(){
        lblHeaderAboutus.text = NSLocalizedString("ARIA Stroke Test", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
    }
    
    // TODO: - DELEGATE METHODS
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("WebView Error : ",error.localizedDescription)
    }
    
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForGetAboutUS(){
        
        let completeURL = NSString(format:"%@%@", MainURL,cmsWebPageCallURL) as String
        let params:NSDictionary = [
            "page" : "ARIA"
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("TermCondition API Parameter :",finalParams)
        print("TermCondition API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: cmsWebPageCallURLTag)
    }
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case cmsWebPageCallURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("TermCondition Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                
                let myDescription = resultDict.value(forKey: "page_description") as! String
                let abc = myDescription.replacingOccurrences(of: "\n", with: "")
                webViewAboutUS.loadHTMLString(abc, baseURL: nil)
                
                let myTitle = resultDict.value(forKey: "page_title") as? String ?? "ARIA Stroke Test"
                lblHeaderAboutus.text = myTitle
            }
            break
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case cmsWebPageCallURLTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
