//
//  MenuCollectionCell.swift
//  S_BPL
//
//  Created by MS-SUB-02 on 29/06/17.
//  Copyright © 2017 MS-SUB-02. All rights reserved.
//

import UIKit

class MenuCollectionCell: UICollectionViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var viewBg: UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        viewBg.layer.cornerRadius=2.0
        viewBg.layer.borderWidth=0.5
        viewBg.layer.borderColor=UIColor.lightGray.cgColor
        
              // Initialization code
    }

}
