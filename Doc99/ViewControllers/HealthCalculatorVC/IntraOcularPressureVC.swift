//
//  IntraOcularPressureVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 02/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class IntraOcularPressureVC: UIViewController {
    
    // FIXME: - VIEW CONTROLLER METHODS
    @IBOutlet var btnSelect1:UIButton!
    @IBOutlet var btnSelect2:UIButton!
    @IBOutlet var btnSelect3:UIButton!
    @IBOutlet var btnSelect4:UIButton!
    
    @IBOutlet var imageViewSelect1:UIImageView!
    @IBOutlet var imageViewSelect2:UIImageView!
    @IBOutlet var imageViewSelect3:UIImageView!
    @IBOutlet var imageViewSelect4:UIImageView!
    
    @IBOutlet var lblSelect1:UILabel!
    @IBOutlet var lblSelect2:UILabel!
    @IBOutlet var lblSelect3:UILabel!
    @IBOutlet var lblSelect4:UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
    }
    func setSelectedButton(radioButton:UIButton) {
        let image = UIImage.init(named: "health_check.png")
        let image1 = UIImage.init(named: "health_Uncheck.png")
        
        if radioButton == btnSelect1{
            imageViewSelect1.image = image
            imageViewSelect2.image = image1
            imageViewSelect3.image = image1
            imageViewSelect4.image = image1
            print("Selected : ",lblSelect1.text ?? "Nothing")
            
            APPDELEGATE.healthIndexData.setValue(lblSelect1.text, forKey: "intraOcular")
            
            
        }
        else if radioButton == btnSelect2{
            imageViewSelect2.image = image
            imageViewSelect3.image = image1
            imageViewSelect4.image = image1
            imageViewSelect1.image = image1
            print("Selected : ",lblSelect2.text ?? "Nothing")
            
            APPDELEGATE.healthIndexData.setValue(lblSelect2.text, forKey: "intraOcular")
            
        }
        else if radioButton == btnSelect3{
            imageViewSelect3.image = image
            imageViewSelect4.image = image1
            imageViewSelect1.image = image1
            imageViewSelect2.image = image1
            print("Selected : ",lblSelect3.text ?? "Nothing")
            
            APPDELEGATE.healthIndexData.setValue(lblSelect3.text, forKey: "intraOcular")
            
        }
        else if radioButton == btnSelect4{
            imageViewSelect4.image = image
            imageViewSelect1.image = image1
            imageViewSelect2.image = image1
            imageViewSelect3.image = image1
            print("Selected : ",lblSelect4.text ?? "Nothing")
            
            APPDELEGATE.healthIndexData.setValue(lblSelect4.text, forKey: "intraOcular")
        }
        
    }

    
    // TODO: - DELEGATE METHODS
    
    
    // TODO: - ACTION METHODS
    @IBAction func btnNextClicked(_ sender: UIButton) {
        let ariaStrokeRiskVC = AriaStrokeRiskVC(nibName: "AriaStrokeRiskVC", bundle: nil)
        self.navigationController?.pushViewController(ariaStrokeRiskVC, animated: true)
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnOcularPressureSelectionClicked(_ sender: UIButton) {
        
        if sender == btnSelect1{
            self.setSelectedButton(radioButton: btnSelect1)
        }
        else if sender == btnSelect2{
            self.setSelectedButton(radioButton: btnSelect2)
        }
        else if sender == btnSelect3{
            self.setSelectedButton(radioButton: btnSelect3)
        }
        else if sender == btnSelect4{
            self.setSelectedButton(radioButton: btnSelect4)
        }
        
    }

    
    
    // TODO: - POST DATA METHODS
    
}
