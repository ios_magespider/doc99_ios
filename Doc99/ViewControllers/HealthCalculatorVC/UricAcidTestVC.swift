//
//  UricAcidTestVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 02/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class UricAcidTestVC: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UIAlertViewDelegate  {
    
    // FIXME: - VIEW CONTROLLER METHODS
    @IBOutlet var btnSelect1:UIButton!
    @IBOutlet var btnSelect2:UIButton!
   // @IBOutlet var btnSelect3:UIButton!
   // @IBOutlet var btnSelect4:UIButton!
    
    @IBOutlet var imageViewSelect1:UIImageView!
    @IBOutlet var imageViewSelect2:UIImageView!
  //  @IBOutlet var imageViewSelect3:UIImageView!
  //  @IBOutlet var imageViewSelect4:UIImageView!
    
    @IBOutlet var lblSelect1:UILabel!
    @IBOutlet var lblSelect2:UILabel!
   // @IBOutlet var lblSelect3:UILabel!
   // @IBOutlet var lblSelect4:UILabel!
    
    @IBOutlet var viewOptions:UIView!

    
    @IBOutlet var viewPicker:UIView!
    @IBOutlet var pickerView:UIPickerView!
    var pickerSelIndx:Int!
    var pickOption:NSMutableArray!
    @IBOutlet var txtOption:UITextField!
    @IBOutlet var btnOption:UIButton!

    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // TODO: - OTHER METHODS
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        
        pickerSelIndx=0
        pickOption = NSMutableArray()
        pickOption = ["Sporty lifestyle","Catch a cold","Gouty limbs"]
        pickerView.reloadAllComponents()
        viewPicker.frame = CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: viewPicker.frame.size.width, height: viewPicker.frame.size.height)
        
        //  self.addTapGestureInOurView()
    }
    
    
    func setSelectedButton(radioButton:UIButton) {
        let image = UIImage.init(named: "health_check.png")
        let image1 = UIImage.init(named: "health_Uncheck.png")
        
        if radioButton == btnSelect1{
            imageViewSelect1.image = image
            imageViewSelect2.image = image1
           // imageViewSelect3.image = image1
            //imageViewSelect4.image = image1
            btnOption.isHidden=true
            txtOption.isHidden=true
            viewOptions.isHidden=true

            print("Selected : ",lblSelect1.text ?? "Nothing")
            
            APPDELEGATE.healthIndexData.setValue("20", forKey: "uricAcid")
        }
        else if radioButton == btnSelect2{
            imageViewSelect2.image = image
          //  imageViewSelect3.image = image1
          //  imageViewSelect4.image = image1
            imageViewSelect1.image = image1
            btnOption.isHidden=false
            txtOption.isHidden=false
            viewOptions.isHidden=false

            print("Selected : ",lblSelect2.text ?? "Nothing")
            if txtOption.text == "Sporty lifestyle"
            {
                APPDELEGATE.healthIndexData.setValue("21", forKey: "uricAcid")
            }
            else  if txtOption.text == "Catch a cold"
            {
                APPDELEGATE.healthIndexData.setValue("22", forKey: "uricAcid")
            }
            else  if txtOption.text == "Gouty limbs"
            {
                APPDELEGATE.healthIndexData.setValue("23", forKey: "uricAcid")
            }
            else
            {
            APPDELEGATE.healthIndexData.setValue(nil, forKey: "uricAcid")
            }
        }
//        else if radioButton == btnSelect3{
//            imageViewSelect3.image = image
//            imageViewSelect4.image = image1
//            imageViewSelect1.image = image1
//            imageViewSelect2.image = image1
//            print("Selected : ",lblSelect3.text ?? "Nothing")
//            
//            APPDELEGATE.healthIndexData.setValue("22", forKey: "uricAcid")
//        }
//        else if radioButton == btnSelect4{
//            imageViewSelect4.image = image
//            imageViewSelect1.image = image1
//            imageViewSelect2.image = image1
//            imageViewSelect3.image = image1
//            print("Selected : ",lblSelect4.text ?? "Nothing")
//            APPDELEGATE.healthIndexData.setValue("23", forKey: "uricAcid")
//        }
//        
    }
    

    
    // MARK: - PICKERVIEW DELEGATE
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickOption.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        // return (self.pickOption[row] as AnyObject).value(forKey: "title") as? String
        return self.pickOption[row] as? String
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50.0
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let label = UILabel()
        label.textAlignment = NSTextAlignment.center
        
        label.font = UIFont.systemFont(ofSize: 20.0)
        label.text = self.pickOption[row] as? String
        return label
    }
    
    
    // TODO: - ACTION METHODS
    @IBAction func btnNextClicked(_ sender: UIButton) {
        
  
        
        
        if APPDELEGATE.healthIndexData.value(forKey: "uricAcid") == nil{
            
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please select Option", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )

            
            
        }
        else if APPDELEGATE.healthIndexData.value(forKey: "uricAcid") != nil{
            
            let pharmacistAdviceVC = PharmacistAdviceVC(nibName: "PharmacistAdviceVC", bundle: nil)
            self.navigationController?.pushViewController(pharmacistAdviceVC, animated: true)

            
            
        }
        else{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please select UricAcid", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        

        
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
        APPDELEGATE.healthIndexData.setValue(nil, forKey: "uricAcid")

    }
    
    @IBAction func btnUricTestSelectionClicked(_ sender: UIButton) {
        
        if sender == btnSelect1{
            self.setSelectedButton(radioButton: btnSelect1)
        }
        else if sender == btnSelect2{
            self.setSelectedButton(radioButton: btnSelect2)
        }
//        else if sender == btnSelect3{
//            self.setSelectedButton(radioButton: btnSelect3)
//        }
//        else if sender == btnSelect4{
//            self.setSelectedButton(radioButton: btnSelect4)
//        }
        
    }

    
    
    @IBAction func btnSelectOptionClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y - self.viewPicker.frame.size.height, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
            
        })
    }
    @IBAction func cancelPicker(sender:AnyObject){
        UIView.animate(withDuration: 0.3) {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
        }
        
        
    }
    
    @IBAction func donePicker(sender:UIButton){
        
        
        pickerSelIndx = pickerView.selectedRow(inComponent: 0)
        
        print("Picker Index is  :-> \(pickerSelIndx) and value is \(pickOption[pickerSelIndx])")
        txtOption.text = self.pickOption[pickerSelIndx] as? String
        //dishID = ((self.dishData[pickerSelIndx] as AnyObject).value(forKey: "_id") as? String)!

        
        if txtOption.text == "Sporty lifestyle"
        {
            APPDELEGATE.healthIndexData.setValue("21", forKey: "uricAcid")
        }
        else  if txtOption.text == "Catch a cold"
        {
            APPDELEGATE.healthIndexData.setValue("22", forKey: "uricAcid")
        }
        else  if txtOption.text == "Gouty limbs"
        {
            APPDELEGATE.healthIndexData.setValue("23", forKey: "uricAcid")
        }

        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
        })
    }
    
}
