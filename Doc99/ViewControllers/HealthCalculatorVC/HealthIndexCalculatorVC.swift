//
//  HealthIndexCalculatorVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 02/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class HealthIndexCalculatorVC: UIViewController,UIWebViewDelegate {
    
    // FIXME: - VARIABLE
    
    @IBOutlet var webViewPrescriptionTC:UIWebView!
    
    
    @IBOutlet var lblEprescription:UILabel!
    @IBOutlet var lblTermCondition:UILabel!
    @IBOutlet var lblStart:UILabel!
    
    
    @IBOutlet var loginView:UIView!
    @IBOutlet var viewProfileData:UIView!

    
    //LOGIN POPUP
    @IBOutlet var loginPopupLabel2:UILabel!
    @IBOutlet var loginPopupLabel3:UILabel!
    
    // MARK: - VIEW CONTROLLER METHODS
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.generalViewControllerSetting()

        if USERDEFAULT.value(forKey: "userID") != nil{
            
            if let isMember = USERDEFAULT.value(forKey: "isMember") as? String{
                let ismem = "\(isMember)"
                if ismem == "1"{
                }
            }
        }
        
        
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetPrescriptionTermCondition), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        
        // Define identifier
        let notificationName1 = Notification.Name("updateAfterMemberShip")
        
                // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateAfterMembershipCompleted), name: notificationName1, object: nil)
        
        
//        int now = int.Parse(DateTime.Today.ToString("yyyyMMdd"));
//        int dob = int.Parse(dateDOB.ToString("yyyyMMdd"));
//        string dif = (now - dob).ToString();
//        string age = "0";
//        if (dif.Length > 4)
//        age = dif.Substring(0, dif.Length - 4);
//        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = false
        
        
        
        
//        if USERDEFAULT.value(forKey: "userID") != nil{
//            
//            if let isMember = USERDEFAULT.value(forKey: "isMember") as? String{
//                let ismem = "\(isMember)"
//                if ismem == "1"{
//                }
//                else{
//                    loginPopupLabel3.text = "MEMBERSHIP"
//                    self.view.addSubview(loginView)
//                }
//            }
//            else{
//                
//            }
//        }
//        else{
//            loginPopupLabel3.text = "LOGIN"
//            self.view.addSubview(loginView)
//        }
//
//        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // TODO: - OTHER METHODS
    func updateAfterMembershipCompleted() {
        self.generalViewControllerSetting()
        loginView.removeFromSuperview()
    }

    func generalViewControllerSetting(){
        //webViewPrescriptionTC.loadRequest(URLRequest(url: URL(string: "http://hardwirestudios.com")!))
        
//        if Reachability.isConnectedToNetwork() == true {
//            SVProgressHUD.show(withStatus: "Loading..")
//            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetPrescriptionTermCondition), userInfo: nil, repeats: false)
//        } else {
//            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
//        }
        
        self.setLocalizationText()
    }
    
    func setLocalizationText(){
        
        lblEprescription.text = NSLocalizedString("Health Index Calculator", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTermCondition.text = NSLocalizedString("Terms & Conditions", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblStart.text = NSLocalizedString("START", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
        loginPopupLabel2.text = NSLocalizedString("Just enrolled to Doc99 app to experience it amazing features.", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        loginPopupLabel3.text = NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

    }
    
    
    // TODO: - DELEGATE METHODS
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("WebView Error : ",error.localizedDescription)
    }
    
    // TODO: - ACTION METHODS
    @IBAction func btnStartClicked(_ sender: UIButton) {
        
        if USERDEFAULT.value(forKey: "userID") != nil{
            
            if USERDEFAULT.value(forKey: "gender") as? String == "" {
                let bloodPressureVC = Doc99ChemistTestVC(nibName: "Doc99ChemistTestVC", bundle: nil)
                self.navigationController?.pushViewController(bloodPressureVC, animated: true)
            }
            else{
                let healthCalc = HealthCalculatorHomeVC(nibName: "HealthCalculatorHomeVC", bundle: nil)
                self.navigationController?.pushViewController(healthCalc, animated: true)
            }
        }
        else{
            let bloodPressureVC = Doc99ChemistTestVC(nibName: "Doc99ChemistTestVC", bundle: nil)
            self.navigationController?.pushViewController(bloodPressureVC, animated: true)
        }
        
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnLoginClicked(_ sender:UIButton){
        
        if USERDEFAULT.value(forKey: "userID") != nil{
            
            if let isMember = USERDEFAULT.value(forKey: "isMember") as? String{
                let ismem = "\(isMember)"
                if ismem == "1"{
                    
                }
                else{
                    let joinMemberVC = JoinMemberVC(nibName: "JoinMemberVC", bundle: nil)
                    joinMemberVC.isFromOther = true
                    joinMemberVC.isFromTab = "2"
                    self.navigationController?.pushViewController(joinMemberVC, animated: true)
                }
            }
            else{
                
            }
        }
        else{
            let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
            loginVC.isFromTab = "2"
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        
        
    }
    @IBAction func btnCancelClicked(_ sender:UIButton){
        
        
        
//       let homeVC = TabBarVC(nibName: "TabBarVC", bundle: nil)
//        homeVC.selectedIndexOfMyTabbarController = 1
//        APPDELEGATE.navigationC  = UINavigationController(rootViewController: homeVC)


    }
      @IBAction func btnBack1Clicked(_ sender:UIButton){
        viewProfileData.removeFromSuperview()

    self.navigationController?.popViewController(animated: true)

    }
    @IBAction func btnReportListClicked(_ sender: UIButton) {
        let reportListVC = ReportListVC(nibName: "ReportListVC", bundle: nil)
        self.navigationController?.pushViewController(reportListVC, animated: true)
    }

    
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForGetPrescriptionTermCondition(){
        
        let completeURL = NSString(format:"%@%@", MainURL,cmsWebPageCallURL) as String
        let params:NSDictionary = [
            "page" : "hicterms"
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("TermCondition API Parameter :",finalParams)
        print("TermCondition API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: cmsWebPageCallURLTag)
    }
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case cmsWebPageCallURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("TermCondition Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                
                let myDescription = resultDict.value(forKey: "page_description") as! String
                let abc = myDescription.replacingOccurrences(of: "\n", with: "")
                webViewPrescriptionTC.loadHTMLString(abc, baseURL: nil)

            }
            break
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case cmsWebPageCallURLTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
        
}
