//
//  ReportListVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 13/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class ReportListVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate {

    @IBOutlet var tableViewReport:UITableView!

    var reportData = NSMutableArray()
    var reportGlobalData = NSMutableArray()
    
    var pageNum1:Int!
    var isLoading1:Bool?
    var refreshControl: UIRefreshControl!
    @IBOutlet var lblHeader:UILabel!
    var healthCalculateData = NSMutableArray()

    @IBOutlet var lblNoReport:UILabel!

    
    // FIXME: - VIEW CONTROLLER METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        //APPDELEGATE.myTabBarController?.tabBar.isHidden = false
    }
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        
        pageNum1=1;
        let indicator2 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator2.startAnimating()
        
        self.reportData = NSMutableArray()
        self.reportGlobalData = NSMutableArray()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetHealthReport), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        self.addPullRefreshForReportList()
        self.setLocalizationText()
    }
    
    func setLocalizationText(){
        lblHeader.text = NSLocalizedString("Report", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
    }
    
    func addPullRefreshForReportList(){
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.gray
        refreshControl.addTarget(self, action: #selector(self.refreshReportList(sender:)), for: .valueChanged)
        tableViewReport.addSubview(refreshControl)
    }
    
    func refreshReportList(sender:AnyObject) {
        // Code to refresh table view
        
        pageNum1=1;
        let indicator2 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator2.startAnimating()
        
        self.reportData = NSMutableArray()
        self.reportGlobalData = NSMutableArray()
        self.tableViewReport.reloadData()

        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetHealthReport), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        refreshControl.endRefreshing()
    }
    
    

    
    func addLoadingIndicatiorOnFooterOnTableView(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableViewReport.tableFooterView = spinner
    }
    func removeLoadingIndicatiorOnFooterOnTableView(){
        tableViewReport.tableFooterView = nil
    }

    // TODO: - DELEGATE ScrollView
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
         if scrollView == tableViewReport {
            if isLoading1 == true{
                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                    pageNum1 = pageNum1 + 1
                    print(pageNum1)
                    isLoading1 = false
                    
                    if Reachability.isConnectedToNetwork() == true {
                        self.addLoadingIndicatiorOnFooterOnTableView()
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetHealthReport), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                    
                }
            }
            
        }
        
        
    }

    
    // TODO: - DELEGATE METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.reportData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "reportCell"
        var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ReportListTableVC
        
        if cell == nil {
            let nib  = Bundle.main.loadNibNamed("ReportListTableVC", owner: self, options: nil)
            cell = nib?[0] as? ReportListTableVC
        }
        cell!.selectionStyle = .none;
        
        
        
        if let reportDate =  (self.reportData[indexPath.row] as AnyObject).value(forKey: "health_date") as? String
        {
            //"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
           let report = "\(reportDate)"
            let a =  getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: report, newFormat: "dd MMM yyyy")
            cell?.lblDate.text = "\(a)"
        }
        
        /*
        if let bmi =  (self.reportData[indexPath.row] as AnyObject).value(forKey: "bmi") as? String
        {
            cell?.lblBMI.text = "BMI:\(bmi)"
        }
        else if let bmi =  (self.reportData[indexPath.row] as AnyObject).value(forKey: "bmi") as? NSNumber
        {
            cell?.lblBMI.text = "BMI:\(bmi)"
        }
        */

        
        /*
        if let ost =  (self.reportData[indexPath.row] as AnyObject).value(forKey: "ost") as? String
        {
            cell?.lblOST.text = "OST:\(ost)"
        }
        else if let ost =  (self.reportData[indexPath.row] as AnyObject).value(forKey: "ost") as? NSNumber
        {
            cell?.lblOST.text = "OST:\(ost)"
        }
        */

        if let total =  (self.reportData[indexPath.row] as AnyObject).value(forKey: "totalscore") as? String
        {
            cell?.lblOST.text = "\(total)/200"
        }
        else if let total =  (self.reportData[indexPath.row] as AnyObject).value(forKey: "totalscore") as? NSNumber
        {
            cell?.lblOST.text = "\(total)/200"
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var bMIValue:String!
        var ostValue:String!
        if let bmi =  (self.reportData[indexPath.row] as AnyObject).value(forKey: "bmi") as? String
        {
            bMIValue = "\(bmi)"
        }
        else if let bmi =  (self.reportData[indexPath.row] as AnyObject).value(forKey: "bmi") as? NSNumber
        {
            bMIValue = "\(bmi)"
        }
        
        
        
        if let ost =  (self.reportData[indexPath.row] as AnyObject).value(forKey: "ost") as? String
        {
            ostValue = "\(ost)"
        }
        else if let ost =  (self.reportData[indexPath.row] as AnyObject).value(forKey: "ost") as? NSNumber
        {
            ostValue = "\(ost)"
        }
        
        healthCalculateData = NSMutableArray()
        
        
        
        if let diastolic_presssure = NumberFormatter().number(from: (self.reportData[indexPath.row] as AnyObject).value(forKey: "diastolic_presssure") as! String) {
            healthCalculateData.add(CGFloat(diastolic_presssure))
        }
        
        if let nutrition = NumberFormatter().number(from: (self.reportData[indexPath.row] as AnyObject).value(forKey: "nutrition") as! String)
        {
            healthCalculateData.add(CGFloat(nutrition))
        }
        
        
        
        
        if let excercise = NumberFormatter().number(from: (self.reportData[indexPath.row] as AnyObject).value(forKey: "excercise") as! String) {
            healthCalculateData.add(CGFloat(excercise))
        }
        
        if let advice = NumberFormatter().number(from: (self.reportData[indexPath.row] as AnyObject).value(forKey: "advice") as! String) {
            healthCalculateData.add(CGFloat(advice))
        }
        
     //   healthCalculateData.add(CGFloat(100.0))

//        if let lasttotalscore = NumberFormatter().number(from: (self.reportData[indexPath.row] as AnyObject).value(forKey: "lasttotalscore") as! String) {
//            healthCalculateData.add(CGFloat(100.0))
//        }
        
        let dic:NSMutableDictionary = NSMutableDictionary()
        if let lasttotalscore = (self.reportData[indexPath.row] as AnyObject).value(forKey: "totalscore") as? String{
            let lastTot = "\(lasttotalscore)"
            
            if let lasttotalscore = NumberFormatter().number(from: lastTot) {
                healthCalculateData.add(CGFloat(lasttotalscore))
                dic["score"] = lasttotalscore
            }
            
        }
        else if let lasttotalscore = (self.reportData[indexPath.row] as AnyObject).value(forKey: "totalscore") as? NSNumber{
            healthCalculateData.add(CGFloat(lasttotalscore))
            dic["score"] = lasttotalscore
        }
        
        if let healthDate = (self.reportData[indexPath.row] as AnyObject).value(forKey: "health_date") as? String{
            dic["health_date"] = healthDate
        }
       
        let array:NSMutableArray = NSMutableArray()
        array.add(dic)
        
        print("HealthCalculated Data",healthCalculateData)

        let reportVC = ReportVC(nibName: "ReportVC", bundle: nil)
        reportVC.bmiValue = bMIValue
        reportVC.ostValue = ostValue
        reportVC.arrLineChartData = array.mutableCopy() as? NSArray
        reportVC.barChartYValuesData = healthCalculateData.mutableCopy() as! NSArray
        self.navigationController?.pushViewController(reportVC, animated: true)
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
    
    
    
    // TODO: - ACTION METHODS
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForGetHealthReport(){
        
        let completeURL = NSString(format:"%@%@", MainURL,healthReportURL) as String
        
        let pageNumber = "\(pageNum1!)"
        // "page":pageNumber,
        // "limit":PAGINATION_LIMITE
        
        let params:NSDictionary = [
             "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
             "token": USERDEFAULT.value(forKey: "token") as! String,
            "page":pageNumber,
            "limit":PAGINATION_LIMITE,
            "lang_type":Language_Type
        ]
        

        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetHealthReport API Parameter :",finalParams)
        print("GetHealthReport API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: healthReportURLTag)
        
        
    }
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        
        case healthReportURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("GetHealthReport List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum1 == 1{
                    self.reportData = NSMutableArray()
                    self.reportGlobalData = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        self.reportGlobalData.add(myData[i])
                    }
                    
                    for i in 0...myData.count - 1 {
                        self.reportData.add(myData[i])
                    }
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum1 > 0) {
                            self.pageNum1 = self.pageNum1 - 1
                        }
                        self.isLoading1 = false
                    }else{
                        self.isLoading1 = true
                    }
                }
                else{
                    self.isLoading1 = false
                    if (self.pageNum1 > 0) {
                        self.pageNum1 = self.pageNum1 - 1
                    }
                    
                }
                
                if self.reportData.count == 0{
                    self.tableViewReport.isHidden = true
                    self.lblNoReport.isHidden = false
                }
                else{
                    self.tableViewReport.isHidden = false
                    self.lblNoReport.isHidden = true
                }

                
                self.tableViewReport.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
    
        case healthReportURLTag:
            self.isLoading1 = false
            if (self.pageNum1 > 0) {
                self.pageNum1 = self.pageNum1 - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
            
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    // TODO: - GOING FOR LOGIN

    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }
}
