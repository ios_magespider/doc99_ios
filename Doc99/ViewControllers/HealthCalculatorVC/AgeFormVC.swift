//
//  AgeFormVC.swift
//  Doc99
//
//  Created by MS-SUB-02 on 29/09/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class AgeFormVC: UIViewController {
    @IBOutlet var btnSelect1:UIButton!
    @IBOutlet var btnSelect2:UIButton!
    
    @IBOutlet var imageViewSelect1:UIImageView!
    @IBOutlet var imageViewSelect2:UIImageView!
    
    @IBOutlet var lblSelect1:UILabel!
    @IBOutlet var lblSelect2:UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setSelectedButton(radioButton:UIButton) {
        let image = UIImage.init(named: "health_check.png")
        let image1 = UIImage.init(named: "health_Uncheck.png")
        

        if radioButton == btnSelect1{
            imageViewSelect1.image = image
            imageViewSelect2.image = image1
            print("Selected : ",lblSelect1.text ?? "Nothing")
            APPDELEGATE.healthIndexData.setValue("1", forKey: "age")
        }
        else if radioButton == btnSelect2{
            imageViewSelect2.image = image
            imageViewSelect1.image = image1
            print("Selected : ",lblSelect2.text ?? "Nothing")
            APPDELEGATE.healthIndexData.setValue("2", forKey: "age")
            
        }
   
    }
    
    // TODO: - DELEGATE METHODS
    
    
    // TODO: - ACTION METHODS
    @IBAction func btnBackClicked(_ sender: UIButton)
    {
        _  = self.navigationController?.popViewController(animated: true)
        APPDELEGATE.healthIndexData.setValue(nil, forKey: "Age")


    }

    @IBAction func btnNextClicked(_ sender: UIButton)
    {
        
        if APPDELEGATE.healthIndexData.value(forKey: "age") != nil{

        let bloodPressureVC = BloodPressureVC(nibName: "BloodPressureVC", bundle: nil)
        self.navigationController?.pushViewController(bloodPressureVC, animated: true)
        }
        else
        {
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please select Age", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        
    }
    
    @IBAction func btnAriaStrockSelectionClicked(_ sender: UIButton) {
        
        if sender == btnSelect1{
            self.setSelectedButton(radioButton: btnSelect1)
        }
        else if sender == btnSelect2{
            self.setSelectedButton(radioButton: btnSelect2)
        }
       
        
    }
}
