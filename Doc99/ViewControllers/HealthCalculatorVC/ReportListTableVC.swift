//
//  ReportListTableVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 13/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class ReportListTableVC: UITableViewCell {

    @IBOutlet var lblDate:UILabel!
    @IBOutlet var lblBMI:UILabel!
    @IBOutlet var lblOST:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
