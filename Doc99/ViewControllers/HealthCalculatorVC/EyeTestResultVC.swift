//
//  EyeTestResultVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 11/06/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class EyeTestResultVC: UIViewController, UIWebViewDelegate {

    //MARK:- IBOutlets
    @IBOutlet weak var viewUserDetail: UIView!
    @IBOutlet weak var labelTestName: UILabel!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelUserGender: UILabel!
    @IBOutlet weak var labelReportDate: UILabel!
    @IBOutlet var viewBg:UIView!
    @IBOutlet weak var labelLeftSight: UILabel!
    @IBOutlet weak var labelRightSight: UILabel!
    @IBOutlet weak var labelIOPValue: UILabel!
    
    @IBOutlet weak var labelRiskDescription: UILabel!
    @IBOutlet weak var webViewDetail: UIWebView!
    @IBOutlet weak var webViewHeight: NSLayoutConstraint!
    
    
    //LOGIN POPUP
    @IBOutlet var loginView:UIView!
    @IBOutlet var loginPopupLabel2:UILabel!
    @IBOutlet var loginPopupLabel3:UILabel!
    
    //MARK:- Variabels
    var reportDate = ""
    var isFromReport = false
    var leftEyeSight = ""
    var rightEyeSight = ""
    var iOPValue = ""
    var isFirstCall = true
    var ssImage : UIImage!
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBg.layer.borderWidth=0.5
        viewBg.layer.borderColor=UIColor.lightGray.cgColor
        
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.view.addSubview(loginView)
        loginView.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        loginView.frame = self.view.frame
        if USERDEFAULT.value(forKey: "userID") != nil{
            
            self.setData()
            
            labelTestName.text = "Eye Test"
            labelUserName.text = USERDEFAULT.value(forKey: "fullName") as? String ?? ""
            labelUserGender.text = USERDEFAULT.value(forKey: "gender") as? String ?? ""
            
            if Utility.isEmpty(reportDate){
                let dd = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy/MM/dd"
                labelReportDate.text = formatter.string(from: dd)
            }
            else{
                labelReportDate.text = reportDate
            }
            
            loginView.removeFromSuperview()
        }
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:-
    func setData() {
        
        if !isFromReport{
            if Reachability.isConnectedToNetwork() == true{
                if isFirstCall == true {
                    SVProgressHUD.show(withStatus: "Loading..")
                    Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForIntraOcularPressureTest), userInfo: nil, repeats: false)
                }
            } else {
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }
        
        labelLeftSight.text = leftEyeSight
        labelRightSight.text = rightEyeSight
        labelIOPValue.text = iOPValue
       
        let left = Float(leftEyeSight)!
        if left < 5.0{
            labelLeftSight.textColor = UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
        }
        else{
            labelLeftSight.textColor = .red
        }
        
        let right = Float(rightEyeSight)!
        if right < 5.0{
            labelRightSight.textColor = UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
        }
        else{
            labelRightSight.textColor = .red
        }
        
        
        let iop = Float(iOPValue)!
        if iop < 15{
            //low
            labelRiskDescription.text = "Your Result is between the Low Risk"
            labelRiskDescription.textColor = UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
        }
        else if iop > 15 && iop < 21 {
            //Medium
            labelRiskDescription.text = "Your Result is between the Medium Risk"
            labelRiskDescription.textColor = .red
        }
        else if iop > 21{
            //high
            labelRiskDescription.text = "Your Result is between the High Risk"
            labelRiskDescription.textColor = .red
        }
        
        
        webViewDetail.loadHTMLString("<html><head><link href=\"https://fonts.googleapis.com/css?family=Lato\" rel=\"stylesheet\"></head><body style=\"background-color:#ECECEC;\"><font face = \"Lato\" size = \"2.5\"><p>Normal Range<br/>左眼視力 LE Sight < 5.0<br/>右眼視力 RE Sight < 5.0<br/><br/>眼壓 Intra-Ocular Pressure 12 - 20 mmHgHigh risk >21 mmHg<br/>Medium risk 16-20 mmHg<br/>Low risk <15 mmHg</p></font></body></html>", baseURL: nil)
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        webViewDetail.scrollView.isScrollEnabled = false
        webViewHeight.constant = webViewDetail.scrollView.contentSize.height
    }
    
    //MARK:-
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnShareReport(_ sender: UIButton)
    {
        
        UIGraphicsBeginImageContextWithOptions(viewBg.bounds.size, viewBg.isOpaque, 0.0)
        viewBg.drawHierarchy(in: viewBg.bounds, afterScreenUpdates: false)
        let snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        ssImage = snapshotImageFromMyView!
        
        let  activityViewController = UIActivityViewController(activityItems: [self,ssImage], applicationActivities: [])
        
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop ]
        
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func btnLoginClicked(_ sender:UIButton){
        
        if USERDEFAULT.value(forKey: "userID") as? String != nil{
            
            if let isMember = USERDEFAULT.value(forKey: "isMember") as? String{
                let ismem = "\(isMember)"
                if ismem == "1"{
                }
                else{
                    let joinMemberVC = JoinMemberVC(nibName: "JoinMemberVC", bundle: nil)
                    joinMemberVC.isFromOther = true
                    joinMemberVC.isFromTab = "6"
                    self.navigationController?.pushViewController(joinMemberVC, animated: true)
                }
            }
            else{
                
            }
        }
        else{
            let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
            loginVC.isFromTab = "6"
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        
    }
    
    
    
    @IBAction func btnHomeClicked(_ sender: UIButton) {
        var viewControllers = navigationController?.viewControllers
        viewControllers?.removeLast(2) // views to pop
        navigationController?.setViewControllers(viewControllers!, animated: true)
    }
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForIntraOcularPressureTest(){
        let completeURL = NSString(format:"%@%@", MainURL,intraOcularPressureTestDataURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "iop":iOPValue,
            ]
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("iop API Parameter :",finalParams)
        print("iop API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: intraOcularPressureTestURLTag)
        
    }
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case intraOcularPressureTestURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("iop Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                self.isFirstCall = false
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                //                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            
            SVProgressHUD.dismiss()
            break

        default:
            break
            
        }
    }
    
    
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        
        case intraOcularPressureTestURLTag:
            SVProgressHUD.dismiss()
            break
        
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
}
