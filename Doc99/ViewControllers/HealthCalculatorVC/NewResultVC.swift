//
//  NewResultVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 29/05/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class NewResultVC: UIViewController,UIWebViewDelegate {
    
    //MARK:-
    
    
    @IBOutlet weak var viewUserDetail: UIView!
    @IBOutlet weak var labelTestName: UILabel!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelUserGender: UILabel!
    @IBOutlet weak var labelReportDate: UILabel!
    
    @IBOutlet weak var detailScrollView: UIScrollView!
    
    @IBOutlet weak var webViewDetail: UIWebView!
    @IBOutlet weak var webViewHeight: NSLayoutConstraint!
    
    @IBOutlet var viewBg:UIView!
    @IBOutlet var viewTwoValue:UIView!
    @IBOutlet var lblFirstOne:UILabel!
    @IBOutlet var lblSecoundOne:UILabel!
    @IBOutlet var lblOnlyOne:UILabel!
    @IBOutlet var lblFirst:UILabel!
    @IBOutlet var lblSecound:UILabel!
    @IBOutlet var lblFinalOne:UILabel!
    @IBOutlet var lblTitle:UILabel!
    @IBOutlet var lblSingalTitle:UILabel!
    
    @IBOutlet var lblSingalBottom:UILabel!
    
    @IBOutlet var lblTitleSecoundFoeBloodGlucose:UILabel!
    
    @IBOutlet var lblFirstBottom:UILabel!
    @IBOutlet var lblSecoundBottom:UILabel!
    
    
    //MARK:-
    
    var reportDate = ""
    var isFromReport = false
    var strFirst:NSString?
    var strSecound:NSString?
    var strThird:NSString?
    
    var isFromTwoValue:Bool!
    
    var isFromBloodPressure:Bool!
    var isFromAreaStoke:Bool!
    
    var isFromIntraOcular:Bool!
    var isFromAriaStroke:Bool!
    var isFromUricAcid:Bool!
    
    
    var gen = ""
    var age = ""
    var tScore = ""
    var isFirstCall:Bool!
    var fromHistory : Bool = false
    
    
    var username: String = ""
    var secound: String = ""
    var textSharing: String = ""
    
    var userName : String = ""
    var gender : String = ""
    var date : String = ""
    
    var ssImage : UIImage!
    
    
    //LOGIN POPUP
    @IBOutlet var loginView:UIView!
    @IBOutlet var loginPopupLabel2:UILabel!
    @IBOutlet var loginPopupLabel3:UILabel!
    
    //MARK:-
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewBg.layer.borderWidth=0.5
        viewBg.layer.borderColor=UIColor.lightGray.cgColor
        
        isFirstCall = true
        
        loginView.setNeedsLayout()
        loginView.layoutIfNeeded()
        if USERDEFAULT.value(forKey: "userID") != nil{
            
            if let isMember = USERDEFAULT.value(forKey: "isMember") as? String{
                let ismem = "\(isMember)"
                if ismem == "1"{
                }
                else{
                    loginPopupLabel3.text = "MEMBERSHIP"
                    self.view.addSubview(loginView)
                    self.addtopLeftViewConstraints()
                    
                    loginView.setNeedsLayout()
                    loginView.layoutIfNeeded()
                    
                }
            }
            else{
                
            }
        }
        else{
            
            loginPopupLabel3.text = "LOGIN"
            self.view.addSubview(loginView)
            self.addtopLeftViewConstraints()
            
            loginView.setNeedsLayout()
            loginView.layoutIfNeeded()
            
        }
        
    }
    
    
    override func viewDidLayoutSubviews() {
        
        loginView.frame = self.view.frame
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.addSubview(loginView)
        loginView.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        loginView.frame = self.view.frame
        if USERDEFAULT.value(forKey: "userID") != nil{
            self.setData()
            
            labelUserName.text = USERDEFAULT.value(forKey: "fullName") as? String ?? ""
            labelUserGender.text = USERDEFAULT.value(forKey: "gender") as? String ?? ""
            
            if Utility.isEmpty(reportDate){
                let dd = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy/MM/dd"
                labelReportDate.text = formatter.string(from: dd)
            }
            else{
                labelReportDate.text = reportDate
            }
            
            
            loginView.removeFromSuperview()
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
   
    func setData()
    {
        
        if isFromTwoValue == true
        {
            viewTwoValue.isHidden  = false
            
            if isFromBloodPressure == true
            {
                //Blood Pressure Result
                
                if !isFromReport{
                    if Reachability.isConnectedToNetwork() == true{
                        if isFirstCall == true {
                            SVProgressHUD.show(withStatus: "Loading..")
                            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForBloodPresureTest), userInfo: nil, repeats: false)
                        }
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
                
                labelTestName.text = "Blood Pressure"
                lblTitle.text = "Blood Pressure"
                lblTitleSecoundFoeBloodGlucose.text = "(Distolic pressure Test)"
                
                webViewDetail.loadHTMLString("<html><head><link href=\"https://fonts.googleapis.com/css?family=Lato\" rel=\"stylesheet\"></head><body style=\"background-color:#ECECEC;\"><font face = \"Lato\" size = \"2.5\"><p><b>Heart Rate : 70 Per/min (Male/Female)</b></br>Sphygmomanometer/Korotkoff Sound Measurements</br></br><b>Systolic : 90-140 mmHg (Male/Female)</b></br>Sphygmomanometer/Korotkoff Sound Measurements</br></br><b>Diastolic : 60-90 mmHg (Male/Female)</b></br>Sphygmomanometer/Korotkoff Sound Measurements</p></font></body></html>", baseURL: nil)
                
                
                if let first = strFirst as String?
                {
                    
                    lblFirst.text = "Systolic"
                    lblFirstOne.text = (first == "") ? "0" : first
                    
                    lblFirstBottom.text = "mmHg"
                    
                    let a = Int(first)
                    switch  a! {
                    case 90 ... 140:
                        lblFirstOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                    default:
                        lblFirstOne.textColor=UIColor.red
                    }
                    
                }
                if let  second = strSecound as String?
                {
                    
                    lblSecound.text = "Diastolic"
                    
                    lblSecoundOne.text = "\(second)"
                    
                    lblSecoundBottom.text = "mmHg"
                    
                    
                    let b = Int(second)
                    switch  b! {
                    case 60 ... 90:
                        lblSecoundOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                    default:
                        lblSecoundOne.textColor=UIColor.red
                    }
                    
                    
                }
                
                lblFinalOne.text = "Your result is above the standard.Please consult the specialist."
                
                
                
            }
            else if isFromAreaStoke == true
            {
                
                labelTestName.text = "ARIA Stroke Test"
                lblTitle.text = "ARIA Stroke Test"
                
                
                webViewDetail.loadHTMLString("<html><head><link href=\"https://fonts.googleapis.com/css?family=Lato\" rel=\"stylesheet\"></head><body style=\"background-color:#ECECEC;\"><font face = \"Lato\" size = \"2.5\"><p style=\"color:rgb(146,146,146);\">ARIA:</p><b>0.406 Left Eye</b></br><b>0.548 Right Eye</b><p style=\"color:rgb(146,146,146);\">CRAE Both Stroke risk study on retinal characteristics includingCentral retinal artery equivalent (CRAE),</br>Central retinal vein equivalent (CRVE).</br></br>Retinal images are captured and uploaded to a cloud server database. The system analyses the photo and determines whether the stroke risk is low, moderate or high.</br></br>Subjects with a probability of less than 0.5 can be considered low risk. Entry suggest that a probability of 0.7 or above as high risk. The range of 0.5 - 0.7 is considered moderate risk.</p></font></body></html>",
                    baseURL: nil)
                
                lblTitleSecoundFoeBloodGlucose.text = ""
                
                if !isFromReport{
                    if Reachability.isConnectedToNetwork() == true {
                        SVProgressHUD.show(withStatus: "Loading..")
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceAriaStrokeRiskTest), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
                
                
                if let first = strFirst as String?
                {
                    
                    lblFirst.text = "Left Eye"
                    lblFirstOne.text = (first == "") ? "0" : first//"\(first)"
                    lblFirstBottom.text = ""
                    
                    
                    let a = Float(first)
                    switch  a! {
                    case 0.3857 ... 0.4263:
                        lblFirstOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                    default:
                        lblFirstOne.textColor=UIColor.red
                    }
                    
                }
                if let  second = strSecound as String?
                {
                    
                    lblSecound.text = "Right Eye"
                    
                    lblSecoundOne.text = "\(second)"
                    
                    lblSecoundBottom.text = ""
                    
                    let b = Float(second)
                    switch  b! {
                    case 0.4932 ... 0.5754:
                        lblSecoundOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                    default:
                        lblSecoundOne.textColor=UIColor.red
                    }
                    
                    
                }
                lblFinalOne.text = "Your result is above the standard.Please consult the specialist."
                
                
                
            }
            else
            {
                
                labelTestName .text = "Blood Glucose Test"
                lblTitle.text = "Blood Glucose Test"
                lblTitleSecoundFoeBloodGlucose.text = ""
                
                webViewDetail.isHidden = true
                
                if !isFromReport{
                    if Reachability.isConnectedToNetwork() == true {
                        SVProgressHUD.show(withStatus: "Loading..")
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForBloodGlucoseTest), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
                
                if let first = strFirst as String?
                {
                    var val = 0
                    if(first == "")
                    {
                        val = 0
                    }
                    else
                    {
                        val = Int(first)!
                    }
                    lblFirst.text = "HbA1C"
                    lblFirstOne.text = (first == "") ? "0" : first//"\(first)"
                    lblFirstBottom.text = "percent"
                   
                    
                    let a = Float(val)
                    switch  a{
                    case 0 ... 6.5:
                        lblFirstOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                    default:
                        lblFirstOne.textColor=UIColor.red
                    }
                    
                }
                if let  second = strSecound as String?
                {
                    
                    lblSecound.text = strThird! as String
                    
                    lblSecoundOne.text = "\(second)"
                    
                    lblSecoundBottom.text = "mmol/L"
                    
                    if strThird == "Fasting Blood Glucose"
                    {
                        let b = Float(second)
                        switch  b! {
                        case 3.9 ... 6.1:
                            lblSecoundOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                        default:
                            lblSecoundOne.textColor=UIColor.red
                        }
                    }
                    else if strThird == "Postprandial 1 Hour"
                    {
                        let b = Float(second)
                        switch  b! {
                        case 7.8 ... 9.0:
                            lblSecoundOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                        default:
                            lblSecoundOne.textColor=UIColor.red
                        }
                    }
                        
                    else if strThird == "Postprandial 2 Hour"
                    {
                        let b = Float(second)
                        switch  b! {
                        case 0 ... 7.8:
                            lblSecoundOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                        default:
                            lblSecoundOne.textColor=UIColor.red
                        }
                    }
                    else
                    {
                        
                    }
                }
            }
            
            viewBg.addSubview(viewTwoValue)
            viewBg .bringSubview(toFront: viewTwoValue)
        }
        else
        {
            lblTitleSecoundFoeBloodGlucose.text = ""
            
            viewTwoValue.isHidden  = true
            if let first = strFirst as String?
            {
                if isFromIntraOcular == true
                {
                    labelTestName.text = "Eye Test"
                    lblTitle.text = "Eye Test"
                    
                       webViewDetail.loadHTMLString("<html><head><link href=\"https://fonts.googleapis.com/css?family=Lato\" rel=\"stylesheet\"></head><body style=\"background-color:#ECECEC;\"><font face = \"Lato\" size = \"2.5\"><p>Normal Range<br/>左眼視力 LE Sight < 5.0<br/>右眼視力 RE Sight < 5.0<br/><br/>眼壓 Intra-Ocular Pressure 12 - 20 mmHgHigh risk >21 mmHg<br/>Medium risk 16-20 mmHg<br/>Low risk <15 mmHg</p></font></body></html>", baseURL: nil)
                    
                    if !isFromReport{
                        if Reachability.isConnectedToNetwork() == true {
                            SVProgressHUD.show(withStatus: "Loading..")
                            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForIntraOcularPressureTest), userInfo: nil, repeats: false)
                        } else {
                            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                        }
                    }
                    
                    
                    lblSingalTitle.text = "IOP"
                    lblOnlyOne.text = "\(first)"
                    lblSingalBottom.text = "mmHg"
                    
                    // let a = Float(first)
                    
                    
                    
                    
                    let iop = Float(first)!
                    if iop < 15{
                        //low
                        lblFinalOne.text = "Your Result is between the Low Risk"
                        lblFinalOne.textColor = UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                    }
                    else if iop > 15 && iop < 21 {
                        //Medium
                        lblFinalOne.text = "Your Result is between the Medium Risk"
                        lblFinalOne.textColor = .red
                    }
                    else if iop > 21{
                        //high
                        lblFinalOne.text = "Your Result is between the High Risk"
                        lblFinalOne.textColor = .red
                    }
                    
                }
                else if isFromUricAcid == true
                {
                    labelTestName.text = "Uric Acid Test"
                    lblTitle.text = "Uric Acid Test"
                    
                    webViewDetail.loadHTMLString("<html><head><link href=\"https://fonts.googleapis.com/css?family=Lato\" rel=\"stylesheet\"></head><body style=\"background-color:#ECECEC;\"><font face = \"Lato\" size = \"2.5\"><p style=\"color:rgb(146,146,146);\">尿酸的正常範圍 <br/>Uric Acid Normal Range:</p><b>0.15 - 0.42 mmol/L   Male Uricase/UV</b></br><b>0.15 - 0.42 mmol/L   Male Uricase/UV</b></font></body></html>",baseURL: nil)
                    
                    if !isFromReport{
                        if Reachability.isConnectedToNetwork() == true {
                            SVProgressHUD.show(withStatus: "Loading..")
                            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForUricAcidTest), userInfo: nil, repeats: false)
                        } else {
                            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                        }
                    }
                    lblSingalTitle.text = "Parameter"
                    lblOnlyOne.text = "\(first)"
                    lblSingalBottom.text = ""
                    
                    
                    
                    if USERDEFAULT.value(forKey: "gender")  != nil
                    {
                        let strGender = USERDEFAULT.value(forKey: "gender") as! String
                        
                        if  strGender == "Male"
                        {
                            switch  Float(first)! {
                            case 0.15 ... 0.42:
                                lblFinalOne.text="Your Result is between the standard.Keep Smiling"
                                lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                                
                            default:
                                lblOnlyOne.textColor=UIColor.red
                            }
                        }
                        else  if strGender == "Female"
                        {
                            switch  Float(first)! {
                            case 0.09 ... 0.36:
                                lblFinalOne.text="Your Result is between the standard.Keep Smiling"
                                lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                                
                            default:
                                lblOnlyOne.textColor=UIColor.red
                            }
                            
                        }
                        
                    }
                }
                else
                {
                    
                    labelTestName.text = "Osteoporosis Self Assessment Tool"
                    lblTitle.text = "Osteoporosis Self Assessment Tool"
                    
                    
                    
                    webViewDetail.loadHTMLString("<html><head><link href=\"https://fonts.googleapis.com/css?family=Lato\" rel=\"stylesheet\"></head><body style=\"background-color:#ECECEC;\"><font face = \"Lato\" size = \"2.5\"><p><p style=\"color:rgb(170,170,170);\">Scale:</p><b>(-20)  to  (-1) Score : High Risk</b></br><b>(-1)  to  3 Score : Moderate Risk</b></br><b>4  to  20 Score: Low Risk</b></br></br><p style=\"color:rgb(170,170,170);\">Formula:</p><b>OST = 0.2*(Weight - Age)</b></br></br><p style=\"color:rgb(170,170,170);\">Reference:<br/>Adler RA, Tran MT, Petkov VI, Performance of the Osteoporosis Self-assessment screening tool for osteoporosis in American men. Mayo Clin Proc. 2003 June;78(6):723-7.</p></p></font></body></html>", baseURL: nil)
                    
                    if(tScore != "")
                    {
                        lblSingalTitle.text = "T-Score"
                        let tscore  = Double(tScore)!
                        lblOnlyOne.text = "\(tScore)"
                     
                        if (tscore >= Double(-1.0)) {
                          
                            lblFinalOne.text="Your result is between the standard. \n Keep Smiling."
                            lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                            //Tscore.setTextColor(getResources().getColor(R.color.green));
                        } else if (tscore < Double(-1.0) && tscore > Double(-2.5)) {
                            lblFinalOne.text="Your result is above the standard. \n Please consult your doctor."
                            lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                           
                        } else if (tscore <= Double(-2.5)) {
                            lblFinalOne.text="Your result is below the standard. \n Please consult your doctor."
                            lblOnlyOne.textColor=UIColor.red
                        }
                    }
                    else
                    {
                         lblSingalTitle.text = "OST"
                        let Weight:String = strSecound! as String
                        let Weight1:Double = Double(Weight)!
                        
                        
                        let age:Double = Double(self.age)!
                        //EmailId
                        
                        let OST = 0.2 * (Weight1 - age)
                        lblOnlyOne.text = "\(OST)"
                        
                        if OST >= 4 && OST <= 16{
                            lblFinalOne.text="Your Result is between the Low Risk"
                            lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                        }
                        else if OST >= -1 && OST <= -3{
                            lblFinalOne.text="Your Result is between the Medium Risk"
                            lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                        }
                        else if OST >= -6 && OST <= -1{
                            lblFinalOne.text="Your Result is between the High Risk"
                            lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                        }
                        else{
                            lblOnlyOne.textColor=UIColor.red
                        }
                    }
                   
                    
                    
                    
                    /*
                    if APPDELEGATE.healthIndexData.value(forKey: "birthDateIs")  == nil
                    {
                        let age:Double = Double(self.calculateAge(dob: USERDEFAULT.value(forKey: "dob") as! String))
                        //EmailId
                        
                        let OST = 0.2 * (Weight1 - age)
                        lblOnlyOne.text = "\(OST)"
                        
                        if OST >= 4 && OST <= 16{
                            lblFinalOne.text="Your Result is between the Low Risk"
                            lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                        }
                        else if OST >= -1 && OST <= -3{
                            lblFinalOne.text="Your Result is between the Medium Risk"
                            lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                        }
                        else if OST >= -6 && OST <= -1{
                            lblFinalOne.text="Your Result is between the High Risk"
                            lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                        }
                        else{
                            lblOnlyOne.textColor=UIColor.red
                        }
                        
                    }
                    else
                    {
                        let age:Double = Double(self.calculateAge(dob: APPDELEGATE.healthIndexData.value(forKey: "birthDateIs") as! String))
                        
                        let OST = 0.2 * (Weight1 - age)
                        lblOnlyOne.text = "\(OST)"
                        
                        switch  Double(OST) {
                        case 4 ... 20:
                            lblFinalOne.text="Your Result is between the Low Risk"
                            lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                            
                        case -1 ... 3:
                            lblFinalOne.text="Your Result is between the Medium Risk"
                            lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                            
                        case -20 ... 2:
                            lblFinalOne.text="Your Result is between the High Risk"
                            lblOnlyOne.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                            
                            
                        default:
                            lblOnlyOne.textColor=UIColor.red
                        }
                    }
                    */
                    
                    //  EmailId
                    
                    if !isFromReport{
                        if Reachability.isConnectedToNetwork() == true {
                            SVProgressHUD.show(withStatus: "Loading..")
                            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForBMIAndOSTTest), userInfo: nil, repeats: false)
                        } else {
                            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                        }
                    }
                   // lblSingalTitle.text = "OST"
                    lblSingalBottom.text = ""
                    
                }
                
            }
            
            
        }
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        webViewDetail.scrollView.isScrollEnabled = false
        webViewHeight.constant = webViewDetail.scrollView.contentSize.height
    }
    
    @IBAction func btnHomeClicked(_ sender: UIButton) {
        
        
        
        var viewControllers = navigationController?.viewControllers
        viewControllers?.removeLast(2) // views to pop
        navigationController?.setViewControllers(viewControllers!, animated: true)
        
        //        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-3] animated:YES];
        
        // let bloodPressureVC = ResultVC(nibName: "ResultVC", bundle: nil)
        //self.navigationController?.pushViewController(bloodPressureVC, animated: true)
        
        
    }
    @IBAction func btnShareReport(_ sender: UIButton)
    {
        
        UIGraphicsBeginImageContextWithOptions(viewBg.bounds.size, viewBg.isOpaque, 0.0)
        viewBg.drawHierarchy(in: viewBg.bounds, afterScreenUpdates: false)
        let snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        ssImage = snapshotImageFromMyView!
       
        let  activityViewController = UIActivityViewController(activityItems: [self,ssImage], applicationActivities: [])
        
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop ]
        
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnLoginClicked(_ sender:UIButton){
        
        if USERDEFAULT.value(forKey: "userID") as? String != nil{
            
            if let isMember = USERDEFAULT.value(forKey: "isMember") as? String{
                let ismem = "\(isMember)"
                if ismem == "1"{
                    
                }
                else{
                    let joinMemberVC = JoinMemberVC(nibName: "JoinMemberVC", bundle: nil)
                    joinMemberVC.isFromOther = true
                    joinMemberVC.isFromTab = "6"
                    self.navigationController?.pushViewController(joinMemberVC, animated: true)
                }
            }
            else{
                
            }
        }
        else{
            let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
            loginVC.isFromTab = "6"
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        
        
    }
    
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForBloodPresureTest(){
        let completeURL = NSString(format:"%@%@", MainURL,bloodPressureTestDataURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "heart_rate":strThird!,
            "systolic":strFirst!,
            "diastolic":strSecound!,
            "systolic_report":"20",
            "diastolic_report":"30"
        ]
        
        
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("editWeightData API Parameter :",finalParams)
        print("editWeightData API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: bloodPressureTestURLTag)
        
    }
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForIntraOcularPressureTest(){
        let completeURL = NSString(format:"%@%@", MainURL,intraOcularPressureTestDataURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "iop":strFirst!,
            ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("editWeightData API Parameter :",finalParams)
        print("editWeightData API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: intraOcularPressureTestURLTag)
        
    }
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceAriaStrokeRiskTest(){
        let completeURL = NSString(format:"%@%@", MainURL,ariaStrokeDataURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "left_eye":strFirst!,
            "right_eye":strSecound!,
            "left_eye_report":"20",
            "right_eye_report":"30",
            ]
        
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("editWeightData API Parameter :",finalParams)
        print("editWeightData API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: ariaStrokeDataURLTag)
        
    }
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForBloodGlucoseTest(){
        let completeURL = NSString(format:"%@%@", MainURL,bloodGlucoseTestDataURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "hba1c":strFirst!,
            "option":strThird!,
            "param":strSecound!,
            "hba1c_report":"20",
            "option_report":"30"
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("editWeightData API Parameter :",finalParams)
        print("editWeightData API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: bloodGlucoseTestURLTag)
        
    }
    func postDataOnWebserviceForBMIAndOSTTest(){
        let completeURL = NSString(format:"%@%@", MainURL,osteoporosisAssetmentToolDataURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "ost":lblOnlyOne.text!,
            "weight":strSecound!,
            "weight_in":strThird!,
             "t_score":tScore,
            "ost_report":"20",
            ]
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("editWeightData API Parameter :",finalParams)
        print("editWeightData API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: osteoporosisAssetmentToolURLTag)
        
    }
    
    func postDataOnWebserviceForUricAcidTest(){
        let completeURL = NSString(format:"%@%@", MainURL,uricAcidDataURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "uat_param":strFirst!,
            "uat_param_report":"0.432"
            
            
        ]
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("editWeightData API Parameter :",finalParams)
        print("editWeightData API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: uricAcidDataURLTag)
        
    }
    
    
    
    // MARK:-
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case bloodPressureTestURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("editWeightData Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                isFirstCall = false
                
                //
                //                self.weightProgramID = (resultDict.value(forKey: "data") as AnyObject).value(forKey: "wp_id") as! String
                //
                //                let notificationName = Notification.Name("refreshMyWeightProgramData")
                //                NotificationCenter.default.post(name: notificationName, object: nil)
                //
                //                self.performSelector(inBackground: #selector(self.postDataOnWebserviceForGetSingleWeightProgram), with: nil)
                //
                //                self.navigationController?.popViewController(animated: true)
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
                
            else if resultDict.value(forKey: "status") as! String == "2"{
                //                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            
            SVProgressHUD.dismiss()
            break
        case intraOcularPressureTestURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("editWeightData Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
                
            else if resultDict.value(forKey: "status") as! String == "2"{
                //                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            
            SVProgressHUD.dismiss()
            break
            
        case ariaStrokeDataURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("editWeightData Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
                
            else if resultDict.value(forKey: "status") as! String == "2"{
                //                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            
            SVProgressHUD.dismiss()
            break
            
        case bloodGlucoseTestURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("editWeightData Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
                
            else if resultDict.value(forKey: "status") as! String == "2"{
                //                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            
            SVProgressHUD.dismiss()
            break
        case osteoporosisAssetmentToolURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("editWeightData Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
                
            else if resultDict.value(forKey: "status") as! String == "2"{
                //                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            
            SVProgressHUD.dismiss()
            break
        case uricAcidDataURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("editWeightData Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
   
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                //                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case bloodPressureTestURLTag:
            SVProgressHUD.dismiss()
            break
        case intraOcularPressureTestURLTag:
            SVProgressHUD.dismiss()
            break
        case ariaStrokeDataURLTag:
            SVProgressHUD.dismiss()
            break
        case bloodGlucoseTestURLTag:
            SVProgressHUD.dismiss()
            break
        case osteoporosisAssetmentToolURLTag:
            SVProgressHUD.dismiss()
            break
        case uricAcidDataURLTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    func addtopLeftViewConstraints()
    {
        
        NSLayoutConstraint(item: loginView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: loginView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: loginView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: loginView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1.0, constant: 0.0).isActive = true
        
    }
    
    
    
    func calculateAge(dob : String) -> Int{
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let date = df.date(from: dob)
        NSLog("%@", dob)
        guard let val = date else{
            return (0)
        }
        var years = 0
        var months = 0
        var days = 0
        
        let cal = NSCalendar.current
        years = cal.component(.year, from: NSDate() as Date) -  cal.component(.year, from: val)
        
        let currMonth = cal.component(.month, from: NSDate() as Date)
        let birthMonth = cal.component(.month, from: val)
        
        //get difference between current month and birthMonth
        months = currMonth - birthMonth
        //if month difference is in negative then reduce years by one and calculate the number of months.
        if months < 0
        {
            years = years - 1
            months = 12 - birthMonth + currMonth
            if cal.component(.day, from: NSDate() as Date) < cal.component(.day, from: val){
                months = months - 1
            }
        } else if months == 0 && cal.component(.day, from: NSDate() as Date) < cal.component(.day, from: val)
        {
            years = years - 1
            months = 11
        }
        
        //Calculate the days
        if cal.component(.day, from: NSDate() as Date) > cal.component(.day, from: val){
            days = cal.component(.day, from: NSDate() as Date) - cal.component(.day, from: val)
        }
        else if cal.component(.day, from: NSDate() as Date) < cal.component(.day, from: val)
        {
            let today = cal.component(.day, from: NSDate() as Date)
            let date = cal.date(byAdding:.month, value:  -1, to: NSDate() as Date)
            
            //let date = cal.dateByAddingUnit(.Month, value: -1, toDate: NSDate(), options: [])
            
            days = (cal.component(.day, from: date!) - cal.component(.day, from: val)) + today
        } else
        {
            days = 0
            if months == 12
            {
                years = years + 1
                months = 0
            }
        }
        
        return years
    }
    

    
}
