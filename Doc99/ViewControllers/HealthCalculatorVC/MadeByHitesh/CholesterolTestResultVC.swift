//
//  CholesterolTestResultVC.swift
//  Doc99
//
//  Created by Hitesh Prajapati on 24/11/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import  SVProgressHUD

class CholesterolTestResultVC: UIViewController, UIWebViewDelegate {

    
    //MARK: IBOutlets
    
    
    @IBOutlet weak var viewUserDetail: UIView!
    @IBOutlet weak var labelTestName: UILabel!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelUserGender: UILabel!
    @IBOutlet weak var labelReportDate: UILabel!
    
    @IBOutlet weak var webViewDetail: UIWebView!
    @IBOutlet weak var webViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var labelTriglycerideMeasure: UILabel!
    @IBOutlet weak var labelTotalCholesterolMeasure: UILabel!
    @IBOutlet weak var labelHdlCholesterolMeasure: UILabel!
    @IBOutlet weak var labelLdlCholesterolMeasure: UILabel!
    
    @IBOutlet weak var labelRiskDescription: UILabel!
    
    @IBOutlet weak var mainView: UIView!
    
    //LOGIN POPUP
    @IBOutlet var loginView: UIView!
    @IBOutlet var loginPopupLabel2:UILabel!
    @IBOutlet var loginPopupLabel3:UILabel!
    
    @IBOutlet weak var btnBackLogin: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    
    
    //MARK: Variables
    var reportDate = ""
    var strTriglyceride :NSString?
    var strTotalCholesterol :NSString?
    var strHdlCholesterol :NSString?
    var strLdlCholesterol :NSString?
    
    var isFromReport = false
    var fromHistory : Bool = false
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
       
        if USERDEFAULT.value(forKey: "userID") != nil{
            /*
            if let isMember = USERDEFAULT.value(forKey: "isMember") as? String{
                let ismem = "\(isMember)"
                if ismem == "1"{
                }
                else{
                    loginPopupLabel3.text = "MEMBERSHIP"
                    self.view.addSubview(loginView)
                    loginView.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
                }
            }
            else{
                
            }
            */
        }
        else{
            
            loginPopupLabel3.text = "LOGIN"
            self.view.addSubview(loginView)
            loginView.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if USERDEFAULT.value(forKey: "userID") != nil{
            mainView.clipsToBounds = true
            mainView.layer.borderWidth = 0.5
            mainView.layer.borderColor = UIColor.lightGray.cgColor
            loginView.removeFromSuperview()
            self.setReultData()
            
            labelUserName.text = USERDEFAULT.value(forKey: "fullName") as? String ?? ""
            labelUserGender.text = USERDEFAULT.value(forKey: "gender") as? String ?? ""
            
            
            if Utility.isEmpty(reportDate){
                let dd = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy/MM/dd"
                labelReportDate.text = formatter.string(from: dd)
            }
            else{
                labelReportDate.text = reportDate
            }
            
        }
        else{
            
            loginPopupLabel3.text = "LOGIN"
            self.view.addSubview(loginView)
            loginView.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:-
    func setReultData(){
        labelTestName.text = "Cholesterol Test"

        webViewDetail.loadHTMLString("<html><head><link href=\"https://fonts.googleapis.com/css?family=Lato\" rel=\"stylesheet\"></head><body style=\"background-color:#ECECEC;\"><font face = \"Lato\" size = \"2.5\"><p><b style=\"color:rgb(146,146,146);\">普通範圍</b> Normal Range:</br><b style=\"color:rgb(146,146,146);\">總膽固醇</b> TC<5.2 mmol/L<br/><b style=\"color:rgb(146,146,146);\">甘油三脂</b></b> TG:<1.7 mmol/L<br/><b style=\"color:rgb(146,146,146);\">高密度脂</b> HDL: >1.04 mmol/L<br/><b style=\"color:rgb(146,146,146);\">低密度脂</b> LDL: <3.37 mmol/L<br/><br/><b style=\"color:rgb(146,146,146);\">參考</b> Reference:<br/><b style=\"color:rgb(146,146,146);\">甘油三脂</b> Triglyceride (TG)<br/><b style=\"color:rgb(146,146,146);\">總膽固醇</b> Total Cholesterol (TC)<br/><b style=\"color:rgb(146,146,146);\">高密度脂 蛋白膽固醇</b> HDL Cholesterol (HDL)<br/><b style=\"color:rgb(146,146,146);\">低密度脂 蛋白膽固醇</b> LDL Cholesterol (LDL)<br/></p></font></body></html>", baseURL: nil)
        
        if !isFromReport{
            if Reachability.isConnectedToNetwork() == true {
                SVProgressHUD.show(withStatus: "Loading..")
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForCholesterolTest), userInfo: nil, repeats: false)
            } else {
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }
        if let a = strTriglyceride as String?, let b = strTotalCholesterol as String?, let c = strHdlCholesterol as String?, let d = strLdlCholesterol as String?{
            labelTriglycerideMeasure.text = "\(a)"
            
            let x = Float(a)
            
            if x! < 5.2 {
                labelTriglycerideMeasure.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
            }
            else{
                labelTriglycerideMeasure.textColor = .red
            }
            
            if((a as NSString).floatValue < 5.2) && ((b as NSString).floatValue < 1.7) && ((c as NSString).floatValue > 1.04) && ((d as NSString).floatValue < 3.37){
                labelRiskDescription.text = "Your Result is between the standard.Keep Smiling"
                labelRiskDescription.textColor = UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
            }
            else{
                labelRiskDescription.text = "Your result is above the standard.Please consult the specialist."
                labelRiskDescription.textColor = .red
            }
        }
        
        if let b = strTotalCholesterol as String?{
            labelTotalCholesterolMeasure.text = "\(b)"
            
            let x = Float(b)
            
            if x! < 1.7 {
                labelTotalCholesterolMeasure.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
            }
            else{
                labelTotalCholesterolMeasure.textColor = .red
            }
            
        }
        
        if let c = strHdlCholesterol as String?{
            labelHdlCholesterolMeasure.text = "\(c)"
            
            let x = Float(c)
            
            if x! > 1.04 {
                labelHdlCholesterolMeasure.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
            }
            else{
                labelHdlCholesterolMeasure.textColor = .red
            }
        }
        
        if let d = strLdlCholesterol as String?{
            labelLdlCholesterolMeasure.text = "\(d)"
            
            let x = Float(d)
            
            if x! < 3.37 {
                labelLdlCholesterolMeasure.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
            }
            else{
                labelLdlCholesterolMeasure.textColor = .red
            }
        }
    }
    
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        webViewDetail.scrollView.isScrollEnabled = false
        webViewHeight.constant = webViewDetail.scrollView.contentSize.height
    }
    
    //MARK:- IBActions
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnShareClicked(_ sender: Any) {
        
        UIGraphicsBeginImageContextWithOptions(mainView.bounds.size, mainView.isOpaque, 0.0)
        mainView.drawHierarchy(in: mainView.bounds, afterScreenUpdates: false)
        let snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let ssImage : UIImage = snapshotImageFromMyView!

        let  activityViewController = UIActivityViewController(activityItems: [self, ssImage], applicationActivities: [])
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    @IBAction func btnHomeClicked(_ sender: Any) {
        var viewControllers = navigationController?.viewControllers
        viewControllers?.removeLast(2) // views to pop
        navigationController?.setViewControllers(viewControllers!, animated: true)
    }
    
    @IBAction func btnLoginClicked(_ sender:UIButton){
        
        if USERDEFAULT.value(forKey: "userID") as? String != nil{
            
            if let isMember = USERDEFAULT.value(forKey: "isMember") as? String{
                let ismem = "\(isMember)"
                if ismem == "1"{
                    
                }
                else{
                    let joinMemberVC = JoinMemberVC(nibName: "JoinMemberVC", bundle: nil)
                    joinMemberVC.isFromOther = true
                    joinMemberVC.isFromTab = "6"
                    self.navigationController?.pushViewController(joinMemberVC, animated: true)
                }
            }
            else{
                
            }
        }
        else{
            let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
            loginVC.isFromTab = "6"
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
    }
    
    
    //MARK: Post Data Methods
    func postDataOnWebserviceForCholesterolTest(){
        let completeURL = NSString(format:"%@%@", MainURL,cholesterolReportURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "triglyceride_param":strTriglyceride!,
            "total_param":strTotalCholesterol!,
            "hdl_param":strHdlCholesterol!,
            "ldl_param":strLdlCholesterol!,
            "triglyceride_result":"0",
            "total_result":"0",
            "hdl_result":"0",
            "ldl_result":"0"
        ]
        
        
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("cholesterolReport API Parameter :",finalParams)
        print("cholesterolReport API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: cholesterolReportURLTag)
    }
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
            
        case cholesterolReportURLTag:
            
                let resultDict = responseObject as! NSDictionary;
                print("cholesterolReportURLTag Response  : \(resultDict)")
                
                if resultDict.value(forKey: "status") as! String == "1"{
                    
                }
                else if resultDict.value(forKey: "status") as! String == "0"{
                    showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
                }
                else if resultDict.value(forKey: "status") as! String == "3"{
                    
                }
                    
                else if resultDict.value(forKey: "status") as! String == "2"{
                    //                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
                }
            break
            
        default:
            break
            
        }
        SVProgressHUD.dismiss()
        
    }
    
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case cholesterolReportURLTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
}
