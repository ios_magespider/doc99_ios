//
//  CholesterolTestVC.swift
//  Doc99
//
//  Created by Hitesh Prajapati on 24/11/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import Contacts

class CholesterolTestVC: UIViewController, UITextFieldDelegate {

    
    //MARK: IBOutlets
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var DetailLabel: UILabel!
    @IBOutlet weak var ViewForTextFields: UIView!
    
    @IBOutlet weak var txtTriglycerideParams: UITextField!
    @IBOutlet weak var txtTotalChParams: UITextField!
    @IBOutlet weak var txtHDLchParams: UITextField!
    @IBOutlet weak var txtLDLchParams: UITextField!
    
    @IBOutlet weak var btnSave: UIButton!
    
    
    //MARK:- Variables
    var activeField: UITextField?
    
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NavBarNumberPad()
        self.addTapGestureInOurView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBActions
    @IBAction func btnBackClicked(_ sender: Any) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSaveRecordClicked(_ sender: Any) {
        if  txtTriglycerideParams.text == "" {
            showAlert(Appname, title: "Please Enter Triglyceride Parameter.")
        }
        else if txtTotalChParams.text == "" {
            showAlert(Appname, title: "Please Enter Total Cholesterol Parameter.")
        }
        else if txtHDLchParams.text == "" {
            showAlert(Appname, title: "Please Enter HDL Cholesterol Parameter.")
        }
        else if txtLDLchParams.text == "" {
            showAlert(Appname, title: "Please Enter LDL Cholesterol Parameter.")
        }
        else{
            let chResult = CholesterolTestResultVC(nibName: "CholesterolTestResultVC", bundle: nil)
            
            chResult.strTriglyceride = String(format: "%@", txtTriglycerideParams.text!) as NSString?
            chResult.strTotalCholesterol = String(format: "%@", txtTotalChParams.text!) as NSString?
            chResult.strHdlCholesterol = String(format: "%@", txtHDLchParams.text!) as NSString?
            chResult.strLdlCholesterol = String(format: "%@", txtLDLchParams.text!) as NSString?
            
            self.navigationController?.pushViewController(chResult, animated: true)
        }
        
    }
    
    
    
    //MARK: Other Methods
    //MARK: Method to hide keyboard on back tap.
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.mainScrollView.addGestureRecognizer(tapRecognizer)
    }
    
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        self.view.endEditing(true)
        mainScrollView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
    }
    //MARK: Numeric Keyboard Hide Method
    func NavBarNumberPad(){
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        numberToolbar.barStyle = .default
        
        let viewBack = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        viewBack.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        numberToolbar.addSubview(viewBack)
        
        let btnCancel = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelNumberPad))
        btnCancel.tintColor = UIColor.white
        
        let btnDone = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.doneWithNumberPad))
        btnDone.tintColor = UIColor.white
        
        numberToolbar.items = NSArray(objects: btnCancel,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),btnDone) as? [UIBarButtonItem]
        
        
        numberToolbar.sizeToFit()
        txtTriglycerideParams.inputAccessoryView = numberToolbar
        txtTotalChParams.inputAccessoryView = numberToolbar
        txtHDLchParams.inputAccessoryView = numberToolbar
        txtLDLchParams.inputAccessoryView = numberToolbar
        
    }
    
    func cancelNumberPad(){
        //txtIop.resignFirstResponder()
        mainScrollView.endEditing(false)
        mainScrollView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
    }
    
    func doneWithNumberPad(){
        //txtIop.resignFirstResponder()
        mainScrollView.endEditing(false)
        mainScrollView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
    }
    
    //MARK: Textfield Delegate Methods
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeField = textField
        
        
        return true
    }
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        let nextTage=textField.tag+1;
//        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
//        if (nextResponder != nil){
//            nextResponder?.becomeFirstResponder()
//        }
//        else
//        {
//            textField.resignFirstResponder()
//            mainScrollView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
//
//        }
//        return false
//    }
    
    
    //MARK: - Keyboard Delegate Methods
    @objc func keyboardWillShow(notification:NSNotification){

        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        
        // Get the existing contentInset for the scrollView and set the bottom property to be the height of the keyboard
        var contentInset = mainScrollView.contentInset
        contentInset.bottom = keyboardSize.height
        
        mainScrollView.contentInset = contentInset
        mainScrollView.scrollIndicatorInsets = contentInset
    }
    
    
    @objc func keyboardWillHide(notification:NSNotification){
        
        let contentInset:UIEdgeInsets = .zero
        mainScrollView.contentInset = contentInset
    }
    
}
