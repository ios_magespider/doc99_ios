//
//  WaistTestVC.swift
//  Doc99
//
//  Created by Hitesh Prajapati on 24/11/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class WaistTestVC: UIViewController, UIPickerViewDataSource, UITextFieldDelegate {
    
    //MARK: IBOutlets
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var txtWaistParams: UITextField!
    @IBOutlet weak var txtHipParams: UITextField!
    @IBOutlet weak var txtGender: UITextField!
    
    @IBOutlet var viewPicker:UIView!
    @IBOutlet var pickerView:UIPickerView!
    
    //MARK: Variables
    var activeField: UITextField?
    var pickerSelIndx:Int!
    var genderArray = NSArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        genderArray = ["Male", "Female"]
        
        self.NavBarNumberPad()
        self.addTapGestureInOurView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSaveRecordClicked(_ sender: Any) {
        if Utility.isEmpty(txtWaistParams.text) {
            showAlert(Appname, title: "Please Enter Waist Circumference Parameters.")
        }
        else if Utility.isEmpty(txtHipParams.text) {
            showAlert(Appname, title: "Please Enter Hip Circumference Parameters.")
        }
        else if Utility.isEmpty(txtGender.text) {
            showAlert(Appname, title: "Please select gender.")
        }
        else{
            let chResult = WaistTestResultVC(nibName: "WaistTestResultVC", bundle: nil)
            chResult.strWaistParam = String(format: "%@", txtWaistParams.text!) as NSString?
            chResult.strHipParam = String(format: "%@", txtHipParams.text!) as NSString?
            chResult.gender = txtGender.text!
            
            self.navigationController?.pushViewController(chResult, animated: true)
        }
    }
    
    @IBAction func btnGenderClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        pickerView.reloadAllComponents()
        
        UIView.animate(withDuration: 0.3, animations: {
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y - self.viewPicker.frame.size.height, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
        })
    }
    
    
    //MARK:- Pickerview
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return (genderArray[row]as! String)
    }
    
    @IBAction func cancelPicker(sender:AnyObject){
        UIView.animate(withDuration: 0.3) {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
        }
        mainScrollView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
    }
    
    @IBAction func donePicker(sender:UIButton){
        mainScrollView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
        pickerSelIndx = pickerView.selectedRow(inComponent: 0)
        print("Picker Index is  :-> \(pickerSelIndx) and value is \(genderArray[pickerSelIndx])")
        txtGender.text = self.genderArray[pickerSelIndx] as? String
        
        UIView.animate(withDuration: 0.3, animations: {
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
        })
    }
    
    
    //MARK:- Method to hide keyboard on back tap.
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.mainScrollView.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        if viewTouched!.isKind(of: UIButton.self){
            
        }
        else{
            self.view.endEditing(true)
            mainScrollView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        }
    }
    
    //MARK:- Numeric Keyboard Hide Method
    func NavBarNumberPad(){
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        numberToolbar.barStyle = .default
        
        let viewBack = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        viewBack.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        numberToolbar.addSubview(viewBack)
        
        let btnCancel = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelNumberPad))
        btnCancel.tintColor = UIColor.white
        
        let btnDone = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.doneWithNumberPad))
        btnDone.tintColor = UIColor.white
        
        numberToolbar.items = NSArray(objects: btnCancel,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),btnDone) as? [UIBarButtonItem]
        
        
        numberToolbar.sizeToFit()
        txtWaistParams.inputAccessoryView = numberToolbar
        txtHipParams.inputAccessoryView = numberToolbar
        
    }
    
    func cancelNumberPad(){
        //txtIop.resignFirstResponder()
        mainScrollView.endEditing(false)
        mainScrollView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
    }
    
    func doneWithNumberPad(){
        //txtIop.resignFirstResponder()
        mainScrollView.endEditing(false)
        mainScrollView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
    }
    
    //MARK:- Textfield Delegate Methods
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let YOffset:Int = 0
        
        UIView.animate(withDuration: 0.3) {
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
        }
        
        mainScrollView.setContentOffset(CGPoint(x: 0, y: CGFloat(textField.tag * YOffset)), animated: true)
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder?
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
            mainScrollView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        }
        return false
    }
}
