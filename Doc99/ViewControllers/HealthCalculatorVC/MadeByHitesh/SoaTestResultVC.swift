//
//  SoaTestResultVC.swift
//  Doc99
//
//  Created by Hitesh Prajapati on 25/11/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD


class SoaTestResultVC: UIViewController {

    //MARK: IBOutlets
    
    @IBOutlet weak var viewUserDetail: UIView!
    @IBOutlet weak var labelTestName: UILabel!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelUserGender: UILabel!
    @IBOutlet weak var labelReportDate: UILabel!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var labelTestMeasure: UILabel!
    @IBOutlet weak var labelRiskDescription: UILabel!
    @IBOutlet weak var mainView: UIView!
    
    //LOGIN POPUP
    @IBOutlet var loginView: UIView!
    @IBOutlet var loginPopupLabel2:UILabel!
    @IBOutlet var loginPopupLabel3:UILabel!
    
    @IBOutlet weak var btnBackLogin: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    
    
    //MARK: Variables
    var strSoaParam :NSString?
    var isFromReport = false
    var fromHistory = false
    var reportDate = ""
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if USERDEFAULT.value(forKey: "userID") != nil{
            /*
            if let isMember = USERDEFAULT.value(forKey: "isMember") as? String{
                let ismem = "\(isMember)"
                if ismem == "1"{
                }
                else{
                    loginPopupLabel3.text = "MEMBERSHIP"
                    self.view.addSubview(loginView)
                    loginView.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
                }
            }
            else{
            }
            */
        }
        else{
            
            loginPopupLabel3.text = "LOGIN"
            self.view.addSubview(loginView)
            loginView.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if USERDEFAULT.value(forKey: "userID") != nil{
            mainView.clipsToBounds = true
            mainView.layer.borderWidth = 0.5
            mainView.layer.borderColor = UIColor.lightGray.cgColor
            loginView.removeFromSuperview()
            
            labelUserName.text = USERDEFAULT.value(forKey: "fullName") as? String ?? ""
            labelUserGender.text = USERDEFAULT.value(forKey: "gender") as? String ?? ""
            
            if Utility.isEmpty(reportDate){
                let dd = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy/MM/dd"
                labelReportDate.text = formatter.string(from: dd)
            }
            else{
                labelReportDate.text = reportDate
            }
            
            self.setReultData()
            
        }
        else{
            
            loginPopupLabel3.text = "LOGIN"
            self.view.addSubview(loginView)
            loginView.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK:-
    
    func setReultData(){
        labelTestName.text = "Sleep Obstructive Apnea Test"
        if !isFromReport{
            if Reachability.isConnectedToNetwork() == true {
                SVProgressHUD.show(withStatus: "Loading..")
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForSoaTest), userInfo: nil, repeats: false)
            } else {
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }
        if let b = strSoaParam as String?{
            labelTestMeasure.text = "\(b)"
            
            let x = Float(b)
            
            if (x! >= 95) && (x! <= 100) {
                
                labelTestMeasure.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                
                labelRiskDescription.text = "Your Result is between the standard.Keep Smiling"
                labelRiskDescription.textColor = UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
            }
            else{
                labelTestMeasure.textColor = .red
                
                labelRiskDescription.text = "Your result is above the standard.Please consult the specialist."
                labelRiskDescription.textColor = .red
            }
        }
    }
    
    //MARK:
    

    //MARK: IBActions
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnShareClicked(_ sender: Any) {
        UIGraphicsBeginImageContextWithOptions(mainView.bounds.size, mainView.isOpaque, 0.0)
        mainView.drawHierarchy(in: mainView.bounds, afterScreenUpdates: false)
        let snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let ssImage : UIImage = snapshotImageFromMyView!
        
        
        let  activityViewController = UIActivityViewController(activityItems: [self,ssImage as UIImage? as Any], applicationActivities: [])
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func btnHomeClicked(_ sender: Any) {
        var viewControllers = navigationController?.viewControllers
        viewControllers?.removeLast(2) // views to pop
        navigationController?.setViewControllers(viewControllers!, animated: true)
    }
    
    @IBAction func btnLoginClicked(_ sender: Any) {
        
        if USERDEFAULT.value(forKey: "userID") as? String != nil{
            
            if let isMember = USERDEFAULT.value(forKey: "isMember") as? String{
                let ismem = "\(isMember)"
                if ismem == "1"{
                    
                }
                else{
                    let joinMemberVC = JoinMemberVC(nibName: "JoinMemberVC", bundle: nil)
                    joinMemberVC.isFromOther = true
                    joinMemberVC.isFromTab = "6"
                    self.navigationController?.pushViewController(joinMemberVC, animated: true)
                }
            }
            else{
                
            }
        }
        else{
            let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
            loginVC.isFromTab = "6"
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
    }
    
    //MARK:
    //MARK: Post Data Methods
    func postDataOnWebserviceForSoaTest(){
        
        let completeURL = NSString(format:"%@%@", MainURL,soaReportUrl) as String
        
        /*
            "user_id":"3",
            "token":"aQD0jXbe0qIcv4xBfdIBARnmOQEE8k6mYLk0s8hGnBJqCqiGo9MZKivYgZYDD9P39Lc05L1Ra4UVbXJ0R3VMFT4WlnOSDQmNMQXEVEwtcBJB43BGm5UQqaHu",
            "lang_type":"3",
         "pulse_oximetry_param":"92",
         "pulse_oximetry_result":"92"
         */
        
        let params:NSDictionary = [
            
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "pulse_oximetry_param":strSoaParam!,
            "pulse_oximetry_result":""
            
        ]
        
        
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("soaReportUrlTag API Parameter :",finalParams)
        print("soaReportUrlTag API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: soaReportUrlTag)
    }

    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
            
        case soaReportUrlTag:
            
            let resultDict = responseObject as! NSDictionary;
            print("soaReportUrlTag Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
                
            else if resultDict.value(forKey: "status") as! String == "2"{
                //                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        default:
            break
            
        }
        SVProgressHUD.dismiss()
        
    }
    
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case soaReportUrlTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
}
