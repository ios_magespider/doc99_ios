//
//  ChildGrowthTestResultVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 12/06/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class ChildGrowthTestResultVC: UIViewController {

    
    //MARK:- IBOutlets
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var labelGender: UILabel!
    @IBOutlet weak var labelChildHeightFeet: UILabel!
    @IBOutlet weak var labelChildHeightInch: UILabel!
    @IBOutlet weak var labelFatherHeightFeet: UILabel!
    @IBOutlet weak var labelFatherHeightInch: UILabel!
    @IBOutlet weak var labelMotherHeightFeet: UILabel!
    @IBOutlet weak var labelMotherHeightInch: UILabel!
    @IBOutlet weak var labelReportDate: UILabel!
    
    @IBOutlet weak var labelMeasure: UILabel!
    
    //LOGIN POPUP
    @IBOutlet var loginView: UIView!
    @IBOutlet var loginPopupLabel2:UILabel!
    @IBOutlet var loginPopupLabel3:UILabel!
    
    @IBOutlet weak var btnBackLogin: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    
    
    //MARK:- Variables
    var isFromReport = false
    var isFirstCall = true
    var gender = ""
    var age : Double = 0.0
    var childFeet = ""
    var childInches = ""
    var fatherFeet = ""
    var fatherInches = ""
    var motherFeet = ""
    var motherInches = ""
    
    
    var childHeightFeet : Double = 0.0
    var motherHeightFeet : Double = 0.0
    var fatherHeightFeet : Double = 0.0
    
    var finalReport = ""
    var reportDate = ""
    override func viewDidLoad() {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if USERDEFAULT.value(forKey: "userID") != nil{
            mainView.clipsToBounds = true
            mainView.layer.borderWidth = 0.5
            mainView.layer.borderColor = UIColor.lightGray.cgColor
            loginView.removeFromSuperview()
            self.setData()
        }
        else{
            loginPopupLabel3.text = "LOGIN"
            self.view.addSubview(loginView)
            loginView.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK:-
    func setData(){
        labelGender.text = gender
        labelChildHeightFeet.text = childFeet
        labelChildHeightInch.text = childInches
        labelFatherHeightFeet.text = fatherFeet
        labelFatherHeightInch.text = fatherInches
        labelMotherHeightFeet.text = motherFeet
        labelMotherHeightInch.text = motherInches
        
        if Utility.isEmpty(reportDate){
            let dd = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM/dd"
            labelReportDate.text = formatter.string(from: dd)
        }
        else{
            labelReportDate.text = reportDate
        }
        
        
        let convertedChildHeightToInch = feetToInches(feet: childHeightFeet)
        let convertedMotherHeightToInch = feetToInches(feet: motherHeightFeet)
        let convertedFatherHeightToInch = feetToInches(feet: fatherHeightFeet)
        
        print("Child = \(convertedChildHeightToInch)\nMother = \(convertedMotherHeightToInch)\nFather = \(convertedFatherHeightToInch)")
        
        var childHeightInch = 0.0
        
        if gender == "Female"{
            childHeightInch = (((convertedFatherHeightToInch * 12) / 13) + convertedMotherHeightToInch) / 2
        }
        else{
            childHeightInch = (convertedFatherHeightToInch + (convertedMotherHeightToInch * 13) / 12) / 2
        }
        print("Fianl Child inch height = \(childHeightInch)")
        
        finalReport = String(format: "%.2f", (childHeightInch * 2.54))
        labelMeasure.text = finalReport
        
        
        if !isFromReport && isFirstCall{
            if Reachability.isConnectedToNetwork() == true {
                SVProgressHUD.show(withStatus: "Loading..")
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForChildGrowthTest), userInfo: nil, repeats: false)
            } else {
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }
    }
    
    func feetToInches(feet: Double) -> Double {
        return feet * 12
    }
    
    
    // MARK:- IBActions
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnShareClicked(_ sender: Any) {
        UIGraphicsBeginImageContextWithOptions(mainView.bounds.size, mainView.isOpaque, 0.0)
        mainView.drawHierarchy(in: mainView.bounds, afterScreenUpdates: false)
        let snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let ssImage : UIImage = snapshotImageFromMyView!
        
        
        let  activityViewController = UIActivityViewController(activityItems: [self,ssImage as UIImage? as Any], applicationActivities: [])
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func btnHomeClicked(_ sender: Any) {
        var viewControllers = navigationController?.viewControllers
        viewControllers?.removeLast(2) // views to pop
        navigationController?.setViewControllers(viewControllers!, animated: true)
    }
    @IBAction func btnLoginClicked(_ sender: Any) {
        
        if USERDEFAULT.value(forKey: "userID") as? String != nil{
            
            if let isMember = USERDEFAULT.value(forKey: "isMember") as? String{
                let ismem = "\(isMember)"
                if ismem == "1"{
                    
                }
                else{
                    let joinMemberVC = JoinMemberVC(nibName: "JoinMemberVC", bundle: nil)
                    joinMemberVC.isFromOther = true
                    joinMemberVC.isFromTab = "6"
                    self.navigationController?.pushViewController(joinMemberVC, animated: true)
                }
            }
            else{
                
            }
        }
        else{
            let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
            loginVC.isFromTab = "6"
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
    }
    
    //MARK:- Post Data Methods
    func postDataOnWebserviceForChildGrowthTest(){
        let completeURL = NSString(format:"%@%@", MainURL,childGrowthTestUrl) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "child_sex": gender,
            "age_month": age,
            "child_height_feet" : childFeet,
            "child_height_inches": childInches,
            "mother_height_feet" : motherFeet,
            "mother_height_inches" : motherInches,
            "father_height_feet" : fatherFeet,
            "father_height_inches" : fatherInches,
            "report" : finalReport
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("Child growth API URL :",completeURL)
        print("Child growth API Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: childGrowthTestUrlTag)
        
    }
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
            
        case childGrowthTestUrlTag:
            
            let resultDict = responseObject as! NSDictionary;
            print("childGrowthTestUrlTag Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                isFirstCall = false
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            break
            
        default:
            break
            
        }
        SVProgressHUD.dismiss()
        
    }
    
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case childGrowthTestUrlTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
}
