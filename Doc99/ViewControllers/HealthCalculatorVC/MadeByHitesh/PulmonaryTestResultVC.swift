//
//  PulmonaryTestResultVC.swift
//  Doc99
//
//  Created by Hitesh Prajapati on 25/11/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class PulmonaryTestResultVC: UIViewController, UIWebViewDelegate {

    
    //MARK: IBOutlets
    
    
    @IBOutlet weak var viewUserDetail: UIView!
    @IBOutlet weak var labelTestName: UILabel!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelUserGender: UILabel!
    @IBOutlet weak var labelReportDate: UILabel!
    
    
    
    @IBOutlet weak var labelRiskDescription: UILabel!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var labelFVCMeasure: UILabel!
    @IBOutlet weak var labelFVCDetail: UILabel!
    
    @IBOutlet weak var labelFEVMeasure: UILabel!
    @IBOutlet weak var labelFEVDetail: UILabel!
    
    @IBOutlet weak var labelFvcRatioMeasure: UILabel!
    @IBOutlet weak var labelFvcRatioDetail: UILabel!

    @IBOutlet weak var labelPEFRMeasure: UILabel!
    @IBOutlet weak var labelPEFRDetail: UILabel!
    
    
    @IBOutlet weak var webViewDetail: UIWebView!
    @IBOutlet weak var webViewHeight: NSLayoutConstraint!
    
    //LOGIN POPUP
    @IBOutlet var loginView: UIView!
    @IBOutlet var loginPopupLabel2:UILabel!
    @IBOutlet var loginPopupLabel3:UILabel!
    
    @IBOutlet weak var btnBackLogin: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    
    
    //MARK: Variables
    
    var userFVC :NSString?
    var userFEV :NSString?
    var userPEFR :NSString?
    var AshanaVal = ""
    var strHeight :NSString?
    var strFVC :Double?
    var strFEV :Double?
    var strPEFR :Double?
    
    var fromHistory : Bool = false
    var reportDate = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if USERDEFAULT.value(forKey: "userID") != nil{
            /*
            if let isMember = USERDEFAULT.value(forKey: "isMember") as? String{
                let ismem = "\(isMember)"
                if ismem == "1"{
                }
                else{
                    loginPopupLabel3.text = "MEMBERSHIP"
                    self.view.addSubview(loginView)
                    loginView.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
                }
            }
            else{
            }
            */
        }
        else{
            
            loginPopupLabel3.text = "LOGIN"
            self.view.addSubview(loginView)
            loginView.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if USERDEFAULT.value(forKey: "userID") != nil{
            mainView.clipsToBounds = true
            mainView.layer.borderWidth = 0.5
            mainView.layer.borderColor = UIColor.lightGray.cgColor
            loginView.removeFromSuperview()
            self.setReultData()
            
            labelUserName.text = USERDEFAULT.value(forKey: "fullName") as? String ?? ""
            labelUserGender.text = USERDEFAULT.value(forKey: "gender") as? String ?? ""
            
            if Utility.isEmpty(reportDate){
                let dd = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy/MM/dd"
                labelReportDate.text = formatter.string(from: dd)
            }
            else{
                labelReportDate.text = reportDate
            }
        }
        else{
            
            loginPopupLabel3.text = "LOGIN"
            loginView.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
            self.view.addSubview(loginView)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK:-
    func setReultData(){
        labelTestName.text = "Pulmonary Test"
        
        webViewDetail.loadHTMLString("<html><head><link href=\"https://fonts.googleapis.com/css?family=Lato\" rel=\"stylesheet\"></head><body style=\"background-color:#ECECEC;\"><font face = \"Lato\" size = \"2.5\"><p style=\"color:rgb(146,146,146);\">Peak expiratory flow rate (PEFR) test can help you discover when you need to adjust your medication. Or it can help determine whether environmental factors or pollutants are affecting your breathing.<br/><br/>Normal (Green)<br/>Mild airflow limitation(FEV1/FVC < 70%; FEV1 > 80% predicted) and PEFR 80-100%<br/><br/>COPD (Red)<br/>Worsening airflow limitation(FEV1/FVC < 70%; 50% < FEV1 < 80% predicted), with shortness of breath; PEFR 50-79%<br/><br/>Severe COPD (Red)<br/>Further worsening of airflow limitation(FEV1/FVC < 70%; 30% < FEV1 < 50% predicted), greater shortness of breath, reduced excercise capacity, an impact on patient’s quality of life; PEFR < 50%</p></font></body></html>", baseURL: nil)
        
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForPulmonaryTest), userInfo: nil, repeats: false)
            
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        

        labelFVCMeasure.text = userFVC as String?
        labelFEVMeasure.text = userFEV as String?
        labelPEFRMeasure.text = userPEFR as String?
        
        
        let x : Double = ((userFEV!.doubleValue)/(userFVC!.doubleValue))
        labelFvcRatioMeasure.text = NSString(format: "%.2f%%", x)  as String?
        
        
        let countedFVC : Double = ((userFVC!.doubleValue)/strFVC!)*100
        if countedFVC <= 80 {
            
            labelFVCDetail.text = NSString(format: "%.f%% (Low)", countedFVC)  as String?
            labelFVCDetail.textColor = .red
            labelFVCMeasure.textColor = .red
        }
        else{
            labelFVCDetail.text = NSString(format: "Medium")  as String?
            labelFVCDetail.textColor = UIColor.init(red: 148.0/255.0, green: 190.0/255.0, blue: 71.0/255.0, alpha: 1)
            labelFVCMeasure.textColor = UIColor.init(red: 148.0/255.0, green: 190.0/255.0, blue: 71.0/255.0, alpha: 1)
        }
        
        let countedFEV : Double = ((userFEV!.doubleValue)/strFEV!)*100
        if countedFEV <= 80 {
            
            labelFEVDetail.text = NSString(format: "%.f%% (Low)", countedFEV)  as String?
            labelFEVDetail.textColor = .red
            labelFEVMeasure.textColor = .red
        }
        else{
            labelFEVDetail.text = NSString(format: "Medium")  as String?
            labelFEVDetail.textColor = UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
            labelFEVMeasure.textColor = UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
        }
        
        
        if (countedFVC <= 80) || (countedFEV <= 80) {
            labelRiskDescription.textColor = .red
            labelFvcRatioMeasure.textColor = .red
            labelRiskDescription.text = "Your result is above the standard.Please consult the specialist."
        }
        else{
            labelRiskDescription.textColor = UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
            labelFvcRatioMeasure.textColor = UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
            labelRiskDescription.text = "Your Result is between the standard.Keep Smiling"
        }
    }
    
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        webViewDetail.scrollView.isScrollEnabled = false
        webViewHeight.constant = webViewDetail.scrollView.contentSize.height
    }
    
    
    //MARK: IBActions
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnShareClicked(_ sender: Any) {
        
        
        UIGraphicsBeginImageContextWithOptions(mainView.bounds.size, mainView.isOpaque, 0.0)
        mainView.drawHierarchy(in: mainView.bounds, afterScreenUpdates: false)
        let snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let ssImage : UIImage = snapshotImageFromMyView!
        
        
        let  activityViewController = UIActivityViewController(activityItems: [self,ssImage], applicationActivities: [])
        
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func btnHomeClicked(_ sender: Any) {
        var viewControllers = navigationController?.viewControllers
        viewControllers?.removeLast(2) // views to pop
        navigationController?.setViewControllers(viewControllers!, animated: true)
    }
    
    @IBAction func btnLoginClicked(_ sender: UIButton) {
        
        if USERDEFAULT.value(forKey: "userID") as? String != nil{
            
            if let isMember = USERDEFAULT.value(forKey: "isMember") as? String{
                let ismem = "\(isMember)"
                if ismem == "1"{
                    
                }
                else{
                    let joinMemberVC = JoinMemberVC(nibName: "JoinMemberVC", bundle: nil)
                    joinMemberVC.isFromOther = true
                    joinMemberVC.isFromTab = "6"
                    self.navigationController?.pushViewController(joinMemberVC, animated: true)
                }
            }
            else{
                
            }
        }
        else{
            let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
            loginVC.isFromTab = "6"
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
    }
    
    
    
    //MARK: Post data methods
    func postDataOnWebserviceForPulmonaryTest(){
        let completeURL = NSString(format:"%@%@", MainURL,pulmonaryReportUrl) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "height":strHeight!,
            "fvc":userFVC!,
            "fev1":userFEV!,
            "pefr" : userPEFR!,
            "asthama_history":AshanaVal,
            "fvc_report":"",
            "fev1_report":"",
            "fvc_fev1_report":"",
            "pefr_report":""
        ]
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        print("pulmonaryReportUrlTag API Parameter :",finalParams)
        print("pulmonaryReportUrlTag API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: pulmonaryReportUrlTag)
    }
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
            
        case pulmonaryReportUrlTag:
            
            let resultDict = responseObject as! NSDictionary;
            print("pulmonaryReportUrlTag Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
                
            else if resultDict.value(forKey: "status") as! String == "2"{
                //                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        default:
            break
            
        }
        SVProgressHUD.dismiss()
        
    }
    
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case pulmonaryReportUrlTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
}
