//
//  ChildGrowthTestVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 12/06/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit

class ChildGrowthTestVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    

    //MARK:- IBOutlets
    @IBOutlet weak var scrView: UIScrollView!
    
    @IBOutlet weak var txtGender: UITextField!
    @IBOutlet weak var txtAge: UITextField!
   
    @IBOutlet weak var txtChildHeightFeet: UITextField!
    @IBOutlet weak var txtChildHeightInches: UITextField!
    
    @IBOutlet weak var txtMotherHightFeet: UITextField!
    @IBOutlet weak var txtMotherHightInches: UITextField!
    
    @IBOutlet weak var txtFatherHightFeet: UITextField!
    @IBOutlet weak var txtFatherHightInches: UITextField!
    
    @IBOutlet var viewPicker:UIView!
    @IBOutlet var pickerView:UIPickerView!
    
    
    //MARK:- Variables
    var pickerSelIndx:Int!
    var genderArray = NSArray()
    
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        genderArray = ["Male", "Female"]
        self.NavBarNumberPad()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK:-
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnGenderClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        pickerView.reloadAllComponents()
        
        UIView.animate(withDuration: 0.3, animations: {
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y - self.viewPicker.frame.size.height, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
        })
    }
    
    @IBAction func btnSaveClicked(_ sender : UIButton){
        if Utility.isEmpty(txtGender.text){
            showAlert(Appname, title: "Please select gender.")
        }
        else if Utility.isEmpty(txtAge.text){
            showAlert(Appname, title: "Please enter age.")
        }
        else if let x : String = txtAge.text! , Int(x)! > 48{
            showAlert(Appname, title: "Children age can't be geater than 4 years.")
        }
        else if Utility.isEmpty(txtChildHeightFeet.text){
            showAlert(Appname, title: "Please enter child height feet.")
        }
        else if Utility.isEmpty(txtChildHeightInches.text){
            showAlert(Appname, title: "Please enter child height inches.")
        }
        else if Utility.isEmpty(txtMotherHightFeet.text){
            showAlert(Appname, title: "Please enter mother height feet.")
        }
        else if Utility.isEmpty(txtMotherHightInches.text){
            showAlert(Appname, title: "Please enter mother height inches.")
        }
        else if Utility.isEmpty(txtFatherHightFeet.text){
            showAlert(Appname, title: "Please enter father height feet.")
        }
        else if Utility.isEmpty(txtFatherHightInches.text){
            showAlert(Appname, title: "Please enter father height inches.")
        }
        else{
            //Child Feet
            var childInch = "00"
            if !Utility.isEmpty(txtChildHeightInches.text){
                childInch = txtChildHeightInches.text!
            }
            let childFeet = txtChildHeightFeet.text!
            let finalChildHeightFeet = childFeet+"."+childInch
            print("Child = \(finalChildHeightFeet)")
            
            //Mother Feet
            var motherInch = "00"
            if !Utility.isEmpty(txtMotherHightInches.text){
                motherInch = txtMotherHightInches.text!
            }
            let motherFeet = txtMotherHightFeet.text!
            let finalMotherHeightFeet = motherFeet+"."+motherInch
            print("mother = \(finalMotherHeightFeet)")
            
            //Father Feet
            var fatherInch = "00"
            if !Utility.isEmpty(txtFatherHightInches.text){
                fatherInch = txtFatherHightInches.text!
            }
            let fatherFeet = txtFatherHightFeet.text!
            let finalFatherHeightFeet = fatherFeet+"."+fatherInch
            print("Father = \(finalFatherHeightFeet)")
            
            let age:String = txtAge.text!
            
            let result = ChildGrowthTestResultVC(nibName: "ChildGrowthTestResultVC", bundle: nil)
            result.gender = txtGender.text!
            result.age = Double(age)!
            
            result.childFeet = txtChildHeightFeet.text!
            result.childInches = txtChildHeightInches.text ?? "0"
            result.motherFeet = txtMotherHightFeet.text!
            result.motherInches = txtMotherHightInches.text ?? "0"
            result.fatherFeet = txtFatherHightFeet.text!
            result.fatherInches = txtFatherHightInches.text ?? "0"
            
            result.childHeightFeet = Double(finalChildHeightFeet)!
            result.motherHeightFeet = Double(finalMotherHeightFeet)!
            result.fatherHeightFeet = Double(finalFatherHeightFeet)!
            self.navigationController?.pushViewController(result, animated: true)
        }
    }
    //MARK:- Pickerview
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return (genderArray[row]as! String)
    }
    
    @IBAction func cancelPicker(sender:AnyObject){
        UIView.animate(withDuration: 0.3) {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
        }
        scrView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
    }
    
    @IBAction func donePicker(sender:UIButton){
        scrView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
        pickerSelIndx = pickerView.selectedRow(inComponent: 0)
        print("Picker Index is  :-> \(pickerSelIndx) and value is \(genderArray[pickerSelIndx])")
        txtGender.text = self.genderArray[pickerSelIndx] as? String
        
        UIView.animate(withDuration: 0.3, animations: {
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
        })
    }
    
    //MARK:-
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtFatherHightFeet || textField == txtFatherHightInches {
            scrView.setContentOffset(CGPoint(x: 0, y: 30), animated: true)
        }
        
        UIView.animate(withDuration: 0.3) {
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
        }
        return true
    }
    
    //MARK:- Numeric Keyboard Hide Method
    func NavBarNumberPad(){
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        numberToolbar.barStyle = .default
        
        let viewBack = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        viewBack.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        numberToolbar.addSubview(viewBack)
        
        let btnCancel = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelNumberPad))
        btnCancel.tintColor = UIColor.white
        
        let btnDone = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.doneWithNumberPad))
        btnDone.tintColor = UIColor.white
        
        numberToolbar.items = NSArray(objects: btnCancel,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),btnDone) as? [UIBarButtonItem]
        numberToolbar.sizeToFit()
        
        txtAge.inputAccessoryView = numberToolbar
        txtChildHeightFeet.inputAccessoryView = numberToolbar
        txtChildHeightInches.inputAccessoryView = numberToolbar
        txtFatherHightFeet.inputAccessoryView = numberToolbar
        txtFatherHightInches.inputAccessoryView = numberToolbar
        txtMotherHightFeet.inputAccessoryView = numberToolbar
        txtMotherHightInches.inputAccessoryView = numberToolbar
        
    }
    
    func cancelNumberPad(){
        self.view.endEditing(true)
        scrView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
    }
    
    func doneWithNumberPad(){
        self.view.endEditing(true)
        scrView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
    }
    
}
