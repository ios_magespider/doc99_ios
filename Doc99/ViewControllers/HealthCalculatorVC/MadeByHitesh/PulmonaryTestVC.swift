//
//  PulmonaryTestVC.swift
//  Doc99
//
//  Created by Hitesh Prajapati on 25/11/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class PulmonaryTestVC: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
  
    

    //MARK: IBOutlets
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var DetailLabel: UILabel!
    @IBOutlet weak var ViewForTextFields: UIView!
    
    @IBOutlet weak var txtHeight: UITextField!
    @IBOutlet weak var txtFVC: UITextField!
    @IBOutlet weak var txtFEV: UITextField!
    @IBOutlet weak var txtPEFR: UITextField!
    
    @IBOutlet weak var txtAsthamaHistory: UITextField!
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var pickerOuterview: UIView!
    
    @IBOutlet weak var pickerview: UIPickerView!
    @IBOutlet weak var NSLCPickerBottom: NSLayoutConstraint!
    
    var arrAshtamaHistory = ["YES","NO"]
    //MARK: Variables
    var exFvc: Double = 0
    var exFev1: Double = 0
    var exPef: Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerview.delegate = self
        pickerview.dataSource = self
        NSLCPickerBottom.constant = -200
        self.NavBarNumberPad()
        self.addTapGestureInOurView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK: - PICKERVIEW DELEGATE
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrAshtamaHistory.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        // return (self.pickOption[row] as AnyObject).value(forKey: "title") as? String
        return self.arrAshtamaHistory[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //self.txtAsthamaHistory.text = self.arrAshtamaHistory[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50.0
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let label = UILabel()
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont.systemFont(ofSize: 20.0)
        label.text = self.arrAshtamaHistory[row]
        return label
    }
    
    //MARK:- button action
    
    @IBAction func btnCancelTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            self.NSLCPickerBottom.constant = -200
        }) { (completion) in
            self.txtAsthamaHistory.text = self.arrAshtamaHistory[0]
        }
    }
    @IBAction func btnDoneTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            self.NSLCPickerBottom.constant = -200
        }) { (completion) in
            self.txtAsthamaHistory.text = self.arrAshtamaHistory[self.pickerview.selectedRow(inComponent: 0)]//self.arrAshtamaHistory[row]
        }
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSaveRecordClicked(_ sender: UIButton) {
        if  txtHeight.text == "" {
            showAlert(Appname, title: "Please Enter Height Parameter.")
        }
        else if txtFVC.text == "" {
            showAlert(Appname, title: "Please Enter FVC Parameter.")
        }
        else if txtFEV.text == "" {
            showAlert(Appname, title: "Please Enter FEV Parameter.")
        }
        else if txtPEFR.text == "" {
            showAlert(Appname, title: "Please Enter PEFR Parameter.")
        }
        else{
            let chResult = PulmonaryTestResultVC(nibName: "PulmonaryTestResultVC", bundle: nil)
            
            chResult.userFEV = String(format: "%@", txtFEV.text!) as NSString?
            chResult.userFVC = String(format: "%@", txtFVC.text!) as NSString?
            chResult.userPEFR = String(format: "%@", txtPEFR.text!) as NSString?
            chResult.strHeight = String(format: "%@", txtHeight.text!) as NSString?
            chResult.AshanaVal = (txtAsthamaHistory.text == "YES") ? "1" : "0"
            chResult.strFEV = exFev1
            chResult.strFVC = exFvc
            chResult.strPEFR = exPef
            
            self.navigationController?.pushViewController(chResult, animated: true)
        }
        
    }
    @IBAction func btnAshtanaHistoryTapped(_ sender: UIButton) {
        pickerview.reloadAllComponents()
        UIView.animate(withDuration: 0.3, animations: {
            self.NSLCPickerBottom.constant = 0
        }) { (completion) in
            //self.txtAsthamaHistory.text = self.arrAshtamaHistory[0]
        }
    }
    //MARK:
    @IBAction func heightValueChanged(_ sender: UITextField) {
        
        if txtHeight.text != ""{
            
            if APPDELEGATE.healthIndexData.value(forKey: "genderIs")  != nil
            {
                let age:Double = Double(self.calculateAge(dob: APPDELEGATE.healthIndexData.value(forKey: "birthDateIs") as! String))
                let strGender = APPDELEGATE.healthIndexData.value(forKey: "genderIs") as! String
                
                let ht = Double(txtHeight.text!)
                
                
                if  strGender == "Male"
                {
                    if (age < 25)
                    {
                        exFvc = -5.508+0.05*ht!+0.078*age;
                        exFev1 = -4.808+0.046*ht!+0.045*age;
                    }
                    else   // Knudson-males-age>= 25
                    {
                        exFvc = -5.459+0.065*ht!-0.029*age;
                        exFev1 = -4.203+0.052*ht!-0.027*age;
                    }
                    
                    if (age < 15)
                    {
                        exPef=455 * (ht!/100) - 332
                        
                    }
                    else   //males age>= 15
                    {
                        exPef = exp((0.544 * log(age))-(0.0151*age)-(74.7 / ht!)+5.48);  // For Male
                    }
                    
                    
//                    txtFVC.text = "\(exFvc)"
//                    txtFEV.text = "\(exFev1)"
//                    txtPEFR.text = NSString(format: "%.2f",exPef)  as String!
                    
                    
                    print("PEF --> \(exPef)")
                }
                else  if strGender == "Female"
                {
                    if (age < 20)
                    {
                        exFvc = -3.469+ht!*0.033+age*0.092;
                        exFev1 = -2.703+ht!*0.027+age*0.085;
                    }
                    else
                    {
                        exFvc = -1.774+ht!*0.037+age*(-0.022);
                        exFev1 = -0.794+ht!*0.027+age*(-0.021);
                    }
                    
                    
                    if (age < 15)
                    {
                        exPef=455 * (ht!/100) - 332
                        
                    }
                    else   //Females age >= 15
                    {
                        exPef = exp((0.5376 * log(age))-(0.0120*age)-(58.8 / ht!)+5.63)
                    }
                    
//                    txtFVC.text = "\(exFvc)"
//                    txtFEV.text = "\(exFev1)"
//                    txtPEFR.text = "\(exPef)"
                }

                
            }
            else if USERDEFAULT.value(forKey: "gender") != nil{
                //print("Counting from USERDEFAULT gender")
                
                let age:Double = Double(self.calculateAge(dob: USERDEFAULT.value(forKey: "dob") as! String))
                let strGender = USERDEFAULT.value(forKey: "gender") as! String
                
                let ht = Double(txtHeight.text!)
                
                
                if  strGender == "Male"
                {
                    if (age < 25)
                    {
                        exFvc = -5.508+0.05*ht!+0.078*age;
                        exFev1 = -4.808+0.046*ht!+0.045*age;
                    }
                    else   // Knudson-males-age>= 25
                    {
                        exFvc = -5.459+0.065*ht!-0.029*age;
                        exFev1 = -4.203+0.052*ht!-0.027*age;
                    }
                    
                    if (age < 15)
                    {
                        exPef=455 * (ht!/100) - 332
                        
                    }
                    else   //males age>= 15
                    {
                        exPef = exp((0.544 * log(age))-(0.0151*age)-(74.7 / ht!)+5.48);  // For Male
                    }
                    
                    
//                    txtFVC.text = "\(exFvc)"
//                    txtFEV.text = "\(exFev1)"
//                    txtPEFR.text = NSString(format: "%.2f",exPef)  as String!
                    
                    
                    print("PEF --> \(exPef)")
                }
                else  if strGender == "Female"
                {
                    if (age < 20)
                    {
                        exFvc = -3.469+ht!*0.033+age*0.092;
                        exFev1 = -2.703+ht!*0.027+age*0.085;
                    }
                    else
                    {
                        exFvc = -1.774+ht!*0.037+age*(-0.022);
                        exFev1 = -0.794+ht!*0.027+age*(-0.021);
                    }
                    
                    
                    if (age < 15)
                    {
                        exPef=455 * (ht!/100) - 332
                        
                    }
                    else   //Females age >= 15
                    {
                        exPef = exp((0.5376 * log(age))-(0.0120*age)-(58.8 / ht!)+5.63)
                    }
                    
//                    txtFVC.text = "\(exFvc)"
//                    txtFEV.text = "\(exFev1)"
//                    txtPEFR.text = "\(exPef)"
                }
            }
        }
        else{
            txtFVC.text = ""
            txtFEV.text = ""
            txtPEFR.text = ""
        }
    }
 
    func calculateAge(dob : String) -> Int{
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let date = df.date(from: dob)
        NSLog("%@", dob)
        guard let val = date else{
            return (0)
        }
        var years = 0
        var months = 0
        var days = 0
        
        let cal = NSCalendar.current
        years = cal.component(.year, from: NSDate() as Date) -  cal.component(.year, from: val)
        
        let currMonth = cal.component(.month, from: NSDate() as Date)
        let birthMonth = cal.component(.month, from: val)
        
        //get difference between current month and birthMonth
        months = currMonth - birthMonth
        //if month difference is in negative then reduce years by one and calculate the number of months.
        if months < 0
        {
            years = years - 1
            months = 12 - birthMonth + currMonth
            if cal.component(.day, from: NSDate() as Date) < cal.component(.day, from: val){
                months = months - 1
            }
        } else if months == 0 && cal.component(.day, from: NSDate() as Date) < cal.component(.day, from: val)
        {
            years = years - 1
            months = 11
        }
        
        //Calculate the days
        if cal.component(.day, from: NSDate() as Date) > cal.component(.day, from: val){
            days = cal.component(.day, from: NSDate() as Date) - cal.component(.day, from: val)
        }
        else if cal.component(.day, from: NSDate() as Date) < cal.component(.day, from: val)
        {
            let today = cal.component(.day, from: NSDate() as Date)
            let date = cal.date(byAdding:.month, value:  -1, to: NSDate() as Date)
            
            //let date = cal.dateByAddingUnit(.Month, value: -1, toDate: NSDate(), options: [])
            
            days = (cal.component(.day, from: date!) - cal.component(.day, from: val)) + today
        } else
        {
            days = 0
            if months == 12
            {
                years = years + 1
                months = 0
            }
        }
        
        return years
    }
    
    //MARK:
    
    
    func addTapGestureInOurView(){
        
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.mainScrollView.addGestureRecognizer(tapRecognizer)
        
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        self.view.endEditing(true)
        mainScrollView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
    }
    //MARK: Numeric Keyboard Hide Method
    func NavBarNumberPad(){
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        numberToolbar.barStyle = .default
        
        let viewBack = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        viewBack.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        numberToolbar.addSubview(viewBack)
        
        let btnCancel = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelNumberPad))
        btnCancel.tintColor = UIColor.white
        
        let btnDone = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.doneWithNumberPad))
        btnDone.tintColor = UIColor.white
        
        numberToolbar.items = NSArray(objects: btnCancel,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),btnDone) as? [UIBarButtonItem]
        
        
        numberToolbar.sizeToFit()
        txtHeight.inputAccessoryView = numberToolbar
        txtFVC.inputAccessoryView = numberToolbar
        txtFEV.inputAccessoryView = numberToolbar
        txtPEFR.inputAccessoryView = numberToolbar
        
    }
    
    func cancelNumberPad(){
        //txtIop.resignFirstResponder()
        mainScrollView.endEditing(false)
        mainScrollView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
    }
    
    func doneWithNumberPad(){
        //txtIop.resignFirstResponder()
        mainScrollView.endEditing(false)
        mainScrollView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
    }
    //MARK: Textfield Delegate Methods
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var YOffset:Int = 0
        
//        if (textField.tag==2) {
//            YOffset=30
//        }
//        else if (textField.tag==3) {
//            YOffset=44
//        }
//        else if (textField.tag==4) {
//            YOffset=51
//        }
        
        mainScrollView.setContentOffset(CGPoint(x: 0, y: CGFloat(textField.tag * YOffset)), animated: true)
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
            mainScrollView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
            
        }
        return false
    }

    
}
