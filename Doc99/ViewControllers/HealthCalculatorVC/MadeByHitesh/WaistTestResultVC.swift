//
//  WaistTestResultVC.swift
//  Doc99
//
//  Created by Hitesh Prajapati on 25/11/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import  SVProgressHUD


class WaistTestResultVC: UIViewController, UIWebViewDelegate {

    
    
    //MARK: IBOutlets
    
    @IBOutlet weak var viewUserDetail: UIView!
    @IBOutlet weak var labelTestName: UILabel!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelUserGender: UILabel!
    @IBOutlet weak var labelReportDate: UILabel!
    
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var labelTestMeasure: UILabel!
    @IBOutlet weak var labelRiskDescription: UILabel!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var webViewDetail: UIWebView!
    @IBOutlet weak var webViewHeight: NSLayoutConstraint!
    
    //LOGIN POPUP
    @IBOutlet var loginView: UIView!
    @IBOutlet var loginPopupLabel2:UILabel!
    @IBOutlet var loginPopupLabel3:UILabel!
    
    @IBOutlet weak var btnBackLogin: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    
    var fromHistory : Bool = false
    
    //MARK: Variables
    var strWaistParam :NSString?
    var strHipParam :NSString?
    var gender = ""
    var isFromReport = false
    var reportDate = ""
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()

        
        if USERDEFAULT.value(forKey: "userID") != nil{
            /*
            if let isMember = USERDEFAULT.value(forKey: "isMember") as? String{
                let ismem = "\(isMember)"
                if ismem == "1"{
                }
                else{
                    loginPopupLabel3.text = "MEMBERSHIP"
                    self.view.addSubview(loginView)
                    loginView.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
                }
            }
            else{
                
            }
            */
        }
        else{
            
            loginPopupLabel3.text = "LOGIN"
            self.view.addSubview(loginView)
            loginView.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        }
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if USERDEFAULT.value(forKey: "userID") != nil{
            mainView.clipsToBounds = true
            mainView.layer.borderWidth = 0.5
            mainView.layer.borderColor = UIColor.lightGray.cgColor
            loginView.removeFromSuperview()
            self.setReultData()
            
            labelUserName.text = USERDEFAULT.value(forKey: "fullName") as? String ?? ""
            labelUserGender.text = USERDEFAULT.value(forKey: "gender") as? String ?? ""
            
            if Utility.isEmpty(reportDate){
                let dd = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy/MM/dd"
                labelReportDate.text = formatter.string(from: dd)
            }
            else{
                labelReportDate.text = reportDate
            }
        }
        else{
            
            loginPopupLabel3.text = "LOGIN"
            self.view.addSubview(loginView)
            loginView.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:-
    func setReultData(){
        
        labelTestName.text = "Waist Hip Ratio Test"
        
        webViewDetail.loadHTMLString("<html><head><link href=\"https://fonts.googleapis.com/css?family=Lato\" rel=\"stylesheet\"></head><body style=\"background-color:#ECECEC;\"><font face = \"Lato\" size = \"2.5\"><p style=\"color:rgb(146,146,146);\">Waist To Hip Ratio</br>Indicator to risk of Myocardial Infarction</br><b style=\"color:rgb(0,0,0);\">Men: 0.9 - 1.0 Normal (Green) Outside is Red</br>Women: 0.7 - 0.75 Normal (Green) Outside is Red</b></br></br>Waist To Hip Ratio was more strongly associated with the risk of MI than BMI in both sexes, especially in women.</br></br>A higher waist-to-hip ratio and waist circumference conferred a greater excess risk of MI in women than men.</br></br>Reference: MI Risk Higher in Apple Shaped Women than Men-Medscape - Mar 02, 2018.</p></font></body></html>", baseURL: nil)
        
        if !isFromReport{
            if Reachability.isConnectedToNetwork() == true {
                SVProgressHUD.show(withStatus: "Loading..")
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForWaistHipTest), userInfo: nil, repeats: false)
            } else {
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }
        
        
        let waist : Double = self.strWaistParam!.doubleValue
        let hip : Double = self.strHipParam!.doubleValue
        
        let result : Double = waist/hip
        
        var res : String?
        
        //let strGender = APPDELEGATE.healthIndexData.value(forKey: "genderIs") as! String
        if  gender == "Male"
        {
            print("gen =  male")
            if result <= 0.95 {
                res = "Low"
                labelTestMeasure.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                labelRiskDescription.text="Your Result is between the standard.Keep Smiling"
                labelRiskDescription.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
            }
            else if (result >= 0.96 && result <= 1.0){
                res = "Medium"
                labelRiskDescription.text = "Your result is above the standard.Please consult the specialist."
                labelRiskDescription.textColor = .red
                
                labelTestMeasure.textColor = .red
            }
            else if result > 1.0{
                res = "High"
                labelRiskDescription.text = "Your result is above the standard.Please consult the specialist."
                labelRiskDescription.textColor = .red
                labelTestMeasure.textColor = .red
            }
        }
        else  if gender == "Female"
        {
            print("gen =  female")
            if (result >= 0.65 && result <= 0.80) {
                res = "Low"
                labelTestMeasure.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
                labelRiskDescription.text="Your Result is between the standard.Keep Smiling"
                labelRiskDescription.textColor=UIColor.init(red: 148/255.0, green: 190/255.0, blue: 71/255.0, alpha: 1)
            }
            else if (result >= 0.81 && result <= 0.85){
                res = "Medium"
                labelRiskDescription.text = "Your result is above the standard.Please consult the specialist."
                labelRiskDescription.textColor = .red
                labelTestMeasure.textColor = .red
            }
            else if result > 0.85{
                res = "High"
                labelRiskDescription.text = "Your result is above the standard.Please consult the specialist."
                labelRiskDescription.textColor = .red
                labelTestMeasure.textColor = .red
            }
        }
        
        labelTestMeasure.text = "\(Double(round(1000*result)/1000))"
        print("Waist Hip Ratio count = \(result), and result is \(res ?? "")")
        
    }
    
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        webViewDetail.scrollView.isScrollEnabled = false
        webViewHeight.constant = webViewDetail.scrollView.contentSize.height
    }
    
    
    //MARK: IBActions
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnShareClicked(_ sender: UIButton) {
        
        UIGraphicsBeginImageContextWithOptions(mainView.bounds.size, mainView.isOpaque, 0.0)
        mainView.drawHierarchy(in: mainView.bounds, afterScreenUpdates: false)
        let snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let ssImage : UIImage = snapshotImageFromMyView!
        
        
        let  activityViewController = UIActivityViewController(activityItems: [self,ssImage], applicationActivities: [])
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func btnHomeClicked(_ sender: UIButton) {
        var viewControllers = navigationController?.viewControllers
        viewControllers?.removeLast(2) // views to pop
        navigationController?.setViewControllers(viewControllers!, animated: true)

    }
    
    
    @IBAction func btnLoginClicked(_ sender: UIButton) {
        
        if USERDEFAULT.value(forKey: "userID") as? String != nil{
            
            if let isMember = USERDEFAULT.value(forKey: "isMember") as? String{
                let ismem = "\(isMember)"
                if ismem == "1"{
                    
                }
                else{
                    let joinMemberVC = JoinMemberVC(nibName: "JoinMemberVC", bundle: nil)
                    joinMemberVC.isFromOther = true
                    joinMemberVC.isFromTab = "6"
                    self.navigationController?.pushViewController(joinMemberVC, animated: true)
                }
            }
            else{
                
            }
        }
        else{
            let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
            loginVC.isFromTab = "6"
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
    }
    
    
    //MARK:- Post Data Methods
    func postDataOnWebserviceForWaistHipTest(){
        let completeURL = NSString(format:"%@%@", MainURL,waistHipReportUrl) as String
       
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "waist_circumference": strWaistParam!,
            "hip_circumference": strHipParam!,
            "waist_hip_result":""
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("waistHipReportUrl API Parameter :",finalParams)
        print("waistHipReportUrl API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: waistHipReportUrlTag)
        
    }
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
            
        case waistHipReportUrlTag:
            
            let resultDict = responseObject as! NSDictionary;
            print("waistHipReportUrlTag Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
                
            else if resultDict.value(forKey: "status") as! String == "2"{
                //                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        default:
            break
            
        }
        SVProgressHUD.dismiss()
        
    }
    
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case waistHipReportUrlTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    
}
