//
//  SmokingProgramTestVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 20/07/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class SmokingProgramTestVC: UIViewController, UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate {

    //MARK:- IBOutlets
    
    @IBOutlet var pickerOuterview: UIView!
    @IBOutlet weak var pickerview: UIPickerView!
    @IBOutlet weak var lblPickerTitle: UILabel!
    @IBOutlet weak var btnPickerDone: UIButton!
    @IBOutlet weak var btnPickerCancel: UIButton!

    @IBOutlet weak var txtSCPType: UITextField!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var txtCigarettePerDay: UITextField!
    @IBOutlet weak var txtCigaretteInPack: UITextField!
    @IBOutlet weak var txtPricePerCigarette: UITextField!

    @IBOutlet weak var tabletTypeOuterview: UIView!
    @IBOutlet weak var nicotineOuterview: UIView!
    @IBOutlet weak var txtTableType: UITextField!
    
    var arrDisplay = [String]()
    var isSelectSCPType = false
    var arrSCPType = ["Nicotine Patches","Varenicline tablets"]
    var arrTabletType = ["0.5mg","1mg"]
    var selectSCPType = ""
    var selectTabletType = ""
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        tabletTypeOuterview.isHidden = true
        nicotineOuterview.isHidden = true
        pickerview.delegate = self
        pickerview.dataSource = self
        txtSCPType.delegate = self
         //tblSortBy.frame = CGRect(x:0,y:self.mainOuterview.frame.size.height - CGFloat(sortedTableviewhight),width:ScreenSize.SCREEN_WIDTH,height:CGFloat(sortedTableviewhight))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        tabletTypeOuterview.isHidden = true
        nicotineOuterview.isHidden = true
        isSelectSCPType = false
        selectTabletType = ""
        selectSCPType = ""
        txtTableType.text = ""
        txtCigarettePerDay.text = ""
        txtCigaretteInPack.text = ""
        txtPricePerCigarette.text = ""
    }
    
    //MARK:-
    
    @IBAction func btnSelectTabletTypeTapped(_ sender: UIButton) {
        arrDisplay = arrTabletType
        isSelectSCPType = false
        pickerOuterview.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.view.addSubview(pickerOuterview)
        pickerview.reloadAllComponents()
    }
    
    @IBAction func btnCancelTapped(_ sender: UIButton) {
        isSelectSCPType = false
        pickerOuterview.removeFromSuperview()
    }
    
    @IBAction func btnDoneTapped(_ sender: UIButton) {
        pickerOuterview.removeFromSuperview()
        if(isSelectSCPType)
        {
            selectTabletType = ""
            isSelectSCPType = false
            txtSCPType.text = arrDisplay[pickerview.selectedRow(inComponent: 0)]
            if(txtSCPType.text == arrSCPType[0])
            {
                selectSCPType = "1"
                txtTableType.text = ""
                tabletTypeOuterview.isHidden = true
                nicotineOuterview.isHidden = false
            }
            else
            {
                selectSCPType = "2"
                txtCigarettePerDay.text = ""
                txtCigaretteInPack.text = ""
                txtPricePerCigarette.text = ""
                tabletTypeOuterview.isHidden = false
                nicotineOuterview.isHidden = true
            }
        }
        else
        {
            txtTableType.text = arrDisplay[pickerview.selectedRow(inComponent: 0)]
            //print("tablet type : \(txtTableType.text)")
            selectTabletType = (txtTableType.text == arrTabletType[0]) ? "1":"2"
        }
    }
    
    @IBAction func btnSCPTypeTapped(_ sender: UIButton) {
        arrDisplay = arrSCPType
        isSelectSCPType = true
        pickerOuterview.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.view.addSubview(pickerOuterview)
        pickerview.reloadAllComponents()
    }

    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnStartClicked(_ sender: UIButton) {
        if(selectSCPType == "1")
        {
            if Utility.isEmpty(txtSCPType.text!){
                showAlert(Appname, title: "Please select SCP Type")
            }
            else if Utility.isEmpty(txtCigarettePerDay.text!){
                showAlert(Appname, title: "Please enter number of cigarette per day.")
            }
            else if Utility.isEmpty(txtCigaretteInPack.text!){
                showAlert(Appname, title: "Please enter number of cigarette in pack.")
            }
            else if Utility.isEmpty(txtPricePerCigarette.text!){
                showAlert(Appname, title: "Please enter price per cigarette.")
            }
            else{
                if Reachability.isConnectedToNetwork(){
                    self.updateUserSmokingDetail()
                }
                else {
                    showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                }
            }
        }
        else
        {
            if Utility.isEmpty(txtSCPType.text!){
                showAlert(Appname, title: "Please select SCP Type.")
            }
            else if Utility.isEmpty(txtTableType.text!){
                showAlert(Appname, title: "Please select tablet type.")
            }
            else{
                if Reachability.isConnectedToNetwork(){
                    self.updateUserSmokingDetail()
                }
                else {
                    showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                }
            }
        }
       
    }
    
    //MARK:- Textfield Delegate
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        mainScrollView.setContentOffset(.zero, animated: true)
        return true
    }
    
    // MARK:- UIPickerView Data Source
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrDisplay.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrDisplay[row]
    }
    
    //MARK:- Post data Methods
    func updateUserSmokingDetail(){
        SVProgressHUD.show(withStatus: "Loading...")
        let completeURL = NSString(format:"%@%@", MainURL,editUserProfileURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "cigarette_per_day" : txtCigarettePerDay.text!,
            "cigarette_in_a_pack": txtCigaretteInPack.text!,
            "price_per_cigarette": txtPricePerCigarette.text!,
            "scp_type" : selectSCPType,
            "tablet_type" : selectTabletType
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("Smoking API Parameter :",finalParams)
        print("Smoking API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: editUserProfileURLTag)
    }
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
            
        case editUserProfileURLTag:
            
            let resultDict = responseObject as! NSDictionary;
            print("editUserProfileURLTag Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                let vc = SmokingProgramResultVC(nibName: "SmokingProgramResultVC", bundle: nil)
                self.navigationController?.pushViewController(vc, animated: true)

            }
            else{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            /*else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                //self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }*/
            break
            
        default:
            break
            
        }
        SVProgressHUD.dismiss()
        
    }
    
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case editUserProfileURLTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
}
