//
//  SmokingCessationProgramVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 20/07/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit

class SmokingCessationProgramVC: UIViewController {

    //IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
    }

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        //self.tabBarController?.tabBar.isHidden = false
    }
    //IBAction Methods
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func btnContinueClicked(_ sender: UIButton) {
        let detail = SmokingProgramDisclaimerVC(nibName: "SmokingProgramDisclaimerVC", bundle: nil)
        self.navigationController?.pushViewController(detail, animated: true)
    }
    
}
