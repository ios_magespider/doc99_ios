//
//  SmokingProgramResultVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 20/07/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD
import MBCircularProgressBar
import JTAppleCalendar
class SmokingProgramResultVC: UIViewController,JTAppleCalendarViewDataSource, JTAppleCalendarViewDelegate {
   

    //MARK:- IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var lblWeek: UILabel!
    @IBOutlet weak var labelRemianDays: UILabel!
    @IBOutlet weak var labelMoneySavedValue: UILabel!
    @IBOutlet weak var labelCigaretteNotSmokeValue: UILabel!
    @IBOutlet weak var labelMoneyWasteValue: UILabel!
    @IBOutlet weak var labelCigaretteSmokeValue: UILabel!
    @IBOutlet weak var alertViewSmokeCigarette: UIView!
    @IBOutlet weak var alertViewResetProgram: UIView!
   
    @IBOutlet weak var progressBar: MBCircularProgressBarView!
    @IBOutlet weak var cvCalander: JTAppleCalendarView!
    @IBOutlet var CalenderOuterview: UIView!
    @IBOutlet weak var lblMonthDisplay: UILabel!
    
    
    //MARK:- variables
    var isSuccess = false
    var remainingDays = ""
    let formatter = DateFormatter()
    var iii: Date?
    var selectedPassDate:String!
    var selectedCalory:String!
    var startDate = ""
    var endDate = ""
    var selectedDate = ""
    var dateData = NSMutableArray()
    var startDisplayDate = ""
    var endDisplayDate = ""
    var isAvailableDate = false
    var arrDates = [String]()
    var arrSCPData = [SCPDataModel]()
    var calenderDisplayMonth = ""
    var calenderDisplayYear = ""
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.tabBar.isHidden = true
        alertViewSmokeCigarette.frame = CGRect(x: 0, y: ScreenSize.SCREEN_HEIGHT, width: self.view.frame.size.width, height: self.view.frame.size.height)
        alertViewResetProgram.frame = CGRect(x: 0, y: ScreenSize.SCREEN_HEIGHT, width: self.view.frame.size.width, height: self.view.frame.size.height)
        
        if Reachability.isConnectedToNetwork(){
            self.getUserSmokingDetail()
        }
        else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
    }

    //MARK: - Private Method
    func setDefault()
    {
        cvCalander.register(UINib(nibName: "DateCell", bundle: nil), forCellWithReuseIdentifier: "DateCell")
        self.cvCalander.allowsMultipleSelection = false
        self.cvCalander.isRangeSelectionUsed = false
        self.cvCalander.ibCalendarDelegate = self
        self.cvCalander.ibCalendarDataSource = self
        self.cvCalander.minimumLineSpacing = 0
        self.cvCalander.minimumInteritemSpacing = 0
        self.cvCalander.cellSize = (cvCalander.frame.size.width/7)
        self.cvCalander.reloadData()
        self.cvCalander.scrollToDate(Date())
        self.cvCalander.visibleDates { (visibledate) in
            self.setupViewsOfCalendar(from: visibledate)
        }
        
    }
   
   
    //MARK:- IBAction Methods
    @IBAction func btnCloseCalenderTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            self.CalenderOuterview.alpha = 0.0
        }) { (completion) in
            self.CalenderOuterview.removeFromSuperview()
        }
    }
    
    @IBAction func btnCalenderOpenTapped(_ sender: UIButton) {
       
        CalenderOuterview.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.view.addSubview(CalenderOuterview)
        setDefault()
        CalenderOuterview.alpha = 0.0
        UIView.animate(withDuration: 0.3, animations: {
             self.CalenderOuterview.alpha = 1.0
        }) { (completion) in
            
        }
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnResetProgramClicked(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3) {
            self.alertViewResetProgram.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
    }
    
    @IBAction func btnISmokedClicked(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3) {
            self.alertViewSmokeCigarette.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
    }
    
    @IBAction func btnCigaretteSmokeCancelClicked(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3) {
            self.alertViewSmokeCigarette.frame = CGRect(x: 0, y: ScreenSize.SCREEN_HEIGHT, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
    }
    
    @IBAction func btnCigaretteSmokeConfirmClicked(_ sender: UIButton) {
        if Reachability.isConnectedToNetwork(){
            //self.resetSmokeProgram()
            self.setUserSmokeCigarett()
        }
        else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        UIView.animate(withDuration: 0.3) {
            self.alertViewSmokeCigarette.frame = CGRect(x: 0, y: ScreenSize.SCREEN_HEIGHT, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
    }
    
    
    @IBAction func btnDenyResetClicked(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3) {
            self.alertViewResetProgram.frame = CGRect(x: 0, y: ScreenSize.SCREEN_HEIGHT, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
    }
    
    @IBAction func btnConfirmResetClicked(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3) {
            self.alertViewResetProgram.frame = CGRect(x: 0, y: ScreenSize.SCREEN_HEIGHT, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
        if Reachability.isConnectedToNetwork(){
            //self.setUserSmokeCigarett()
            self.resetSmokeProgram()
        }
        else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
    }
    
    //MARK:- Post data Methods
    func getUserSmokingDetail(){
        SVProgressHUD.show(withStatus: "Loading...")
        let completeURL = NSString(format:"%@%@", MainURL,getUserSmokingDetailUrl) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "month":calenderDisplayMonth,
            "year":calenderDisplayYear
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("Smoking detail URL :",completeURL)
        print("Smoking detail Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getUserSmokingDetailUrltag)
    }
    
    func setUserSmokeCigarett(){
        SVProgressHUD.show(withStatus: "Loading...")
        let completeURL = NSString(format:"%@%@", MainURL,userSmokeCigaretteUrl) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("setUserSmokeCigarett URL :",completeURL)
        print("setUserSmokeCigarett Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: userSmokeCigaretteTag)
    }
    
    
    func resetSmokeProgram(){
        SVProgressHUD.show(withStatus: "Loading...")
        let completeURL = NSString(format:"%@%@", MainURL,userResetSmokeProgramUrl) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("setUserSmokeCigarett URL :",completeURL)
        print("setUserSmokeCigarett Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: userResetSmokeProgarmTag)
    }
    
    func dateStore()
    {
        //formatter.dateFormat = "yyyy-MM-dd"
        //let calenderDate = formatter.string(from: date)
        arrDates.removeAll()
       /* startDisplayDate = "2018-07-14"
        endDisplayDate = "2018-08-26"*/
        formatter.dateFormat = "yyyy-MM-dd"
        let currentDate = formatter.date(from: formatter.string(from:Date()))
        
        var stdate = formatter.date(from: startDisplayDate)!//formatter.string(from: startDisplayDate)
        let enddate = formatter.date(from: endDisplayDate)!
        while stdate <= enddate {
            arrDates.append(formatter.string(from: stdate))
            if(stdate == currentDate)
            {
                break
            }
            stdate = Calendar.current.date(byAdding: .day, value: 1, to: stdate)!
        }
        cvCalander.reloadData()
        print(arrDates)
    }
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
            
        case getUserSmokingDetailUrltag:
            
            let resultDict = responseObject as! NSDictionary;
            print("pulmonaryReportUrlTag Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                if let detail = resultDict["data"] as? NSDictionary{
                    
                    isSuccess = true
                    
                    let savedValue = detail.value(forKey: "money_saved") as! Int
                    let cigarette_not_smoked = detail.value(forKey: "cigarette_not_smoked") as! Int
                    let money_waste = detail.value(forKey: "money_waste") as! Int
                    let cigarette_smoked = detail.value(forKey: "cigarette_smoked") as! Int
                    
                    let remaining_days = detail.value(forKey: "remaining_days") as! Int
                    let weekProgram = detail.value(forKey: "week_program") as! String
                    //self.startDisplayDate = detail.value(forKey: "scp_start_date") as! String
                    lblWeek.text = weekProgram + " Weeks"
                    progressBar.maxValue = CGFloat(Int(weekProgram)!*7)
                    labelMoneySavedValue.text = "$ \(savedValue)"
                    labelCigaretteNotSmokeValue.text = "\(cigarette_not_smoked)"
                    labelMoneyWasteValue.text = "$ \(money_waste)"
                    labelCigaretteSmokeValue.text = "\(cigarette_smoked)"
                    let scp_start_date = detail.value(forKey: "scp_start_date") as! String
                    let tempArray = detail.value(forKey: "scp_data") as! [[String:Any]]
                    self.arrSCPData = tempArray.map(SCPDataModel.init)//detail.value(forKey: "scp_data") as! NSArray
                    
                    if(scp_start_date != "0000-00-00 00:00:00" && scp_start_date != "")
                    {
                        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        let date = formatter.date(from: detail.value(forKey: "scp_start_date") as! String)
                        formatter.dateFormat = "yyyy-MM-dd"
                        self.startDisplayDate = formatter.string(from: date!)
                    }
                    
                    let scp_end_date = detail.value(forKey: "scp_end_date") as! String
                    if(scp_end_date != "0000-00-00 00:00:00" && scp_end_date != "")
                    {
                        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        let date = formatter.date(from: detail.value(forKey: "scp_end_date") as! String)
                        formatter.dateFormat = "yyyy-MM-dd"
                        self.endDisplayDate = formatter.string(from: date!)
                    }
                    
                    self.dateStore()
                    print(self.startDisplayDate)
                    labelRemianDays.text = "\(remaining_days)"
                    UIView.animate(withDuration: 0.5) {
                        self.progressBar.value = CGFloat(remaining_days)
                    }
                }
            }
            else{//} if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            break
            
        case userSmokeCigaretteTag:
            let resultDict = responseObject as! NSDictionary;
            print("userSmokeCigaretteTag Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                //self.getUserSmokingDetail()
                //self.navigationController?.popToRootViewController(animated: true)
                self.viewDidLoad()
            }
            else{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            
            break
           
        case userResetSmokeProgarmTag:
            let resultDict = responseObject as! NSDictionary;
            print("userResetSmokeProgarmTag Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                //self.navigationController?.popViewController(animated: false)
                let vc = SmokingCessationProgramVC(nibName: "SmokingCessationProgramVC",bundle: nil)
                self.navigationController?.pushViewController(vc,animated: true)
                
            }
            else{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            break
            
        default:
            break
            
        }
        SVProgressHUD.dismiss()
        
    }
    
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case getUserSmokingDetailUrltag:
            
            break
        case userSmokeCigaretteTag:
            break
            
        default:
            break
        }
        SVProgressHUD.dismiss()
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
}

extension SmokingProgramResultVC {
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        
        let date = visibleDates.monthDates.first!.date
        formatter.dateFormat = "yyyy"
        let year = self.formatter.string(from: date)
        formatter.dateFormat = "MMMM"
        let month = self.formatter.string(from: date)
        formatter.dateFormat = "MM"
        let monthDigit = self.formatter.string(from: date)
        self.lblMonthDisplay.text = "\(month) \(year)"
        print(cvCalander.visibleDates().outdates)
        calenderDisplayMonth = monthDigit
        calenderDisplayYear = year
        
        if(cvCalander.visibleDates().indates.count > 0)
        {
            if(cvCalander.visibleDates().indates.count == 1)
            {
                startDate = formatter.string(from: cvCalander.visibleDates().monthDates[0].date)
            }
            else
            {
                startDate = formatter.string(from: cvCalander.visibleDates().indates[0].date)
            }
        }
        else
        {
            startDate = formatter.string(from: cvCalander.visibleDates().monthDates[0].date)
        }
        
        endDate = formatter.string(from: cvCalander.visibleDates().outdates[cvCalander.visibleDates().outdates.count-1].date)
        getUserSmokingDetail()
        // endDate = formatter.string(from: Calendar.current.date(byAdding: .day, value: 1, to: cvCalander.visibleDates().outdates[cvCalander.visibleDates().outdates.count-1].date)!)
      
    }
    func configureCell(view: JTAppleCell?, cellState: CellState) {
        guard let myCustomCell = view as? DateCell  else { return }
       // handleCellTextColor(view: myCustomCell, cellState: cellState)
        //handleCellSelection(view: myCustomCell, cellState: cellState)
        
        formatter.dateFormat = "yyyy-MM-dd"
       // let strDate = formatter.string(from: cellState.date)
        myCustomCell.lblDate.backgroundColor = UIColor.red
        // if(arrMainCalendarData)
        
        /*if(arrMainCalendarData.count > 0)
        {
            let isMatch = arrMainCalendarData.filter{ $0.date == strDate}.first
            if(isMatch != nil)
            {
                if(isMatch?.color == "1")
                {
                    myCustomCell.lblBottomDot.backgroundColor = Constant.Color.GreenDotColor
                }
                else if(isMatch?.color == "2")
                {
                    myCustomCell.lblBottomDot.backgroundColor = Constant.Color.RedDotColor
                }
                else
                {
                    myCustomCell.lblBottomDot.backgroundColor = UIColor.clear
                }
            }
        }*/
    }
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let myCustomCell = calendar.dequeueReusableCell(withReuseIdentifier: "DateCell", for: indexPath) as! DateCell
        myCustomCell.lblDate.text = cellState.text
        myCustomCell.lblOuterview.layer.cornerRadius = myCustomCell.lblOuterview.frame.size.height/2
        myCustomCell.lblOuterview.clipsToBounds = true
        if cellState.dateBelongsTo != .thisMonth {
            myCustomCell.lblOuterview.isHidden = true
        }
        else
        {
            myCustomCell.lblOuterview.isHidden = false
        }
        formatter.dateFormat = "yyyy-MM-dd"
        let calenderDate = formatter.string(from: date)
        
        let isAvailableData = arrSCPData.filter{$0.date == calenderDate}.first
        
        if(isAvailableData != nil)
        {
            if(isAvailableData?.color == 0)
            {
                myCustomCell.lblDate.backgroundColor = UIColor.UIColorFromHex(hex: "D7D7D7", alpha: 1.0)
            }
            else if(isAvailableData?.color == 1)
            {
                myCustomCell.lblDate.backgroundColor = UIColor.UIColorFromHex(hex:"F25402" /*"E95101"*/, alpha: 1.0)
            }
            else if(isAvailableData?.color == 2)
            {
                myCustomCell.lblDate.backgroundColor = UIColor.yellow
            }
            else if(isAvailableData?.color == 3)
            {
                myCustomCell.lblDate.backgroundColor = UIColor.red
            }
            else if(isAvailableData?.color == 4)
            {
                myCustomCell.lblDate.backgroundColor = UIColor.UIColorFromHex(hex:"00b300" /*"E95101"*/, alpha: 1.0)
            }
            else
            {
                 myCustomCell.lblDate.backgroundColor = UIColor.UIColorFromHex(hex: "D7D7D7", alpha: 1.0)
            }
        }
        else
        {
            myCustomCell.lblDate.backgroundColor = UIColor.UIColorFromHex(hex: "D7D7D7", alpha: 1.0)
        }
        /*if(arrDates.contains(calenderDate))
        {
             myCustomCell.lblDate.backgroundColor = UIColor.UIColorFromHex(hex:"F25402" /*"E95101"*/, alpha: 1.0)
        }
        else
        {
             myCustomCell.lblDate.backgroundColor = UIColor.UIColorFromHex(hex: "D7D7D7", alpha: 1.0)
        }*/
        
        
        /*startDisplayDate = "2018-07-14"
        endDisplayDate = "2018-08-19"
        let currentDate = formatter.string(from: Date())
        print(calenderDate + "  " +  currentDate)
        var diffInDays = 0
      //  print("day diffrent \(diffInDays)")
        if(/*startDisplayDate == calenderDate || */isAvailableDate)
        {
            let endDateDiff = Calendar.current.dateComponents([.day], from:  date, to: formatter.date(from: endDisplayDate)!).day!
            if(endDateDiff < 0)
            {
                isAvailableDate = false
                diffInDays = 0
            }
            else
            {
                isAvailableDate = true
                diffInDays = Calendar.current.dateComponents([.day], from:  date, to: Date()).day!
            }
            
        }
        let endDateDiff = Calendar.current.dateComponents([.day], from:  date, to: formatter.date(from: endDisplayDate)!).day!
        if(diffInDays > 0  || calenderDate == currentDate)
        {
            if(endDateDiff<0)
            {
                myCustomCell.lblDate.backgroundColor = UIColor.UIColorFromHex(hex: "D7D7D7", alpha: 1.0)
            }
            else
            {
                myCustomCell.lblDate.backgroundColor = UIColor.UIColorFromHex(hex:"F25402" /*"E95101"*/, alpha: 1.0)
            }
        }
        else
        {
            isAvailableDate = false
            myCustomCell.lblDate.backgroundColor = UIColor.UIColorFromHex(hex: "D7D7D7", alpha: 1.0)
        }*/
        return myCustomCell
    }
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        setupViewsOfCalendar(from: visibleDates)
    }
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        let startDate = formatter.date(from: "2005 01 01")!
        let endDate = formatter.date(from: "2030 01 01")!
        let parameters = ConfigurationParameters(startDate: startDate,endDate: endDate,numberOfRows: 6, firstDayOfWeek: .sunday)
        return parameters
    }
    
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        //
    }
    
}

class SCPDataModel:NSObject
{
    var cigarette_count = ""
    var color = 0
    var date = ""
    
    init(dic:[String:Any])
    {
        cigarette_count = dic["cigarette_count"] as? String ?? ""
        color = dic["color"] as? Int ?? 0
        date = dic["date"] as? String ?? ""
    }
}
