//
//  SmokingProgramDisclaimerVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 20/07/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class SmokingProgramDisclaimerVC: UIViewController, UIWebViewDelegate {

    //MARK:- IBOUtlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var webViewSmokingDetail: UIWebView!
    
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetPrescriptionTermCondition), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
    }
    
    
    //MARK:- WebView Delegate Methods
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("WebView Error : ",error.localizedDescription)
    }
    
    //MARK:- IBActions
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNextClicked(_ sender: UIButton) {
        let vc = SmokingProgramTestVC(nibName: "SmokingProgramTestVC", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
 
    
    //MARK:- API Request/Response Methods
    func postDataOnWebserviceForGetPrescriptionTermCondition(){
        
        let completeURL = NSString(format:"%@%@", MainURL,cmsWebPageCallURL) as String
        let params:NSDictionary = [
            "page" : "smoking-cessation-program"
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
//        print("TermCondition API Parameter :",finalParams)
//        print("TermCondition API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: smokingProgramDetailUrlTag)
    }
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case smokingProgramDetailUrlTag:
            let resultDict = responseObject as! NSDictionary;
            //print("TermCondition Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                let myDescription = resultDict.value(forKey: "page_description") as! String
                //let abc = myDescription.replacingOccurrences(of: "\n", with: "")
                webViewSmokingDetail.loadHTMLString(myDescription, baseURL: nil)
            }
            break
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case smokingProgramDetailUrlTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
        }
        //print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
}
