//
//  ReportVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 02/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD
import QuartzCore
class ReportVC: UIViewController,LineChartDelegate {
   
    
   
   // @IBOutlet var lblBmi:UILabel!
    //@IBOutlet var lblOst:UILabel!
    //@IBOutlet var lblTitleBmi:UILabel!
   // @IBOutlet var lblTitleOst:UILabel!
    @IBOutlet var lblHome:UILabel!
    @IBOutlet var lblHeader:UILabel!

    var label = UILabel()
    var lineChart: LineChart!
    var bmiValue:String!
    var ostValue:String!
    
    var barChart :PNBarChart!
    @IBOutlet var viewPnChart:UIView!
    var barChartYValuesData :NSArray!
    var arrLineChartData:NSArray?
    var pieChart :PNPieChart!
    @IBOutlet var viewPieChart:UIView!

  //  var lineChart : PNLineChart!
    
    @IBOutlet var lblColor1:UILabel!
    @IBOutlet var lblColor2:UILabel!
    @IBOutlet var lblColor3:UILabel!
    @IBOutlet var lblColor4:UILabel!
    
    @IBOutlet var lblTitleColor1:UILabel!
    @IBOutlet var lblTitleColor2:UILabel!
    @IBOutlet var lblTitleColor3:UILabel!
    @IBOutlet var lblTitleColor4:UILabel!
    @IBOutlet var btnshare:UIButton!
    @IBOutlet var btnDoc99:UIButton!
    
    @IBOutlet var viewReport:UIView!


    var reportSnapImage:UIImage!
    
    // FIXME: - VIEW CONTROLLER METHODS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //lblBmi.text = bmiValue
       // lblOst.text = ostValue
        self.generalViewControllerSetting()
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        //self.setLocalizationText()
        self.addTapGestureInOurView()
        self.setupPieChart()
        self.setupLineChart()
        
        lblColor1.layer.cornerRadius =  lblColor1.frame.size.width / 2
        lblColor1.layer.masksToBounds = true
        lblColor2.layer.cornerRadius =  lblColor2.frame.size.width / 2
        lblColor2.layer.masksToBounds = true
        lblColor3.layer.cornerRadius =  lblColor3.frame.size.width / 2
        lblColor3.layer.masksToBounds = true
        lblColor4.layer.cornerRadius =  lblColor4.frame.size.width / 2
        lblColor4.layer.masksToBounds = true
       /* lblColor5.layer.cornerRadius =  lblColor5.frame.size.width / 2
        lblColor5.layer.masksToBounds = true*/

    }
    
    
    
    func setLocalizationText(){
      //  lblTitleBmi.text = NSLocalizedString("BMI", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
       // lblTitleOst.text = NSLocalizedString("OST", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblHome.text = NSLocalizedString("HOME", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblHeader.text = NSLocalizedString("Report", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
        lblTitleColor1.text = NSLocalizedString("Blood Pressure", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitleColor2.text = NSLocalizedString("Nutrition", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitleColor3.text = NSLocalizedString("Physical Exercise", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitleColor4.text = NSLocalizedString("Pharmacist Advice", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        //lblTitleColor5.text = NSLocalizedString("Last Health Index Score", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

        btnshare.setTitle(NSLocalizedString("Share Report", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        btnDoc99.setTitle(NSLocalizedString("Consult Doc-99", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)

    }
    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    func didSelectDataPoint(_ x: CGFloat, yValues: [CGFloat]) {
        
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        if viewTouched!.isKind(of: UIButton.self) && viewTouched!.isKind(of:UITextView.self){
            
        }
        else{
            self.view.endEditing(true)
        }
        if let barView = viewTouched as? PNBar {
            let aa = barChart.userClickedOnBarChartIndex(barIndex: barView.tag)
            print(aa)
            
            if aa == 0{
                
            }
            else if aa == 1{
                
            }
            else if aa == 2{
                
            }
        }
    }

    /*private func setLineChart() -> PNLineChart {
        
        return lineChart
    }*/
    
    func setupLineChart(){
        if(arrLineChartData != nil)
        {
            if((arrLineChartData?.count)! > 0)
            {
               // let themutablearray = [[[arrLineChartData reverseObjectEnumerator] allObjects] mutableCopy];
                let reverseArray = arrLineChartData?.reverseObjectEnumerator().allObjects
                arrLineChartData = reverseArray! as NSArray
                var stringYValuesData = [CGFloat]()//[CGFloat(10),CGFloat(20),CGFloat(30),CGFloat(50),CGFloat(20)]//[CGFloat]()
                var stringXValuesData = [String]()//["Test1","Test2","Test3","Test4","Test5"]//[String]()
                for i in 0..<arrLineChartData!.count{
                   
                    let dic = arrLineChartData![i] as! NSDictionary
                    let formate = DateFormatter()
                    let healthDate = dic["health_date"] as? String
                    formate.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let dates = formate.date(from: healthDate!)
                    formate.dateFormat = "yyyy-MM-dd"
                    let value = formate.string(from: dates!)
                    
                    stringXValuesData.append(value)
                    
                    if let score = dic["score"] as? NSNumber
                    {
                        stringYValuesData.append(CGFloat(score))
                    }
                    else if let score = dic["score"] as? String
                    {
                        stringYValuesData.append(CGFloat(Double(score)!))
                    }
                    else
                    {
                        stringYValuesData.append(CGFloat(0))
                    }
                    if(i == 4)
                    {
                        break
                    }
                }
                
                var views: [String: AnyObject] = [:]
                views["label"] = label
                let data: [CGFloat] = stringYValuesData
                let xLabels: [String] = stringXValuesData
                lineChart = LineChart()
                lineChart.frame = CGRect(x:0,y:0,width:viewPnChart.frame.size.width,height:viewPnChart.frame.size.height)
                lineChart.animation.enabled = true
                lineChart.area = false
                lineChart.x.labels.visible = true
                lineChart.x.grid.count = 5
                //lineChart.y.grid.count = 5
                lineChart.y.axis.inset = 20.0
                //lineChart.y.labels.values = ["0","50","100","150","200"]
                lineChart.y.grid.count = 10
                lineChart.x.labels.values = xLabels
                lineChart.y.labels.visible = true
                lineChart.addLine(data)
                lineChart.translatesAutoresizingMaskIntoConstraints = false
                lineChart.delegate = self
                self.viewPnChart.addSubview(lineChart)
                views["chart"] = lineChart
            }
        }
        

    }
    
    func setupPieChart(){
        
        let item1 = PNPieChartDataItem(dateValue: barChartYValuesData.object(at: 0) as! CGFloat, dateColor:  UIColor.init(red: 110.0/255.0, green: 121.0/255.0, blue: 200.0/255.0, alpha: 1.0), description: "")
        let item2 = PNPieChartDataItem(dateValue: barChartYValuesData.object(at: 1) as! CGFloat, dateColor: UIColor.init(red: 38.0/255.0, green: 206.0/255.0, blue: 252.0/255.0, alpha: 1.0), description: "")
        let item3 = PNPieChartDataItem(dateValue: barChartYValuesData.object(at: 2) as! CGFloat, dateColor: UIColor.init(red: 131.0/255.0, green: 208.0/255.0, blue: 115.0/255.0, alpha: 1.0), description: "")
        let item4 = PNPieChartDataItem(dateValue: barChartYValuesData.object(at: 3) as! CGFloat, dateColor: UIColor.init(red: 255.0/255.0, green: 183.0/255.0, blue: 106.0/255.0, alpha: 1.0), description: "")
        //let item5 = PNPieChartDataItem(dateValue: barChartYValuesData.object(at: 4) as! CGFloat, dateColor: UIColor.init(red: 255.0/255.0, green: 115.0/255.0, blue: 94.0/255.0, alpha: 1.0), description: "")

        
        
        let frame = CGRect(x: 0.0, y: 0.0, width: 150.0, height: 150.0)
        let items: [PNPieChartDataItem] = [item1, item2, item3,item4]
//        let items: [PNPieChartDataItem] = barChartYValuesData as! [PNPieChartDataItem]

        pieChart = PNPieChart(frame: frame, items: items)
        pieChart.descriptionTextColor = UIColor.clear
//        pieChart.descriptionTextFont = UIFont(name: "Avenir-Medium", size: 14.0)!
        
        
        viewPieChart.addSubview(pieChart)
        pieChart.strokeChart()
 }
    
    // TODO: - ACTION METHODS
    @IBAction func btnHomeClicked(_ sender: UIButton) {
//        let homeVC = TabBarVC(nibName: "TabBarVC", bundle: nil)
//        homeVC.selectedIndexOfMyTabbarController = 0
//        self.navigationController?.pushViewController(homeVC, animated: true)
        
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
        for aViewController:UIViewController in viewControllers {
            if aViewController.isKind(of: HealthIndexCalculatorVC.self) {
                _ = self.navigationController?.popToViewController(aViewController, animated: true)
            }
        }
        
    }
    
    @IBAction func btnShareReportClicked(_ sender: UIButton) {
        
        reportSnapImage =  pb_takeSnapshot(snapShotView: viewReport)
        let date = getStringDateFromDate(dateFormat: "MMM d, yyyy", enterDate: Date())
        let username = USERDEFAULT.value(forKey: "fullName") as! String

        let textSharing = "Username : \(username) \nDate: \(date)"

        //self,"Sharing From Doc99",
        
        let  activityViewController = UIActivityViewController(activityItems: [self,reportSnapImage,textSharing], applicationActivities: [])
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        

    }
    @IBAction func btnConsultClicked(_ sender: UIButton) {
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
   
}
