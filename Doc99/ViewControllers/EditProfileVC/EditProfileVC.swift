//
//  EditProfileVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 14/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class EditProfileVC: UIViewController,EMCCountryDelegate,DiallingCodeDelegate,UIAlertViewDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var txtFullname:UITextField!
    @IBOutlet var txtEmailid:UITextField!
    @IBOutlet var txtMobile:UITextField!
    @IBOutlet var txtCountryCode:UITextField!
    @IBOutlet var txtBirthdate:UITextField!
    @IBOutlet var imageViewArrow:UIImageView!

    
    @IBOutlet var datePicker:UIDatePicker!
    @IBOutlet var viewDatePicker:UIView!
    
    @IBOutlet var lblHeader:UILabel!
    @IBOutlet var lblTitleName:UILabel!
    @IBOutlet var lblTitleEmail:UILabel!
    @IBOutlet var lblTitleMobile:UILabel!
    @IBOutlet var lblTitleBirthday:UILabel!
    @IBOutlet var btnUpdate:UIButton!

    @IBOutlet var viewCountryPicker: UIView!
    @IBOutlet weak var tableCountryCode: UITableView!
    @IBOutlet weak var btnCloseCountryView: UIButton!


    
    var arrayCountryCode : NSArray = [["code":"852","country":"Hong Kong"],["code":"65","country":"Singapore"],["code":"60","country":"Malaysia"],
                                      ["code":"66","country":"Thailand"],["code":"850","country":"Korea"],["code":"81","country":"Japan"],
                                      ["code":"853","country":"Macau"],["code":"86","country":"China"],["code":"91","country":"India"],
                                      ["code":"1","country":"USA"],["code":"1","country":"Canada"],["code":"61","country":"Australia"]]
    
    // MARK:- VIEW CONTROLLER METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        imageViewArrow.isHidden = false

        let notificationName1 = Notification.Name("notificationForGetProfileData")
        NotificationCenter.default.addObserver(self, selector: #selector(self.getProfileData), name: notificationName1, object: nil)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
        
        viewDatePicker.frame = CGRect(x: self.view.frame.size.width - self.viewDatePicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: viewDatePicker.frame.size.width, height: viewDatePicker.frame.size.height)

        if let countryCode = USERDEFAULT.value(forKey: "countryCode") as? String{
            txtCountryCode.text = "\(countryCode)"
            imageViewArrow.isHidden = true
        }
        else{
            imageViewArrow.isHidden = false
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getProfileData() {
        if Reachability.isConnectedToNetwork() == true {
//            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetProfile), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
    }

    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetProfile), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }

        self.addTapGestureInOurView()
        self.NavBarNumberPad()
        self.setLocalizationText()
    }
    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        if viewTouched!.isKind(of: UIButton.self){
            
        }
        else{
            self.view.endEditing(true)
            //scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        }
        
        if (viewTouched!.superview!.superview == viewDatePicker) {
        }else if(viewTouched!.superview == viewDatePicker){
        }else if(viewTouched == viewDatePicker){
        }else{
            UIView.animate(withDuration: 0.3) {
                
                self.viewDatePicker.frame=CGRect(x: self.view.frame.size.width - self.viewDatePicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewDatePicker.frame.size.width, height: self.viewDatePicker.frame.size.height)
                
            }
        }

        
        
    }
    
    func setProfileData(profileData:NSMutableDictionary){
        print("Profile Data : " , profileData)
        
        if let fullname  = profileData.value(forKey: "fullname") as? String{
            txtFullname.text = "\(fullname)"
        }
        
        if let email  = profileData.value(forKey: "email") as? String{
            txtEmailid.text = "\(email)"
        }
        
        if let countryCode  = profileData.value(forKey: "ccode") as? String{
            txtCountryCode.text = "\(countryCode)"
        }
        
        if let phoneNumber  = profileData.value(forKey: "phone") as? String{
            txtMobile.text = "\(phoneNumber)"
        }
        
        if let dateofBirth  = profileData.value(forKey: "dob") as? String{
            txtBirthdate.text = "\(dateofBirth)"
        }
    }
    
    // Numeric Keyboard Hide Method
    func NavBarNumberPad(){
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        numberToolbar.barStyle = .default
        
        let viewBack = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        viewBack.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        numberToolbar.addSubview(viewBack)
        
        let btnCancel = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelNumberPad))
        btnCancel.tintColor = UIColor.white
        
        let btnDone = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.doneWithNumberPad))
        btnDone.tintColor = UIColor.white
        
        numberToolbar.items = NSArray(objects: btnCancel,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),btnDone) as? [UIBarButtonItem]
        
        
        numberToolbar.sizeToFit()
        txtMobile.inputAccessoryView = numberToolbar
    }
    
    func cancelNumberPad(){
        txtMobile.resignFirstResponder()
    }
    
    func doneWithNumberPad(){
        txtMobile.resignFirstResponder()
    }
    
    
    func setLocalizationText(){
        
        btnUpdate.setTitle(NSLocalizedString("UPDATE PROFILE", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        
        
        
        lblTitleName.text = NSLocalizedString("Name", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitleEmail.text = NSLocalizedString("Email Address", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitleMobile.text = NSLocalizedString("Mobile", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitleBirthday.text = NSLocalizedString("Birthdate", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblHeader.text = NSLocalizedString("Edit Profile", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

        
        
        
    }

    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    
    //AlertDelegate Methods protocol
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }
    

    
    
    // TODO: - DELEGATE METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
          //  scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
            
        }
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var YOffset:Int = 0
        
        if (textField.tag==4) {
            YOffset=0
        }else if (textField.tag==5){
            YOffset=0
        }else if (textField.tag==6){
            YOffset=5
        }else if (textField.tag==7){
            YOffset=15
        }else if (textField.tag==8){
            YOffset=20
        }
        
        if (DeviceType.IS_IPHONE_4_OR_LESS) {
            
            if (textField.tag==4) {
                YOffset=0
            }else if (textField.tag==5){
                YOffset=10
            }else if (textField.tag==6){
                YOffset=15
            }else if (textField.tag==7){
                YOffset=20
            }else if (textField.tag==8){
                YOffset=25
            }
            
        }
      //  scrMain.setContentOffset(CGPoint(x: 0, y: CGFloat(textField.tag * YOffset)), animated: true)
        return true
        
    }
    
    
    //MARK:- Table Delegate Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCountryCode.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cellIdentifier")
        cell.selectionStyle = .none
        
        
        let str = String(format: "+%@  %@", (arrayCountryCode[indexPath.row]as! NSDictionary).value(forKey: "code")as? String ?? "", (arrayCountryCode[indexPath.row]as! NSDictionary).value(forKey: "country")as? String ?? "")
        
        cell.textLabel?.text = str
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        txtCountryCode.text = String(format: "+%@", (arrayCountryCode[indexPath.row]as! NSDictionary).value(forKey: "code")as? String ?? "")
        self.btnCloseCountryViewClicked(btnCloseCountryView)
    }
    
    @IBAction func btnCloseCountryViewClicked(_ sender: UIButton) {
        viewCountryPicker.removeFromSuperview()
    }
    
    //MARK: - EMCCountryPickerController Delegates Methods
    
    func countryController(_ sender: Any!, didSelect chosenCountry: EMCCountry?) {
        // Do something with chosenCountry
        //   print(chosenCountry.countryName)
        print(chosenCountry?.countryCode ?? "Blank")
        print(chosenCountry?.countryName() ?? "Blank")
        let diallingCode:DiallingCode = DiallingCode.init(delegate: self)
        //  diallingCode.getCountriesWithDiallingCode(chosenCountry.countryCode)
        diallingCode.getForCountry(chosenCountry?.countryCode)
    }
    
    //MARK: - DiallingCode Delegates Methods
    func failedToGetDiallingCode() {
        self.dismiss(animated: true, completion: nil)
    }
    func didGetDiallingCode(_ diallingCode: String!, forCountry countryCode: String!) {
        
        print(countryCode)
        print(diallingCode)
        
        print("+\(diallingCode!)")
        
        txtCountryCode.text = "+\(diallingCode!)"
        //        btnCountryCode.setTitle("+\(diallingCode!)", for: .normal)
      //  serverCountryCode = diallingCode as String!
        
   
        USERDEFAULT.setValue(txtCountryCode.text, forKey: "countryCode")
        USERDEFAULT.synchronize()
        
        imageViewArrow.isHidden = false
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func didGetCountries(_ countries: [Any]!, forDiallingCode diallingCode: String!) {
        print(diallingCode)
        
    }

    
    
    // TODO: - ACTION METHODS
    @IBAction func btnBackClicked(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnUpdateClicked(_ sender:UIButton){
        self.view.endEditing(true)
      //  scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
        if Utility.isEmpty(txtFullname.text){
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter fullname", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if !(Utility.isEmpty(txtEmailid.text)) && isValidEmail(testStr: txtEmailid.text!) == false{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter emailId in valid format", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if Utility.isEmpty(txtCountryCode.text){
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please select countrycode", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if Utility.isEmpty(txtMobile.text){
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter mobilenumber", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else{
            if Reachability.isConnectedToNetwork() == true {
                SVProgressHUD.show(withStatus: "Loading..")
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForUpdateProfile), userInfo: nil, repeats: false)
            } else {
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }

    }
    
    @IBAction func btnCountryCodeCliked(_ sender:UIButton){
        
        viewCountryPicker.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.view.addSubview(viewCountryPicker)
        
//        let countryPicker = EMCCountryPickerController.init()
//        countryPicker.countryDelegate = self
//        self.present(countryPicker, animated: true, completion: nil)
    }
    
    
    @IBAction func btnDatePickerSelectedClicked(_ sender:UIButton){
        self.view.endEditing(true)
        
                if txtBirthdate.text != "" {
                    let str = txtBirthdate.text!
                    if str != "0000-00-00"{
                        let dateFormater = DateFormatter()
                        dateFormater.dateFormat = "yyyy-MM-dd"
                        var dateFromString = Date.init()
                        dateFromString = dateFormater.date(from: str as String)!
                        datePicker.setDate(dateFromString as Date, animated: true)
                    }
                }
        
        datePicker.maximumDate = NSDate() as Date
        datePicker.datePickerMode = .date;
        
        
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewDatePicker.frame=CGRect(x: self.view.frame.size.width - self.viewDatePicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y - self.viewDatePicker.frame.size.height, width: self.viewDatePicker.frame.size.width, height: self.viewDatePicker.frame.size.height)
            
            
        })
        
        
    }
    
    
    
    
    
    @IBAction func cancelPicker(sender:AnyObject){
        UIView.animate(withDuration: 0.3) {
            self.viewDatePicker.frame=CGRect(x: self.view.frame.size.width - self.viewDatePicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewDatePicker.frame.size.width, height: self.viewDatePicker.frame.size.height)
        }
    }
    @IBAction func doneDatePicker(sender:UIButton){
        
        txtBirthdate.text = getStringDateFromDate(dateFormat: "yyyy-MM-dd", enterDate: datePicker.date)
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewDatePicker.frame=CGRect(x: self.view.frame.size.width - self.viewDatePicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewDatePicker.frame.size.width, height: self.viewDatePicker.frame.size.height)
            
        })
    }
    
    
    // MARK: - POST DATA METHODS
    func postDataOnWebserviceForGetProfile(){
        let completeURL = NSString(format:"%@%@", MainURL,getUserProfileURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetProfile API Parameter :",finalParams)
        print("GetProfile API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getUserProfileURLTag)
    }
    
    func postDataOnWebserviceForUpdateProfile(){
        let completeURL = NSString(format:"%@%@", MainURL,editUserProfileURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "fullname":txtFullname.text!,
            "phone":txtMobile.text!,
            "ccode":txtCountryCode.text!,
            "dob":txtBirthdate.text!
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("UpdateProfile API Parameter :",finalParams)
        print("UpdateProfile API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: editUserProfileURLTag)
    }

    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case getUserProfileURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("GETProfile Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                let dataDictionary = resultDict.value(forKey: "data") as! NSDictionary
                
                USERDEFAULT.setValue(dataDictionary.value(forKey: "email") as! String, forKey: "emailID")
                USERDEFAULT.synchronize()
                
                USERDEFAULT.setValue(dataDictionary.value(forKey: "fullname") as! String, forKey: "fullName")
                USERDEFAULT.synchronize()
                
                USERDEFAULT.setValue(dataDictionary.value(forKey: "is_member") as! String, forKey: "isMember")
                USERDEFAULT.synchronize()
                
                
                USERDEFAULT.setValue(dataDictionary.value(forKey: "dob") as! String, forKey: "dob")
                USERDEFAULT.synchronize()
                
                let profileData:NSMutableDictionary = NSMutableDictionary(dictionary: resultDict.value(forKey: "data") as! NSDictionary)
                self.setProfileData(profileData: profileData)
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }

            
            SVProgressHUD.dismiss()
            break
            
        case editUserProfileURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("UpdateProfile Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                let dataDictionary = resultDict.value(forKey: "data") as! NSDictionary
                
                USERDEFAULT.setValue(dataDictionary.value(forKey: "email") as! String, forKey: "emailID")
                USERDEFAULT.synchronize()
                
                USERDEFAULT.setValue(dataDictionary.value(forKey: "fullname") as! String, forKey: "fullName")
                USERDEFAULT.synchronize()
                
                USERDEFAULT.setValue(dataDictionary.value(forKey: "is_member") as! String, forKey: "isMember")
                USERDEFAULT.synchronize()
                
                
                if let fullname = (resultDict.value(forKey: "data") as! NSDictionary).value(forKey: "fullname") as? String{
                    let name = "\(fullname)"
                    USERDEFAULT.set(name, forKey: "fullName")
                    USERDEFAULT.synchronize()
                    let notificationName = Notification.Name("updateProfileDataNotification")
                    NotificationCenter.default.post(name: notificationName, object: nil)
                    self.navigationController?.popViewController(animated: true)
                }
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }

            
            SVProgressHUD.dismiss()
            break

            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case getUserProfileURLTag:
            SVProgressHUD.dismiss()
            break
            
        case editUserProfileURLTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    
    
}
