//
//  RegisterByPassVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 01/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD

class RegisterByPassVC: UIViewController,EMCCountryDelegate,DiallingCodeDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var txtFullname:UITextField!
    @IBOutlet var txtEmailid:UITextField!
    @IBOutlet var txtMobile:UITextField!
    @IBOutlet var txtPassword:UITextField!
    @IBOutlet var txtRePassword:UITextField!
    @IBOutlet var txtCountryCode:UITextField!
    
    @IBOutlet var scrMain:UIScrollView!
    
    
    var serverCountryCode:String!
    @IBOutlet var imageViewArrow:UIImageView!
    
    @IBOutlet var lblFullname:UILabel!
    @IBOutlet var lblEmailID:UILabel!
    @IBOutlet var lblMobile:UILabel!
    @IBOutlet var lblPassword:UILabel!
    @IBOutlet var lblRePassword:UILabel!
    @IBOutlet var lblSignUp:UILabel!
    
    @IBOutlet var viewCountryPicker: UIView!
    @IBOutlet weak var tableCountryCode: UITableView!
    @IBOutlet weak var btnCloseCountryView: UIButton!
    
    //MARK:-
    var arrayCountryCode : NSArray = [["code":"852","country":"Hong Kong"],["code":"65","country":"Singapore"],["code":"60","country":"Malaysia"],
                                      ["code":"66","country":"Thailand"],["code":"850","country":"Korea"],["code":"81","country":"Japan"],
                                      ["code":"853","country":"Macau"],["code":"86","country":"China"],["code":"91","country":"India"],
                                      ["code":"1","country":"USA"],["code":"1","country":"Canada"],["code":"61","country":"Australia"]]
    var isFromWelcomeScreen:Bool?
    
    
    // FIXME: - VIEW CONTROLLER METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        imageViewArrow.isHidden = false
        
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
        if ScreenSize.SCREEN_HEIGHT < 568 {
            scrMain.isScrollEnabled = true
        }
        
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
        
        if let countryCode = USERDEFAULT.value(forKey: "countryCode") as? String{
            txtCountryCode.text = "\(countryCode)"
            //imageViewArrow.isHidden = true
        }
        else{
            //imageViewArrow.isHidden = false
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        serverCountryCode = ""
        self.setLocalizationText()
        self.addTapGestureInOurView()
        self.NavBarNumberPad()
    }
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        if viewTouched!.isKind(of: UIButton.self){
            
        }
        else{
            self.view.endEditing(true)
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        }
    }
    
    
    // Numeric Keyboard Hide Method
    func NavBarNumberPad(){
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        numberToolbar.barStyle = .default
        
        let viewBack = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        viewBack.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        numberToolbar.addSubview(viewBack)
        
        let btnCancel = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelNumberPad))
        btnCancel.tintColor = UIColor.white
        
        let btnDone = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.doneWithNumberPad))
        btnDone.tintColor = UIColor.white
        
        numberToolbar.items = NSArray(objects: btnCancel,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),btnDone) as? [UIBarButtonItem]
        
        
        numberToolbar.sizeToFit()
        txtMobile.inputAccessoryView = numberToolbar
    }
    
    func cancelNumberPad(){
        txtMobile.resignFirstResponder()
    }
    
    func doneWithNumberPad(){
        txtMobile.resignFirstResponder()
    }
    @IBAction func btnCloseCountryViewClicked(_ sender: UIButton) {
        viewCountryPicker.removeFromSuperview()
    }
    
    func setLocalizationText()
    {
        
        lblEmailID.text = NSLocalizedString("emailid", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblPassword.text = NSLocalizedString("password", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblFullname.text = NSLocalizedString("fullname", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblRePassword.text = NSLocalizedString("retypepassword", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblMobile.text = NSLocalizedString("mobile", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblSignUp.text = NSLocalizedString("signup", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
    }
    
    //MARK:- Table Delegate Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCountryCode.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cellIdentifier")
        cell.selectionStyle = .none
        
        
        let str = String(format: "+%@  %@", (arrayCountryCode[indexPath.row]as! NSDictionary).value(forKey: "code")as? String ?? "", (arrayCountryCode[indexPath.row]as! NSDictionary).value(forKey: "country")as? String ?? "")
        
        cell.textLabel?.text = str
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        txtCountryCode.text = String(format: "+%@", (arrayCountryCode[indexPath.row]as! NSDictionary).value(forKey: "code")as? String ?? "")
        self.btnCloseCountryViewClicked(btnCloseCountryView)
    }
    
    
    // TODO: - DELEGATE METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
            
        }
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var YOffset:Int = 0
        
        if (DeviceType.IS_IPHONE_5){
            if (textField.tag==4) {
                YOffset=0
            }else if (textField.tag==5){
                YOffset=0
            }else if (textField.tag==6){
                YOffset=0
            }else if (textField.tag==7){
                YOffset=3
            }else if (textField.tag==8){
                YOffset=8
            }
        }
        else if (DeviceType.IS_IPHONE_4_OR_LESS) {
            
            if (textField.tag==4) {
                YOffset=0
            }else if (textField.tag==5){
                YOffset=10
            }else if (textField.tag==6){
                YOffset=15
            }else if (textField.tag==7){
                YOffset=20
            }else if (textField.tag==8){
                YOffset=25
            }
        }
        scrMain.setContentOffset(CGPoint(x: 0, y: CGFloat(textField.tag * YOffset)), animated: true)
        return true
        
    }
    
    
    
    
    //MARK: - EMCCountryPickerController Delegates Methods
    
    func countryController(_ sender: Any!, didSelect chosenCountry: EMCCountry?) {
        // Do something with chosenCountry
        //   print(chosenCountry.countryName)
        print(chosenCountry?.countryCode ?? "Blank")
        print(chosenCountry?.countryName() ?? "Blank")
        let diallingCode:DiallingCode = DiallingCode.init(delegate: self)
        //  diallingCode.getCountriesWithDiallingCode(chosenCountry.countryCode)
        diallingCode.getForCountry(chosenCountry?.countryCode)
    }
    
    //MARK: - DiallingCode Delegates Methods
    func failedToGetDiallingCode() {
        self.dismiss(animated: true, completion: nil)
    }
    func didGetDiallingCode(_ diallingCode: String!, forCountry countryCode: String!) {
        
        print(countryCode)
        print(diallingCode)
        
        print("+\(diallingCode!)")
        
        txtCountryCode.text = "+\(diallingCode!)"
        //        btnCountryCode.setTitle("+\(diallingCode!)", for: .normal)
        serverCountryCode = diallingCode as String!
        
      
        USERDEFAULT.setValue(txtCountryCode.text, forKey: "countryCode")
        USERDEFAULT.synchronize()

        imageViewArrow.isHidden = true
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func didGetCountries(_ countries: [Any]!, forDiallingCode diallingCode: String!) {
        print(diallingCode)
        
    }
    
    
    // TODO: - ACTION METHODS
    
    @IBAction func btnRegistrationClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
        if Utility.isEmpty(txtFullname.text){
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter fullname", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if !(Utility.isEmpty(txtEmailid.text)) && isValidEmail(testStr: txtEmailid.text!) == false{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter emailId in valid format", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if Utility.isEmpty(txtCountryCode.text){
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please select countrycode", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if Utility.isEmpty(txtMobile.text){
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter mobilenumber", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }else if !(txtMobile.text!.count == 9) && !(txtMobile.text!.count == 10) {
            txtMobile.becomeFirstResponder()
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Phone number minimum length should be 10 characters.", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if Utility.isEmpty(txtPassword.text){
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter password", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if Utility.isEmpty(txtRePassword.text){
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter confirmpassword", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if txtPassword.text != txtRePassword.text{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Password doesn't match", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else{
            if Reachability.isConnectedToNetwork() == true {
                SVProgressHUD.show(withStatus: "Loading..")
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForRegistration), userInfo: nil, repeats: false)
            } else {
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }
    }
    @IBAction func btnBackClicked(_ sender:UIButton){
        _ =  self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCountryCodeCliked(_ sender:UIButton){
        self.view.endEditing(true)
        
        viewCountryPicker.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.view.addSubview(viewCountryPicker)
        
//        let countryPicker = EMCCountryPickerController.init()
//        countryPicker.countryDelegate = self
//        self.present(countryPicker, animated: true, completion: nil)
    }
    
    @IBAction func btnLoginClicked(_ sender:UIButton){
        let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
        loginVC.byPassScreenName = "welcomeScreen"
        self.navigationController?.pushViewController(loginVC, animated: true)

    }

    
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForRegistration(){
        let completeURL = NSString(format:"%@%@", MainURL,registrationURL) as String
        
        let params:NSDictionary = [
            "fullname" : txtFullname.text!,
            "email" : txtEmailid.text!,
            "phone":txtMobile.text!,
            "ccode":txtCountryCode.text!,
            "password":txtPassword.text!,
            "device_token":APPDELEGATE.deviceTokenForPushNotification ?? "",
            "device_type":"1",
            "lang_type":Language_Type,
            ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("Registration API Parameter :",finalParams)
        print("Registration API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: registrationURLTag)
        
    }
    
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case registrationURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("Registration Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{

                let login = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(login, animated: true)
                
                
//            let dataDictionary = resultDict.value(forKey: "data") as! NSDictionary
//            USERDEFAULT.setValue(dataDictionary.value(forKey: "user_id") as! String, forKey: "userID")
//            USERDEFAULT.synchronize()
//
//            USERDEFAULT.setValue(dataDictionary.value(forKey: "token") as! String, forKey: "token")
//            USERDEFAULT.synchronize()
//
//            USERDEFAULT.setValue(dataDictionary.value(forKey: "email") as! String, forKey: "emailID")
//            USERDEFAULT.synchronize()
//
//            USERDEFAULT.setValue(dataDictionary.value(forKey: "fullname") as! String, forKey: "fullName")
//            USERDEFAULT.synchronize()
                
//                if isFromWelcomeScreen == true{
//                    let joinMemberVC = JoinMemberVC(nibName: "JoinMemberVC", bundle: nil)
//                    self.navigationController?.pushViewController(joinMemberVC, animated: true)
//                }
//                else{
//                    let  homeVC = TabBarVC(nibName: "TabBarVC", bundle: nil)
//                    homeVC.selectedIndexOfMyTabbarController = 0
//                    self.navigationController?.pushViewController(homeVC, animated: true)
//                }
                
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                let dataDictionary = resultDict.value(forKey: "data") as! NSDictionary

                //showAlert(Appname, title: resultDict.value(forKey: "message") as! String)
                let varificationVC = VerificationVC(nibName: "VerificationVC", bundle: nil)
//                varificationVC.emailID = txtEmailid.text!
                
                varificationVC.emailID = dataDictionary.value(forKey: "phone") as? String
                varificationVC.ccCode = (dataDictionary.value(forKey: "ccode") as! String)

                varificationVC.isFrom = "Register"
                if isFromWelcomeScreen == true{
                    varificationVC.byPassScreenName = "welcomeScreen"
                }
                else{
                    varificationVC.byPassScreenName = "healthCategoryScreen"
                }
                
                self.navigationController?.pushViewController(varificationVC, animated: true)
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case registrationURLTag:
            SVProgressHUD.dismiss()
            break
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
}
