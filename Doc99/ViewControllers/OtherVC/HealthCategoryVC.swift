//
//  HealthCategoryVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 01/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SDWebImage

class HealthCategoryVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate {
    
    @IBOutlet var tableViewHealthCategory:UITableView!
    
    
    //For Pasignation
    var pageNum:Int!
    var isLoading:Bool?
    
    var healthData = NSMutableArray()
    var healthGlobalData = NSMutableArray()
    
    var boolArray:NSMutableArray!
    
    var healthCategory:NSMutableArray!
    
    
    var refreshControl: UIRefreshControl!
    
    @IBOutlet var lblHeader:UILabel!
    @IBOutlet var lblNext:UILabel!


    
    // FIXME: - VIEW CONTROLLER METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.healthData = NSMutableArray()
        self.healthGlobalData = NSMutableArray()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetHealthConcerns), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        
        
        
        self.setLocalizationText()
        self.addPullRefreshForHealthConcern()
        
    }
    
    func addPullRefreshForHealthConcern(){
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.gray
        refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: .valueChanged)
        tableViewHealthCategory.addSubview(refreshControl)
    }
    
    func refresh(sender:AnyObject) {
        // Code to refresh table view
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.healthData = NSMutableArray()
        self.healthGlobalData = NSMutableArray()
        self.tableViewHealthCategory.reloadData()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetHealthConcerns), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        
        
        refreshControl.endRefreshing()
    }
    

    
    
    func addLoadingIndicatiorOnFooterOnTableView(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableViewHealthCategory.tableFooterView = spinner
    }
    func removeLoadingIndicatiorOnFooterOnTableView(){
        tableViewHealthCategory.tableFooterView = nil
    }
    
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }
    
        func setLocalizationText(){
    
            lblHeader.text = NSLocalizedString("Select maximum 3 categories of health concerns", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
            lblNext.text = NSLocalizedString("NEXT", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
            
        }
    
    
    // TODO: - DELEGATE ScrollView
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == tableViewHealthCategory {
            if isLoading == true{
                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                    pageNum = pageNum + 1
                    print(pageNum)
                    isLoading = false
                    
                    if Reachability.isConnectedToNetwork() == true {
                        self.addLoadingIndicatiorOnFooterOnTableView()
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetHealthConcerns), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                    
                }
            }
            
        }
        
        
    }
    
    
    // TODO: - DELEGATE METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.healthData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "healthCategory"
        var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? HealthCategoryTableVC
        
        if cell == nil {
            let nib  = Bundle.main.loadNibNamed("HealthCategoryTableVC", owner: self, options: nil)
            cell = nib?[0] as? HealthCategoryTableVC
        }
        cell!.selectionStyle = .none;
        
        
        if let articalCategoryName =  (self.healthData[indexPath.row] as AnyObject).value(forKey: "hc_name") as? String
        {
            cell?.lblTitle.text = "\(articalCategoryName)"
        }
        
        if let articalCategoryName =  (self.healthData[indexPath.row] as AnyObject).value(forKey: "bodyorgan") as? String
        {
            cell?.lblSubTitle.text = "\(articalCategoryName)"
        }
        
        
        if boolArray.object(at: indexPath.row) as! String == "1"{
            cell?.imageViewSelect.image = UIImage.init(named: "check_blue.png")
        }
        else{
            cell?.imageViewSelect.image = UIImage.init(named: "check_gray.png")
        }
        
        
        if ((self.healthData.object(at: indexPath.row) as AnyObject).value(forKey: "image") as? String == "")
        {
            cell?.imageViewHealthCategory.image = UIImage.init(named: "image_placeholder.png")
            cell?.activityIndicatorForHealth.isHidden = true
            
        }
        else{
            let imageUrl = (self.healthData.object(at: indexPath.row) as AnyObject).value(forKey: "image") as? String
            
            let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl!) as String
            let url : NSString = fullUrl as NSString
            let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
            let searchURL : NSURL = NSURL(string: urlStr as String)!
            
            cell?.activityIndicatorForHealth.isHidden = false
            cell?.activityIndicatorForHealth.startAnimating()
            
            
            cell?.imageViewHealthCategory.sd_setImage(with: searchURL as URL, completed: { (image:UIImage?, error:Error?, cacheType:SDImageCacheType!, imageURL:URL?) in
                
                if ((error) != nil) {
                    cell?.imageViewHealthCategory.image = UIImage.init(named: "image_placeholder.png")
                }
                cell?.activityIndicatorForHealth.isHidden = true
            })
            
        }

        
        
        cell?.btnSelect.tag = indexPath.row
        cell?.btnSelect.addTarget(self, action: #selector(self.btnCategorySelectionClicked(_:)), for: .touchUpInside)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    
    
    // TODO: - ACTION METHODS
    
    @IBAction func btnCategorySelectionClicked(_ sender:UIButton){
        
        
        if boolArray.object(at: sender.tag) as! String == "0"{
            boolArray.replaceObject(at: sender.tag, with: "1")
        }
        else{
            boolArray.replaceObject(at: sender.tag, with: "0")
        }
        
        
        let indexPath = NSIndexPath(row: sender.tag, section: 0)
        tableViewHealthCategory.reloadRows(at: [indexPath as IndexPath], with: .none)
        
        healthCategory = NSMutableArray.init()
        for i in 0...healthData.count - 1 {
            if boolArray.object(at: i) as! String == "1"{
                healthCategory.add((self.healthData[i] as AnyObject).value(forKey: "hc_id") as! String)
            }
        }
        
    }
    
    
    
    @IBAction func btnNextClicked(_ sender:UIButton){
        //        let registrationVC = RegisterVC(nibName: "RegisterVC", bundle: nil)
        //        registrationVC.isFromHealthCategory = true
        //        self.navigationController?.pushViewController(registrationVC, animated: true)
        
        if healthCategory.count > 0{
            if healthCategory.count > 2{
                let registrationVC = RegisterByPassVC(nibName: "RegisterByPassVC", bundle: nil)
                registrationVC.isFromWelcomeScreen = false
                self.navigationController?.pushViewController(registrationVC, animated: true)
            }
            else{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString("Select maximum 3 categories of health concerns", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }
        else{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please select health concerns", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        
        
        
        
        
    }
    @IBAction func btnBackClicked(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForGetHealthConcerns(){
        
        let completeURL = NSString(format:"%@%@", MainURL,healthConcernURL) as String
        
         let pageNumber = "\(pageNum!)"
        // "page":pageNumber,
        // "limit":PAGINATION_LIMITE
        
        let params:NSDictionary = [
           // "user_id" : "49",
           // "token": "ICp48GEVMOBwzHOUu1KJN2xJ2PxsmCSa1jDfTGpERXP6s7pbFH2ZpPvg4PpuJSWX5uCYy701DfKNHHeacneTpCMxU3C0JEvlT5CFjVqbv63FXjlOUZoQ6jsW",
             "page":pageNumber,
             "limit":PAGINATION_LIMITE,
            "lang_type":Language_Type
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetArticalCategory API Parameter :",finalParams)
        print("GetArticalCategory API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: healthConcernURLTag)
        
        
        
    }
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        
        switch tag {
        case healthConcernURLTag:
            let resultDict = responseObject as! NSDictionary;
//            print("getDrugDiseaseURLTag List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum == 1{
                    self.healthData = NSMutableArray()
                    self.healthGlobalData = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        self.healthGlobalData.add(myData[i])
                    }
                    
                    for i in 0...myData.count - 1 {
                        self.healthData.add(myData[i])
                    }
                    
                    boolArray = NSMutableArray.init()
                    
                    for _ in 0...healthData.count - 1 {
                        boolArray.add("0")
                    }
                    
                    
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum > 0) {
                            self.pageNum = self.pageNum - 1
                        }
                        self.isLoading = false
                    }else{
                        self.isLoading = true
                    }
                }
                else{
                    self.isLoading = false
                    if (self.pageNum > 0) {
                        self.pageNum = self.pageNum - 1
                    }
                    
                    if self.healthData.count == 0{
                        self.tableViewHealthCategory.isHidden = true
                        //self.lblNoArtical.isHidden = false
                    }
                    else{
                        self.tableViewHealthCategory.isHidden = false
                        //self.lblNoArtical.isHidden = true
                    }
                }
                self.tableViewHealthCategory.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case healthConcernURLTag:
            self.isLoading = false
            if (self.pageNum > 0) {
                self.pageNum = self.pageNum - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    
}
