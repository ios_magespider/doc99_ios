//
//  JoinVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 01/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class JoinVC: UIViewController {

    
    @IBOutlet var lblTitle1:UILabel!
    @IBOutlet var lblTitle2:UILabel!
    @IBOutlet var lblTitle3:UILabel!
    
    @IBOutlet var btnTitle1:UIButton!
    @IBOutlet var btnTitle2:UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    

    // FIXME: - VIEW CONTROLLER METHODS

    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        self.setLocalizationText()
    }
    
    func setLocalizationText()
    {
        lblTitle1.text = NSLocalizedString("Enjoy drug search with Doc-99?", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitle2.text = NSLocalizedString("Join our FISH HEAD health concern groups for updated drug search to refill for gratitude program", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitle3.text = NSLocalizedString("JOIN NOW", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
        btnTitle1.setTitle(NSLocalizedString("Skip & goto blog article", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        btnTitle2.setTitle(NSLocalizedString("Let’s  Go", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
    }
    
    // TODO: - DELEGATE METHODS
    // TODO: - ACTION METHODS
    @IBAction func btnBackClicked(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onTapbtnLogin(_ sender: UIButton) {
        let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
        self.navigationController?.pushViewController(loginVC, animated: true)
        
        
    }
    @IBAction func btnJoinNowClicked(_ sender:UIButton){
//        let healthCategoryVC = HealthCategoryVC(nibName: "HealthCategoryVC", bundle: nil)
//        self.navigationController?.pushViewController(healthCategoryVC, animated: true)
        
        let registrationVC = RegisterByPassVC(nibName: "RegisterByPassVC", bundle: nil)
        registrationVC.isFromWelcomeScreen = true
        self.navigationController?.pushViewController(registrationVC, animated: true)

    }
    @IBAction func btnSkipClicked(_ sender:UIButton){
        let homeVC = TabBarVC(nibName: "TabBarVC", bundle: nil)
        homeVC.selectedIndexOfMyTabbarController = 1
        self.navigationController?.pushViewController(homeVC, animated: true)

    }
    @IBAction func btnLetsGoClicked(_ sender:UIButton){
       
    }
    // TODO: - POST DATA METHODS

    

}
