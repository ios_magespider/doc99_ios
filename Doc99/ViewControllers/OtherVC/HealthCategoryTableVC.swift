//
//  HealthCategoryTableVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 01/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class HealthCategoryTableVC: UITableViewCell {

    @IBOutlet var imageViewSelect:UIImageView!
    @IBOutlet var lblTitle:UILabel!
    @IBOutlet var lblSubTitle:UILabel!
    @IBOutlet var btnSelect:UIButton!
    @IBOutlet var imageViewHealthCategory:UIImageView!
    @IBOutlet var activityIndicatorForHealth:UIActivityIndicatorView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
