//
//  AddressListVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 01/02/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit

class AddressListVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate{
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var tblAddress: UITableView!
    @IBOutlet weak var viewBtnAdd: UIView!
    
    @IBOutlet weak var labelButtonTitle: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    
    
    var pageNum:Int!
    var isLoading:Bool?
    
    var isFromSetting:Bool?
    
    var arrayAddressMain = NSMutableArray()
    var arrayAddressBool = NSMutableArray()
    
    var addressID : String?
    
    var oldSelection : Int = 0
    var newSelection : Int = 0
    
    var isAddressSelected : Bool = false
    
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollViewBottom: NSLayoutConstraint!
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let notificationName1 = Notification.Name("refreshAddressListData")
        NotificationCenter.default.addObserver(self, selector: #selector(refreshAddressListData(notification:)), name: notificationName1, object: nil)
        
        if isFromSetting!{
            scrollViewBottom.constant = 0
//            labelButtonTitle.text = "ADD"
//            viewBtnAdd.isHidden = true
//            mainScrollView.frame = CGRect(x: mainScrollView.frame.origin.x, y: mainScrollView.frame.origin.y, width: mainScrollView.frame.size.width, height: mainScrollView.frame.size.height+52)
        }
        else{
            labelButtonTitle.text = "UPLOAD PRESCRIPTION"
            imgArrow.isHidden = false
            viewBtnAdd.isHidden = false
        }
        
        pageNum=1
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetAddressInfo), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK:- IBActions
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnAddNewClicked(_ sender: Any) {
        let addAddressVC = AddAddressVC(nibName: "AddAddressVC", bundle: nil)
        addAddressVC.editAddress = false
        self.navigationController?.pushViewController(addAddressVC, animated: true)
    }
    
    @IBAction func btnUploadPrescriptionClicked(_ sender: Any) {
        if isAddressSelected {
            /*
             APPDELEGATE.prescriptionData.setValue(txtFlatNumber.text!, forKey: "flatNo")
             APPDELEGATE.prescriptionData.setValue(txtStreetName.text!, forKey: "streetName")
             APPDELEGATE.prescriptionData.setValue(txtLandmark.text!, forKey: "landMark")
             APPDELEGATE.prescriptionData.setValue(txtArea.text!, forKey: "area")
             APPDELEGATE.prescriptionData.setValue(txtCity.text!, forKey: "city")
             APPDELEGATE.prescriptionData.setValue(txtPostalCode.text!, forKey: "postalCode")
             */
            
            let addressDict = ["flat_building" : (arrayAddressMain[newSelection] as? NSDictionary)?.value(forKey: "flat_building") as? String,
                               "area" : (arrayAddressMain[newSelection] as? NSDictionary)?.value(forKey: "area") as? String,
                               "landmark" : (arrayAddressMain[newSelection] as? NSDictionary)?.value(forKey: "landmark") as? String,
                               "name" : (arrayAddressMain[newSelection] as? NSDictionary)?.value(forKey: "name") as? String,
                               "phone" : (arrayAddressMain[newSelection] as? NSDictionary)?.value(forKey: "phone") as? String,
                               "postal_code" : (arrayAddressMain[newSelection] as? NSDictionary)?.value(forKey: "postal_code") as? String,
                               "street" : (arrayAddressMain[newSelection] as? NSDictionary)?.value(forKey: "street") as? String,
                               "ua_id" : (arrayAddressMain[newSelection] as? NSDictionary)?.value(forKey: "ua_id") as? String,
                               "user_id" : (arrayAddressMain[newSelection] as? NSDictionary)?.value(forKey: "user_id") as? String,]
            
            APPDELEGATE.prescriptionData.setValue(addressDict, forKey: "AddressDetail")
            print("APPDELEGATE array data :- \(APPDELEGATE.prescriptionData)")
            
            
            let uploadPre = UploadPrescriptionVC(nibName: "UploadPrescriptionVC", bundle: nil)
            self.navigationController?.pushViewController(uploadPre, animated: true)
        }
        else{
            showAlert(Appname, title: "Please select address.")
        }
    }
    
    
    //MARK:- Tableview Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayAddressMain .count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "addressTableCell"
        
        var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? AddressTableCustomCellVC
        
        if cell == nil {
            let nib  = Bundle.main.loadNibNamed("AddressTableCustomCellVC", owner: self, options: nil)
            cell = nib?[0] as? AddressTableCustomCellVC
        }
        cell!.selectionStyle = .none;
        cell!.labelName.text = (arrayAddressMain .object(at: indexPath.row) as! NSDictionary) .value(forKey: "name") as? String ?? ""
        cell!.labelAddressOne.text = (arrayAddressMain .object(at: indexPath.row) as! NSDictionary) .value(forKey: "flat_building") as? String ?? ""
        cell!.labelAddressTwo.text = ((arrayAddressMain .object(at: indexPath.row) as! NSDictionary) .value(forKey: "street") as? String ?? "")
        cell!.labelAddressThree.text = ((arrayAddressMain .object(at: indexPath.row) as! NSDictionary) .value(forKey: "area") as? String ?? "") + " \(((arrayAddressMain .object(at: indexPath.row) as! NSDictionary) .value(forKey: "landmark") as? String ?? ""))"
        cell!.labelPostalCode.text = ((arrayAddressMain .object(at: indexPath.row) as! NSDictionary) .value(forKey: "city") as? String ?? "") + " \(((arrayAddressMain .object(at: indexPath.row) as! NSDictionary) .value(forKey: "postal_code") as? String ?? ""))"
        cell!.labelPhone.text = (arrayAddressMain .object(at: indexPath.row) as! NSDictionary) .value(forKey: "phone") as? String ?? ""
        
        cell!.btnSelection.tag = indexPath.row
        
        if isFromSetting! {
            cell!.imgViewSelection.isHidden = false
            cell!.btnSelection.isHidden = false
            
            cell?.imgViewSelection.image = UIImage.init(named:"trash.png")
            cell!.btnSelection.addTarget(self, action: #selector(btnDeleteClicked(_:)), for: .touchUpInside)
            
            cell?.imgShare.isHidden = false
            cell?.btnShare.isHidden = false
            cell?.btnShare.tag = indexPath.row
            cell?.btnShare.addTarget(self, action: #selector(btnShareClicked(_:)), for: .touchUpInside)
            
        }
        else{
            
            cell?.imgShare.isHidden = true
            
            cell!.imgViewSelection.isHidden = false
            cell!.btnSelection.isHidden = false
            
            if arrayAddressBool[indexPath.row] as! String == "0"{
                cell?.imgViewSelection.image = UIImage.init(named:"check_gray.png")
            }
            else{
                cell?.imgViewSelection.image = UIImage.init(named:"check_blue.png")
            }
            
            cell!.btnSelection.addTarget(self, action: #selector(btnSelectClicked(_:)), for: .touchUpInside)
        }
        
        
        
        
        
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if isFromSetting! {
            let addAddressVC = AddAddressVC(nibName: "AddAddressVC", bundle: nil)
            addAddressVC.editAddress = true
            addAddressVC.editAddressDetail = arrayAddressMain[indexPath.row] as! NSDictionary
            self.navigationController?.pushViewController(addAddressVC, animated: true)
//        }
    }
    
    
    //MARK:- Cell button click methods
    @IBAction func btnDeleteClicked(_ sender: UIButton){
        print("Delete at \(sender.tag)")
        self.addressID = (self.arrayAddressMain[sender.tag] as AnyObject).value(forKey: "ua_id") as? String
        
        if Reachability.isConnectedToNetwork() == true {
            self.arrayAddressMain.removeObject(at: sender.tag)
//            self.arrayAddressMain.removeObject(at: sender.tag)
            self.tblAddress.reloadData()
            self.setScrollViewHeight()
            
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForDeleteAddressInfo), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
    }
    
    @IBAction func btnShareClicked(_ sender: UIButton){
        
        let ssImage = tblAddress.snapshotRows(at: [IndexPath(row: sender.tag, section: 0)])
        //tblAddress.reloadData()
        
        let  activityViewController = UIActivityViewController(activityItems: [self,ssImage as UIImage? as Any], applicationActivities: [])
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        tblAddress.reloadData()
    }
   
    @IBAction func btnSelectClicked(_ sender: UIButton){
        
        oldSelection = newSelection
        newSelection = sender.tag
        
        self.addressID = (self.arrayAddressMain[sender.tag] as AnyObject).value(forKey: "ua_id") as? String
        
        arrayAddressBool.replaceObject(at: oldSelection, with: "0")
        arrayAddressBool.replaceObject(at: newSelection, with: "1")
        
        if oldSelection == newSelection {
            tblAddress.reloadRows(at: [NSIndexPath(row: newSelection, section: 0) as IndexPath], with: .none)
        }
        else{
            tblAddress.reloadRows(at: [NSIndexPath(row: oldSelection, section: 0) as IndexPath, NSIndexPath(row: newSelection, section: 0) as IndexPath], with: .none)
        }
        
        isAddressSelected = true
    }
    
    
    //MARK:- Methods
    func refreshAddressListData(notification: NSNotification) {
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()

        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetAddressInfo), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
    }
    
    func addLoadingIndicatiorOnFooterOnTableView(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tblAddress.tableFooterView = spinner
    }
    func removeLoadingIndicatiorOnFooterOnTableView(){
        tblAddress.tableFooterView = nil
    }
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }
    
    // TODO: - ScrollView DELEGATE
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == mainScrollView || scrollView == tblAddress {
            if isLoading == true{
                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                    pageNum = pageNum + 1
                    print(pageNum)
                    isLoading = false
                    
                    if Reachability.isConnectedToNetwork() == true {
                        self.addLoadingIndicatiorOnFooterOnTableView()
                        self.setScrollViewHeight()
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetAddressInfo), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
            }
            
        }
    }
    
    func setScrollViewHeight(){
        
        tableHeight.constant = tblAddress.contentSize.height
        
        //tblAddress.frame = CGRect(x: tblAddress.frame.origin.x, y: tblAddress.frame.origin.y, width: tblAddress.frame.size.width, height: tblAddress.contentSize.height)
        
        mainScrollView.contentSize = CGSize(width: mainScrollView.frame.size.width, height: tblAddress.frame.origin.y+tblAddress.contentSize.height)
        
    }
    
    
    //MARK:- Post data Methods
    func postDataOnWebserviceForGetAddressInfo(){
        let completeURL = NSString(format:"%@%@", MainURL,getAddressListUrl) as String
        
        let pageNumber = "\(pageNum!)"
        
        let params = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "page":pageNumber,
            "limit":PAGINATION_LIMITE
            ] as [String : Any]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("Get Address URL :",completeURL)
        print("Get Address Request :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getAddressListUrlTag)
        
    }
    
    func postDataOnWebserviceForDeleteAddressInfo(){
        let completeURL = NSString(format:"%@%@", MainURL,deleteAddressUrl) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "ua_id":addressID!
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("Delete Address URL :",completeURL)
        print("Delete Address Request :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: deleteAddressUrlTag)
    }
    
    //MARK:- Web service Call back methods
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case getAddressListUrlTag:
            let resultDict = responseObject as! NSDictionary;
            print("GetPatientInfo List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                if self.pageNum == 1{
                    arrayAddressMain = NSMutableArray()
                    arrayAddressBool = NSMutableArray()
                }
                
                var myData = NSArray()
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    arrayAddressMain.addObjects(from: myData as! [Any])
                    
                    if isFromSetting == false{
                        for _ in 0...myData.count-1{
                            arrayAddressBool.add("0")
                        }
                    }
                }
                else{
                    self.isLoading = false
                    if (self.pageNum > 1) {
                        self.pageNum = self.pageNum - 1
                    }
                }
                
                
                if (myData.count < PAGINATION_LIMITE) {
                    if (self.pageNum > 0) {
                        self.pageNum = self.pageNum - 1
                    }
                    self.isLoading = false
                }else{
                    self.isLoading = true
                }
                
                
                
                if arrayAddressMain.count == 0 {
                    tblAddress.isHidden = true
                }
                else{
                    tblAddress.isHidden = false
                }
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            
            
            self.tblAddress .reloadData()
            self.removeLoadingIndicatiorOnFooterOnTableView()
            self.setScrollViewHeight()
            break
            
        case deleteAddressUrlTag:
            let resultDict = responseObject as! NSDictionary;
            print("Remove PatientInfo Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                
            }
            tblAddress.reloadData()
            self.setScrollViewHeight()
            
            break
            
        default: break
            
        }
    }
    
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case getAddressListUrlTag:
            self.isLoading = false
            if (self.pageNum > 0) {
                self.pageNum = self.pageNum - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
}



extension UITableView
{
    func snapshotRows(at indexPaths: Set<IndexPath>) -> UIImage?
    {
        guard !indexPaths.isEmpty else { return nil }
        var rect = self.rectForRow(at: indexPaths.first!)
        for indexPath in indexPaths
        {
            let cellRect = self.rectForRow(at: indexPath)
            rect = rect.union(cellRect)
        }
        
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        for indexPath in indexPaths
        {
            let cell = self.cellForRow(at: indexPath) as? AddressTableCustomCellVC
            cell?.imgShare.isHidden = true
            cell?.imgViewSelection.isHidden = true
            cell?.layer.bounds.origin.y = self.rectForRow(at: indexPath).origin.y - rect.minY
            cell?.layer.render(in: context)
            cell?.layer.bounds.origin.y = 0
        }
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
