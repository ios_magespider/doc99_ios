//
//  AddAddressVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 01/02/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class AddAddressVC: UIViewController, UITextFieldDelegate, UIAlertViewDelegate {

    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtFlatDetail: UITextField!
    @IBOutlet weak var txtStreet: UITextField!
    @IBOutlet weak var txtLandmark: UITextField!
    @IBOutlet weak var txtArea: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtPostalCode: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    
    @IBOutlet weak var scrViewAddress: UIScrollView!
    @IBOutlet weak var labelButtonTitle: UILabel!
    
    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint?
    
    @IBOutlet var currentTextField : UITextField!
    
    var tapRecognizer:UITapGestureRecognizer!
    
    var editAddress : Bool!
    
    var editAddressDetail : NSDictionary!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.NavBarNumberPad()
        self.addTapGestureInOurView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        if editAddress! {
            self.setAddressData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setAddressData(){
        txtName.text = editAddressDetail.value(forKey: "name") as? String
        txtFlatDetail.text = editAddressDetail.value(forKey: "flat_building") as? String
        txtStreet.text = editAddressDetail.value(forKey: "street") as? String
        txtLandmark.text = editAddressDetail.value(forKey: "landmark") as? String
        txtArea.text = editAddressDetail.value(forKey: "area") as? String
        txtCity.text = editAddressDetail.value(forKey: "city") as? String
        txtPostalCode.text = editAddressDetail.value(forKey: "postal_code") as? String
        txtPhone.text = editAddressDetail.value(forKey: "phone") as? String
        
        labelButtonTitle.text = "UPDATE"
    }
    
    //MARK:- IBActions
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddClicked(_ sender: Any) {
        if Utility.isEmpty(txtName.text){
            showAlert(Appname, title: "Please enter name.")
        }
        else if Utility.isEmpty(txtFlatDetail.text){
            showAlert(Appname, title: "Please enter flat detail.")
        }
        else if Utility.isEmpty(txtStreet.text){
            showAlert(Appname, title: "Please street name.")
        }
        else if Utility.isEmpty(txtArea.text){
            showAlert(Appname, title: "Please enter area.")
        }
        else if Utility.isEmpty(txtCity.text){
            showAlert(Appname, title: "Please enter city.")
        }
        else if Utility.isEmpty(txtPostalCode.text){
            showAlert(Appname, title: "Please enter postal code.")
        }
        else if Utility.isEmpty(txtPhone.text){
            showAlert(Appname, title: "Please enter phone number.")
        }
        else{
            if Reachability.isConnectedToNetwork() == true {
                SVProgressHUD.show(withStatus: "Loading..")
                if editAddress!{
                    Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForUpdateAddress), userInfo: nil, repeats: false)
                }
                else{
                    Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForAddAddress), userInfo: nil, repeats: false)
                }
            } else {
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }
    }
    
    //MARK:- Textfield Delegates
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        currentTextField = textField
        if currentTextField == txtCity {
            UIView.animate(withDuration: 0.3) {
                self.scrViewAddress.contentOffset = CGPoint(x: 0, y: self.txtName.frame.origin.y)
            }
        }
        else if currentTextField == txtPostalCode {
            UIView.animate(withDuration: 0.3) {
                self.scrViewAddress.contentOffset = CGPoint(x: 0, y: self.txtStreet.frame.origin.y)
            }
        }
        else if currentTextField == txtPhone {
            UIView.animate(withDuration: 0.3) {
                self.scrViewAddress.contentOffset = CGPoint(x: 0, y: self.txtLandmark.frame.origin.y)
            }
        }
        else{
            UIView.animate(withDuration: 0.3) {
                self.scrViewAddress.contentOffset = CGPoint(x: 0, y: 0)
            }
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        return false
    }
    
    
    //MARK:- Postdata mathods
    func postDataOnWebserviceForAddAddress() {
        let completeURL = NSString(format:"%@%@", MainURL,addAddressUrl) as String
        
        let landmark:String!
        
        if txtLandmark.text == ""{
            landmark = ""
        }
        else{
            landmark = txtLandmark.text
        }
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "name":txtName.text!,
            "flat_building":txtFlatDetail.text!,
            "street":txtStreet.text!,
            "landmark":landmark,
            "area":txtArea.text!,
            "city":txtCity.text!,
            "postal_code":txtPostalCode.text!,
            "phone":txtPhone.text!
        ]
        /*
         "user_id":"3",
         "token":"aQD0jXbe0qIcv4xBfdIBARnmOQEE8k6mYLk0s8hGnBJqCqiGo9MZKivYgZYDD9P39Lc05L1Ra4UVbXJ0R3VMFT4WlnOSDQmNMQXEVEwtcBJB43BGm5UQqaHu",
         "lang_type":"3",
         "name":"dharmesh",
         "flat_building":"24 a",
         "street":"10",
         "landmark":"surat",
         "area":"varachha",
         "city":"surat",
         "postal_code":"395006",
         
         */
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("Add Address API URL :",completeURL)
        print("Add Address API Request :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: addAddressUrlTag)
    }
    
    func postDataOnWebserviceForUpdateAddress(){
        let completeURL = NSString(format:"%@%@", MainURL,editAddressUrl) as String
        
        let landmark:String!
        
        if txtLandmark.text == ""{
            landmark = ""
        }
        else{
            landmark = txtLandmark.text
        }
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "ua_id":editAddressDetail.value(forKey: "ua_id") as! String ,
            "name":txtName.text!,
            "flat_building":txtFlatDetail.text!,
            "street":txtStreet.text!,
            "landmark":landmark,
            "area":txtArea.text!,
            "city":txtCity.text!,
            "postal_code":txtPostalCode.text!,
            "phone":txtPhone.text!
        ]
       
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("Add Address API URL :",completeURL)
        print("Add Address API Request :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: editAddressUrlTag)
    }
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case addAddressUrlTag:
            let resultDict = responseObject as! NSDictionary;
            print("AddPatientInfo Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                
                let notificationName = Notification.Name("refreshAddressListData")
                NotificationCenter.default.post(name: notificationName, object: nil)
                
                   showAlert(Appname, title: resultDict.value(forKey: "message") as! String)
                _ = self.navigationController?.popViewController(animated: true)
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            SVProgressHUD.dismiss()
            break
            
        case editAddressUrlTag:
            let resultDict = responseObject as! NSDictionary;
            print("AddPatientInfo Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                let notificationName = Notification.Name("refreshAddressListData")
                NotificationCenter.default.post(name: notificationName, object: nil)
                _ = self.navigationController?.popViewController(animated: true)
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""),
                                              title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            SVProgressHUD.dismiss()
            
            break
            
        default:
        break
            
        }
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case addAddressUrlTag:
            SVProgressHUD.dismiss()
            break

        case editAddressUrlTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    //MARK:- Keyboard Methods
    // Numeric Keyboard Hide Method
    func NavBarNumberPad(){
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        numberToolbar.barStyle = .default
        
        let viewBack = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        viewBack.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        numberToolbar.addSubview(viewBack)
        
        let btnCancel = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelNumberPad))
        btnCancel.tintColor = UIColor.white
        
        let btnDone = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.doneWithNumberPad))
        btnDone.tintColor = UIColor.white
        
        numberToolbar.items = NSArray(objects: btnCancel,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),btnDone) as? [UIBarButtonItem]
        
        numberToolbar.sizeToFit()
        txtPostalCode.inputAccessoryView = numberToolbar
        txtPhone.inputAccessoryView = numberToolbar
    }
    
    func keyboardWillShow(notification:NSNotification){
        self.view.addGestureRecognizer(tapRecognizer)
    }
    func keyboardWillHide(notification:NSNotification){
        self.view.removeGestureRecognizer(tapRecognizer)
    }
    
    func cancelNumberPad(){
        if currentTextField == txtPostalCode {
            txtPostalCode .resignFirstResponder()
        }
        else if currentTextField == txtPhone {
            txtPhone .resignFirstResponder()
        }
        UIView.animate(withDuration: 0.3) {
            self.scrViewAddress.contentOffset = CGPoint(x: 0, y: 0)
        }
    }
    
    func doneWithNumberPad(){
        if currentTextField == txtPostalCode {
            txtPostalCode .resignFirstResponder()
        }
        else if currentTextField == txtPhone {
            txtPhone .resignFirstResponder()
        }
        UIView.animate(withDuration: 0.3) {
            self.scrViewAddress.contentOffset = CGPoint(x: 0, y: 0)
        }
    }
    
    
    //MARK:- Other
    
    func addTapGestureInOurView(){
        tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        if viewTouched!.isKind(of: UIButton.self) && viewTouched!.isKind(of:UITextView.self){
        }
        else{
            self.view.endEditing(true)
            UIView.animate(withDuration: 0.3) {
              self.scrViewAddress.contentOffset = CGPoint(x: 0, y: 0)
            }
        }
    }
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        
        
        
        
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                break
                
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }
}
