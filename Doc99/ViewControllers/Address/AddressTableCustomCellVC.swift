//
//  AddressTableCustomCellVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 01/02/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit

class AddressTableCustomCellVC: UITableViewCell {
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelAddressOne: UILabel!
    @IBOutlet weak var labelAddressTwo: UILabel!
    @IBOutlet weak var labelAddressThree: UILabel!
    @IBOutlet weak var labelPostalCode: UILabel!
    @IBOutlet weak var labelPhone: UILabel!
    @IBOutlet weak var imgViewSelection: UIImageView!
    @IBOutlet weak var btnSelection: UIButton!
    
    @IBOutlet weak var imgShare: UIImageView!
    @IBOutlet weak var btnShare: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
