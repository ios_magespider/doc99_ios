//
//  MAPTermsVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 31/01/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class MAPTermsVC: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var labelAgree: UILabel!
    @IBOutlet weak var webViewPrescriptionTC: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //labelAgree.text = "AGREE TO CONTINUE"
        
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetPrescriptionTermCondition), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBActions
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func btnCorrectClicked(_ sender: Any) {
        print("Correct clicked..")
    }
    
    @IBAction func btnAgreeClicked(_ sender: Any) {
        print("Agree clicked..")
        
        let orderTypevc = OrderHandlingSelectionVC.init(nibName: "OrderHandlingSelectionVC", bundle: nil)
        self.navigationController?.pushViewController(orderTypevc, animated: true)
        
    }
    
    //MARK:- Webview Delegate Methods
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("WebView Error : ",error.localizedDescription)
    }
    
    // MARK: - POST DATA METHODS
    func postDataOnWebserviceForGetPrescriptionTermCondition(){
        
        let completeURL = NSString(format:"%@%@", MainURL,cmsWebPageCallURL) as String
        let params:NSDictionary = [
            "page" : "termsforeprescription"
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("TermCondition API Parameter :",finalParams)
        print("TermCondition API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: cmsWebPageCallURLTag)
    }
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case cmsWebPageCallURLTag:
            let resultDict = responseObject as! NSDictionary;
            //print("TermCondition Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                let myDescription = resultDict.value(forKey: "page_description") as! String
                let abc = myDescription.replacingOccurrences(of: "\n", with: "<br/>")
                webViewPrescriptionTC.loadHTMLString(abc, baseURL: nil)
            }
            break
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case cmsWebPageCallURLTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
}
