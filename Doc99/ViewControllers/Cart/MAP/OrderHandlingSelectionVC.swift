//
//  OrderHandlingSelectionVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 31/01/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit

class OrderHandlingSelectionVC: UIViewController {
    @IBOutlet weak var imgPickupOrder: UIImageView!
    @IBOutlet weak var imgCourierOrder: UIImageView!
    
    var selectedOrderType : NSString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK:- IBActions
    @IBAction func btnBackClicked(_ sender: Any) {
//        APPDELEGATE.myTabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPickupOrderClicked(_ sender: Any) {
        imgPickupOrder.image = UIImage.init(named: "check_blue.png")
        imgCourierOrder.image = UIImage.init(named: "check_gray.png")
        selectedOrderType = "pickup"
    }
    
    @IBAction func btnCourierOrderClicked(_ sender: Any) {
        imgPickupOrder.image = UIImage.init(named: "check_gray.png")
        imgCourierOrder.image = UIImage.init(named: "check_blue.png")
        selectedOrderType = "courier"
    }
    
    @IBAction func btnNextClicked(_ sender: Any) {
        if selectedOrderType == "pickup" {
            // go for patient selection
            let patientInfoVC = PatientInfoVC(nibName: "PatientInfoVC", bundle: nil)
            patientInfoVC.isFromSettingPage = false
            self.navigationController?.pushViewController(patientInfoVC, animated: true)
        }
        else if selectedOrderType == "courier" {
            // go for address selection and then patient selection
            let addressInfoVC = AddressListVC(nibName: "AddressListVC", bundle: nil)
            addressInfoVC.isFromSetting = false
            self.navigationController?.pushViewController(addressInfoVC, animated: true)
        }
        else{
            showAlert(Appname, title: "Please select order type.")
        }
    }
    
}
