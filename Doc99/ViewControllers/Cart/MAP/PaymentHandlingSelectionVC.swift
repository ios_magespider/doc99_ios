//
//  PaymentHandlingSelectionVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 31/01/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit

class PaymentHandlingSelectionVC: UIViewController {

    @IBOutlet weak var viewOffline: UIView!
    @IBOutlet weak var imgOffline: UIImageView!
    @IBOutlet weak var viewOfflineDetail: UIView!
    
    
    @IBOutlet weak var viewOnline: UIView!
    @IBOutlet weak var imgOnline: UIImageView!
    @IBOutlet weak var viewOnlineDetail: UIView!
    
    var isOffline : Bool?
    
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBActions
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnOfflineClicked(_ sender: Any) {
        isOffline = true
        imgOffline.image = UIImage.init(named:"check_blue.png")
        imgOnline.image = UIImage.init(named:"check_gray.png")
        
        UIView .animate(withDuration: 0.2, animations: {
            self.viewOfflineDetail.frame = CGRect(x: self.viewOfflineDetail.frame.origin.x, y: self.viewOfflineDetail.frame.origin.y, width: self.viewOfflineDetail.frame.size.width, height: 135)
            
            
            self.viewOnline.frame = CGRect(x: self.viewOnline.frame.origin.x, y: self.viewOfflineDetail.frame.origin.y+self.viewOfflineDetail.frame.size.height+8, width: self.viewOnline.frame.size.width, height: self.viewOnline.frame.size.height)
            
            self.viewOnlineDetail.frame = CGRect(x: self.viewOnlineDetail.frame.origin.x, y: self.viewOnline.frame.origin.y+self.viewOnline.frame.size.height, width: self.viewOnlineDetail.frame.size.width, height: 0)
            
            self.viewOfflineDetail.isHidden = false
            self.viewOnlineDetail.isHidden = true
            
        })
    }
    
    @IBAction func btnOnlineClicked(_ sender: Any) {
        isOffline = false

        imgOnline.image = UIImage.init(named:"check_blue.png")
        imgOffline.image = UIImage.init(named:"check_gray.png")
        
        
        UIView.animate(withDuration: 0.2, animations: {
            self.viewOfflineDetail.frame = CGRect(x: self.viewOfflineDetail.frame.origin.x, y: self.viewOfflineDetail.frame.origin.y, width: self.viewOfflineDetail.frame.size.width, height: 0)
            
            self.viewOnline.frame = CGRect(x: self.viewOnline.frame.origin.x, y: self.viewOfflineDetail.frame.origin.y+self.viewOfflineDetail.frame.size.height+8, width: self.viewOnline.frame.size.width, height: self.viewOnline.frame.size.height)
            
            self.viewOnlineDetail.frame = CGRect(x: self.viewOnlineDetail.frame.origin.x, y: self.viewOnline.frame.origin.y+self.viewOnline.frame.size.height, width: self.viewOnlineDetail.frame.size.width, height: 45)
            
            self.viewOfflineDetail.isHidden = true
            self.viewOnlineDetail.isHidden = false
            
        })
    }
    
    
}
