//
//  JoinMemberVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 31/05/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class JoinMemberVC: UIViewController,PayPalPaymentDelegate {

    // FIXME: - VIEW CONTROLLER METHODS
    @IBOutlet var viewGold:UIView!
    @IBOutlet var viewSilver:UIView!
    
    @IBOutlet var lblUnderGold:UILabel!
    @IBOutlet var lblUnderSilver:UILabel!

    
    @IBOutlet var scrView:UIScrollView!
    //FOR SCROLLING VIEW
    var currentPag:Int!
    var pageCount:Int!
    let screenWidth = Int(274.0)

    var isFromOther:Bool?
    
    var planValue:String!
    var amountValue:String!
    var transactionID:String!


    @IBOutlet var viewAlreadyMember:UIView!

    @IBOutlet var btnPurchase:UIView!
    @IBOutlet var lblPurchase:UILabel!
    @IBOutlet var imageViewPurhcase:UIImageView!

    var isFromTab:String?

    
    // PAYPAL INTEGRATION
    var payPalConfig = PayPalConfiguration() // default
    var environment:String = PayPalEnvironmentSandbox {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
        
        
        self.setPayPalEnvironment(environment: environment)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        planValue = "2"
        amountValue = "990"
        pageCount=2;
        currentPag=0;
        
        scrView.contentSize = CGSize(width: CGFloat(screenWidth * pageCount), height: scrView.frame.size.height)
        
        viewGold.frame = CGRect(x: 0.0, y: viewGold.frame.origin.y, width: viewGold.frame.size.width, height: viewGold.frame.size.height)
        scrView.addSubview(viewGold)
        
        
        viewSilver.frame = CGRect(x: viewGold.frame.origin.x+viewGold.frame.size.width, y: viewSilver.frame.origin.y, width: viewSilver.frame.size.width, height: viewSilver.frame.size.height)
        scrView.addSubview(viewSilver)
        
        if isFromOther == true {
            if let member = USERDEFAULT.value(forKey: "isMember") as? String{
                let mem = "\(member)"
                if mem == "1"{
                    
                    btnPurchase.isHidden = true
                    lblPurchase.isHidden = true
                    imageViewPurhcase.isHidden = true
                    
//                   self.view.addSubview(viewAlreadyMember)
                }
                else{
                    btnPurchase.isHidden = false
                    lblPurchase.isHidden = false
                    imageViewPurhcase.isHidden = false

                    viewAlreadyMember.removeFromSuperview()
                }
            }
        }
        else{
            viewAlreadyMember.removeFromSuperview()
        }
        self.paypalConfiguration()
    }
    
    
    
    func settingBtnBgColor(_ sender:UIView){
        
        if sender == viewGold{
            lblUnderGold.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
            lblUnderSilver.backgroundColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        }
        else if sender == viewSilver{
            lblUnderGold.backgroundColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            lblUnderSilver.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        }
        
    }

    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }
    
    // TODO: - PAYPAL METHODS

    func paypalConfiguration() {
        // Set up payPalConfig
        payPalConfig.acceptCreditCards = false
//        payPalConfig.merchantName = "Awesome Shirts, Inc."
//        payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
//        payPalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        
        // Setting the languageOrLocale property is optional.
        //
        // If you do not set languageOrLocale, then the PayPalPaymentViewController will present
        // its user interface according to the device's current language setting.
        //
        // Setting languageOrLocale to a particular language (e.g., @"es" for Spanish) or
        // locale (e.g., @"es_MX" for Mexican Spanish) forces the PayPalPaymentViewController
        // to use that language/locale.
        //
        // For full details, including a list of available languages and locales, see PayPalPaymentViewController.h.
        
        payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
        
        // Setting the payPalShippingAddressOption property is optional.
        //
        // See PayPalConfiguration.h for details.
        
        self.environment = PayPalEnvironmentSandbox
        
        payPalConfig.payPalShippingAddressOption = .payPal
        payPalConfig.rememberUser = false

        
        print("PayPal iOS SDK Version: \(PayPalMobile.libraryVersion())")
        
    }
    
    func setPayPalEnvironment(environment:String) {
//        self.environment = environment
        self.environment = environment
        
        PayPalMobile.preconnect(withEnvironment: self.environment)
    }
    
    func settingsPaypalMethod(myAmount:String){
        
        let subtotal = NSDecimalNumber(string: myAmount)
        
        let payment = PayPalPayment()
        
        
        payment.amount = subtotal;
        payment.currencyCode = "USD";
        payment.shortDescription = "MEMBERSHIP";
        payment.paymentDetails = nil; // if not including payment details, then leave payment.paymentDetails as nil

        
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
            print("Payment not processalbe: \(payment)")
        }

    }
    
    
     // TODO: -  PayPalPaymentDelegate METHODS
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        
        self.sendCompletedPaymentToServer(completedPayment: completedPayment) //Payment was processed successfully send to server for verification and fulfillment
        
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            
        })
    }
    
    func sendCompletedPaymentToServer(completedPayment:PayPalPayment) {
        
        print("Here is your proof of payment:\n\n \(completedPayment.confirmation) \n\nSend this to your server for confirmation and fulfillment")
        
        let dict = NSMutableDictionary(dictionary: completedPayment.confirmation)
        print("Data : ",dict)
         
        transactionID = (dict.value(forKey: "response") as AnyObject).value(forKey: "id") as! String
        
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForJoinMember), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }

    }
        
        
    
    
    // TODO: - DELEGATE METHODS
    
    func setupScrollViewWithCurrentPage(currentPage:Int){
        scrView.contentOffset = CGPoint(x: currentPag * screenWidth, y: 0)
    }
    
    //ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.tag == 50) {
            currentPag = Int(scrollView.contentOffset.x) / Int(scrollView.frame.size.width);
            
            if (currentPag == 0) {
                self.settingBtnBgColor(viewGold)
                planValue = "2"
                amountValue = "990"
            }
            else if (currentPag == 1){
                self.settingBtnBgColor(viewSilver)
                planValue = "1"
                amountValue = "1500"
            }
            scrView.contentOffset = CGPoint(x: scrView.contentOffset.x, y: 0.0)
        }
    }
    // TODO: - ACTION METHODS
    @IBAction func btnBackClicked(_ sender:UIButton){
     //   _ = self.navigationController?.popViewController(animated: true)
//        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
//        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
        
        if isFromOther == true{
            self.navigationController?.popViewController(animated: true)
        }
        else{
            
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
            for aViewController:UIViewController in viewControllers {
                if aViewController.isKind(of: WelcomeVC.self) {
                    _ = self.navigationController?.popToViewController(aViewController, animated: true)
                }
            }
        }
        
    }
    
    @IBAction func btnPurchaseClicked(_ sender:UIButton){
      
        
//        if Reachability.isConnectedToNetwork() == true {
//            SVProgressHUD.show(withStatus: "Loading..")
//            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForJoinMember), userInfo: nil, repeats: false)
//        } else {
//            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
//        }
        
        self.settingsPaypalMethod(myAmount: amountValue)
        
    }
    
    // TODO: - POST DATA METHODS

    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForJoinMember(){
        let completeURL = NSString(format:"%@%@", MainURL,joinMemberURL) as String
        
        let paymentInfo = NSMutableDictionary()
        paymentInfo.setValue(amountValue, forKey: "amount")
        paymentInfo.setValue(transactionID, forKey: "transaction_id")
        
        print("Plan Value : ",planValue)
        print("amountValue Value : ",amountValue)
        print("transactionID : ",transactionID)
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "payment_info":paymentInfo,
            "plan":planValue
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        print("Join Member API Parameter :",finalParams)
        print("Join Member API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: joinMemberURLTag)
    }
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case joinMemberURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("Join Member Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
//                let notificationName = Notification.Name("notificationForGetProfileData")
//                NotificationCenter.default.post(name: notificationName, object: nil)
                
                let notificationName = Notification.Name("updateAfterMemberShip")
                NotificationCenter.default.post(name: notificationName, object: nil)
                
                
                let dataDictionary = resultDict.value(forKey: "data") as! NSDictionary
                
                USERDEFAULT.setValue(dataDictionary.value(forKey: "email") as! String, forKey: "emailID")
                USERDEFAULT.synchronize()
                
                USERDEFAULT.setValue(dataDictionary.value(forKey: "fullname") as! String, forKey: "fullName")
                USERDEFAULT.synchronize()
                
                if let member = dataDictionary.value(forKey: "is_member") as? String{
                    let mem = "\(member)"
                    USERDEFAULT.setValue(mem, forKey: "isMember")
                    USERDEFAULT.synchronize()
                }
                else if let member = dataDictionary.value(forKey: "is_member") as? NSNumber{
                    let mem = "\(member)"
                    USERDEFAULT.setValue(mem, forKey: "isMember")
                    USERDEFAULT.synchronize()
                }
                
                
                
                
                if isFromOther == true{
                    self.navigationController?.popViewController(animated: true)
                }
                else{
                    
                    if isFromTab=="0"{
                        let homeVC = TabBarVC(nibName: "TabBarVC", bundle: nil)
                        homeVC.selectedIndexOfMyTabbarController = 0
                        self.navigationController?.pushViewController(homeVC, animated: true)
                    }
                    else if isFromTab=="1"{
                        let homeVC = TabBarVC(nibName: "TabBarVC", bundle: nil)
                        homeVC.selectedIndexOfMyTabbarController = 1
                        self.navigationController?.pushViewController(homeVC, animated: true)
                    }
                    else if isFromTab=="2"{
                        let homeVC = TabBarVC(nibName: "TabBarVC", bundle: nil)
                        homeVC.selectedIndexOfMyTabbarController = 2
                        self.navigationController?.pushViewController(homeVC, animated: true)
                    }
                    else if isFromTab=="3"{
                        let homeVC = TabBarVC(nibName: "TabBarVC", bundle: nil)
                        homeVC.selectedIndexOfMyTabbarController = 3
                        self.navigationController?.pushViewController(homeVC, animated: true)
                    }
                    else if isFromTab=="4"{
                        let homeVC = TabBarVC(nibName: "TabBarVC", bundle: nil)
                        homeVC.selectedIndexOfMyTabbarController = 4
                        self.navigationController?.pushViewController(homeVC, animated: true)
                    }
                    else{
                        let homeVC = TabBarVC(nibName: "TabBarVC", bundle: nil)
                        homeVC.selectedIndexOfMyTabbarController = 0
                        self.navigationController?.pushViewController(homeVC, animated: true)
                    }
                }
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case joinMemberURLTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    
}
