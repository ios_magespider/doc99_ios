//
//  ForgetPasswordVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 30/03/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class FBMobileVC: UIViewController,EMCCountryDelegate,DiallingCodeDelegate {
    
    
    
    @IBOutlet var scrMain:UIScrollView!
    
    @IBOutlet var lblRequestCode:UILabel!
    

    //For Mobile
    @IBOutlet var viewMobileVarification:UIView!
    @IBOutlet var txtMobile:UITextField!
    var serverCountryCode:String!
    @IBOutlet var imageViewArrow:UIImageView!
    @IBOutlet var txtCountryCode:UITextField!

    var fbRegisterData:NSDictionary!
    
    // FIXME: - VIEW CONTROLLER METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("FBRegisterparameter  : ",fbRegisterData)
        
        self.generalViewControllerSetting()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
        
        if let countryCode = USERDEFAULT.value(forKey: "countryCode") as? String{
            txtCountryCode.text = "\(countryCode)"
            imageViewArrow.isHidden = true
        }
        else{
            imageViewArrow.isHidden = false
        }

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        
        scrMain.addSubview(viewMobileVarification)
        serverCountryCode = ""

        self.addTapGestureInOurView()
        self.setLocalizationText()
        self.NavBarNumberPad()
      //  self.addTapGestureInOurVerificationCodeView()
    }
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        if viewTouched!.isKind(of: UIButton.self){
            
        }
        else{
            self.view.endEditing(true)
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        }
    }
  /*  func addTapGestureInOurVerificationCodeView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap1(_:)))
        tapRecognizer.cancelsTouchesInView = false
        viewUnderVerificationCode.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap1(_ sender:UITapGestureRecognizer){
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        if viewTouched!.isKind(of: UIButton.self){
            
        }
        else{
            viewVerificationCode.removeFromSuperview()
        }
    }*/

    
    func setLocalizationText(){
        
//        lblDescription.text = NSLocalizedString("forgotdescription", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
//        lblRegisterEmailID.text = NSLocalizedString("registeremailid", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
//        lblRequestCode.text = NSLocalizedString("requestcode", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
//        
//        btnAlreadyAccount.setTitle(NSLocalizedString("alreadyaccount", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        
        
    }
    
    
    // Numeric Keyboard Hide Method
    func NavBarNumberPad(){
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        numberToolbar.barStyle = .default
        
        let viewBack = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        viewBack.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        numberToolbar.addSubview(viewBack)
        
        let btnCancel = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelNumberPad))
        btnCancel.tintColor = UIColor.white
        
        let btnDone = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.doneWithNumberPad))
        btnDone.tintColor = UIColor.white
        
        numberToolbar.items = NSArray(objects: btnCancel,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),btnDone) as? [UIBarButtonItem]
        
        
        numberToolbar.sizeToFit()
        txtMobile.inputAccessoryView = numberToolbar
    }
    
    func cancelNumberPad(){
        txtMobile.resignFirstResponder()
    }
    
    func doneWithNumberPad(){
        txtMobile.resignFirstResponder()
    }
    
    
    // TODO: - DELEGATE METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
            
        }
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var YOffset:Int = 0
        
        if (textField.tag==4) {
            YOffset=10
        }
        if (DeviceType.IS_IPHONE_4_OR_LESS) {
            
            if (textField.tag==4) {
                YOffset=30
            }
        }
        scrMain.setContentOffset(CGPoint(x: 0, y: CGFloat(textField.tag * YOffset)), animated: true)
        return true
    }

    
    //MARK: - EMCCountryPickerController Delegates Methods
    
    func countryController(_ sender: Any!, didSelect chosenCountry: EMCCountry?) {
        // Do something with chosenCountry
        //   print(chosenCountry.countryName)
        print(chosenCountry?.countryCode ?? "Blank")
        print(chosenCountry?.countryName() ?? "Blank")
        let diallingCode:DiallingCode = DiallingCode.init(delegate: self)
        //  diallingCode.getCountriesWithDiallingCode(chosenCountry.countryCode)
        diallingCode.getForCountry(chosenCountry?.countryCode)
    }
    
    //MARK: - DiallingCode Delegates Methods
    func failedToGetDiallingCode() {
        self.dismiss(animated: true, completion: nil)
    }
    func didGetDiallingCode(_ diallingCode: String!, forCountry countryCode: String!) {
        
        print(countryCode)
        print(diallingCode)
        
        print("+\(diallingCode!)")
        
        txtCountryCode.text = "+\(diallingCode!)"
        //        btnCountryCode.setTitle("+\(diallingCode!)", for: .normal)
        serverCountryCode = diallingCode as String!
        
        USERDEFAULT.setValue(txtCountryCode.text, forKey: "countryCode")
        USERDEFAULT.synchronize()
        
        imageViewArrow.isHidden = true
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func didGetCountries(_ countries: [Any]!, forDiallingCode diallingCode: String!) {
        print(diallingCode)
        
    }
    
    
    // TODO: - ACTION METHODS
    
      @IBAction func btnLoginClicked(_ sender:UIButton){
       _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnMobileRequestCodeClicked(_ sender:UIButton){
        self.view.endEditing(true)
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)

        if txtCountryCode.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please select countrycode", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if txtMobile.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter mobilenumber", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else{
            if Reachability.isConnectedToNetwork() == true {
                SVProgressHUD.show(withStatus: "Loading..")
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForRegistration), userInfo: nil, repeats: false)
            } else {
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }

        
    }
    

    
  

    
    @IBAction func btnCountryCodeCliked(_ sender:UIButton){
        let countryPicker = EMCCountryPickerController.init()
        countryPicker.countryDelegate = self
        self.present(countryPicker, animated: true, completion: nil)
    }

    
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForRegistration(){
        let completeURL = NSString(format:"%@%@", MainURL,registrationURL) as String
        
        let params:NSDictionary = [
                "fullname" : fbRegisterData.value(forKey: "fullname") as! String,
                "email" : fbRegisterData.value(forKey: "email") as! String,
                "phone":txtMobile.text!,
                "ccode":txtCountryCode.text!,
                "device_token":"3ltutv",
                "device_type":"1",
                "lang_type":Language_Type,
                "auth_id":fbRegisterData.value(forKey: "auth_id") as! String,
                "auth_provider":fbRegisterData.value(forKey: "auth_provider") as! String,
                
            ]
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("Registration API Parameter :",finalParams)
        print("Registration API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: registrationURLTag)
        
    }
    
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case registrationURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("Registration Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                let dataDictionary = resultDict.value(forKey: "data") as! NSDictionary
                
               
//                let homeVC = TabBarVC(nibName: "TabBarVC", bundle: nil)
//                homeVC.selectedIndexOfMyTabbarController = 0
//                self.navigationController?.pushViewController(homeVC, animated: true)
                
                
                //showAlert(Appname, title: resultDict.value(forKey: "message") as! String)
                let varificationVC = VerificationVC(nibName: "VerificationVC", bundle: nil)
                //                varificationVC.emailID = txtEmailid.text!
                varificationVC.emailID = dataDictionary.value(forKey: "phone") as! String
                varificationVC.ccCode = dataDictionary.value(forKey: "ccode") as! String
                
                varificationVC.isFrom = "FBLogin"
                varificationVC.byPassScreenName = "healthCategoryScreen"
                self.navigationController?.pushViewController(varificationVC, animated: true)
                

            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
                let dataDictionary = resultDict.value(forKey: "data") as! NSDictionary
                
                //showAlert(Appname, title: resultDict.value(forKey: "message") as! String)
                let varificationVC = VerificationVC(nibName: "VerificationVC", bundle: nil)
                //                varificationVC.emailID = txtEmailid.text!
                varificationVC.emailID = dataDictionary.value(forKey: "phone") as! String
                varificationVC.ccCode = dataDictionary.value(forKey: "ccode") as! String
                
                varificationVC.isFrom = "FBLogin"
                varificationVC.byPassScreenName = "healthCategoryScreen"
                
                self.navigationController?.pushViewController(varificationVC, animated: true)
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case registrationURLTag:
            SVProgressHUD.dismiss()
            break
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }

    
}
