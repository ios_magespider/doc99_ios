//
//  WelcomeVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 31/05/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class WelcomeVC: UIViewController {

    @IBOutlet var lblWelcome:UILabel!
    @IBOutlet var lblTerm:UILabel!
    @IBOutlet var lblStart:UILabel!

    @IBOutlet var btnLogin:UIButton!
    @IBOutlet var btnMember:UIButton!
    @IBOutlet weak var lblTermsAndCondation: FRHyperLabel!
    
    
    // FIXME: - VIEW CONTROLLER METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the
        
        
        //New Changes

        let string = "By using our Doc-99 Chemist apps or site, you  you acknowledge that you have read and accept our Terms of Service and Privacy Policy and the use of cookies as described there in."

        let attributes = [NSForegroundColorAttributeName :  UIColor.white, NSFontAttributeName: UIFont(name: "Lato-Regular", size: 16)!]
        
       
         self.lblTermsAndCondation.attributedText = NSAttributedString(string: string, attributes: attributes)
        
        let handler = { (hyperLabel: FRHyperLabel?, substring: String?) -> Void in
            
            if substring == "Doc-99 Chemist"{
                let termConditionVC = AboutUsVC(nibName: "AboutUsVC", bundle: nil)
                self.navigationController?.pushViewController(termConditionVC, animated: true)
            }else{
                let termConditionVC = TermConditionVC(nibName: "TermConditionVC", bundle: nil)
                self.navigationController?.pushViewController(termConditionVC, animated: true)
            }
            
           
        }
        //Step 3: Add link substrings
        self.lblTermsAndCondation.setLinksForSubstrings(["Doc-99 Chemist", "Terms of Service and Privacy Policy"], withLinkHandler: handler)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        
        self.setLocalizationText()
    }
    
    func setLocalizationText(){
        
        btnLogin.setTitle(NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        btnMember.setTitle(NSLocalizedString("Join Doc99 Member", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        
        lblWelcome.text = NSLocalizedString("Welcome To Doc99", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblStart.text = NSLocalizedString("START NOW", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTerm.text = NSLocalizedString("By Clicking, you agree to our Terms & that you have  read our  Data Use Policy", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
    }

    
    // TODO: - DELEGATE METHODS
    
    // TODO: - ACTION METHODS
    
    @IBAction func btnLoginClicked(_ sender:UIButton){
        let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    @IBAction func btnStartNowClicked(_ sender:UIButton){
        
        let articalWelcomeVC = ArticalWelcomeVC(nibName: "ArticalWelcomeVC", bundle: nil)
        self.navigationController?.pushViewController(articalWelcomeVC, animated: true)
        
//        if let firstTIme = USERDEFAULT.value(forKey: "isFirstTime") as? String{
//            let first = "\(firstTIme)"
//            if first == "NO"{
//                
//                let homeVC = TabBarVC(nibName: "TabBarVC", bundle: nil)
//                homeVC.selectedIndexOfMyTabbarController = 1  
//                self.navigationController?.pushViewController(homeVC, animated: true)
//                
//            }
//            else{
//                let articalWelcomeVC = ArticalWelcomeVC(nibName: "ArticalWelcomeVC", bundle: nil)
//                self.navigationController?.pushViewController(articalWelcomeVC, animated: true)
//            }
//        }
//        else{
//            let articalWelcomeVC = ArticalWelcomeVC(nibName: "ArticalWelcomeVC", bundle: nil)
//            self.navigationController?.pushViewController(articalWelcomeVC, animated: true)
//        }
        
    }
    @IBAction func btnJoinMemberClicked(_ sender:UIButton){
        
        let registrationVC = RegisterVC(nibName: "RegisterVC", bundle: nil)
       
        self.navigationController?.pushViewController(registrationVC, animated: true)

    }
    
    @IBAction func btnTermConditionClicked(_ sender:UIButton){
        
    }

    
    // TODO: - POST DATA METHODS
    
    

}
extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}
extension Range where Bound == String.Index {
    var nsRange:NSRange {
        return NSRange(location: self.lowerBound.encodedOffset,
                       length: self.upperBound.encodedOffset -
                        self.lowerBound.encodedOffset)
    }
}
