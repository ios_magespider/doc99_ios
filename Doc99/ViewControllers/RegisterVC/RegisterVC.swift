//
//  RegisterVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 30/03/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class RegisterVC: UIViewController,EMCCountryDelegate,DiallingCodeDelegate, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate {

    @IBOutlet var txtFullname:UITextField!
    @IBOutlet var txtEmailid:UITextField!
    @IBOutlet var txtMobile:UITextField!
    @IBOutlet var txtPassword:UITextField!
    @IBOutlet var txtRePassword:UITextField!
    @IBOutlet var txtCountryCode:UITextField!

    @IBOutlet var scrMain:UIScrollView! 
    @IBOutlet var txtBirthDate:UITextField!

    @IBOutlet weak var imgAgree: UIImageView!
    @IBOutlet weak var btnAgree: UIButton!
    
    
    @IBOutlet var datePicker:UIDatePicker!
    @IBOutlet var viewDatePicker:UIView!
    

    var serverCountryCode:String!
    
    
    @IBOutlet var lblFullname:UILabel!
    @IBOutlet var lblEmailID:UILabel!
    @IBOutlet var lblMobile:UILabel!
    @IBOutlet var lblPassword:UILabel!
    @IBOutlet var lblRePassword:UILabel!
    @IBOutlet var lblSignUp:UILabel!
    @IBOutlet var btnAlreadyAccount:UIButton!

    @IBOutlet var viewCountryPicker: UIView!
    @IBOutlet weak var tableCountryCode: UITableView!
    @IBOutlet weak var btnCloseCountryView: UIButton!
    
    //MARK:-
    
    var isFromHealthCategory:Bool?
    var isAgree = false
    
    var arrayCountryCode : NSArray = [["code":"852","country":"Hong Kong"],["code":"65","country":"Singapore"],["code":"60","country":"Malaysia"],
                                      ["code":"66","country":"Thailand"],["code":"850","country":"Korea"],["code":"81","country":"Japan"],
                                      ["code":"853","country":"Macau"],["code":"86","country":"China"],["code":"91","country":"India"],
                                      ["code":"1","country":"USA"],["code":"1","country":"Canada"],["code":"61","country":"Australia"]]
    
    // FIXME: - VIEW CONTROLLER METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        txtBirthDate.delegate = self
        self.generalViewControllerSetting()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        
        serverCountryCode = ""
        self.setLocalizationText()
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            print("Current Country code :",countryCode)
            
        }
    }
    
    
    @IBAction func btnCloseCountryViewClicked(_ sender: UIButton) {
        viewCountryPicker.removeFromSuperview()
    }
    
  
    func setLocalizationText(){
        
        lblEmailID.text = NSLocalizedString("emailid".localized, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblPassword.text = NSLocalizedString("password".localized, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblFullname.text = NSLocalizedString("fullname".localized, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblRePassword.text = NSLocalizedString("retypepassword".localized, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblMobile.text = NSLocalizedString("code".localized, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblSignUp.text = NSLocalizedString("signup".localized, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")


    }
    
    
    //MARK:- Table Delegate Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCountryCode.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cellIdentifier")
        cell.selectionStyle = .none
        
        
        let str = String(format: "+%@  %@", (arrayCountryCode[indexPath.row]as! NSDictionary).value(forKey: "code")as? String ?? "", (arrayCountryCode[indexPath.row]as! NSDictionary).value(forKey: "country")as? String ?? "")
        
        cell.textLabel?.text = str
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        txtCountryCode.text = String(format: "+%@", (arrayCountryCode[indexPath.row]as! NSDictionary).value(forKey: "code")as? String ?? "")
        self.btnCloseCountryViewClicked(btnCloseCountryView)
    }
    
    // TODO: - DELEGATE METHODS
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        if(textField == txtBirthDate)
        {
            let maxLength = 4
            let currentString: NSString = txtBirthDate.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        } else  if textField ==  txtMobile {
            let maxLength = 10
            let currentString: NSString = txtMobile.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
      
        return true

    }
    
    //MARK: - EMCCountryPickerController Delegates Methods
    
    func countryController(_ sender: Any!, didSelect chosenCountry: EMCCountry?) {
        // Do something with chosenCountry
        //   print(chosenCountry.countryName)
        print(chosenCountry?.countryCode ?? "Blank")
        print(chosenCountry?.countryName() ?? "Blank")
        let diallingCode:DiallingCode = DiallingCode.init(delegate: self)
        //  diallingCode.getCountriesWithDiallingCode(chosenCountry.countryCode)
        diallingCode.getForCountry(chosenCountry?.countryCode)
    }
    
    //MARK: - DiallingCode Delegates Methods
    func failedToGetDiallingCode() {
        self.dismiss(animated: true, completion: nil)
    }
    func didGetDiallingCode(_ diallingCode: String!, forCountry countryCode: String!) {
        
        print(countryCode)
        print(diallingCode)
        
        print("+\(diallingCode!)")
        
        txtCountryCode.text = "+\(diallingCode!)"
        //        btnCountryCode.setTitle("+\(diallingCode!)", for: .normal)
        serverCountryCode = diallingCode as String!
        
        USERDEFAULT.setValue(txtCountryCode.text, forKey: "countryCode")
        USERDEFAULT.synchronize()
        
        
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func didGetCountries(_ countries: [Any]!, forDiallingCode diallingCode: String!) {
        print(diallingCode)
        
    }
    
    
    // TODO: - ACTION METHODS
    @IBAction func btnDatePickerSelectedClicked(_ sender:UIButton){
        self.view.endEditing(true)
        
        if txtBirthDate.text != "" {
            let str = txtBirthDate.text!
            if str != "0000-00-00"{
                let dateFormater = DateFormatter()
                dateFormater.dateFormat = "yyyy-MM-dd"
                var dateFromString = Date.init()
                dateFromString = dateFormater.date(from: str as String)!
                datePicker.setDate(dateFromString as Date, animated: true)
            }
        }
        
        datePicker.maximumDate = NSDate() as Date
        datePicker.datePickerMode = .date;
        
        
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewDatePicker.frame=CGRect(x: self.view.frame.size.width - self.viewDatePicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y - self.viewDatePicker.frame.size.height, width: self.viewDatePicker.frame.size.width, height: self.viewDatePicker.frame.size.height)
            
            
        })
        
        
    }
    
    @IBAction func cancelPicker(sender:AnyObject){
        UIView.animate(withDuration: 0.3) {
            self.viewDatePicker.frame=CGRect(x: self.view.frame.size.width - self.viewDatePicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewDatePicker.frame.size.width, height: self.viewDatePicker.frame.size.height)
        }
    }
    @IBAction func doneDatePicker(sender:UIButton){
        
        txtBirthDate.text = getStringDateFromDate(dateFormat: "yyyy-MM-dd", enterDate: datePicker.date)
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewDatePicker.frame=CGRect(x: self.view.frame.size.width - self.viewDatePicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewDatePicker.frame.size.width, height: self.viewDatePicker.frame.size.height)
            
        })
    }
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnRegistrationClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        //scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
        
        
        if Utility.isEmpty(txtFullname.text){
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter fullname", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if !Utility.isEmpty(txtEmailid.text) && isValidEmail(testStr: txtEmailid.text!) == false{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter emailId in valid format", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if Utility.isEmpty(txtCountryCode.text){
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please select countrycode", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if Utility.isEmpty(txtMobile.text){
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter mobilenumber", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }else if !(txtMobile.text!.count == 9) && !(txtMobile.text!.count == 10) {
            txtMobile.becomeFirstResponder()
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Phone number minimum length should be 10 characters.", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if Utility.isEmpty(txtBirthDate.text){
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter year of birth", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if Utility.isEmpty(txtPassword.text){
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter password", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if Utility.isEmpty(txtRePassword.text){
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter confirm password", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if txtPassword.text != txtRePassword.text{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Password doesn't match", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if !isAgree{
            showAlert(Appname, title: "Please agree to terms.")
        }
        else{
            if Reachability.isConnectedToNetwork() == true {
                SVProgressHUD.show(withStatus: "Loading..")
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForRegistration), userInfo: nil, repeats: false)
            } else {
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }
    }
    @IBAction func btnTermsClicked(_ sender:UIButton){
        let terms = TermsAndDescliamerVC(nibName: "TermsAndDescliamerVC", bundle: nil)
        self.navigationController?.pushViewController(terms, animated: true)
    }
    
    @IBAction func btnCountryCodeCliked(_ sender:UIButton){
        
        self.view.endEditing(true)
        
        viewCountryPicker.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.view.addSubview(viewCountryPicker)
        
//        let countryPicker = EMCCountryPickerController.init()
//        countryPicker.countryDelegate = self
//        self.present(countryPicker, animated: true, completion: nil)
    }
    
    @IBAction func btnAgreeClicked(_ sender: Any) {
        if isAgree {
            isAgree = false
            imgAgree.image = UIImage(named: "check_gray.png")
        }
        else{
            isAgree = true
            imgAgree.image = UIImage(named: "check_blue.png")
        }
    }
    
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForRegistration(){
        let completeURL = NSString(format:"%@%@", MainURL,registrationURL) as String
        
        let params:NSDictionary = [
            "fullname" : txtFullname.text!,
            "email" : txtEmailid.text!,
            "phone":txtMobile.text!,
            "ccode":txtCountryCode.text!,
            "password":txtPassword.text!,
            "dob":txtBirthDate.text!,
            "device_token":APPDELEGATE.deviceTokenForPushNotification ?? "",
            "device_type":"1",
            "lang_type":Language_Type,
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("Registration API Parameter :",finalParams)
        print("Registration API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: registrationURLTag)
        
    }
    
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case registrationURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("Registration Response  : \(resultDict)")

            if resultDict.value(forKey: "status") as! String == "1"{
                
//                let dataDictionary = resultDict.value(forKey: "data") as! NSDictionary
//                USERDEFAULT.setValue(dataDictionary.value(forKey: "user_id") as! String, forKey: "userID")
//                USERDEFAULT.synchronize()
//                
//                USERDEFAULT.setValue(dataDictionary.value(forKey: "token") as! String, forKey: "token")
//                USERDEFAULT.synchronize()
//                
//                USERDEFAULT.setValue(dataDictionary.value(forKey: "email") as! String, forKey: "emailID")
//                USERDEFAULT.synchronize()
//                
//                USERDEFAULT.setValue(dataDictionary.value(forKey: "fullname") as! String, forKey: "fullName")
//                USERDEFAULT.synchronize()
                
                if isFromHealthCategory == true{
//                    let dataDictionary = resultDict.value(forKey: "data") as! NSDictionary
//
//                    USERDEFAULT.setValue(dataDictionary.value(forKey: "user_id") as! String, forKey: "userID")
//                    USERDEFAULT.synchronize()
//
//                    USERDEFAULT.setValue(dataDictionary.value(forKey: "token") as! String, forKey: "token")
//                    USERDEFAULT.synchronize()
//
//                    USERDEFAULT.setValue(dataDictionary.value(forKey: "email") as! String, forKey: "emailID")
//                    USERDEFAULT.synchronize()
//
//                    USERDEFAULT.setValue(dataDictionary.value(forKey: "fullname") as! String, forKey: "fullName")
//                    USERDEFAULT.synchronize()
//
//                    USERDEFAULT.setValue(dataDictionary.value(forKey: "dob") as! String, forKey: "dob")
//                    USERDEFAULT.synchronize()
//
//                    let homeVC = TabBarVC(nibName: "TabBarVC", bundle: nil)
//                    homeVC.selectedIndexOfMyTabbarController = 0
//                    self.navigationController?.pushViewController(homeVC, animated: true)
                    
                    
                    
                } else if resultDict.value(forKey: "status") as! String == "3"{
                    let dataDictionary = resultDict.value(forKey: "data") as! NSDictionary
                    
                    //showAlert(Appname, title: resultDict.value(forKey: "message") as! String)
                    let varificationVC = VerificationVC(nibName: "VerificationVC", bundle: nil)
                    //                varificationVC.emailID = txtEmailid.text!
                    
                    varificationVC.emailID = dataDictionary.value(forKey: "phone") as? String
                    varificationVC.ccCode = (dataDictionary.value(forKey: "ccode") as! String)
                    varificationVC.isFrom = "Register"
                    
                    self.navigationController?.pushViewController(varificationVC, animated: true)
                }
                    
                else{
                    self.navigationController?.popViewController(animated: true)
                }
                
                
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            let dataDictionary = resultDict.value(forKey: "data") as! NSDictionary
                
                //showAlert(Appname, title: resultDict.value(forKey: "message") as! String)
                let varificationVC = VerificationVC(nibName: "VerificationVC", bundle: nil)
//                varificationVC.emailID = txtEmailid.text!
                varificationVC.emailID = dataDictionary.value(forKey: "phone") as! String
                varificationVC.ccCode = dataDictionary.value(forKey: "ccode") as! String

                varificationVC.isFrom = "Register"
                varificationVC.byPassScreenName = "healthCategoryScreen"
                
                self.navigationController?.pushViewController(varificationVC, animated: true)
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case registrationURLTag:
            SVProgressHUD.dismiss()
            break
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
   
    
    

    
}
