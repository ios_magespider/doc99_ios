//
//  TermsAndDescliamerVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 20/03/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit

class TermsAndDescliamerVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
