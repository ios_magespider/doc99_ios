//
//  ArticalDetailVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 12/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD

class ArticalDetailVC: UIViewController,UIScrollViewDelegate {

    @IBOutlet var lblArticalName:UILabel!
    @IBOutlet var lblArticalCategory:UILabel!
    @IBOutlet var lblArticalDescription:UILabel!
    @IBOutlet var imageViewArtical:UIImageView!

    @IBOutlet var scrArticalDetail:UIScrollView!
    @IBOutlet var viewArticalDetail:UIView!

    
    @IBOutlet var activityIndicatorForArticalDetailImage:UIActivityIndicatorView!

    @IBOutlet var viewHeader:UIView!
    @IBOutlet var lblHeader:UILabel!
    @IBOutlet var imageViewHeader:UIImageView!

    @IBOutlet var imageViewAuthor:UIImageView!
    @IBOutlet var lblAuthorName:UILabel!
    @IBOutlet var viewAuthor:UIView!

    var articalID:String!

    var articalDATA = NSDictionary()
    
    
    @IBOutlet var imageViewBookMark:UIImageView!

    // FIXME: - VIEW CONTROLLER METHODS
    override func viewDidLoad() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        super.viewDidLoad()
        self.generalViewControllerSetting()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        viewArticalDetail.frame = CGRect(x: 0, y: 0, width: scrArticalDetail.frame.size.width, height: viewArticalDetail.frame.size.height)
        scrArticalDetail.addSubview(viewArticalDetail)
        
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetArticalDetail), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }

    }
    
    func setArticalDetailData(articalData:NSDictionary){
        
        
        if let articalName =  articalData.value(forKey: "article_name") as? String
        {
            lblArticalName.text = "\(articalName)"
            lblHeader.text = "\(articalName)"
        }
    
        
        
        if let categoryData =  articalData.value(forKey: "article_category") as? NSArray
        {
            let catData = categoryData
            if catData.count > 0 {
                lblArticalCategory.isHidden = false
                let mutableStirng = NSMutableString()
                
                for i in 0...catData.count - 1 {
                    if i != 0{
                        mutableStirng.append(", ")
                    }
                    
                    if let str = (catData.object(at: i) as AnyObject).value(forKey: "ac_name") as? String{
                    mutableStirng.append(str)
                    }
                }
                
                lblArticalCategory.text = mutableStirng as String//(catData.object(at: 0) as AnyObject).value(forKey: "ac_name") as? String
                lblArticalCategory.clipsToBounds = true
                lblArticalCategory.layer.cornerRadius = 5.0

                lblArticalCategory.sizeToFit()
                
                //lblArticalCategory.frame = CGRect(x: (lblArticalCategory.frame.origin.x), y: ((lblArticalCategory.frame.origin.y) + (lblArticalCategory.frame.size.height) ), width: (lblArticalCategory.frame.size.width) + 10, height: (lblArticalCategory.frame.size.height) + 5.0)
                lblArticalCategory.textAlignment = .center


            }
            else{
                lblArticalCategory.isHidden = true
            }
        }
        

        
        
        
        let imageUrl = articalData.value(forKey: "article_img") as? String
        let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl!) as String
        let url : NSString = fullUrl as NSString
        let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
        let searchURL : NSURL = NSURL(string: urlStr as String)!
        
        
        activityIndicatorForArticalDetailImage.isHidden = false
        activityIndicatorForArticalDetailImage.startAnimating()
        
        imageViewArtical.sd_setImage(with: searchURL as URL, completed: { (image:UIImage?, error:Error?, cacheType:SDImageCacheType!, imageURL:URL?) in
            
            if ((error) != nil) {
                self.imageViewArtical.image = UIImage.init(named: "image_placeholder.png")
            }
            self.activityIndicatorForArticalDetailImage.isHidden = true
        })
        
        
        
        lblArticalDescription.text = articalData.value(forKey: "article_desc") as? String
        lblArticalDescription.sizeToFit()
        
        lblArticalName.sizeToFit()
        
        lblArticalDescription.frame = CGRect(x: lblArticalDescription.frame.origin.x, y: lblArticalName.frame.origin.y + lblArticalName.frame.size.height + 8, width: lblArticalDescription.frame.size.width, height: lblArticalDescription.frame.size.height)
        
        viewArticalDetail.frame = CGRect(x: viewArticalDetail.frame.origin.x, y: viewArticalDetail.frame.origin.y, width: ScreenSize.SCREEN_WIDTH, height: lblArticalDescription.frame.size.height + lblArticalDescription.frame.origin.y + 10.0)
        
        scrArticalDetail.contentSize = CGSize(width: scrArticalDetail.frame.size.width, height: viewArticalDetail.frame.size.height)

        
        if let ifFav = articalData.value(forKey: "is_favourite") as? String{
            let fav = "\(ifFav)"
            if fav == "1"{
                imageViewBookMark.image = UIImage.init(named: "bookmark_green.png")
            }
            else{
                imageViewBookMark.image = UIImage.init(named: "bookmark_white.png")
            }
        }
        else{
            imageViewBookMark.image = UIImage.init(named: "bookmark_white.png")
        }
        
    }
    
    // TODO: - DELEGATE METHODS
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //print("ContentOffset",scrArticalDetail.contentOffset.y)
        if scrArticalDetail.contentOffset.y > (imageViewArtical.frame.size.height - viewHeader.frame.size.height){
            UIView.animate(withDuration: 0.2, animations: {
                self.imageViewArtical.isHidden = true
                self.viewHeader.backgroundColor = UIColor.init(red: 98.0/255.0, green: 192.0/255.0, blue: 210.0/255.0, alpha: 1.0)
                self.viewHeader.alpha = 1.0
                
            })
            lblHeader.isHidden = false
        }
        else{
            lblHeader.isHidden = true
          UIView.animate(withDuration: 0.2, animations: {
            self.imageViewArtical.isHidden = false
            self.viewHeader.backgroundColor = UIColor.clear
            self.viewHeader.alpha = 0.0
          })
        }
    }
    
    // TODO: - ACTION METHODS
    @IBAction func btnBackClicked(_ sender:UIButton){
        _ =  self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAuthorClicked(_ sender:UIButton){
        let authorDetailVC = AuthorDetailVC(nibName: "AuthorDetailVC", bundle: nil)
        self.navigationController?.pushViewController(authorDetailVC, animated: true)
    }
    
    @IBAction func btnSharingClicked(_ sender:UIButton){
        
        if USERDEFAULT.value(forKey: "userID") != nil{
            // text to share
            let text = articalDATA.value(forKey: "article_name") as? String
            
            //        let imageUrl = (self.articalData[sender.tag] as AnyObject).value(forKey: "article_img") as! String
            //        let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl) as String
            
            
            
            let  activityViewController = UIActivityViewController(activityItems: [self,text!,imageViewArtical.image!], applicationActivities: [])
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            
            // exclude some activity types from the list (optional)
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)

        }
        else{
            // text to share
            let  activityViewController = UIActivityViewController(activityItems: [self,lblArticalName.text!,imageViewArtical.image!], applicationActivities: [])
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            
            // exclude some activity types from the list (optional)
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)

        }
        
        
        
    }
    
    @IBAction func btnBookmarkClicked(_ sender:UIButton){
        
        if USERDEFAULT.value(forKey: "userID") != nil{
            let ifFav = self.articalDATA.value(forKey: "is_favourite") as? String
            articalID = self.articalDATA.value(forKey: "article_id") as? String
            
            if ifFav == "1"{
                let temp = self.articalDATA.mutableCopy() as! NSMutableDictionary
                print(temp)
                temp.setValue("0", forKey: "is_favourite")
                
                articalDATA = temp.mutableCopy() as! NSDictionary
                
                imageViewBookMark.image = UIImage.init(named: "bookmark_white.png")
                
                if Reachability.isConnectedToNetwork() == true {
                    self.performSelector(inBackground: #selector(self.postDataOnWebserviceForRemoveFromBookMark), with: nil)
                }else{
                    showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                }
                
                
            }
            else{
                let temp = self.articalDATA.mutableCopy() as! NSMutableDictionary
                print(temp)
                temp.setValue("1", forKey: "is_favourite")
                
                articalDATA = temp.mutableCopy() as! NSDictionary
                
                imageViewBookMark.image = UIImage.init(named: "bookmark_green.png")
                
                
                if Reachability.isConnectedToNetwork() == true {
                    self.performSelector(inBackground: #selector(self.postDataOnWebserviceForAddToBookMark), with: nil)
                }else{
                    showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                }
                
            }
        }
        else{
            let ifFav = self.articalDATA.value(forKey: "is_favourite") as? String
            articalID = self.articalDATA.value(forKey: "article_id") as? String
            
            if ifFav == "1"{
                let temp = self.articalDATA.mutableCopy() as! NSMutableDictionary
                print(temp)
                temp.setValue("0", forKey: "is_favourite")
                
                articalDATA = temp.mutableCopy() as! NSDictionary
                
                imageViewBookMark.image = UIImage.init(named: "bookmark_white.png")
            }
            else{
                let temp = self.articalDATA.mutableCopy() as! NSMutableDictionary
                print(temp)
                temp.setValue("1", forKey: "is_favourite")
                
                articalDATA = temp.mutableCopy() as! NSDictionary
                
                imageViewBookMark.image = UIImage.init(named: "bookmark_green.png")
            }
            
//            USERDEFAULT.setValue(self.articalDATA, forKey: "offlineBookMarkArticalData")
//            USERDEFAULT.synchronize()

        }
        
    }
    

    // TODO: - POST DATA METHODS

    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForGetArticalDetail(){
        let completeURL = NSString(format:"%@%@", MainURL,articleDetailURL) as String
        
        var params:NSDictionary!

        
        if USERDEFAULT.value(forKey: "userID") != nil{
            params = [
                "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
                "token": USERDEFAULT.value(forKey: "token") as! String,
                "lang_type":Language_Type,
                "article_id":articalID
            ]

        }
        else{
            params = [
                "lang_type":Language_Type,
                "article_id":articalID
            ]
            
        }
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetArticalDetail API Parameter :",finalParams)
        print("GetArticalDetail API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: articleDetailURLTag)
        
    }
    
    func postDataOnWebserviceForAddToBookMark(){
        let completeURL = NSString(format:"%@%@", MainURL,addArticalBookMarkURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "article_id":articalID
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("AddToBookMark API Parameter :",finalParams)
        print("AddToBookMark API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: addArticalBookMarkURLTag)
    }
    
    func postDataOnWebserviceForRemoveFromBookMark(){
        let completeURL = NSString(format:"%@%@", MainURL,removeArticalBookMarkURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "article_id":articalID
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("AddToBookMark API Parameter :",finalParams)
        print("AddToBookMark API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: removeArticalBookMarkURLTag)
    }
    

    
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case articleDetailURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("GetArticalDetail List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                self.setArticalDetailData(articalData: resultDict.value(forKey: "data") as! NSDictionary)
                articalDATA = resultDict.value(forKey: "data") as! NSDictionary
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            SVProgressHUD.dismiss()
            break
            
        case addArticalBookMarkURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("Add Bookmark Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                let notificationName = Notification.Name("refreshingArticalList")
                // Post notification
                NotificationCenter.default.post(name: notificationName, object: nil)
                
                let notificationName1 = Notification.Name("refreshingBookmarkArticalList")
                // Post notification
                NotificationCenter.default.post(name: notificationName1, object: nil)


                
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        case removeArticalBookMarkURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("Remove Bookmark Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                let notificationName = Notification.Name("refreshingArticalList")
                // Post notification
                NotificationCenter.default.post(name: notificationName, object: nil)

                let notificationName1 = Notification.Name("refreshingBookmarkArticalList")
                // Post notification
                NotificationCenter.default.post(name: notificationName1, object: nil)
                
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            break

        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case articleDetailURLTag:
            SVProgressHUD.dismiss()
            break
            
        case addArticalBookMarkURLTag:
            break
        case removeArticalBookMarkURLTag:
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
            
        default:
            break
        }
    }
    

    
    
}
