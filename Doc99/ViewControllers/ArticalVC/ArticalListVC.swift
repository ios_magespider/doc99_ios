//
//  ArticalListVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 12/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SDWebImage


class ArticalListVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate {

    @IBOutlet var tableViewArticalList:UITableView!
    //For Pasignation
    var pageNum:Int!
    var isLoading:Bool?

    var articalData = NSMutableArray()
    var articalGlobalData = NSMutableArray()

    @IBOutlet var lblNoArtical:UILabel!

    var articalID:String!
    
    var isFilterData:Bool?
    var filterIDDataString:String!

    var filterData = NSDictionary()

    
    @IBOutlet var viewSearchArtical:UIView!
    @IBOutlet var txtSearch:UITextField!
    @IBOutlet var headerView:UIView!


    @IBOutlet var lblFilterCount:UILabel!
    @IBOutlet var lblFilterBy:UILabel!
    @IBOutlet var imageViewFilter:UIImageView!

    
    @IBOutlet var lblHeaderArtical:UILabel!

    @IBOutlet var btnBookMark:UIButton!
    @IBOutlet var btnSearch:UIButton!

    var refreshControl: UIRefreshControl!


    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Tabbar Selected Index",APPDELEGATE.myTabBarController?.selectedIndex ?? "Nothing")
        
        self.generalViewControllerSetting()
        
        tableViewArticalList.rowHeight = UITableViewAutomaticDimension
        tableViewArticalList.estimatedRowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = false
        
        //self.viewSearchArtical.frame = CGRect(x: ScreenSize.SCREEN_WIDTH, y: self.headerView.frame.origin.y, width: self.viewSearchArtical.frame.size.width, height: headerView.frame.size.height)

//        if USERDEFAULT.value(forKey: "userID") != nil{
//            btnBookMark.isHidden = false
//            btnSearch.frame = CGRect(x: btnSearch.frame.origin.x, y: btnSearch.frame.origin.y, width: btnSearch.frame.size.width, height: btnSearch.frame.size.height)
//        }
//        else{
//            btnBookMark.isHidden = true
//            btnSearch.frame = CGRect(x: btnBookMark.frame.origin.x, y: btnBookMark.frame.origin.y, width: btnSearch.frame.size.width, height: btnSearch.frame.size.height)
//        }

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // TODO: - OTHER METHODS

    func generalViewControllerSetting(){
        
        headerView.addSubview(viewSearchArtical)
        
        self.viewSearchArtical.frame = CGRect(x: ScreenSize.SCREEN_WIDTH, y: 0,
                                              width: self.viewSearchArtical.frame.size.width,
                                              height: headerView.frame.size.height)
        
        lblFilterCount.layer.cornerRadius = lblFilterCount.frame.size.width / 2
        lblFilterCount.clipsToBounds = true
        // Define identifier
        let notificationName = Notification.Name("refreshingArticalList")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(notificationFromBookmarkCalled(notification:)), name: notificationName, object: nil)
        
        
        // Define identifier
        let notificationName1 = Notification.Name("filterArticalList")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(filterArticalDataCalled(notification:)), name: notificationName1, object: nil)
        
        

        if USERDEFAULT.value(forKey: "filterData") != nil{
            filterData = USERDEFAULT.value(forKey: "filterData") as! NSDictionary
            
            if let articalCatData = filterData.value(forKey: "articalCatData") as? NSArray{
                let atrticalCatData1 = articalCatData
                print("atrtical Category Data :",atrticalCatData1)
                lblFilterCount.isHidden = false
                lblFilterCount.text = "1"
            }
            else{
                lblFilterCount.isHidden = true
                lblFilterCount.text = ""
            }
            
            lblFilterBy.textColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
            imageViewFilter.image = UIImage.init(named: "filter_blue.png")
            isFilterData = true
            
            
        }
        else{
            lblFilterCount.isHidden = true
            lblFilterCount.text = ""

            lblFilterBy.textColor = UIColor.init(red: 108.0/255.0, green: 110.0/255.0, blue: 112.0/255.0, alpha: 1.0)
            imageViewFilter.image = UIImage.init(named: "filter_gray.png")

            isFilterData = false
        }

        
        

     //   isFilterData = false
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.articalData = NSMutableArray()
        self.articalGlobalData = NSMutableArray()
        
        if Reachability.isConnectedToNetwork() == true {
        self.addLoadingIndicatiorOnFooterOnTableView()
        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetArticalList), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        self.addTapGestureInOurView()
        self.setLocalizationText()
        self.addPullRefreshForArtical()

    }
    
    func addPullRefreshForArtical(){
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.gray
        refreshControl.addTarget(self, action: #selector(self.refreshArtical(sender:)), for: .valueChanged)
        tableViewArticalList.addSubview(refreshControl)
    }
    
    func refreshArtical(sender:AnyObject) {
        // Code to refresh table view
        
        
        if USERDEFAULT.value(forKey: "filterData") != nil{
            filterData = USERDEFAULT.value(forKey: "filterData") as! NSDictionary
            
            if let articalCatData = filterData.value(forKey: "articalCatData") as? NSArray{
                let atrticalCatData1 = articalCatData
                print("atrtical Category Data :",atrticalCatData1)
                lblFilterCount.isHidden = false
                lblFilterCount.text = "1"
            }
            else{
                lblFilterCount.isHidden = true
                lblFilterCount.text = ""
            }
            
            lblFilterBy.textColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
            imageViewFilter.image = UIImage.init(named: "filter_blue.png")
            isFilterData = true
            
            
        }
        else{
            lblFilterCount.isHidden = true
            lblFilterCount.text = ""
            
            lblFilterBy.textColor = UIColor.init(red: 108.0/255.0, green: 110.0/255.0, blue: 112.0/255.0, alpha: 1.0)
            imageViewFilter.image = UIImage.init(named: "filter_gray.png")
            
            isFilterData = false
        }
        
        
        
        
        //   isFilterData = false
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.articalData = NSMutableArray()
        self.articalGlobalData = NSMutableArray()
        self.tableViewArticalList.reloadData()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetArticalList), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        
        refreshControl.endRefreshing()
    }
    
    

    
    
    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        if viewTouched!.isKind(of: UIButton.self){
        }
        else{
            self.view.endEditing(true)
        }
    }

    
    
    func addLoadingIndicatiorOnFooterOnTableView(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableViewArticalList.tableFooterView = spinner
    }
    func removeLoadingIndicatiorOnFooterOnTableView(){
        tableViewArticalList.tableFooterView = nil
    }
    
    func notificationFromBookmarkCalled(notification: NSNotification) {
        
        if USERDEFAULT.value(forKey: "filterData") != nil{
            filterData = USERDEFAULT.value(forKey: "filterData") as! NSDictionary
            
            if let articalCatData = filterData.value(forKey: "articalCatData") as? NSArray{
                let atrticalCatData1 = articalCatData
                print("atrtical Category Data :",atrticalCatData1)
                lblFilterCount.isHidden = false
                lblFilterCount.text = "1"
            }
            else{
                lblFilterCount.isHidden = true
                lblFilterCount.text = ""
            }
            
            lblFilterBy.textColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
            imageViewFilter.image = UIImage.init(named: "filter_blue.png")

            isFilterData = true
            
            
        }
        else{
            lblFilterCount.isHidden = true
            lblFilterCount.text = ""
            
            lblFilterBy.textColor = UIColor.init(red: 108.0/255.0, green: 110.0/255.0, blue: 112.0/255.0, alpha: 1.0)
            imageViewFilter.image = UIImage.init(named: "filter_gray.png")

            isFilterData = false
        }

        
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.articalData = NSMutableArray()
        self.articalGlobalData = NSMutableArray()
        tableViewArticalList.reloadData()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetArticalList), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }

        
    }
    
    func filterArticalDataCalled(notification: NSNotification) {
       
        print("FilterData :",USERDEFAULT.value(forKey: "filterData") ?? "NOOO")
        
        
        if USERDEFAULT.value(forKey: "filterData") != nil{
            filterData = USERDEFAULT.value(forKey: "filterData") as! NSDictionary
            
            if let articalCatData = filterData.value(forKey: "articalCatData") as? NSArray{
                let atrticalCatData1 = articalCatData
                print("atrtical Category Data :",atrticalCatData1)
                lblFilterCount.isHidden = false
                lblFilterCount.text = "1"
            }
            else{
                lblFilterCount.isHidden = true
                lblFilterCount.text = ""
            }
            
            lblFilterBy.textColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
            imageViewFilter.image = UIImage.init(named: "filter_blue.png")
            isFilterData = true
        }
        else{
            lblFilterBy.textColor = UIColor.init(red: 108.0/255.0, green: 110.0/255.0, blue: 112.0/255.0, alpha: 1.0)
            imageViewFilter.image = UIImage.init(named: "filter_gray.png")

            lblFilterCount.isHidden = true
            lblFilterCount.text = ""

            isFilterData = false
        }
        

        self.articalData = NSMutableArray()
        self.articalGlobalData = NSMutableArray()
        self.tableViewArticalList.reloadData()
        
        
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetArticalList), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }

        
    }

    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    
    func setLocalizationText(){
        
        lblHeaderArtical.text = NSLocalizedString("artical", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblFilterBy.text = NSLocalizedString("filterby", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblNoArtical.text = NSLocalizedString("noartical", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
    }
    
    
    
    // TODO: - Textfield DELEGATE METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        return false
    }

    
    
    // TODO: - DELEGATE ScrollView
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
          if scrollView == tableViewArticalList {
            if isLoading == true{
                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                    pageNum = pageNum + 1
                    print(pageNum)
                    isLoading = false
                    
                    if Reachability.isConnectedToNetwork() == true {
                        self.addLoadingIndicatiorOnFooterOnTableView()
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetArticalList), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
            }
            
        }
        
        
    }
    
    
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }

    
    
    // TODO: - DELEGATE METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.articalData.count == 0 {
            return 0
        }
        return self.articalData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let identifier = "articalCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ArticalTableVC
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("ArticalTableVC", owner: self, options: nil)
                cell = nib?[0] as? ArticalTableVC
            }
            cell!.selectionStyle = .none;
        
        
        if let articalName =  (self.articalData[indexPath.row] as AnyObject).value(forKey: "article_name") as? String
        {
            cell?.lblArticalName.text = "\(articalName)"
        }

        if let categoryData =  (self.articalData[indexPath.row] as AnyObject).value(forKey: "category") as? NSArray
        {
            let catData = categoryData
            if catData.count > 0 {
                cell?.lblCategory.isHidden = false
                let mutableStirng = NSMutableString()

                for i in 0...catData.count - 1 {
                    if i != 0{
                        mutableStirng.append(" , ")
                    }
                    mutableStirng.append((catData.object(at: i) as AnyObject).value(forKey: "ac_name") as! String)
                }
                cell?.lblCategory.text = mutableStirng as String
                
               
                cell?.lblCategory.sizeToFit()

                print("CategoryFrame : ",cell?.lblCategory.frame ?? "123")
                
//                cell?.lblCategory.frame = CGRect(x: (cell?.lblCategory.frame.origin.x)!, y: ((cell?.imageViewArtical.frame.origin.y)! + (cell?.imageViewArtical.frame.size.height)! + 10.0), width: (cell?.lblCategory.frame.size.width)! + 10, height: (cell?.lblCategory.frame.size.height)! + 10.0)
                cell?.lblCategory.textAlignment = .center
                cell?.lblCategory.clipsToBounds = true
                cell?.lblCategory.layer.cornerRadius = 5.0
                //(catData.object(at: 0) as AnyObject).value(forKey: "ac_name") as? String
            }
            else{
             cell?.lblCategory.isHidden = true
            }
            
        }
        else{
             cell?.lblCategory.isHidden = true
        }
        
        
        
        if let favour =  (self.articalData[indexPath.row] as AnyObject).value(forKey: "is_favourite") as? NSNumber
        {
            let favourateArtical = "\(favour)"
            if favourateArtical == "1"{
                cell?.imageViewBookmarked.image = UIImage.init(named: "bookmark_green.png")
            }
            else if favourateArtical == "0"{
                cell?.imageViewBookmarked.image = UIImage.init(named: "bookmark_gray.png")
            }
        }
        else if let favour =  (self.articalData[indexPath.row] as AnyObject).value(forKey: "is_favourite") as? String
        {
            let favourateArtical = "\(favour)"
            if favourateArtical == "1"{
                cell?.imageViewBookmarked.image = UIImage.init(named: "bookmark_green.png")
            }
            else if favourateArtical == "0"{
                cell?.imageViewBookmarked.image = UIImage.init(named: "bookmark_gray.png")
            }
        }
        
        
        cell?.btnBookMark.tag = indexPath.row
        cell?.btnShare.tag = indexPath.row
        
        cell?.btnBookMark.addTarget(self, action: #selector(self.btnBookmarkClicked(_:)), for: .touchUpInside)
        cell?.btnShare.addTarget(self, action: #selector(self.btnSharingClicked(_:)), for: .touchUpInside)
        
        
        
        
        let imageUrl = (self.articalData[indexPath.row] as AnyObject).value(forKey: "article_img") as? String
        let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl!) as String
        let url : NSString = fullUrl as NSString
        let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
        let searchURL : NSURL = NSURL(string: urlStr as String)!
        
        
        cell?.activityIndicatorForArticalImage.isHidden = false
        cell?.activityIndicatorForArticalImage.startAnimating()
        
        cell?.imageViewArtical.sd_setImage(with: searchURL as URL, completed: { (image:UIImage?, error:Error?, cacheType:SDImageCacheType!, imageURL:URL?) in
            
            if ((error) != nil) {
                cell?.imageViewArtical.image = UIImage.init(named: "image_placeholder.png")
            }
            
            cell?.activityIndicatorForArticalImage.isHidden = true
            
        })
            
        
            return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let articalDetailVC = ArticalDetailVC(nibName: "ArticalDetailVC", bundle: nil)
        articalDetailVC.articalID = (self.articalData[indexPath.row] as AnyObject).value(forKey: "article_id") as? String
        self.navigationController?.pushViewController(articalDetailVC, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 270
    }
    
    // TODO: - ACTION METHODS
    @IBAction func btnBookmarkClicked(_ sender:UIButton){
        
        if USERDEFAULT.value(forKey: "userID") != nil{
            let ifFav = (self.articalData[sender.tag] as AnyObject).value(forKey: "is_favourite") as? String
            articalID = (self.articalData[sender.tag] as AnyObject).value(forKey: "article_id") as? String
            
            if ifFav == "1"{
                let temp = (self.articalData[sender.tag] as! NSDictionary).mutableCopy() as! NSMutableDictionary
                print(temp)
                temp.setValue("0", forKey: "is_favourite")
                
                if (self.articalData.count > sender.tag) {
                    self.articalData.replaceObject(at: sender.tag, with: temp)
                    self.articalGlobalData.replaceObject(at: sender.tag, with: temp)
                    let indexPath = NSIndexPath(row: sender.tag, section: 0)
                    tableViewArticalList.reloadRows(at: [indexPath as IndexPath], with: .none)
                }
                
                
                if Reachability.isConnectedToNetwork() == true {
                    self.performSelector(inBackground: #selector(self.postDataOnWebserviceForRemoveFromBookMark), with: nil)
                }else{
                    showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                }
                
            }
            else{
                let temp = (self.articalData[sender.tag] as! NSDictionary).mutableCopy() as! NSMutableDictionary
                print(temp)
                temp.setValue("1", forKey: "is_favourite")
                
                if (self.articalData.count > sender.tag) {
                    self.articalData.replaceObject(at: sender.tag, with: temp)
                    self.articalGlobalData.replaceObject(at: sender.tag, with: temp)
                    let indexPath = NSIndexPath(row: sender.tag, section: 0)
                    tableViewArticalList.reloadRows(at: [indexPath as IndexPath], with: .none)
                }
                
                if Reachability.isConnectedToNetwork() == true {
                    self.performSelector(inBackground: #selector(self.postDataOnWebserviceForAddToBookMark), with: nil)
                }else{
                    showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                }
                
            }
            
        }
        else{
            let ifFav = (self.articalData[sender.tag] as AnyObject).value(forKey: "is_favourite") as? String
            articalID = (self.articalData[sender.tag] as AnyObject).value(forKey: "article_id") as? String
            
            if ifFav == "1"{
                let temp = (self.articalData[sender.tag] as! NSDictionary).mutableCopy() as! NSMutableDictionary
                print(temp)
                temp.setValue("0", forKey: "is_favourite")
                
                if (self.articalData.count > sender.tag) {
                    self.articalData.replaceObject(at: sender.tag, with: temp)
                    self.articalGlobalData.replaceObject(at: sender.tag, with: temp)
                    let indexPath = NSIndexPath(row: sender.tag, section: 0)
                    tableViewArticalList.reloadRows(at: [indexPath as IndexPath], with: .none)
                }
                
            }
            else{
                let temp = (self.articalData[sender.tag] as! NSDictionary).mutableCopy() as! NSMutableDictionary
                print(temp)
                temp.setValue("1", forKey: "is_favourite")
                
                if (self.articalData.count > sender.tag) {
                    self.articalData.replaceObject(at: sender.tag, with: temp)
                    self.articalGlobalData.replaceObject(at: sender.tag, with: temp)
                    let indexPath = NSIndexPath(row: sender.tag, section: 0)
                    tableViewArticalList.reloadRows(at: [indexPath as IndexPath], with: .none)
                }
            }
            
            USERDEFAULT.setValue(self.articalData, forKey: "offlineBookMarkArticalData")
            USERDEFAULT.synchronize()
        }
        
       
    }
    
    @IBAction func btnBackClicked(_ sender:UIButton){
        _ =  self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBookMarkClicked(_ sender:UIButton){
        let bookMarkArticalVC = BookmarkArticalVC(nibName: "BookmarkArticalVC", bundle: nil)
        self.navigationController?.pushViewController(bookMarkArticalVC, animated: true)
    }
    
    @IBAction func btnFilterClicked(_ sender:UIButton){
        let articalFilterVC = ArticalFilterVC(nibName: "ArticalFilterVC", bundle: nil)
        self.navigationController?.pushViewController(articalFilterVC, animated: true)
    }

    @IBAction func btnSearchClicked(_ sender:UIButton){
        txtSearch.becomeFirstResponder()
        
        UIView.animate(withDuration: 0.3) {
            
            self.viewSearchArtical.frame = CGRect(x: self.headerView.frame.origin.x, y: self.headerView.frame.origin.y, width: self.viewSearchArtical.frame.size.width, height: self.viewSearchArtical.frame.size.height)
            
        }
        
    }
    
    @IBAction func btnSearchCancelClicked(_ sender:UIButton){
        txtSearch.text = ""
        txtSearch.resignFirstResponder()
        
        UIView.animate(withDuration: 0.3) {
            
            self.viewSearchArtical.frame = CGRect(x: self.headerView.frame.origin.x+self.headerView.frame.size.width, y: self.headerView.frame.origin.y, width: self.viewSearchArtical.frame.size.width, height: self.viewSearchArtical.frame.size.height)
            
        }
        
        
        articalData = articalGlobalData.mutableCopy() as! NSMutableArray
        if articalData.count == 0 {
            lblNoArtical.isHidden = false
            tableViewArticalList.isHidden = true
        }
        else{
            tableViewArticalList.isHidden = false
            lblNoArtical.isHidden = true
        }
        tableViewArticalList.reloadData()
    }
    
    @IBAction func btnSharingClicked(_ sender:UIButton){
        
        if USERDEFAULT.value(forKey: "userID") != nil{
            // text to share
            let text = (self.articalData[sender.tag] as AnyObject).value(forKey: "article_name") as! String
            
            //        let imageUrl = (self.articalData[sender.tag] as AnyObject).value(forKey: "article_img") as! String
            //        let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl) as String
            
            let indexpath = IndexPath(row: sender.tag, section: 0)
            let cell = tableViewArticalList.cellForRow(at: indexpath) as! ArticalTableVC
            
            
            let  activityViewController = UIActivityViewController(activityItems: [self,text,cell.imageViewArtical.image!], applicationActivities: [])
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            
            // exclude some activity types from the list (optional)
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
        }
        else{
            // text to share
            
            let indexpath = IndexPath(row: sender.tag, section: 0)
            let cell = tableViewArticalList.cellForRow(at: indexpath) as! ArticalTableVC
            
            
            let  activityViewController = UIActivityViewController(activityItems: [self,cell.lblArticalName.text!,cell.imageViewArtical.image!], applicationActivities: [])
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            
            // exclude some activity types from the list (optional)
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
        }
        
        
        
        
    }
    
    
    
    @IBAction func searchFilter(){
        var predicate = String()

        
        if (txtSearch.text == "") {
            articalData = articalGlobalData.mutableCopy() as! NSMutableArray
        }else{
            predicate = String(format: "SELF['article_name'] contains[c] '%@'", txtSearch.text!)
            print(predicate)
            
            let myPredicate:NSPredicate = NSPredicate(format:predicate)
            let temp = self.articalGlobalData.mutableCopy() as! NSArray
            self.articalData = (temp.filtered(using: myPredicate) as NSArray).mutableCopy() as! NSMutableArray
            
            
        }
        
        if articalData.count == 0 {
            lblNoArtical.isHidden = false
            tableViewArticalList.isHidden = true
        }
        else{
            tableViewArticalList.isHidden = false
            lblNoArtical.isHidden = true
        }
        tableViewArticalList.reloadData()

    }
    
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForGetArticalList(){
        let completeURL = NSString(format:"%@%@", MainURL,getArticalListURL) as String
        
        let pageNumber = "\(pageNum!)"

         var params:NSDictionary!
        
        if USERDEFAULT.value(forKey: "userID") != nil{
            if self.isFilterData == true{
                // params = [
                //     "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
                //     "token": USERDEFAULT.value(forKey: "token") as! String,
                //     "lang_type":"1",
                //     "page":pageNumber,
                //      "limit":PAGINATION_LIMITE,
                //      "ac_id":filterIDDataString
                //  ]
                
                let parameter = NSMutableDictionary()
                parameter.setValue(USERDEFAULT.value(forKey: "userID") as! String, forKey: "user_id")
                parameter.setValue(USERDEFAULT.value(forKey: "token") as! String, forKey: "token")
                parameter.setValue(Language_Type, forKey: "lang_type")
                parameter.setValue(pageNumber, forKey: "page")
                parameter.setValue(PAGINATION_LIMITE, forKey: "limit")
                
                var filterIDDataString:String = ""
                let filterIDDataArray = filterData.value(forKey: "articalCatData") as! NSArray
                if filterIDDataArray.count > 0 {
                    filterIDDataString = filterIDDataArray.componentsJoined(by: ",")
                }
                
                if filterIDDataString != "" {
                    parameter.setValue(filterIDDataString, forKey: "ac_id")
                }
                
                
                params = parameter.mutableCopy() as! NSDictionary
                
            }
            else{
                params = [
                    "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
                    "token": USERDEFAULT.value(forKey: "token") ?? "",
                    "lang_type":Language_Type,
                    "page":pageNumber,
                    "limit":PAGINATION_LIMITE
                ]
            }
        }
        else{
            if self.isFilterData == true{
                // params = [
                //     "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
                //     "token": USERDEFAULT.value(forKey: "token") as! String,
                //     "lang_type":"1",
                //     "page":pageNumber,
                //      "limit":PAGINATION_LIMITE,
                //      "ac_id":filterIDDataString
                //  ]
                
                let parameter = NSMutableDictionary()
                
                parameter.setValue(Language_Type, forKey: "lang_type")
                parameter.setValue(pageNumber, forKey: "page")
                parameter.setValue(PAGINATION_LIMITE, forKey: "limit")
                
                var filterIDDataString:String = ""
                let filterIDDataArray = filterData.value(forKey: "articalCatData") as! NSArray
                if filterIDDataArray.count > 0 {
                    filterIDDataString = filterIDDataArray.componentsJoined(by: ",")
                }
                
                if filterIDDataString != "" {
                    parameter.setValue(filterIDDataString, forKey: "ac_id")
                }
                
                
                params = parameter.mutableCopy() as! NSDictionary
                
            }
            else{
                params = [
                    "lang_type":Language_Type,
                    "page":pageNumber,
                    "limit":PAGINATION_LIMITE
                ]
                
            }
        }
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetFavourtiteStore API Parameter :",finalParams)
        print("GetFavourtiteStore API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getArticalListURLTag)
        
    }
    
    func postDataOnWebserviceForAddToBookMark(){
        let completeURL = NSString(format:"%@%@", MainURL,addArticalBookMarkURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "article_id":articalID
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("AddToBookMark API Parameter :",finalParams)
        print("AddToBookMark API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: addArticalBookMarkURLTag)
    }
    
    func postDataOnWebserviceForRemoveFromBookMark(){
        let completeURL = NSString(format:"%@%@", MainURL,removeArticalBookMarkURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "article_id":articalID
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("AddToBookMark API Parameter :",finalParams)
        print("AddToBookMark API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: removeArticalBookMarkURLTag)
    }

    

    
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case getArticalListURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("GetArtical List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                var myData = NSArray()
                
                if self.pageNum == 1{
                    self.articalData = NSMutableArray()
                    self.articalGlobalData = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        self.articalGlobalData.add(myData[i])
                    }
                    
                    for i in 0...myData.count - 1 {
                        self.articalData.add(myData[i])
                    }
                    
                    print("My Article Data : ",myData)
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum > 0) {
                            self.pageNum = self.pageNum - 1
                        }
                        self.isLoading = false
                    }else{
                        self.isLoading = true
                    }
                }
                else{
                    self.isLoading = false
                    if (self.pageNum > 0) {
                        self.pageNum = self.pageNum - 1
                    }
                    
                }
                
                if self.articalData.count == 0{
                    self.tableViewArticalList.isHidden = true
                    self.lblNoArtical.isHidden = false
                }
                else{
                    self.tableViewArticalList.isHidden = false
                    self.lblNoArtical.isHidden = true
                }

                self.tableViewArticalList.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
                
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            break
            
            
        case addArticalBookMarkURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("Add Bookmark Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        case removeArticalBookMarkURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("Remove Bookmark Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            break
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case getArticalListURLTag:
            self.isLoading = false
            if (self.pageNum > 0) {
                self.pageNum = self.pageNum - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
        case addArticalBookMarkURLTag:
            break
        case removeArticalBookMarkURLTag:
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    

}
