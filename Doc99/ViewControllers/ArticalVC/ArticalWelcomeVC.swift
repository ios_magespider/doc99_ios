//
//  ArticalWelcomeVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 31/05/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD

class ArticalWelcomeVC: UIViewController,UIAlertViewDelegate,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var viewWell1:UIView!
    @IBOutlet var viewWell2:UIView!
    @IBOutlet var viewWell3:UIView!
    @IBOutlet var viewWell4:UIView!
    @IBOutlet var viewWell5:UIView!
    
    
    @IBOutlet var lblWell1:UILabel!
    @IBOutlet var lblWell2:UILabel!
    @IBOutlet var lblWell3:UILabel!
    @IBOutlet var lblWell4:UILabel!
    @IBOutlet var lblWell5:UILabel!
    
    @IBOutlet var btnNext:UIButton!
    @IBOutlet var btnPrevious:UIButton!
    @IBOutlet var btnSkip:UIButton!

    @IBOutlet var scrView:UIScrollView!
    
    @IBOutlet var collectionViewSaleCounter:UICollectionView!
    let reuseIdentifier = "articalWelcomeCell"

    
    //FOR SCROLLING VIEW
    var currentPag:Int!
    var pageCount:Int!
    let screenWidth = Int(ScreenSize.SCREEN_WIDTH)
    
    @IBOutlet var tableViewHealthCategory:UITableView!
    
    
    //For Pasignation
    var pageNum:Int!
    var isLoading:Bool?
    
    var healthData = NSMutableArray()
    var healthGlobalData = NSMutableArray()
    
    var boolArray:NSMutableArray!
    
    var healthCategory:NSMutableArray!
    
    var healthConcernData = NSArray()
    
    var refreshControl: UIRefreshControl!

    // FIXME: - VIEW CONTROLLER METHODS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewSaleCounter.reloadData()
        
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.healthData = NSMutableArray()
        self.healthGlobalData = NSMutableArray()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetHealthConcerns), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
//        if Reachability.isConnectedToNetwork() == true {
//            self.performSelector(inBackground: #selector(self.postDataOnWebserviceForGetProfile), with: nil)
//        } else {
//            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
//        }
        
        self.addPullRefresh()
        
        
        self.generalViewControllerSetting()
        
        // Define identifier
        let notificationName5 = Notification.Name("updateHealthConcernDataNotification")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateHealthConcernData), name: notificationName5, object: nil)
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
    }
    

    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        
        self.collectionViewSaleCounter.register(UINib(nibName: "ArticalWelcomeCollectionVC", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)

        pageCount=5;
        currentPag=0;
        btnPrevious.isEnabled = false
        
        let scrHeight = ScreenSize.SCREEN_HEIGHT-79
        
        scrView.contentSize = CGSize(width: CGFloat(screenWidth * pageCount), height: scrView.frame.size.height)
        
        viewWell1.frame = CGRect(x: 0.0, y: viewWell1.frame.origin.y, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT-79)
        scrView.addSubview(viewWell1)
        
        viewWell2.frame = CGRect(x: viewWell1.frame.origin.x+viewWell1.frame.size.width, y: viewWell2.frame.origin.y, width: ScreenSize.SCREEN_WIDTH, height: scrHeight)
        scrView.addSubview(viewWell2)
        
        viewWell3.frame = CGRect(x: viewWell2.frame.origin.x+viewWell2.frame.size.width, y: viewWell3.frame.origin.y, width: ScreenSize.SCREEN_WIDTH, height: scrHeight)
        scrView.addSubview(viewWell3)

        viewWell4.frame = CGRect(x: viewWell3.frame.origin.x+viewWell3.frame.size.width, y: viewWell4.frame.origin.y, width: ScreenSize.SCREEN_WIDTH, height: scrHeight)
        scrView.addSubview(viewWell4)

        viewWell5.frame = CGRect(x: viewWell4.frame.origin.x+viewWell4.frame.size.width, y: viewWell5.frame.origin.y, width: ScreenSize.SCREEN_WIDTH, height: scrHeight)
        scrView.addSubview(viewWell5)
    }
    
    func updateHealthConcernData(){
        
        self.healthData = NSMutableArray()
        self.healthGlobalData = NSMutableArray()
        if Reachability.isConnectedToNetwork() == true {
            //            self.addLoadingIndicatiorOnFooterOnTableView()
            //            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetHealthConcerns), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
    }

    
    
    func addPullRefresh(){
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.gray
        refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: .valueChanged)
        tableViewHealthCategory.addSubview(refreshControl)
    }

    
    func refresh(sender:AnyObject) {
        // Code to refresh table view
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.healthData = NSMutableArray()
        self.healthGlobalData = NSMutableArray()
        self.tableViewHealthCategory.reloadData()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetHealthConcerns), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
//        if Reachability.isConnectedToNetwork() == true {
//            self.performSelector(inBackground: #selector(self.postDataOnWebserviceForGetProfile), with: nil)
//        } else {
//            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
//        }
        
        
        
        refreshControl.endRefreshing()
    }
    
    func addLoadingIndicatiorOnFooterOnTableView(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableViewHealthCategory.tableFooterView = spinner
    }
    func removeLoadingIndicatiorOnFooterOnTableView(){
        tableViewHealthCategory.tableFooterView = nil
    }
    
    
    func settingBtnBgColor(_ sender:UIView){
        
        if sender == viewWell1{
            lblWell1.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
            lblWell2.backgroundColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            lblWell3.backgroundColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            lblWell4.backgroundColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            lblWell5.backgroundColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        }
        else if sender == viewWell2{
            lblWell2.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
            lblWell1.backgroundColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            lblWell3.backgroundColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            lblWell4.backgroundColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            lblWell5.backgroundColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        }
        else if sender == viewWell3{
            lblWell3.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
            lblWell2.backgroundColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            lblWell1.backgroundColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            lblWell4.backgroundColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            lblWell5.backgroundColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        
        }
        else if sender == viewWell4{
            lblWell4.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
            lblWell2.backgroundColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            lblWell3.backgroundColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            lblWell1.backgroundColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            lblWell5.backgroundColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        }
        else if sender == viewWell5{
            lblWell5.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
            lblWell2.backgroundColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            lblWell3.backgroundColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            lblWell4.backgroundColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            lblWell1.backgroundColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        }
        
    }
    
//    func addLoadingIndicatiorOnFooterOnTableView(){
//        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
//        spinner.startAnimating()
//        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
//        tableViewHealthCategory.tableFooterView = spinner
//    }
//    func removeLoadingIndicatiorOnFooterOnTableView(){
//        tableViewHealthCategory.tableFooterView = nil
//    }
    
    
    // TODO: - DELEGATE METHODS
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }

    
    
    func setupScrollViewWithCurrentPage(currentPage:Int){
        scrView.contentOffset = CGPoint(x: currentPag * screenWidth, y: 0)
    }
    
    //ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.tag == 50) {
            currentPag = Int(scrollView.contentOffset.x) / Int(scrollView.frame.size.width);
            
            if (currentPag == 0) {
                self.settingBtnBgColor(viewWell1)
            }
            else if (currentPag == 1){
                self.settingBtnBgColor(viewWell2)
            }
            else if (currentPag == 2){
                self.settingBtnBgColor(viewWell3)
            }
            else if (currentPag == 3){
                self.settingBtnBgColor(viewWell4)
            }
            else if (currentPag == 4){
                self.settingBtnBgColor(viewWell5)
            }
            if(currentPag != 4)
            {
                btnNext.setTitle("Next", for: .normal)
                btnNext.tag = 101
                btnSkip.isHidden = false
            }
            else
            {
                btnNext.setTitle("Finish", for: .normal)
                btnNext.tag = 100
                btnSkip.isHidden = true
                
            }
            scrView.contentOffset = CGPoint(x: scrView.contentOffset.x, y: 0.0)
        }
//        else if scrollView == tableViewHealthCategory {
//            if isLoading == true{
//                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
//                    pageNum = pageNum + 1
//                    print(pageNum)
//                    isLoading = false
//                    
//                    if Reachability.isConnectedToNetwork() == true {
//                        self.addLoadingIndicatiorOnFooterOnTableView()
//                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetDrugDisease), userInfo: nil, repeats: false)
//                    } else {
//                        showAlert("Check Connection", title: "Internet is not available.")
//                    }
//                    
//                }
//            }
//            
//        }
        

    }
    
    
//    // MARK: - UICollectionViewDataSource protocol
//    
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return self.healthData.count
//    }
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ArticalWelcomeCollectionVC
//        cell.lblName.text = (self.healthData.object(at: indexPath.row) as AnyObject).value(forKey: "dd_name") as? String
//      
//        
////        
////        if ((self.salesCounterData.object(at: indexPath.row) as AnyObject).value(forKey: "image") as? String == "")
////        {
////            cell.imageViewSalesCounter.image = UIImage.init(named: "image_placeholder.png")
//////            cell.activityIndicatorForCategory.isHidden = true
////            
////        }
////        else{
////            let imageUrl = (self.salesCounterData.object(at: indexPath.row) as AnyObject).value(forKey: "image") as? String
////            
////            let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl!) as String
////            let url : NSString = fullUrl as NSString
////            let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
////            let searchURL : NSURL = NSURL(string: urlStr as String)!
////            
//////            cell.activityIndicatorForCategory.isHidden = false
//////            cell.activityIndicatorForCategory.startAnimating()
////            
////            
////            cell.imageViewSalesCounter.sd_setImage(with: searchURL as URL, completed: { (image:UIImage?, error:Error?, cacheType:SDImageCacheType!, imageURL:URL?) in
////                
////                if ((error) != nil) {
////                    cell.imageViewSalesCounter.image = UIImage.init(named: "image_placeholder.png")
////                }
////                //cell.activityIndicatorForCategory.isHidden = true
////            })
////            
////        }
//        
//        cell.imageViewSalesCounter.image = UIImage.init(named: "wel4.png")
//        
////        if boolArrayForCategory.object(at: indexPath.row) as! String == "1"{
////            cell.imageViewSelect.image = UIImage.init(named: "check_blue.png")
////        }
////        else{
////            cell.imageViewSelect.image = UIImage.init(named: "check_gray.png")
////        }
//
//        
////        cell.btnSelect.tag = indexPath.row
////        cell.btnSelect.addTarget(self, action: #selector(self.btnDrugCategorySelectionClicked(_:)), for: .touchUpInside)
//        
//        
//        
//        return cell
//    }
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        
//    }
    
    
    // TODO: - DELEGATE METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.healthData.count == 0{
            return 0
        }
        return self.healthData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "healthCategory"
        var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? HealthCategoryTableVC
        
        if cell == nil {
            let nib  = Bundle.main.loadNibNamed("HealthCategoryTableVC", owner: self, options: nil)
            cell = nib?[0] as? HealthCategoryTableVC
        }
        cell!.selectionStyle = .none;
        
        
        if let articalCategoryName =  (self.healthData[indexPath.row] as AnyObject).value(forKey: "hc_name") as? String
        {
            cell?.lblTitle.text = "\(articalCategoryName)"
        }
        
        if let articalCategoryName =  (self.healthData[indexPath.row] as AnyObject).value(forKey: "bodyorgan") as? String
        {
            cell?.lblSubTitle.text = "\(articalCategoryName)"
        }
        
        
        if boolArray.object(at: indexPath.row) as! String == "1"{
            cell?.imageViewSelect.image = UIImage.init(named: "check_blue.png")
        }
        else{
            cell?.imageViewSelect.image = UIImage.init(named: "check_gray.png")
        }
        
        
        if ((self.healthData.object(at: indexPath.row) as AnyObject).value(forKey: "image") as? String == "")
        {
            cell?.imageViewHealthCategory.image = UIImage.init(named: "image_placeholder.png")
            cell?.activityIndicatorForHealth.isHidden = true
            
        }
        else{
            let imageUrl = (self.healthData.object(at: indexPath.row) as AnyObject).value(forKey: "image") as? String
            
            let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl!) as String
            let url : NSString = fullUrl as NSString
            let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
            let searchURL : NSURL = NSURL(string: urlStr as String)!
            
            cell?.activityIndicatorForHealth.isHidden = false
            cell?.activityIndicatorForHealth.startAnimating()
            
            
            cell?.imageViewHealthCategory.sd_setImage(with: searchURL as URL, completed: { (image:UIImage?, error:Error?, cacheType:SDImageCacheType!, imageURL:URL?) in
                
                if ((error) != nil) {
                    cell?.imageViewHealthCategory.image = UIImage.init(named: "image_placeholder.png")
                }
                cell?.activityIndicatorForHealth.isHidden = true
            })
            
        }
        
        
        
        cell?.btnSelect.tag = indexPath.row
        cell?.btnSelect.addTarget(self, action: #selector(self.btnCategorySelectionClicked(_:)), for: .touchUpInside)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    
    
    
    // TODO: - ACTION METHODS
    
    @IBAction func btnCategorySelectionClicked(_ sender:UIButton){
        
        boolArray = NSMutableArray()
        for _ in 0...healthData.count - 1 {
            boolArray.add("0")
        }
        boolArray.replaceObject(at: sender.tag, with: "1")
        
        tableViewHealthCategory.reloadData()
        
        
        /*
        if boolArray.object(at: sender.tag) as! String == "0"{
            boolArray.replaceObject(at: sender.tag, with: "1")
        }
        else{
            boolArray.replaceObject(at: sender.tag, with: "0")
        }
        */
        
//        let indexPath = NSIndexPath(row: sender.tag, section: 0)
//        tableViewHealthCategory.reloadRows(at: [indexPath as IndexPath], with: .none)
        
        healthCategory = NSMutableArray.init()
        for i in 0...healthData.count - 1 {
            if boolArray.object(at: i) as! String == "1"{
                healthCategory.add((self.healthData[i] as AnyObject).value(forKey: "hc_id") as! String)
            }
        }
        
        print("Health Category",healthCategory)
    }
    
    
    @IBAction func btnNextClicked(_ sender:UIButton){

        if currentPag < 4{
            
            currentPag=currentPag+1
            print("Current Page : ",currentPag)
            print("Page Count: ",pageCount)
            
            self.setupScrollViewWithCurrentPage(currentPage: currentPag)
        }else{
            let joinDrugVC = JoinVC(nibName: "JoinVC", bundle: nil)
            self.navigationController?.pushViewController(joinDrugVC, animated: true)
        }
       
       

    }
    
    @IBAction func btnPreviousClicked(_ sender:UIButton){
        
        btnNext.isEnabled = true
        
        if (currentPag==1) {
            btnPrevious.isEnabled = false
        }
        currentPag=currentPag-1
        
        if currentPag != 4{
            btnNext.setTitle("Next", for: .normal)
            btnNext.tag = 101

        }
        
        print("Current Page : ",currentPag)
        print("Page Count: ",pageCount)

        self.setupScrollViewWithCurrentPage(currentPage: currentPag)
        
    }
    
    @IBAction func btnSkipClicked(_ sender:UIButton){
//        USERDEFAULT.set("NO", forKey: "isFirstTime")
//        USERDEFAULT.synchronize()
        
//        let homeVC = TabBarVC(nibName: "TabBarVC", bundle: nil)
//        homeVC.selectedIndexOfMyTabbarController = 0
//        self.navigationController?.pushViewController(homeVC, animated: true)
        let joinDrugVC = JoinVC(nibName: "JoinVC", bundle: nil)
        self.navigationController?.pushViewController(joinDrugVC, animated: true)
        
        
    }
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForGetHealthConcerns(){
        
        let completeURL = NSString(format:"%@%@", MainURL,healthConcernURL) as String
        
        //         let pageNumber = "\(pageNum!)"
        
        let params:NSDictionary = [
            // "user_id" : "49",
            // "token": "ICp48GEVMOBwzHOUu1KJN2xJ2PxsmCSa1jDfTGpERXP6s7pbFH2ZpPvg4PpuJSWX5uCYy701DfKNHHeacneTpCMxU3C0JEvlT5CFjVqbv63FXjlOUZoQ6jsW",
            //             "page":pageNumber,
            //             "limit":PAGINATION_LIMITE,
            "lang_type":Language_Type
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetArticalCategory API Parameter :",finalParams)
        print("GetArticalCategory API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: healthConcernURLTag)
        
        
        
    }
    
    func postDataOnWebserviceForGetProfile(){
        let completeURL = NSString(format:"%@%@", MainURL,getUserProfileURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetProfile API Parameter :",finalParams)
        print("GetProfile API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getUserProfileURLTag)
    }
    
    func postDataOnWebserviceForUpdateProfile(){
        let completeURL = NSString(format:"%@%@", MainURL,editUserProfileURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "health_concern":healthCategory,
            ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("UpdateProfile API Parameter :",finalParams)
        print("UpdateProfile API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: editUserProfileURLTag)
    }
    
    
    
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case healthConcernURLTag:
            let resultDict = responseObject as! NSDictionary;
//            print("getDrugDiseaseURLTag List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum == 1{
                    self.healthData = NSMutableArray()
                    self.healthGlobalData = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        self.healthGlobalData.add(myData[i])
                    }
                    
                    for i in 0...myData.count - 1 {
                        self.healthData.add(myData[i])
                    }
                    
                    boolArray = NSMutableArray.init()
                    
                    for _ in 0...healthData.count - 1 {
                        boolArray.add("0")
                    }
                    
                    
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum > 0) {
                            self.pageNum = self.pageNum - 1
                        }
                        self.isLoading = false
                    }else{
                        self.isLoading = true
                    }
                }
                else{
                    self.isLoading = false
                    if (self.pageNum > 0) {
                        self.pageNum = self.pageNum - 1
                    }
                    
                    if self.healthData.count == 0{
                        self.tableViewHealthCategory.isHidden = true
                        //self.lblNoArtical.isHidden = false
                    }
                    else{
                        self.tableViewHealthCategory.isHidden = false
                        //self.lblNoArtical.isHidden = true
                    }
                }
                self.tableViewHealthCategory.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
                
                if let healthData = USERDEFAULT.value(forKey: "userHealthConcernData") as? NSArray{
                    healthConcernData = healthData.mutableCopy() as! NSArray
                    print("USERDEFAULT HEALTH CONCERN DATA : ",healthConcernData)
                }
                
                self.setHealthConcernData()
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        case getUserProfileURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("GETProfile Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                healthConcernData = (resultDict.value(forKey: "data") as! NSDictionary).value(forKey: "userhealth_concern") as! NSArray
                self.setHealthConcernData()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            //   SVProgressHUD.dismiss()
            break
            
        case editUserProfileURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("UpdateProfile Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                if let fullname = (resultDict.value(forKey: "data") as! NSDictionary).value(forKey: "fullname") as? String{
                    let name = "\(fullname)"
                    USERDEFAULT.set(name, forKey: "fullName")
                    USERDEFAULT.synchronize()
                    let notificationName = Notification.Name("updateProfileDataNotification")
                    NotificationCenter.default.post(name: notificationName, object: nil)
                    
                }
                
                let notificationName = Notification.Name("updateHealthConcernDataNotification")
                NotificationCenter.default.post(name: notificationName, object: nil)
                
                let joinDrugVC = JoinVC(nibName: "JoinVC", bundle: nil)
                self.navigationController?.pushViewController(joinDrugVC, animated: true)

                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            SVProgressHUD.dismiss()
            break
            
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case healthConcernURLTag:
            self.isLoading = false
            if (self.pageNum > 0) {
                self.pageNum = self.pageNum - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
            
        case getUserProfileURLTag:
            //  SVProgressHUD.dismiss()
            break
            
        case editUserProfileURLTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    // TODO: - OTHERS METHODS
    func setHealthConcernData() {
        let temp = NSMutableArray()
        
        if healthData.count > 0{
            for i in 0..<self.healthData.count{
                let id = (self.healthData[i] as AnyObject).value(forKey: "hc_id") as! String
                temp.add("\(id)")
            }
            print(temp)
        }
        
        if healthConcernData.count > 0{
            if let abc = healthConcernData as? NSArray
            {
                for i in 0..<abc.count{
                    let myIndexValue:Int = temp.index(of: "\(abc[i])")
                    if self.healthData.count > myIndexValue{
                        self.boolArray.replaceObject(at: myIndexValue, with: "1")
                        let path = NSIndexPath(row: myIndexValue, section: 0)
                        tableViewHealthCategory.reloadRows(at: [path as IndexPath], with:.none)
                    }
                }
            }
            USERDEFAULT.setValue(healthConcernData, forKey: "userHealthConcernData")
            USERDEFAULT.synchronize()

        }
        
        
        if healthData.count > 0{
            healthCategory = NSMutableArray.init()
            for i in 0...healthData.count - 1 {
                if boolArray.object(at: i) as! String == "1"{
                    healthCategory.add((self.healthData[i] as AnyObject).value(forKey: "hc_id") as! String)
                }
            }
            
            print("Selected Health Concern Data : ",healthCategory)
            USERDEFAULT.setValue(healthCategory, forKey: "userHealthCategoryData")
            USERDEFAULT.synchronize()
            
        }
        
     
        

        
    }
    

    

}
