//
//  ArticalWelcomeCollectionVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 31/05/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class ArticalWelcomeCollectionVC: UICollectionViewCell {

    @IBOutlet var imageViewSelect:UIImageView!
    @IBOutlet var lblName:UILabel!
    @IBOutlet var btnSelect:UIButton!
    @IBOutlet var imageViewSalesCounter:UIImageView!
    @IBOutlet var activityIndicatorForHealth:UIActivityIndicatorView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
