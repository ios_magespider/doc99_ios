//
//  ArticalFilterVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 13/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class ArticalFilterVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate {

    @IBOutlet var tableViewFilterArtical:UITableView!
    @IBOutlet var lblCategoryCount:UILabel!

    //For Pasignation
    var pageNum:Int!
    var isLoading:Bool?
    
    var articalCategoryData = NSMutableArray()
    var articalCategoryGlobalData = NSMutableArray()
    
    var boolArray:NSMutableArray!
    var categoryCount:Int!
    var articalCategory = NSMutableArray()
    var finalFilterData = NSMutableDictionary()

    var alreadyFilterData = NSDictionary()

    @IBOutlet var lblFilterHeader:UILabel!
    @IBOutlet var lblArticalCAtegory:UILabel!
    @IBOutlet var btnReset:UIButton!
    @IBOutlet var btnApply:UIButton!
    
    var refreshControl: UIRefreshControl!



    
    // FIXME: - VIEW CONTROLLER METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.articalCategoryData = NSMutableArray()
        self.articalCategoryGlobalData = NSMutableArray()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetArticalCategory), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }

        self.setLocalizationText()
        self.addPullRefreshForFilterArtical()
    }
    
    func addPullRefreshForFilterArtical(){
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.gray
        refreshControl.addTarget(self, action: #selector(self.refreshFilterArtical(sender:)), for: .valueChanged)
        tableViewFilterArtical.addSubview(refreshControl)
    }
    
    func refreshFilterArtical(sender:AnyObject) {
        // Code to refresh table view
       
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.articalCategoryData = NSMutableArray()
        self.articalCategoryGlobalData = NSMutableArray()
        self.tableViewFilterArtical.reloadData()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetArticalCategory), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }

        refreshControl.endRefreshing()
    }

    
    
    func addLoadingIndicatiorOnFooterOnTableView(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableViewFilterArtical.tableFooterView = spinner
    }
    func removeLoadingIndicatiorOnFooterOnTableView(){
        tableViewFilterArtical.tableFooterView = nil
    }

    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }
    
    func setLocalizationText(){
        
        lblFilterHeader.text = NSLocalizedString("filter", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblArticalCAtegory.text = NSLocalizedString("articalcategory", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
        btnReset.setTitle(NSLocalizedString("reset", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        btnApply.setTitle(NSLocalizedString("apply", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        
    }
    
    
    // TODO: - DELEGATE ScrollView
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == tableViewFilterArtical {
            if isLoading == true{
                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                    pageNum = pageNum + 1
                    print(pageNum)
                    isLoading = false
                    
                    if Reachability.isConnectedToNetwork() == true {
                        self.addLoadingIndicatiorOnFooterOnTableView()
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetArticalCategory), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))

                    }
                }
            }
            
        }
        
        
    }

    
    // TODO: - DELEGATE METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.articalCategoryData.count == 0 {
            return 0
        }
        return self.articalCategoryData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "articalFilterCell"
        var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ArticalFilterTableVC
        
        if cell == nil {
            let nib  = Bundle.main.loadNibNamed("ArticalFilterTableVC", owner: self, options: nil)
            cell = nib?[0] as? ArticalFilterTableVC
        }
        cell!.selectionStyle = .none;
        
        
        if let articalCategoryName =  (self.articalCategoryData[indexPath.row] as AnyObject).value(forKey: "ac_name") as? String
        {
            cell?.lblCategoryName.text = "\(articalCategoryName)"
        }
        
        if boolArray.object(at: indexPath.row) as! String == "1"{
            cell?.imageViewSelect.image = UIImage.init(named: "check_blue.png")
            cell?.lblCategoryName.textColor = UIColor.black
            cell?.lblCategoryName.font = UIFont(name:"Lato-Bold", size: 14.0)

        }
        else{
            cell?.imageViewSelect.image = UIImage.init(named: "check_gray.png")
            cell?.lblCategoryName.textColor = UIColor.black
            cell?.lblCategoryName.font = UIFont(name:"Lato-Regular", size: 14.0)

        }
        
        
//        if let favour =  (self.articalData[indexPath.row] as AnyObject).value(forKey: "is_favourite") as? NSNumber
//        {
//            let favourateArtical = "\(favour)"
//            if favourateArtical == "1"{
//                cell?.imageViewBookmarked.image = UIImage.init(named: "bookmark_green.png")
//            }
//            else if favourateArtical == "0"{
//                cell?.imageViewBookmarked.image = UIImage.init(named: "bookmark_gray.png")
//            }
//        }
//        else if let favour =  (self.articalData[indexPath.row] as AnyObject).value(forKey: "is_favourite") as? String
//        {
//            let favourateArtical = "\(favour)"
//            if favourateArtical == "1"{
//                cell?.imageViewBookmarked.image = UIImage.init(named: "bookmark_green.png")
//            }
//            else if favourateArtical == "0"{
//                cell?.imageViewBookmarked.image = UIImage.init(named: "bookmark_gray.png")
//            }
//        }
        
        
        cell?.btnCategory.tag = indexPath.row
        cell?.btnCategory.addTarget(self, action: #selector(self.btnCategorySelectionClicked(_:)), for: .touchUpInside)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    
    
    // TODO: - ACTION METHODS
    
    @IBAction func btnCategorySelectionClicked(_ sender:UIButton){
        
        
        if boolArray.object(at: sender.tag) as! String == "0"{
            boolArray.replaceObject(at: sender.tag, with: "1")
            categoryCount = categoryCount + 1
        }
        else{
            boolArray.replaceObject(at: sender.tag, with: "0")
            categoryCount = categoryCount - 1
        }
        
        if categoryCount > 0 {
            lblCategoryCount.text = "\(categoryCount!) Selected"
        }
        else{
            lblCategoryCount.text = ""
        }
        
        
        let indexPath = NSIndexPath(row: sender.tag, section: 0)
        tableViewFilterArtical.reloadRows(at: [indexPath as IndexPath], with: .none)
        
        articalCategory = NSMutableArray.init()
        
        
        for i in 0...articalCategoryData.count - 1 {
            if boolArray.object(at: i) as! String == "1"{
                articalCategory.add((self.articalCategoryData[i] as AnyObject).value(forKey: "ac_id") as! String)
            }
        }
        
        
        finalFilterData.setValue(articalCategory, forKey: "articalCatData")
        print("articalCategory ID",articalCategory)
    }
    
    @IBAction func btnCloseClicked(_ sender:UIButton){
          _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnResetClicked(_ sender:UIButton){
        boolArray = NSMutableArray.init()
        categoryCount = 0
        
        if articalCategoryData.count > 0{
            for i in 0...articalCategoryData.count - 1 {
                boolArray.add("0")
            }
        }
        
        tableViewFilterArtical.reloadData()
        lblCategoryCount.text = ""

        USERDEFAULT.removeObject(forKey: "filterData")
        USERDEFAULT.synchronize()
        
        let notificationName = Notification.Name("filterArticalList")
        //NotificationCenter.default.post(name: notificationName, object: articalCategory, userInfo: nil)
        NotificationCenter.default.post(name: notificationName, object: nil)
        _ = self.navigationController?.popViewController(animated: true)

        
    }
    
    @IBAction func btnApplyClicked(_ sender:UIButton){
        
        if articalCategory.count == 0{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please select ArticalCategory", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else{
            USERDEFAULT.setValue(finalFilterData, forKey: "filterData")
            USERDEFAULT.synchronize()

            
            let notificationName = Notification.Name("filterArticalList")
            //NotificationCenter.default.post(name: notificationName, object: articalCategory, userInfo: nil)
            NotificationCenter.default.post(name: notificationName, object: nil)

            
            _ = self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForGetArticalCategory(){
        let completeURL = NSString(format:"%@%@", MainURL,getArticalCategoryURL) as String
        
        let pageNumber = "\(pageNum!)"
        
        var params:NSDictionary!
        
        if USERDEFAULT.value(forKey: "userID") != nil{
            params = [
                "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
                "token": USERDEFAULT.value(forKey: "token") as! String,
                "lang_type":Language_Type,
                "page":pageNumber,
                "limit":PAGINATION_LIMITE
            ]

        }
        else{
            params = [
                "lang_type":Language_Type,
                "page":pageNumber,
                "limit":PAGINATION_LIMITE
            ]
        }

        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetArticalCategory API Parameter :",finalParams)
        print("GetArticalCategory API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getArticalCategoryURLTag)
        
    }
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case getArticalCategoryURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("GetArticalCategory List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum == 1{
                    self.articalCategoryData = NSMutableArray()
                    self.articalCategoryGlobalData = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        self.articalCategoryGlobalData.add(myData[i])
                    }
                    
                    for i in 0...myData.count - 1 {
                        self.articalCategoryData.add(myData[i])
                    }
                    
                    boolArray = NSMutableArray.init()
                    
                    categoryCount = 0
                    for i in 0...articalCategoryData.count - 1 {
                        boolArray.add("0")
                    }
                    
                    
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum > 0) {
                            self.pageNum = self.pageNum - 1
                        }
                        self.isLoading = false
                    }else{
                        self.isLoading = true
                    }
                }
                else{
                    self.isLoading = false
                    if (self.pageNum > 0) {
                        self.pageNum = self.pageNum - 1
                    }
                    
                    if self.articalCategoryData.count == 0{
                        self.tableViewFilterArtical.isHidden = true
                        //self.lblNoArtical.isHidden = false
                    }
                    else{
                        self.tableViewFilterArtical.isHidden = false
                        //self.lblNoArtical.isHidden = true
                    }
                }
                
                self.tableViewFilterArtical.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
                
                
                if USERDEFAULT.value(forKey: "filterData") != nil{
                    print("Already FilterData :",USERDEFAULT.value(forKey: "filterData") ?? "NOOO")
                    alreadyFilterData = USERDEFAULT.value(forKey: "filterData") as! NSDictionary
                    
                    
                    let temp = NSMutableArray()
                    
                    for i in 0..<self.articalCategoryData.count{
                        let id = (self.articalCategoryData[i] as AnyObject).value(forKey: "ac_id") as! String
                        temp.add("\(id)")
                    }
                    print(temp)
                    
                    if let abc = alreadyFilterData.value(forKey: "articalCatData") as? NSArray
                    {
                        for i in 0..<abc.count{
                            let myIndexValue:Int = temp.index(of: "\(abc[i])")
                            if self.articalCategoryData.count > myIndexValue{
                                self.boolArray.replaceObject(at: myIndexValue, with: "1")
                                let path = NSIndexPath(row: myIndexValue, section: 0)
                                tableViewFilterArtical.reloadRows(at: [path as IndexPath], with:.none)
                            }

                        }
                    }
                    articalCategory = NSMutableArray.init()
                    
                    for i in 0...articalCategoryData.count - 1 {
                        if boolArray.object(at: i) as! String == "1"{
                            articalCategory.add((self.articalCategoryData[i] as AnyObject).value(forKey: "ac_id") as! String)
                        }
                    }
                    finalFilterData.setValue(articalCategory, forKey: "articalCatData")
                    print("articalCategory ID",articalCategory)
                    
                    
                    categoryCount = articalCategory.count
                    
                    if categoryCount > 0 {
                        lblCategoryCount.text = "\(categoryCount!) Selected"
                    }
                    else{
                        lblCategoryCount.text = ""
                    }
                }
                
                
                
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case getArticalCategoryURLTag:
            self.isLoading = false
            if (self.pageNum > 0) {
                self.pageNum = self.pageNum - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }

    

}
