//
//  BookmarkArticalVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 12/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SDWebImage

class BookmarkArticalVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate {
    
    @IBOutlet var tableViewBookmarkArtical:UITableView!
    //For Pasignation
    var pageNum:Int!
    var isLoading:Bool?
    
    var bookmarkArticalData = NSMutableArray()
    var bookmarkArticalGlobalData = NSMutableArray()
    
    @IBOutlet var lblNoArtical:UILabel!
    
    var articalID:String!

    @IBOutlet var viewSearchArtical:UIView!
    @IBOutlet var txtSearch:UITextField!
    @IBOutlet var headerView:UIView!

    var bookMarkIndex:Int!

    @IBOutlet var lblBookmarkheader:UILabel!

    var offlineBookMarkData1 = NSMutableArray()

    var refreshControl: UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        
        let notificationName = Notification.Name("refreshingBookmarkArticalList")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(notificationBookmarkCalled(notification:)), name: notificationName, object: nil)

        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
        
        
        self.viewSearchArtical.frame = CGRect(x: ScreenSize.SCREEN_WIDTH, y: self.headerView.frame.origin.y, width: self.viewSearchArtical.frame.size.width, height: self.viewSearchArtical.frame.size.height)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // TODO: - OTHER METHODS
   
    
    func generalViewControllerSetting(){
        
        headerView.addSubview(viewSearchArtical)

        if USERDEFAULT.value(forKey: "userID") != nil{
            pageNum=1;
            let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            indicator1.startAnimating()
            
            self.bookmarkArticalData = NSMutableArray()
            self.bookmarkArticalGlobalData = NSMutableArray()
            
            if Reachability.isConnectedToNetwork() == true {
                self.addLoadingIndicatiorOnFooterOnTableView()
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetBookmarkedArticalList), userInfo: nil, repeats: false)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }

        }
        else{
            if let offlineBookMarkData = USERDEFAULT.value(forKey: "offlineBookMarkArticalData") as? NSArray{
                
                offlineBookMarkData1 = NSMutableArray()
                self.bookmarkArticalData = NSMutableArray()
                self.bookmarkArticalGlobalData = NSMutableArray()
                
                print("Offline Bookmark Data : ",offlineBookMarkData)
                print("Offline Bookmark Data Count: ",offlineBookMarkData.count)
                
                if offlineBookMarkData.count > 0{
                    for i in 0...offlineBookMarkData.count - 1 {
                        if let ifFav = (offlineBookMarkData[i] as AnyObject).value(forKey: "is_favourite") as? String{
                            let fav = "\(ifFav)"
                            if fav == "1"{
                                offlineBookMarkData1.add(offlineBookMarkData[i] as! NSDictionary)
                            }
                        }
                    }
                }
                
                self.bookmarkArticalData = offlineBookMarkData1.mutableCopy() as! NSMutableArray
                self.bookmarkArticalGlobalData = offlineBookMarkData1.mutableCopy() as! NSMutableArray

                
                print("Offline Bookmark Data : ",bookmarkArticalData)
                print("Offline Bookmark Data Count: ",bookmarkArticalData.count)


                if self.bookmarkArticalData.count == 0{
                    self.tableViewBookmarkArtical.isHidden = true
                    self.lblNoArtical.isHidden = false
                }
                else{
                    self.tableViewBookmarkArtical.isHidden = false
                    self.lblNoArtical.isHidden = true
                }
                self.tableViewBookmarkArtical.reloadData()
            }
            else{
                if self.bookmarkArticalData.count == 0{
                    self.tableViewBookmarkArtical.isHidden = true
                    self.lblNoArtical.isHidden = false
                }
                else{
                    self.tableViewBookmarkArtical.isHidden = false
                    self.lblNoArtical.isHidden = true
                }
                self.tableViewBookmarkArtical.reloadData()
            }
        }
        
        self.addTapGestureInOurView()
        self.setLocalizationText()
        self.addPullRefreshForBookMarkArtical()
    }
    
    
    func addPullRefreshForBookMarkArtical(){
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.gray
        refreshControl.addTarget(self, action: #selector(self.refreshBookMarkArtical(sender:)), for: .valueChanged)
        tableViewBookmarkArtical.addSubview(refreshControl)
    }
    
    func refreshBookMarkArtical(sender:AnyObject) {
        // Code to refresh table view
        
        
        if USERDEFAULT.value(forKey: "userID") != nil{
            pageNum=1;
            let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            indicator1.startAnimating()
            
            self.bookmarkArticalData = NSMutableArray()
            self.bookmarkArticalGlobalData = NSMutableArray()
            self.tableViewBookmarkArtical.reloadData()
            
            if Reachability.isConnectedToNetwork() == true {
                self.addLoadingIndicatiorOnFooterOnTableView()
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetBookmarkedArticalList), userInfo: nil, repeats: false)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
        }
        else{
            
            offlineBookMarkData1 = NSMutableArray()
            self.bookmarkArticalData = NSMutableArray()
            self.bookmarkArticalGlobalData = NSMutableArray()
            
            if let offlineBookMarkData = USERDEFAULT.value(forKey: "offlineBookMarkArticalData") as? NSArray{
                print("Offline Bookmark Data : ",offlineBookMarkData)
                print("Offline Bookmark Data Count: ",offlineBookMarkData.count)
                
                if offlineBookMarkData.count > 0{
                    for i in 0...offlineBookMarkData.count - 1 {
                        if let ifFav = (offlineBookMarkData[i] as AnyObject).value(forKey: "is_favourite") as? String{
                            let fav = "\(ifFav)"
                            if fav == "1"{
                                offlineBookMarkData1.add(offlineBookMarkData[i] as! NSDictionary)
                            }
                        }
                    }
                }
                
                self.bookmarkArticalData = offlineBookMarkData1.mutableCopy() as! NSMutableArray
                self.bookmarkArticalGlobalData = offlineBookMarkData1.mutableCopy() as! NSMutableArray
                
                
                print("Offline Bookmark Data : ",bookmarkArticalData)
                print("Offline Bookmark Data Count: ",bookmarkArticalData.count)
                
                
                if self.bookmarkArticalData.count == 0{
                    self.tableViewBookmarkArtical.isHidden = true
                    self.lblNoArtical.isHidden = false
                }
                else{
                    self.tableViewBookmarkArtical.isHidden = false
                    self.lblNoArtical.isHidden = true
                }
                self.tableViewBookmarkArtical.reloadData()
            }
            else{
                if self.bookmarkArticalData.count == 0{
                    self.tableViewBookmarkArtical.isHidden = true
                    self.lblNoArtical.isHidden = false
                }
                else{
                    self.tableViewBookmarkArtical.isHidden = false
                    self.lblNoArtical.isHidden = true
                }
                self.tableViewBookmarkArtical.reloadData()
            }
        }

        
        
        refreshControl.endRefreshing()
    }

    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        if viewTouched!.isKind(of: UIButton.self){
        }
        else{
            self.view.endEditing(true)
        }
    }

    
    func notificationBookmarkCalled(notification: NSNotification) {
        
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.bookmarkArticalData = NSMutableArray()
        self.bookmarkArticalGlobalData = NSMutableArray()
        tableViewBookmarkArtical.reloadData()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetBookmarkedArticalList), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }

    }
    
    
    func addLoadingIndicatiorOnFooterOnTableView(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableViewBookmarkArtical.tableFooterView = spinner
    }
    func removeLoadingIndicatiorOnFooterOnTableView(){
        tableViewBookmarkArtical.tableFooterView = nil
    }
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    func showAlertForRemoveBookmark(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: "YES", otherButtonTitles: "NO")
        alert.show()
       // alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }

    func setLocalizationText(){
        lblBookmarkheader.text = NSLocalizedString("bookmarked", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblNoArtical.text = NSLocalizedString("nobookmarkartical", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
    }
    
    
    // TODO: - AlertView Delegate

    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        case 1002:
            switch buttonIndex {
            case 0:
                
                if USERDEFAULT.value(forKey: "userID") != nil{
                    self.bookmarkArticalData.removeObject(at: bookMarkIndex)
                    self.bookmarkArticalGlobalData.removeObject(at: bookMarkIndex)
                    
                    if self.bookmarkArticalData.count == 0{
                        self.tableViewBookmarkArtical.isHidden = true
                        self.lblNoArtical.isHidden = false
                    }
                    else{
                        self.tableViewBookmarkArtical.isHidden = false
                        self.lblNoArtical.isHidden = true
                    }
                    
                    tableViewBookmarkArtical.reloadData()
                    
                    if Reachability.isConnectedToNetwork() == true {
                        self.performSelector(inBackground: #selector(self.postDataOnWebserviceForRemoveFromBookMark), with: nil)
                    }else{
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
                else{
                    
                    
//                    let offlineIDData:NSMutableArray = (USERDEFAULT.value(forKey: "offlineBookMarkArticalData") as! NSArray) as! NSMutableArray
//                    print("Offline ID Data : ",offlineIDData)
//                    
//                    
//                    let temp = NSMutableArray()
//                    for i in 0..<offlineIDData.count{
//                        let id = (offlineIDData[i] as AnyObject).value(forKey: "article_id") as! String
//                        temp.add("\(id)")
//                    }
//                    print(temp)
//                    
//                   
//                    
//                    let myIndexValue:Int = temp.index(of: "\(bookmarkArticalData[bookMarkIndex])")
//                    if offlineIDData.count > myIndexValue{
//                        offlineIDData.setValue("1", forKey: "is_favourite")
//                    }
//                    
//                    
//                    print("Offline ID Data : ",offlineIDData)
//                    let finalData = offlineIDData.mutableCopy()
//                    print("Final ID Data : ",finalData)
//
//                    USERDEFAULT.setValue(finalData, forKey: "offlineBookMarkArticalData")
//                    USERDEFAULT.synchronize()

                    
                    
                    self.bookmarkArticalData.removeObject(at: bookMarkIndex)
                    self.bookmarkArticalGlobalData.removeObject(at: bookMarkIndex)
                    
                    if self.bookmarkArticalData.count == 0{
                        self.tableViewBookmarkArtical.isHidden = true
                        self.lblNoArtical.isHidden = false
                    }
                    else{
                        self.tableViewBookmarkArtical.isHidden = false
                        self.lblNoArtical.isHidden = true
                    }
                    tableViewBookmarkArtical.reloadData()
                }
                
              
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
  
        default:
            break
        }
    }

    
    
    // TODO: - DELEGATE ScrollView
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == tableViewBookmarkArtical {
            if isLoading == true{
                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                    pageNum = pageNum + 1
                    print(pageNum)
                    isLoading = false
                    
                    if Reachability.isConnectedToNetwork() == true {
                        self.addLoadingIndicatiorOnFooterOnTableView()
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetBookmarkedArticalList), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
            }
            
        }
        
        
    }
    
    // TODO: - Textfield DELEGATE METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        return false
    }
    
    
    
    // TODO: - DELEGATE METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bookmarkArticalData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "articalCell"
        var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ArticalTableVC
        
        if cell == nil {
            let nib  = Bundle.main.loadNibNamed("ArticalTableVC", owner: self, options: nil)
            cell = nib?[0] as? ArticalTableVC
        }
        cell!.selectionStyle = .none;
        
        
        if let articalName =  (self.bookmarkArticalData[indexPath.row] as AnyObject).value(forKey: "article_name") as? String
        {
            cell?.lblArticalName.text = "\(articalName)"
        }
        
        
        let imageUrl = (self.bookmarkArticalData[indexPath.row] as AnyObject).value(forKey: "article_img") as? String
        let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl!) as String
        let url : NSString = fullUrl as NSString
        let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
        let searchURL : NSURL = NSURL(string: urlStr as String)!
        
        
        cell?.activityIndicatorForArticalImage.isHidden = false
        cell?.activityIndicatorForArticalImage.startAnimating()
        
        cell?.imageViewArtical.sd_setImage(with: searchURL as URL, completed: { (image:UIImage?, error:Error?, cacheType:SDImageCacheType!, imageURL:URL?) in
            
            if ((error) != nil) {
                cell?.imageViewArtical.image = UIImage.init(named: "image_placeholder.png")
            }
            
            cell?.activityIndicatorForArticalImage.isHidden = true
            
        })
        
        if let favour =  (self.bookmarkArticalData[indexPath.row] as AnyObject).value(forKey: "is_favourite") as? NSNumber
        {
            let favourateArtical = "\(favour)"
            if favourateArtical == "1"{
                cell?.imageViewBookmarked.image = UIImage.init(named: "bookmark_green.png")
            }
            else if favourateArtical == "0"{
                cell?.imageViewBookmarked.image = UIImage.init(named: "bookmark_gray.png")
            }
        }
        else if let favour =  (self.bookmarkArticalData[indexPath.row] as AnyObject).value(forKey: "is_favourite") as? String
        {
            let favourateArtical = "\(favour)"
            if favourateArtical == "1"{
                cell?.imageViewBookmarked.image = UIImage.init(named: "bookmark_green.png")
            }
            else if favourateArtical == "0"{
                cell?.imageViewBookmarked.image = UIImage.init(named: "bookmark_gray.png")
            }
        }
        
        cell?.btnBookMark.tag = indexPath.row
        cell?.btnBookMark.addTarget(self, action: #selector(self.btnBookmarkClicked(_:)), for: .touchUpInside)

        cell?.btnShare.tag = indexPath.row
        cell?.btnShare.addTarget(self, action: #selector(self.btnSharingClicked(_:)), for: .touchUpInside)

        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let articalDetailVC = ArticalDetailVC(nibName: "ArticalDetailVC", bundle: nil)
        articalDetailVC.articalID = (self.bookmarkArticalData[indexPath.row] as AnyObject).value(forKey: "article_id") as? String
        self.navigationController?.pushViewController(articalDetailVC, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 275.0
    }
    
    // TODO: - ACTION METHODS
    @IBAction func btnBackClicked(_ sender:UIButton){
        _ =  self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnBookmarkClicked(_ sender:UIButton){

       // let ifFav = (self.bookmarkArticalData[sender.tag] as AnyObject).value(forKey: "is_favourite") as? String
        
            articalID = (self.bookmarkArticalData[sender.tag] as AnyObject).value(forKey: "article_id") as? String
            bookMarkIndex = sender.tag
            self.showAlertForRemoveBookmark(Appname, title: "Are you want to sure remove Bookmarked Article?", alertTag: 1002)
        
        
        
       
        
        
//        if ifFav == "1"{
//            let temp = (self.bookmarkArticalData[sender.tag] as! NSDictionary).mutableCopy() as! NSMutableDictionary
//            print(temp)
//            temp.setValue("0", forKey: "is_favourite")
//            
//            if (self.bookmarkArticalData.count > sender.tag) {
//                self.bookmarkArticalData.replaceObject(at: sender.tag, with: temp)
//                self.bookmarkArticalGlobalData.replaceObject(at: sender.tag, with: temp)
//                let indexPath = NSIndexPath(row: sender.tag, section: 0)
//                tableViewBookmarkArtical.reloadRows(at: [indexPath as IndexPath], with: .none)
//            }
//            
//            
//            if Reachability.isConnectedToNetwork() == true {
//                self.performSelector(inBackground: #selector(self.postDataOnWebserviceForRemoveFromBookMark), with: nil)
//            }else{
//                showAlert("Check Connection", title: "Internet is not available.")
//            }
//            
//        }
//        else{
//            let temp = (self.bookmarkArticalData[sender.tag] as! NSDictionary).mutableCopy() as! NSMutableDictionary
//            print(temp)
//            temp.setValue("1", forKey: "is_favourite")
//            
//            if (self.bookmarkArticalData.count > sender.tag) {
//                self.bookmarkArticalData.replaceObject(at: sender.tag, with: temp)
//                self.bookmarkArticalGlobalData.replaceObject(at: sender.tag, with: temp)
//                let indexPath = NSIndexPath(row: sender.tag, section: 0)
//                tableViewBookmarkArtical.reloadRows(at: [indexPath as IndexPath], with: .none)
//            }
//            
//            if Reachability.isConnectedToNetwork() == true {
//                self.performSelector(inBackground: #selector(self.postDataOnWebserviceForAddToBookMark), with: nil)
//            }else{
//                showAlert("Check Connection", title: "Internet is not available.")
//            }
//            
//        }
        
        
    }
    
    @IBAction func btnSharingClicked(_ sender:UIButton){
        // text to share
        let text = (self.bookmarkArticalData[sender.tag] as AnyObject).value(forKey: "article_name") as! String
        
//        let imageUrl = (self.bookmarkArticalData[sender.tag] as AnyObject).value(forKey: "article_img") as! String
//        let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl) as String
        
        let indexpath = IndexPath(row: sender.tag, section: 0)
        let cell = tableViewBookmarkArtical.cellForRow(at: indexpath) as! ArticalTableVC

        
        let  activityViewController = UIActivityViewController(activityItems: [text,cell.imageViewArtical.image!], applicationActivities: [])
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
        
        
    }
    

    @IBAction func btnSearchClicked(_ sender:UIButton){
        txtSearch.becomeFirstResponder()
        
        UIView.animate(withDuration: 0.3) {
            
            self.viewSearchArtical.frame = CGRect(x: self.headerView.frame.origin.x, y: self.headerView.frame.origin.y, width: self.viewSearchArtical.frame.size.width, height: self.viewSearchArtical.frame.size.height)
            
        }
        
    }
    
    @IBAction func btnSearchCancelClicked(_ sender:UIButton){
        txtSearch.text = ""
        txtSearch.resignFirstResponder()
        
        UIView.animate(withDuration: 0.3) {
            
            self.viewSearchArtical.frame = CGRect(x: self.headerView.frame.origin.x+self.headerView.frame.size.width, y: self.headerView.frame.origin.y, width: self.viewSearchArtical.frame.size.width, height: self.viewSearchArtical.frame.size.height)
            
        }
        
        
        bookmarkArticalData = bookmarkArticalGlobalData.mutableCopy() as! NSMutableArray
        if bookmarkArticalData.count == 0 {
            lblNoArtical.isHidden = false
            tableViewBookmarkArtical.isHidden = true
        }
        else{
            tableViewBookmarkArtical.isHidden = false
            lblNoArtical.isHidden = true
        }
        tableViewBookmarkArtical.reloadData()

    }
    
    
    @IBAction func searchFilter(){
        var predicate = String()
        
        
        if (txtSearch.text == "") {
            bookmarkArticalData = bookmarkArticalGlobalData.mutableCopy() as! NSMutableArray
        }else{
            predicate = String(format: "SELF['article_name'] contains[c] '%@'", txtSearch.text!)
            print(predicate)
            
            let myPredicate:NSPredicate = NSPredicate(format:predicate)
            let temp = self.bookmarkArticalGlobalData.mutableCopy() as! NSArray
            bookmarkArticalData = (temp.filtered(using: myPredicate) as NSArray).mutableCopy() as! NSMutableArray
            
            
        }
        
        if bookmarkArticalData.count == 0 {
            lblNoArtical.isHidden = false
            tableViewBookmarkArtical.isHidden = true
        }
        else{
            tableViewBookmarkArtical.isHidden = false
            lblNoArtical.isHidden = true
        }
        tableViewBookmarkArtical.reloadData()
        
    }

    
    
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForGetBookmarkedArticalList(){
        let completeURL = NSString(format:"%@%@", MainURL,getBookmarkArticalListURL) as String
        
        let pageNumber = "\(pageNum!)"

        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "page":pageNumber,
            "limit":PAGINATION_LIMITE
            ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetFavourtiteStore API Parameter :",finalParams)
        print("GetFavourtiteStore API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getBookmarkArticalListURLTag)
        
    }
    
    func postDataOnWebserviceForAddToBookMark(){
        let completeURL = NSString(format:"%@%@", MainURL,addArticalBookMarkURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "article_id":articalID
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("AddToBookMark API Parameter :",finalParams)
        print("AddToBookMark API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: addArticalBookMarkURLTag)
    }
    
    func postDataOnWebserviceForRemoveFromBookMark(){
        let completeURL = NSString(format:"%@%@", MainURL,removeArticalBookMarkURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "article_id":articalID
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("AddToBookMark API Parameter :",finalParams)
        print("AddToBookMark API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: removeArticalBookMarkURLTag)
    }
    

    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case getBookmarkArticalListURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("GetArtical List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum == 1{
                    self.bookmarkArticalData = NSMutableArray()
                    self.bookmarkArticalGlobalData = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        self.bookmarkArticalGlobalData.add(myData[i])
                    }
                    
                    for i in 0...myData.count - 1 {
                        self.bookmarkArticalData.add(myData[i])
                    }
                    
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum > 0) {
                            self.pageNum = self.pageNum - 1
                        }
                        self.isLoading = false
                    }else{
                        self.isLoading = true
                    }
                }
                else{
                    self.isLoading = false
                    if (self.pageNum > 0) {
                        self.pageNum = self.pageNum - 1
                    }
                    
                    if self.bookmarkArticalData.count == 0{
                        self.tableViewBookmarkArtical.isHidden = true
                        self.lblNoArtical.isHidden = false
                    }
                    else{
                        self.tableViewBookmarkArtical.isHidden = false
                        self.lblNoArtical.isHidden = true
                    }
                    
                }
                
                self.tableViewBookmarkArtical.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        case addArticalBookMarkURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("Add Bookmark Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                
                let notificationName = Notification.Name("refreshingArticalList")
                // Post notification
                NotificationCenter.default.post(name: notificationName, object: nil)

                
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }

            break
            
        case removeArticalBookMarkURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("Remove Bookmark Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                let notificationName = Notification.Name("refreshingArticalList")
                // Post notification
                NotificationCenter.default.post(name: notificationName, object: nil)

            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }

            break

            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case getBookmarkArticalListURLTag:
            self.isLoading = false
            if (self.pageNum > 0) {
                self.pageNum = self.pageNum - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
        case addArticalBookMarkURLTag:
            break
        case removeArticalBookMarkURLTag:
            break

            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    
    
}
