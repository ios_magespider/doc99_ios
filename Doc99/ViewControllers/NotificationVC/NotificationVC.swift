//
//  NotificationVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 27/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class NotificationVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet var tableViewNotification:UITableView!
    //For Pasignation
    var pageNum:Int!
    var isLoading:Bool?
    
    var notificationData = NSMutableArray()
    var notitficationGlobalData = NSMutableArray()
    @IBOutlet var lblNoNotification:UILabel!
    
    var activityID:String!

    var notificationUnReadCount:String!

    
    // FIXME: - VIEW CONTROLLER METHODS

    override func viewDidLoad() {
        super.viewDidLoad()

        if Reachability.isConnectedToNetwork() == true {
            self.performSelector(inBackground: #selector(self.postDataOnWebserviceForUNReadNotificationCount), with: nil)
        } else {
        }

        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.notificationData = NSMutableArray()
        self.notitficationGlobalData = NSMutableArray()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetNotification), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        self.addTapGestureInOurView()

        
    }
    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        if viewTouched!.isKind(of: UIButton.self){
        }
        else{
            self.view.endEditing(true)
        }
    }

    
    func addLoadingIndicatiorOnFooterOnTableView(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableViewNotification.tableFooterView = spinner
    }
    func removeLoadingIndicatiorOnFooterOnTableView(){
        tableViewNotification.tableFooterView = nil
    }
    
    // TODO: - DELEGATE METHODS
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "notificationCell"
        
        var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? NotificationTableVC
        
        if cell == nil {
            let nib  = Bundle.main.loadNibNamed("NotificationTableVC", owner: self, options: nil)
            cell = nib?[0] as? NotificationTableVC
        }
        
        cell!.selectionStyle = .none;
        
        
        
//        cell?.lblTitle.text = (notificationData.object(at: indexPath.row)as AnyObject).value(forKey: "comments") as? String
//        
//        cell?.lblTitle = Utility.setlableFrame(cell?.lblTitle, fontSize: 14.0)
//        
//        if (CGFloat((cell?.lblTitle.frame.size.height)!) < 20.0) {
//            cell?.lblTitle.frame = CGRect(x: (cell?.lblTitle.frame.origin.x)!, y: (cell?.lblTitle.frame.origin.y)!, width: (cell?.lblTitle.frame.size.width)!, height: (cell?.lblTitle.frame.size.height)!)
//        }
//        
//        print("New Title Frame",cell?.lblTitle.frame ?? "123")
//        
//        cell?.imageViewBack.frame = CGRect(x: (cell?.imageViewBack.frame.origin.x)!, y: (cell?.imageViewBack.frame.origin.y)!, width: (cell?.imageViewBack.frame.size.width)!, height: (cell?.lblTitle.frame.origin.y)! + (cell?.lblTitle.frame.size.height)! + 5)
//        
//        if let isreadStatus = (notificationData.object(at: indexPath.row)as AnyObject).value(forKey: "isread") as? NSNumber
//        {
//            let  isreadStatusString = "\(isreadStatus)"
//            if isreadStatusString == "0"{
//                cell?.imageViewRead.isHidden = false
//                
//            }
//            else{
//                cell?.imageViewRead.isHidden = true
//                
//            }
//            
//        }
        
        
       
//        cell?.autoresizingMask = [.flexibleRightMargin, .flexibleLeftMargin, .flexibleBottomMargin,.flexibleTopMargin,.flexibleHeight,.flexibleWidth]
//        cell?.contentView.autoresizingMask = [.flexibleRightMargin, .flexibleLeftMargin, .flexibleBottomMargin,.flexibleTopMargin,.flexibleHeight,.flexibleWidth]
        
  
        
        return cell!
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        UIApplication.shared.applicationIconBadgeNumber = Int(self.notificationUnReadCount)! - 1
        
        let aa = Int(self.notificationUnReadCount)! - 1
        APPDELEGATE.badgeNumber = "\(aa)"
        
        
        if let isreadStatus = (notificationData.object(at: indexPath.row)as AnyObject).value(forKey: "isread") as? NSNumber
        {
            let  isreadStatusString = "\(isreadStatus)"
            if isreadStatusString == "0"{
                
                activityID = (notificationData.object(at: indexPath.row)as AnyObject).value(forKey: "activityid") as! String
                
                if Reachability.isConnectedToNetwork() == true {
                    Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForReadNotification), userInfo: nil, repeats: false)
                } else {
                }
            }
        }
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        var myCellHeight:CGFloat!
//        
//        myCellHeight = 40.0
//        
//        
//        var myLabel = UILabel.init()
//        myLabel.frame = CGRect(x: 10.0, y: 10.0, width: 270.0, height: 20.0)
//        myLabel.numberOfLines = 0
//        
//        myLabel.text = (notificationData.object(at: indexPath.row)as AnyObject).value(forKey: "comments") as? String
//        
//        
//        
//        myLabel = Utility.setlableFrame(myLabel, fontSize: 14.0)
//        
//        if (myLabel.frame.size.height < 20.0) {
//            myLabel.frame = CGRect(x: 10.0, y: 10.0, width: 270.0, height: 20.0)
//        }
//        
//        if ((notificationData.object(at: indexPath.row)as AnyObject).value(forKey: "comments") as? String) != ""{
//            myCellHeight = myLabel.frame.size.height + 20.0
//            
//        }
//        else{
//            myCellHeight = 40.0
//        }
//        
//        print("My Cell Height",myCellHeight)
//        return myCellHeight
        
        return 100
    }
    
    
    
    
    
    // TODO: - DELEGATE ScrollView
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == tableViewNotification {
            if isLoading == true{
                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                    pageNum = pageNum + 1
                    print(pageNum)
                    isLoading = false
                    
                    if Reachability.isConnectedToNetwork() == true {
                        self.addLoadingIndicatiorOnFooterOnTableView()
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetNotification), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
            }
            
        }
        
    }
    
    
    
    
    // TODO: - ACTION METHODS
    @IBAction func btnClearAllClicked(_ sender:UIButton){
        
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForClearNotification), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        
    }

    
    
    // TODO: - POST DATA METHODS
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForGetNotification(){
        
        let completeURL = NSString(format:"%@%@", MainURL,getNotificationURL) as String
       
        let pageNumber = "\(pageNum!)"

       let params = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "page":pageNumber,
            "limit":PAGINATION_LIMITE
        ] as [String : Any]
        
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("getNotification API Parameter :",finalParams)
        print("getNotification API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getNotificationURLTag)
    }
    
    func postDataOnWebserviceForReadNotification(){
        
        let completeURL = NSString(format:"%@%@", MainURL,readNotificationURL) as String
        
        let pageNumber = "\(pageNum!)"
        
        let params = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "activity_id":activityID
            ] as [String : Any]
        
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("ReadNotification API Parameter :",finalParams)
        print("ReadNotification API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: readNotificationURLTag)
    }

    func postDataOnWebserviceForUNReadNotificationCount(){
        let completeURL = NSString(format:"%@%@", MainURL,unReadCountNotificationURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token" : USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("UNReadNotificationCount API Parameter :",finalParams)
        print("UNReadNotificationCount API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: unReadCountNotificationURLTag)
        
    }
    
    func postDataOnWebserviceForClearNotification(){
        
        let completeURL = NSString(format:"%@%@", MainURL,clearAllNotificationURL) as String
        
        let params = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type
            ] as [String : Any]
        
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("ClearNotification API Parameter :",finalParams)
        print("ClearNotification API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: clearAllNotificationURLTag)
    }

    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case getNotificationURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("getNotification Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
           
                var myData = NSArray()
                
                if self.pageNum == 1{
                    self.notificationData = NSMutableArray()
                    self.notitficationGlobalData = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        self.notitficationGlobalData.add(myData[i])
                    }
                    
                    for i in 0...myData.count - 1 {
                        self.notificationData.add(myData[i])
                    }
                    
                    print("My Article Data : ",myData)
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum > 0) {
                            self.pageNum = self.pageNum - 1
                        }
                        self.isLoading = false
                    }else{
                        self.isLoading = true
                    }
                }
                else{
                    self.isLoading = false
                    if (self.pageNum > 0) {
                        self.pageNum = self.pageNum - 1
                    }
                    
                }
                
                if self.notificationData.count == 0{
                    self.tableViewNotification.isHidden = true
                    self.lblNoNotification.isHidden = false
                }
                else{
                    self.tableViewNotification.isHidden = false
                    self.lblNoNotification.isHidden = true
                }
                
                self.tableViewNotification.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
                
                
            }
            break
            
        case readNotificationURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("Read Notification Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                
            }
            break
            
        case unReadCountNotificationURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("UnRead Notification Count Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                if let notificationCount = resultDict.value(forKey: "totalunread") as? NSNumber{
                    let notCount = "\(notificationCount)"
                    
                    APPDELEGATE.badgeNumber = notCount
                    UIApplication.shared.applicationIconBadgeNumber = Int(notCount)!
                    self.notificationUnReadCount = notCount
                    
                    print("Notification Unread Count :->", notCount)
                    
                }
                
            }
            break
            
        case clearAllNotificationURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("Clear Notification Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                
                self.notificationData.removeAllObjects()
                
                if self.notificationData.count == 0{
                    self.tableViewNotification.isHidden = true
                    self.lblNoNotification.isHidden = false
                }
                else{
                    self.tableViewNotification.isHidden = false
                    self.lblNoNotification.isHidden = true
                    
                }
                
                
                UIApplication.shared.applicationIconBadgeNumber = 0
                APPDELEGATE.badgeNumber = "0"
                
                
                if Reachability.isConnectedToNetwork() == true {
                    self.performSelector(inBackground: #selector(self.postDataOnWebserviceForUNReadNotificationCount), with: nil)
                } else {
                }
                

                
            }
            SVProgressHUD.dismiss()
            break
    
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case getNotificationURLTag:
            self.isLoading = false
            if (self.pageNum > 0) {
                self.pageNum = self.pageNum - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            
            break
        case readNotificationURLTag:
            
            break
        case unReadCountNotificationURLTag:
            
            break
        case clearAllNotificationURLTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }

    
    
}
