//
//  TabBarVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 30/03/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class TabBarVC: UIViewController {
    
    var selectedIndexOfMyTabbarController:Int!
   // @IBOutlet var loginView:UIView!
   

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Selected Index : ",selectedIndexOfMyTabbarController)
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btnLogoutClicked(_ sender:UIButton){
        APPDELEGATE.logOutUser()
        let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.settingTabBarController()
        APPDELEGATE.myTabBarController?.selectedIndex = self.selectedIndexOfMyTabbarController
        
//        if APPDELEGATE.myTabBarController?.selectedIndex == 0 || APPDELEGATE.myTabBarController?.selectedIndex == 2 || APPDELEGATE.myTabBarController?.selectedIndex == 3 || APPDELEGATE.myTabBarController?.selectedIndex == 4{
//            
//            if USERDEFAULT.value(forKey: "userID") != nil{
//                
//            }
//            else{
//                self.view.addSubview(loginView)
//            }
//            
//        }
        
        
    }
    
    @IBAction func btnLoginClicked(_ sender:UIButton){
        let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
        loginVC.isFromTab = "0"
        self.navigationController?.pushViewController(loginVC, animated: true)
        
    }
    @IBAction func btnCancelClicked(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func settingTabBarController() {
        
        let firstview = DrugListVC(nibName: "DrugListVC", bundle: nil)
        let secondview = ArticalListVC(nibName: "ArticalListVC", bundle: nil)
        let thirdview = HealthIndexCalculatorVC(nibName: "HealthIndexCalculatorVC", bundle: nil)
        let fourthview = UserHomeVC(nibName: "UserHomeVC", bundle: nil)
        let fifthview = ProfileVC(nibName: "ProfileVC", bundle: nil)
        
        let firsttab = UINavigationController(rootViewController: firstview)
        let secondtab = UINavigationController(rootViewController: secondview)
        let thirdtab = UINavigationController(rootViewController: thirdview)
        let fourthtab = UINavigationController(rootViewController: fourthview)
        let fifthtab = UINavigationController(rootViewController: fifthview)
        
        APPDELEGATE.myTabBarController = UITabBarController.init()
        APPDELEGATE.myTabBarController?.viewControllers = NSArray(objects: firsttab, secondtab, thirdtab,fourthtab,fifthtab) as? [UIViewController]
        
        
        let item1  = APPDELEGATE.myTabBarController?.tabBar.items?[0]
        let item2  = APPDELEGATE.myTabBarController?.tabBar.items?[1]
        let item3  = APPDELEGATE.myTabBarController?.tabBar.items?[2]
        let item4  = APPDELEGATE.myTabBarController?.tabBar.items?[3]
        let item5  = APPDELEGATE.myTabBarController?.tabBar.items?[4]

        item1?.title = "Drugs"
        item1?.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.init(red: 181.0/255.0, green: 182.0/255.0, blue: 183.0/255.0, alpha: 1.0)], for: .normal)
        item1?.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)], for: .selected)
        item1?.image = UIImage(named:"home-unselect")?.withRenderingMode(.alwaysOriginal)
        item1?.selectedImage = UIImage.init(named: "home-select")?.withRenderingMode(.alwaysOriginal)
//        item1?.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        item1?.titlePositionAdjustment = UIOffset(horizontal: 5, vertical: -3)
        
        
        item2?.title = "Updates"
        item2?.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.init(red: 181.0/255.0, green: 182.0/255.0, blue: 183.0/255.0, alpha: 1.0)], for: .normal)
        item2?.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)], for: .selected)
        item2?.image = UIImage.init(named: "search-unselect")?.withRenderingMode(.alwaysOriginal)
        item2?.selectedImage = UIImage.init(named: "search-select")?.withRenderingMode(.alwaysOriginal)
//        item2?.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        item2?.titlePositionAdjustment = UIOffset(horizontal: 5, vertical: -3)
        
        item3?.title = "Health Cal"
        item3?.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.init(red: 181.0/255.0, green: 182.0/255.0, blue: 183.0/255.0, alpha: 1.0)], for: .normal)
        item3?.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)], for: .selected)
        item3?.image = UIImage.init(named: "bag-unselect")?.withRenderingMode(.alwaysOriginal)
        item3?.selectedImage = UIImage.init(named: "bag-select")?.withRenderingMode(.alwaysOriginal)
//        item3?.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        item3?.titlePositionAdjustment = UIOffset(horizontal: 5, vertical: -3)
        
        item4?.title = "Pharmacy"
        item4?.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.init(red: 181.0/255.0, green: 182.0/255.0, blue: 183.0/255.0, alpha: 1.0)], for: .normal)
        item4?.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)], for: .selected)
        item4?.image = UIImage.init(named: "user-unselect")?.withRenderingMode(.alwaysOriginal)
        item4?.selectedImage = UIImage.init(named: "user-select")?.withRenderingMode(.alwaysOriginal)
//        item4?.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        item4?.titlePositionAdjustment = UIOffset(horizontal: 5, vertical: -3)
        
        item5?.title = "More"
        item5?.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.init(red: 181.0/255.0, green: 182.0/255.0, blue: 183.0/255.0, alpha: 1.0)], for: .normal)
        item5?.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)], for: .selected)
        item5?.image = UIImage.init(named: "more-unselect")?.withRenderingMode(.alwaysOriginal)
        item5?.selectedImage = UIImage.init(named: "more-select")?.withRenderingMode(.alwaysOriginal)
//        item5?.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        item5?.titlePositionAdjustment = UIOffset(horizontal: 5, vertical: -3)
        
        
        APPDELEGATE.myTabBarController?.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -5)
        APPDELEGATE.myTabBarController?.tabBar.barTintColor = UIColor.white
        
        
        self.view.addSubview((APPDELEGATE.myTabBarController?.view)!)
        
        
        //APPDELEGATE.window?.addSubview((APPDELEGATE.myTabBarController?.view)!)
        
//        if DeviceType.IS_IPHONE_4_OR_LESS{
//            APPDELEGATE.myTabBarController?.view.frame = CGRect(x: (APPDELEGATE.myTabBarController?.view.frame.origin.x)!, y: (APPDELEGATE.myTabBarController?.view.frame.origin.y)!, width: (APPDELEGATE.myTabBarController?.view.frame.size.width)!, height: 568 - 88)
//        }
//        else{
        APPDELEGATE.myTabBarController?.view.frame = CGRect(x: (APPDELEGATE.myTabBarController?.view.frame.origin.x)!, y: (APPDELEGATE.myTabBarController?.view.frame.origin.y)!, width: (self.view.frame.size.width), height: (self.view.frame.size.height))
//        }
    }
    
    
    
}
