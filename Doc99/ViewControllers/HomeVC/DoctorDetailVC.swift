//
//  DoctorDetailVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 17/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD
class DoctorDetailVC: UIViewController {

    // FIXME: - VIEW CONTROLLER METHODS

    @IBOutlet var lblDoctorName:UILabel!
    @IBOutlet var lblDoctorAddress:UILabel!
    @IBOutlet var lblMile:UILabel!
    @IBOutlet var imageViewFavourite:UIImageView!
    @IBOutlet var btnFavourite:UIButton!
    
    @IBOutlet var lblDoctorDesignation:UILabel!
    @IBOutlet var lblClinicName:UILabel!
    @IBOutlet var lblClinicAddress:UILabel!
    
    @IBOutlet var btnContact99:UIButton!

    var doctorDetailData = NSDictionary()

    var doctorID:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        
        print("Doctor Detail Data : ",doctorDetailData)
        self.setDoctorDetail(doctorData: doctorDetailData)
        self.setLocalizationText()
    }
    
    func setDoctorDetail(doctorData:NSDictionary){
        
        doctorID = doctorData.value(forKey: "d_id") as? String
        
        if let storename = doctorData.value(forKey: "first_name") as? String{
            lblDoctorName.text = storename
        }
        
        if let storeaddress = doctorData.value(forKey: "address") as? String{
            lblDoctorAddress.text = storeaddress
        }
        
        if let distance = doctorData.value(forKey: "distance") as? String{
            if distance != ""{
                let abc =  "\(String.localizedStringWithFormat("%.2f", Float(distance)!))"
                lblMile.text = "\(abc) Kilometer Away"
            }
        }
        else if let distance1 = doctorData.value(forKey: "distance") as? NSNumber{
            let abc =  "\(String.localizedStringWithFormat("%.2f", Float(distance1)))"
            lblMile.text = "\(abc) Kilometer Away"
        }
        
        if let practising_hospital_clinic_name = doctorData.value(forKey: "practising_hospital_clinic_name") as? String{
            lblClinicName.text = practising_hospital_clinic_name
        }
        
        if let practisingaddress = doctorData.value(forKey: "h_c_address") as? String{
            lblClinicAddress.text = practisingaddress
        }
        
        if let designation = doctorData.value(forKey: "designation") as? String{
            lblDoctorDesignation.text = designation
        }

        
        
        if let favour =  doctorData.value(forKey: "is_favourite") as? NSNumber
        {
            let favourateArtical = "\(favour)"
            if favourateArtical == "1"{
                imageViewFavourite.image = UIImage.init(named: "like.png")
            }
            else if favourateArtical == "0"{
                imageViewFavourite.image = UIImage.init(named: "unlike.png")
            }
        }
        else if let favour =  doctorData.value(forKey: "is_favourite") as? String
        {
            let favourateArtical = "\(favour)"
            if favourateArtical == "1"{
                imageViewFavourite.image = UIImage.init(named: "like.png")
            }
            else if favourateArtical == "0"{
                imageViewFavourite.image = UIImage.init(named: "unlike.png")
            }
        }

        
        
    }
    
    func setLocalizationText(){
        btnContact99.setTitle(NSLocalizedString("contact99", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        
    }

    
    
    // TODO: - DELEGATE METHODS
    // TODO: - ACTION METHODS
    @IBAction func btnFavouriteDoctorClicked(_ sender:UIButton){
        
        let ifFav = doctorDetailData.value(forKey: "is_favourite") as? String
        
        if ifFav == "1"{
            let temp:NSMutableDictionary = self.doctorDetailData.mutableCopy() as! NSMutableDictionary
            print(temp)
            temp.setValue("0", forKey: "is_favourite")
            
            self.doctorDetailData = temp.mutableCopy() as! NSDictionary
            
            imageViewFavourite.image = UIImage.init(named: "unlike.png")
            
           // self.doctorData.replaceObject(at: 0, with: temp)

            
           // if (self.doctorData.count > sender.tag) {
           //     self.doctorData.replaceObject(at: sender.tag, with: temp)
           //     self.doctorGlobalData.replaceObject(at: sender.tag, with: temp)
           //     let indexPath = NSIndexPath(row: sender.tag, section: 0)
           //     collectionViewDoctor.reloadItems(at: [indexPath as IndexPath])
           // }
            
            
            if Reachability.isConnectedToNetwork() == true {
                self.performSelector(inBackground: #selector(self.postDataOnWebserviceForRemoveFavouriteDoctor), with: nil)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
        }
        else{
            let temp:NSMutableDictionary = self.doctorDetailData.mutableCopy() as! NSMutableDictionary
            print(temp)
            temp.setValue("1", forKey: "is_favourite")
            
            self.doctorDetailData = temp.mutableCopy() as! NSDictionary

            imageViewFavourite.image = UIImage.init(named: "like.png")

            
          //  if (self.doctorData.count > sender.tag) {
          //      self.doctorData.replaceObject(at: sender.tag, with: temp)
          //      self.doctorGlobalData.replaceObject(at: sender.tag, with: temp)
          //      let indexPath = NSIndexPath(row: sender.tag, section: 0)
          //      collectionViewDoctor.reloadItems(at: [indexPath as IndexPath])
          //  }
            
            if Reachability.isConnectedToNetwork() == true {
                self.performSelector(inBackground: #selector(self.postDataOnWebserviceForAddFavouriteDoctor), with: nil)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
        }
    }
    
    @IBAction func btnBackClicked(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnContactDoctorClicked(_ sender:UIButton){
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForContactDOC99), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
    }

   
    
    // TODO: - POST DATA METHODS
    
    
    func postDataOnWebserviceForAddFavouriteDoctor(){
        let completeURL = NSString(format:"%@%@", MainURL,addFavouriteDoctorURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "d_id":doctorID
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("AddFavouriteStore API Parameter :",finalParams)
        print("AddFavouriteStore API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: addFavouriteDoctorURLTag)
    }
    
    func postDataOnWebserviceForRemoveFavouriteDoctor(){
        let completeURL = NSString(format:"%@%@", MainURL,removeFavouriteDoctorURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "d_id":doctorID
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("RemoveFavouriteStore API Parameter :",finalParams)
        print("RemoveFavouriteStore API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: removeFavouriteDoctorURLTag)
    }
    
    func postDataOnWebserviceForContactDOC99(){
        let completeURL = NSString(format:"%@%@", MainURL,contactDoctorURL) as String
        
        let timeStamp = Int(Date().timeIntervalSince1970)
        let strTimeStamp = String(timeStamp)
        
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "d_id":doctorID,
            "timestamp":strTimeStamp
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("ContactDOC99 API Parameter :",finalParams)
        print("ContactDOC99 API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: contactDoctorURLTag)
    }

    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case addFavouriteDoctorURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("AddFavouriteDoctor Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                let notificationName = Notification.Name("refreshDoctorData")
               // NotificationCenter.default.post(name: notificationName, object: nil)
                NotificationCenter.default.post(name: notificationName, object: nil, userInfo: self.doctorDetailData as? [AnyHashable : Any])

                
                

            }
            break
            
        case removeFavouriteDoctorURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("RemoveFavouriteDoctor Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                let notificationName = Notification.Name("refreshDoctorData")
                //NotificationCenter.default.post(name: notificationName, object: nil)
                
                NotificationCenter.default.post(name: notificationName, object: nil, userInfo: self.doctorDetailData as? [AnyHashable : Any])

            }
            break
            
        case contactDoctorURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("contactDoctor Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            SVProgressHUD.dismiss()
            break
            
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case addFavouriteDoctorURLTag:
            break
        case removeFavouriteDoctorURLTag:
            break
        case contactDoctorURLTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    

}
