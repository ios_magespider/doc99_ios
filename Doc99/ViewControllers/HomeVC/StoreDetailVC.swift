//
//  StoreDetailVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 17/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class StoreDetailVC: UIViewController {
    
    // FIXME: - VIEW CONTROLLER METHODS
    
    @IBOutlet var lblStoreName:UILabel!
    @IBOutlet var lblStoreAddress:UILabel!
    @IBOutlet var lblMile:UILabel!
    @IBOutlet var imageViewFavourite:UIImageView!
    @IBOutlet var btnFavourite:UIButton!

    @IBOutlet var lblDoctorDesignation:UILabel!
    @IBOutlet var lblClinicName:UILabel!
    @IBOutlet var lblClinicAddress:UILabel!

    
    @IBOutlet var btnEPrescription:UIButton!

    var storeDetailData = NSDictionary()

    var storeID:String!
    var storelatitude:String!
    var storeLongitude:String!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        print("Store Detail Data : ",storeDetailData)
        self.setDtoreDetail(storeData: storeDetailData)
        self.setLocalizationText()
    }
    
    func setDtoreDetail(storeData:NSDictionary){
        
        storeID = storeData.value(forKey: "store_id") as? String
        
        if let storename = storeData.value(forKey: "firstname") as? String{
            lblStoreName.text = storename
        }
        
        if let storeaddress = storeData.value(forKey: "address") as? String{
            lblStoreAddress.text = storeaddress
        }
        
        if let distance = storeData.value(forKey: "distance") as? String{
            if distance != ""{
                let abc =  "\(String.localizedStringWithFormat("%.2f", Float(distance)!))"
                lblMile.text = "\(abc) Kilometer Away"
            }
            
        }
        else if let distance1 = storeData.value(forKey: "distance") as? NSNumber{
            let abc =  "\(String.localizedStringWithFormat("%.2f", Float(distance1)))"

            lblMile.text = "\(abc) Kilometer Away"
        }
        
        
        
        if let favour =  storeData.value(forKey: "is_favourite") as? NSNumber
        {
            let favourateArtical = "\(favour)"
            if favourateArtical == "1"{
                imageViewFavourite.image = UIImage.init(named: "like.png")
            }
            else if favourateArtical == "0"{
                imageViewFavourite.image = UIImage.init(named: "unlike.png")
            }
        }
        else if let favour =  storeData.value(forKey: "is_favourite") as? String
        {
            let favourateArtical = "\(favour)"
            if favourateArtical == "1"{
                imageViewFavourite.image = UIImage.init(named: "like.png")
            }
            else if favourateArtical == "0"{
                imageViewFavourite.image = UIImage.init(named: "unlike.png")
            }
        }
        
        if let latitude = storeData.value(forKey: "latitude") as? String{
            let lati = "\(latitude)"
            if lati != ""{
                storelatitude = lati
            }
            else{
                storelatitude = "0.000"
            }
        }
        else{
            storelatitude = "0.000"
        }
        
        if let longtitude = storeData.value(forKey: "longtitude") as? String{
            let longi = "\(longtitude)"
            if longi != ""{
                storeLongitude = longi
            }
            else{
                storeLongitude = "0.000"
            }
        }
        else{
            storeLongitude = "0.000"
        }
        
    }
    
    
    func setLocalizationText(){
        btnEPrescription.setTitle(NSLocalizedString("eprescription", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        
    }
    // TODO: - DELEGATE METHODS
    
    
    // TODO: - ACTION METHODS
    @IBAction func btnFavouriteStoredClicked(_ sender:UIButton){
        
        let ifFav = storeDetailData.value(forKey: "is_favourite") as? String
        
        if ifFav == "1"{
            
            let temp:NSMutableDictionary = storeDetailData.mutableCopy() as! NSMutableDictionary
            print(temp)
            temp.setValue("0", forKey: "is_favourite")
            storeDetailData = temp.mutableCopy() as! NSDictionary
            imageViewFavourite.image = UIImage.init(named: "unlike.png")

            
            if Reachability.isConnectedToNetwork() == true {
                self.performSelector(inBackground: #selector(self.postDataOnWebserviceForRemoveFavouriteStore), with: nil)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
        }
        else{
            
            let temp:NSMutableDictionary = storeDetailData.mutableCopy() as! NSMutableDictionary
            print(temp)
            temp.setValue("1", forKey: "is_favourite")
            storeDetailData = temp.mutableCopy() as! NSDictionary
            imageViewFavourite.image = UIImage.init(named: "unlike.png")

            
            
            imageViewFavourite.image = UIImage.init(named: "like.png")
            
            if Reachability.isConnectedToNetwork() == true {
                self.performSelector(inBackground: #selector(self.postDataOnWebserviceForAddFavouriteStore), with: nil)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
        }
    }
    
    @IBAction func btnBackClicked(_ sender:UIButton){
       _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnEPrescriptionClicked(_ sender:UIButton){
        let prescriptionVC = MAPTermsVC(nibName: "MAPTermsVC", bundle: nil)
        APPDELEGATE.prescriptionData.setValue(storeID, forKey: "storeID")
        APPDELEGATE.prescriptionData.setValue(lblStoreName.text, forKey: "storeName")
        APPDELEGATE.prescriptionData.setValue(lblStoreAddress.text, forKey: "storeAddress")
        APPDELEGATE.prescriptionData.setValue(storelatitude, forKey: "storeLatitude")
        APPDELEGATE.prescriptionData.setValue(storeLongitude, forKey: "storeLongitude")
        self.navigationController?.pushViewController(prescriptionVC, animated: true)
        
    }
    
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForAddFavouriteStore(){
        let completeURL = NSString(format:"%@%@", MainURL,addFavouriteStoreURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "store_id":storeID
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("AddFavouriteStore API Parameter :",finalParams)
        print("AddFavouriteStore API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: addFavouriteStoreURLTag)
    }
    
    func postDataOnWebserviceForRemoveFavouriteStore(){
        let completeURL = NSString(format:"%@%@", MainURL,removeFavouriteStoreURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "store_id":storeID
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("RemoveFavouriteStore API Parameter :",finalParams)
        print("RemoveFavouriteStore API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: removeFavouriteStoreURLTag)
    }
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
       
            
        case addFavouriteStoreURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("AddFavouriteStore Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                let notificationName = Notification.Name("refreshStoreData")
              //  NotificationCenter.default.post(name: notificationName, object: nil)
                NotificationCenter.default.post(name: notificationName, object: nil, userInfo: self.storeDetailData as? [AnyHashable : Any])

            }
            break
            
        case removeFavouriteStoreURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("RemoveFavouriteStore Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                let notificationName = Notification.Name("refreshStoreData")
               // NotificationCenter.default.post(name: notificationName, object: nil)
                NotificationCenter.default.post(name: notificationName, object: nil, userInfo: self.storeDetailData as? [AnyHashable : Any])

            }
            break
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
   
        case addFavouriteStoreURLTag:
            break
        case removeFavouriteStoreURLTag:
            break
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    

    
    
}
