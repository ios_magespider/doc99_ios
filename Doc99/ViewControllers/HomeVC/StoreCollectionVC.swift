//
//  StoreCollectionVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 03/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class StoreCollectionVC: UICollectionViewCell {

    @IBOutlet var lblStoreName:UILabel!
    @IBOutlet var lblSoreTitle:UILabel!
    @IBOutlet var lblTime:UILabel!
    @IBOutlet var imageViewFavourite:UIImageView!
    @IBOutlet var btnFavourite:UIButton!
    @IBOutlet weak var NSLCLeftArrowWidth: NSLayoutConstraint!
    
    @IBOutlet weak var NSLCRightArrowWidth: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
