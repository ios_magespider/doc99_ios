//
//  SearchStoreVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 18/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class SearchStoreVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,UIScrollViewDelegate {

    @IBOutlet var tableViewStore:UITableView!
    @IBOutlet var lblNoStore:UILabel!
    
    @IBOutlet var tableViewDoctor:UITableView!
    @IBOutlet var lblNoDoctor:UILabel!

    
    var storeData = NSMutableArray()
    var storeGlobalData = NSMutableArray()
    
    var doctorData = NSMutableArray()
    var doctorGlobalData = NSMutableArray()
    
    var doctorID:String!
    var storeID:String!

    var isFromStore:Bool?

    @IBOutlet var txtSearch:UITextField!

    var singleStoreData = NSDictionary()

    
    // FIXME: - VIEW CONTROLLER METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
        
    }
    

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        
        
        if isFromStore == true{
            tableViewStore.isHidden = false
            tableViewDoctor.isHidden = true
            txtSearch.attributedPlaceholder = NSAttributedString(string:"Search Store",attributes:nil)
            
            if Reachability.isConnectedToNetwork() == true {
                SVProgressHUD.show(withStatus: "Loading..")
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForStoreList), userInfo: nil, repeats: false)
            } else {
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }

        }
        else{
            tableViewDoctor.isHidden = false
            tableViewStore.isHidden = true
            txtSearch.attributedPlaceholder = NSAttributedString(string:"Search Doctor",attributes:nil)


            if Reachability.isConnectedToNetwork() == true {
                SVProgressHUD.show(withStatus: "Loading..")
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForDoctorList), userInfo: nil, repeats: false)
            } else {
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }

        }
        
        self.addTapGestureInOurView()
        self.setLocalizationText()
    }
    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        if viewTouched!.isKind(of: UIButton.self){
            
        }
        else{
            self.view.endEditing(true)
        }
    }

    
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }

    func setLocalizationText(){
        lblNoStore.text = NSLocalizedString("nostorefound", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblNoDoctor.text = NSLocalizedString("nodoctorfound", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
    }
    
    // TODO: - DELEGATE METHODS
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        return false
    }
    
    
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }
    

    
    //TableView Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewStore{
                return self.storeData.count
        }
        else{
                return self.doctorData.count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableViewStore{
            let identifier = "favouriteStoreCell"
            
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FvouriteStoreTableVC
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("FvouriteStoreTableVC", owner: self, options: nil)
                cell = nib?[0] as? FvouriteStoreTableVC
            }
            cell!.selectionStyle = .none;
            
            
            
            
            if let storename = (self.storeData[indexPath.row] as AnyObject).value(forKey: "firstname") as? String{
                cell?.lblStoreName.text = storename
            }
            
            if let storeaddress = (self.storeData[indexPath.row] as AnyObject).value(forKey: "address") as? String{
                cell?.lblStoreDescription.text = storeaddress
            }
            
            
            if let favour =  (self.storeData[indexPath.row] as AnyObject).value(forKey: "is_favourite") as? NSNumber
            {
                let favourateArtical = "\(favour)"
                if favourateArtical == "1"{
                    cell?.imageViewFavourite.image = UIImage.init(named: "like.png")
                }
                else if favourateArtical == "0"{
                    cell?.imageViewFavourite.image = UIImage.init(named: "unlike.png")
                }
            }
            else if let favour =  (self.storeData[indexPath.row] as AnyObject).value(forKey: "is_favourite") as? String
            {
                let favourateArtical = "\(favour)"
                if favourateArtical == "1"{
                    cell?.imageViewFavourite.image = UIImage.init(named: "like.png")
                }
                else if favourateArtical == "0"{
                    cell?.imageViewFavourite.image = UIImage.init(named: "unlike.png")
                }
            }
            
            
            cell?.btnFavourite.tag = indexPath.row
            cell?.btnFavourite.addTarget(self, action: #selector(self.btnFavouriteStoredClicked(_:)), for: .touchUpInside)
            
            
            return cell!

            
        }
        else{
            let identifier = "favouriteStoreCell"
            
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FvouriteStoreTableVC
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("FvouriteStoreTableVC", owner: self, options: nil)
                cell = nib?[0] as? FvouriteStoreTableVC
            }
            cell!.selectionStyle = .none;
            
            
            
            
            if let doctorname = (self.doctorData[indexPath.row] as AnyObject).value(forKey: "first_name") as? String{
                cell?.lblStoreName.text = doctorname
            }
            
            if let doctoraddress = (self.doctorData[indexPath.row] as AnyObject).value(forKey: "address") as? String{
                cell?.lblStoreDescription.text = doctoraddress
            }
            
            
            if let favour =  (self.doctorData[indexPath.row] as AnyObject).value(forKey: "is_favourite") as? NSNumber
            {
                let favourateArtical = "\(favour)"
                if favourateArtical == "1"{
                    cell?.imageViewFavourite.image = UIImage.init(named: "like.png")
                }
                else if favourateArtical == "0"{
                    cell?.imageViewFavourite.image = UIImage.init(named: "unlike.png")
                }
            }
            else if let favour =  (self.doctorData[indexPath.row] as AnyObject).value(forKey: "is_favourite") as? String
            {
                let favourateArtical = "\(favour)"
                if favourateArtical == "1"{
                    cell?.imageViewFavourite.image = UIImage.init(named: "like.png")
                }
                else if favourateArtical == "0"{
                    cell?.imageViewFavourite.image = UIImage.init(named: "unlike.png")
                }
            }
            
            
            cell?.btnFavourite.tag = indexPath.row
            cell?.btnFavourite.addTarget(self, action: #selector(self.btnFavouriteDoctorClicked(_:)), for: .touchUpInside)
            
            
            return cell!

        }
        
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableViewStore {
//            let storeDetailVC = StoreDetailVC(nibName: "StoreDetailVC", bundle: nil)
//            storeDetailVC.storeDetailData = self.storeData[indexPath.row] as! NSDictionary
//            self.navigationController?.pushViewController(storeDetailVC, animated: true)
            
        }else if tableView.tag == 101{
            //let userChefScheduleVC = UserChefScheduleViewController(nibName: "UserChefScheduleViewController", bundle: nil)
            
            //userChefScheduleVC.chefID = (((self.favouriteChefData[indexPath.row] as AnyObject).value(forKey: "chef_id") as AnyObject).value(forKey: "_id") as AnyObject) as? String
            //self.navigationController?.pushViewController(userChefScheduleVC, animated: true)
            
        }
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 80.0
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        txtSearch.resignFirstResponder()
    }
    
    // TODO: - ACTION METHODS
    @IBAction func btnFavouriteStoredClicked(_ sender:UIButton){
        
        let ifFav = (self.storeData[sender.tag] as AnyObject).value(forKey: "is_favourite") as? String
        storeID = (self.storeData[sender.tag] as AnyObject).value(forKey: "store_id") as? String
        
        if ifFav == "1"{
            let temp = (self.storeData[sender.tag] as! NSDictionary).mutableCopy() as! NSMutableDictionary
            print(temp)
            temp.setValue("0", forKey: "is_favourite")
            
            singleStoreData = temp.mutableCopy() as! NSDictionary
            print("Single Store Data : ",singleStoreData)
            
            let notificationName = Notification.Name("refreshFavouriteStoreData")
            //                NotificationCenter.default.post(name: notificationName, object: nil)
            NotificationCenter.default.post(name: notificationName, object: nil, userInfo: self.singleStoreData as? [AnyHashable : Any])
            
            if (self.storeData.count > sender.tag) {
                self.storeData.replaceObject(at: sender.tag, with: temp)
                self.storeGlobalData.replaceObject(at: sender.tag, with: temp)
                let indexPath = NSIndexPath(row: sender.tag, section: 0)
                tableViewStore.reloadRows(at: [indexPath as IndexPath], with: .none)
            }
            
            
            if Reachability.isConnectedToNetwork() == true {
                self.performSelector(inBackground: #selector(self.postDataOnWebserviceForRemoveFavouriteStore), with: nil)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
        }
        else{
            let temp = (self.storeData[sender.tag] as! NSDictionary).mutableCopy() as! NSMutableDictionary
            print(temp)
            temp.setValue("1", forKey: "is_favourite")
            
            singleStoreData = temp.mutableCopy() as! NSDictionary
            print("Single Store Data : ",singleStoreData)
            let notificationName = Notification.Name("refreshFavouriteStoreData")
            //                NotificationCenter.default.post(name: notificationName, object: nil)
            NotificationCenter.default.post(name: notificationName, object: nil, userInfo: self.singleStoreData as? [AnyHashable : Any])

            
            
            if (self.storeData.count > sender.tag) {
                self.storeData.replaceObject(at: sender.tag, with: temp)
                self.storeGlobalData.replaceObject(at: sender.tag, with: temp)
                let indexPath = NSIndexPath(row: sender.tag, section: 0)
                tableViewStore.reloadRows(at: [indexPath as IndexPath], with: .none)
            }
            
            if Reachability.isConnectedToNetwork() == true {
                self.performSelector(inBackground: #selector(self.postDataOnWebserviceForAddFavouriteStore), with: nil)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
        }
    }
    
    @IBAction func btnFavouriteDoctorClicked(_ sender:UIButton){
        
        let ifFav = (self.doctorData[sender.tag] as AnyObject).value(forKey: "is_favourite") as? String
        doctorID = (self.doctorData[sender.tag] as AnyObject).value(forKey: "d_id") as? String
        
        if ifFav == "1"{
            let temp = (self.doctorData[sender.tag] as! NSDictionary).mutableCopy() as! NSMutableDictionary
            print(temp)
            temp.setValue("0", forKey: "is_favourite")
            
            if (self.doctorData.count > sender.tag) {
                self.doctorData.replaceObject(at: sender.tag, with: temp)
                self.doctorGlobalData.replaceObject(at: sender.tag, with: temp)
                let indexPath = NSIndexPath(row: sender.tag, section: 0)
                tableViewDoctor.reloadRows(at: [indexPath as IndexPath], with: .none)
            }
            
            
            if Reachability.isConnectedToNetwork() == true {
                self.performSelector(inBackground: #selector(self.postDataOnWebserviceForRemoveFavouriteDoctor), with: nil)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
        }
        else{
            let temp = (self.doctorData[sender.tag] as! NSDictionary).mutableCopy() as! NSMutableDictionary
            print(temp)
            temp.setValue("1", forKey: "is_favourite")
            
            if (self.doctorData.count > sender.tag) {
                self.doctorData.replaceObject(at: sender.tag, with: temp)
                self.doctorGlobalData.replaceObject(at: sender.tag, with: temp)
                let indexPath = NSIndexPath(row: sender.tag, section: 0)
                tableViewDoctor.reloadRows(at: [indexPath as IndexPath], with: .none)
            }
            
            if Reachability.isConnectedToNetwork() == true {
                self.performSelector(inBackground: #selector(self.postDataOnWebserviceForAddFavouriteDoctor), with: nil)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
        }
    }

    @IBAction func searchFilter(){
        
        if isFromStore == true{
            var predicate = String()
            
            
            if (txtSearch.text == "") {
                storeData = storeGlobalData.mutableCopy() as! NSMutableArray
            }else{
                predicate = String(format: "SELF['firstname'] contains[c] '%@'", txtSearch.text!)
                print(predicate)
                
                let myPredicate:NSPredicate = NSPredicate(format:predicate)
                let temp = storeGlobalData.mutableCopy() as! NSArray
                storeData = (temp.filtered(using: myPredicate) as NSArray).mutableCopy() as! NSMutableArray
                
                
            }
            
            if storeData.count == 0 {
                lblNoStore.isHidden = false
                tableViewStore.isHidden = true
            }
            else{
                tableViewStore.isHidden = false
                lblNoStore.isHidden = true
            }
            tableViewStore.reloadData()
        }
        else{
            var predicate = String()
            
            
            if (txtSearch.text == "") {
                doctorData = doctorGlobalData.mutableCopy() as! NSMutableArray
            }else{
                predicate = String(format: "SELF['first_name'] contains[c] '%@'", txtSearch.text!)
                print(predicate)
                
                let myPredicate:NSPredicate = NSPredicate(format:predicate)
                let temp = doctorGlobalData.mutableCopy() as! NSArray
                doctorData = (temp.filtered(using: myPredicate) as NSArray).mutableCopy() as! NSMutableArray
                
                
            }
            
            if doctorData.count == 0 {
                lblNoDoctor.isHidden = false
                tableViewDoctor.isHidden = true
            }
            else{
                tableViewDoctor.isHidden = false
                lblNoDoctor.isHidden = true
            }
            tableViewDoctor.reloadData()
        }
        
        
        
        
    }

    @IBAction func btnBackClicked(_ sender:UIButton){
      _ = self.navigationController?.popViewController(animated: true)
    }
    
    // TODO: - POST DATA METHODS
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForStoreList(){
        let completeURL = NSString(format:"%@%@", MainURL,getStoreListURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "latitude":APPDELEGATE.GLOBAL_latitude,
            "longtitude":APPDELEGATE.GLOBAL_longtitude,
            "miles":MILE
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("StoreList API Parameter :",finalParams)
        print("StoreList API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getStoreListURLTag)
    }
    
    func postDataOnWebserviceForDoctorList(){
        let completeURL = NSString(format:"%@%@", MainURL,getDoctorListURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "latitude":APPDELEGATE.GLOBAL_latitude,
            "longtitude":APPDELEGATE.GLOBAL_longtitude,
            "miles":MILE
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("DoctorList API Parameter :",finalParams)
        print("DoctorList API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getDoctorListURLTag)
    }
    
    func postDataOnWebserviceForAddFavouriteStore(){
        let completeURL = NSString(format:"%@%@", MainURL,addFavouriteStoreURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "store_id":storeID
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("AddFavouriteStore API Parameter :",finalParams)
        print("AddFavouriteStore API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: addFavouriteStoreURLTag)
    }
    
    func postDataOnWebserviceForRemoveFavouriteStore(){
        let completeURL = NSString(format:"%@%@", MainURL,removeFavouriteStoreURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "store_id":storeID
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("RemoveFavouriteStore API Parameter :",finalParams)
        print("RemoveFavouriteStore API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: removeFavouriteStoreURLTag)
    }
    
    func postDataOnWebserviceForAddFavouriteDoctor(){
        let completeURL = NSString(format:"%@%@", MainURL,addFavouriteDoctorURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "d_id":doctorID
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("AddFavouriteStore API Parameter :",finalParams)
        print("AddFavouriteStore API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: addFavouriteDoctorURLTag)
    }
    
    func postDataOnWebserviceForRemoveFavouriteDoctor(){
        let completeURL = NSString(format:"%@%@", MainURL,removeFavouriteDoctorURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "d_id":doctorID
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("RemoveFavouriteStore API Parameter :",finalParams)
        print("RemoveFavouriteStore API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: removeFavouriteDoctorURLTag)
    }
    
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case getStoreListURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("StoreList Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                storeData = NSMutableArray(array: resultDict.value(forKey: "data") as! NSArray)
                storeGlobalData = NSMutableArray(array: resultDict.value(forKey: "data") as! NSArray)
                
                if storeData.count == 0 {
                    lblNoStore.isHidden = false
                    tableViewStore.isHidden = true
                }
                else{
                    tableViewStore.isHidden = false
                    lblNoStore.isHidden = true
                }
                tableViewStore.reloadData()
                

            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            SVProgressHUD.dismiss()
            tableViewStore.reloadData()
            break
            
        case getDoctorListURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("DoctorList Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                doctorData = NSMutableArray(array: resultDict.value(forKey: "data") as! NSArray)
                doctorGlobalData = NSMutableArray(array: resultDict.value(forKey: "data") as! NSArray)
                
                if doctorData.count == 0 {
                    lblNoDoctor.isHidden = false
                    tableViewDoctor.isHidden = true
                }
                else{
                    tableViewDoctor.isHidden = false
                    lblNoDoctor.isHidden = true
                }
                tableViewDoctor.reloadData()
                

                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            SVProgressHUD.dismiss()
            tableViewDoctor.reloadData()
            break
            
        case addFavouriteStoreURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("AddFavouriteStore Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                
                

            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }

            break
            
        case removeFavouriteStoreURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("RemoveFavouriteStore Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
              
                

            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }

            break
            
        case addFavouriteDoctorURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("AddFavouriteDoctor Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }

            break
            
        case removeFavouriteDoctorURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("RemoveFavouriteDoctor Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }

            break

            
            
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case getStoreListURLTag:
            SVProgressHUD.dismiss()
            break
        case getDoctorListURLTag:
            SVProgressHUD.dismiss()
            break
        case addFavouriteStoreURLTag:
            break
        case removeFavouriteStoreURLTag:
            break
        case addFavouriteDoctorURLTag:
            break
        case removeFavouriteDoctorURLTag:
            break
            
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }

}
