//
//  UserHomeVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 30/03/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD
import MapKit
import SDWebImage


class UserHomeVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,MKMapViewDelegate,UIAlertViewDelegate,UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource {

    let reuseIdentifier = "storeCell"
    @IBOutlet var collectionViewStore:UICollectionView!
    @IBOutlet var collectionViewDoctor:UICollectionView!

    
    var storeData = NSMutableArray()
    var storeGlobalData = NSMutableArray()

    var doctorData = NSMutableArray()
    var doctorGlobalData = NSMutableArray()
    
    var mapData = NSMutableArray()


    var doctorID:String!
    var storeID:String!
    
    var isHeaderDoctorStore:Bool?

    @IBOutlet var btnHeaderDoctorStore:UIButton!

    @IBOutlet var myMapView:MKMapView!

    @IBOutlet weak var btnMyLocation: UIButton!
    @IBOutlet var btnHeaderStore:UIButton!
    @IBOutlet var btnHeaderDoctor:UIButton!
    
    //MARK:- HP
    @IBOutlet weak var viewCountryBG: UIView!
    @IBOutlet weak var labelSelectedCountry: UILabel!
    @IBOutlet weak var btnSelectCountry: UIButton!
    @IBOutlet weak var viewDropDown: UIView!
    @IBOutlet weak var tableCountryList: UITableView!
    
    var isCountryListOpen = false
    
    
    var arrayCountryCode : NSArray = [["code":"852","country":"Hong Kong"],["code":"65","country":"Singapore"],["code":"60","country":"Malaysia"],
                                      ["code":"66","country":"Thailand"],["code":"850","country":"Korea"],["code":"81","country":"Japan"],
                                      ["code":"853","country":"Macau"],["code":"86","country":"China"],["code":"91","country":"India"],
                                      ["code":"1","country":"USA"],["code":"1","country":"Canada"],["code":"61","country":"Australia"]]
    
    var selectedCountry = "Hong Kong"
    
    var selectedType = "store"
    
    
    var currentPinTag = 0
    var didSelect = false
    
    var pagenum = 1
    var isLoading = true
    
    
    @IBOutlet weak var viewBtnOption: UIView!
    @IBOutlet weak var btnStore: UIButton!
    @IBOutlet weak var btnClinic: UIButton!
    
    
    
    //MARK:- HP
    
    //MARK:-
    var qtree:QTree!

    var myIndexValue:Int!

    @IBOutlet var loginView:UIView!
    
    @IBOutlet var viewSaleCounter:UIView!
    
    
    @IBOutlet var collectionViewSaleCounter:UICollectionView!
    let reuseIdentifier1 = "articalWelcomeCell"
    
    var healthData = NSMutableArray()
    var healthGlobalData = NSMutableArray()
    var boolArrayForCategory:NSMutableArray!
    var healthCategory:NSMutableArray!
    var saleSelectionCount:Int!
    @IBOutlet var lblSaleCounter:UILabel!

    var healthConcernData = NSArray()

    var refreshControl: UIRefreshControl!

    //LOGIN POPUP
    @IBOutlet var loginPopupLabel2:UILabel!
    @IBOutlet var loginPopupLabel3:UILabel!

    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        /*
        if USERDEFAULT.value(forKey: "userID") != nil{
            if let isMember = USERDEFAULT.value(forKey: "isMember") as? String{
                let ismem = "\(isMember)"
                if ismem == "1"{
                    self.generalViewControllerSetting()
                }
            }
        }
        */
        
        
        viewCountryBG.clipsToBounds = true
        viewCountryBG.layer.cornerRadius = 20.0
        viewCountryBG.dropShadow(color: .black, opacity: 0.5, offSet: CGSize(width: 0, height: 10), radius: 10, scale: true)
        
        tableCountryList.clipsToBounds = true
        tableCountryList.layer.cornerRadius = 20.0
        
        viewDropDown.clipsToBounds = true
        viewDropDown.layer.cornerRadius = 20.0
        viewDropDown.dropShadow(scale: true, radius: 20)
        
        
        viewBtnOption.clipsToBounds = true
        viewBtnOption.layer.cornerRadius = viewBtnOption.frame.size.height/2
        
        
        self.btnMyLocationClicked(btnMyLocation)
        
        // Define identifier
        let notificationName1 = Notification.Name("refreshStoreData")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(refreshStoreData(notification:)), name: notificationName1, object: nil)
        
        // Define identifier
        let notificationName2 = Notification.Name("refreshDoctorData")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(refreshDoctorData(notification:)), name: notificationName2, object: nil)
        
        // Define identifier
        let notificationName3 = Notification.Name("refreshFavouriteStoreData")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(favStoreDataRefresh(notification:)), name: notificationName3, object: nil)
        
        // Define identifier
        let notificationName4 = Notification.Name("refreshFavouriteDoctorData")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(favDoctorDataRefresh(notification:)), name: notificationName4, object: nil)
        

        // Define identifier
        let notificationName5 = Notification.Name("updateHealthConcernDataNotification")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateHealthConcernData), name: notificationName5, object: nil)

        
        // Define identifier
        let notificationName6 = Notification.Name("updateAfterMemberShip")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateAfterMembershipCompleted), name: notificationName6, object: nil)
        
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = false
        
        if USERDEFAULT.value(forKey: "userID") != nil{
            
//            if let firstTIme = USERDEFAULT.value(forKey: "isSaleCounterSearch") as? String{
//                let first = "\(firstTIme)"
//                if first == "NO"{
//                    viewSaleCounter.removeFromSuperview()
//                }
//                else{
//                    self.view.addSubview(viewSaleCounter)
//                }
//            }
//            else{
//                self.view.addSubview(viewSaleCounter)
//            }
            
            if let isMember = USERDEFAULT.value(forKey: "isMember") as? String{
                let ismem = "\(isMember)"
                if ismem == "1"{
                    //old code already commented
//                    loginView.removeFromSuperview()
//                    self.generalViewControllerSetting()
                    //--------
                }
                else{
                    //commented by Hitesh
//                    loginPopupLabel3.text = "MEMBERSHIP"
//                    self.view.addSubview(loginView)
                }
            }
            else{
                
            }
        }
        else{
            loginPopupLabel3.text = "LOGIN"
            loginView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
            self.view.addSubview(loginView)
        }
        

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:-
    
    @IBAction func btnSelectCountryClicked(_ sender: UIButton) {
        if !isCountryListOpen{
            isCountryListOpen = true
            viewCountryBG.dropShadow(color: .black, opacity: 0.5, offSet: CGSize(width: 0, height: 5), radius: 10, scale: true)
            UIView.animate(withDuration: 0.3, animations: {
                self.viewDropDown.frame = CGRect(x: self.viewDropDown.frame.origin.x,
                                                 y: self.viewDropDown.frame.origin.y,
                                                 width: self.viewDropDown.frame.size.width, height: 280)
            }) { (x) in
                self.viewDropDown.dropShadow(color: .darkGray, opacity: 0.5, offSet: CGSize(width: 0, height: 10), radius: 15, scale: true)
            }
            
        }
        else{
            isCountryListOpen = false
            
            self.viewDropDown.dropShadow(color: .clear, opacity: 0, offSet: CGSize(width: 0, height: 0), radius: 0, scale: true)
            viewCountryBG.dropShadow(color: .black, opacity: 0.5, offSet: CGSize(width: 0, height: 10), radius: 10, scale: true)
            UIView.animate(withDuration: 0.3) {
                self.viewDropDown.frame = CGRect(x: self.viewDropDown.frame.origin.x,
                                                     y: self.viewDropDown.frame.origin.y,
                                                     width: self.viewDropDown.frame.size.width, height: 0)
            }
        }
    }
    
   
    @IBAction func btnStoreClicked(_ sender: UIButton) {
        
        if Reachability.isConnectedToNetwork(){
            currentPinTag = 0
            
            if storeData.count > 0
            {
                storeData.removeAllObjects()
            }
            
            if storeGlobalData.count > 0{
                storeGlobalData.removeAllObjects()
            }
            
            
            if mapData.count > 0{
                mapData.removeAllObjects()
            }
            
            
            self.settingClusteringOnMap()
            collectionViewStore.reloadData()
            
            btnStore.setImage(#imageLiteral(resourceName: "store_blue.png"), for: .normal)  //store_blue
            btnClinic.setImage(#imageLiteral(resourceName: "pharmacy gray.png"), for: .normal) //Pharmacy gray.png
            
            selectedType = "store"
            pagenum = 1
            self.postDataOnWebserviceForStoreList()
        }
        else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
    }
    
    @IBAction func btnClinicCLicked(_ sender: UIButton) {
        
        if Reachability.isConnectedToNetwork(){
            currentPinTag = 0
            
            if storeData.count > 0
            {
                storeData.removeAllObjects()
            }
            
            if storeGlobalData.count > 0{
                storeGlobalData.removeAllObjects()
            }
            
            
            if mapData.count > 0{
                mapData.removeAllObjects()
            }
            
            
            self.settingClusteringOnMap()
            collectionViewStore.reloadData()
            
            btnStore.setImage(#imageLiteral(resourceName: "store_gray.png"), for: .normal)  //store_gray
            btnClinic.setImage(#imageLiteral(resourceName: "pharmasist1.png"), for: .normal) //pharmasist1.png
            
            selectedType = "clinic"
            pagenum = 1
            self.postDataOnWebserviceForStoreList()
        }
        else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
    }
    
    
    // FIXME: - VIEW CONTROLLER METHODS
    // TODO: - OTHER METHODS
    
    func updateAfterMembershipCompleted() {
        self.generalViewControllerSetting()
        loginView.removeFromSuperview()
    }
    
    func generalViewControllerSetting(){
        
        lblSaleCounter.layer.borderColor = UIColor.gray.cgColor
        lblSaleCounter.layer.borderWidth = 1.0
        lblSaleCounter.layer.cornerRadius = 10.0
        
        

        
        self.collectionViewSaleCounter.register(UINib(nibName: "ArticalWelcomeCollectionVC", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier1)

        self.collectionViewStore.register(UINib(nibName: "StoreCollectionVC", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionViewDoctor.register(UINib(nibName: "StoreCollectionVC", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)

        if Reachability.isConnectedToNetwork() == true{
            if ((USERDEFAULT.value(forKey: "userID") as? String) != nil){
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForStoreList), userInfo: nil, repeats: false)
            }
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        self.healthData = NSMutableArray()
        self.healthGlobalData = NSMutableArray()
        

        
//        if Reachability.isConnectedToNetwork() == true {
////            self.addLoadingIndicatiorOnFooterOnTableView()
//            SVProgressHUD.show(withStatus: "Loading..")
//            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForHealthConcern), userInfo: nil, repeats: false)
//        }else{
//            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
//        }
//        
//        
//        if Reachability.isConnectedToNetwork() == true {
//            self.performSelector(inBackground: #selector(self.postDataOnWebserviceForGetProfile), with: nil)
//        } else {
//            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
//        }
        
        
//        if Reachability.isConnectedToNetwork() == true {
//            SVProgressHUD.show(withStatus: "Loading..")
//            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForDoctorList), userInfo: nil, repeats: false)
//        } else {
//            showAlert(CheckConnection, title: InternetError)
//        }
//
        self.setLocalizationText()
        self.addPullRefreshForHealthConcern()
        self.setiPhone4sDeviceSupport()
    }
    
    func setiPhone4sDeviceSupport() {
        if DeviceType.IS_IPHONE_4_OR_LESS{
            viewSaleCounter.frame = CGRect(x: viewSaleCounter.frame.origin.x, y: viewSaleCounter.frame.origin.y, width: viewSaleCounter.frame.size.width, height: viewSaleCounter.frame.size.height - 88 )
        }
        
    }

    
    func addPullRefreshForHealthConcern(){
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.gray
        refreshControl.addTarget(self, action: #selector(self.refreshHealthConcern(sender:)), for: .valueChanged)
        collectionViewSaleCounter.addSubview(refreshControl)
    }
    
    func refreshHealthConcern(sender:AnyObject) {
        // Code to refresh table view
       
        self.healthData = NSMutableArray()
        self.healthGlobalData = NSMutableArray()
        self.collectionViewSaleCounter.reloadData()
        
        if Reachability.isConnectedToNetwork() == true {
            //            self.addLoadingIndicatiorOnFooterOnTableView()
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForHealthConcern), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        if Reachability.isConnectedToNetwork() == true {
            self.performSelector(inBackground: #selector(self.postDataOnWebserviceForGetProfile), with: nil)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        refreshControl.endRefreshing()
    }
    
    func recenterMapToPlacemark(strLatitude:String,strLongtitude:String,isChange:Bool){
        var latitudeAPI:Double!
        var longtitudeAPI:Double!
        
        if strLatitude != "" || strLongtitude != ""{
            latitudeAPI = Double(strLatitude)
            longtitudeAPI = Double(strLongtitude)
        }
        else{
            latitudeAPI = Double(0.0)
            longtitudeAPI = Double(0.0)
        }
        
        
        
        let myCurrentLocationCordinate:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitudeAPI, longtitudeAPI)
        
        myMapView.setCenter(myCurrentLocationCordinate, animated: true)
        
        
        
        // let region = MKCoordinateRegionMakeWithDistance(myCurrentLocationCordinate, 5000, 5000)
    //        let span = MKCoordinateSpanMake(0.05, 0.05)
    //        let region = MKCoordinateRegion(center: myCurrentLocationCordinate, span: span)
    //        myMapView.setRegion(region, animated: true)
        
        if isChange == true{
            let viewRegion = MKCoordinateRegionMakeWithDistance(myCurrentLocationCordinate, 5000, 5000)
            if(viewRegion.center.longitude == -180.00000000){
                print("Invalid region!")
                return;
            }
            
            let adjustedRegion: MKCoordinateRegion = myMapView.regionThatFits(viewRegion)
            
            if(adjustedRegion.center.longitude == -180.00000000){
                print("Invalid region!")
            }else{
                print("Valid region!")
                myMapView.setRegion(adjustedRegion, animated: true)
            }
        }
        
    }
    
    func refreshStoreData(notification: NSNotification) {
        
        let dict  = NSDictionary(dictionary: notification.userInfo!) as NSDictionary
        
        print("Store notificationDict",dict )
        print(" Store notificationDict",storeData)
        
     //   print("notification store_id",dict.value(forKey: "store_id") as! String)

        
       // let myIndexValue:Int = self.storeData.index(of: dict.value(forKey: "store_id")!)
        if (self.storeData.count > myIndexValue) {
            self.storeData.replaceObject(at: myIndexValue, with: dict)
            self.storeGlobalData.replaceObject(at: myIndexValue, with: dict)
            let indexPath = NSIndexPath(row: myIndexValue, section: 0)
            collectionViewStore.reloadItems(at: [indexPath as IndexPath])
        }
        
        self.reloadAnnotations()

    }
    
    func refreshDoctorData(notification: NSNotification) {
        
        let dict  = NSMutableDictionary(dictionary: notification.userInfo!) as NSMutableDictionary
        
        print(" Doctor notificationDict",dict )
        print(" Doctor notificationDict",self.doctorData)
        print("notification DoctorID",dict.value(forKey: "d_id") as! String)

      //  let aa = dict.value(forKey: "d_id") as! String
        
     //   let myIndexValue:Int = self.doctorData.index(of:aa)
        
        if (self.doctorData.count > myIndexValue) {
            self.doctorData.replaceObject(at: myIndexValue, with: dict)
            self.doctorGlobalData.replaceObject(at: myIndexValue, with: dict)
            let indexPath = NSIndexPath(row: myIndexValue, section: 0)
            collectionViewDoctor.reloadItems(at: [indexPath as IndexPath])
        }
        
        self.reloadAnnotations()

    }
    
    func favStoreDataRefresh(notification: NSNotification) {
        
        let dict  = NSDictionary(dictionary: notification.userInfo!) as NSDictionary
        
        print("Store notificationDict",dict )
        print(" Store notificationDict",storeData)
        
        print("notification store_id",dict.value(forKey: "store_id") as! String)
        
        let temp = NSMutableArray()
        
        for i in 0..<self.storeData.count{
            let id = (self.storeData[i] as AnyObject).value(forKey: "store_id") as! String
            temp.add("\(id)")
        }
        print(temp)
        
        
        let myIndexValue1:Int = temp.index(of: dict.value(forKey: "store_id") as! String)
        if (self.storeData.count > myIndexValue1) {
            self.storeData.replaceObject(at: myIndexValue1, with: dict)
            self.storeGlobalData.replaceObject(at: myIndexValue1, with: dict)
            let indexPath = NSIndexPath(row: myIndexValue1, section: 0)
            collectionViewStore.reloadItems(at: [indexPath as IndexPath])
        }
        
        self.reloadAnnotations()
        
        print("Update Store Data",storeData)

    }

    func favDoctorDataRefresh(notification: NSNotification) {
        
        let dict  = NSDictionary(dictionary: notification.userInfo!) as NSDictionary
        
        print("Doctor notificationDict",dict )
        print(" Doctor notificationDict",doctorData)
        
        print("notification Doctor ID",dict.value(forKey: "d_id") as! String)
        
        let temp = NSMutableArray()
        
        for i in 0..<self.doctorData.count{
            let id = (self.doctorData[i] as AnyObject).value(forKey: "d_id") as! String
            temp.add("\(id)")
        }
        print(temp)
        
        
        let myIndexValue1:Int = temp.index(of: dict.value(forKey: "d_id") as! String)
        if (self.doctorData.count > myIndexValue1) {
            self.doctorData.replaceObject(at: myIndexValue1, with: dict)
            self.doctorGlobalData.replaceObject(at: myIndexValue1, with: dict)
            let indexPath = NSIndexPath(row: myIndexValue1, section: 0)
            collectionViewDoctor.reloadItems(at: [indexPath as IndexPath])
        }
        
        self.reloadAnnotations()
        
        print("Update Doctor Data",doctorData)
        
    }
    

    
    
    
//    func storeDoctorDataChange(notification: NSNotification) {
//        
//        if Reachability.isConnectedToNetwork() == true {
//            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForStoreList), userInfo: nil, repeats: false)
//        } else {
//            showAlert(CheckConnection, title: InternetError)
//        }
//
//
//        if Reachability.isConnectedToNetwork() == true {
//            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForDoctorList), userInfo: nil, repeats: false)
//        } else {
//            showAlert(CheckConnection, title: InternetError)
//        }
//        
//    }
    
    
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    
    func setLocalizationText(){
        btnHeaderStore.setTitle(NSLocalizedString("store", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        btnHeaderDoctor.setTitle(NSLocalizedString("doctor", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        
        loginPopupLabel2.text = NSLocalizedString("Just enrolled to Doc99 app to experience it amazing features.", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        loginPopupLabel3.text = NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
    }

    func updateHealthConcernData(){
        
        self.healthData = NSMutableArray()
        self.healthGlobalData = NSMutableArray()
        
        if Reachability.isConnectedToNetwork() == true {
            //            self.addLoadingIndicatiorOnFooterOnTableView()
//            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForHealthConcern), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        if Reachability.isConnectedToNetwork() == true {
            self.performSelector(inBackground: #selector(self.postDataOnWebserviceForGetProfile), with: nil)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        

        
    }

    
    // TODO: - DELEGATE METHODS
    //AlertDelegate Methods protocol
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == collectionViewStore{
            
            if isLoading
            {
                if((scrollView.contentOffset.x + scrollView.frame.size.width) >= scrollView.contentSize.width)
                {
                    if Reachability.isConnectedToNetwork(){
                        pagenum+=1
                        SVProgressHUD.show(withStatus: "Loading...")
                        self.postDataOnWebserviceForStoreList()
                    }
                    else{
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
            }
            
            
            let visibleRect = CGRect.init(origin: collectionViewStore.contentOffset, size: collectionViewStore.bounds.size)
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            let visibleIndexPath = collectionViewStore.indexPathForItem(at: visiblePoint)!
            
            currentPinTag = visibleIndexPath.row
            didSelect = true
            
            self.recenterMapToPlacemark(strLatitude: (self.mapData.object(at: visibleIndexPath.row) as AnyObject).value(forKey: "latitude") as! String, strLongtitude: (self.mapData.object(at: visibleIndexPath.row) as AnyObject).value(forKey: "longtitude") as! String,isChange: true)
            
            /*
            for cell: UICollectionViewCell in collectionViewStore.visibleCells
            {
                var indexPath: IndexPath = collectionViewStore.indexPath(for: cell)!
                print("\(indexPath)")
                
                currentPinTag = indexPath.row
                didSelect = true
                
                self.recenterMapToPlacemark(strLatitude: (self.mapData.object(at: visibleIndexPath.row) as AnyObject).value(forKey: "latitude") as! String, strLongtitude: (self.mapData.object(at: visibleIndexPath.row) as AnyObject).value(forKey: "longtitude") as! String,isChange: true)
            }
            */
        }
    }
    
    // MARK: - MapView Delegate
    func mapView(_ mapView: MKMapView, didFailToLocateUserWithError error: Error) {
        print("didFailToLocateUserWithError",error.localizedDescription)
    }
    func mapViewDidFailLoadingMap(_ mapView: MKMapView, withError error: Error) {
        print("mapViewDidFailLoadingMap",error.localizedDescription)
    }

    
    
    //MARK:- Tableview Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCountryCode.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cellIdentifier")
        cell.selectionStyle = .none
        
        
        cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
        let str = String(format: "%@", (arrayCountryCode[indexPath.row]as! NSDictionary).value(forKey: "country")as? String ?? "")
        
        
        cell.textLabel?.textAlignment = .center
        
        cell.textLabel?.text = str
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if Reachability.isConnectedToNetwork(){
        
            selectedCountry =  String(format: "%@", (arrayCountryCode[indexPath.row]as! NSDictionary).value(forKey: "country")as? String ?? "")
            btnSelectCountryClicked(btnSelectCountry)
            labelSelectedCountry.text = selectedCountry
            
            if storeData.count > 0
            {
                storeData.removeAllObjects()
            }
            
            if storeGlobalData.count > 0{
                storeGlobalData.removeAllObjects()
            }
            
            
            if mapData.count > 0{
                mapData.removeAllObjects()
            }
            
            
            self.settingClusteringOnMap()
            collectionViewStore.reloadData()
            
            self.postDataOnWebserviceForStoreList()
        }
        else{
             showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
    }
    
    
    
    // MARK: - UICollectionViewDataSource protocol
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 100{
            return self.storeData.count
        }
        else if collectionView.tag == 101{
            return self.doctorData.count
        }
        else{
            return self.healthData.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        
        if collectionView.tag == 100{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! StoreCollectionVC
            
            // cell.autoresizingMask = [.flexibleRightMargin, .flexibleLeftMargin, .flexibleBottomMargin,.flexibleTopMargin,.flexibleHeight,.flexibleWidth]
            
            if let storename = (self.storeData[indexPath.row] as AnyObject).value(forKey: "firstname") as? String{
                cell.lblStoreName.text = storename
            }
            
            if let storeaddress = (self.storeData[indexPath.row] as AnyObject).value(forKey: "address") as? String{
                cell.lblSoreTitle.text = storeaddress
            }
            
            
            if let distance = (self.storeData[indexPath.row] as AnyObject).value(forKey: "distance") as? String{
                if distance != ""{
                    let abc =  "\(String.localizedStringWithFormat("%.2f", Float(distance)!))"
                    cell.lblTime.text = "\(abc) Kilometer Away"
                }
            }
            else if let distance1 = (self.storeData[indexPath.row] as AnyObject).value(forKey: "distance") as? NSNumber{
                    let abc =  "\(String.localizedStringWithFormat("%.2f", Float(distance1)))"
                    cell.lblTime.text = "\(abc) Kilometer Away"
            }
            
            
            
            if let favour =  (self.storeData[indexPath.row] as AnyObject).value(forKey: "is_favourite") as? NSNumber
            {
                let favourateArtical = "\(favour)"
                if favourateArtical == "1"{
                    cell.imageViewFavourite.image = UIImage.init(named: "like.png")
                }
                else if favourateArtical == "0"{
                    cell.imageViewFavourite.image = UIImage.init(named: "unlike.png")
                }
            }
            else if let favour =  (self.storeData[indexPath.row] as AnyObject).value(forKey: "is_favourite") as? String
            {
                let favourateArtical = "\(favour)"
                if favourateArtical == "1"{
                    cell.imageViewFavourite.image = UIImage.init(named: "like.png")
                }
                else if favourateArtical == "0"{
                    cell.imageViewFavourite.image = UIImage.init(named: "unlike.png")
                }
            }
            if(indexPath.row == 0)
            {
                cell.NSLCLeftArrowWidth.constant = 0
            }
            else
            {
                cell.NSLCLeftArrowWidth.constant = 20
            }
            if(storeData.count - 1 == indexPath.row)
            {
                cell.NSLCRightArrowWidth.constant = 0
            }
            else
            {
                cell.NSLCRightArrowWidth.constant = 20
            }
            cell.btnFavourite.tag = indexPath.row
            cell.btnFavourite.addTarget(self, action: #selector(self.btnFavouriteStoredClicked(_:)), for: .touchUpInside)
            return cell
        }
        else if collectionView.tag == 101{

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! StoreCollectionVC
            
            // cell.autoresizingMask = [.flexibleRightMargin, .flexibleLeftMargin, .flexibleBottomMargin,.flexibleTopMargin,.flexibleHeight,.flexibleWidth]
            
            if let storename = (self.doctorData[indexPath.row] as AnyObject).value(forKey: "first_name") as? String{
                cell.lblStoreName.text = storename
            }
            
            if let storeaddress = (self.doctorData[indexPath.row] as AnyObject).value(forKey: "address") as? String{
                cell.lblSoreTitle.text = storeaddress
            }
            
            if let distance = (self.doctorData[indexPath.row] as AnyObject).value(forKey: "distance") as? String{
                if distance != ""{
                    let abc =  "\(String.localizedStringWithFormat("%.2f", Float(distance)!))"
                    cell.lblTime.text = "\(abc) Kilometer Away"
                }
            }
            else if let distance1 = (self.doctorData[indexPath.row] as AnyObject).value(forKey: "distance") as? NSNumber{
                let abc =  "\(String.localizedStringWithFormat("%.2f", Float(distance1)))"
                cell.lblTime.text = "\(abc) Kilometer Away"
            }

            
            if let favour =  (self.doctorData[indexPath.row] as AnyObject).value(forKey: "is_favourite") as? NSNumber
            {
                let favourateArtical = "\(favour)"
                if favourateArtical == "1"{
                    cell.imageViewFavourite.image = UIImage.init(named: "like.png")
                }
                else if favourateArtical == "0"{
                    cell.imageViewFavourite.image = UIImage.init(named: "unlike.png")
                }
            }
            else if let favour =  (self.doctorData[indexPath.row] as AnyObject).value(forKey: "is_favourite") as? String
            {
                let favourateArtical = "\(favour)"
                if favourateArtical == "1"{
                    cell.imageViewFavourite.image = UIImage.init(named: "like.png")
                }
                else if favourateArtical == "0"{
                    cell.imageViewFavourite.image = UIImage.init(named: "unlike.png")
                }
            }
            
            cell.btnFavourite.tag = indexPath.row
            cell.btnFavourite.addTarget(self, action: #selector(self.btnFavouriteDoctorClicked(_:)), for: .touchUpInside)
            if(indexPath.row == 0)
            {
                cell.NSLCLeftArrowWidth.constant = 0
            }
            else
            {
                cell.NSLCLeftArrowWidth.constant = 20
            }
            if(doctorData.count - 1 == indexPath.row)
            {
                cell.NSLCRightArrowWidth.constant = 0
            }
            else
            {
                cell.NSLCRightArrowWidth.constant = 20
            }
            return cell

        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier1, for: indexPath) as! ArticalWelcomeCollectionVC
            
            
            if let articalCategoryName =  (self.healthData[indexPath.row] as AnyObject).value(forKey: "hc_name") as? String
            {
                cell.lblName.text = "\(articalCategoryName)"
            }
            
            
            if boolArrayForCategory.object(at: indexPath.row) as! String == "1"{
                cell.imageViewSelect.image = UIImage.init(named: "check_blue.png")
            }
            else{
                cell.imageViewSelect.image = UIImage.init(named: "check_gray.png")
            }
            
            
            if ((self.healthData.object(at: indexPath.row) as AnyObject).value(forKey: "image") as? String == "")
            {
                cell.imageViewSalesCounter.image = UIImage.init(named: "image_placeholder.png")
                cell.activityIndicatorForHealth.isHidden = true
                
            }
            else{
                let imageUrl = (self.healthData.object(at: indexPath.row) as AnyObject).value(forKey: "image") as? String
                
                let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl!) as String
                let url : NSString = fullUrl as NSString
                let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
                let searchURL : NSURL = NSURL(string: urlStr as String)!
                
                cell.activityIndicatorForHealth.isHidden = false
                cell.activityIndicatorForHealth.startAnimating()
                
                
                cell.imageViewSalesCounter.sd_setImage(with: searchURL as URL, completed: { (image:UIImage?, error:Error?, cacheType:SDImageCacheType!, imageURL:URL?) in
                    
                    if ((error) != nil) {
                        cell.imageViewSalesCounter.image = UIImage.init(named: "image_placeholder.png")
                    }
                    cell.activityIndicatorForHealth.isHidden = true
                })
                
            }

            cell.btnSelect.tag = indexPath.row
            cell.btnSelect.addTarget(self, action: #selector(self.btnDrugCategorySelectionClicked(_:)), for: .touchUpInside)
            
            
            
            return cell

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 100{
            let storeDetailVC = StoreDetailVC(nibName: "StoreDetailVC", bundle: nil)
            storeDetailVC.storeDetailData = self.storeData[indexPath.row] as! NSDictionary
            self.myIndexValue = indexPath.row
            self.navigationController?.pushViewController(storeDetailVC, animated: true)
        }
        else if collectionView.tag == 101{
            let doctorDetailVC = DoctorDetailVC(nibName: "DoctorDetailVC", bundle: nil)
            doctorDetailVC.doctorDetailData = self.doctorData[indexPath.row] as! NSDictionary
            self.myIndexValue = indexPath.row
            self.navigationController?.pushViewController(doctorDetailVC, animated: true)
        }
        else{
            
        }
    }
    
    // TODO: - ACTION METHODS
    @IBAction func btnHeaderDoctorStoreClicked(_ sender:UIButton){
        if isHeaderDoctorStore == true{
            isHeaderDoctorStore = false

            
            collectionViewStore.isHidden = false
            collectionViewDoctor.isHidden = true
            
            
            if Reachability.isConnectedToNetwork() == true {
                SVProgressHUD.show(withStatus: "Loading..")
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForStoreList), userInfo: nil, repeats: false)
            } else {
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
            

          //  btnHeaderDoctorStore.setTitle("Store", for: .normal)
            
            btnHeaderStore.backgroundColor = UIColor.init(colorLiteralRed: 99.0/255.0, green: 192.0/255.0, blue: 210.0/255.0, alpha: 1.0)
            btnHeaderStore.setTitleColor(UIColor.white, for: .normal)
            btnHeaderDoctor.backgroundColor = UIColor.white
            btnHeaderDoctor.setTitleColor(UIColor.init(colorLiteralRed: 181.0/255.0, green: 182.0/255.0, blue: 183.0/255.0, alpha: 1.0), for: .normal)


        }
        else{
            isHeaderDoctorStore = true

            collectionViewStore.isHidden = true
            collectionViewDoctor.isHidden = false
            
            if Reachability.isConnectedToNetwork() == true {
                SVProgressHUD.show(withStatus: "Loading..")
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForDoctorList), userInfo: nil, repeats: false)
            } else {
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
          // btnHeaderDoctorStore.setTitle("Doctor", for: .normal)
            
            
            btnHeaderDoctor.backgroundColor = UIColor.init(colorLiteralRed: 99.0/255.0, green: 192.0/255.0, blue: 210.0/255.0, alpha: 1.0)
            btnHeaderDoctor.setTitleColor(UIColor.white, for: .normal)
            btnHeaderStore.backgroundColor = UIColor.white
            btnHeaderStore.setTitleColor(UIColor.init(colorLiteralRed: 181.0/255.0, green: 182.0/255.0, blue: 183.0/255.0, alpha: 1.0), for: .normal)
            
            

        }
    }
    
    @IBAction func btnFavouriteStoredClicked(_ sender:UIButton){
        
        let ifFav = (self.storeData[sender.tag] as AnyObject).value(forKey: "is_favourite") as? String
        storeID = (self.storeData[sender.tag] as AnyObject).value(forKey: "store_id") as? String
        
        if ifFav == "1"{
            let temp = (self.storeData[sender.tag] as! NSDictionary).mutableCopy() as! NSMutableDictionary
            print(temp)
            temp.setValue("0", forKey: "is_favourite")
            
            if (self.storeData.count > sender.tag) {
                self.storeData.replaceObject(at: sender.tag, with: temp)
                self.storeGlobalData.replaceObject(at: sender.tag, with: temp)
                let indexPath = NSIndexPath(row: sender.tag, section: 0)
                collectionViewStore.reloadItems(at: [indexPath as IndexPath])
            }
            
            
            if Reachability.isConnectedToNetwork() == true {
                self.performSelector(inBackground: #selector(self.postDataOnWebserviceForRemoveFavouriteStore), with: nil)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
        }
        else{
            let temp = (self.storeData[sender.tag] as! NSDictionary).mutableCopy() as! NSMutableDictionary
            print(temp)
            temp.setValue("1", forKey: "is_favourite")
            
            if (self.storeData.count > sender.tag) {
                self.storeData.replaceObject(at: sender.tag, with: temp)
                self.storeGlobalData.replaceObject(at: sender.tag, with: temp)
                let indexPath = NSIndexPath(row: sender.tag, section: 0)
                collectionViewStore.reloadItems(at: [indexPath as IndexPath])
            }
            
            if Reachability.isConnectedToNetwork() == true {
                self.performSelector(inBackground: #selector(self.postDataOnWebserviceForAddFavouriteStore), with: nil)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
        }
    }

    @IBAction func btnFavouriteDoctorClicked(_ sender:UIButton){
        
        let ifFav = (self.doctorData[sender.tag] as AnyObject).value(forKey: "is_favourite") as? String
        doctorID = (self.doctorData[sender.tag] as AnyObject).value(forKey: "d_id") as? String
        
        if ifFav == "1"{
            let temp = (self.doctorData[sender.tag] as! NSDictionary).mutableCopy() as! NSMutableDictionary
            print(temp)
            temp.setValue("0", forKey: "is_favourite")
            
            if (self.doctorData.count > sender.tag) {
                self.doctorData.replaceObject(at: sender.tag, with: temp)
                self.doctorGlobalData.replaceObject(at: sender.tag, with: temp)
                let indexPath = NSIndexPath(row: sender.tag, section: 0)
                collectionViewDoctor.reloadItems(at: [indexPath as IndexPath])
            }
            
            
            if Reachability.isConnectedToNetwork() == true {
                self.performSelector(inBackground: #selector(self.postDataOnWebserviceForRemoveFavouriteDoctor), with: nil)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
        }
        else{
            let temp = (self.doctorData[sender.tag] as! NSDictionary).mutableCopy() as! NSMutableDictionary
            print(temp)
            temp.setValue("1", forKey: "is_favourite")
            
            if (self.doctorData.count > sender.tag) {
                self.doctorData.replaceObject(at: sender.tag, with: temp)
                self.doctorGlobalData.replaceObject(at: sender.tag, with: temp)
                let indexPath = NSIndexPath(row: sender.tag, section: 0)
                collectionViewDoctor.reloadItems(at: [indexPath as IndexPath])
            }
            
            if Reachability.isConnectedToNetwork() == true {
                self.performSelector(inBackground: #selector(self.postDataOnWebserviceForAddFavouriteDoctor), with: nil)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
        }
    }
    
    @IBAction func btnMyLocationClicked(_ sender:UIButton){
        // My Location
        var lat:String?
        var long:String?
        
        if USERDEFAULT.value(forKey: "currentLatitude") != nil{
            lat = USERDEFAULT.value(forKey: "currentLatitude") as? String
        }
        else{
            lat = "0.000000"
        }
        
        if USERDEFAULT.value(forKey: "currentLongtitude") != nil{
            long = USERDEFAULT.value(forKey: "currentLongtitude") as? String
        }
        else{
            long = "0.000000"
        }
        
        //btnCurrentLocation.isHidden = true
        
        self.recenterMapToPlacemark(strLatitude: lat!, strLongtitude: long!,isChange: true)
    }
    
    @IBAction func btnLogoutClicked(_ sender:UIButton){
        APPDELEGATE.logOutUser()
        let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    @IBAction func btnArticalClicked(_ sender:UIButton){
        let homeVC = TabBarVC(nibName: "TabBarVC", bundle: nil)
        homeVC.selectedIndexOfMyTabbarController = 1
        self.navigationController?.pushViewController(homeVC, animated: true)

    }
    @IBAction func btnDrugClicked(_ sender:UIButton){
        let drugVC = DrugListVC(nibName: "DrugListVC", bundle: nil)
        self.navigationController?.pushViewController(drugVC, animated: true)
    }
    
    @IBAction func btnFavouriteStoreClicked(_ sender:UIButton){
        
        let favouriteVC = FavouriteStoreVC(nibName: "FavouriteStoreVC", bundle: nil)
        self.navigationController?.pushViewController(favouriteVC, animated: true)
        
    }
    @IBAction func btnNotificationClicked(_ sender:UIButton){
        let notificationVC = NotificationVC(nibName: "NotificationVC", bundle: nil)
        self.navigationController?.pushViewController(notificationVC, animated: true)

    }
    @IBAction func btnSearchClicked(_ sender:UIButton){
        
        print("From : ",isHeaderDoctorStore ?? "123")
        let searchVC = SearchStoreVC(nibName: "SearchStoreVC", bundle: nil)
        if isHeaderDoctorStore == true{
            searchVC.isFromStore = false
        }
        else{
            searchVC.isFromStore = true
        }
        self.navigationController?.pushViewController(searchVC, animated: true)
    }

    @IBAction func btnLoginClicked(_ sender:UIButton){
        
        if USERDEFAULT.value(forKey: "userID") != nil{
            
            if let isMember = USERDEFAULT.value(forKey: "isMember") as? String{
                let ismem = "\(isMember)"
                if ismem == "1"{
                    
                }
                else{
                    let joinMemberVC = JoinMemberVC(nibName: "JoinMemberVC", bundle: nil)
                    joinMemberVC.isFromOther = true
                    joinMemberVC.isFromTab = "3"
                    self.navigationController?.pushViewController(joinMemberVC, animated: true)
                }
            }
            else{
                
            }
        }
        else{
            let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
            loginVC.isFromTab = "3"
            self.navigationController?.pushViewController(loginVC, animated: true)
        }

        
        
        
    }
    @IBAction func btnCancelClicked(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSalecounterSearchClicked(_ sender:UIButton){
        
        
        if healthCategory.count > 0{
            if healthCategory.count > 2{
                
                viewSaleCounter.removeFromSuperview()
                
                if Reachability.isConnectedToNetwork() == true {
                    SVProgressHUD.show(withStatus: "Loading..")
                    Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForUpdateProfile), userInfo: nil, repeats: false)
                } else {
                    showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                }
                
                
                USERDEFAULT.set("NO", forKey: "isSaleCounterSearch")
                USERDEFAULT.synchronize()
                
            }
            else{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Select maximum 3 categories of health concerns", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
            }
        }
        else{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please select health concerns", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }

        
        
        
        
        
    }
    
    @IBAction func btnDrugCategorySelectionClicked(_ sender:UIButton){
        
        //        //Single Selection
        //
        //        // boolArrayForCategory.replaceObject(at: sender.tag, with: "1")
        //        let indexPath = NSIndexPath(row: sender.tag, section: 0)
        //
        //        let myIndexValue:Int = boolArrayForCategory.index(of: "1")
        //
        //
        //        boolArrayForCategory = zeroTempDataArray.mutableCopy() as! NSMutableArray
        //
        //        let oneDataArray = zeroTempDataArray.mutableCopy() as! NSMutableArray
        //
        //        oneDataArray.replaceObject(at: indexPath.row, with: "1")
        //        boolArrayForCategory.replaceObject(at: indexPath.row, with: oneDataArray.object(at: indexPath.row))
        //
        //        UIView.animate(withDuration: 0, animations: {
        //            self.collectionViewFilterDrug.performBatchUpdates({
        //                let indexPath1 = IndexPath(row: indexPath.row, section: 0)
        //                print("Indexpath1 \(indexPath1.row)")
        //                self.collectionViewFilterDrug.reloadItems(at: [indexPath1])
        //            }, completion: nil)
        //        })
        //
        //        UIView.animate(withDuration: 0, animations: {
        //            self.collectionViewFilterDrug.performBatchUpdates({
        //                if (myIndexValue<self.boolArrayForCategory.count) {
        //                    let indexPath2 = IndexPath(row: myIndexValue, section: 0)
        //                    print("Indexpath2 \(indexPath2.row)")
        //                    self.collectionViewFilterDrug.reloadItems(at: [indexPath2])
        //                }
        //            }, completion: nil)
        //        })
        //
        //
        //        diseaseCategoryData = NSMutableArray.init()
        //
        //        for i in 0...drugCategoryData.count - 1 {
        //            if boolArrayForCategory.object(at: i) as! String == "1"{
        //                diseaseCategoryData.add((self.drugCategoryData[i] as AnyObject).value(forKey: "dc_id") as! String)
        //            }
        //        }
        //
        //        print("diseaseCategoryData ID",diseaseCategoryData)
        //
        //        finalFilterData.setValue(diseaseCategoryData, forKey: "dcIDData")
        //
        //
        //
        
        if boolArrayForCategory.object(at: sender.tag) as! String == "0"{
            boolArrayForCategory.replaceObject(at: sender.tag, with: "1")
            saleSelectionCount = saleSelectionCount + 1
        }
        else{
            boolArrayForCategory.replaceObject(at: sender.tag, with: "0")
            saleSelectionCount = saleSelectionCount - 1
        }
        
        let indexPath = NSIndexPath(row: sender.tag, section: 0)
        collectionViewSaleCounter.reloadItems(at: [indexPath as IndexPath])
        
        if saleSelectionCount > 0 {
            lblSaleCounter.isHidden = false
            lblSaleCounter.text = "\(saleSelectionCount!) Selected"
        }
        else{
            lblSaleCounter.isHidden = true
            lblSaleCounter.text = ""
        }

        
        
        
        healthCategory = NSMutableArray.init()
        
        for i in 0...healthData.count - 1 {
            if boolArrayForCategory.object(at: i) as! String == "1"{
                healthCategory.add((self.healthData[i] as AnyObject).value(forKey: "hc_id") as! String)
            }
        }
        
        print("Health Concern ID",healthCategory)
        
    }
    
    

    
    
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForStoreList(){
        let completeURL = NSString(format:"%@General/SearchStoreByCountry", MainURL) as String
        
        /*
         //getStoreListURL
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "latitude":APPDELEGATE.GLOBAL_latitude,
            "longtitude":APPDELEGATE.GLOBAL_longtitude,
            "miles":MILE
        ]
        */
        
        SVProgressHUD.show(withStatus: "Loading...")
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "country":selectedCountry,
            "user_type" : selectedType,
            "page":pagenum,
            "limit":PAGINATION_LIMITE
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("StoreList API Parameter :",finalParams)
        print("StoreList API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getStoreListURLTag)
    }
    
    func postDataOnWebserviceForDoctorList(){
        let completeURL = NSString(format:"%@%@", MainURL,getDoctorListURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "latitude":APPDELEGATE.GLOBAL_latitude,
            "longtitude":APPDELEGATE.GLOBAL_longtitude,
            "miles":MILE
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("DoctorList API Parameter :",finalParams)
        print("DoctorList API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getDoctorListURLTag)
    }

    func postDataOnWebserviceForAddFavouriteStore(){
        let completeURL = NSString(format:"%@%@", MainURL,addFavouriteStoreURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "store_id":storeID
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("AddFavouriteStore API Parameter :",finalParams)
        print("AddFavouriteStore API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: addFavouriteStoreURLTag)
    }
    
    func postDataOnWebserviceForRemoveFavouriteStore(){
        let completeURL = NSString(format:"%@%@", MainURL,removeFavouriteStoreURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "store_id":storeID
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("RemoveFavouriteStore API Parameter :",finalParams)
        print("RemoveFavouriteStore API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: removeFavouriteStoreURLTag)
    }

    func postDataOnWebserviceForAddFavouriteDoctor(){
        let completeURL = NSString(format:"%@%@", MainURL,addFavouriteDoctorURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "d_id":doctorID
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("AddFavouriteStore API Parameter :",finalParams)
        print("AddFavouriteStore API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: addFavouriteDoctorURLTag)
    }
    
    func postDataOnWebserviceForRemoveFavouriteDoctor(){
        let completeURL = NSString(format:"%@%@", MainURL,removeFavouriteDoctorURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "d_id":doctorID
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("RemoveFavouriteStore API Parameter :",finalParams)
        print("RemoveFavouriteStore API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: removeFavouriteDoctorURLTag)
    }
    
    
    func postDataOnWebserviceForHealthConcern(){
        
        let completeURL = NSString(format:"%@%@", MainURL,healthConcernURL) as String
        
     //   let pageNumber = "\(pageNum1!)"
        // "page":pageNumber,
        // "limit":PAGINATION_LIMITE
        
        let params:NSDictionary = [
            // "user_id" : "49",
            // "token": "ICp48GEVMOBwzHOUu1KJN2xJ2PxsmCSa1jDfTGpERXP6s7pbFH2ZpPvg4PpuJSWX5uCYy701DfKNHHeacneTpCMxU3C0JEvlT5CFjVqbv63FXjlOUZoQ6jsW",
          //  "page":pageNumber,
          //  "limit":PAGINATION_LIMITE,
            "lang_type":Language_Type
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetArticalCategory API Parameter :",finalParams)
        print("GetArticalCategory API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: healthConcernURLTag)
        
        
    }

    func postDataOnWebserviceForGetProfile(){
        let completeURL = NSString(format:"%@%@", MainURL,getUserProfileURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetProfile API Parameter :",finalParams)
        print("GetProfile API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getUserProfileURLTag)
    }
    

    
    
    func postDataOnWebserviceForUpdateProfile(){
        let completeURL = NSString(format:"%@%@", MainURL,editUserProfileURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "health_concern":healthCategory,
            ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("UpdateProfile API Parameter :",finalParams)
        print("UpdateProfile API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: editUserProfileURLTag)
    }
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case getStoreListURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("StoreList Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                let temp = resultDict.value(forKey: "data") as! NSArray
                
                if (temp.count > 0)
                {
                    storeData.addObjects(from: temp as! [Any])
                    
                    if temp.count < PAGINATION_LIMITE {
                        if (self.pagenum > 1)
                        {
                            self.pagenum -= 1
                        }
                        self.isLoading = false
                    }
                    else{
                        self.isLoading = true
                    }
                }
                else{
                    self.isLoading = false
                    if (self.pagenum > 1)
                    {
                        self.pagenum -= 1
                    }
                }
                
                
                storeGlobalData = storeData
                mapData = storeData
                print("Count -> \(mapData.count)")
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            self.settingClusteringOnMap()
            collectionViewStore.reloadData()
            if pagenum == 1{
                if mapData.count > 0{
                    UIView.animate(withDuration: 0.2, animations: {
                        
                    }) { (time) in
                        self.recenterMapToPlacemark(strLatitude: (self.mapData[0] as! NSDictionary).value(forKey: "latitude") as! String, strLongtitude: (self.mapData[0] as! NSDictionary).value(forKey: "longtitude") as! String,isChange: false)
                    }
                }
            }
            SVProgressHUD.dismiss()
            break
            
        case getDoctorListURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("DoctorList Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                doctorData = NSMutableArray(array: resultDict.value(forKey: "data") as! NSArray)
                doctorGlobalData = NSMutableArray(array: resultDict.value(forKey: "data") as! NSArray)
                
                mapData = NSArray() as! NSMutableArray
                mapData = NSArray(array: resultDict.value(forKey: "data") as! NSArray) as! NSMutableArray
                
                self.settingClusteringOnMap()
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
            }
            
            SVProgressHUD.dismiss()
            collectionViewDoctor.reloadData()
            break

        case addFavouriteStoreURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("AddFavouriteStore Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            break
            
        case removeFavouriteStoreURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("RemoveFavouriteStore Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        case addFavouriteDoctorURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("AddFavouriteDoctor Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        case removeFavouriteDoctorURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("RemoveFavouriteDoctor Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            break
            
        case healthConcernURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("healthConcern List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                //  if self.pageNum == 1{
                self.healthData = NSMutableArray()
                self.healthGlobalData = NSMutableArray()
                //  }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        self.healthGlobalData.add(myData[i])
                    }
                    
                    for i in 0...myData.count - 1 {
                        self.healthData.add(myData[i])
                    }
                    
                    boolArrayForCategory = NSMutableArray.init()
                    saleSelectionCount = 0
                    for _ in 0...healthData.count - 1 {
                        boolArrayForCategory.add("0")
                    }
                    
                    
                    
                    //                    if (myData.count < PAGINATION_LIMITE) {
                    //                        if (self.pageNum > 0) {
                    //                            self.pageNum = self.pageNum - 1
                    //                        }
                    //                        self.isLoading = false
                    //                    }else{
                    //                        self.isLoading = true
                    //                    }
                }
                else{
                    //                    self.isLoading = false
                    //                    if (self.pageNum > 0) {
                    //                        self.pageNum = self.pageNum - 1
                    //                    }
                    
                    if self.healthData.count == 0{
                        self.collectionViewSaleCounter.isHidden = true
                        //self.lblNoArtical.isHidden = false
                    }
                    else{
                        self.collectionViewSaleCounter.isHidden = false
                        //self.lblNoArtical.isHidden = true
                    }
                }
                self.collectionViewSaleCounter.reloadData()
                
              
                self.setHealthConcernData()

                
                //                self.removeLoadingIndicatiorOnFooterOnTableView()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            SVProgressHUD.dismiss()
            break
            
        case getUserProfileURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("GETProfile Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                healthConcernData = (resultDict.value(forKey: "data") as! NSDictionary).value(forKey: "userhealth_concern") as! NSArray
                self.setHealthConcernData()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
            }
            //   SVProgressHUD.dismiss()
            break

            
        case editUserProfileURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("UpdateProfile Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                if let fullname = (resultDict.value(forKey: "data") as! NSDictionary).value(forKey: "fullname") as? String{
                    let name = "\(fullname)"
                    USERDEFAULT.set(name, forKey: "fullName")
                    USERDEFAULT.synchronize()
                    let notificationName = Notification.Name("updateProfileDataNotification")
                    NotificationCenter.default.post(name: notificationName, object: nil)
                }
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
            }
            
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case getStoreListURLTag:
            SVProgressHUD.dismiss()
            break
        case getDoctorListURLTag:
            SVProgressHUD.dismiss()
            break
        case addFavouriteStoreURLTag:
            break
        case removeFavouriteStoreURLTag:
            break
        case addFavouriteDoctorURLTag:
            break
        case removeFavouriteDoctorURLTag:
            break
        case healthConcernURLTag:
            //            self.isLoading = false
            //            if (self.pageNum > 0) {
            //                self.pageNum = self.pageNum - 1
            //            }
            //            self.removeLoadingIndicatiorOnFooterOnTableView()
            SVProgressHUD.dismiss()
            
            
            break
        case editUserProfileURLTag:
            SVProgressHUD.dismiss()
            break
            
        case getUserProfileURLTag:
            //  SVProgressHUD.dismiss()
            break

            

        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    
    
    // TODO: -  CLUSTERING METHODS
    func settingClusteringOnMap() {
        
        myMapView.showsUserLocation = true
        
        
        self.qtree = QTree()
        
        
        DispatchQueue.global(qos: .default).async {
        
            srand48(0)
            
            if self.mapData.count > 0{
                
                for i in 0...self.mapData.count-1{
                    let object = DummyAnnotation.init()
                    
                    if (self.mapData.object(at: i) as AnyObject).value(forKey: "latitude") as! String != "" &&  (self.mapData.object(at: i) as AnyObject).value(forKey: "longtitude") as! String != ""{
                        
                        object.coordinate = CLLocationCoordinate2DMake(Double(((self.mapData.object(at: i) as AnyObject).value(forKey: "latitude") as! String))! , Double(((self.mapData.object(at: i) as AnyObject).value(forKey: "longtitude") as! String))!)
                        
                        //print("mapData :",self.mapData.object(at: i) as AnyObject)
                        
                        if self.isHeaderDoctorStore == true{
                            if let name = (self.mapData.object(at: i) as AnyObject).value(forKey: "firstname") as? String
                            {
                                object.title = "\(name)"
                            }
                        }
                        else{
                            if let name = (self.mapData.object(at: i) as AnyObject).value(forKey: "first_name") as? String
                            {
                                object.title = "\(name)"
                            }
                            
                        }
                        
                        object.nTag = UInt(i)
                        //print("Dummy Annotation",object)
                        
//                        if let finalObject = object {
//                            self.qtree.insertObject(finalObject)
//                        }
                        
                        self.qtree.insertObject(object)
                        
                        DispatchQueue.main.async {
                            self.reloadAnnotations()
                        }
                        
                    }
                }
                
            }
            else{
                //remove all previous annotations from map
                
                if self.myMapView.annotations.count > 0{
                    DispatchQueue.main.async {
                        //let annotationsToRemove = self.myMapView.annotations.filter { $0 !== self.myMapView.userLocation }
                        let annotationsToRemove = self.myMapView.annotations
                        self.myMapView.removeAnnotations( annotationsToRemove )
                    }
                }
            }
        }
        
        
//        if mapData.count == 0{
//            self.reloadAnnotations()
//        }
        
        
    }
    
    func removeMapAnnotations() {
        
        
        let userLocation = myMapView.userLocation
        
        let pins : NSMutableArray = (myMapView.annotations as NSArray).mutableCopy() as! NSMutableArray
        if (userLocation == nil){
            pins.remove(userLocation)
        }
        
        myMapView.removeAnnotations(pins as! [MKAnnotation])
        
    }
    
    func reloadAnnotations(){
        
        if self.mapData.count == 0 {
            return
        }
        
        
        
        let mapRegion:MKCoordinateRegion = myMapView.region
        
        let useClustering:Int = -1
        
        //   let minNonClusteredSpan:CLLocationDegrees = useClustering ? min(Int(mapRegion.span.latitudeDelta), Int(mapRegion.span.longitudeDelta)) / 15 : 0
        
        var minNonClusteredSpan:CLLocationDegrees
        
        if useClustering == -1{
            minNonClusteredSpan = (min(mapRegion.span.latitudeDelta, mapRegion.span.longitudeDelta) / 15) as CLLocationDegrees
        }
        else{
            minNonClusteredSpan = 0
        }
        
        
        
        let objects:NSArray = self.qtree.getObjectsIn(mapRegion, minNonClusteredSpan: CLLocationDegrees(minNonClusteredSpan)) as NSArray
        
        
        
        
        let annotationsToRemove:NSMutableArray = (myMapView.annotations as NSArray).mutableCopy() as! NSMutableArray
        
        annotationsToRemove.remove(myMapView.userLocation)
        annotationsToRemove.remove(objects)
        myMapView.removeAnnotations(NSArray(array: annotationsToRemove) as! [MKAnnotation])
        
        
        let annotationsToAdd:NSMutableArray = objects.mutableCopy() as! NSMutableArray
        annotationsToAdd.removeObjects(in: myMapView.annotations)
        myMapView.addAnnotations(NSArray(array: annotationsToAdd) as! [MKAnnotation])
        
        
        var lat:String?
        var long:String?
        
        if USERDEFAULT.value(forKey: "currentLatitude") != nil{
            lat = USERDEFAULT.value(forKey: "currentLatitude") as? String
        }
        else{
            lat = "0.000000"
        }
        
        if USERDEFAULT.value(forKey: "currentLongtitude") != nil{
            long = USERDEFAULT.value(forKey: "currentLongtitude") as? String
        }
        else{
            long = "0.000000"
        }
        
        
        let myCurrentLocationCordinate:CLLocationCoordinate2D = CLLocationCoordinate2DMake(Double(lat!)!, Double(long!)!)
        let annotation = MKPointAnnotation()
        annotation.coordinate = myCurrentLocationCordinate
        //annotation.title = "iOSDevCenter-Kirit Modi"
        // annotation.subtitle = "Ahmedabad"
        myMapView.addAnnotation(annotation)
        
        
        
        
    }
    
    
    // TODO: -  MKMAPVIEW DELEGATES METHODS
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        if didSelect{
            self.reloadAnnotations()
        }
        didSelect = true
        
        //        if btnCurrentLocation.isHidden == true{
//            btnCurrentLocation.isHidden = false
//        }
//        else{
//            btnCurrentLocation.isHidden = true
//        }
        
    }
    
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        
        
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
//        for annotation in (mapView.annotations as Array){
//            
//            if annotation.isKind(of: MKUserLocation.self)
//            {
//                mapView.removeAnnotation(annotation )
//            }
//            else if annotation.isKind(of: QCluster.self){
//            }
//            else{
////                let anView:MKAnnotationView = view
//                let anView = mapView.view(for: annotation)!
//                print(anView.tag)
//                print(view.tag)
//                
//                
//                if (anView.tag == view.tag) {
//                    anView.image = UIImage.init(named: "pharmasist1.png")
//                }
//                else{
//                    anView.image = UIImage.init(named: "pharmasist.png")
//                }
//                
//                if mapData.count > view.tag{
//                    
//                    if isHeaderDoctorStore == true{
//                        let nextItem:NSIndexPath = NSIndexPath(item: anView.tag, section: 0)
//                        
//                        collectionViewDoctor.scrollToItem(at: nextItem as IndexPath, at: .left, animated: true)
//                    }
//                    else{
//                        let nextItem:NSIndexPath = NSIndexPath(item: anView.tag, section: 0)
//                        collectionViewStore.scrollToItem(at: nextItem as IndexPath, at: .left, animated: true)
//                        
//                        self.recenterMapToPlacemark(strLatitude: (self.mapData.object(at: view.tag) as AnyObject).value(forKey: "latitude") as! String, strLongtitude: (self.mapData.object(at: view.tag) as AnyObject).value(forKey: "longtitude") as! String)
//                        
//                    }
//                }
//            }
//            
//            
//            
//        }
        
        
        let annotation = view.annotation!
        
        if annotation.isKind(of: MKUserLocation.self)
        {
            mapView.removeAnnotation(annotation )
        }
        else if annotation.isKind(of: QCluster.self){
        }
        else{
            let anView:MKAnnotationView = view //mapView.view(for: annotation)!
            print(anView.tag)
            print(view.tag)

            currentPinTag = view.tag
            
            if selectedType == "store"
            {
                //store
                if (anView.tag == view.tag) {
                    anView.image = UIImage.init(named: "store_blue.png")
                }
                else{
                    anView.image = UIImage.init(named: "store_green.png")
                }
            }
            else{
                //clinic
                if (anView.tag == view.tag) {
                    anView.image = UIImage.init(named: "pharmasist1.png")
                }
                else{
                    anView.image = UIImage.init(named: "pharmasist.png")
                }
            }
            
            
            
            
            
            if mapData.count > anView.tag{
                
                if isHeaderDoctorStore == true{
                    let nextItem:NSIndexPath = NSIndexPath(item: anView.tag, section: 0)
                    collectionViewDoctor.scrollToItem(at: nextItem as IndexPath, at: .left, animated: true)
                }
                else{
                    let nextItem:NSIndexPath = NSIndexPath(item: anView.tag, section: 0)
                    collectionViewStore.scrollToItem(at: nextItem as IndexPath, at: .left, animated: true)
                    
                    self.reloadAnnotations()
                  //  self.recenterMapToPlacemark(strLatitude: (self.mapData.object(at: anView.tag) as AnyObject).value(forKey: "latitude") as! String, strLongtitude: (self.mapData.object(at: anView.tag) as AnyObject).value(forKey: "longtitude") as! String)

                }
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation.isKind(of: QCluster.self){
            
            var annotationView: ClusterAnnotationView?
            
            annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: ClusterAnnotationView.reuseId()) as? ClusterAnnotationView
            
            
            if(annotationView == nil){
                annotationView = ClusterAnnotationView.init(cluster: annotation as! QCluster)
            }
            
            annotationView?.cluster = annotation as! QCluster
            
            let myDetailButton:UIButton = UIButton(type: .detailDisclosure)
            myDetailButton.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
            myDetailButton.contentVerticalAlignment = .center
            myDetailButton.tag = (annotationView?.tag)!
            myDetailButton.contentHorizontalAlignment = .center
            annotationView?.rightCalloutAccessoryView = myDetailButton
            annotationView?.annotation = annotation
            
            return annotationView
        }
        else{
            
            var pinView:MKAnnotationView?
            
            if annotation.isEqual(mapView.userLocation) {
                return nil
            }
            else{
                
                let defaultPinID = "com.invasivecode.pin"
                if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: defaultPinID) {
                    pinView = dequeuedAnnotationView
                }
                else{
                    pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: defaultPinID)
                }
            }
            
            pinView?.canShowCallout = false
            pinView?.isEnabled = true
            
            if let refPlacePin:DummyAnnotation = annotation as? DummyAnnotation
            {
                pinView?.tag = Int(refPlacePin.nTag)
                
                if pinView?.tag == mapData.count{
                    pinView?.canShowCallout = true
                }
 
                
                if selectedType == "store"
                {
                    if pinView?.tag == currentPinTag{
                        pinView?.image = UIImage.init(named: "store_blue.png")
                    }
                    else{
                        pinView?.image = UIImage.init(named: "store_green.png")
                    }
                }
                else{
                    if pinView?.tag == currentPinTag{
                        pinView?.image = UIImage.init(named: "pharmasist1.png")
                    }
                    else{
                        pinView?.image = UIImage.init(named: "pharmasist.png")
                    }
                }
                
                /*
                if isHeaderDoctorStore == true{
                    pinView?.image = UIImage.init(named: "doctorpin.png")
                }
                else{
                    pinView?.image = UIImage.init(named: "pharmasist.png")
                }
                */
                
                let myDetailButton:UIButton = UIButton(type: .detailDisclosure)
                myDetailButton.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
                myDetailButton.contentVerticalAlignment = .center
                myDetailButton.tag = Int(refPlacePin.nTag)
                myDetailButton.contentHorizontalAlignment = .center
                pinView?.rightCalloutAccessoryView = myDetailButton
                pinView?.annotation = annotation
            }
            else{
                pinView?.canShowCallout = false
                pinView?.image = UIImage.init(named: "user_location.png")
            }
            
            return pinView;
        }
        
    }
    
    
    
    // TODO: - OTHERS METHODS
    func setHealthConcernData() {
        let temp = NSMutableArray()
        
        if healthData.count > 0{
            for i in 0..<self.healthData.count{
                let id = (self.healthData[i] as AnyObject).value(forKey: "hc_id") as! String
                temp.add("\(id)")
            }
            print(temp)
        }
        
        if healthConcernData.count > 0{
            if let abc = healthConcernData as? NSArray
            {
                for i in 0..<abc.count{
                    let myIndexValue:Int = temp.index(of: "\(abc[i])")
                    if self.healthData.count > myIndexValue{
                        self.boolArrayForCategory.replaceObject(at: myIndexValue, with: "1")
                        let path = NSIndexPath(row: myIndexValue, section: 0)
                        collectionViewSaleCounter.reloadItems(at: [path as IndexPath])
                    }
                }
            }
            USERDEFAULT.setValue(healthConcernData, forKey: "userHealthConcernData")
            USERDEFAULT.synchronize()

        }
        
        
        if healthData.count > 0{
            healthCategory = NSMutableArray.init()
            for i in 0...healthData.count - 1 {
                if boolArrayForCategory.object(at: i) as! String == "1"{
                    healthCategory.add((self.healthData[i] as AnyObject).value(forKey: "hc_id") as! String)
                }
            }
            
            saleSelectionCount = healthCategory.count
            if saleSelectionCount > 0 {
                lblSaleCounter.isHidden = false
                lblSaleCounter.text = "\(saleSelectionCount!) Selected"
            }
            else{
                lblSaleCounter.isHidden = true
                lblSaleCounter.text = ""
            }

            print("Selected Health Concern Data : ",healthCategory)

            USERDEFAULT.setValue(healthCategory, forKey: "userHealthCategoryData")
            USERDEFAULT.synchronize()

        }
        

        
    }
    
    
}


extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true, radius: CGFloat) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 0, height: 3)
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
