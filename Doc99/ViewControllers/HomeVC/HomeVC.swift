//
//  HomeVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 18/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    @IBOutlet var collectionViewDoctor:UICollectionView!

    @IBOutlet var btnArticle:UIButton!
    @IBOutlet var btnDrug:UIButton!

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = false
        self.generalViewControllerSetting()
     //   UIApplication.shared.statusBarStyle = .lightContent

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // FIXME: - VIEW CONTROLLER METHODS
    
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        self.setLocalizationText()
    }
    
    func setLocalizationText(){
        btnArticle.setTitle(NSLocalizedString("Article", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        btnDrug.setTitle(NSLocalizedString("Drug", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)

    }

    // TODO: - DELEGATE METHODS
    
    // TODO: - ACTION METHODS
    
    @IBAction func btnArticalClicked(_ sender:UIButton){
        let homeVC = TabBarVC(nibName: "TabBarVC", bundle: nil)
        homeVC.selectedIndexOfMyTabbarController = 1
        self.navigationController?.pushViewController(homeVC, animated: true)

    }
    @IBAction func btnDrugClicked(_ sender:UIButton){
        let drugVC = DrugListVC(nibName: "DrugListVC", bundle: nil)
        self.navigationController?.pushViewController(drugVC, animated: true)
    }

    
    // TODO: - POST DATA METHODS
    


}
