//
//  UserTypeSelectionVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 23/03/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class UserTypeSelectionVC: UIViewController,SyncManagerDelegate {

    //MARK:- Outlets
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnDoctor: UIButton!
    @IBOutlet weak var btnPhrmasist: UIButton!
    @IBOutlet weak var btnSelfUse: UIButton!
    
    var user_type : String = ""
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBActions
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDoctorClicked(_ sender: UIButton) {
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show(withStatus: "Loading..")
            user_type = "1"
            self.postDataUpdateProfile()
        }
        else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
    }
    
    @IBAction func btnPhrmasistClicked(_ sender: UIButton) {
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show(withStatus: "Loading..")
            user_type = "2"
            self.postDataUpdateProfile()
        }
        else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
    }
    
    @IBAction func btnSelfUseClicked(_ sender: UIButton) {
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show(withStatus: "Loading..")
            user_type = "3"
            self.postDataUpdateProfile()
        }
        else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
    }
    
    
    //MARK:- API Call Methods
    func postDataUpdateProfile() {
        let completeURL = NSString(format:"%@%@", MainURL,editUserProfileURL) as String
       
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID")as! String,
            "token" : USERDEFAULT.value(forKey: "token")as! String,
            "lang_type" : "1",
            "fullname" : USERDEFAULT.value(forKey: "fullName")as! String,
            "phone" : USERDEFAULT.value(forKey: "phone")as! String,
            "user_type" : user_type,
            "dob" : USERDEFAULT.value(forKey: "dob")as! String,
            "gender" : USERDEFAULT.value(forKey: "gender")as! String,
            "ccode" : USERDEFAULT.value(forKey: "ccode")as! String
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("UpdateProfile URL :",completeURL)
        print("UpdateProfile Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: loginURLTag)
    }
    
    //MARK:- API call back Mathods
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        
        let resultDict = responseObject as! NSDictionary
        print("update profile response ->")
        
        if resultDict.value(forKey: "status")as! String == "1" {
            //isTypeSelected
            let dataDictionary = resultDict.value(forKey: "data") as! NSDictionary
            USERDEFAULT.set("ok", forKey: "isTypeSelected")
            USERDEFAULT.setValue(dataDictionary.value(forKey: "user_type") as? String, forKey: "user_type")
            USERDEFAULT.synchronize()
            
            for (key, value) in USERDEFAULT.dictionaryRepresentation() {
                print("-- \(key) = \(value) \n")
            }
            
            let  homeVC = TabBarVC(nibName: "TabBarVC", bundle: nil)
            homeVC.selectedIndexOfMyTabbarController = 0
            self.navigationController?.pushViewController(homeVC, animated: true)
            
        }
        SVProgressHUD .dismiss()
    }
    
    func syncFailure(_ error: Error!, withTag tag: Int) {
        SVProgressHUD.dismiss()
        print("update profile failure -> \(error)")
    }
}
