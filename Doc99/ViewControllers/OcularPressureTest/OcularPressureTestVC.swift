//
//  OcularPressureTestVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 13/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class OcularPressureTestVC: UIViewController,UITextFieldDelegate {

    @IBOutlet var txtPressureLevel:UITextField!
    @IBOutlet var lblPressureLevel:UILabel!
    @IBOutlet var lblPressureTitle:UILabel!
    @IBOutlet var scrMain:UIScrollView!

    
    @IBOutlet var lblHeader:UILabel!
    @IBOutlet var lblHome:UILabel!
    @IBOutlet var lblTitlePressureLevel:UILabel!
    @IBOutlet var lblIntraOcular:UILabel!
    

    // FIXME: - VIEW CONTROLLER METHODS
    var pressureLevel:Double!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        self.addTapGestureInOurView()
        self.NavBarNumberPadForTextField()
        txtPressureLevel.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

        self.setLocalizationText()
    }
    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    
    func setLocalizationText(){
        lblPressureTitle.text = NSLocalizedString("Low Risk", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblHeader.text = NSLocalizedString("ARIA Stroke Risk Test", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitlePressureLevel.text = NSLocalizedString("Pressure Level", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblIntraOcular.text = NSLocalizedString("Intra Ocular Pressure", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblHome.text = NSLocalizedString("HOME", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
    }
    
    
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        
        if viewTouched!.isKind(of: UIButton.self) && viewTouched!.isKind(of:UITextView.self){
            
        }
        else{
            self.view.endEditing(true)
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)

        }
    }

    func NavBarNumberPadForTextField(){
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        numberToolbar.barStyle = .default
        
        let viewBack = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        viewBack.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        numberToolbar.addSubview(viewBack)
        
        let btnCancel = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelNumberPadMile))
        btnCancel.tintColor = UIColor.white
        
        let btnDone = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.doneWithNumberPadMile))
        btnDone.tintColor = UIColor.white
        
        numberToolbar.items = NSArray(objects: btnCancel,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),btnDone) as? [UIBarButtonItem]
        
        
        numberToolbar.sizeToFit()
        txtPressureLevel.inputAccessoryView = numberToolbar
        
    }
    func cancelNumberPadMile(){
        txtPressureLevel.resignFirstResponder()
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)

    }
    
    func doneWithNumberPadMile(){
        txtPressureLevel.resignFirstResponder()
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)

    }
    
    // TODO: - DELEGATE METHODS
    func textFieldDidChange(_ textField: UITextField) {
        if textField.text == ""{
            pressureLevel = 0
            lblPressureLevel.text = "0"
            lblPressureTitle.text = ""
            
        }
        else{
            pressureLevel = Double(textField.text!)
            if pressureLevel < 15
            {
                lblPressureLevel.text = textField.text
                lblPressureTitle.text = "Low Risk"
                lblPressureTitle.text = NSLocalizedString("Low Risk", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

            }
            else if pressureLevel > 21
            {
                lblPressureLevel.text = textField.text
                lblPressureTitle.text = "High Risk"
                lblPressureTitle.text = NSLocalizedString("High Risk", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

            }
            else{
                lblPressureLevel.text = textField.text
                lblPressureTitle.text = "Medium Risk"
                lblPressureTitle.text = NSLocalizedString("Medium Risk", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

            }
            print("Left Eye :",pressureLevel)
        }
        
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        }
        return false
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var YOffset:Int = 0
        
        if (textField.tag==5) {
            YOffset=0
        }
        
        
        if (DeviceType.IS_IPHONE_4_OR_LESS) {
            
            if (textField.tag==5) {
                YOffset=15
            }
        }
        scrMain.setContentOffset(CGPoint(x: 0, y: CGFloat(textField.tag * YOffset)), animated: true)
        return true
        
    }
    
    // TODO: - ACTION METHODS
    @IBAction func btnHomeClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)

        
        let homeVC = TabBarVC(nibName: "TabBarVC", bundle: nil)
        homeVC.selectedIndexOfMyTabbarController = 0
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }

    
    // TODO: - POST DATA METHODS
}
