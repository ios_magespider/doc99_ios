//
//  OcularPressureTermVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 13/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD
class OcularPressureTermVC: UIViewController,UIWebViewDelegate {
    
    // FIXME: - VARIABLE
    
    @IBOutlet var webViewOcular:UIWebView!
    
    
    @IBOutlet var lblOcular:UILabel!
    @IBOutlet var lblTermCondition:UILabel!
    @IBOutlet var lblStart:UILabel!
    
    
    
    // FIXME: - VIEW CONTROLLER METHODS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if USERDEFAULT.value(forKey: "userID") != nil{
            self.generalViewControllerSetting()
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        //webViewPrescriptionTC.loadRequest(URLRequest(url: URL(string: "http://hardwirestudios.com")!))
        
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetPrescriptionTermCondition), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
         self.setLocalizationText()
    }
    
    func setLocalizationText(){
        
        lblOcular.text = NSLocalizedString("Intra Ocular Pressure Test", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTermCondition.text = NSLocalizedString("Terms & Conditions", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblStart.text = NSLocalizedString("START", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
    }
    
    
    // TODO: - DELEGATE METHODS
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("WebView Error : ",error.localizedDescription)
    }
    
    
    
    // TODO: - ACTION METHODS
    @IBAction func btnStartClicked(_ sender: UIButton) {
        
        let ocularPressureTestVC = OcularPressureTestVC(nibName: "OcularPressureTestVC", bundle: nil)
        self.navigationController?.pushViewController(ocularPressureTestVC, animated: true)
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForGetPrescriptionTermCondition(){
        
        let completeURL = NSString(format:"%@%@", MainURL,cmsWebPageCallURL) as String
        let params:NSDictionary = [
            "page" : "OcularPressureTerm"
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("TermCondition API Parameter :",finalParams)
        print("TermCondition API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: cmsWebPageCallURLTag)
    }
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case cmsWebPageCallURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("TermCondition Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                
                let myDescription = resultDict.value(forKey: "page_description") as! String
                let abc = myDescription.replacingOccurrences(of: "\n", with: "<br/>")
                webViewOcular.loadHTMLString(abc, baseURL: nil)
                
                
            }
            break
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case cmsWebPageCallURLTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
}
