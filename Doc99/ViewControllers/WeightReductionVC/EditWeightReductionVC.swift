//
//  EditWeightReductionVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 13/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class EditWeightReductionVC: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UIAlertViewDelegate {
    
    // FIXME: - VIEW CONTROLLER METHODS
    @IBOutlet var txtCaloryPerDay:UITextField!
    @IBOutlet var txtNumberOfDay:UITextField!
    
    
    @IBOutlet var viewPicker:UIView!
    @IBOutlet var pickerView:UIPickerView!
    var pickerSelIndx:Int!
    var pickOption:NSMutableArray!
    
    
    @IBOutlet var lblHeader:UILabel!
    @IBOutlet var lblTitle1:UILabel!
    @IBOutlet var lblTitle2:UILabel!
    @IBOutlet var lblTitle3:UILabel!
    @IBOutlet var lblTitle4:UILabel!
    
    @IBOutlet var btnSave:UIButton!
    
    var weightProgramID:String!
    var myProgramData:NSDictionary!
    
    var caloryPerDayFromAPI : String!
    
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        
        print("weightProgramID :: -> ",weightProgramID)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
        
        
        pickerSelIndx=0
        pickOption = NSMutableArray()
        pickOption = ["5","6","7"]
        pickerView.reloadAllComponents()
        
        viewPicker.frame = CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: viewPicker.frame.size.width, height: viewPicker.frame.size.height)
        
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetSingleWeightProgram), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        self.addTapGestureInOurView()
        self.setLocalizationText()
        self.NavBarNumberPad()
    }
    

    func setLocalizationText(){
        lblHeader.text = NSLocalizedString("Weight Reduction Program", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
        lblTitle1.text = NSLocalizedString("The Calories intake to maintain your weight", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        //lblTitle1.text = NSLocalizedString("How much calorie you should consume per day?", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitle2.text = NSLocalizedString("Your calorie intake", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
        lblTitle3.text = NSLocalizedString("How many days do you want to fasting within the program?", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        //lblTitle3.text = NSLocalizedString("How many days do you want to fasting in a week?", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitle4.text = NSLocalizedString("Select number of days", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
        btnSave.setTitle(NSLocalizedString("SAVE", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
      
    }
    

    
    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        
        if viewTouched!.isKind(of: UIButton.self) && viewTouched!.isKind(of:UITextView.self){
            
        }
        else{
            self.view.endEditing(true)
            //            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
            
            
            if (viewTouched!.superview!.superview == viewPicker) {
            }else if(viewTouched!.superview == viewPicker){
            }else if(viewTouched == viewPicker){
            }else{
                UIView.animate(withDuration: 0.3) {
                    
                    self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
                    
                    
                    
                }
                
            }
            
            
            
            
        }
    }
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    
    //AlertDelegate Methods protocol
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }

    
    func editWeightProgramData(weightProgramData:NSDictionary){
        if let numberOfDay = weightProgramData.value(forKey: "days") as? String{
            txtNumberOfDay.text = "\(numberOfDay)"
        }
        if let caloriePerDay = weightProgramData.value(forKey: "kcal") as? String{
            txtCaloryPerDay.text = "\(caloriePerDay)"
            caloryPerDayFromAPI = "\(caloriePerDay)"
        }
    }
    
    // Numeric Keyboard Hide Method
    func NavBarNumberPad(){
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        numberToolbar.barStyle = .default
        
        let viewBack = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        viewBack.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        numberToolbar.addSubview(viewBack)
        
        let btnCancel = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelNumberPad))
        btnCancel.tintColor = UIColor.white
        
        let btnDone = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.doneWithNumberPad))
        btnDone.tintColor = UIColor.white
        
        numberToolbar.items = NSArray(objects: btnCancel,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),btnDone) as? [UIBarButtonItem]
        
        
        numberToolbar.sizeToFit()
        
        txtCaloryPerDay.inputAccessoryView = numberToolbar
        
    }
    
    func cancelNumberPad(){
        txtCaloryPerDay.resignFirstResponder()
    }
    
    func doneWithNumberPad(){
        txtCaloryPerDay.resignFirstResponder()
    }
    
    // TODO: - DELEGATE METHODS
    
    // MARK: - PICKERVIEW DELEGATE
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickOption.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        // return (self.pickOption[row] as AnyObject).value(forKey: "title") as? String
        return self.pickOption[row] as? String
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50.0
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let label = UILabel()
        label.textAlignment = NSTextAlignment.center
        
        label.font = UIFont.systemFont(ofSize: 20.0)
        label.text = self.pickOption[row] as? String
        return label
    }
    
    
    
    // TODO: - ACTION METHODS
    
    @IBAction func btnResetClicked(_ sender: Any) {
        //txtCaloryPerDay.text = caloryPerDayFromAPI
        
        let weightReductForm1VC = WeightReductForm1VC(nibName: "WeightReductForm1VC", bundle: nil)
        self.navigationController?.pushViewController(weightReductForm1VC, animated: true)
    }
    
    @IBAction func btnNextClicked(_ sender: UIButton) {
        
        if txtCaloryPerDay.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter calorie per days.", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if txtNumberOfDay.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please select number of days.", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else{
            print("Weight Reduction Program Data",APPDELEGATE.weightReductionData)
            
            if Reachability.isConnectedToNetwork() == true {
                SVProgressHUD.show(withStatus: "Loading..")
                
                APPDELEGATE.weightReductionData.setValue(txtCaloryPerDay.text, forKey: "caloryPerDay")
                APPDELEGATE.weightReductionData.setValue(txtNumberOfDay.text, forKey: "numberofDay")
                
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForEditWeightReductionData), userInfo: nil, repeats: false)
            } else {
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }
    }
    
    // TODO: - ACTION METHODS
    @IBAction func btnSelectNumberOfDayClicked(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y - self.viewPicker.frame.size.height, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
            
        })
    }
    
    
    @IBAction func cancelPicker(sender:AnyObject){
        UIView.animate(withDuration: 0.3) {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
        }
    }
    
    @IBAction func donePicker(sender:UIButton){
        
        pickerSelIndx = pickerView.selectedRow(inComponent: 0)
        print("Picker Index is  :-> \(pickerSelIndx) and value is \(pickOption[pickerSelIndx])")
        txtNumberOfDay.text = self.pickOption[pickerSelIndx] as? String
        //dishID = ((self.dishData[pickerSelIndx] as AnyObject).value(forKey: "_id") as? String)!
        
        
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
        })
    }
    
    
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForEditWeightReductionData(){
        let completeURL = NSString(format:"%@%@", MainURL,editWeightDataURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "kcal":APPDELEGATE.weightReductionData.value(forKey: "caloryPerDay") as! String,
            "days":APPDELEGATE.weightReductionData.value(forKey: "numberofDay") as! String,
            "wp_id":myProgramData.value(forKey: "wp_id") as! String,
            "start_date":myProgramData.value(forKey: "start_date") as! String
        ]
        
//        myProgramData.value(forKey: "height") as! String
//        myProgramData.value(forKey: "weight") as! String
//        myProgramData.value(forKey: "activity") as! String
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("editWeightData API Parameter :",finalParams)
        print("editWeightData API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: editWeightDataURLTag)
        
    }
    func postDataOnWebserviceForGetSingleWeightProgram(){
        let completeURL = NSString(format:"%@%@", MainURL,getSingleWeightDataURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "wp_id":self.weightProgramID,
        ]
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("GetSingleWeightProgram API Parameter :",finalParams)
        print("GetSingleWeightProgram API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getSingleWeightDataURLTag)
        
    }

    
    
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case editWeightDataURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("editWeightData Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{

                self.weightProgramID = (resultDict.value(forKey: "data") as AnyObject).value(forKey: "wp_id") as! String

                let notificationName = Notification.Name("refreshMyWeightProgramData")
                NotificationCenter.default.post(name: notificationName, object: nil)
                
                self.performSelector(inBackground: #selector(self.postDataOnWebserviceForGetSingleWeightProgram), with: nil)
                
                self.navigationController?.popViewController(animated: true)
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
                
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            
            SVProgressHUD.dismiss()
            break
            
        case getSingleWeightDataURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("Single Weight Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                self.editWeightProgramData(weightProgramData: resultDict.value(forKey: "data") as! NSDictionary)
                myProgramData = resultDict.value(forKey: "data") as! NSDictionary
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            SVProgressHUD.dismiss()
            break

            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case editWeightDataURLTag:
            SVProgressHUD.dismiss()
            break
            
        case getSingleWeightDataURLTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    
}
