//
//  CaloriTableVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 22/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class CaloriTableVC: UITableViewCell {

    @IBOutlet var lblCalery:UILabel!
    @IBOutlet var lblFoodName:UILabel!
    @IBOutlet var lblOtherDetail:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
