//
//  WeightReductForm4VC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 02/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class WeightReductForm4VC: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UIAlertViewDelegate {
    
    // FIXME: - VIEW CONTROLLER METHODS
    @IBOutlet var txtCaloryPerDay:UITextField!
    @IBOutlet var txtNumberOfDay:UITextField!
    @IBOutlet var txtDate:UITextField!

    @IBOutlet var viewPicker:UIView!
    @IBOutlet var pickerView:UIPickerView!
    var pickerSelIndx:Int!
    var pickOption:NSMutableArray!
    
    
    @IBOutlet var datePicker:UIDatePicker!
    @IBOutlet var viewDatePicker:UIView!

    @IBOutlet var lblHeader:UILabel!
    @IBOutlet var lblTitle1:UILabel!
    @IBOutlet var lblTitle2:UILabel!
    @IBOutlet var lblTitle3:UILabel!
    @IBOutlet var lblTitle4:UILabel!
    @IBOutlet var lblTitle5:UILabel!
    @IBOutlet var lblTitle6:UILabel!
    @IBOutlet var lblNext:UILabel!
    
    @IBOutlet var btnCancel:UIButton!
    @IBOutlet var btnDone:UIButton!
    @IBOutlet var btnCancel1:UIButton!
    @IBOutlet var btnDone1:UIButton!
    
    var strDateValue:String!
    var arrCalorieIntake = [String]()
    var myWeightProgramData = NSMutableArray()
    var myWeightProgramGlobalData = NSMutableArray()
    var dateData = NSMutableArray()
    var weightProgramId:String!
    var approxCaloryValue:String!
    var isSelectCaloryIntake = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
        
        
        pickerSelIndx=0
        pickOption = NSMutableArray()
        pickOption = ["5","6","7"]
        pickerView.reloadAllComponents()
        
        viewPicker.frame = CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: viewPicker.frame.size.width, height: viewPicker.frame.size.height)
        
        viewDatePicker.frame = CGRect(x: self.view.frame.size.width - self.viewDatePicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: viewDatePicker.frame.size.width, height: viewDatePicker.frame.size.height)
        
        
        // Set Default value
        
        calculationSalaryIntake()
        
        pickerView.selectRow(0, inComponent: 0, animated: true)
        self.pickerView.reloadAllComponents()
        pickerSelIndx = pickerView.selectedRow(inComponent: 0)
        txtNumberOfDay.text = self.pickOption[pickerSelIndx] as? String
        txtDate.text = getStringDateFromDate(dateFormat: "yyyy-MM-dd", enterDate: Date())
        strDateValue = getStringDateFromDate(dateFormat: "yyyy-MM-dd HH:mm:ss", enterDate: Date())

        
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // TODO: - OTHER METHODS
    func calculationSalaryIntake()
    {
        if(approxCaloryValue == "")
        {
            approxCaloryValue = "0"
        }
        arrCalorieIntake.append("\(Int(approxCaloryValue)!) " + "Calories/day to maintain your weight.".localized)
        arrCalorieIntake.append("\(Int(approxCaloryValue)! - (500*1)) " + "Calories/day to lose 0.5 kg per week.".localized)
        arrCalorieIntake.append("\(Int(approxCaloryValue)! - (500*2)) " + "Calories/day to lose 1 kg per week.".localized)
        arrCalorieIntake.append("\(Int(approxCaloryValue)! - (500*3)) " + "Calories/day by nutrition advice.".localized)
        txtCaloryPerDay.text = arrCalorieIntake[0]
    }
    func generalViewControllerSetting(){
        self.addTapGestureInOurView()
        self.NavBarNumberPad()
        self.setLocalizationText()
        
//        if Reachability.isConnectedToNetwork() == true {
//            SVProgressHUD.show(withStatus: "Loading..")
//            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForMyWeightProgramData), userInfo: nil, repeats: false)
//        } else {
//            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
//        }
        
    }
    
    
    // SET LOCALIZATION
    func setLocalizationText(){
        lblHeader.text = NSLocalizedString("Weight Reduction Program", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitle1.text = NSLocalizedString("The Calories intake to maintain your weight", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        //lblTitle1.text = NSLocalizedString("How much calorie you would like to fasting?", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitle2.text = NSLocalizedString("Your calorie intake", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
        lblTitle3.text = NSLocalizedString("How many days do you want to fasting within the program?", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        //lblTitle3.text = NSLocalizedString("How many days do you want to fasting in a week?", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitle4.text = NSLocalizedString("Select number of days", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitle5.text = NSLocalizedString("When do you want to start?(Within 14 days)", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitle6.text = NSLocalizedString("Select date", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblNext.text = NSLocalizedString("NEXT", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
        btnCancel.setTitle(NSLocalizedString("Cancel", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        btnDone.setTitle(NSLocalizedString("Done", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        btnCancel1.setTitle(NSLocalizedString("Cancel", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        btnDone1.setTitle(NSLocalizedString("Done", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        
    }

    
    
    // Numeric Keyboard Hide Method
    func NavBarNumberPad(){
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        numberToolbar.barStyle = .default
        
        let viewBack = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        viewBack.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        numberToolbar.addSubview(viewBack)
        
        let btnCancel = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelNumberPad))
        btnCancel.tintColor = UIColor.white
        
        let btnDone = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.doneWithNumberPad))
        btnDone.tintColor = UIColor.white
        
        numberToolbar.items = NSArray(objects: btnCancel,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),btnDone) as? [UIBarButtonItem]
        
        
        numberToolbar.sizeToFit()
        
        txtCaloryPerDay.inputAccessoryView = numberToolbar
        
    }
    
    func cancelNumberPad(){
        txtCaloryPerDay.resignFirstResponder()
    }
    
    func doneWithNumberPad(){
        txtCaloryPerDay.resignFirstResponder()
    }
    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    
    @IBAction func btnCaloriesIntakeTapped(_ sender: UIButton) {
        isSelectCaloryIntake = true
        self.pickerView.reloadAllComponents()
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y - self.viewPicker.frame.size.height, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
        })
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        
        if viewTouched!.isKind(of: UIButton.self) && viewTouched!.isKind(of:UITextView.self){
            
        }
        else{
            self.view.endEditing(true)
            //            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
            
            
            if (viewTouched!.superview!.superview == viewPicker) {
            }else if(viewTouched!.superview == viewPicker){
            }else if(viewTouched == viewPicker){
            }else{
                UIView.animate(withDuration: 0.3) {
                    
                    self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
                    
                    
                    
                }
                
            }
            
            if (viewTouched!.superview!.superview == viewDatePicker) {
            }else if(viewTouched!.superview == viewDatePicker){
            }else if(viewTouched == viewDatePicker){
            }else{
                UIView.animate(withDuration: 0.3) {
                    
                    self.viewDatePicker.frame=CGRect(x: self.view.frame.size.width - self.viewDatePicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewDatePicker.frame.size.width, height: self.viewDatePicker.frame.size.height)
                    
                }
            }

            
            
        }
    }
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    
    //AlertDelegate Methods protocol
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }
    
    

    
    
    // TODO: - DELEGATE METHODS
    
    // MARK: - PICKERVIEW DELEGATE
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(isSelectCaloryIntake)
        {
            return self.arrCalorieIntake.count
        }
        return self.pickOption.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        // return (self.pickOption[row] as AnyObject).value(forKey: "title") as? String
        if(isSelectCaloryIntake)
        {
            return self.arrCalorieIntake[row]
        }
        return self.pickOption[row] as? String
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40.0
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let label = UILabel()
        label.textAlignment = NSTextAlignment.center
        
        label.font = UIFont.systemFont(ofSize: 16.0)
        
        if(isSelectCaloryIntake)
        {
            label.text = self.arrCalorieIntake[row]
        }
        else
        {
            label.text = self.pickOption[row] as? String
        }
        return label
    }

    
    
    // TODO: - ACTION METHODS
    @IBAction func btnNextClicked(_ sender: UIButton) {
        
        if txtCaloryPerDay.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter calorie per days.", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if txtNumberOfDay.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please select number of days.", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else if txtDate.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please select start date", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else{
            
            APPDELEGATE.weightReductionData.setValue(txtCaloryPerDay.text, forKey: "caloryPerDay")
            APPDELEGATE.weightReductionData.setValue(txtNumberOfDay.text, forKey: "numberofDay")
            APPDELEGATE.weightReductionData.setValue(txtDate.text, forKey: "date")
            
            print("Weight Reduction Program Data",APPDELEGATE.weightReductionData)
            
//            if Reachability.isConnectedToNetwork() == true {
//                SVProgressHUD.show(withStatus: "Loading..")
//                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForSaveWeightReductionData), userInfo: nil, repeats: false)
//            } else {
//                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
//            }
            
            let finalDate = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: strDateValue, newFormat: "yyyy-MM-dd")
            let myIndexValue:Int = dateData.index(of: "\(finalDate)")
            if self.dateData.count > myIndexValue{
                
                weightProgramId = (myWeightProgramData.object(at: myIndexValue) as AnyObject).value(forKey: "wp_id") as! String
                
                if Reachability.isConnectedToNetwork() == true {
                    SVProgressHUD.show(withStatus: "Loading..")
                    Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForEditWeightReductionData), userInfo: nil, repeats: false)
                } else {
                    showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                }

            }
            else{
                
                weightProgramId = ""
                if Reachability.isConnectedToNetwork() == true {
                    SVProgressHUD.show(withStatus: "Loading..")
                    Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForSaveWeightReductionData), userInfo: nil, repeats: false)
                } else {
                    showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                }

            }
            
            print("Weight Program ID : ",weightProgramId)
            
            
            
        }
        
    
    }
    
    // TODO: - ACTION METHODS
    @IBAction func btnSelectNumberOfDayClicked(_ sender: UIButton) {
        isSelectCaloryIntake = false
        self.pickerView.reloadAllComponents()
        UIView.animate(withDuration: 0.3, animations: {
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y - self.viewPicker.frame.size.height, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
        })
    }
    
    @IBAction func btnPickerSelectedClicked(_ sender:UIButton){
        self.view.endEditing(true)
        
        //        if txtSearch.text != "" {
        //            let str = txtSearch.text!
        //            let dateFormater = DateFormatter()
        //            dateFormater.dateFormat = "yyyy-MM-dd"
        //            var dateFromString = Date.init()
        //            dateFromString = dateFormater.date(from: str as String)!
        //            datePicker.setDate(dateFromString as Date, animated: true)
        //        }
        
        datePicker.minimumDate = NSDate() as Date
        datePicker.datePickerMode = .date;
        
        
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewDatePicker.frame=CGRect(x: self.view.frame.size.width - self.viewDatePicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y - self.viewDatePicker.frame.size.height, width: self.viewDatePicker.frame.size.width, height: self.viewDatePicker.frame.size.height)
            
            
        })
        
        
    }

    
    
    @IBAction func cancelPicker(sender:AnyObject){
        UIView.animate(withDuration: 0.3) {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
        }
        
        UIView.animate(withDuration: 0.3) {
            
            self.viewDatePicker.frame=CGRect(x: self.view.frame.size.width - self.viewDatePicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewDatePicker.frame.size.width, height: self.viewDatePicker.frame.size.height)
            
        }

        
    }
    
    @IBAction func donePicker(sender:UIButton){
        
        
        //dishID = ((self.dishData[pickerSelIndx] as AnyObject).value(forKey: "_id") as? String)!
        if(isSelectCaloryIntake)
        {
            txtCaloryPerDay.text = arrCalorieIntake[self.pickerView.selectedRow(inComponent: 0)]
        }
        else
        {
            pickerSelIndx = pickerView.selectedRow(inComponent: 0)
            print("Picker Index is  :-> \(pickerSelIndx) and value is \(pickOption[pickerSelIndx])")
            txtNumberOfDay.text = self.pickOption[pickerSelIndx] as? String
        }
        
        
        
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
        })
    }

    @IBAction func doneDatePicker(sender:UIButton){
        
        txtDate.text = getStringDateFromDate(dateFormat: "yyyy-MM-dd", enterDate: datePicker.date)
        strDateValue = getStringDateFromDate(dateFormat: "yyyy-MM-dd HH:mm:ss", enterDate: datePicker.date)
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewDatePicker.frame=CGRect(x: self.view.frame.size.width - self.viewDatePicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewDatePicker.frame.size.width, height: self.viewDatePicker.frame.size.height)
            
        })

        
        
    }
    
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForSaveWeightReductionData(){
        let completeURL = NSString(format:"%@%@", MainURL,saveWeightData) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "weight":APPDELEGATE.weightReductionData.value(forKey: "weight") as! String,
            "height":APPDELEGATE.weightReductionData.value(forKey: "height") as! String,
            "activity":APPDELEGATE.weightReductionData.value(forKey: "activity") as! String,
            "kcal":APPDELEGATE.weightReductionData.value(forKey: "caloryPerDay") as! String,
            "days":APPDELEGATE.weightReductionData.value(forKey: "numberofDay") as! String,
            "start_date":strDateValue
            ]
        
        
            let finalParams:NSDictionary = [
                "data" : params
            ]
        
        
        
        print("SaveWeightReductionData API Parameter :",finalParams)
        print("SaveWeightReductionData API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: saveWeightDataURLTag)
        
    }
    
    func postDataOnWebserviceForMyWeightProgramData(){
        let completeURL = NSString(format:"%@%@", MainURL,myWeightProgramDataURL) as String
        
        let date = Date()
        //let calendar = Calendar.current
        
        //let month = calendar.component(.month, from: date)
        //let year = calendar.component(.year, from: date)
        
        let formate = DateFormatter()
        formate.dateFormat = "MM"
        let month = formate.string(from: date)
        
        formate.dateFormat = "yyyy"
        let year = formate.string(from: date)
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "month": "\(month)",
            "year": "\(year)",
            "lang_type":Language_Type
        ]
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("MyWeightProgramData API Parameter :",finalParams)
        print("MyWeightProgramData API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: myWeightProgramDataURLTag)
        
    }
    
    func postDataOnWebserviceForEditWeightReductionData(){
        let completeURL = NSString(format:"%@%@", MainURL,editWeightDataURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "kcal":APPDELEGATE.weightReductionData.value(forKey: "caloryPerDay") as! String,
            "days":APPDELEGATE.weightReductionData.value(forKey: "numberofDay") as! String,
            "weight":APPDELEGATE.weightReductionData.value(forKey: "weight") as! String,
            "height":APPDELEGATE.weightReductionData.value(forKey: "height") as! String,
            "activity":APPDELEGATE.weightReductionData.value(forKey: "activity") as! String,
            "wp_id":weightProgramId,
            "start_date":strDateValue
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("editWeightData API Parameter :",finalParams)
        print("editWeightData API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: editWeightDataURLTag)
        
    }

    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case saveWeightDataURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("SaveWeightReductionData Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                USERDEFAULT.set("NO", forKey: "isFirstTimeWeightReduct")
                USERDEFAULT.synchronize()
                
                let weightProgramID = (resultDict.value(forKey: "data") as AnyObject).value(forKey: "wp_id") as! String
                USERDEFAULT.set(weightProgramID, forKey: "weightProgramID")
                USERDEFAULT.synchronize()

                
                let weightReductForm5VC = WeightReductForm5VC(nibName: "WeightReductForm5VC", bundle: nil)
                weightReductForm5VC.isFromAddWeightProgram = true
                self.navigationController?.pushViewController(weightReductForm5VC, animated: true)

                let notificationName = Notification.Name("refreshMyWeightProgramData")
                NotificationCenter.default.post(name: notificationName, object: nil)

                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            
            SVProgressHUD.dismiss()
            break
            
        case myWeightProgramDataURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("MyWeightProgramData Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                myWeightProgramData = NSMutableArray(array: resultDict.value(forKey: "data") as! NSArray)
                myWeightProgramGlobalData = NSMutableArray(array: resultDict.value(forKey: "data") as! NSArray)
                
                if myWeightProgramData.count > 0{
                    for i in 0...myWeightProgramData.count - 1 {
                        
                        let aa = (myWeightProgramData.object(at: i) as AnyObject).value(forKey: "start_date") as! String
                        let finalDate = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: aa, newFormat: "yyyy-MM-dd")
                        dateData.add(finalDate)
                    }
                }
                print("date Data",dateData)
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            SVProgressHUD.dismiss()
            break

        case editWeightDataURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("editWeightData Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                USERDEFAULT.set("NO", forKey: "isFirstTimeWeightReduct")
                USERDEFAULT.synchronize()
                
                let weightProgramID = (resultDict.value(forKey: "data") as AnyObject).value(forKey: "wp_id") as! String
                USERDEFAULT.set(weightProgramID, forKey: "weightProgramID")
                USERDEFAULT.synchronize()
                
                
                let weightReductForm5VC = WeightReductForm5VC(nibName: "WeightReductForm5VC", bundle: nil)
                weightReductForm5VC.isFromAddWeightProgram = true
                self.navigationController?.pushViewController(weightReductForm5VC, animated: true)
                
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
                
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            
            SVProgressHUD.dismiss()
            break
   
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case saveWeightDataURLTag:
            SVProgressHUD.dismiss()
            break
        case editWeightDataURLTag:
            SVProgressHUD.dismiss()
            break
        case myWeightProgramDataURLTag:
            SVProgressHUD.dismiss()
            break
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    
}
