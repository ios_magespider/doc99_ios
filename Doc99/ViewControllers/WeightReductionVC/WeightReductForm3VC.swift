//
//  WeightReductForm3VC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 02/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class WeightReductForm3VC: UIViewController {
    
    // FIXME: - VIEW CONTROLLER METHODS
    @IBOutlet var lblCalory:UILabel!

    @IBOutlet var lblHeader:UILabel!
    @IBOutlet var lblNext:UILabel!
    @IBOutlet var lblHeader1:UILabel!

    var caloryValue:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        lblCalory.text  = "\(caloryValue!) kcal"
        APPDELEGATE.weightReductionData.setValue(caloryValue, forKey: "energyCalory")
        self.setLocalizationText()
    }
    func setLocalizationText(){
        lblHeader.text = NSLocalizedString("Weight Reduction Program", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblHeader1.text = NSLocalizedString("The amount of energy required for you everyday approximate:", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblNext.text = NSLocalizedString("NEXT", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        

        
    }
    

    // TODO: - DELEGATE METHODS
    // TODO: - ACTION METHODS
    @IBAction func btnNextClicked(_ sender: UIButton) {
        if APPDELEGATE.weightReductionData.value(forKey: "energyCalory") != nil{
            let weightReductForm4VC = WeightReductForm4VC(nibName: "WeightReductForm4VC", bundle: nil)
            weightReductForm4VC.approxCaloryValue = caloryValue
            self.navigationController?.pushViewController(weightReductForm4VC, animated: true)
        }
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        APPDELEGATE.weightReductionData.setValue(nil, forKey: "energyCalory")
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    // TODO: - POST DATA METHODS
}
