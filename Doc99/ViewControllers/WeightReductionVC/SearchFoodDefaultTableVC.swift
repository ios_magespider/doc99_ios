//
//  SearchFoodDefaultTableVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 10/07/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class SearchFoodDefaultTableVC: UITableViewCell {

    
    @IBOutlet var lblFoodName:UILabel!
    @IBOutlet var btnCancel:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
