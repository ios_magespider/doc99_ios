//
//  WeightReductForm2VC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 02/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class WeightReductForm2VC: UIViewController {
    
    // FIXME: - VIEW CONTROLLER METHODS
    @IBOutlet var btnSelect1:UIButton!
    @IBOutlet var btnSelect2:UIButton!
    @IBOutlet var btnSelect3:UIButton!
    
    
    @IBOutlet var imageViewSelect1:UIImageView!
    @IBOutlet var imageViewSelect2:UIImageView!
    @IBOutlet var imageViewSelect3:UIImageView!
    
    
    @IBOutlet var lblSelect1:UILabel!
    @IBOutlet var lblSelect2:UILabel!
    @IBOutlet var lblSelect3:UILabel!
    
    
    @IBOutlet var lblHeader:UILabel!
    @IBOutlet var lblTitleLowActivity1:UILabel!
    @IBOutlet var lblTitleHighActivity1:UILabel!
    @IBOutlet var lblObeseActivity1:UILabel!
    @IBOutlet var lblActivityCondition:UILabel!
    @IBOutlet var lblNext:UILabel!
    
    var caloryValue:String!
    var weightValue:String!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        self.setLocalizationText()

    }
    
    func setLocalizationText(){
        lblHeader.text = NSLocalizedString("Weight Reduction Program", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblNext.text = NSLocalizedString("NEXT", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
        lblSelect1.text = NSLocalizedString("Low Activity", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblSelect2.text = NSLocalizedString("High Activity", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblSelect3.text = NSLocalizedString("Obese Conditions", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

        
        lblTitleLowActivity1.text = NSLocalizedString("Daily activities effort in light exercises such as periods of standing, physical work.", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitleHighActivity1.text = NSLocalizedString("Daily activities effort in intense exercises such regular exercise, moderate physical work.", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblObeseActivity1.text = NSLocalizedString("BMI over 25 with low activities", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

        lblActivityCondition.text = NSLocalizedString("What is your activities condition?", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
    }

    func setSelectedButton(radioButton:UIButton) {
        let image = UIImage.init(named: "health_check.png")
        let image1 = UIImage.init(named: "health_Uncheck.png")
        
        if radioButton == btnSelect1{
            imageViewSelect1.image = image
            imageViewSelect2.image = image1
            imageViewSelect3.image = image1
       
            print("Selected : ",lblSelect1.text ?? "Nothing")
            
            let weight = Int(weightValue)
            let finalValue = ((weight! * 30) - 35)
            caloryValue = "\(finalValue)"
            print("Calory Value : ",caloryValue)
            APPDELEGATE.weightReductionData.setValue(caloryValue, forKey: "activity")
        }
        else if radioButton == btnSelect2{
            imageViewSelect2.image = image
            imageViewSelect3.image = image1
       
            imageViewSelect1.image = image1
            print("Selected : ",lblSelect2.text ?? "Nothing")

            let weight = Int(weightValue)
            let finalValue = ((weight! * 35) - 40)
            caloryValue = "\(finalValue)"
            print("Calory Value : ",caloryValue)
            
            APPDELEGATE.weightReductionData.setValue(caloryValue, forKey: "activity")
        }
        else if radioButton == btnSelect3{
            imageViewSelect3.image = image
            imageViewSelect1.image = image1
            imageViewSelect2.image = image1
            print("Selected : ",lblSelect3.text ?? "Nothing")
            
            let weight = Int(weightValue)
            let finalValue = ((weight! * 25) - 35)
            caloryValue = "\(finalValue)"
            print("Calory Value : ",caloryValue)
            
            APPDELEGATE.weightReductionData.setValue(caloryValue, forKey: "activity")
        }
        
    }

    
    // TODO: - DELEGATE METHODS
    // TODO: - ACTION METHODS
    @IBAction func btnNextClicked(_ sender: UIButton) {
        
        if APPDELEGATE.weightReductionData.value(forKey: "activity") == nil{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please select your activity", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        else{
            let weightReductForm3VC = WeightReductForm3VC(nibName: "WeightReductForm3VC", bundle: nil)
            weightReductForm3VC.caloryValue = caloryValue
            self.navigationController?.pushViewController(weightReductForm3VC, animated: true)
        }
        
        
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActivityConditionSelectionClicked(_ sender: UIButton) {
        
        if sender == btnSelect1{
            self.setSelectedButton(radioButton: btnSelect1)
        }
        else if sender == btnSelect2{
            self.setSelectedButton(radioButton: btnSelect2)
        }
        else if sender == btnSelect3{
            self.setSelectedButton(radioButton: btnSelect3)
        }
    }
    
    
    // TODO: - POST DATA METHODS
}
