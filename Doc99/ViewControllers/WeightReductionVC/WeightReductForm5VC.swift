//
//  WeightReductForm5VC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 02/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD
import JTAppleCalendar

class WeightReductForm5VC: UIViewController,UIAlertViewDelegate {
    
    // FIXME: - VIEW CONTROLLER METHODS
    @IBOutlet var lblHeader:UILabel!
    @IBOutlet var lblFoodCaloryList:UILabel!
    @IBOutlet var lblSpecialist:UILabel!

    var myWeightProgramData = NSMutableArray()
    var myWeightProgramGlobalData = NSMutableArray()

    var weightProgramID:String!
    
    
    // CALENDER
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var weekView: UIView!

    @IBOutlet weak var monthNewLabel: UILabel!

    
    @IBOutlet var numbers: [UIButton]!
    @IBOutlet var headerss: [UIButton]!
    @IBOutlet var directions: [UIButton]!
    @IBOutlet var outDates: [UIButton]!
    @IBOutlet var inDates: [UIButton]!
    
    var numberOfRows = 6
    let formatter = DateFormatter()
    var testCalendar = Calendar.current
    var generateInDates: InDateCellGeneration = .forAllMonths
    var generateOutDates: OutDateCellGeneration = .tillEndOfGrid
    var prePostVisibility: ((CellState, CellView?)->())?
    var hasStrictBoundaries = true
    let firstDayOfWeek: DaysOfWeek = .monday
    let disabledColor = UIColor.lightGray
    let enabledColor = UIColor.blue
    let dateCellSize: CGFloat? = nil
    var monthSize: MonthSize? = nil
    var prepostHiddenValue = false
    
    let red = UIColor.red
    let white = UIColor.white
    let black = UIColor.black
    let gray = UIColor.gray
    let shade = UIColor(colorWithHexValue: 0x4E4E4E)
    
    
    let reuseIdentifier = "CellView"

    var dateData = NSMutableArray()
    var weightProgramDateData = NSMutableArray()


    var isFromAddWeightProgram:Bool?
    

    var selectedPassDate:String!
    var selectedCalory:String!
    
    
    var wrp_month = ""
    var wrp_year = ""

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let date = Date()
        let formate = DateFormatter()
        formate.dateFormat = "MM"
        wrp_month = formate.string(from: date)
        formate.dateFormat = "yyyy"
        wrp_year = formate.string(from: date)
        
        self.generalViewControllerSetting()
        
        // Define identifier
        let notificationName3 = Notification.Name("refreshMyWeightProgramData")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(refreshMyWeightData(notification:)), name: notificationName3, object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        
        hideOutsideHeaders(UIButton())
        //        calendarView.itemSize = CGFloat(53.43 - 20)
        
        
        //        testCalendar = Calendar(identifier: .gregorian)
        //        let timeZone = TimeZone(identifier: "Asia/Amman")!
        //        testCalendar.timeZone = timeZone
        //
        //        let locale = Locale(identifier: "ar_JO")
        //        testCalendar.locale = locale
        
        //        calendarView.calendarDataSource = self
        //        calendarView.calendarDelegate = self
        // ___________________________________________________________________
        // Registering header cells is optional
        
        calendarView.register(UINib(nibName: "CellView", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        
        
        calendarView.register(UINib(nibName: "PinkSectionHeaderView", bundle: Bundle.main),
                              forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,
                              withReuseIdentifier: "PinkSectionHeaderView")
        
        //        calendarView.registerDecorationView(nib: UINib(nibName: "SectionDecorationView", bundle: Bundle.main))
        
        calendarView.allowsMultipleSelection = false
        calendarView.isRangeSelectionUsed = true
        //
        
//        let panGensture = UILongPressGestureRecognizer(target: self, action: #selector(didStartRangeSelecting(gesture:)))
//        panGensture.minimumPressDuration = 0.5
//        calendarView.addGestureRecognizer(panGensture)
        //        calendarView.rangeSelectionWillBeUsed = true
        //        calendarView.isRangeSelectionUsed = true
        
        
        prePostVisibility = {state, cell in
            if state.dateBelongsTo == .thisMonth {
                cell?.isHidden = false
            } else {
                cell?.isHidden = true
            }
        }
        
        calendarView.cellSize += 10
        
        self.calendarView.reloadData() {
            self.calendarView.visibleDates {[unowned self] (visibleDates: DateSegmentInfo) in
                self.setupViewsOfCalendar(from: visibleDates)
            }
            
            self.weekView.isHidden = false
            self.monthLabel.isHidden = false
            let btn  = UIButton.init()
            btn.setTitle("Horizontal", for: .normal)
            self.changeDirection(btn)
            
            let btn1  = UIButton.init()
            btn1.setTitle("6", for: .normal)
            self.changeToRow(btn1)

        }
        
        
        
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForMyWeightProgramData), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        self.setLocalizationText()

    }
    
    func setLocalizationText(){
        lblHeader.text = NSLocalizedString("Weight Reduction Program", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        //lblFoodCaloryList.text = NSLocalizedString("FOOD CALORIE LIST", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblFoodCaloryList.text = "INPUT MEAL LIST"
        lblSpecialist.text = NSLocalizedString("You can order the food from our specialist, by clicking calendar.", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

    }
    func refreshMyWeightData(notification: NSNotification) {
        dateData = NSMutableArray()
        weightProgramDateData = NSMutableArray()
        calendarView.reloadData()

        calendarView.deselectAllDates(triggerSelectionDelegate: true)
        
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForMyWeightProgramData), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }

    }

    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    
    
    //AlertDelegate Methods protocol
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }
    

    

    
    // TODO: - DELEGATE METHODS
    // TODO: - ACTION METHODS
    
    @IBAction func btnFoodCaleryListClicked(_ sender: UIButton) {
//        let weightReductForm6VC = WeightReductForm6VC(nibName: "WeightReductForm6VC", bundle: nil)
//        self.navigationController?.pushViewController(weightReductForm6VC, animated: true)
        
        if weightProgramID == nil || weightProgramID == ""{
            showAlert(Appname, title: "Please select Program Date from calender")
        }
        else{
            let recordCalorieVC = RecordCalorieVC(nibName: "RecordCalorieVC", bundle: nil)
            recordCalorieVC.weightProgramID = self.weightProgramID
            recordCalorieVC.selectedPassDate = self.selectedPassDate
            recordCalorieVC.selectedCalory = self.selectedCalory
            self.navigationController?.pushViewController(recordCalorieVC, animated: true)
        }

        
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        
        if isFromAddWeightProgram == true{
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
            for aViewController:UIViewController in viewControllers {
                if aViewController.isKind(of: WeightReductDetailVC.self) {
                    _ = self.navigationController?.popToViewController(aViewController, animated: true)
                }
            }

        }
        else{
        _  = self.navigationController?.popViewController(animated: true)        
        }
        
    
    }
    @IBAction func btnSettingClicked(_ sender: UIButton) {
        
        if weightProgramID == nil || weightProgramID == ""{
            showAlert(Appname, title: "Please select Program Date from calender")
        }
        else{
            let editWeightReduction = EditWeightReductionVC(nibName: "EditWeightReductionVC", bundle: nil)
            editWeightReduction.weightProgramID = self.weightProgramID
            self.navigationController?.pushViewController(editWeightReduction, animated: true)
        }
    }

    // CALENDER ACTION METHODS
    @IBAction func scrollNone(_ sender: Any) {
        calendarView.scrollingMode = .none
    }
    
    @IBAction func scrollFixed(_ sender: Any) {
        calendarView.scrollingMode = .stopAtEachCalendarFrame
    }
    @IBAction func showPrepost(_ sender: UIButton) {
        prePostVisibility = {state, cell in
            cell?.isHidden = false
        }
        calendarView.reloadData()
    }
    @IBAction func hidePrepost(_ sender: UIButton) {
        prePostVisibility = {state, cell in
            if state.dateBelongsTo == .thisMonth {
                cell?.isHidden = false
            } else {
                cell?.isHidden = true
            }
        }
        calendarView.reloadData()
    }
    
    @IBAction func showOutsideHeaders(_ sender: UIButton) {
        monthLabel.isHidden = false
        weekView.isHidden = false
    }
    @IBAction func hideOutsideHeaders(_ sender: UIButton) {
        monthLabel.isHidden = true
        weekView.isHidden = true
    }
    
    @IBAction func decreaseCellInset(_ sender: UIButton) {
        calendarView.minimumLineSpacing -= 0.5
        calendarView.minimumInteritemSpacing -= 0.5
        calendarView.reloadData()
    }
    
    @IBAction func increaseCellInset(_ sender: UIButton) {
        calendarView.minimumLineSpacing += 0.5
        calendarView.minimumInteritemSpacing += 0.5
        calendarView.reloadData()
    }
    
    
    @IBAction func decreaseItemSize(_ sender: UIButton) {
        calendarView.cellSize -= 1
        calendarView.reloadData()
    }
    
    @IBAction func increaseItemSize(_ sender: UIButton) {
        calendarView.cellSize += 1
        calendarView.reloadData()
    }
    
    
    @IBAction func changeToRow(_ sender: UIButton) {
        numberOfRows = Int(sender.title(for: .normal)!)!
        
//        for aButton in numbers {
//            aButton.tintColor = disabledColor
//        }
//        sender.tintColor = enabledColor
        calendarView.reloadData()
    }
    
    @IBAction func changeDirection(_ sender: UIButton) {
//        for aButton in directions {
//            aButton.tintColor = disabledColor
//        }
//        sender.tintColor = enabledColor
        
        if sender.title(for: .normal)! == "Horizontal" {
            calendarView.scrollDirection = .horizontal
            calendarView.cellSize = 0
        } else {
            calendarView.scrollDirection = .vertical
            calendarView.cellSize = 25
        }
        calendarView.reloadData()
    }
    
    @IBAction func toggleStrictBoundary(sender: UIButton) {
        hasStrictBoundaries = !hasStrictBoundaries
        if hasStrictBoundaries {
            sender.tintColor = enabledColor
        } else {
            sender.tintColor = disabledColor
        }
        calendarView.reloadData()
    }
    
    @IBAction func headers(_ sender: UIButton) {
        for aButton in headerss {
            aButton.tintColor = disabledColor
        }
        sender.tintColor = enabledColor
        
        if sender.title(for: .normal)! == "HeadersOn" {
            monthSize = MonthSize(defaultSize: 50, months: [75: [.feb, .apr]])
        } else {
            monthSize = nil
        }
        calendarView.reloadData()
    }
    
    @IBAction func outDateGeneration(_ sender: UIButton) {
        for aButton in outDates {
            aButton.tintColor = disabledColor
        }
        sender.tintColor = enabledColor
        
        switch sender.title(for: .normal)! {
        case "EOR":
            generateOutDates = .tillEndOfRow
        case "EOG":
            generateOutDates = .tillEndOfGrid
        case "OFF":
            generateOutDates = .off
        default:
            break
        }
        calendarView.reloadData()
        
    }
    
    @IBAction func inDateGeneration(_ sender: UIButton) {
        for aButton in inDates {
            aButton.tintColor = disabledColor
        }
        sender.tintColor = enabledColor
        
        switch sender.title(for: .normal)! {
        case "First":
            generateInDates = .forFirstMonthOnly
        case "All":
            generateInDates = .forAllMonths
        case "Off":
            generateInDates = .off
        default:
            break
        }
        
        calendarView.reloadData()
    }
    
    
    // TODO: - POST DATA METHODS
    
    func postDataOnWebserviceForMyWeightProgramData(){
        let completeURL = NSString(format:"%@%@", MainURL,myWeightProgramDataURL) as String
        
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "month": wrp_month,
            "year": wrp_year,
            "lang_type": Language_Type
        ]
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("MyWeightProgramData API Parameter :",finalParams)
        print("MyWeightProgramData API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: myWeightProgramDataURLTag)
        
    }
   
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case myWeightProgramDataURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("MyWeightProgramData Response  : \(resultDict)")
            
            dateData.removeAllObjects()
            weightProgramDateData.removeAllObjects()
            
            if resultDict.value(forKey: "status") as! String == "1"{
                myWeightProgramData = NSMutableArray(array: resultDict.value(forKey: "data") as! NSArray)
                myWeightProgramGlobalData = NSMutableArray(array: resultDict.value(forKey: "data") as! NSArray)
//                let oldDate = dateFormater.date(from: "2017-01-03 18:30:00")
//                let newDate = dateFormater.date(from: "2017-01-10 18:30:00")
                if myWeightProgramData.count > 0{
                    for i in 0...myWeightProgramData.count - 1 {
                        
                        let data = myWeightProgramData[i] as! NSDictionary
                        print(data)
                        let wp_id = data["wp_id"] as? String ?? ""
                        //let wpm_id = data["wpm_id"] as? String ?? ""
                        let date = data["wpm_date"] as! String
                        //let strTotalCal = data["totalkcal"] as! String
                        let strObtainCal = "\(data["obtainkcal"] as! NSNumber)"
                        let color = "\(data["color"] as! NSNumber)"
                        print("\(date)  =  \(color)")
                        //let per = data["per"] as? String ?? ""
                        /*
                        let totalCal = Int(strTotalCal)!
                        let obtainCal = Int(strObtainCal)!
                        var colorCode = "0"
                        
                        if obtainCal > 0 {
                            if obtainCal < totalCal  {
                                colorCode = "3"
                                print("green")
                            } else if obtainCal > totalCal {
                                colorCode = "2"
                                print("red")
                            } else {
                                colorCode = "1"
                                print("gray")
                            }
                        } else {
                            colorCode = "0"
                        }
                        */
                        //print(wp_id, wpm_id, date, totalCal, obtainCal, color, per)
                        
                        let dictData = NSMutableDictionary()
                        dictData.setValue(date, forKey: "selectedDate")
                        dictData.setValue(wp_id, forKey: "wp_id")
                        dictData.setValue(strObtainCal, forKey: "selectedCalory")
                        dictData.setValue(color, forKey: "dateColor")
                        
                        print(dictData)
                        
                        //self.weightProgramID = wp_id
                        //self.selectedPassDate = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: date, newFormat: "yyyy-MM-dd")
                        //self.selectedCalory = strObtainCal
                        
                        dateData.add(dictData)
                        
                        /*
                        let dateFormater = DateFormatter()
                        dateFormater.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        
                        let FromDate = dateFormater.date(from: (myWeightProgramData.object(at: i) as AnyObject).value(forKey: "start_date") as! String)
                        
                        let days = (myWeightProgramData.object(at: i) as AnyObject).value(forKey: "days") as! String
                        let dayss = Int(days)! - 1
                        
                        if let fromDate = FromDate{
                            let ToDate = testCalendar.date(byAdding: .day, value: dayss, to: fromDate)
                        }
                        for j in 0...dayss {
                            let aa = (myWeightProgramData.object(at: i) as AnyObject).value(forKey: "start_date") as! String
                            let weightID = (myWeightProgramData.object(at: i) as AnyObject).value(forKey: "wp_id") as! String
                            let calory = (myWeightProgramData.object(at: i) as AnyObject).value(forKey: "kcal") as! String

                            let finalDate = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: aa, newFormat: "yyyy-MM-dd")
                            
                            let dateFormater = DateFormatter()
                            dateFormater.dateFormat = "yyyy-MM-dd"
                            let FromDate = dateFormater.date(from: finalDate)
                            let ToDate1 = testCalendar.date(byAdding: .day, value: j, to: FromDate!)
                            let strDate = getStringDateFromDate(dateFormat: "yyyy-MM-dd HH:mm:ss Z", enterDate: ToDate1!)
                            let finalDate1 = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss Z", strDate: strDate, newFormat: "yyyy-MM-dd")

                            let dictData = NSMutableDictionary()
                            dictData.setValue(finalDate1, forKey: "selectedDate")
                            dictData.setValue(weightID, forKey: "wp_id")
                            dictData.setValue(calory, forKey: "selectedCalory")
                            
                            self.weightProgramID = (myWeightProgramData.object(at: i) as AnyObject).value(forKey: "wp_id") as! String
                            self.selectedPassDate = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: (myWeightProgramData.object(at: i) as AnyObject).value(forKey: "start_date") as! String, newFormat: "yyyy-MM-dd")
                            self.selectedCalory = (myWeightProgramData.object(at: i) as AnyObject).value(forKey: "kcal") as! String



                            dateData.add(dictData)
//                            dateData.add(finalDate)
                        }
                        */
                        
                    }
                    
                    if dateData.count > 0{
                        for i in 0...dateData.count - 1 {
                            weightProgramDateData.add((dateData.object(at: i) as AnyObject).value(forKey: "selectedDate") as! String)
                        }
                    }
                    
                    //                calendarView.allowsSelection = false
                }
                print("dateData :- ",dateData)
                print("weightProgramDateData :- ",weightProgramDateData)
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }

            
            
            SVProgressHUD.dismiss()
            calendarView.reloadData()

            break
            
        default:
            break
            
        }
        
    }
    
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case myWeightProgramDataURLTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    func singleSelectedDate(singleDate:Date){
        
//        var dateFormater = DateFormatter()
//        dateFormater.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        let otherDates = NSMutableArray()
//        if dateData.count > 0{
//            for i in 0...dateData.count - 1 {
//                if (dateData.object(at: i) as AnyObject).value(forKey: "selectedDate") as! String != singleDate{
//                    print("1")
//                    let temp = getStringData(currentFormat: "yyyy-MM-dd", strDate: (dateData.object(at: i) as AnyObject).value(forKey: "selectedDate") as! String, newFormat: "yyyy-MM-dd HH:mm:ss")
//                    let FromDate = dateFormater.date(from: temp)
//                    calendarView.selectDates(from: FromDate!, to: FromDate!, keepSelectionIfMultiSelectionAllowed: true)
//                }
//                else{
//                    print("2")
////                    let temp = getStringData(currentFormat: "yyyy-MM-dd", strDate: (dateData.object(at: i) as AnyObject).value(forKey: "selectedDate") as! String, newFormat: "yyyy-MM-dd HH:mm:ss")
////                    let FromDate = dateFormater.date(from: temp)
////                    otherDates.add(FromDate!)
////                    print("Other Date : ",otherDates)
//                }
//            }
//            
//            calendarView.deselect(dates: otherDates as! [Date])
//        }

      //  calendarView.deselectAllDates(triggerSelectionDelegate: true)

        
//        let dateFormater = DateFormatter()
//        dateFormater.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        let temp = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss Z", strDate: singleDate, newFormat: "yyyy-MM-dd HH:mm:ss")
//        let FromDate = dateFormater.date(from: temp)
        calendarView.selectDates(from: singleDate, to: singleDate, keepSelectionIfMultiSelectionAllowed: true)
    }
    
    // CALENDER METHODS
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if let firstDateInfo = calendarView.visibleDates().indates.first {
            calendarView.viewWillTransition(to: size, with: coordinator, anchorDate: firstDateInfo.date)
            
            //calendarView.viewWillTransition(to: size, with: coordinator, focusDateIndexPathAfterRotate: firstDateInfo.indexPath)
        } else {
            let firstDateInfo = calendarView.visibleDates().monthDates.first!
            calendarView.viewWillTransition(to: size, with: coordinator, anchorDate: firstDateInfo.date)
            
            //calendarView.viewWillTransition(to: size, with: coordinator, focusDateIndexPathAfterRotate: firstDateInfo.indexPath)
        }
        
    }
    
    var rangeSelectedDates: [Date] = []
    func didStartRangeSelecting(gesture: UILongPressGestureRecognizer) {
        let point = gesture.location(in: gesture.view!)
        rangeSelectedDates = calendarView.selectedDates
        print("RangeSelectedDates : ",rangeSelectedDates)
        if let cellState = calendarView.cellStatus(at: point) {
            let date = cellState.date
            if !calendarView.selectedDates.contains(date) {
                let dateRange = calendarView.generateDateRange(from: calendarView.selectedDates.first ?? date, to: date)
                for aDate in dateRange {
                    if !rangeSelectedDates.contains(aDate) {
                        rangeSelectedDates.append(aDate)
                    }
                }
                calendarView.selectDates(from: rangeSelectedDates.first!, to: date, keepSelectionIfMultiSelectionAllowed: true)
            } else {
                let indexOfNewlySelectedDate = rangeSelectedDates.index(of: date)! + 1
                let lastIndex = rangeSelectedDates.endIndex
                let followingDay = testCalendar.date(byAdding: .day, value: 1, to: date)!
                calendarView.selectDates(from: followingDay, to: rangeSelectedDates.last!, keepSelectionIfMultiSelectionAllowed: false)
                rangeSelectedDates.removeSubrange(indexOfNewlySelectedDate..<lastIndex)
            }
        }
        
        if gesture.state == .ended {
            rangeSelectedDates.removeAll()
        }
    }

    @IBAction func printSelectedDates() {
        print("\nSelected dates --->")
        for date in calendarView.selectedDates {
            print(formatter.string(from: date))
        }
    }
    
    @IBAction func resize(_ sender: UIButton) {
        calendarView.frame = CGRect(
            x: calendarView.frame.origin.x,
            y: calendarView.frame.origin.y,
            width: calendarView.frame.width,
            height: calendarView.frame.height - 50
        )
        calendarView.reloadData()
    }
    
    @IBAction func reloadCalendar(_ sender: UIButton) {
        calendarView.reloadData()
    }
    
    @IBAction func next(_ sender: UIButton) {
        self.calendarView.scrollToSegment(.next)
    }
    
    @IBAction func previous(_ sender: UIButton) {
        self.calendarView.scrollToSegment(.previous)
    }
    
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        guard let startDate = visibleDates.monthDates.first?.date else {
            return
        }
        let month = testCalendar.dateComponents([.month], from: startDate).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        // 0 indexed array
        let year = testCalendar.component(.year, from: startDate)
        monthLabel.text = monthName + " " + String(year)
        
        wrp_month = String(format: "%02d", month)
        wrp_year = "\(year)"
        
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForMyWeightProgramData), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
//        monthLabel.text = String(year)
//        monthNewLabel.text = monthName
    }
    
    func handleCellConfiguration(cell: JTAppleCell?, cellState: CellState) {
        handleCellSelection(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
        prePostVisibility?(cellState, cell as? CellView)
    }
    
    // Function to handle the text color of the calendar
    func handleCellTextColor(view: JTAppleCell?, cellState: CellState) {
        guard let myCustomCell = view as? CellView  else {
            return
        }
        
//        let dayInt = testCalendar.dateComponents([.weekday], from: cellState.date).weekday
        //if dayInt == 1 || dayInt == 7{
        //            myCustomCell.dayLabel.textColor = UIColor.init(red: 108.0/255.0, green: 110.0/255.0, blue: 112.0/255.0, alpha: 1.0)
        //        }
        
        let strDate = getStringDateFromDate(dateFormat: "yyyy-MM-dd HH:mm:ss Z", enterDate: cellState.date)
        let finalDate = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss Z", strDate: strDate, newFormat: "yyyy-MM-dd")
        
        let isCheck:Bool?
        let myIndexValue:Int = weightProgramDateData.index(of: "\(finalDate)")
        if self.weightProgramDateData.count > myIndexValue{
            isCheck = true
        }
        else{
            isCheck = false
        }

        
        
        if testCalendar.isDateInToday(cellState.date) {
            if cellState.isSelected {
                myCustomCell.dayLabel.textColor = white
            }
            else {
                if cellState.dateBelongsTo == .thisMonth {
                    myCustomCell.dayLabel.textColor = black
                } else {
                    myCustomCell.dayLabel.textColor = gray
                }
            }

        }
        else{
            if isCheck == true{
                if cellState.isSelected {
                    myCustomCell.dayLabel.textColor = black
                }
                else {
                    if cellState.dateBelongsTo == .thisMonth {
                        myCustomCell.dayLabel.textColor = black
                    } else {
                        myCustomCell.dayLabel.textColor = gray
                    }
                }
            }
            else{
                if cellState.isSelected {
                    myCustomCell.dayLabel.textColor = black
                }
                else {
                    if cellState.dateBelongsTo == .thisMonth {
                        myCustomCell.dayLabel.textColor = black
                    } else {
                        myCustomCell.dayLabel.textColor = gray
                    }
                }
            }
            
      
        }
        
        
    }
    
    // Function to handle the calendar selection
    func handleCellSelection(view: JTAppleCell?, cellState: CellState) {
        guard let myCustomCell = view as? CellView else {return }
        //        switch cellState.selectedPosition() {
        //        case .full:
        //            myCustomCell.backgroundColor = .green
        //        case .left:
        //            myCustomCell.backgroundColor = .yellow
        //        case .right:
        //            myCustomCell.backgroundColor = .red
        //        case .middle:
        //            myCustomCell.backgroundColor = .blue
        //        case .none:
        //            myCustomCell.backgroundColor = nil
        //        }
        //
        //dateComponents(_ components: Set<Calendar.Component>, from start: DateComponents, to end: DateComponents) -> DateComponents
        
//        testCalendar.dateComponents(.day, from: <#T##Date#>, to: <#T##Date#>)
        
      //  print("Calender Dates : ",cellState.date)
        let strDate = getStringDateFromDate(dateFormat: "yyyy-MM-dd HH:mm:ss Z", enterDate: cellState.date)
        let finalDate = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss Z", strDate: strDate, newFormat: "yyyy-MM-dd")

        let isCheck:Bool?
        let myIndexValue:Int = weightProgramDateData.index(of: "\(finalDate)")
        if self.weightProgramDateData.count > myIndexValue{
            isCheck = true
        }
        else{
            isCheck = false
        }
        
        print("isCheck",isCheck ?? "Wrong")
            
        if testCalendar.isDateInToday(cellState.date) {
            
            if cellState.isSelected {
                myCustomCell.selectedView.isHidden = false
                myCustomCell.selectedView.layer.cornerRadius =  myCustomCell.selectedView.frame.size.width / 2
                myCustomCell.selectedView.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
                
                myCustomCell.eventView.isHidden = true
                myCustomCell.eventView.layer.cornerRadius =  myCustomCell.eventView.frame.size.width / 2
                myCustomCell.eventView.backgroundColor = UIColor.init(red: 233.0/255.0, green: 166.0/255.0, blue: 119.0/255.0, alpha: 1.0)
            }
            else{
                myCustomCell.selectedView.isHidden = false
                myCustomCell.selectedView.layer.cornerRadius =  myCustomCell.selectedView.frame.size.width / 2
                myCustomCell.selectedView.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
                
                myCustomCell.eventView.isHidden = true
                myCustomCell.eventView.layer.cornerRadius =  myCustomCell.eventView.frame.size.width / 2
                myCustomCell.eventView.backgroundColor = UIColor.init(red: 233.0/255.0, green: 166.0/255.0, blue: 119.0/255.0, alpha: 1.0)
            }
            
        }
        else{
            
            if isCheck == true{
                if cellState.isSelected {
                    myCustomCell.selectedView.isHidden = false
                    myCustomCell.selectedView.layer.cornerRadius =  myCustomCell.selectedView.frame.size.width / 2
                    myCustomCell.selectedView.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
                    
                    myCustomCell.eventView.isHidden = false
                    myCustomCell.eventView.layer.cornerRadius =  myCustomCell.eventView.frame.size.width / 2
                    myCustomCell.eventView.backgroundColor = UIColor.init(red: 233.0/255.0, green: 166.0/255.0, blue: 119.0/255.0, alpha: 1.0)
                }
                else{
                    
                    print("testing :- \(dateData[myIndexValue])\n****************************************************************************************************")
                    
                    
                    myCustomCell.selectedView.isHidden = true
                    myCustomCell.selectedView.layer.cornerRadius =  myCustomCell.selectedView.frame.size.width / 2
                    myCustomCell.selectedView.backgroundColor = UIColor.init(red: 78.0/255.0, green: 78.0/255.0, blue: 78.0/255.0, alpha: 1.0)
                    
                    myCustomCell.eventView.isHidden = false
                    myCustomCell.eventView.layer.cornerRadius =  myCustomCell.eventView.frame.size.width / 2
                    //myCustomCell.eventView.backgroundColor = UIColor.init(red: 233.0/255.0, green: 166.0/255.0, blue: 119.0/255.0, alpha: 1.0)
                    
                    let dict = dateData[myIndexValue] as! NSDictionary
                    let color = dict.value(forKey: "dateColor") as! String
                    
                    if color == "0" {
                        // white
                        myCustomCell.eventView.backgroundColor = UIColor.init(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
                    } else if color == "1" {
                        // gray
                        myCustomCell.eventView.backgroundColor = UIColor.init(red: 221.0/255.0, green: 221.0/255.0, blue: 221.0/255.0, alpha: 1.0)
                    } else if color == "2" {
                        // red
                        myCustomCell.eventView.backgroundColor = UIColor.init(red: 234.0/255.0, green: 91.0/255.0, blue: 1.0/255.0, alpha: 1.0)
                    } else if color == "3" {
                        // green
                        myCustomCell.eventView.backgroundColor = UIColor.init(red: 148.0/255.0, green: 189.0/255.0, blue: 71.0/255.0, alpha: 1.0)
                    } else {
                        // white
                        myCustomCell.eventView.backgroundColor = UIColor.init(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
                    }
                    
                    
                }
            }
            else{
                if cellState.isSelected {
                    
                    myCustomCell.eventView.isHidden = true
                    myCustomCell.eventView.layer.cornerRadius =  myCustomCell.eventView.frame.size.width / 2
                    myCustomCell.eventView.backgroundColor = UIColor.init(red: 233.0/255.0, green: 166.0/255.0, blue: 119.0/255.0, alpha: 1.0)
                    
                    myCustomCell.selectedView.layer.cornerRadius =  myCustomCell.selectedView.frame.size.width / 2
                    myCustomCell.selectedView.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
                    myCustomCell.selectedView.isHidden = false
                    
                }
                else {
                    
                    myCustomCell.eventView.isHidden = true
                    myCustomCell.eventView.layer.cornerRadius =  myCustomCell.eventView.frame.size.width / 2
                    myCustomCell.eventView.backgroundColor = UIColor.init(red: 233.0/255.0, green: 166.0/255.0, blue: 119.0/255.0, alpha: 1.0)

                    myCustomCell.selectedView.backgroundColor = UIColor.init(red: 78.0/255.0, green: 78.0/255.0, blue: 78.0/255.0, alpha: 1.0)
                    myCustomCell.selectedView.layer.cornerRadius =  myCustomCell.selectedView.frame.size.width / 2
                    myCustomCell.selectedView.isHidden = true
                }
            }
        }
        
        
        
//        if cellState.isSelected {
//            myCustomCell.selectedView.layer.cornerRadius =  myCustomCell.selectedView.frame.size.width / 2
//            myCustomCell.selectedView.isHidden = false
//        }
//        else if testCalendar.isDateInToday(cellState.date) {
//            myCustomCell.selectedView.isHidden = false
//            myCustomCell.selectedView.layer.cornerRadius =  myCustomCell.selectedView.frame.size.width / 2
//            myCustomCell.selectedView.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
//        } else {
//            myCustomCell.selectedView.isHidden = true
//        }
        
    }
    
    
    @IBAction func decreaseSectionInset(_ sender: UIButton) {
        
        calendarView.sectionInset.bottom -= 3
        calendarView.sectionInset.top -= 3
        calendarView.sectionInset.left -= 3
        calendarView.sectionInset.right -= 3
        
        calendarView.reloadData()
    }
    
    @IBAction func increaseSectionInset(_ sender: UIButton) {
        calendarView.sectionInset.bottom += 3
        calendarView.sectionInset.top += 3
        calendarView.sectionInset.left += 3
        calendarView.sectionInset.right += 3
        calendarView.reloadData()
    }

    
}

// MARK : JTAppleCalendarDelegate
extension WeightReductForm5VC: JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource {
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        
    }
    
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = testCalendar.timeZone
        formatter.locale = testCalendar.locale
        
        let dateFormater1 = DateFormatter()
        dateFormater1.dateFormat = "yyyy-MM-dd"
        let date =  formatter.string(from: Date())

        //"2017 01 01"
        //self.selectedPassDate
        
//        if let selectDate = self.selectedPassDate{
//            if selectDate == ""{
//                date =  formatter.string(from: Date())
//            }
//            else{
//                date =  formatter.string(from: dateFormater1.date(from: self.selectedPassDate)!)
//            }
//        }
//        else{
//            date =  formatter.string(from: Date())
//        }
//        
        
        
        let startDate = formatter.date(from:date)!
        let endDate = formatter.date(from: "2030 02 01")!
        
        let parameters = ConfigurationParameters(startDate: startDate,
                                                 endDate: endDate,
                                                 numberOfRows: numberOfRows,
                                                 calendar: testCalendar,
                                                 generateInDates: generateInDates,
                                                 generateOutDates: generateOutDates,
                                                 firstDayOfWeek: firstDayOfWeek,
                                                 hasStrictBoundaries: hasStrictBoundaries)
        return parameters
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let myCustomCell = calendar.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CellView
        
//        print("Cell Value : ",cellState.text)
//        print("Cell Date : ",cellState.date)
//        
//        let strDate = getStringDateFromDate(dateFormat: "yyyy-MM-dd HH:mm:ss Z", enterDate: cellState.date)
//        let finalDate = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss Z", strDate: strDate, newFormat: "yyyy-MM-dd HH:mm:ss")
//        
//        if ((myWeightProgramData.object(at: indexPath.row) as AnyObject).value(forKey: "start_date") as! String) == finalDate{
//            print("Matched Date : ",finalDate)
////            calendarView.selectDates(from: oldDate!, to: newDate!, keepSelectionIfMultiSelectionAllowed: false)
//        }
//        else{
//            
//        }
        
        // print("IndexPath :- \(indexPath.row)    Date :- \(date) ")
        
        myCustomCell.dayLabel.text = cellState.text
        
        if testCalendar.isDateInToday(date) {
//            myCustomCell.backgroundColor = red
        } else {
            myCustomCell.backgroundColor = white
        }
        
        
        
        handleCellConfiguration(cell: myCustomCell, cellState: cellState)
        
        
        return myCustomCell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        handleCellConfiguration(cell: cell, cellState: cellState)
        
//        let strDate = getStringDateFromDate(dateFormat: "yyyy-MM-dd HH:mm:ss Z", enterDate: cellState.date)
//        let finalDate = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss Z", strDate: strDate, newFormat: "yyyy-MM-dd HH:mm:ss")
        
        let strDate = getStringDateFromDate(dateFormat: "yyyy-MM-dd HH:mm:ss Z", enterDate: cellState.date)
        let finalDate = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss Z", strDate: strDate, newFormat: "yyyy-MM-dd")
        
        
        let myIndexValue:Int = weightProgramDateData.index(of: "\(finalDate)")
        if self.weightProgramDateData.count > myIndexValue{
            self.weightProgramID = (dateData.object(at: myIndexValue) as AnyObject).value(forKey: "wp_id") as! String
            self.selectedPassDate = (dateData.object(at: myIndexValue) as AnyObject).value(forKey: "selectedDate") as! String
            self.selectedCalory = (dateData.object(at: myIndexValue) as AnyObject).value(forKey: "selectedCalory") as! String
        }
        else{
//            self.weightProgramID = ""
//            self.selectedPassDate = ""
//            self.selectedCalory = ""
        }
        
       // self.singleSelectedDate(singleDate: strDate)

        print("Weight Program ID : ",self.weightProgramID)
        print("SelectedDate : ",self.selectedPassDate)
        
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        handleCellConfiguration(cell: cell, cellState: cellState)
        
//        let dayInt = testCalendar.dateComponents([.weekday], from: date).weekday
//        print("Day :",dayInt!)
        
        print("Cell : ",cell ?? "Error")
        print("Cell State: ",cellState)
        print("Cell date: ",date)
        
      
        let strDate = getStringDateFromDate(dateFormat: "yyyy-MM-dd HH:mm:ss Z", enterDate: cellState.date)
        let finalDate = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss Z", strDate: strDate, newFormat: "yyyy-MM-dd")

        
        let myIndexValue:Int = weightProgramDateData.index(of: "\(finalDate)")
        if self.weightProgramDateData.count > myIndexValue{
            self.weightProgramID = (dateData.object(at: myIndexValue) as AnyObject).value(forKey: "wp_id") as! String
            self.selectedPassDate = (dateData.object(at: myIndexValue) as AnyObject).value(forKey: "selectedDate") as! String
            self.selectedCalory = (dateData.object(at: myIndexValue) as AnyObject).value(forKey: "selectedCalory") as! String

        }
        else{
//            self.weightProgramID = ""
//            self.selectedPassDate = ""
//            self.selectedCalory = ""

        }
        
//        self.singleSelectedDate(singleDate: cellState.date)
        
        print("Weight Program ID : ",self.weightProgramID)
        print("SelectedDate : ",self.selectedPassDate)

    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        self.setupViewsOfCalendar(from: visibleDates)
    }
    
    func scrollDidEndDecelerating(for calendar: JTAppleCalendarView) {
        let visibleDates = calendarView.visibleDates()
        //        let dateWeShouldNotCross = formatter.date(from: "2017 08 07")!
        //        let dateToScrollBackTo = formatter.date(from: "2017 07 03")!
        //        if visibleDates.monthDates.contains (where: {$0.date >= dateWeShouldNotCross}) {
        //            calendarView.scrollToDate(dateToScrollBackTo)
        //            return
        //        }
        self.setupViewsOfCalendar(from: visibleDates)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, headerViewForDateRange range: (start: Date, end: Date), at indexPath: IndexPath) -> JTAppleCollectionReusableView {
        let date = range.start
        let month = testCalendar.component(.month, from: date)
        
        let header: JTAppleCollectionReusableView
        if month % 2 > 0 {
            header = calendar.dequeueReusableJTAppleSupplementaryView(withReuseIdentifier: "WhiteSectionHeaderView", for: indexPath)
            (header as! WhiteSectionHeaderView).title.text = formatter.string(from: date)
        } else {
            header = calendar.dequeueReusableJTAppleSupplementaryView(withReuseIdentifier: "PinkSectionHeaderView", for: indexPath)
            (header as! PinkSectionHeaderView).title.text = formatter.string(from: date)
        }
        return header
    }
    
    func sizeOfDecorationView(indexPath: IndexPath) -> CGRect {
        let stride = calendarView.frame.width * CGFloat(indexPath.section)
        return CGRect(x: stride + 5, y: 5, width: calendarView.frame.width - 10, height: calendarView.frame.height - 10)
    }
    
    func calendarSizeForMonths(_ calendar: JTAppleCalendarView?) -> MonthSize? {
        return monthSize
    }
}
