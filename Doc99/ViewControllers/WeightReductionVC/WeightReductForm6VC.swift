//
//  WeightReductForm6VC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 02/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class WeightReductForm6VC: UIViewController {
    
    // FIXME: - VIEW CONTROLLER METHODS
    @IBOutlet var viewMemberShip:UIView!
    @IBOutlet var btnSelect1:UIButton!
    @IBOutlet var btnSelect2:UIButton!
    
    @IBOutlet var imageViewSelect1:UIImageView!
    @IBOutlet var imageViewSelect2:UIImageView!
    
    @IBOutlet var lblSelect1:UILabel!
    @IBOutlet var lblSelect2:UILabel!
    
    @IBOutlet var lblHeader:UILabel!
    @IBOutlet var lblGo:UILabel!
    @IBOutlet var lblTitle1:UILabel!
    @IBOutlet var lblTitle2:UILabel!

    @IBOutlet var btnCancel:UIButton!
    @IBOutlet var btnSubmit:UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        self.setLocalizationText()

    }
    
    func setLocalizationText(){
        lblHeader.text = NSLocalizedString("Weight Reduction Program", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblGo.text = NSLocalizedString("GO", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitle1.text = NSLocalizedString("Please call our specialist to order a recommended food for your fasting", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitle2.text = NSLocalizedString("Record your calorie intake", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
        lblSelect1.text = NSLocalizedString("Would you like to go for one time ordering charge for specialist advice?", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblSelect2.text = NSLocalizedString("Take membership plan to get rid of multiple payments", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

        btnCancel.setTitle(NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        btnSubmit.setTitle(NSLocalizedString("SUBMIT", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)

    }

    
    func setSelectedButton(radioButton:UIButton) {
        let image = UIImage.init(named: "health_check.png")
        let image1 = UIImage.init(named: "health_Uncheck.png")
        
        if radioButton == btnSelect1{
            imageViewSelect1.image = image
            imageViewSelect2.image = image1
            
            print("Selected : ",lblSelect1.text ?? "Nothing")
            APPDELEGATE.weightReductionData.setValue(lblSelect1.text, forKey: "orderSelection")
        }
        else if radioButton == btnSelect2{
            imageViewSelect2.image = image
            imageViewSelect1.image = image1
            
            print("Selected : ",lblSelect2.text ?? "Nothing")
            APPDELEGATE.weightReductionData.setValue(lblSelect2.text, forKey: "orderSelection")
        }
        
        
    }
    
    

    
    // TODO: - DELEGATE METHODS
    // TODO: - ACTION METHODS
    @IBAction func btnGoClicked(_ sender: UIButton) {
        viewMemberShip.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        
        self.view.addSubview(viewMemberShip)
    }
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        
        /* //Commented by hitesh..
        if APPDELEGATE.weightReductionData.value(forKey: "orderSelection") == nil{
            showAlert(Appname, title: "Please select membership method")
        }
        else{
//            let joinVC = JoinMemberVC(nibName: "JoinMemberVC", bundle: nil)
//            joinVC.isFromOther = true
//            self.navigationController?.pushViewController(joinVC, animated: true)
            
            
            viewMemberShip.removeFromSuperview()
            let recordCalorieVC = RecordCalorieVC(nibName: "RecordCalorieVC", bundle: nil)
            self.navigationController?.pushViewController(recordCalorieVC, animated: true)
        }
        */
    }
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        viewMemberShip.removeFromSuperview()
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        APPDELEGATE.weightReductionData.setValue(nil, forKey: "orderSelection")

        _  = self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func btnRadioSelectionClicked(_ sender: UIButton) {
        
        if sender == btnSelect1{
            self.setSelectedButton(radioButton: btnSelect1)
        }
        else if sender == btnSelect2{
            self.setSelectedButton(radioButton: btnSelect2)
        }
    }
    
    
    // TODO: - POST DATA METHODS
}
