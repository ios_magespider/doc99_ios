//
//  RecordCalorieVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 02/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import KAProgressLabel
import SVProgressHUD

class RecordCalorieVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var tableViewCalori:UITableView!
    @IBOutlet var tableViewCalender:UITableView!
    
    @IBOutlet var lblUnderCalori:UILabel!
    @IBOutlet var lblUnderCalender:UILabel!
    
    @IBOutlet var btnCalori:UIButton!
    @IBOutlet var btnCalender:UIButton!
    
    @IBOutlet var imageViewCalory:UIImageView!
    @IBOutlet var imageViewCalender:UIImageView!

    
    @IBOutlet var viewCalori:UIView!
    @IBOutlet var viewCalender:UIView!
    
    @IBOutlet var scrView:UIScrollView!
    //FOR SCROLLING VIEW
    var currentPag:Int!
    var pageCount:Int!
    
    var caloriData = NSMutableArray()
    var caloriGlobalData = NSMutableArray()
    var sectionData = NSArray()
    
    var breakFastData = NSMutableArray()
    var lunchDataData = NSMutableArray()
    var dinnerData = NSMutableArray()
    var otherData = NSMutableArray()
    
    
    var weightReportData = NSMutableArray()
    var weightReportGlobalData = NSMutableArray()
    
    
    //For Pasignation
    var pageNum:Int!
    var isLoading:Bool?
    
    var pageNum1:Int!
    var isLoading1:Bool?
    
    
    var favouriteStoreID:String!
    var favouriteDoctorID:String!
    
    var index:Int!
    
    @IBOutlet var lblNoCalori:UILabel!
    @IBOutlet var lblNoCalender:UILabel!
    
    @IBOutlet var viewUnderScorllView:UIView!
    
    let screenWidth = Int(ScreenSize.SCREEN_WIDTH)
    
    
    @IBOutlet var datePicker:UIDatePicker!
    @IBOutlet var viewDatePicker:UIView!
    @IBOutlet var txtDate:UITextField!
    
    
    @IBOutlet weak var labelCurrentDate: UILabel!
    @IBOutlet var lblHeader:UILabel!
    @IBOutlet var lblCaloryRemaining:UILabel!
    @IBOutlet var lblCaloryConsumed:UILabel!

    @IBOutlet var btnCancel:UIButton!
    @IBOutlet var btnDone:UIButton!

    @IBOutlet var lblCalenderTitle1:UILabel!
    @IBOutlet var lblCalenderTitle2:UILabel!

    @IBOutlet var pLabel:KAProgressLabel!

    @IBOutlet var viewReview:UIView!
    @IBOutlet var textviewReview:UITextView!

    @IBOutlet var lblCalenderBorder:UILabel!
    
    @IBOutlet var viewForCaleryHeader:UIView!
    @IBOutlet var lblHeaderName:UILabel!

    @IBOutlet var btnReviewCancel:UIButton!
    @IBOutlet var btnReviewSubmit:UIButton!
    @IBOutlet var lblReviewTitle1:UILabel!
    @IBOutlet var lblReviewTitle2:UILabel!

    var weightProgramID:String!
    var wPID:String!

    var dateValue:String!
    
    @IBOutlet var lblRemaining:UILabel!
    @IBOutlet var lblConsumed:UILabel!

    var totalBreakFast:String!
    var totalLunch:String!
    var totalDinner:String!
    var totalOther:String!
    
    var selectedPassDate:String!
    var selectedCalory:String!
    var progressBarValue:CGFloat!


    // FIXME: - VIEW CONTROLLER METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        labelCurrentDate.text = selectedPassDate
        print("SelectedDate : ",selectedPassDate)
        print("SelectedCalory : ",selectedCalory)
        print("weightProgramID : ",weightProgramID)

        sectionData = ["Breakfast","Lunch","Dinner","Other"]
        
//        if let foodData = USERDEFAULT.value(forKey: "selectedFoodData") as? NSDictionary
//        {
//            print("Selected Food Data : ", foodData)
//            
//            breakFastData = NSMutableArray(array: foodData.value(forKey: "breakfastarray") as! NSArray)
//            lunchDataData = NSMutableArray(array: foodData.value(forKey: "luncharray") as! NSArray)
//            dinnerData = NSMutableArray(array: foodData.value(forKey: "dinnerarray") as! NSArray)
//            otherData = NSMutableArray(array: foodData.value(forKey: "otherarray") as! NSArray)
//            
//            caloriData = NSMutableArray()
//            caloriData.add(breakFastData)
//            caloriData.add(lunchDataData)
//            caloriData.add(dinnerData)
//            caloriData.add(otherData)
//            
//            print("Calories Data : ", caloriData)
//            
//            if let consumed = foodData.value(forKey: "consumed") as? String{
//                lblConsumed.text = "\(consumed)"
//            }
//            else if let consumed = foodData.value(forKey: "consumed") as? NSNumber{
//                lblConsumed.text = "\(consumed)"
//            }
//            else{
//                lblConsumed.text = "00"
//            }
//            
//            if let remain = foodData.value(forKey: "remain") as? String{
//                lblRemaining.text = "\(remain)"
//            }
//            else  if let remain = foodData.value(forKey: "remain") as? NSNumber{
//                lblRemaining.text = "\(remain)"
//            }
//            else{
//                lblRemaining.text = "00"
//            }
//            
//            if let breakfast = foodData.value(forKey: "breakfast") as? String{
//                totalBreakFast = "\(breakfast)"
//            }
//            else  if let breakfast = foodData.value(forKey: "breakfast") as? NSNumber{
//                totalBreakFast = "\(breakfast)"
//            }
//            else{
//                totalBreakFast = "00"
//            }
//            
//            if let lunch = foodData.value(forKey: "lunch") as? String{
//                totalLunch = "\(lunch)"
//            }
//            else  if let lunch = foodData.value(forKey: "lunch") as? NSNumber{
//                totalLunch = "\(lunch)"
//            }
//            else{
//                totalLunch = "00"
//            }
//            
//            
//            if let dinner = foodData.value(forKey: "dinner") as? String{
//                totalDinner = "\(dinner)"
//            }
//            else  if let dinner = foodData.value(forKey: "dinner") as? NSNumber{
//                totalDinner = "\(dinner)"
//            }
//            else{
//                totalDinner = "00"
//            }
//
//            
//            if let other = foodData.value(forKey: "other") as? String{
//                totalOther = "\(other)"
//            }
//            else  if let other = foodData.value(forKey: "other") as? NSNumber{
//                totalOther = "\(other)"
//            }
//            else{
//                totalOther = "00"
//            }
//            
//            
//            //            print("selected Food Data : ",foodData)
//            //            caloriData = NSMutableArray(array: foodData)
//            tableViewCalori.reloadData()
//        }

     
       // tableViewCalori.reloadData()

        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
        
        viewDatePicker.frame = CGRect(x: self.view.frame.size.width - self.viewDatePicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: viewDatePicker.frame.size.width, height: viewDatePicker.frame.size.height)

        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        
        labelCurrentDate.text = getStringDateFromDate(dateFormat: "dd MMMM yyy", enterDate: Date())
        
        txtDate.text = getStringDateFromDate(dateFormat: "MMMM yyyy", enterDate: Date())
        dateValue  = getStringDateFromDate(dateFormat: "MM yyyy", enterDate: Date())
        
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForMyWeightProgramReportData), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForSingleDayProgram), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }

        

        
        lblCalenderBorder.layer.borderWidth = 1.0
        lblCalenderBorder.layer.borderColor = UIColor.init(red: 233.0/255.0, green: 233.0/255.0, blue: 233.0/255.0, alpha: 1.0).cgColor
        
        progressBarValue = 0.0
        self.setProgressBar()
        
        // Define identifier
        let notificationName3 = Notification.Name("refreshFoodData")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(refreshFoodData(notification:)), name: notificationName3, object: nil)

        // Define identifier
        let notificationName4 = Notification.Name("refreshReportData")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(refreshReportData(notification:)), name: notificationName4, object: nil)
        
        
        
        pageCount=2;
        currentPag=0;
        
        scrView.contentSize = CGSize(width: CGFloat(screenWidth * pageCount), height: scrView.frame.size.height)
        
        viewCalori.frame = CGRect(x: 0.0, y: viewCalori.frame.origin.y, width: viewCalori.frame.size.width, height: viewCalori.frame.size.height)
        scrView.addSubview(viewCalori)
        
        
        viewCalender.frame = CGRect(x: viewCalori.frame.origin.x+viewCalori.frame.size.width, y: viewCalender.frame.origin.y, width: viewCalender.frame.size.width, height: viewCalender.frame.size.height)
        scrView.addSubview(viewCalender)
        
        self.settingBtnBgColor(btnCalori)
        
//        pageNum=1;
//        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
//        indicator1.startAnimating()
//        
//        self.caloriData = NSMutableArray()
//        self.caloriGlobalData = NSMutableArray()
//        
//        if Reachability.isConnectedToNetwork() == true {
//            self.addLoadingIndicatiorOnFooterOnTableViewStore()
//            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetFavourtiteStore), userInfo: nil, repeats: false)
//        }else{
//            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
//        }
//        
//        
//        
//        
//        pageNum1=1;
//        let indicator2 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
//        indicator2.startAnimating()
//        
//        self.calenderData = NSMutableArray()
//        self.calenderGlobalData = NSMutableArray()
//        
//        
//        if Reachability.isConnectedToNetwork() == true {
//            self.addLoadingIndicatiorOnFooterOnTableViewDoctor()
//            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetFavourtiteDoctor), userInfo: nil, repeats: false)
//        } else {
//            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
//        }
        
        
      //  self.setDiviceSupport()
        
        self.addTapGestureInOurView()
        self.setLocalizationText()
    }
    
    func refreshFoodData(notification: NSNotification) {
        
//        if let foodData = USERDEFAULT.value(forKey: "selectedFoodData") as? NSDictionary
//        {
//            print("Selected Food Data : ", foodData)
//
//            breakFastData = NSMutableArray(array: foodData.value(forKey: "breakfastarray") as! NSArray)
//            lunchDataData = NSMutableArray(array: foodData.value(forKey: "luncharray") as! NSArray)
//            dinnerData = NSMutableArray(array: foodData.value(forKey: "dinnerarray") as! NSArray)
//            otherData = NSMutableArray(array: foodData.value(forKey: "otherarray") as! NSArray)
//
//            caloriData = NSMutableArray()
//            caloriData.add(breakFastData)
//            caloriData.add(lunchDataData)
//            caloriData.add(dinnerData)
//            caloriData.add(otherData)
//            
//            print("Calories Data : ", caloriData)
//           
//            if let consumed = foodData.value(forKey: "consumed") as? String{
//                lblConsumed.text = "\(consumed)"
//            }
//            else if let consumed = foodData.value(forKey: "consumed") as? NSNumber{
//                lblConsumed.text = "\(consumed)"
//            }
//            else{
//                lblConsumed.text = "00"
//            }
//            
//            if let remain = foodData.value(forKey: "remain") as? String{
//                lblRemaining.text = "\(remain)"
//            }
//            else if let remain = foodData.value(forKey: "remain") as? NSNumber{
//                lblRemaining.text = "\(remain)"
//            }
//            else{
//                lblRemaining.text = "00"
//            }
//            
//            if let breakfast = foodData.value(forKey: "breakfast") as? String{
//                totalBreakFast = "\(breakfast)"
//            }
//            else  if let breakfast = foodData.value(forKey: "breakfast") as? NSNumber{
//                totalBreakFast = "\(breakfast)"
//            }
//            else{
//                totalBreakFast = "00"
//            }
//            
//            if let lunch = foodData.value(forKey: "lunch") as? String{
//                totalLunch = "\(lunch)"
//            }
//            else  if let lunch = foodData.value(forKey: "lunch") as? NSNumber{
//                totalLunch = "\(lunch)"
//            }
//            else{
//                totalLunch = "00"
//            }
//            
//            
//            if let dinner = foodData.value(forKey: "dinner") as? String{
//                totalDinner = "\(dinner)"
//            }
//            else  if let dinner = foodData.value(forKey: "dinner") as? NSNumber{
//                totalDinner = "\(dinner)"
//            }
//            else{
//                totalDinner = "00"
//            }
//            
//            
//            if let other = foodData.value(forKey: "other") as? String{
//                totalOther = "\(other)"
//            }
//            else  if let other = foodData.value(forKey: "other") as? NSNumber{
//                totalOther = "\(other)"
//            }
//            else{
//                totalOther = "00"
//            }
//            
//
////            print("selected Food Data : ",foodData)
////            caloriData = NSMutableArray(array: foodData)
//            tableViewCalori.reloadData()
//        }
        
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForSingleDayProgram), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        
    }
    
    func refreshReportData(notification: NSNotification) {
        
        txtDate.text = getStringDateFromDate(dateFormat: "MMMM yyyy", enterDate: Date())
        dateValue  = getStringDateFromDate(dateFormat: "MM yyyy", enterDate: Date())
        
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForMyWeightProgramReportData), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
    }
    
    
    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        if viewTouched!.isKind(of: UIButton.self){
            
        }
        else{
            self.view.endEditing(true)
        }
        
        if (viewTouched!.superview!.superview == viewDatePicker) {
        }else if(viewTouched!.superview == viewDatePicker){
        }else if(viewTouched == viewDatePicker){
        }else{
            UIView.animate(withDuration: 0.3) {
                
                self.viewDatePicker.frame=CGRect(x: self.view.frame.size.width - self.viewDatePicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewDatePicker.frame.size.width, height: self.viewDatePicker.frame.size.height)
                
            }
        }

        
        
    }
    
    
    func settingBtnBgColor(_ sender:UIButton){
        
        if sender == btnCalori{
            
            btnCalori.setTitleColor(UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0), for: .normal)
            btnCalender.setTitleColor(UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0), for: .normal)
            
            imageViewCalory.image = UIImage.init(named: "weight_Calory_Select.png")
            imageViewCalender.image = UIImage.init(named: "weight_Calendar.png")
            
            lblUnderCalender.isHidden = true
            lblUnderCalori.isHidden = false
            
        }
        else if sender == btnCalender{
            
            btnCalender.setTitleColor(UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0), for: .normal)
            btnCalori.setTitleColor(UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0), for: .normal)
            
            imageViewCalory.image = UIImage.init(named: "weight_Calory.png")
            imageViewCalender.image = UIImage.init(named: "weight_Calendar_Select.png")
            
            lblUnderCalender.isHidden = false
            lblUnderCalori.isHidden = true
            
        }
        
    }
   
    func setupScrollViewWithCurrentPage(currentPage:Int){
        scrView.contentOffset = CGPoint(x: currentPag * screenWidth, y: 0)
    }
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
//    func setDiviceSupport()
//    {
//        if DeviceType.IS_IPHONE_4_OR_LESS
//        {
//            
//            viewCalori.frame = CGRect(x: viewStore.frame.origin.x, y: viewStore.frame.origin.y, width: viewStore.frame.size.width, height: viewStore.frame.size.height - 88.0)
//            
//            viewCalender.frame = CGRect(x: viewDoctor.frame.origin.x, y: viewDoctor.frame.origin.y, width: viewDoctor.frame.size.width, height: viewDoctor.frame.size.height - 88.0)
//            
//            
//            tableViewStore.frame = CGRect(x: tableViewStore.frame.origin.x, y: tableViewStore.frame.origin.y, width: tableViewStore.frame.size.width, height: tableViewStore.frame.size.height - 88.0)
//            
//            tableViewDoctor.frame = CGRect(x: tableViewDoctor.frame.origin.x, y: tableViewDoctor.frame.origin.y, width: tableViewDoctor.frame.size.width, height: tableViewDoctor.frame.size.height - 88.0)
//            
//            scrView.frame = CGRect(x: scrView.frame.origin.x, y: scrView.frame.origin.y, width: scrView.frame.size.width, height: scrView.frame.size.height - 88.0)
//            
//            viewUnderScorllView.frame = CGRect(x: viewUnderScorllView.frame.origin.x, y: viewUnderScorllView.frame.origin.y, width: viewUnderScorllView.frame.size.width, height: viewUnderScorllView.frame.size.height - 88.0)
//            
//            
//        }
//    }
    
    func setLocalizationText(){
        
        lblHeader.text = NSLocalizedString("Weight Reduction Program", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblCaloryRemaining.text = NSLocalizedString("Calories Remaining", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblCaloryConsumed.text = NSLocalizedString("Calories Consumed", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblCalenderTitle1.text = NSLocalizedString("Great!!  You have completed 2 days diet", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblCalenderTitle2.text = NSLocalizedString("3 days remain to complete diet", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

        
        btnCancel.setTitle(NSLocalizedString("Cancel", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        btnDone.setTitle(NSLocalizedString("Done", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        btnCalori.setTitle(NSLocalizedString("Calorie", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        btnCalender.setTitle(NSLocalizedString("Calendar", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        
        btnReviewCancel.setTitle(NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        btnReviewSubmit.setTitle(NSLocalizedString("SUBMIT", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)

        lblReviewTitle1.text = NSLocalizedString("Share your experience with weight reduction program & how we can improve it", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblReviewTitle2.text = NSLocalizedString("Your review", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

    }
    
    func setProgressBar(){
        
        self.pLabel.backgroundColor = UIColor.clear
        
        self.pLabel.startDegree = 1.0
        self.pLabel.endDegree = 100.0
        
        if progressBarValue >= 1.0{
            self.pLabel.progress = 1.0
        }
        else{
            self.pLabel.progress = progressBarValue
        }
        
        
        //pLabel.setProgress(0.5, timing: TPPropertyAnimationTimingEaseInEaseOut, duration: 1, delay: 0)

        
        self.pLabel.trackWidth = 5.0
        self.pLabel.progressWidth = 5.0
        self.pLabel.roundedCornersWidth = 0.0
        
        self.pLabel.trackColor = UIColor.init(red: 148.0 / 255.0, green: 190.0 / 255.0, blue: 71.0 / 255.0, alpha: 0.5)
        self.pLabel.progressColor = UIColor.init(red: 148.0 / 255.0, green: 190.0 / 255.0, blue: 71.0 / 255.0, alpha: 1.0)
        self.pLabel.isStartDegreeUserInteractive = false
        self.pLabel.isEndDegreeUserInteractive = false
        //pLabel.progress = 50.0/100.0
        print("Progress :",self.pLabel.progress)
        
        let progress = String.init(format: "%.0f%", self.pLabel.progress * 100 )
        self.pLabel.text = "\(progress)%"
        
        self.pLabel.textColor = UIColor.black

//        let weakSelf: RecordCalorieVC? = self
//        self.pLabel.labelVCBlock = {(_ label: KAProgressLabel) -> Void in
//            print("Progress : ",label.progress)
//            weakSelf?.pLabel.text = String.init(format: "%.0f", label.progress * 100 + 1)
//            } as! labelValueChangedCompletion

    }
    
    func setSelectedWeightProgramFromReport(dateValue:String,caloryValue:String,weightID:String){
        currentPag=0;
        self.setupScrollViewWithCurrentPage(currentPage: currentPag)
        self.settingBtnBgColor(btnCalori)

        
        selectedPassDate = dateValue
        selectedCalory = caloryValue
        weightProgramID = weightID
        print("Date  :  Calory",selectedPassDate,selectedCalory)
        
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForSingleDayProgram), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }

    }
    
    
    
    // TODO: - Textfield DELEGATE METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        return false
    }
    
    //MARK: - ACTION SHEET DELEGATE
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
        }
    }
    
    // TODO: - DELEGATE METHODS
    
    //ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.tag == 50) {
            currentPag = Int(scrollView.contentOffset.x) / Int(scrollView.frame.size.width);
            
            if (currentPag == 0) {
                self.settingBtnBgColor(btnCalori)
            }
            else if (currentPag == 1){
                self.settingBtnBgColor(btnCalender)
                let btn = UIButton.init()
                btn.tag = 101
                
                self.btnHeaderClicked(btn)
                
            }
            
            scrView.contentOffset = CGPoint(x: scrView.contentOffset.x, y: 0.0)
        }
        
    }
    
    
    //TableView Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView.tag == 100 {
            return sectionData.count
        }else if tableView.tag == 101{
            return 1
        }
        return 0
    }
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        if tableView.tag == 100 {
//            return sectionData[section] as? String
//        }
//        return nil
//    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let vw = UIView()
//        vw.backgroundColor = UIColor.red
//        return vw
//        lblHeaderName.tag = section
//        lblHeaderName.text = sectionData[section] as? String
        
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        customView.backgroundColor = UIColor.white
        
        let label = UILabel(frame: CGRect(x: 47, y: 13, width: 200, height: 24))
        label.tag = section
        label.text = sectionData[section] as? String
        customView.addSubview(label)
        
        let labelTotal = UILabel(frame: CGRect(x: 250, y: 13, width: 55, height: 24))
        labelTotal.textColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        labelTotal.textAlignment = .right
        if section == 0{
            labelTotal.text = totalBreakFast
        }
        else if section == 1{
            labelTotal.text = totalLunch
        }
        else if section == 2{
            labelTotal.text = totalDinner
        }
        else if section == 3{
            labelTotal.text = totalOther
        }
        
        customView.addSubview(labelTotal)

        
        
        
        let imageview = UIImageView(frame: CGRect(x: 15, y: 13, width: 24, height: 24))
        imageview.image = UIImage.init(named: "Calorie_add.png")
        customView.addSubview(imageview)
        
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        button.tag = section
        button.addTarget(self, action: #selector(self.btnHeaderSectionClicked(_:)), for: .touchUpInside)
        customView.addSubview(button)

        let label1 = UILabel(frame: CGRect(x: 0, y: 48, width: 320, height: 2))
        label1.textColor = UIColor.clear
        label1.backgroundColor = UIColor.init(red: 181.0/255.0, green: 182.0/255.0, blue: 183.0/255.0, alpha: 1.0)
        customView.addSubview(label1)
        
        return customView
        
//        viewForCaleryHeader = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
//        viewForCaleryHeader.backgroundColor = UIColor.red
//        lblCalenderBorder = UILabel(frame: CGRect(x: 47, y: 14, width: 257, height: 22))
//        lblHeaderName.backgroundColor = UIColor.black
//        lblHeaderName.tag = section
//        lblHeaderName.text = sectionData[section] as? String
//        viewForCaleryHeader.addSubview(lblHeaderName)
//        return viewForCaleryHeader

    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 100 {
//            return self.caloriData.count
            if self.caloriData.count == 0{
                return 0
            }
            return (self.caloriData[section] as AnyObject).count
            
        }else if tableView.tag == 101{
            return self.weightReportData.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 100{
            let identifier = "caloryCell"
            
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? CaloriTableVC
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("CaloriTableVC", owner: self, options: nil)
                cell = nib?[0] as? CaloriTableVC
            }
            cell!.selectionStyle = .none;
            
            if let food_name =  ((self.caloriData[indexPath.section] as AnyObject).objectAt(indexPath.row) as! NSDictionary).value(forKey: "food_name") as? String
            {
                cell?.lblFoodName.text = "\(food_name)"
            }
            
            if let kcal = ((self.caloriData[indexPath.section] as AnyObject).objectAt(indexPath.row) as! NSDictionary).value(forKey: "kcal") as? String
            {
                cell?.lblCalery.text = "\(kcal) kcal"
            }
            
            if let qty = ((self.caloriData[indexPath.section] as AnyObject).objectAt(indexPath.row) as! NSDictionary).value(forKey: "qty") as? String
            {
                cell?.lblOtherDetail.text = "\(qty)"
            }
            
            
            
            
            return cell!
        }
        else {
            let identifier = "weightCalenderVC"
            
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? CalenderTableVC
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("CalenderTableVC", owner: self, options: nil)
                cell = nib?[0] as? CalenderTableVC
            }
            
            
            cell!.selectionStyle = .none;
            
            
            if let totalkcal =  (weightReportData.object(at: indexPath.row) as AnyObject).value(forKey: "totalkcal") as? String
            {
                cell?.lblObtainCalory.text = "\(totalkcal) kcal"
            }
            else if let totalkcal =  (weightReportData.object(at: indexPath.row) as AnyObject).value(forKey: "totalkcal") as? NSNumber
            {
                cell?.lblObtainCalory.text = "\(totalkcal) kcal"
            }
            
            if let obtainkcal =  (weightReportData.object(at: indexPath.row) as AnyObject).value(forKey: "obtainkcal") as? String
            {
                
                cell?.lblTotalCalory.text = "\(obtainkcal) kcal"

            }
            else if let obtainkcal =  (weightReportData.object(at: indexPath.row) as AnyObject).value(forKey: "obtainkcal") as? NSNumber
            {
                
                cell?.lblTotalCalory.text = "\(obtainkcal) kcal"

            }

            if let wpm_date =  (weightReportData.object(at: indexPath.row) as AnyObject).value(forKey: "wpm_date") as? String
            {
                let wpmDate  = "\(wpm_date)"
                let date = getStringData(currentFormat: "yyyy-MM-dd", strDate: wpmDate, newFormat: "dd")
                let day = getStringData(currentFormat: "yyyy-MM-dd", strDate: wpmDate, newFormat: "E")
                let month = getStringData(currentFormat: "yyyy-MM-dd", strDate: wpmDate, newFormat: "MMMM")
                let year = getStringData(currentFormat: "yyyy-MM-dd", strDate: wpmDate, newFormat: "yyyy")

                cell?.lblDay.text = "\(date) \(day)"
                cell?.lblMonth.text = "\(month) \(year)"
            }
            
            var percentage:CGFloat!
            if let per =  (weightReportData.object(at: indexPath.row) as AnyObject).value(forKey: "per") as? String
            {
                let per1 = "\(per)"
                let floatPer = Double(per1)
                
                if floatPer != nil{
                    if floatPer! > Double(100.0){
                        percentage = 1.0
                    }
                    else{
                        let perc = (floatPer! / Double(100))
                        percentage = CGFloat(perc)
                    }
                }
                else{
                        percentage = 0.0
                }
                
            }
            else if let per =  (weightReportData.object(at: indexPath.row) as AnyObject).value(forKey: "per") as? NSNumber
            {
                if Float("\(per)")! >= 100.0{
                    percentage = 1.0
                }
                else{
                    let perc = (Float("\(per)")! / Float(100))
                    percentage = CGFloat(perc)
                }
            }
            
            
            
            
            cell?.pLabel.backgroundColor = UIColor.clear
            cell?.pLabel.startDegree = 1.0
            cell?.pLabel.endDegree = 100.0
            
            cell?.pLabel.trackWidth = 5.0
            cell?.pLabel.progressWidth = 5.0
            cell?.pLabel.roundedCornersWidth = 0.0
            
            cell?.pLabel.trackColor = UIColor.init(red: 148.0 / 255.0, green: 190.0 / 255.0, blue: 71.0 / 255.0, alpha: 0.5)
            cell?.pLabel.progressColor = UIColor.init(red: 148.0 / 255.0, green: 190.0 / 255.0, blue: 71.0 / 255.0, alpha: 1.0)
            cell?.pLabel.isStartDegreeUserInteractive = false
            cell?.pLabel.isEndDegreeUserInteractive = false
            //pLabel.progress = 50.0/100.0
            //self.pLabel.text = String(describing: pLabel.progress * 100)
            
            cell?.pLabel.textColor = UIColor.black
            cell?.pLabel.progress = percentage
            print("Progress :",cell?.pLabel.progress ?? "Nothing")
            
            let progress = String.init(format: "%.0f", (cell?.pLabel.progress)! * 100 )

            cell?.pLabel.text = "\(progress)%"



            
            
            return cell!
            
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 100 {
            
        }else if tableView.tag == 101{
//            self.view.addSubview(viewReview)
            self.setSelectedWeightProgramFromReport(dateValue: (weightReportData.object(at: indexPath.row) as AnyObject).value(forKey: "wpm_date") as! String, caloryValue: (weightReportData.object(at: indexPath.row) as AnyObject).value(forKey: "totalkcal") as! String, weightID: weightProgramID)
            
            
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 100 {
            return 50.0
        }else if tableView.tag == 101{
            return 60.0
        }
        return 0.0
    }
    
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
//        footerView.backgroundColor = UIColor.blue
//        return footerView
//    }
    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        if tableView.tag == 100 {
//        return 40
//        }
//        return 0
//    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView.tag == 100 {
            return 50
        }
        return 0
    }
    
    // TODO: - ACTION METHODS
    
    @IBAction func btnOrderFoodClicked(_ sender: Any) {
                let weightReductForm6VC = WeightReductForm6VC(nibName: "WeightReductForm6VC", bundle: nil)
                self.navigationController?.pushViewController(weightReductForm6VC, animated: true)
    }
    @IBAction func btnHeaderClicked(_ sender:UIButton){
        
        if sender.tag == 10 {
            currentPag=0;
            self.setupScrollViewWithCurrentPage(currentPage: currentPag)
            self.settingBtnBgColor(btnCalori)
            
        }else if sender.tag == 11 {
            currentPag=1;
            self.setupScrollViewWithCurrentPage(currentPage: currentPag)
            self.settingBtnBgColor(btnCalender)
        }
        
        
    }
    @IBAction func btnHeaderSectionClicked(_ sender:UIButton){
        print("Sender",sender.tag)
        let searchFood = SearchFoodVC(nibName: "SearchFoodVC", bundle: nil)
        if sender.tag == 0{
            searchFood.FoodType = "1"
        }
        else if sender.tag == 1{
            searchFood.FoodType = "2"
        }
        else if sender.tag == 2{
            searchFood.FoodType = "3"
        }
        else if sender.tag == 3{
            searchFood.FoodType = "4"
        }
        searchFood.weightProgramID = self.wPID
        searchFood.selectedPassDate = self.selectedPassDate
        self.navigationController?.pushViewController(searchFood, animated: true)
    }
    
    @IBAction func btnBackClicked(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func btnDatePickerSelectedClicked(_ sender:UIButton){
        self.view.endEditing(true)
        
        if txtDate.text != "" {
            let str = txtDate.text!
            if str != "0000-00-00"{
                let dateFormater = DateFormatter()
                dateFormater.dateFormat = "MMMM yyyy"
                var dateFromString = Date.init()
                dateFromString = dateFormater.date(from: str as String)!
                datePicker.setDate(dateFromString as Date, animated: true)
            }
        }
        
//        datePicker.maximumDate = NSDate() as Date
        datePicker.datePickerMode = .date;
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewDatePicker.frame=CGRect(x: self.view.frame.size.width - self.viewDatePicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y - self.viewDatePicker.frame.size.height, width: self.viewDatePicker.frame.size.width, height: self.viewDatePicker.frame.size.height)
            
            
        })
    }
    
    
    
    
    
    @IBAction func cancelPicker(sender:AnyObject){
        UIView.animate(withDuration: 0.3) {
            self.viewDatePicker.frame=CGRect(x: self.view.frame.size.width - self.viewDatePicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewDatePicker.frame.size.width, height: self.viewDatePicker.frame.size.height)
        }
    }
    @IBAction func doneDatePicker(sender:UIButton){
        
        txtDate.text = getStringDateFromDate(dateFormat: "MMMM yyyy", enterDate: datePicker.date)
        dateValue  = getStringDateFromDate(dateFormat: "MM yyyy", enterDate: datePicker.date)

        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForMyWeightProgramReportData), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewDatePicker.frame=CGRect(x: self.view.frame.size.width - self.viewDatePicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewDatePicker.frame.size.width, height: self.viewDatePicker.frame.size.height)
            
        })
    }

    
    @IBAction func btnLunchClicked(_ sender:UIButton){
        let searchFood = SearchFoodVC(nibName: "SearchFoodVC", bundle: nil)
        searchFood.selectedPassDate = self.selectedPassDate
        self.navigationController?.pushViewController(searchFood, animated: true)
    }
    
    @IBAction func btnSubmitClicked(_ sender:UIButton){
        viewReview.removeFromSuperview()
    }
    @IBAction func btnCancelClicked(_ sender:UIButton){
        viewReview.removeFromSuperview()
    }
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForMyWeightProgramReportData(){
        let completeURL = NSString(format:"%@%@", MainURL,myWeightProgramReportURL) as String
        
        
        let datePart = (dateValue)?.components(separatedBy: " ")
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "wp_id":weightProgramID,
            "month":datePart![0] ,
            "year":datePart![1]
        ]
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("WeightProgramReportData API Parameter :",finalParams)
        print("WeightProgramReportData API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: myWeightProgramReportURLTag)
        
    }
    
    func postDataOnWebserviceForSingleDayProgram(){
        let completeURL = NSString(format:"%@%@", MainURL,singleDayProgramURL) as String
        
        
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "wp_id":weightProgramID,
            "date":selectedPassDate,
            "kcal":selectedCalory
        ]
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        print("SingleDayProgram API Parameter :",finalParams)
        print("SingleDayProgram API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: singleDayProgramURLTag)
        
    }

    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case myWeightProgramReportURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("WeightProgramReportData Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                weightReportData = NSMutableArray(array: resultDict.value(forKey: "data") as! NSArray)
                weightReportGlobalData = NSMutableArray(array: resultDict.value(forKey: "data") as! NSArray)
                
                if weightReportData.count == 0{
                    tableViewCalender.isHidden = true
                    lblNoCalender.isHidden = false
                }
                else{
                    tableViewCalender.isHidden = false
                    lblNoCalender.isHidden = true
                }
                
                
                lblCalenderTitle1.text = "Great!!! you have completed \(resultDict.value(forKey: "obatain") ?? "") days diet"
                lblCalenderTitle2.text = "\(resultDict.value(forKey: "remain") ?? "") days remian to complete diet"
        
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
    
            SVProgressHUD.dismiss()
            tableViewCalender.reloadData()
            break
            
        case singleDayProgramURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("singleDayProgramURLTag Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                if let foodData = resultDict.value(forKey: "data") as? NSDictionary{
                    
                    let totalCalory:String!
                    if let total_kcal = foodData.value(forKey: "total_kcal") as? String{
                        totalCalory  = "\(total_kcal)"
                    }
                    else if let total_kcal = foodData.value(forKey: "total_kcal") as? NSNumber{
                        totalCalory = "\(total_kcal)"
                    }
                    else{
                        totalCalory = "0"
                    }
                    
                    let consumeCalory:String!
                    if let consumed = foodData.value(forKey: "consumed") as? String{
                        consumeCalory  = "\(consumed)"
                    }
                    else if let consumed = foodData.value(forKey: "consumed") as? NSNumber{
                        consumeCalory = "\(consumed)"
                    }
                    else{
                        consumeCalory = "0"
                    }
                    
                  
                    if consumeCalory != "0"{
//                        let percentageValue:Int = ((Int(consumeCalory)! * 100) / Int(totalCalory)!)
//                        print("percentageValue : ",percentageValue)
                        
                        let calcu = Float(consumeCalory)! / Float(totalCalory)!
                        progressBarValue = CGFloat(calcu)
                        print("progressBarValue : ",calcu)
                        self.setProgressBar()
                    }
                    
                    
                    
                    
                    if let wpm_id = foodData.value(forKey: "wpm_id") as? String{
                        self.wPID  = "\(wpm_id)"
                    }
                    else if let wpm_id = foodData.value(forKey: "wpm_id") as? NSNumber{
                        self.wPID = "\(wpm_id)"
                    }
                    else{
                        self.wPID = ""
                    }
                    
                    
                        
                    breakFastData = NSMutableArray(array: foodData.value(forKey: "breakfastarray") as! NSArray)
                    lunchDataData = NSMutableArray(array: foodData.value(forKey: "luncharray") as! NSArray)
                    dinnerData = NSMutableArray(array: foodData.value(forKey: "dinnerarray") as! NSArray)
                    otherData = NSMutableArray(array: foodData.value(forKey: "otherarray") as! NSArray)
                    
                    caloriData = NSMutableArray()
                    caloriData.add(breakFastData)
                    caloriData.add(lunchDataData)
                    caloriData.add(dinnerData)
                    caloriData.add(otherData)
                    
                    print("Calories Data : ", caloriData)
                    
                    if let consumed = foodData.value(forKey: "consumed") as? String{
                        lblConsumed.text = "\(consumed)"
                    }
                    else if let consumed = foodData.value(forKey: "consumed") as? NSNumber{
                        lblConsumed.text = "\(consumed)"
                    }
                    else{
                        lblConsumed.text = "00"
                    }
                    
                    if let remain = foodData.value(forKey: "remain") as? String{
                        lblRemaining.text = "\(remain)"
                    }
                    else if let remain = foodData.value(forKey: "remain") as? NSNumber{
                        lblRemaining.text = "\(remain)"
                    }
                    else{
                        lblRemaining.text = "00"
                    }
                    
                    if let breakfast = foodData.value(forKey: "breakfast") as? String{
                        totalBreakFast = "\(breakfast)"
                    }
                    else  if let breakfast = foodData.value(forKey: "breakfast") as? NSNumber{
                        totalBreakFast = "\(breakfast)"
                    }
                    else{
                        totalBreakFast = ""
                    }
                    
                    if let lunch = foodData.value(forKey: "lunch") as? String{
                        totalLunch = "\(lunch)"
                    }
                    else  if let lunch = foodData.value(forKey: "lunch") as? NSNumber{
                        totalLunch = "\(lunch)"
                    }
                    else{
                        totalLunch = ""
                    }
                    
                    
                    if let dinner = foodData.value(forKey: "dinner") as? String{
                        totalDinner = "\(dinner)"
                    }
                    else  if let dinner = foodData.value(forKey: "dinner") as? NSNumber{
                        totalDinner = "\(dinner)"
                    }
                    else{
                        totalDinner = ""
                    }
                    
                    
                    if let other = foodData.value(forKey: "other") as? String{
                        totalOther = "\(other)"
                    }
                    else  if let other = foodData.value(forKey: "other") as? NSNumber{
                        totalOther = "\(other)"
                    }
                    else{
                        totalOther = ""
                    }
                }
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            SVProgressHUD.dismiss()
            tableViewCalori.reloadData()
            break

            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case myWeightProgramReportURLTag:
            SVProgressHUD.dismiss()
            break
            
        case singleDayProgramURLTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
}
