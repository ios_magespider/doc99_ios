//
//  WeightReductionVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 02/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class WeightReductionVC: UIViewController {

    // FIXME: - VIEW CONTROLLER METHODS
    @IBOutlet var lblHeader:UILabel!
    @IBOutlet var lblContinue:UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        self.setLocalizationText()
    }
    
    func setLocalizationText(){
        lblHeader.text = NSLocalizedString("Weight Reduction Program", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblContinue.text = NSLocalizedString("CONTINUE", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
    }
    
    // TODO: - DELEGATE METHODS
    // TODO: - ACTION METHODS
    @IBAction func btnContinueClicked(_ sender: UIButton) {
        
//        if let firstTIme = USERDEFAULT.value(forKey: "isFirstTimeWeightReduct") as? String{
//            let first = "\(firstTIme)"
//            if first == "NO"{
//                let weightReductForm5VC = WeightReductForm5VC(nibName: "WeightReductForm5VC", bundle: nil)
//                self.navigationController?.pushViewController(weightReductForm5VC, animated: true)
//            }
//            else{
//                let weightReductDetailVC = WeightReductDetailVC(nibName: "WeightReductDetailVC", bundle: nil)
//                self.navigationController?.pushViewController(weightReductDetailVC, animated: true)
//            }
//        }
//        else{
            let weightReductDetailVC = WeightReductDetailVC(nibName: "WeightReductDetailVC", bundle: nil)
            self.navigationController?.pushViewController(weightReductDetailVC, animated: true)
//        }
        
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    // TODO: - POST DATA METHODS
}
