//
//  WeightReductForm1VC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 02/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class WeightReductForm1VC: UIViewController {
    
    // FIXME: - VIEW CONTROLLER METHODS
    @IBOutlet var txtWeight:UITextField!
    @IBOutlet var txtheight:UITextField!

    @IBOutlet var scrMain:UIScrollView!
    
    @IBOutlet var lblWeight:UILabel!
    @IBOutlet var lblheight:UILabel!
    @IBOutlet var lblHeader:UILabel!
    @IBOutlet var lblNext:UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        self.addTapGestureInOurView()
        self.NavBarNumberPad()
        self.setLocalizationText()

    }
    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        
        if viewTouched!.isKind(of: UIButton.self) && viewTouched!.isKind(of:UITextView.self){
            
        }
        else{
            self.view.endEditing(true)
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        }
    }

    func setLocalizationText(){
        lblHeader.text = NSLocalizedString("Weight Reduction Program", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblWeight.text = NSLocalizedString("Your Weight (kg)", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblheight.text = NSLocalizedString("Your Height (cm)", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblNext.text = NSLocalizedString("NEXT", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
    }

    
    // Numeric Keyboard Hide Method
    func NavBarNumberPad(){
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        numberToolbar.barStyle = .default
        
        let viewBack = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        viewBack.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        numberToolbar.addSubview(viewBack)
        
        let btnCancel = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelNumberPad))
        btnCancel.tintColor = UIColor.white
        
        let btnDone = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.doneWithNumberPad))
        btnDone.tintColor = UIColor.white
        
        numberToolbar.items = NSArray(objects: btnCancel,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),btnDone) as? [UIBarButtonItem]
        
        
        numberToolbar.sizeToFit()
        
        txtWeight.inputAccessoryView = numberToolbar
        txtheight.inputAccessoryView = numberToolbar
        
    }
    
    func cancelNumberPad(){
        
        txtWeight.resignFirstResponder()
        txtheight.resignFirstResponder()
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
    }
    
    func doneWithNumberPad(){
        txtWeight.resignFirstResponder()
        txtheight.resignFirstResponder()
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
    }

    
    
    // TODO: - DELEGATE METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
            
        }
        return false
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var YOffset:Int = 0
        
        if (textField.tag==5){
            YOffset=0
        }
        else if (textField.tag==6){
            YOffset=0
        }
        
        if (DeviceType.IS_IPHONE_4_OR_LESS) {
            if (textField.tag==5){
                YOffset=10
            }
            else if (textField.tag==6){
                YOffset=10
            }
        }
        scrMain.setContentOffset(CGPoint(x: 0, y: CGFloat(textField.tag * YOffset)), animated: true)
        return true
        
    }
    

  

    
    // TODO: - ACTION METHODS
    @IBAction func btnNextClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
  
//        if APPDELEGATE.weightReductionData.value(forKey: "weight") != nil && APPDELEGATE.weightReductionData.value(forKey: "height") != nil {
            if txtWeight.text == ""{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter weight", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
            }
            else if txtheight.text == ""{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter height", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
            }
            else{
                
                APPDELEGATE.weightReductionData.setValue(txtWeight.text, forKey: "weight")
                APPDELEGATE.weightReductionData.setValue(txtheight.text, forKey: "height")
                
                let weightReductForm2VC = WeightReductForm2VC(nibName: "WeightReductForm2VC", bundle: nil)
                weightReductForm2VC.weightValue = txtWeight.text
                self.navigationController?.pushViewController(weightReductForm2VC, animated: true)
                
            }

//        }
//        else{
//            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter weight", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
//        }
        
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        APPDELEGATE.weightReductionData.setValue(nil, forKey: "weight")
        APPDELEGATE.weightReductionData.setValue(nil, forKey: "height")
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    // TODO: - POST DATA METHODS
}
