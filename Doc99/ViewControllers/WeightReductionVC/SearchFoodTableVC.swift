//
//  SearchFoodTableVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 22/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class SearchFoodTableVC: UITableViewCell {

    @IBOutlet var lblFoodName:UILabel!
    @IBOutlet var lblQty:UILabel!
    @IBOutlet var lblCalery:UILabel!

    @IBOutlet var imageViewSelect:UIImageView!
    @IBOutlet var btnCategory:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
