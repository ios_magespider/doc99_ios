//
//  WeightReductDetailVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 02/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class WeightReductDetailVC: UIViewController,UIAlertViewDelegate {
    
    // FIXME: - VARIABLE
    
    @IBOutlet var webViewPrescriptionTC:UIWebView!
    
    
    @IBOutlet var lblEprescription:UILabel!
    @IBOutlet var lblTermCondition:UILabel!
    @IBOutlet var lblStart:UILabel!
    
    //var myWeightProgramData = NSMutableArray()
    //var myWeightProgramGlobalData = NSMutableArray()
    
    var wp_id = ""

    // FIXME: - VIEW CONTROLLER METHODS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        
        // Define identifier
        let notificationName3 = Notification.Name("refreshMyWeightProgramData")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(refreshMyWeightProgramData(notification:)), name: notificationName3, object: nil)
        
        
              // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        //webViewPrescriptionTC.loadRequest(URLRequest(url: URL(string: "http://hardwirestudios.com")!))
        
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetPrescriptionTermCondition), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForMyWeightProgramData), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }

         self.setLocalizationText()
    }
    func refreshMyWeightProgramData(notification: NSNotification) {
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForMyWeightProgramData), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }

    }

    
    
    func setLocalizationText(){
        
        lblEprescription.text = NSLocalizedString("Weight Reduction Program", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTermCondition.text = NSLocalizedString("Disclaimer", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblStart.text = NSLocalizedString("START", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
    }
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    
    // TODO: - DELEGATE METHODS
   
    
    //AlertDelegate Methods protocol
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }

    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("WebView Error : ",error.localizedDescription)
    }
    
    // TODO: - ACTION METHODS
    @IBAction func btnStartClicked(_ sender: UIButton) {
        
//        if let firstTIme = USERDEFAULT.value(forKey: "isFirstTimeWeightReduct") as? String{
//            let first = "\(firstTIme)"
//            if first == "NO"{
//                let weightReductForm5VC = WeightReductForm5VC(nibName: "WeightReductForm5VC", bundle: nil)
//                weightReductForm5VC.isFromAddWeightProgram = false
//                self.navigationController?.pushViewController(weightReductForm5VC, animated: true)
//
//            }
//            else{
//                let weightReductForm1VC = WeightReductForm1VC(nibName: "WeightReductForm1VC", bundle: nil)
//                self.navigationController?.pushViewController(weightReductForm1VC, animated: true)
//
//            }
//        }
//        else{
//            let weightReductForm1VC = WeightReductForm1VC(nibName: "WeightReductForm1VC", bundle: nil)
//            self.navigationController?.pushViewController(weightReductForm1VC, animated: true)
//
//        }
        
        if wp_id != ""{
            let weightReductForm5VC = WeightReductForm5VC(nibName: "WeightReductForm5VC", bundle: nil)
            weightReductForm5VC.isFromAddWeightProgram = false
            self.navigationController?.pushViewController(weightReductForm5VC, animated: true)

        }
        else{
            let weightReductForm1VC = WeightReductForm1VC(nibName: "WeightReductForm1VC", bundle: nil)
            self.navigationController?.pushViewController(weightReductForm1VC, animated: true)
        }
        
        
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }
    
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForGetPrescriptionTermCondition(){
        
        let completeURL = NSString(format:"%@%@", MainURL,cmsWebPageCallURL) as String
        let params:NSDictionary = [
            "page" : "wightreductiondisclaimer"
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("TermCondition API Parameter :",finalParams)
        print("TermCondition API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: cmsWebPageCallURLTag)
    }
    
    func postDataOnWebserviceForMyWeightProgramData(){
        let completeURL = NSString(format:"%@%@", MainURL,myWeightProgramCheckURL) as String
        
        let date = Date()
        //let calendar = Calendar.current
        
        //let month = calendar.component(.month, from: date)
        //let year = calendar.component(.year, from: date)
        
        let formate = DateFormatter()
        formate.dateFormat = "MM"
        let month = formate.string(from: date)
        
        formate.dateFormat = "yyyy"
        let year = formate.string(from: date)
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "month": "\(month)",
            "year": "\(year)",
            "lang_type":Language_Type
        ]
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("MyWeightProgramData API Parameter :",finalParams)
        print("MyWeightProgramData API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: myWeightProgramCheckURLTag)
        
    }
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case cmsWebPageCallURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("TermCondition Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                
                let myDescription = resultDict.value(forKey: "page_description") as! String
                let abc = myDescription.replacingOccurrences(of: "\n", with: "")
                webViewPrescriptionTC.loadHTMLString(abc, baseURL: nil)
                
                
            }
          //  SVProgressHUD.dismiss()
            break
            
        case myWeightProgramCheckURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("myWeightProgramCheck Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                if let data = resultDict.value(forKey: "data") as? NSDictionary {
                    wp_id = data.value(forKey: "wp_id") as? String ?? ""
                }
                //myWeightProgramData = NSMutableArray(array: resultDict.value(forKey: "data") as! NSArray)
                //myWeightProgramGlobalData = NSMutableArray(array: resultDict.value(forKey: "data") as! NSArray)
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
          //  SVProgressHUD.dismiss()
            break

            
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case cmsWebPageCallURLTag:
            SVProgressHUD.dismiss()
            break
            
        case myWeightProgramCheckURLTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
}
