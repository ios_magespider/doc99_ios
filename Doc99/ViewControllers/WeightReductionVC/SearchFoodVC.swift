//
//  SearchFoodVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 02/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD

class SearchFoodVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate {
    
    @IBOutlet var tableViewSearch:UITableView!
    
    @IBOutlet var tableViewSearchDefault:UITableView!

    @IBOutlet weak var labelNoHistory: UILabel!
    
    @IBOutlet var txtSearch:UITextField!
    
    var selectedPassDate:String!

    //For Pasignation
    var pageNum:Int!
    var isLoading:Bool?
    
    var searchDefaultData = NSMutableArray()
    var searchSaveData = NSMutableArray()

    
    var searchData = NSMutableArray()
    var searchGlobalData = NSMutableArray()
    
    @IBOutlet var lblNoDrug:UILabel!
    
    var boolArray:NSMutableArray!

    var selectedFoodData = NSMutableArray()

    @IBOutlet var btnDone:UIButton!

    var FoodType:String!
    var foodID:String!
    var weightProgramID:String!
    
    var zeroTempDataArray:NSMutableArray!

    
    @IBOutlet var viewSearch:UIView!
    @IBOutlet var viewMain:UIView!



    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        foodID = ""
        print("SelectedDate : ",selectedPassDate)
        print("weightProgramID : ",weightProgramID)
        print("foodID : ",foodID)
        print("FoodType : ",FoodType)

        tableViewSearchDefault.isHidden = false
        tableViewSearchDefault.layer.masksToBounds = true
        tableViewSearchDefault.layer.borderColor = UIColor( red: 108.0/255.0, green: 110.0/255.0, blue:112.0/255.0, alpha: 0.2 ).cgColor
        tableViewSearchDefault.layer.borderWidth = 1.0

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // TODO: - OTHER METHODS
    
    func generalViewControllerSetting(){
        
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.searchData = NSMutableArray()
        self.searchGlobalData = NSMutableArray()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForSearchFood), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        self.addTapGestureInOurView()
        self.setLocalizationText()
        self.setSearchDefaultData()
    }
    
    func setSearchDefaultData(){
        
        if let saveData = USERDEFAULT.value(forKey: "saveSearchData") as? NSArray{
            searchDefaultData = saveData.mutableCopy() as! NSMutableArray
            searchSaveData = saveData.mutableCopy() as! NSMutableArray
        }
        print("search Default data :",searchDefaultData)
        if searchDefaultData.count == 0 {
            labelNoHistory.isHidden = false
        }
        else{
            labelNoHistory.isHidden = true
        }
        
        tableViewSearchDefault.reloadData()
        
        tableViewSearchDefault.frame = CGRect(x: tableViewSearchDefault.frame.origin.x, y: tableViewSearchDefault.frame.origin.y, width: tableViewSearchDefault.frame.size.width, height: tableViewSearchDefault.contentSize.height)
        
    }
    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        if viewTouched!.isKind(of: UIButton.self) && viewTouched!.isKind(of:UITextView.self){
            
        }
        else{
            self.view.endEditing(true)
        }
        
    }
    
    
    
    
    func addLoadingIndicatiorOnFooterOnTableView(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableViewSearch.tableFooterView = spinner
    }
    func removeLoadingIndicatiorOnFooterOnTableView(){
        tableViewSearch.tableFooterView = nil
    }
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
        }
    }
    
    func setLocalizationText(){
        lblNoDrug.text = NSLocalizedString("No Food Found", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
        btnDone.setTitle(NSLocalizedString("DONE", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)

        txtSearch.attributedPlaceholder = NSAttributedString(string:NSLocalizedString("Search Food", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""),
                                                           attributes:nil)

    }
    
    // TODO: - Textfield DELEGATE METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        let nextTage=textField.tag+1;
//        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
//        if (nextResponder != nil){
//            nextResponder?.becomeFirstResponder()
//        }
//        else
//        {
            textField.resignFirstResponder()
//        }
        return false
    }
    
    
    // TODO: - DELEGATE ScrollView
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        
//        if scrollView == tableViewSearch {
//            if isLoading == true{
//                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
//                    pageNum = pageNum + 1
//                    print(pageNum)
//                    isLoading = false
//                    
//                    if Reachability.isConnectedToNetwork() == true {
//                        self.addLoadingIndicatiorOnFooterOnTableView()
//                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetDrugList), userInfo: nil, repeats: false)
//                    } else {
//                        showAlert("Check Connection", title: "Internet is not available.")
//                    }
//                }
//            }
//            
//        }
//        
//        
//    }
    
    
    // TODO: - DELEGATE METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewSearch{
            return self.searchData.count
        }
        else if tableView == tableViewSearchDefault{
            return self.searchDefaultData.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableViewSearch{
            let identifier = "searchFoodCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? SearchFoodTableVC
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("SearchFoodTableVC", owner: self, options: nil)
                cell = nib?[0] as? SearchFoodTableVC
            }
            cell!.selectionStyle = .none;
            
            
            if let food_name =  (self.searchData[indexPath.row] as AnyObject).value(forKey: "food_name") as? String
            {
                cell?.lblFoodName.text = "\(food_name)"
            }
            
            if let kcal =  (self.searchData[indexPath.row] as AnyObject).value(forKey: "kcal") as? String
            {
                cell?.lblCalery.text = "\(kcal) kcal"
            }
            
            if let qty =  (self.searchData[indexPath.row] as AnyObject).value(forKey: "qty") as? String
            {
                cell?.lblQty.text = "\(qty)"
            }
            
            
            if boolArray.object(at: indexPath.row) as! String == "1"{
                cell?.imageViewSelect.image = UIImage.init(named: "check_blue.png")
            }
            else{
                cell?.imageViewSelect.image = UIImage.init(named: "check_gray.png")
            }
            
            
            cell?.btnCategory.tag = indexPath.row
            cell?.btnCategory.addTarget(self, action: #selector(self.btnCategorySelectionClicked(_:)), for: .touchUpInside)
            
            
            return cell!
        }
        else{
            let identifier = "foodDefaultCell"
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? SearchFoodDefaultTableVC
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("SearchFoodDefaultTableVC", owner: self, options: nil)
                cell = nib?[0] as? SearchFoodDefaultTableVC
            }
            cell!.selectionStyle = .none;
            
            
            if let food_name =  (self.searchDefaultData[indexPath.row] as AnyObject).value(forKey: "food_name") as? String
            {
                cell?.lblFoodName.text = "\(food_name)"
            }
            
            cell?.btnCancel.tag = indexPath.row
            cell?.btnCancel.addTarget(self, action: #selector(self.btnCancelDefaultClicked(_:)), for: .touchUpInside)
            
            
            return cell!

        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableViewSearch{
            return 62.0
        }
        else if tableView == tableViewSearchDefault{
            return 40.0
        }
        return 0.0
    }
    
    // TODO: - ACTION METHODS
    @IBAction func btnBackClicked(_ sender:UIButton){
        _ =  self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnBackSearchClicked(_ sender:UIButton){
       viewSearch.removeFromSuperview()
    }
    
    @IBAction func btnCancelDefaultClicked(_ sender:UIButton){
        searchDefaultData.removeObject(at: sender.tag)
        USERDEFAULT.setValue(searchDefaultData, forKey: "saveSearchData")
        USERDEFAULT.synchronize()
        self.setSearchDefaultData()
    }
    
    @IBAction func btnCategorySelectionClicked(_ sender:UIButton){
        
//        if boolArray.object(at: sender.tag) as! String == "0"{
//            boolArray.replaceObject(at: sender.tag, with: "1")
////            foodID = (self.searchData[sender.tag] as AnyObject).value(forKey: "fi_id") as! String
//        }
//        else{
//            boolArray.replaceObject(at: sender.tag, with: "0")
//        }
//        let indexPath = NSIndexPath(row: sender.tag, section: 0)
//        tableViewSearch.reloadRows(at: [indexPath as IndexPath], with: .none)
//        
        
        
        let indexPath = NSIndexPath(row: sender.tag, section: 0)
        
        let myIndexValue:Int = boolArray.index(of: "1")
        
        boolArray = zeroTempDataArray.mutableCopy() as! NSMutableArray
        
        let oneDataArray = zeroTempDataArray.mutableCopy() as! NSMutableArray
        
        oneDataArray.replaceObject(at: indexPath.row, with: "1")
        boolArray.replaceObject(at: indexPath.row, with: oneDataArray.object(at: indexPath.row))
        
        UIView.animate(withDuration: 0, animations: {
//            self.collectionViewFilterDrug.performBatchUpdates({
//                let indexPath1 = IndexPath(row: indexPath.row, section: 0)
//                print("Indexpath1 \(indexPath1.row)")
//                self.collectionViewFilterDrug.reloadItems(at: [indexPath1])
//            }, completion: nil)
            
            let indexPath1 = IndexPath(row: indexPath.row, section: 0)
            print("Indexpath1 \(indexPath1.row)")
            self.tableViewSearch.reloadRows(at: [indexPath1], with: .none)
            
        })
        
        UIView.animate(withDuration: 0, animations: {
//            self.collectionViewFilterDrug.performBatchUpdates({
//                if (myIndexValue<self.boolArrayForCategory.count) {
//                    let indexPath2 = IndexPath(row: myIndexValue, section: 0)
//                    print("Indexpath2 \(indexPath2.row)")
//                    self.collectionViewFilterDrug.reloadItems(at: [indexPath2])
//                }
//            }, completion: nil)
            
            if (myIndexValue<self.boolArray.count) {
                let indexPath2 = IndexPath(row: myIndexValue, section: 0)
                print("Indexpath2 \(indexPath2.row)")
                self.tableViewSearch.reloadRows(at: [indexPath2], with: .none)
            }

        })
        

        
        
        
        
        selectedFoodData = NSMutableArray.init()
        for i in 0...searchData.count - 1 {
            if boolArray.object(at: i) as! String == "1"{
                selectedFoodData.add(self.searchData[i] as! NSDictionary)
            }
        }
        
        print("Selected Food Data : ",selectedFoodData)
        
    }
    
    
    @IBAction func searchFilter(){
//        var predicate = String()
//        
//        
//        if (txtSearch.text == "") {
//            searchData = searchGlobalData.mutableCopy() as! NSMutableArray
//        }else{
//            predicate = String(format: "SELF['drug_name'] contains[c] '%@'", txtSearch.text!)
//            print(predicate)
//            
//            let myPredicate:NSPredicate = NSPredicate(format:predicate)
//            let temp = self.searchGlobalData.mutableCopy() as! NSArray
//            self.searchData = (temp.filtered(using: myPredicate) as NSArray).mutableCopy() as! NSMutableArray
//            
//        }
        
        viewMain.addSubview(viewSearch)
        tableViewSearchDefault.isHidden = true
        labelNoHistory.isHidden = true

        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForSearchFood), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
    }
    
    @IBAction func btnDoneClicked(_ sender:UIButton){
        
//        let fIDData = NSMutableArray()
//        if selectedFoodData.count > 0{
//            for i in 0...selectedFoodData.count - 1 {
//               fIDData.add((selectedFoodData.object(at: i) as AnyObject).value(forKey: "fi_id") as! String)
//            }
//        }
//        
//        foodID = fIDData.componentsJoined(by: ",")
//        print("Food ID Date: ",foodID)
        
        let idData  = NSMutableArray()

        if selectedFoodData.count > 0{
            
            if searchSaveData.count > 0{
                for i in 0...searchSaveData.count - 1 {
                    idData.add((searchSaveData.object(at: i) as AnyObject).value(forKey: "fi_id") as! String)
                }
                
                let myIndexValue:Int = idData.index(of: (selectedFoodData.object(at: 0) as AnyObject).value(forKey: "fi_id") as! String)
                print("My IndexValue ::: ",myIndexValue)
                if (myIndexValue<self.boolArray.count) {
                }
                else{
                    searchSaveData.add(selectedFoodData.object(at: 0))
                }


            }
            else{
                searchSaveData.add(selectedFoodData.object(at: 0))

            }
            
            
            
            print("search Save Data ::: ",searchSaveData)
            
            USERDEFAULT.setValue(searchSaveData, forKey: "saveSearchData")
            USERDEFAULT.synchronize()
            self.setSearchDefaultData()
            viewSearch.removeFromSuperview()
            
            tableViewSearchDefault.isHidden = false
            
            if Reachability.isConnectedToNetwork() == true {
                SVProgressHUD.show(withStatus: "Loading..")
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForSaveFoodItem), userInfo: nil, repeats: false)
            } else {
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }
        else{
            showAlert(Appname, title: "Please select food")
        }
        
    }
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForSearchFood(){
        let completeURL = NSString(format:"%@%@", MainURL,searchFoodURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "keyword":txtSearch.text!
        ]
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("SearchFood API Parameter :",finalParams)
        print("SearchFood API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: searchFoodURLTag)
        
    }
    
    func postDataOnWebserviceForSaveFoodItem(){
        let completeURL = NSString(format:"%@%@", MainURL,saveFoodItemURL) as String
        
        
        //let date = getStringDateFromDate(dateFormat: "yyyy-MM-dd", enterDate: Date())
        foodID = (selectedFoodData.object(at: 0) as AnyObject).value(forKey: "fi_id") as! String
        
        print("FoodID",foodID)
        print("FoodType",FoodType)
        print("Weight Program ID",weightProgramID)
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "wpm_id":weightProgramID,
            "date":selectedPassDate,
            "food_type":FoodType,
            "fi_id":foodID
        ]
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        
        
        print("Save Food API Parameter :",finalParams)
        print("Save Food API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: saveFoodItemURLTag)
        
    }

    
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case searchFoodURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("SearchFood Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                searchData = NSMutableArray(array: resultDict.value(forKey: "data") as! NSArray)
                searchGlobalData = NSMutableArray(array: resultDict.value(forKey: "data") as! NSArray)

                boolArray = NSMutableArray.init()
                if searchData.count > 0{
                    for _ in 0...searchData.count - 1 {
                        boolArray.add("0")
                    }
                    
                    zeroTempDataArray = NSMutableArray.init()
                    for _ in 0...searchData.count - 1 {
                        zeroTempDataArray.add("0")
                    }
                    
                }
                
                
                
                if self.searchData.count == 0 {
                    lblNoDrug.isHidden = false
                    tableViewSearch.isHidden = true
                    btnDone.isUserInteractionEnabled = false
                }
                else{
                    tableViewSearch.isHidden = false
                    lblNoDrug.isHidden = true
                    btnDone.isUserInteractionEnabled = true
                }
                tableViewSearch.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }

            break
            
        case saveFoodItemURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("Save Food Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                let data = resultDict.value(forKey: "data") as! NSDictionary
                //selectedFoodData
//                USERDEFAULT.setValue(data, forKey: "selectedFoodData")
//                USERDEFAULT.synchronize()
                
                
                let notificationName = Notification.Name("refreshFoodData")
                NotificationCenter.default.post(name: notificationName, object: nil)
                
                let notificationName1 = Notification.Name("refreshReportData")
                NotificationCenter.default.post(name: notificationName1, object: nil)

                
           //     self.navigationController?.popViewController(animated: true)


                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "3"{
                
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            SVProgressHUD.dismiss()
            break
            
            
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case searchFoodURLTag:
//            SVProgressHUD.dismiss()
            self.removeLoadingIndicatiorOnFooterOnTableView()

            break   
        case saveFoodItemURLTag:
            SVProgressHUD.dismiss()
            break
     
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    
    
}
