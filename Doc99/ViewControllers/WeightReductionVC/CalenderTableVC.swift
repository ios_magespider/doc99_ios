//
//  CalenderTableVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 05/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import KAProgressLabel

class CalenderTableVC: UITableViewCell {

    @IBOutlet var pLabel:KAProgressLabel!
    @IBOutlet var lblTotalCalory:UILabel!
    @IBOutlet var lblObtainCalory:UILabel!
    @IBOutlet var lblDay:UILabel!
    @IBOutlet var lblMonth:UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
