//
//  AriaStrokeVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 13/06/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class AriaStrokeVC: UIViewController,UITextFieldDelegate {
    // FIXME: - VARIABLES
    @IBOutlet var txtLeftEye:UITextField!
    @IBOutlet var txtRightEye:UITextField!
    @IBOutlet var lblLeftEye:UILabel!
    @IBOutlet var lblLeftTitle:UILabel!
    @IBOutlet var lblRightEye:UILabel!
    @IBOutlet var lblRightTitle:UILabel!
    
    @IBOutlet var lblTitleLeft:UILabel!
    @IBOutlet var lblTitleRight:UILabel!
    @IBOutlet var lblTitleLeft1:UILabel!
    @IBOutlet var lblTitleRight1:UILabel!
    @IBOutlet var lblHome:UILabel!
    @IBOutlet var lblHeader:UILabel!


    var leftEye:Double!
    var rightEye:Double!

    @IBOutlet var scrMain:UIScrollView!

    // FIXME: - VIEW CONTROLLER METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        self.addTapGestureInOurView()
        self.NavBarNumberPadForTextField()
        txtLeftEye.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        txtRightEye.addTarget(self, action: #selector(textFieldDidChange1(_:)), for: .editingChanged)
        self.setLocalizationText()
    }
    
    
    func setLocalizationText(){
        
        lblHeader.text = NSLocalizedString("ARIA Stroke Risk Test", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitleLeft.text = NSLocalizedString("Left Eye", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitleRight.text = NSLocalizedString("Right Eye", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitleLeft1.text = NSLocalizedString("Left Eye", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblTitleRight1.text = NSLocalizedString("Right Eye", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblLeftTitle.text = NSLocalizedString("Low Risk", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblRightTitle.text = NSLocalizedString("Low Risk", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblHome.text = NSLocalizedString("HOME", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

        
        
        
        
    }
    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        
        if viewTouched!.isKind(of: UIButton.self) && viewTouched!.isKind(of:UITextView.self){
            
        }
        else{
            self.view.endEditing(true)
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)

        }
    }

    // Numeric Keyboard Hide Method
    func NavBarNumberPadForTextField(){
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        numberToolbar.barStyle = .default
        
        let viewBack = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        viewBack.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        numberToolbar.addSubview(viewBack)
        
        let btnCancel = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelNumberPadMile))
        btnCancel.tintColor = UIColor.white
        
        let btnDone = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.doneWithNumberPadMile))
        btnDone.tintColor = UIColor.white

        numberToolbar.items = NSArray(objects: btnCancel,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),btnDone) as? [UIBarButtonItem]
        
        numberToolbar.sizeToFit()
        txtLeftEye.inputAccessoryView = numberToolbar
        txtRightEye.inputAccessoryView = numberToolbar

    }
    
    func cancelNumberPadMile(){
        txtLeftEye.resignFirstResponder()
        txtRightEye.resignFirstResponder()
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)

    }
    
    func doneWithNumberPadMile(){
        txtLeftEye.resignFirstResponder()
        txtRightEye.resignFirstResponder()
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)

    }
    
    
    
    // TODO: - DELEGATE METHODS
    
    func textFieldDidChange(_ textField: UITextField) {
        if textField.text == ""{
            leftEye = 0.0
            lblLeftEye.text = "0.0"
            lblLeftTitle.text = ""

        }
        else if (textField.text?.hasPrefix("."))!{
            leftEye = Double(0.0)
            textField.text = "0."
            if leftEye < 0.5
            {
                lblLeftEye.text = textField.text
                lblLeftTitle.text = "Low Risk"
                lblLeftTitle.text = NSLocalizedString("Low Risk", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
                
            }
            else if leftEye > 0.7
            {
                lblLeftEye.text = textField.text
                lblLeftTitle.text = "High Risk"
                lblLeftTitle.text = NSLocalizedString("High Risk", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
                
            }
            else{
                lblLeftEye.text = textField.text
                lblLeftTitle.text = "Medium Risk"
                lblLeftTitle.text = NSLocalizedString("Medium Risk", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
                
            }
            print("Left Eye :",leftEye)
        }
        else{
            leftEye = Double(textField.text!)
            if leftEye < 0.5
            {
                lblLeftEye.text = textField.text
                lblLeftTitle.text = "Low Risk"
                lblLeftTitle.text = NSLocalizedString("Low Risk", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

            }
            else if leftEye > 0.7
            {
                lblLeftEye.text = textField.text
                lblLeftTitle.text = "High Risk"
                lblLeftTitle.text = NSLocalizedString("High Risk", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

            }
            else{
                lblLeftEye.text = textField.text
                lblLeftTitle.text = "Medium Risk"
                lblLeftTitle.text = NSLocalizedString("Medium Risk", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

            }
            print("Left Eye :",leftEye)
        }
        
    }
    func textFieldDidChange1(_ textField: UITextField) {
        if textField.text == ""{
            rightEye = 0.0
            lblRightEye.text = "0.0"
            lblRightTitle.text = ""
        }
        else if (textField.text?.hasPrefix("."))!{
            
            rightEye = Double(0.0)
            textField.text = "0."
            if rightEye < 0.5
            {
                lblRightEye.text = textField.text
                lblRightTitle.text = "Low Risk"
                lblRightTitle.text = NSLocalizedString("Low Risk", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
                
                
            }
            else if rightEye > 0.7
            {
                lblRightEye.text = textField.text
                lblRightTitle.text = "High Risk"
                lblRightTitle.text = NSLocalizedString("High Risk", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
                
            }
            else{
                lblRightEye.text = textField.text
                lblRightTitle.text = "Medium Risk"
                lblRightTitle.text = NSLocalizedString("Medium Risk", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
            }
            print("Left Eye :",rightEye)

        }
        else{
            rightEye = Double(textField.text!)
            if rightEye < 0.5
            {
                lblRightEye.text = textField.text
                lblRightTitle.text = "Low Risk"
                lblRightTitle.text = NSLocalizedString("Low Risk", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
                
                
            }
            else if rightEye > 0.7
            {
                lblRightEye.text = textField.text
                lblRightTitle.text = "High Risk"
                lblRightTitle.text = NSLocalizedString("High Risk", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
                
            }
            else{
                lblRightEye.text = textField.text
                lblRightTitle.text = "Medium Risk"
                lblRightTitle.text = NSLocalizedString("Medium Risk", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
            }
            print("Left Eye :",rightEye)
        }
        
    }

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        }
        return false
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var YOffset:Int = 0
        
        if (textField.tag==5) {
            YOffset=0
        }
        else if (textField.tag==6){
            YOffset=0
        }
        
        if (DeviceType.IS_IPHONE_4_OR_LESS) {
            
            if (textField.tag==5) {
                YOffset=15
            }
            else if (textField.tag==6){
                YOffset=15
                
            }
        }
        scrMain.setContentOffset(CGPoint(x: 0, y: CGFloat(textField.tag * YOffset)), animated: true)
        return true
        
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newStr: String = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let expression: String = "^([0-9]*)(\\.([0-9]+)?)?$"
        let regex = try? NSRegularExpression(pattern: expression, options: .caseInsensitive)
        let noOfMatches: Int = (regex?.numberOfMatches(in: newStr, options: [], range: NSRange(location: 0, length: (newStr.characters.count ))))!
        if noOfMatches == 0 {
            return false
        }
        return true
    }
    
    
    // TODO: - ACTION METHODS
    @IBAction func btnHomeClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)

        let homeVC = TabBarVC(nibName: "TabBarVC", bundle: nil)
        homeVC.selectedIndexOfMyTabbarController = 0
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _  = self.navigationController?.popViewController(animated: true)
    }

    
    // TODO: - POST DATA METHODS
    
    
}
