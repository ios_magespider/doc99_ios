//
//  MapOrderDetailVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 31/03/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class MapOrderDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource, PayPalPaymentDelegate {

    //MARK:- IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var viewRequiredDoc: UIView!
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var viewSaveButton: UIView!
    @IBOutlet weak var btnReviewOrder: UIButton!
    @IBOutlet weak var btnMakePayment: UIButton!
    
    @IBOutlet var viewDetail: UIView!
    @IBOutlet weak var labelOrderStatus: UILabel!
    
    @IBOutlet weak var viewOrderStatus: UIView!
    @IBOutlet weak var labelOrderId: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var labelStatusDescription: UILabel!
    
    @IBOutlet weak var labelProduct: UILabel!
    @IBOutlet weak var tableProductList: UITableView!
    
    @IBOutlet weak var viewAmount: UIView!
    @IBOutlet weak var labelSubtTotal: UILabel!
    @IBOutlet weak var labelSubtTotalValue: UILabel!
    
    @IBOutlet weak var labelDiscount: UILabel!
    @IBOutlet weak var labelDiscountValue: UILabel!
    
    @IBOutlet weak var labelDeliveryCharge: UILabel!
    @IBOutlet weak var labelDeliveryChargeValue: UILabel!
    
    @IBOutlet weak var labelHandlingCharge: UILabel!
    @IBOutlet weak var labelHandlingChargeValue: UILabel!
    
    @IBOutlet weak var labelServiceTax: UILabel!
    @IBOutlet weak var labelServiceTaxValue: UILabel!
    
    @IBOutlet weak var labelTotalAmount: UILabel!
    @IBOutlet weak var labelTotalAmountValue: UILabel!
    
    //MARK:- Variables
    var mapId : String = ""
    var orderId : String = ""
    var mapDetail = NSDictionary()
    
    
    // PAYPAL INTEGRATION
    var payPalConfig = PayPalConfiguration() // default
    var environment:String = PayPalEnvironmentSandbox {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewSettings()
        self.adjustViewHeight()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK:-
    func generalViewSettings() {
        titleLabel.text = orderId
        labelOrderId.text = orderId

        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            self.postDataOnWebserviceForGetMapDetail()
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:
                NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        mainScrollView .addSubview(viewDetail)
        
        //orderReviewCell
        tableProductList.register(UINib(nibName: "MapOrderReviewCell", bundle: nil), forCellReuseIdentifier: "orderReviewCell")
        
    }
    
    func adjustViewHeight() {
        tableProductList.frame = CGRect(x: 0, y: tableProductList.frame.origin.y, width: ScreenSize.SCREEN_WIDTH, height: tableProductList.contentSize.height)
        tableProductList.isScrollEnabled = false
        
        viewAmount.frame = CGRect(x: 0, y: tableProductList.frame.origin.y + tableProductList.contentSize.height, width: ScreenSize.SCREEN_WIDTH, height: viewAmount.frame.size.height)
        
        viewDetail.frame = CGRect(x: 0, y: viewDetail.frame.origin.y, width: viewDetail.frame.size.width, height: viewAmount.frame.origin.y + viewAmount.frame.size.height + 8)
        
        mainScrollView.contentSize = CGSize(width: ScreenSize.SCREEN_WIDTH, height: viewDetail.frame.size.height)
        
    }
    
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let loginAlert = UIAlertController(title: messageT, message: title, preferredStyle:.alert)
        let actionOK = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        let actionLogin = UIAlertAction(title: "LOGIN", style: .default) { (actionLogin) in
            APPDELEGATE.logOutUserWithoutToken()
            let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        
        
        loginAlert.addAction(actionOK)
        loginAlert.addAction(actionLogin)
        
        if !(self.presentedViewController != nil) {
            self.present(loginAlert, animated: true, completion: nil)
        }
    }
    
    
    
    //MARK:- IBActions
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnReviewOrderClicked(_ sender: Any) {
        let review = MapOrderVC(nibName: "MapOrderVC", bundle: nil)
        review.mapFormData = mapDetail
        review.isOrderReview = true
        review.orderId = orderId
        self.navigationController?.pushViewController(review, animated: true)
        
    }
    @IBAction func btnMakePaymentClicked(_ sender: Any) {
        if mapDetail.count > 0 {
            
            if Reachability.isConnectedToNetwork() == true {
                settingsPaypalMethod(myAmount: (mapDetail.value(forKey: "total_payment")as? String)!)
            } else {
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }
    }
    
    
    
    //MARK:- PayPalPayment Delegate METHODS
    func settingsPaypalMethod(myAmount:String){
        
        let subtotal = NSDecimalNumber(string: myAmount)
        
        let payment = PayPalPayment()
        
        
        payment.amount = subtotal;
        payment.currencyCode = "USD";
        payment.shortDescription = "Order payment";
        payment.paymentDetails = nil; // if not including payment details, then leave payment.paymentDetails as nil
        
        
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            showAlert(Appname, title: "Payment not possible.")
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
            print("Payment not processalbe: \(payment)")
        }
        
    }
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        
        self.sendCompletedPaymentToServer(completedPayment: completedPayment) //Payment was processed successfully send to server for verification and fulfillment
        
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            
        })
    }
    
    func sendCompletedPaymentToServer(completedPayment:PayPalPayment) {
        
        print("Here is your proof of payment:\n\n \(completedPayment.confirmation) \n\nSend this to your server for confirmation and fulfillment")
        
        let dict = NSMutableDictionary(dictionary: completedPayment.confirmation)
        print("Data : ",dict)
        
        /*
         transactionID = (dict.value(forKey: "response") as AnyObject).value(forKey: "id") as! String
         */
        if Reachability.isConnectedToNetwork() == true {
            
            SVProgressHUD.show(withStatus: "Loading..")
            self.postDataMakePayment(dict: dict)
            
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
    }
    
    
    
    
    
    
    
    
    //MARK:- TableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return APPDELEGATE.arrayMAPOredrDrug.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "orderReviewCell") as? MapOrderReviewCell
        
        if cell == nil {
            let nib  = Bundle.main.loadNibNamed("MapOrderReviewCell", owner: self, options: nil)
            cell = nib?[0] as? MapOrderReviewCell
        }
        
        
        cell?.labelDrugName.text = (APPDELEGATE.arrayMAPOredrDrug.object(at: indexPath.row) as? NSDictionary)?.value(forKey: "product_name") as? String ?? ""
        cell?.labelMg.text = "\((APPDELEGATE.arrayMAPOredrDrug.object(at: indexPath.row) as? NSDictionary)?.value(forKey: "weight") as? String ?? "0" + " mg")"
        cell?.labelPackageInfo.text = (APPDELEGATE.arrayMAPOredrDrug.object(at: indexPath.row) as? NSDictionary)?.value(forKey: "product_desc") as? String ?? ""
        cell?.labelQuantity.text = "\((APPDELEGATE.arrayMAPOredrDrug.object(at: indexPath.row) as? NSDictionary)?.value(forKey: "qty") as? String ?? "") qty"
        
        cell?.labelPrice.text = "HK$ \((APPDELEGATE.arrayMAPOredrDrug.object(at: indexPath.row) as? NSDictionary)?.value(forKey: "price") as? String ?? "")"
        
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        adjustViewHeight()
    }
    
    
    
    //MARK:- Web service call methods
    func postDataOnWebserviceForGetMapDetail(){
        
        let completeURL = NSString(format:"%@%@", MainURL,getMapFormDetailUrl) as String
        
        var params:NSDictionary!
        
        let parameter = NSMutableDictionary()
        parameter.setValue(USERDEFAULT.value(forKey: "userID") as? String ?? "", forKey: "user_id")
        parameter.setValue(USERDEFAULT.value(forKey: "token") as? String ?? "", forKey: "token")
        parameter.setValue(Language_Type, forKey: "lang_type")
        parameter.setValue(mapId, forKey: "map_id")
        
        params = parameter.mutableCopy() as! NSDictionary
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("get Map Detail URL :",completeURL)
        print("get Map Detail Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getMapFormDetailUrlTag)
    }

    func postDataMakePayment(dict : NSMutableDictionary) {
        //makePaymentUrl
        
        let completeURL = NSString(format:"%@%@", MainURL,makePaymentUrl) as String
        
        var params:NSDictionary!
        
        let parameter = NSMutableDictionary()
        parameter.setValue(USERDEFAULT.value(forKey: "userID") as? String ?? "", forKey: "user_id")
        parameter.setValue(USERDEFAULT.value(forKey: "token") as? String ?? "", forKey: "token")
        parameter.setValue(Language_Type, forKey: "lang_type")
        parameter.setValue(mapId, forKey: "map_id")
        parameter.setValue(dict, forKey: "payment_info")
        
        params = parameter.mutableCopy() as! NSDictionary
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("Make Payment URL :",completeURL)
        print("Make Payment Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: makePaymentUrlTag)
    }
    
    //MARK: - Web Service call back Methods
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case getMapFormDetailUrlTag:
            
            let resultDict = responseObject as! NSDictionary;
            print("Get Single map detail Response  : \(resultDict)")
            SVProgressHUD.dismiss()
            if resultDict.value(forKey: "status") as! String == "1"{
                mapDetail = resultDict.value(forKey: "data") as! NSDictionary
                
                if mapDetail.value(forKey: "document_required")as? String == "1" {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.mainScrollView.frame = CGRect(x: 0, y: self.mainScrollView.frame.origin.y+41, width: self.mainScrollView.frame.size.width, height: self.mainScrollView.frame.size.height - 42)
                    })
                }
                
                
                if mapDetail.value(forKey: "map_status")as? String == "1" {
                    labelStatus.text = "Processing"
                }
                else if mapDetail.value(forKey: "map_status")as? String == "2" {
                    labelStatus.text = "Waiting for payment"
                }
                else if mapDetail.value(forKey: "map_status")as? String == "3" {
                    labelStatus.text = "cancelled"
                }
                else if mapDetail.value(forKey: "map_status")as? String == "4" {
                    labelStatus.text = "Completed"
                }
                else{
                    labelStatus.text = ""
                }
                
                
                labelSubtTotalValue.text = mapDetail.value(forKey: "sub_total")as? String
                labelDiscountValue.text = mapDetail.value(forKey: "discount")as? String
                labelDeliveryChargeValue.text = mapDetail.value(forKey: "delivery_charge")as? String
                labelHandlingChargeValue.text = mapDetail.value(forKey: "handling_charge")as? String
                labelServiceTaxValue.text = mapDetail.value(forKey: "vat")as? String
                labelTotalAmountValue.text = mapDetail.value(forKey: "total_payment")as? String
                
                if mapDetail.value(forKey: "map_status")as? String == "2"{
                    if mapDetail.value(forKey: "total_payment")as? String == "0" {
                        btnMakePayment.isUserInteractionEnabled = false
                        btnMakePayment.backgroundColor = UIColor.lightGray
                    }
                }
                else{
                    btnMakePayment.isUserInteractionEnabled = false
                    btnMakePayment.backgroundColor = UIColor.lightGray
                }
                
                APPDELEGATE.arrayMAPOredrDrug = (mapDetail.value(forKey: "products") as! NSArray).mutableCopy() as! NSMutableArray
                tableProductList.reloadData()
                adjustViewHeight()
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        case makePaymentUrlTag:
            let resultDict = responseObject as! NSDictionary;
            print("Make payment Response  : \(resultDict)")
            SVProgressHUD.dismiss()
            if resultDict.value(forKey: "status") as! String == "1"{
                showAlert(Appname, title: "Payment success..")
                self.navigationController?.popViewController(animated: true)
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        default:
            break
            
        }
        
    }
    
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
            
        case getMapFormDetailUrlTag:
            SVProgressHUD.dismiss()
            break
        
        case makePaymentUrlTag:
            SVProgressHUD.dismiss()
            break
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""),
                  title: NSLocalizedString(FailureAlert,tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
}
