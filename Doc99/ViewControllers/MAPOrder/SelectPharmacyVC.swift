//
//  SelectPharmacyVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 26/03/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class SelectPharmacyVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    //MARK:- IBOutlets
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var tablePharmacyList: UITableView!
    @IBOutlet weak var labelPharmaNotFound: UILabel!
    @IBOutlet weak var btnSubmitForm: UIButton!
    
    
    @IBOutlet weak var viewConfirmCredit: UIView!
    @IBOutlet weak var labelCreditCaption: UILabel!
    @IBOutlet weak var btnCancelCredit: UIButton!
    @IBOutlet weak var btnConfirmCredit: UIButton!
    
    //MARK:- Variables
    var arrayDetail = NSMutableDictionary()
    
    var pageNum:Int!
    var isLoading:Bool?
    
    var arrayPharmaList = NSMutableArray()
    var arrayBool = NSMutableArray()
    
    var prevSelection : Int! = 0
    var currentSelection : Int! = 0
    var storeSelected = false
    
     
    var timer: Timer? = nil
    
    var confirmTag = 101
    var buyTag = 102
    
    
    var storeId : String = ""
    var storeOwnerId : String = ""
    
    var mapId : String = ""
    
    
    
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.generalViewSetting()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- Custom Methods
    
    func generalViewSetting() {
        
        pageNum = 1
        if Reachability.isConnectedToNetwork() {
            self.addLoadingIndicatiorOnFooterOnTableView()
            self.postDataOnWebserviceForGetPharmacyList()
        }
        else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }

        tablePharmacyList.register(UINib(nibName: "PharmacyListCell", bundle: nil), forCellReuseIdentifier: "pharmacyCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(postDataCheckUserCredit), name: Notification.Name("CheckUserCredit"), object: nil)
        
    }
    
    func addLoadingIndicatiorOnFooterOnTableView(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tablePharmacyList.tableFooterView = spinner
    }
    
    func removeLoadingIndicatiorOnFooterOnTableView(){
        tablePharmacyList.tableFooterView = nil
    }
    
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let loginAlert = UIAlertController(title: messageT, message: title, preferredStyle:.alert)
        let actionOK = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        let actionLogin = UIAlertAction(title: "LOGIN", style: .default) { (actionLogin) in
            APPDELEGATE.logOutUserWithoutToken()
            let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        
        loginAlert.addAction(actionOK)
        loginAlert.addAction(actionLogin)
        
        if !(self.presentedViewController != nil) {
            self.present(loginAlert, animated: true, completion: nil)
        }
    }
    
    //MARK:- IBActions
    @IBAction func btnBackClicked(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnSubmitClicked(_ sender: Any) {
        
        if arrayBool.count > 0 {
            if storeSelected{
                //Check for available credit
                self.postDataCheckUserCredit()
            }
            else{
                showAlert(Appname, title: "Please select pharmacy.")
            }
        }
        else{
            showAlert(Appname, title: "Please select pharmacy.")
        }
        
        
    }
    
    @IBAction func btnCancelClicked(_ sender: Any) {
        UIView.animate(withDuration: 0.3) {
            self.viewConfirmCredit.frame = CGRect(x: 0, y: ScreenSize.SCREEN_HEIGHT, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        }
    }
    
    @IBAction func btnConfirmClicked(_ sender: Any) {
        if btnConfirmCredit.tag == confirmTag {
            
            if Reachability.isConnectedToNetwork(){
                //self.postDataSendMapOrder()
                SVProgressHUD.show(withStatus: "Loading..")
                UIView.animate(withDuration: 0.3) {
                    self.viewConfirmCredit.frame = CGRect(x: 0, y: ScreenSize.SCREEN_HEIGHT, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
                }
                self.postDataUseCredit()
                
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:
                    NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
        }
        else if btnConfirmCredit.tag == buyTag {
            UIView.animate(withDuration: 0.3) {
                self.viewConfirmCredit.frame = CGRect(x: 0, y: ScreenSize.SCREEN_HEIGHT, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
            }
            let buyCredit = UserCreditVC(nibName: "UserCreditVC", bundle: nil)
            buyCredit.isFromSettings = false
            self.navigationController?.pushViewController(buyCredit, animated: true)
        }
        
        /*
        UIView.animate(withDuration: 0.3) {
            self.viewConfirmCredit.frame = CGRect(x: 0, y: ScreenSize.SCREEN_HEIGHT, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        }
        */
    }
    
    @IBAction func cellBtnSelectClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        if arrayBool.count > sender.tag {
            prevSelection = currentSelection
            currentSelection = sender.tag
            
            arrayBool.replaceObject(at: prevSelection, with: "0")
            arrayBool.replaceObject(at: currentSelection, with: "1")
            
            if prevSelection == currentSelection{
                tablePharmacyList.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
            }
            else{
                tablePharmacyList.reloadRows(at: [IndexPath(row: prevSelection, section: 0),
                                                  IndexPath(row: currentSelection, section: 0)], with: .none)
            }
            
            storeId = (arrayPharmaList[sender.tag]as? NSDictionary)?.value(forKey: "store_id") as! String
            storeOwnerId = (arrayPharmaList[sender.tag]as? NSDictionary)?.value(forKey: "store_owner_id") as! String
            
            storeSelected = true
        }
    }
    
    
    //MARK:- Textfield Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .done
        {
            self.view.endEditing(true)
        }
        
        return true
    }
    
    @IBAction func searchFilter(){
        
        tablePharmacyList.isHidden = false
        labelPharmaNotFound.isHidden = true
        
        if arrayPharmaList.count > 0 && arrayBool.count > 0
        {
            arrayBool.removeAllObjects()
            arrayPharmaList.removeAllObjects()
            tablePharmacyList.reloadData()
        }
        
        if (txtSearch.text == "") {
            if Reachability.isConnectedToNetwork() == true {
                pageNum = 1
                self.addLoadingIndicatiorOnFooterOnTableView()

                Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.postDataOnWebserviceForGetPharmacyList), userInfo: nil, repeats: false)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:
                    NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }else{
            
            if Reachability.isConnectedToNetwork() == true {
                pageNum = 1
                
                self.addLoadingIndicatiorOnFooterOnTableView()
                
                timer?.invalidate()
                timer = Timer.scheduledTimer(
                    timeInterval: 2,
                    target: self,
                    selector: #selector(self.postDataOnWebserviceForGetPharmacyList),
                    userInfo: nil,
                    repeats: false)
                
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
        }
        
    }
    
    //MARK:- Table View Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayPharmaList.count
    }
    
    
    //MARK:- ScrollView Delegate Methdos
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
        if scrollView == tablePharmacyList {
            if isLoading == true{
                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                    pageNum = pageNum + 1
                    print(pageNum)
                    isLoading = false
                    
                    if Reachability.isConnectedToNetwork() == true {
                        self.addLoadingIndicatiorOnFooterOnTableView()
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetPharmacyList), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "pharmacyCell") as? PharmacyListCell
        
        if cell == nil {
            let nib  = Bundle.main.loadNibNamed("PharmacyListCell", owner: self, options: nil)
            cell = nib?[0] as? PharmacyListCell
        }
        
        cell?.labelPharmaName.text = (arrayPharmaList[indexPath.row] as! NSDictionary).value(forKey: "store_name") as? String ?? ""
        
        
        if arrayBool[indexPath.row] as! String == "0"{
            cell?.imgSelection.image = UIImage(named: "check_gray.png")
        }
        else{
            cell?.imgSelection.image = UIImage(named: "check_blue.png")
        }
        
        cell?.btnSelection.tag = indexPath.row
        cell?.btnSelection.addTarget(self, action: #selector(cellBtnSelectClicked(_:)), for: .touchUpInside)
        
        cell?.selectionStyle = .none
        return cell!
    }
    
    
    //MARK:- Web Service Methods
    
    func postDataOnWebserviceForGetPharmacyList(){
        storeSelected = false
        
        let completeURL = NSString(format:"%@%@", MainURL,getPharmacyListUrl) as String

        let params:NSMutableDictionary = [
            "lang_type":Language_Type
        ]
        if !((txtSearch.text?.isEmpty)!) {
            params.setValue(txtSearch.text!, forKey: "keyword")
        }
        let finalParams:NSDictionary = [
            "data" :params
        ]
        
        print("Get Pharmacy URL :",completeURL)
        print("Get Pharmacy Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getPharmacyListUrlTag)
    }
    
    func postDataCheckUserCredit(){
        SVProgressHUD.show(withStatus: "Loading...")
        let completeURL = NSString(format:"%@%@", MainURL,checkCreditUrl) as String
        
        let finalParams:NSDictionary = [
            "data" :["lang_type":Language_Type,
                     "user_id":USERDEFAULT.value(forKey: "userID") as? String ?? "",
                     "token":USERDEFAULT.value(forKey: "token") as? String ?? ""
            ]
        ]
        
        print("Check Credit URL :",completeURL)
        print("Check Credit Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: checkCreditUrlTag)
    }
    
    func postDataUseCredit(){
        
        let completeURL = NSString(format:"%@%@", MainURL,useCreditUrl) as String
        
        let finalParams:NSDictionary = [
            "data" :["lang_type":Language_Type,
                     "user_id":USERDEFAULT.value(forKey: "userID") as? String ?? "",
                     "token":USERDEFAULT.value(forKey: "token") as? String ?? ""
            ]
        ]
        
        print("Use Credit URL :",completeURL)
        print("Use Credit Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: useCreditUrlTag)
        
    }
    
    func postDataSendMapOrder() {
        var completeURL = ""
        
        if !(mapId .isEmpty) {
            completeURL = NSString(format:"%@%@", MainURL,editSavedMapUrl) as String
            self.arrayDetail.setValue(mapId, forKey: "map_id")
        }
        else{
            
            completeURL = NSString(format:"%@%@", MainURL,saveSendMAPOrderUrl) as String
        }
        
        self.arrayDetail.setValue(storeId, forKey: "store_id")
        self.arrayDetail.setValue(storeOwnerId, forKey: "store_owner_id")

        let finalParams:NSDictionary = [
            "data" : self.arrayDetail
        ]
        print("Send MAP Order URL :",completeURL)
        print("Send MAP Order Requets :- \(finalParams)")
        
        //make new request from array detail and make API call for submit MAP Order
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: saveSendMAPOrderUrlTag)
    }
    
    
    //MARK:- Web Service Callback Methods
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case getPharmacyListUrlTag:
            let resultDict = responseObject as! NSDictionary;
            //print("Get Pharmacy List List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()

                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    arrayPharmaList.addObjects(from: myData as! [Any])
                    print("Get Pharma Response  : \(arrayPharmaList)")
                    
                  
                    for _ in 0...myData.count - 1 {
                        self.arrayBool.add("0")
                    }
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum > 0) {
                            self.pageNum = self.pageNum - 1
                        }
                        self.isLoading = false
                    }else{
                        self.isLoading = true
                    }
                }
                else{
                    self.isLoading = false
                    if (self.pageNum > 0) {
                        self.pageNum = self.pageNum - 1
                    }
                    
                    if arrayPharmaList.count == 0{
                        tablePharmacyList.isHidden = true
                        labelPharmaNotFound.isHidden = false
                    }
                    else{
                        tablePharmacyList.isHidden = false
                        labelPharmaNotFound.isHidden = true
                    }
                }
                self.tablePharmacyList.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break

        case checkCreditUrlTag:
            let resultDict = responseObject as! NSDictionary;
            SVProgressHUD.dismiss()
            
            print("Chcek Credit response -> \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                
                btnConfirmCredit.tag = confirmTag
                btnConfirmCredit.setTitle("CONFIRM", for: .normal)
                labelCreditCaption.text = resultDict.value(forKey: "message") as? String
                
                UIView.animate(withDuration: 0.3) {
                    self.viewConfirmCredit.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
                }
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                SVProgressHUD.dismiss()
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            else{
                btnConfirmCredit.tag = buyTag
                btnConfirmCredit.setTitle("BUY", for: .normal)
                
                labelCreditCaption.text = resultDict.value(forKey: "message") as? String
                
                UIView.animate(withDuration: 0.3) {
                    self.viewConfirmCredit.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
                }
            }
            
            break

        case useCreditUrlTag:
            
            let resultDict = responseObject as! NSDictionary;
            print("Use Credit response -> \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                //call API For Save MAP
                self.postDataSendMapOrder()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                SVProgressHUD.dismiss()
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                SVProgressHUD.dismiss()
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        case saveSendMAPOrderUrlTag:
            let resultDict = responseObject as! NSDictionary;
            print("Send Map order Response  : \(resultDict)")
            
            SVProgressHUD.dismiss()
            
            if resultDict.value(forKey: "status") as! String == "1"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
                
                NotificationCenter.default.post(name: NSNotification.Name("reloadSavedMapList"), object: nil)
                
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
                self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true);
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        default:
            break
            
        }
    }
    
    
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case getPharmacyListUrlTag:
            self.isLoading = false
            if (self.pageNum > 0) {
                self.pageNum = self.pageNum - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
            
        case checkCreditUrlTag:
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
}
