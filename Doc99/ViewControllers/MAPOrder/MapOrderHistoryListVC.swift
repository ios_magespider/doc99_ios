//
//  MapOrderHistoryListVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 31/03/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit

class MapOrderHistoryListVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK:- IBOutlets
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableHistoryList: UITableView!
    @IBOutlet weak var labelNoDataFound: UILabel!
    
    //MARK:- Variables
    var arrayMapFormList = NSMutableArray()
    var pageNum:Int!
    var isLoading:Bool?
    var timer: Timer? = nil
    
    var selectedMapFormId = ""
    var deleteMapFormIndex : Int!
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewSettings()
        
        
        tableHistoryList.register(UINib(nibName: "SubmittedMapListCell", bundle: nil), forCellReuseIdentifier: "submittedMapCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:-
    func generalViewSettings() {
        pageNum = 1
        if Reachability.isConnectedToNetwork() == true {
            self.postDataOnWebserviceForMapFormList()
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
    }
    
    func addLoadingIndicatiorOnFooterOnTableView(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableHistoryList.tableFooterView = spinner
    }
    
    func removeLoadingIndicatiorOnFooterOnTableView(){
        tableHistoryList.tableFooterView = nil
    }
    
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let loginAlert = UIAlertController(title: messageT, message: title, preferredStyle:.alert)
        let actionOK = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        let actionLogin = UIAlertAction(title: "LOGIN", style: .default) { (actionLogin) in
            APPDELEGATE.logOutUserWithoutToken()
            let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        
        
        loginAlert.addAction(actionOK)
        loginAlert.addAction(actionLogin)
        
        if !(self.presentedViewController != nil) {
            self.present(loginAlert, animated: true, completion: nil)
        }
        
    }
    
    
    //MARK:- IBActions
    
    @IBAction func btnBackClicked(_ sender: Any) {
        APPDELEGATE.myTabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cellBtnPdfClicked(_ sender: UIButton) {
        
    }
    
    @IBAction func cellBtnShareClicked(_ sender: UIButton) {
        
    }
    
    
    //MARK:- ScrollView Delegate Methdos
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
        if scrollView == tableHistoryList {
            if isLoading == true{
                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                    pageNum = pageNum + 1
                    print(pageNum)
                    isLoading = false
                    
                    if Reachability.isConnectedToNetwork() == true {
                        self.addLoadingIndicatiorOnFooterOnTableView()
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForMapFormList), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
            }
        }
    }
    
    //MARK:- Table view delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMapFormList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "submittedMapCell") as? SubmittedMapListCell
        
        if cell == nil {
            let nib  = Bundle.main.loadNibNamed("SubmittedMapListCell", owner: self, options: nil)
            cell = nib?[0] as? SubmittedMapListCell
        }
        
        cell?.labelId.text = (arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "map_id_display") as? String ?? ""
        cell?.labelDate.text = Utility.getDateFrom((arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "created_at") as? String, givenFormat: "yyyy-MM-dd hh:mm:ss", returnFormat: "dd MMM yyyy")
        
        if (arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "map_status")as? String == "1" {
            cell?.labelStatus.text = "Processing"
        }
        else if (arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "map_status")as? String == "2" {
            cell?.labelStatus.text = "Waiting for payment"
        }
        else if (arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "map_status")as? String == "3" {
            cell?.labelStatus.text = "cancelled"
        }
        else if (arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "map_status")as? String == "4" {
            cell?.labelStatus.text = "Completed"
        }
        else{
            cell?.labelStatus.text = ""
        }
        
        cell?.btnPdf.isHidden = true
        
        
        cell?.btnShare.tag = indexPath.row
//        cell?.btnShare.setImage(UIImage(named: "pdf.png"), for: .normal)
        cell?.btnShare.addTarget(self, action: #selector(cellBtnPdfClicked(_:)), for: .touchUpInside)
        
        cell?.btnDelete.tag = indexPath.row
//        cell?.btnDelete.setImage(UIImage(named: "share_map.png"), for: .normal)
        cell?.btnDelete.addTarget(self, action: #selector(cellBtnShareClicked(_:)), for: .touchUpInside)
        
        if (arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "document_required")as? String == "1" {
            cell?.viewRequired.isHidden = false
        }
        else{
            cell?.viewRequired.isHidden = true
        }
        
        
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let history = MapOrderDetailVC(nibName: "MapOrderDetailVC", bundle: nil)
        history.orderId = (arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "map_id_display")as! String
        history.mapId = (arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "map_id")as? String ?? ""
        
        self.navigationController?.pushViewController(history, animated: true)
        
    }
    
    //MARK:- Web Service Call Methods
    
    func postDataOnWebserviceForMapFormList(){
        self.addLoadingIndicatiorOnFooterOnTableView()
        
        let completeURL = NSString(format:"%@%@", MainURL,getMapHistoryListUrl) as String
        
        let pageNumber = "\(pageNum!)"
        
        var params:NSDictionary!
        
        let parameter = NSMutableDictionary()
        parameter.setValue(USERDEFAULT.value(forKey: "userID") as? String ?? "", forKey: "user_id")
        parameter.setValue(USERDEFAULT.value(forKey: "token") as? String ?? "", forKey: "token")
        parameter.setValue(Language_Type, forKey: "lang_type")
        parameter.setValue(pageNumber, forKey: "page")
        parameter.setValue(PAGINATION_LIMITE, forKey: "limit")
        
        
        params = parameter.mutableCopy() as! NSDictionary
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("Get map history URL :",completeURL)
        print("Get map history Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getMapHistoryListUrlTag)
    }
    
    
    //MARK:- Web Service Callback Methods
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
            
        case getMapHistoryListUrlTag:
            let resultDict = responseObject as! NSDictionary;
                        print("Saved map List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum == 1{
                    self.arrayMapFormList = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    arrayMapFormList.addObjects(from: myData as! [Any])
                    print("Map history List Response  : \(arrayMapFormList)")
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum > 0) {
                            self.pageNum = self.pageNum - 1
                        }
                        self.isLoading = false
                    }else{
                        self.isLoading = true
                    }
                }
                else{
                    self.isLoading = false
                    if (self.pageNum > 0) {
                        self.pageNum = self.pageNum - 1
                    }
                    
                    if arrayMapFormList.count == 0{
                        tableHistoryList.isHidden = true
                        labelNoDataFound.isHidden = false
                    }
                    else{
                        tableHistoryList.isHidden = false
                        labelNoDataFound.isHidden = true
                    }
                }
                
                tableHistoryList.reloadData()
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break

        default:
            break
            
        }
    }
    
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case getMapHistoryListUrlTag:
            self.isLoading = false
            if (self.pageNum > 0) {
                self.pageNum = self.pageNum - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
        
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
}
