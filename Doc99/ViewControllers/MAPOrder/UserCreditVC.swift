//
//  UserCreditVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 27/03/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class UserCreditVC: UIViewController, UITableViewDataSource, UITableViewDelegate, PayPalPaymentDelegate {
    
    //MARK:- IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var viewRemainingCredit: UIView!
    @IBOutlet weak var labelUserCreditValue: UILabel!
    @IBOutlet weak var labelRemaining: UILabel!
    
    @IBOutlet weak var viewCreditPlan: UIView!
    @IBOutlet weak var tableCreditPlan: UITableView!
    @IBOutlet weak var planTableIndicator: UIActivityIndicatorView!
    @IBOutlet weak var labelPlanNotFound: UILabel!
    
    @IBOutlet weak var btnBecomeMember: UIButton!
    @IBOutlet weak var creditViewHeight: NSLayoutConstraint!
    
    //MARK:- Variables
    var arrayCreditPlans = NSArray()
    
    var ucp_id : String!
    var amount : String!
    var credit : String!
    
    var isFromSettings = false
    
    //MARK:-
    
    
    // PAYPAL INTEGRATION
    var payPalConfig = PayPalConfiguration() // default
    var environment:String = PayPalEnvironmentSandbox {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.generalViewSettings()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK:- Custom Methods
    
    func generalViewSettings() {
        
        if !isFromSettings {
            viewCreditPlan.frame = CGRect(x: viewCreditPlan.frame.origin.x, y: viewRemainingCredit.frame.origin.y, width: viewCreditPlan.frame.size.width, height: viewCreditPlan.frame.size.height)
            creditViewHeight.constant = 0
            
            viewRemainingCredit.isHidden = true
        }
        
        if Reachability.isConnectedToNetwork() {
            planTableIndicator.isHidden = false
            planTableIndicator.startAnimating()
            self.postDataCheckUserCredit()
            self.postDataGetCreditPlans()
        }
        else{
             showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
    }
    
    
    //MARK:- IBActions
    @IBAction func btnBackClicked(_ sender: Any) {
        if isFromSettings {
            APPDELEGATE.myTabBarController?.tabBar.isHidden = false
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnMemberClicked(_ sender: Any) {
        
    }
    
    @IBAction func cellBtnBuyClicked(_ sender: Any) {
     
        print("selected plan amount = \((arrayCreditPlans[(sender as AnyObject).tag] as? NSDictionary)?.value(forKey: "amount") as? String ?? "")")
        
        ucp_id = (arrayCreditPlans[(sender as AnyObject).tag] as? NSDictionary)?.value(forKey: "ucp_id") as? String
        amount = (arrayCreditPlans[(sender as AnyObject).tag] as? NSDictionary)?.value(forKey: "amount") as? String
        credit = (arrayCreditPlans[(sender as AnyObject).tag] as? NSDictionary)?.value(forKey: "credit") as? String
        
        self.settingsPaypalMethod(myAmount: amount!)
    }
    
    //MARK:- PayPalPayment Delegate METHODS
    func settingsPaypalMethod(myAmount:String){
        
        let subtotal = NSDecimalNumber(string: myAmount)
        
        let payment = PayPalPayment()
        
        
        payment.amount = subtotal;
        payment.currencyCode = "USD";
        payment.shortDescription = "Buy Credit";
        payment.paymentDetails = nil; // if not including payment details, then leave payment.paymentDetails as nil
        
        
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
            print("Payment not processalbe: \(payment)")
        }
        
    }
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        
        self.sendCompletedPaymentToServer(completedPayment: completedPayment) //Payment was processed successfully send to server for verification and fulfillment
        
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            
        })
    }
    
    func sendCompletedPaymentToServer(completedPayment:PayPalPayment) {
        
        print("Here is your proof of payment:\n\n \(completedPayment.confirmation) \n\nSend this to your server for confirmation and fulfillment")
        
        let dict = NSMutableDictionary(dictionary: completedPayment.confirmation)
        print("Data : ",dict)
        
        /*
        transactionID = (dict.value(forKey: "response") as AnyObject).value(forKey: "id") as! String
        */
        if Reachability.isConnectedToNetwork() == true {
            
            SVProgressHUD.show(withStatus: "Loading..")
            self.postDataAddCreditPlan(dict: dict)
            
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
    }
    
    
    
    
    //MARK:- Table view Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCreditPlans.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "creditCell") as? BuyCreditCell
        
        if cell == nil {
            let nib  = Bundle.main.loadNibNamed("BuyCreditCell", owner: self, options: nil)
            cell = nib?[0] as? BuyCreditCell
        }
        
        
        cell?.labelPlanName.text = "\((arrayCreditPlans[indexPath.row] as! NSDictionary).value(forKey: "credit") as? String ?? "") Credit = $\((arrayCreditPlans[indexPath.row] as! NSDictionary).value(forKey: "amount") as? String ?? "")"
        
        cell?.labelBuy.clipsToBounds = true
        cell?.labelBuy.layer.cornerRadius = 3.0
        
        
        cell?.btnBuy.tag = indexPath.row
        cell?.btnBuy.addTarget(self, action: #selector(cellBtnBuyClicked(_:)), for: .touchUpInside)
        
        return cell!
    }
    
    
    
    //MARK:- Web Service Call Methods
    func postDataGetCreditPlans() {
        SVProgressHUD.show(withStatus: "Loading..")
        let completeURL = NSString(format:"%@%@", MainURL,getCreditPlansUrl) as String
        
        let finalParams:NSDictionary = [
            "data" :["lang_type":Language_Type]
        ]
        
        print("Get Credit Plan URL :",completeURL)
        print("Get Credit Plan Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getCreditPlansUrlTag)
    }
    
    func postDataAddCreditPlan(dict : NSMutableDictionary) {
        let completeURL = NSString(format:"%@%@", MainURL,addCreditPlanUrl) as String
        /*
         {
         "data":{
         "lang_type":"1",
         "user_id":"3",
         "token":"aQD0jXbe0qIcv4xBfdIBARnmOQEE8k6mYLk0s8hGnBJqCqiGo9MZKivYgZYDD9P39Lc05L1Ra4UVbXJ0R3VMFT4WlnOSDQmNMQXEVEwtcBJB43BGm5UQqaHu",
         "ucp_id":"1",
         "credit":"15",
         "payment_info":{ "amount":"1500","transaction_id":"564984115"}
         }
         }
         */
        let finalParams:NSDictionary = [
            "data" :["lang_type":Language_Type,
                     "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
                     "token": USERDEFAULT.value(forKey: "token") as! String,
                     "ucp_id":ucp_id!,
                     "credit":credit!,
                     "payment_info" : dict
            ]
        ]
        
        print("Add Credit Plan URL :",completeURL)
        print("Add Credit Plan Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: addCreditPlanUrltag)
    }
    
    func postDataCheckUserCredit(){
        
        let completeURL = NSString(format:"%@%@", MainURL,checkCreditUrl) as String
        
        let finalParams:NSDictionary = [
            "data" :["lang_type":Language_Type,
                     "user_id":USERDEFAULT.value(forKey: "userID") as? String ?? "",
                     "token":USERDEFAULT.value(forKey: "token") as? String ?? ""
            ]
        ]
        
        print("Check Credit URL :",completeURL)
        print("Check Credit Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: checkCreditUrlTag)
    }
    
    
    
    //MARK:- Web Service Callback Methods
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case getCreditPlansUrlTag:
            let resultDict = responseObject as! NSDictionary;
            SVProgressHUD.dismiss()
            
            print("get Credit plan response -> \(resultDict)")
            
            planTableIndicator.stopAnimating()
            planTableIndicator.isHidden = true
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                arrayCreditPlans = resultDict.value(forKey: "data")as! NSArray
                if arrayCreditPlans.count > 0 {
                    labelPlanNotFound.isHidden = true
                    tableCreditPlan.reloadData()
                }
                else{
                    labelPlanNotFound.isHidden = false
                }
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            break
            
        case addCreditPlanUrltag:
            let resultDict = responseObject as! NSDictionary;
            SVProgressHUD.dismiss()
            
            print("ADD Credit plan response -> \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                // Credit add success..
                
                if isFromSettings {
                    self.postDataCheckUserCredit()
                }
                else{
                    showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
                    NotificationCenter.default.post(name: NSNotification.Name("CheckUserCredit"), object: nil)
                    
                    self.navigationController?.popViewController(animated: true)
                }
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            break
            
            
        case checkCreditUrlTag:
            let resultDict = responseObject as! NSDictionary;
            SVProgressHUD.dismiss()
            
            
            
            if resultDict.value(forKey: "status") as! String == "1"{
                let temp = (resultDict.value(forKey: "data")as! NSDictionary).mutableCopy() as! NSMutableDictionary
                temp.setValue("xyz", forKey: "XYZ")
                
                print("Chcek Credit response -> \(temp)")
                
                labelUserCreditValue.text = "\(temp.value(forKey: "credit")as! Int) Cr."
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                SVProgressHUD.dismiss()
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                labelUserCreditValue.text = "0 Cr."
            }
            
            break
            
        default:
            break
        }
    }
    
    
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case getCreditPlansUrlTag:
            
            planTableIndicator.stopAnimating()
            planTableIndicator.isHidden = true
            break
            
        case addCreditPlanUrltag:
            break
        case checkCreditUrlTag:
            labelUserCreditValue.text = ""
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
 
    
    //MARK:- OTHER
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }
    
    
}
