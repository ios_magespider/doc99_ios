//
//  UserOrderListVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 02/04/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

// For user_type 2(Pharmacy), To see list of orders made by normal user to pharmacy.



import UIKit
import SVProgressHUD

class UserOrderListVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIDocumentInteractionControllerDelegate {
    
    //MARK:- IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var labelNoDataFound: UILabel!
    @IBOutlet weak var tableFormList: UITableView!
    
    //MARK:- Variables
    var arrayMapFormList = NSMutableArray()
    var pageNum:Int!
    var isLoading:Bool?
    var timer: Timer? = nil
    
    var selectedMapFormId = ""
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.generalViewSettings()
        
        tableFormList.register(UINib(nibName: "SubmittedMapListCell", bundle: nil), forCellReuseIdentifier: "submittedMapCell")
        NotificationCenter.default.addObserver(self, selector: #selector(generalViewSettings), name: NSNotification.Name("refreshUserOrderList"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:-
    func generalViewSettings() {
        pageNum = 1
        
        if Reachability.isConnectedToNetwork() == true {
            self.postDataOnWebserviceGetUserOrderList()
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
    }
    
    func addLoadingIndicatiorOnFooterOnTableView(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableFormList.tableFooterView = spinner
    }
    
    func removeLoadingIndicatiorOnFooterOnTableView(){
        tableFormList.tableFooterView = nil
    }
    
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let loginAlert = UIAlertController(title: messageT, message: title, preferredStyle:.alert)
        let actionOK = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        let actionLogin = UIAlertAction(title: "LOGIN", style: .default) { (actionLogin) in
            APPDELEGATE.logOutUserWithoutToken()
            let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        
        loginAlert.addAction(actionOK)
        loginAlert.addAction(actionLogin)
        
        if !(self.presentedViewController != nil) {
            self.present(loginAlert, animated: true, completion: nil)
        }
        
    }
    
    //MARK:- IBActions
    @IBAction func btnBackClicked(_ sender: Any) {
        
        APPDELEGATE.myTabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cellBtnPdfClicked(_ sender: UIButton) {
        let mapID = (arrayMapFormList[sender.tag]as! NSDictionary).value(forKey: "map_id") as? String ?? ""
        let name = (arrayMapFormList[sender.tag]as! NSDictionary).value(forKey: "map_id_display") as? String ?? ""
        let userID = USERDEFAULT.value(forKey: "userID")as? String ?? ""
        
        if !(mapID.isEmpty) && !(name.isEmpty) && !(userID.isEmpty){
            self.downloadPdf(id: mapID, fileName: name,userID: userID)
        }
        else{
            showAlert(Appname, title: "Can't download file.")
            print("Error while downloading file..cellBtnPdfClicked")
        }
    }
    
    
    
    //MARK:- Table view Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMapFormList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "submittedMapCell") as? SubmittedMapListCell
        
        if cell == nil {
            let nib  = Bundle.main.loadNibNamed("SubmittedMapListCell", owner: self, options: nil)
            cell = nib?[0] as? SubmittedMapListCell
        }
        
        cell?.labelId.text = (arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "map_id_display") as? String ?? ""
        cell?.labelDate.text = Utility.getDateFrom((arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "created_at") as? String, givenFormat: "yyyy-MM-dd hh:mm:ss", returnFormat: "dd MMM yyyy")
        
        if (arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "map_status")as? String == "1" {
            cell?.labelStatus.text = "Processing"
        }
        else if (arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "map_status")as? String == "2" {
            cell?.labelStatus.text = "Waiting for payment"
        }
        else if (arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "map_status")as? String == "3" {
            cell?.labelStatus.text = "cancelled"
        }
        else if (arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "map_status")as? String == "4" {
            cell?.labelStatus.text = "Completed"
        }
        else{
            cell?.labelStatus.text = ""
        }
        
        
        if (arrayMapFormList[indexPath.row] as! NSDictionary).value(forKey: "document_required")as? String == "1" {
            cell?.viewRequired.isHidden = false
        }
        else{
            cell?.viewRequired.isHidden = true
        }
        
        cell?.btnDelete.tag = indexPath.row
        cell?.btnDelete.setImage(UIImage(named: "pdf.png"), for: .normal)
        cell?.btnDelete.addTarget(self, action: #selector(cellBtnPdfClicked(_:)), for: .touchUpInside)
        
        cell?.btnPdf.isHidden = true
        cell?.btnShare.isHidden = true
        
        //cell?.viewRequired.isHidden = true
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let review = UserOrderDetailVC(nibName: "UserOrderDetailVC", bundle: nil)
        review.orderId = (arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "map_id_display") as? String ?? ""
        review.mapId = (arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "map_id") as? String ?? ""
        self.navigationController?.pushViewController(review, animated: true)
    }
    
    
    
    //MARK:- Web Service Call Methods
    
    func postDataOnWebserviceGetUserOrderList(){
        self.addLoadingIndicatiorOnFooterOnTableView()
        
        let completeURL = NSString(format:"%@%@", MainURL,getUserOrderListUrl) as String
        
        let pageNumber = "\(pageNum!)"
        
        var params:NSDictionary!
        
        let parameter = NSMutableDictionary()
        parameter.setValue(USERDEFAULT.value(forKey: "userID") as? String ?? "", forKey: "user_id")
        parameter.setValue(USERDEFAULT.value(forKey: "token") as? String ?? "", forKey: "token")
        parameter.setValue(Language_Type, forKey: "lang_type")
        parameter.setValue(pageNumber, forKey: "page")
        parameter.setValue(PAGINATION_LIMITE, forKey: "limit")
        
        
        params = parameter.mutableCopy() as! NSDictionary
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("Get User order URL :",completeURL)
        print("Get User order Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getUserOrderListUrlTag)
    }
    
    
    //MARK:- Web Service Callback Methods
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
            
        case getUserOrderListUrlTag:
            
            let resultDict = responseObject as! NSDictionary;
            print("User Order List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum == 1{
                    self.arrayMapFormList = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    arrayMapFormList.addObjects(from: myData as! [Any])
                    //print("User Order List Response  : \(arrayMapFormList)")
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum > 0) {
                            self.pageNum = self.pageNum - 1
                        }
                        self.isLoading = false
                    }else{
                        self.isLoading = true
                    }
                }
                else{
                    self.isLoading = false
                    if (self.pageNum > 0) {
                        self.pageNum = self.pageNum - 1
                    }
                    
                    if arrayMapFormList.count == 0{
                        tableFormList.isHidden = true
                        labelNoDataFound.isHidden = false
                    }
                    else{
                        tableFormList.isHidden = false
                        labelNoDataFound.isHidden = true
                    }
                }
                
                tableFormList.reloadData()
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            
            break;
            
        default:
            break
        }
    }
 
    
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case getUserOrderListUrlTag:
            self.isLoading = false
            if (self.pageNum > 0) {
                self.pageNum = self.pageNum - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    
    
    //MARK:-
    func downloadPdf(id:String, fileName:String, userID : String){
        // Create destination URL
        
        let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last as URL?)!
        let destinationFileUrl = documentsUrl.appendingPathComponent(fileName+".pdf")
        
        //Create URL to the source file you want to download
        
        let strUrl = SingleMapPDFurl+id+"&user_id="+userID
        let fileURL = URL(string: strUrl)
        
        print("file url -> \(String(describing: fileURL))")
        
        let isFileFound:Bool? = FileManager.default.fileExists(atPath: destinationFileUrl.path)
        if isFileFound == true
        {
            let viewer = UIDocumentInteractionController(url: URL(fileURLWithPath: destinationFileUrl.path))
            viewer.delegate = self
            viewer.presentPreview(animated: true)
        }
        else{
            SVProgressHUD.show(withStatus: "Downloading...")
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig)
            let request = URLRequest(url:fileURL!)
            
            let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
                if let tempLocalUrl = tempLocalUrl, error == nil {
                    // Success
                    if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                        SVProgressHUD.dismiss()
                        print("Successfully downloaded. Status code: \(statusCode)")
                    }
                    
                    do
                    {
                        if(FileManager.default.fileExists(atPath: destinationFileUrl.path))
                        {
                            try FileManager.default.removeItem(at: destinationFileUrl)
                            try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                            SVProgressHUD.dismiss()
                            self.showFileWithPath(path: destinationFileUrl.path)
                        }
                        else
                        {
                            try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                            self.showFileWithPath(path: destinationFileUrl.path)
                            SVProgressHUD.dismiss()
                        }
                    }
                    catch (let writeError)
                    {
                        print("Error creating a file \(destinationFileUrl) : \(writeError)")
                    }
                    
                } else {
                    SVProgressHUD.dismiss()
                    print("Error took place while downloading a file. Error description: %@", error?.localizedDescription ?? "Errorr.....");
                }
            }
            task.resume()
        }
    }
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    
    func showFileWithPath(path: String)
    {
        let isFileFound:Bool? = FileManager.default.fileExists(atPath: path)
        if isFileFound == true
        {
            let viewer = UIDocumentInteractionController(url: URL(fileURLWithPath: path))
            viewer.delegate = self
            viewer.presentPreview(animated: true)
        }
    }
    
}
