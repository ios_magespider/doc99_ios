//
//  MapOrderVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 23/03/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class MapOrderVC: UIViewController, UIScrollViewDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- Outlets
    
    @IBOutlet weak var scrollViewMain: UIScrollView!
    @IBOutlet weak var viewForButtonSave: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var btnSendToPharmacy: UIButton!
    
    @IBOutlet var viewDetailForm: UIView!
    
    //--      Section-A Outlets      --//
    @IBOutlet weak var labelSectionA: UILabel!
    @IBOutlet weak var textViewNote: UITextView!
    @IBOutlet weak var viewSectionA: UIView!
    
    @IBOutlet weak var labelDeliveryDate: UILabel!
    @IBOutlet weak var btnDeliveryDate: UIButton!
    @IBOutlet weak var txtDeliveryDate: UITextField!
    
    @IBOutlet weak var labelPackaging: UILabel!
    @IBOutlet weak var txtPackaging: UITextField!
    
    @IBOutlet weak var labelBatchNo: UILabel!
    @IBOutlet weak var txtBatchNo: UITextField!
    
    @IBOutlet weak var labelInBoundAWB: UILabel!
    @IBOutlet weak var txtInBoundAWB: UITextField!
    
    @IBOutlet weak var labelOutBoundAWB: UILabel!
    @IBOutlet weak var txtOutBoundAWB: UITextField!
    
    @IBOutlet weak var labelCourierReferance: UILabel!
    @IBOutlet weak var txtCourierReferance: UITextField!
    
    @IBOutlet weak var labelConsignee: UILabel!
    @IBOutlet weak var txtConsignee: UITextField!
    
    @IBOutlet weak var labelTrackingNo: UILabel!
    @IBOutlet weak var txtTrackingNo: UITextField!
    
    @IBOutlet weak var labelCourier: UILabel!
    @IBOutlet weak var txtCourier: UITextField!
    
    //--      Product Outlets      --//
    @IBOutlet weak var labelProduct: UILabel!
    @IBOutlet weak var viewProduct: UIView!
    
    @IBOutlet weak var viewAddProduct: UIView!
    @IBOutlet weak var btnAddProduct: UIButton!
    @IBOutlet weak var labelSearchProduct: UILabel!
    @IBOutlet weak var txtSearchProduct: UITextField!
    
    @IBOutlet weak var tableProductCart: UITableView!
    
    //--      Section-B Outlets      --//
    @IBOutlet weak var labelSectionB: UILabel!
    @IBOutlet weak var viewSectionB: UIView!
    
    @IBOutlet weak var labelPurchaserName: UILabel!
    @IBOutlet weak var txtPurchaserName: UITextField!
    
    @IBOutlet weak var labelPurchaserDate: UILabel!
    @IBOutlet weak var btnPurchaserDate: UIButton!
    @IBOutlet weak var txtPurchaserDate: UITextField!
    
    @IBOutlet weak var labelPurchaserAddress: UILabel!
    @IBOutlet weak var txtPurchaserAddress: UITextField!
    
    @IBOutlet weak var labelDeliveryAddress: UILabel!
    @IBOutlet weak var txtDeliveryAddress: UITextField!
    
    @IBOutlet weak var labelShipperName: UILabel!
    @IBOutlet weak var txtShipperName: UITextField!
    
    @IBOutlet weak var labelShipperAddress: UILabel!
    @IBOutlet weak var txtShipperAddress: UITextField!
    
    @IBOutlet weak var labelShipperDate: UILabel!
    @IBOutlet weak var btnShipperDate: UIButton!
    @IBOutlet weak var txtShipperDate: UITextField!
    
    @IBOutlet weak var labelSupplyDate: UILabel!
    @IBOutlet weak var btnSupplyDateClicked: UIButton!
    @IBOutlet weak var txtSupplyDate: UITextField!
    
    @IBOutlet weak var lblTobecompletedShipperContactName: UILabel!
    @IBOutlet weak var lblTobecompletedShipperAddress: UILabel!
    @IBOutlet weak var lblTobecompletedShipperDate: UILabel!
    @IBOutlet weak var lblTobecompletedSupplyDate: UILabel!
    
    
    //--      Contact Outlets      --//
    @IBOutlet weak var labelContact: UILabel!
    @IBOutlet weak var viewContactDetail: UIView!
    @IBOutlet weak var labelContactName: UILabel!
    @IBOutlet weak var txtViewContactAddress: UITextView!
    
    //--      Date Picker Outlets      --//
    @IBOutlet weak var viewDatePicker: UIView!
    @IBOutlet weak var labelDateType: UILabel!
    @IBOutlet weak var btnCancelDatePicker: UIButton!
    @IBOutlet weak var btnDoneDatePicker: UIButton!
    @IBOutlet weak var myDatePicker: UIDatePicker!
    
    //--      Search Drug Table Outlets      --//
    @IBOutlet var viewDrugList: UIView!
    @IBOutlet weak var tableDrugList: UITableView!
    @IBOutlet weak var labelDrugNotFound: UILabel!
    
    
    @IBOutlet weak var btnViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnViewFrameHeight: NSLayoutConstraint!
    
    
    //MARK:- Variables
    var arrayDrugList = NSMutableArray()
    var pageNum:Int!
    var isLoading:Bool?
    var timer: Timer? = nil
    
    var isFromSettings:Bool?
    var editMapId : String = ""
    var isOrderReview = false
    
    var mapFormData = NSDictionary()
    var orderId : String = ""
    
    var purchaseDate : String = ""
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isOrderReview {
            
            btnViewHeight.constant = 0
            titleLabel.text = orderId
            
            //set data in form
            self.setFromData(mapDetail: mapFormData)
            viewSectionA.isUserInteractionEnabled = false
            viewSectionB.isUserInteractionEnabled = false
        }
        else{
            print("aaaa --> \(btnViewFrameHeight.constant)")
            btnViewHeight.constant = viewForButtonSave.frame.size.height
        }
        
        self.viewDetailForm.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: self.viewDetailForm.frame.size.height)
        self.scrollViewMain .addSubview(viewDetailForm)
        self.scrollViewMain.contentSize = CGSize(width: ScreenSize.SCREEN_WIDTH, height: viewDetailForm.frame.size.height)
        
        if USERDEFAULT.value(forKey: "user_type")as! String == "3" || USERDEFAULT.value(forKey: "user_type")as! String == "2" || USERDEFAULT.value(forKey: "user_type")as! String == "1"{
            viewSectionA.isUserInteractionEnabled = false
            viewSectionA.isHidden = true
            
            labelProduct.frame = CGRect(x: labelProduct.frame.origin.x, y: viewSectionA.frame.origin.y, width: labelProduct.frame.size.width,
                                        height: labelProduct.frame.size.height)
            
            viewProduct.frame = CGRect(x: 0, y: labelProduct.frame.origin.y + labelProduct.frame.size.height + 6,
                                       width: ScreenSize.SCREEN_WIDTH,
                                       height: self.tableProductCart.frame.origin.y + self.tableProductCart.frame.size.height)
            
            
            txtPurchaserName.text = USERDEFAULT.value(forKey: "fullName")as? String ?? ""
            
            txtShipperName.isUserInteractionEnabled = false
            txtShipperAddress.isUserInteractionEnabled = false
            
            btnShipperDate.isEnabled = false
            txtShipperDate.isUserInteractionEnabled = false
            
            btnSupplyDateClicked.isEnabled = false
            txtSupplyDate.isUserInteractionEnabled = false
            
            txtDeliveryAddress.returnKeyType = .done
        }
        
        self.generalViewSettings()
        
        purchaseDate = Utility.getStringFrom(myDatePicker.date, format: "yyyy-MM-dd hh:mm:ss")
        txtPurchaserDate.text = getStringDateFromDate(dateFormat: "MM/dd/yyyy", enterDate: myDatePicker.date)
        
        if !(editMapId .isEmpty) {
            
            if Reachability.isConnectedToNetwork() == true {
                SVProgressHUD.show(withStatus: "Loading..")
                self.postDataOnWebserviceForGetMapDetail()
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:
                    NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }
        
       
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK:- Custom Methods
//    override func updateViewConstraints() {
//        super.updateViewConstraints()
//    }
    
    func generalViewSettings() {
        
        myDatePicker.minimumDate = NSDate() as Date
        myDatePicker.datePickerMode = .date;
        
        self.viewDatePicker.frame = CGRect(x: 0,
                                           y: ScreenSize.SCREEN_HEIGHT,
                                           width: self.viewDatePicker.frame.size.width,
                                           height: self.viewDatePicker.frame.size.height)
        
        if isOrderReview {
            tableProductCart.register(UINib.init(nibName: "MapOrderReviewCell", bundle: nil), forCellReuseIdentifier: "orderReviewCell")
        }
        else{
            tableProductCart.register(UINib.init(nibName: "MAPOrderProductCartCell", bundle: nil), forCellReuseIdentifier: "CellIdentifier")
        }
        tableProductCart.delegate = self
        tableProductCart.dataSource = self
        
        self.adjustViewHeight()
        
        if(APPDELEGATE.arrayMAPOredrDrug.count > 0){
            tableProductCart.reloadData()
            self.adjustViewHeight()
        }
        
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
    }
    
    func setFromData(mapDetail: NSDictionary) {
        
        txtDeliveryDate.text = Utility.getDateFrom(mapDetail.value(forKey: "sa_delievry_date")as? String, givenFormat: "yyyy-MM-dd HH:mm:ss", returnFormat: "MM/dd/yyyy") ?? ""
        
        txtPackaging.text = mapDetail.value(forKey: "sa_packaging")as? String
        txtBatchNo.text = mapDetail.value(forKey: "sa_batch_no")as? String
        txtInBoundAWB.text = mapDetail.value(forKey: "sa_inbound_hk_awbno")as? String
        txtOutBoundAWB.text = mapDetail.value(forKey: "sa_outbound_hk_date")as? String
        txtCourierReferance.text = mapDetail.value(forKey: "sa_courier_ref")as? String
        txtConsignee.text = mapDetail.value(forKey: "sa_consignee_airport")as? String
        txtTrackingNo.text = mapDetail.value(forKey: "sa_tracking_number")as? String
        txtCourier.text = mapDetail.value(forKey: "sa_courier")as? String
        
        txtPurchaserName.text = mapDetail.value(forKey: "sb_p_name")as? String
        txtPurchaserDate.text = Utility.getDateFrom(mapDetail.value(forKey: "sb_p_date")as? String, givenFormat: "yyyy-MM-dd HH:mm:ss", returnFormat: "MM/dd/yyyy")
        
        //let date = mapDetail.value(forKey: "sb_p_date")as? String ?? ""
        purchaseDate = Utility.getDateFrom(mapDetail.value(forKey: "sb_p_date")as? String, givenFormat: "yyyy-MM-dd HH:mm:ss", returnFormat: "yyyy-MM-dd hh:mm:ss") ?? ""
        
        //print("old date = \(purchaseDate)")
        
        txtPurchaserAddress.text = mapDetail.value(forKey: "sb_p_address")as? String
        txtDeliveryAddress.text = mapDetail.value(forKey: "sb_p_delivery_address")as? String
        txtShipperName.text = mapDetail.value(forKey: "sb_s_contact_name")as? String
        txtShipperAddress.text = mapDetail.value(forKey: "sb_s_address")as? String
        
        txtShipperDate.text = Utility.getDateFrom(mapDetail.value(forKey: "sb_s_date")as? String, givenFormat: "yyyy-MM-dd HH:mm:ss", returnFormat: "MM/dd/yyyy") ?? ""
        
        txtSupplyDate.text = Utility.getDateFrom(mapDetail.value(forKey: "sb_supply_date")as? String, givenFormat: "yyyy-MM-dd HH:mm:ss", returnFormat: "MM/dd/yyyy") ?? ""
        
        
        APPDELEGATE.arrayMAPOredrDrug = (mapDetail.value(forKey: "products") as! NSArray).mutableCopy() as! NSMutableArray
        tableProductCart.reloadData()
        adjustViewHeight()
        
    }
    
    func adjustViewHeight(){
        
        if isOrderReview {
            viewAddProduct.isHidden = true
            //viewAddProduct.frame = CGRect(x: 0, y: viewAddProduct.frame.origin.y, width: ScreenSize.SCREEN_WIDTH, height: 0)
            tableProductCart.frame = CGRect(x: 0, y: viewAddProduct.frame.origin.y,
                                            width: self.tableProductCart.frame.size.width,
                                            height: self.tableProductCart.contentSize.height)
        }
        
        tableProductCart .isScrollEnabled = false
        
        tableProductCart.frame = CGRect(x: 0, y: self.tableProductCart.frame.origin.y,
                                        width: self.tableProductCart.frame.size.width,
                                        height: self.tableProductCart.contentSize.height)
        
        viewProduct.frame = CGRect(x: 0, y: self.viewProduct.frame.origin.y,
                                   width: ScreenSize.SCREEN_WIDTH,
                                   height: self.tableProductCart.frame.origin.y + self.tableProductCart.frame.size.height)
        
        
        labelSectionB.frame = CGRect(x: labelSectionB.frame.origin.x, y: viewProduct.frame.origin.y+viewProduct.frame.size.height + 16,
                                     width: labelSectionB.frame.size.width, height: labelSectionB.frame.size.height)
        
        viewSectionB.frame = CGRect(x: viewSectionB.frame.origin.x, y:labelSectionB.frame.origin.y+labelSectionB.frame.size.height + 4,
                                    width: viewSectionB.frame.size.width, height: viewSectionB.frame.size.height)
        
        
        labelContact.frame = CGRect(x: labelContact.frame.origin.x, y: viewSectionB.frame.origin.y+viewSectionB.frame.size.height + 16,
                                    width: labelContact.frame.size.width, height: labelContact.frame.size.height)
        
        viewContactDetail.frame = CGRect(x: viewContactDetail.frame.origin.x, y:labelContact.frame.origin.y+labelContact.frame.size.height + 4,
                                         width: viewContactDetail.frame.size.width, height: viewContactDetail.frame.size.height)
        
        viewDetailForm.frame = CGRect(x: 0, y: 0,
                                      width: self.viewDetailForm.frame.size.width, height: viewContactDetail.frame.origin.y + viewContactDetail.frame.size.height + 15)
        
        self.scrollViewMain.contentSize = CGSize(width: ScreenSize.SCREEN_WIDTH, height: viewDetailForm.frame.size.height)
        
//        print("Table height -> \(self.tableProductCart.contentSize.height)")
//        print("superview height -> \(viewProduct.frame.size.height)")
    }
    
    func addLoadingIndicatiorOnFooterOnTableView(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableDrugList.tableFooterView = spinner
    }
    
    func removeLoadingIndicatiorOnFooterOnTableView(){
        tableDrugList.tableFooterView = nil
    }
    
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let loginAlert = UIAlertController(title: messageT, message: title, preferredStyle:.alert)
        let actionOK = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        let actionLogin = UIAlertAction(title: "LOGIN", style: .default) { (actionLogin) in
            APPDELEGATE.logOutUserWithoutToken()
            let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        
        
        loginAlert.addAction(actionOK)
        loginAlert.addAction(actionLogin)
        
        if !(self.presentedViewController != nil) {
            self.present(loginAlert, animated: true, completion: nil)
        }
    }
    /*
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
        }
    }
    */
    //MARK:- Table Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableDrugList{
            return arrayDrugList.count
        }
        else{
            return APPDELEGATE.arrayMAPOredrDrug .count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableDrugList {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "cellIdentifier")
            
            cell.textLabel?.text = (arrayDrugList .object(at: indexPath.row) as? NSDictionary)?.value(forKey: "drug_name") as? String ?? ""
            cell.selectionStyle = .none
            return cell
        }
        else{
            
            
            if isOrderReview {
                //tableProductCart.register(UINib.init(nibName: "MapOrderReviewCell", bundle: nil), forCellReuseIdentifier: "orderReviewCell")
                var cell = tableView.dequeueReusableCell(withIdentifier: "orderReviewCell") as? MapOrderReviewCell
                
                if cell == nil {
                    let nib  = Bundle.main.loadNibNamed("MapOrderReviewCell", owner: self, options: nil)
                    cell = nib?[0] as? MapOrderReviewCell
                }
                
                cell?.labelDrugName.text = (APPDELEGATE.arrayMAPOredrDrug.object(at: indexPath.row) as? NSDictionary)?.value(forKey: "product_name") as? String ?? ""
                cell?.labelMg.text = "\((APPDELEGATE.arrayMAPOredrDrug.object(at: indexPath.row) as? NSDictionary)?.value(forKey: "weight") as? String ?? "0" + " mg")"
                cell?.labelPackageInfo.text = (APPDELEGATE.arrayMAPOredrDrug.object(at: indexPath.row) as? NSDictionary)?.value(forKey: "product_desc") as? String ?? ""
                cell?.labelQuantity.text = "\((APPDELEGATE.arrayMAPOredrDrug.object(at: indexPath.row) as? NSDictionary)?.value(forKey: "qty") as? String ?? "") qty"
                
                cell?.labelPrice.isHidden = true
                
                cell!.selectionStyle = .none;
                return cell!
            }
            else{
                //tableProductCart.register(UINib.init(nibName: "MAPOrderProductCartCell", bundle: nil), forCellReuseIdentifier: "CellIdentifier")
                var cell = tableView.dequeueReusableCell(withIdentifier: "CellIdentifier") as? MAPOrderProductCartCell
                
                if cell == nil {
                    let nib  = Bundle.main.loadNibNamed("MAPOrderProductCartCell", owner: self, options: nil)
                    cell = nib?[0] as? MAPOrderProductCartCell
                }
                
                cell?.labelDrugName.text = (APPDELEGATE.arrayMAPOredrDrug.object(at: indexPath.row) as? NSDictionary)?.value(forKey: "product_name") as? String ?? ""
                cell?.labelMg.text = "\((APPDELEGATE.arrayMAPOredrDrug.object(at: indexPath.row) as? NSDictionary)?.value(forKey: "weight") as? String ?? "0" + " mg")"
                cell?.labelPackageInfo.text = (APPDELEGATE.arrayMAPOredrDrug.object(at: indexPath.row) as? NSDictionary)?.value(forKey: "product_desc") as? String ?? ""
                cell?.labelQuantity.text = (APPDELEGATE.arrayMAPOredrDrug.object(at: indexPath.row) as? NSDictionary)?.value(forKey: "qty") as? String ?? ""
                
                
                if USERDEFAULT.value(forKey: "user_type")as! String == "3" || USERDEFAULT.value(forKey: "user_type")as! String == "2" || USERDEFAULT.value(forKey: "user_type")as! String == "1"{
                    cell?.btnDelete.tag = indexPath.row
                    cell?.btnDelete.addTarget(self, action: #selector(cellBtnDeleteClicked(_:)), for: .touchUpInside)
                    
                    cell?.btnPlus.tag = indexPath.row
                    cell?.btnPlus.addTarget(self, action: #selector(cellBtnIncreaseClicked(_:)), for: .touchUpInside)
                    
                    cell?.btnMinus.tag = indexPath.row
                    cell?.btnMinus.addTarget(self, action: #selector(cellBtnDecreaseClicked(_:)), for: .touchUpInside)
                }
                
                cell!.selectionStyle = .none;
                return cell!
            }
            
            
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableDrugList{
            
            if APPDELEGATE.arrayMAPOredrDrug.count > 0 {
                
                var index = 0
                var found = false
                var i = 0
                
                for temp in APPDELEGATE.arrayMAPOredrDrug{
                    if ((temp as! NSDictionary).value(forKey: "drug_id") as? String) == (arrayDrugList.object(at: indexPath.row) as? NSDictionary)!.value(forKey: "drug_id") as? String{
                        found = true
                        index = i
                        //i = i+1
                        break
                    }
                    else{
                        i = i+1
                        found = false
                    }
                }
                
                
                if found == true{
                    //replace at index
                    print("replacing at index .. \(index)")
                    
                    var qty = Int(((APPDELEGATE.arrayMAPOredrDrug[index] as! NSDictionary).value(forKey: "qty") as? String)!)
                    qty = qty! + 1
                    
                    let x = (APPDELEGATE.arrayMAPOredrDrug[index] as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    x.setValue(String(qty!), forKey: "qty")
                    
                    APPDELEGATE.arrayMAPOredrDrug.replaceObject(at: i, with: x)
                    
                }
                else{
                    print("Created new entry for non-empty array..")
                    
                    let arr : NSDictionary = [
                        "drug_id" : (arrayDrugList.object(at: indexPath.row) as? NSDictionary)!.value(forKey: "drug_id") as? String ?? "",
                        "product_id": "1",
                        "product_name": (arrayDrugList.object(at: indexPath.row) as? NSDictionary)!.value(forKey: "drug_name") as? String ?? "",
                        "drug_description" : (arrayDrugList.object(at: indexPath.row) as? NSDictionary)!.value(forKey: "drug_description") as? String ?? "",
                        "price" : "0",
                        "qty" : "1",
                        "packaging_unit" : "",
                        "m_country" : "",
                        "weight" : (arrayDrugList.object(at: indexPath.row) as? NSDictionary)!.value(forKey: "drug_description") as? String ?? ""
                    ]
                    
                    APPDELEGATE.arrayMAPOredrDrug.add(arr)
                }
            }
            else{
                print("Created new entry for blank array..")
                
                let arr : NSDictionary = [
                    "drug_id" : (arrayDrugList.object(at: indexPath.row) as? NSDictionary)!.value(forKey: "drug_id") as? String ?? "",
                    "product_id": "1",
                    "product_name": (arrayDrugList.object(at: indexPath.row) as? NSDictionary)!.value(forKey: "drug_name") as? String ?? "",
                    "drug_description" : (arrayDrugList.object(at: indexPath.row) as? NSDictionary)!.value(forKey: "drug_description") as? String ?? "",
                    "price" : "0",
                    "qty" : "1",
                    "packaging_unit" : "",
                    "m_country" : "",
                    "weight" : (arrayDrugList.object(at: indexPath.row) as? NSDictionary)!.value(forKey: "drug_description") as? String ?? ""
                    ]
                
                APPDELEGATE.arrayMAPOredrDrug.add(arr)
            }
            
            print("arrayMAPOredrDrug --> \(APPDELEGATE.arrayMAPOredrDrug)")
            
            
            tableProductCart.reloadData()
            self.adjustViewHeight()
            
            self.view .endEditing(true)
            txtSearchProduct.text = ""
            tableDrugList.delegate = nil
            tableDrugList.dataSource = nil
            
            viewDrugList.removeFromSuperview()
            
            tableDrugList.isHidden = false
            if arrayDrugList.count > 0{
                arrayDrugList.removeAllObjects()
            }
            labelDrugNotFound.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == tableProductCart{
            self.adjustViewHeight()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableDrugList {
            return 44
        }
        else{
            return 100
        }
    }
    
    //MARK:- IBActions
    @IBAction func btnBackClicked(_ sender: Any) {
        if isFromSettings == true
        {
            APPDELEGATE.myTabBarController?.tabBar.isHidden = false
        }
        
        if isOrderReview {
            APPDELEGATE.arrayMAPOredrDrug = NSMutableArray()
        }
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnInfoClicked(_ sender: UIButton) {
        let info = MapOrderDescription(nibName: "MapOrderDescription", bundle: nil)
        self.navigationController?.pushViewController(info, animated: true)
    }
    
    @IBAction func btnAddDrugClicked(_ sender: Any) {
        self.view.endEditing(true)
        
        //1. Create the alert controller.
        let alert = UIAlertController(title: "Doc99", message: "Add Unlisted Drug", preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.text = ""
            textField.placeholder = "Enter Drug Name"
        }
        alert.addTextField { (textFieldDesc) in
            textFieldDesc.text = ""
            textFieldDesc.placeholder = "Enter Drug Description"
        }
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "ADD", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            let textDesc = alert?.textFields![1]
            
            print("Name: \(textField?.text ?? "")")
            print("Description: \(textDesc?.text ?? "")")
            
            
            if Utility.isEmpty(textField?.text){
                showAlert(Appname, title: "Please enter drug name.")
            }
            else{
                
                if APPDELEGATE.arrayMAPOredrDrug.count > 0 {
                    
                    var index = 0
                    var found = false
                    var i = 0
                    
                    for temp in APPDELEGATE.arrayMAPOredrDrug{
                        if ((temp as! NSDictionary).value(forKey: "product_name") as! String).lowercased() == textField?.text?.lowercased(){
                            found = true
                            index = i
                            //i = i+1
                            break
                        }
                        else{
                            i = i+1
                            found = false
                        }
                    }
                    
                    
                    if found == true{
                        //replace at index
                        
                        print("replacing at index .. \(index)")
                        
                        var qty = Int(((APPDELEGATE.arrayMAPOredrDrug[index] as! NSDictionary).value(forKey: "qty") as? String)!)
                        qty = qty! + 1
                        
                        let x = (APPDELEGATE.arrayMAPOredrDrug[index] as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        x.setValue(String(qty!), forKey: "qty")
                        
                        APPDELEGATE.arrayMAPOredrDrug.replaceObject(at: i, with: x)
                        
                    }
                    else{
                        print("Created new entry for non-empty array..")
                        
                        let arr : NSDictionary = [
                            "drug_id" : "",
                            "product_id": "1",
                            "product_name": textField?.text ?? "",
                            "product_desc" : textDesc?.text ?? "",
                            "price" : "0",
                            "qty" : "1",
                            "packaging_unit" : "",
                            "m_country" : "",
                            "weight" : "",
                            ]
                        APPDELEGATE.arrayMAPOredrDrug.add(arr)
                    }
                }
                else{
                    print("Created new entry for blank array..")
                    
                    
                    let arr : NSDictionary = [
                        "drug_id" : "",
                        "product_id": "1",
                        "product_name": textField?.text ?? "",
                        "product_desc" : textDesc?.text ?? "",
                        "price" : "0",
                        "qty" : "1",
                        "packaging_unit" : "",
                        "m_country" : "",
                        "weight" : "",
                        ]
                    APPDELEGATE.arrayMAPOredrDrug.add(arr)
                }
                
                print("arrayMAPOredrDrug --> \(APPDELEGATE.arrayMAPOredrDrug)")
                
                
                
                
                self.tableProductCart.reloadData()
                self.adjustViewHeight()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
        
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnDeliveryDateClicked(_ sender: Any) {
        self.view.endEditing(true)
        labelDateType.text = "Select Delivery Date"
        UIView.animate(withDuration: 0.3) {
            self.viewDatePicker.frame = CGRect(x: 0,
                                          y: ScreenSize.SCREEN_HEIGHT - self.viewDatePicker.frame.size.height,
                                          width: self.viewDatePicker.frame.size.width,
                                          height: self.viewDatePicker.frame.size.height)
        }
    }
    
    @IBAction func btnPurchaseDateClicked(_ sender: Any) {
        self.view.endEditing(true)
        labelDateType.text = "Select Purchase Date"
        UIView.animate(withDuration: 0.3) {
            self.viewDatePicker.frame = CGRect(x: 0,
                                               y: ScreenSize.SCREEN_HEIGHT - self.viewDatePicker.frame.size.height,
                                               width: self.viewDatePicker.frame.size.width,
                                               height: self.viewDatePicker.frame.size.height)
        }
    }
    @IBAction func btnShipperDateClicked(_ sender: Any) {
        self.view.endEditing(true)
        labelDateType.text = "Select Shipper Date"
        UIView.animate(withDuration: 0.3) {
            self.viewDatePicker.frame = CGRect(x: 0,
                                               y: ScreenSize.SCREEN_HEIGHT - self.viewDatePicker.frame.size.height,
                                               width: self.viewDatePicker.frame.size.width,
                                               height: self.viewDatePicker.frame.size.height)
        }
    }
    
    @IBAction func btnSupplyDateClicked(_ sender: Any) {
        self.view.endEditing(true)
        labelDateType.text = "Select Supply Date"
        UIView.animate(withDuration: 0.3) {
            self.viewDatePicker.frame = CGRect(x: 0,
                                               y: ScreenSize.SCREEN_HEIGHT - self.viewDatePicker.frame.size.height,
                                               width: self.viewDatePicker.frame.size.width,
                                               height: self.viewDatePicker.frame.size.height)
        }
    }
    
    @IBAction func cellBtnDeleteClicked(_ sender: UIButton) {
        if APPDELEGATE.arrayMAPOredrDrug.count > sender.tag {
            APPDELEGATE.arrayMAPOredrDrug.removeObject(at: sender.tag)
            tableProductCart.reloadData()
            self.adjustViewHeight()
        }
    }
    
    @IBAction func cellBtnIncreaseClicked(_ sender: UIButton) {
        if APPDELEGATE.arrayMAPOredrDrug.count > sender.tag{
            var qty = Int(((APPDELEGATE.arrayMAPOredrDrug[sender.tag] as! NSDictionary).value(forKey: "qty") as? String)!)
            qty = qty! + 1
            
            let x = (APPDELEGATE.arrayMAPOredrDrug[sender.tag] as! NSDictionary).mutableCopy() as! NSMutableDictionary
            x.setValue(String(qty!), forKey: "qty")
            
            APPDELEGATE.arrayMAPOredrDrug.replaceObject(at: sender.tag, with: x)
            
            tableProductCart.reloadRows(at: [NSIndexPath(row: sender.tag, section: 0) as IndexPath], with: .none)
            
        }
    }
    
    @IBAction func cellBtnDecreaseClicked(_ sender: UIButton) {
        var qty = Int(((APPDELEGATE.arrayMAPOredrDrug[sender.tag] as! NSDictionary).value(forKey: "qty") as? String)!)
        if qty! > Int(1) {
            qty = qty! - 1
            
            let x = (APPDELEGATE.arrayMAPOredrDrug[sender.tag] as! NSDictionary).mutableCopy() as! NSMutableDictionary
            x.setValue(String(qty!), forKey: "qty")
            
            APPDELEGATE.arrayMAPOredrDrug.replaceObject(at: sender.tag, with: x)
            
            tableProductCart.reloadRows(at: [NSIndexPath(row: sender.tag, section: 0) as IndexPath], with: .none)
        }
        
    }
    
    @IBAction func btnSaveClicked(_ sender: Any) {
        if Reachability.isConnectedToNetwork() == true {
//            if USERDEFAULT.value(forKey: "user_type")as! String == "3"{
            
                if APPDELEGATE.arrayMAPOredrDrug.count == 0 {
                    showAlert(Appname, title: "Please select drug to save order")
                }
                else if txtPurchaserName.text! .isEmpty
                {
                    showAlert(Appname, title: "Please enter purchaser name")
                }
                else if txtPurchaserDate.text! .isEmpty
                {
                    showAlert(Appname, title: "Please select purchaser date")
                }
                else if txtPurchaserAddress.text! .isEmpty
                {
                    showAlert(Appname, title: "Please enter purchaser address")
                }
                else if txtDeliveryAddress.text! .isEmpty
                {
                    showAlert(Appname, title: "Please enter delivery address")
                }
                else{
                    if (editMapId .isEmpty)
                    {
                        SVProgressHUD.show(withStatus: "Loading..")
                        self.postDataSaveSendMapOrder()
                    }
                    else{
                        // Update Map Form
                        SVProgressHUD.show(withStatus: "Loading..")
                        self.postDataUpdateMapForm()
                    }
                }
//            }
            
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
    }
    
    @IBAction func btnSendToPharmaClicked(_ sender: Any) {
        
//        if USERDEFAULT.value(forKey: "user_type")as! String == "3"{
            if APPDELEGATE.arrayMAPOredrDrug.count == 0 {
                showAlert(Appname, title: "Please select drug to save order")
            }
            else if txtPurchaserName.text! .isEmpty
            {
                showAlert(Appname, title: "Please enter purchaser name")
            }
            else if txtPurchaserDate.text! .isEmpty
            {
                showAlert(Appname, title: "Please select purchaser date")
            }
            else if txtPurchaserAddress.text! .isEmpty
            {
                showAlert(Appname, title: "Please enter purchaser address")
            }
            else if txtDeliveryAddress.text! .isEmpty
            {
                showAlert(Appname, title: "Please enter delivery address")
            }
            else{

                let selectPharma = SelectPharmacyVC(nibName: "SelectPharmacyVC", bundle: nil)
                
                if !(editMapId .isEmpty) {
                    selectPharma.mapId = editMapId
                }
                
                let parameter = NSMutableDictionary()
                parameter.setValue(USERDEFAULT.value(forKey: "userID") as? String ?? "", forKey: "user_id")
                parameter.setValue(USERDEFAULT.value(forKey: "token") as? String ?? "", forKey: "token")
                parameter.setValue(Language_Type, forKey: "lang_type")
                parameter.setValue(txtPurchaserName.text!, forKey: "sb_p_name")
                parameter.setValue(txtPurchaserAddress.text!, forKey: "sb_p_address")
                parameter.setValue(txtDeliveryAddress.text!, forKey: "sb_p_delivery_address")
                parameter.setValue(purchaseDate, forKey: "sb_p_date")
                parameter.setValue(APPDELEGATE.arrayMAPOredrDrug, forKey: "products")
                selectPharma.arrayDetail = parameter
                
                self.navigationController?.pushViewController(selectPharma, animated: true)
            }
//        }
        
    }
    
    
    //MARK:- DatePicker Methods
    @IBAction func btnCancelDateClicked(_ sender: Any) {
        UIView.animate(withDuration: 0.3) {
            self.viewDatePicker.frame = CGRect(x: 0,
                                               y: ScreenSize.SCREEN_HEIGHT,
                                               width: self.viewDatePicker.frame.size.width,
                                               height: self.viewDatePicker.frame.size.height)
        }
    }
    
    @IBAction func btnDoneDateClicked(_ sender: Any) {
        if labelDateType.text == "Select Shipper Date" {
            txtShipperDate.text = getStringDateFromDate(dateFormat: "MM/dd/yyyy", enterDate: myDatePicker.date)
        }
        else if labelDateType.text == "Select Supply Date" {
            txtSupplyDate.text = getStringDateFromDate(dateFormat: "MM/dd/yyyy", enterDate: myDatePicker.date)
        }
        else if labelDateType.text == "Select Delivery Date" {
            txtDeliveryDate.text = getStringDateFromDate(dateFormat: "MM/dd/yyyy", enterDate: myDatePicker.date)
        }
        else if labelDateType.text == "Select Purchase Date" {
            print("Date = \(myDatePicker.date)")
            print("c Date = \(Utility.getStringFrom(myDatePicker.date, format: "yyyy-MM-dd hh:mm:ss"))")
            purchaseDate = Utility.getStringFrom(myDatePicker.date, format: "yyyy-MM-dd hh:mm:ss")
            txtPurchaserDate.text = getStringDateFromDate(dateFormat: "MM/dd/yyyy", enterDate: myDatePicker.date)
        }
        UIView.animate(withDuration: 0.3) {
            self.viewDatePicker.frame = CGRect(x: 0,
                                               y: ScreenSize.SCREEN_HEIGHT,
                                               width: self.viewDatePicker.frame.size.width,
                                               height: self.viewDatePicker.frame.size.height)
        }
    }
    
    //MARK:- ScrollView Dalegate methods
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == tableDrugList {
            if isLoading == true{
                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                    pageNum = pageNum + 1
                    print(pageNum)
                    isLoading = false
                    
                    if Reachability.isConnectedToNetwork() == true {
                        self.addLoadingIndicatiorOnFooterOnTableView()
                        
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetDrugList), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
            }
        }
        
        UIView.animate(withDuration: 0.3) {
            self.viewDatePicker.frame = CGRect(x: 0,
                                               y: ScreenSize.SCREEN_HEIGHT,
                                               width: self.viewDatePicker.frame.size.width,
                                               height: self.viewDatePicker.frame.size.height)
        }
        

    }
    
    //MARK:- TextField delegate methods
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        
        if (textField == txtSearchProduct)
        {
            let frame = scrollViewMain .convert(viewProduct.frame, from: viewDetailForm)
            scrollViewMain.setContentOffset(CGPoint(x: 0, y: frame.origin.y+77), animated: true)
            
            tableDrugList.delegate = self;
            tableDrugList.dataSource = self;
            
            var height:CGFloat = 0
            if ScreenSize.SCREEN_HEIGHT > 568
            {
                height = tableDrugList.frame.size.height + 25 + (tableDrugList.frame.size.height / 2)
            }
            else if DeviceType.IS_IPHONE_4_OR_LESS{
                height = tableDrugList.frame.size.height - 90
            }
            else{
                height = tableDrugList.frame.size.height
            }
            
            
            viewDrugList.frame = CGRect(x: 0, y: viewAddProduct.frame.origin.y + viewAddProduct.frame.size.height, width: ScreenSize.SCREEN_WIDTH, height: height)
            self.view .addSubview(viewDrugList)
            
            //viewDrugList.frame = CGRect(x: 0, y: 108, width: viewDrugList.frame.size.width, height: viewDrugList.frame.size.height)
            
        }
        
        UIView.animate(withDuration: 0.3) {
            self.viewDatePicker.frame = CGRect(x: 0,
                                               y: ScreenSize.SCREEN_HEIGHT,
                                               width: self.viewDatePicker.frame.size.width,
                                               height: self.viewDatePicker.frame.size.height)
        }
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.returnKeyType == .done {
            if(textField == txtSearchProduct){
                textField.text = ""
                
                tableDrugList.delegate = nil
                tableDrugList.dataSource = nil
                
                viewDrugList.removeFromSuperview()
                
                tableDrugList.isHidden = false
                if arrayDrugList.count > 0{
                    arrayDrugList.removeAllObjects()
                }
                labelDrugNotFound.isHidden = true
            }
            textField.resignFirstResponder()
        }
        else{
            let nextTage=textField.tag+1;
            let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
            if (nextResponder != nil){
                nextResponder?.becomeFirstResponder()
            }
            else
            {
                textField.resignFirstResponder()
            }
        }
        return false
    }
 
    @IBAction func searchFilter(){
        
        tableDrugList.isHidden = false
        labelDrugNotFound.isHidden = true
        
        if arrayDrugList.count > 0
        {
            arrayDrugList.removeAllObjects()
            tableDrugList.reloadData()
        }
        
        if (txtSearchProduct.text == "") {
//            pageNum = 1
//            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetDrugList), userInfo: nil, repeats: false)
            
        }else{
            
            if Reachability.isConnectedToNetwork() == true {
                
                pageNum = 1
                
                self.addLoadingIndicatiorOnFooterOnTableView()
                //Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetDrugList), userInfo: nil, repeats: false)
                
                timer?.invalidate()
                timer = Timer.scheduledTimer(
                    timeInterval: 2,
                    target: self,
                    selector: #selector(self.postDataOnWebserviceForGetDrugList),
                    userInfo: nil,
                    repeats: false)
                
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
            //            predicate = String(format: "SELF['drug_name'] contains[c] '%@'", txtSearch.text!)
            //            print(predicate)
            //
            //            let myPredicate:NSPredicate = NSPredicate(format:predicate)
            //            let temp = self.drugGlobalData.mutableCopy() as! NSArray
            //            self.drugData = (temp.filtered(using: myPredicate) as NSArray).mutableCopy() as! NSMutableArray
            
        }
        
    }
    
    
    
    //MARK: - Web Service Methods
    func postDataOnWebserviceForGetMapDetail(){
        
        let completeURL = NSString(format:"%@%@", MainURL,getMapFormDetailUrl) as String
        
        var params:NSDictionary!
        
        let parameter = NSMutableDictionary()
        parameter.setValue(USERDEFAULT.value(forKey: "userID") as? String ?? "", forKey: "user_id")
        parameter.setValue(USERDEFAULT.value(forKey: "token") as? String ?? "", forKey: "token")
        parameter.setValue(Language_Type, forKey: "lang_type")
        parameter.setValue(editMapId, forKey: "map_id")
        
        params = parameter.mutableCopy() as! NSDictionary
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("get Map Detail URL :",completeURL)
        print("get Map Detail Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getMapFormDetailUrlTag)
    }
    
    func postDataOnWebserviceForGetDrugList(){
        self.addLoadingIndicatiorOnFooterOnTableView()
        
        let completeURL = NSString(format:"%@%@", MainURL,getDrugListURL) as String
        
        let pageNumber = "\(pageNum!)"
        
        var params:NSDictionary!
        
        let parameter = NSMutableDictionary()
        parameter.setValue(USERDEFAULT.value(forKey: "userID") as? String ?? "", forKey: "user_id")
        parameter.setValue(USERDEFAULT.value(forKey: "token") as? String ?? "", forKey: "token")
        parameter.setValue(Language_Type, forKey: "lang_type")
        parameter.setValue(pageNumber, forKey: "page")
        parameter.setValue(PAGINATION_LIMITE, forKey: "limit")
        
        
        //        let userInfo = timer.userInfo
        //        print("Hints for textField: \(userInfo)")
        
        if (!(txtSearchProduct.text!) .isEmpty){
            parameter.setValue(txtSearchProduct.text!, forKey: "drug_name")
        }
        
        params = parameter.mutableCopy() as! NSDictionary
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetDrugList API Parameter :",finalParams)
        print("GetDrugList API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getDrugListURLTag)
        
    }
    
    func postDataSaveSendMapOrder() {
        let completeURL = NSString(format:"%@%@", MainURL,saveSendMAPOrderUrl) as String
        
        var params:NSDictionary!
        
        let parameter = NSMutableDictionary()
        parameter.setValue(USERDEFAULT.value(forKey: "userID") as? String ?? "", forKey: "user_id")
        parameter.setValue(USERDEFAULT.value(forKey: "token") as? String ?? "", forKey: "token")
        parameter.setValue(Language_Type, forKey: "lang_type")
        parameter.setValue(txtPurchaserName.text!, forKey: "sb_p_name")
        parameter.setValue(txtPurchaserAddress.text!, forKey: "sb_p_address")
        parameter.setValue(txtDeliveryAddress.text!, forKey: "sb_p_delivery_address")
        parameter.setValue(purchaseDate, forKey: "sb_p_date")
        parameter.setValue(APPDELEGATE.arrayMAPOredrDrug, forKey: "products")
        
        params = parameter.mutableCopy() as! NSDictionary
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("saveSendMAPOrder API Parameter :",finalParams)
        print("saveSendMAPOrder API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: saveSendMAPOrderUrlTag)
    }
    
    func postDataUpdateMapForm() {
        let completeURL = NSString(format:"%@%@", MainURL,editSavedMapUrl) as String
        
        var params:NSDictionary!
        
        let parameter = NSMutableDictionary()
        parameter.setValue(USERDEFAULT.value(forKey: "userID") as? String ?? "", forKey: "user_id")
        parameter.setValue(USERDEFAULT.value(forKey: "token") as? String ?? "", forKey: "token")
        parameter.setValue(Language_Type, forKey: "lang_type")
        parameter.setValue(editMapId, forKey: "map_id")
        
        //Section A Parameters
        /*
        //Pass When user_type = 2(Pharmacy)
        parameter.setValue(txtDeliveryDate.text!, forKey: "sa_delievry_date")
        parameter.setValue(txtPackaging.text!, forKey: "sa_packaging")
        parameter.setValue(txtBatchNo.text!, forKey: "sa_batch_no")
        parameter.setValue(txtInBoundAWB.text!, forKey: "sa_inbound_hk_awbno")
        parameter.setValue(txtOutBoundAWB.text!, forKey: "sa_outbound_hk_date")
        parameter.setValue(txtCourierReferance.text!, forKey: "sa_courier_ref")
        parameter.setValue(txtConsignee.text!, forKey: "sa_consignee_airport")
        parameter.setValue(txtTrackingNo.text!, forKey: "sa_tracking_number")
        parameter.setValue(txtCourier.text!, forKey: "sa_courier")
         */
        //Section B Parameters
        parameter.setValue(txtPurchaserName.text!, forKey: "sb_p_name")
        parameter.setValue(txtPurchaserAddress.text!, forKey: "sb_p_address")
        parameter.setValue(txtDeliveryAddress.text!, forKey: "sb_p_delivery_address")
        parameter.setValue(purchaseDate, forKey: "sb_p_date")
        /*
        //Pass when user_type = 2(Pharmacy)
        parameter.setValue(txtShipperName.text!, forKey: "sb_s_contact_name")
        parameter.setValue(txtShipperAddress.text!, forKey: "sb_s_address")
        parameter.setValue(txtShipperDate.text!, forKey: "sb_s_date")
        parameter.setValue(txtSupplyDate.text!, forKey: "sb_supply_date")
        */
        //Selected Drug
        parameter.setValue(APPDELEGATE.arrayMAPOredrDrug, forKey: "products")
        
        params = parameter.mutableCopy() as! NSDictionary
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("Edit Saved map URL :",completeURL)
        print("Edit Saved map Parameters :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: editSavedMapUrlTag)
    }
    
    
    //MARK: - Web Service call back Methods
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case getDrugListURLTag:
            let resultDict = responseObject as! NSDictionary;
            //print("GetDrugList List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum == 1{
                    self.arrayDrugList = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    arrayDrugList.addObjects(from: myData as! [Any])
                    
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum > 0) {
                            self.pageNum = self.pageNum - 1
                        }
                        self.isLoading = false
                    }else{
                        self.isLoading = true
                    }
                }
                else{
                    self.isLoading = false
                    if (self.pageNum > 0) {
                        self.pageNum = self.pageNum - 1
                    }
                    
                    if arrayDrugList.count == 0{
                        tableDrugList.isHidden = true
                        labelDrugNotFound.isHidden = false
                    }
                    else{
                        tableDrugList.isHidden = false
                        labelDrugNotFound.isHidden = true
                    }
                }
                self.tableDrugList.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableView()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        case saveSendMAPOrderUrlTag:
            let resultDict = responseObject as! NSDictionary;
            print("Save Map order Response  : \(resultDict)")
            
            SVProgressHUD.dismiss()
            if resultDict.value(forKey: "status") as! String == "1"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
                self.navigationController?.popViewController(animated: true)
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
           
        case getMapFormDetailUrlTag:
            
            let resultDict = responseObject as! NSDictionary;
            print("Get Single map detail Response  : \(resultDict)")
            SVProgressHUD.dismiss()
            if resultDict.value(forKey: "status") as! String == "1"{
                
                //set data in form
                self.setFromData(mapDetail: resultDict.value(forKey: "data") as! NSDictionary)
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            
            break
            
        case editSavedMapUrlTag:
            let resultDict = responseObject as! NSDictionary;
            print("Edit saved map detail Response  : \(resultDict)")
            SVProgressHUD.dismiss()
            if resultDict.value(forKey: "status") as! String == "1"{
                APPDELEGATE.arrayMAPOredrDrug = NSMutableArray()
                
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
                self.navigationController?.popViewController(animated: true)
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case getDrugListURLTag:
            self.isLoading = false
            if (self.pageNum > 0) {
                self.pageNum = self.pageNum - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
            
        case saveSendMAPOrderUrlTag:
            SVProgressHUD.dismiss()
            break
        
        case getMapFormDetailUrlTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
}
