//
//  MapOrderDescription.swift
//  Doc99
//
//  Created by MS-MAC-009 on 18/06/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit

class MapOrderDescription: UIViewController {
    @IBOutlet weak var txtViewAbout: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        txtViewAbout.scrollRangeToVisible(NSMakeRange(0, 1))
    }
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
}
