//
//  MapOrderReviewProductCell.swift
//  Doc99
//
//  Created by MS-MAC-009 on 03/04/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit

class MapOrderReviewProductCell: UITableViewCell {

    
    @IBOutlet weak var labelHeader: UILabel!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtDesc: UITextField!
    @IBOutlet weak var txtQuantity: UITextField!
    @IBOutlet weak var txtPackageUnit: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtUnitPrice: UITextField!
    @IBOutlet weak var txtTotalValue: UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
