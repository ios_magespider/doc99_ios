//
//  SellingHistoryCell.swift
//  Doc99
//
//  Created by MS-MAC-009 on 04/04/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit

class SellingHistoryCell: UITableViewCell {

    @IBOutlet weak var labelID: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var labelValue: UILabel!
    @IBOutlet weak var btnPdf: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
