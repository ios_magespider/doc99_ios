//
//  BuyCreditCell.swift
//  Doc99
//
//  Created by MS-MAC-009 on 27/03/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit

class BuyCreditCell: UITableViewCell {
    @IBOutlet weak var btnBuy: UIButton!
    @IBOutlet weak var labelPlanName: UILabel!
    @IBOutlet weak var labelBuy: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
