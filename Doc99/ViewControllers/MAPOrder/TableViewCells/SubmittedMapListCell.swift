//
//  SubmittedMapListCell.swift
//  Doc99
//
//  Created by MS-MAC-009 on 31/03/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit

class SubmittedMapListCell: UITableViewCell {

    @IBOutlet weak var labelId: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnPdf: UIButton!
    
    @IBOutlet weak var viewRequired: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
