//
//  MAPOrderProductCartCell.swift
//  Doc99
//
//  Created by MS-MAC-009 on 24/03/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit

class MAPOrderProductCartCell: UITableViewCell {

    
    @IBOutlet weak var labelDrugName: UILabel!
    @IBOutlet weak var labelMg: UILabel!
    @IBOutlet weak var labelPackageInfo: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var labelQuantity: UILabel!
    
    @IBOutlet weak var viewQuantity: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
