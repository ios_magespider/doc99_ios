//
//  PharmacyListCell.swift
//  Doc99
//
//  Created by MS-MAC-009 on 27/03/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit

class PharmacyListCell: UITableViewCell {

    @IBOutlet weak var imgSelection: UIImageView!
    @IBOutlet weak var labelPharmaName: UILabel!
    @IBOutlet weak var btnSelection: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
