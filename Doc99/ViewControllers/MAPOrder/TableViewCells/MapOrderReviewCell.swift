//
//  MapOrderReviewCell.swift
//  Doc99
//
//  Created by MS-MAC-009 on 31/03/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit

class MapOrderReviewCell: UITableViewCell {

    
    @IBOutlet weak var labelDrugName: UILabel!
    @IBOutlet weak var labelMg: UILabel!
    @IBOutlet weak var labelPackageInfo: UILabel!
    
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelQuantity: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
