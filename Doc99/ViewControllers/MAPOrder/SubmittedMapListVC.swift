//
//  SubmittedMapListVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 31/03/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class SubmittedMapListVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIDocumentInteractionControllerDelegate {

    //MARK:- IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var labelNoDataFound: UILabel!
    @IBOutlet weak var tableFormList: UITableView!
    
    @IBOutlet weak var viewConfirm: UIView!
    
    
    //MARK:- Variables
    var arrayMapFormList = NSMutableArray()
    var pageNum:Int!
    var isLoading:Bool?
    var timer: Timer? = nil
    
    var selectedMapFormId = ""
    var deleteMapFormIndex : Int!
    
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewSettings()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK:-
    func generalViewSettings() {
        self.viewConfirm.frame = CGRect(x: 0, y: ScreenSize.SCREEN_HEIGHT, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        
        pageNum = 1
        if Reachability.isConnectedToNetwork() == true {
            self.postDataOnWebserviceForMapFormList()
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        tableFormList.register(UINib(nibName: "SubmittedMapListCell", bundle: nil), forCellReuseIdentifier: "submittedMapCell")
        
    }
    
    func addLoadingIndicatiorOnFooterOnTableView(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableFormList.tableFooterView = spinner
    }
    
    func removeLoadingIndicatiorOnFooterOnTableView(){
        tableFormList.tableFooterView = nil
    }
    
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let loginAlert = UIAlertController(title: messageT, message: title, preferredStyle:.alert)
        let actionOK = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        let actionLogin = UIAlertAction(title: "LOGIN", style: .default) { (actionLogin) in
            APPDELEGATE.logOutUserWithoutToken()
            let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        
        
        loginAlert.addAction(actionOK)
        loginAlert.addAction(actionLogin)
        
        
        
        if !(self.presentedViewController != nil) {
            self.present(loginAlert, animated: true, completion: nil)
        }
    }
    
    
    //MARK:- IBActions
    
    @IBAction func btnBackClicked(_ sender: Any) {
        
        APPDELEGATE.myTabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func btnCancelClicked(_ sender: Any) {
        UIView.animate(withDuration: 0.3) {
            self.viewConfirm.frame = CGRect(x: 0, y: ScreenSize.SCREEN_HEIGHT, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        }
    }
    
    @IBAction func btnConfirmClicked(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3) {
            self.viewConfirm.frame = CGRect(x: 0, y: ScreenSize.SCREEN_HEIGHT, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        }
        
        if Reachability.isConnectedToNetwork() {
            SVProgressHUD.show(withStatus: "Loading..")
            self.postDataOnWebserviceDeleteMapForm()
        }else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
    }
    
    @IBAction func cellBtnPdfClicked(_ sender: UIButton) {
        let mapID = (arrayMapFormList[sender.tag]as! NSDictionary).value(forKey: "map_id") as? String ?? ""
        let name = (arrayMapFormList[sender.tag]as! NSDictionary).value(forKey: "map_id_display") as? String ?? ""
        let userID = USERDEFAULT.value(forKey: "userID")as? String ?? ""
        
        if !(mapID.isEmpty) && !(name.isEmpty) && !(userID.isEmpty){
            self.downloadPdf(id: mapID, fileName: name,userID: userID)
        }
        else{
            showAlert(Appname, title: "Can't download file.")
            print("Error while downloading file..cellBtnPdfClicked")
        }
    }
    
    @IBAction func cellBtnShareClicked(_ sender: UIButton) {}
    @IBAction func cellBtnDeleteClicked(_ sender: UIButton) {
        if arrayMapFormList.count > sender.tag {
            
            selectedMapFormId = (arrayMapFormList[sender.tag]as! NSDictionary).value(forKey: "map_id") as? String ?? ""
            deleteMapFormIndex = sender.tag
            
            UIView.animate(withDuration: 0.3) {
                self.viewConfirm.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
            }
        }
    }
    
    //MARK:- ScrollView Delegate Methdos
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
        if scrollView == tableFormList {
            if isLoading == true{
                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                    pageNum = pageNum + 1
                    print(pageNum)
                    isLoading = false
                    
                    if Reachability.isConnectedToNetwork() == true {
                        self.addLoadingIndicatiorOnFooterOnTableView()
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForMapFormList), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
            }
        }
    }
    
    //MARK:- Table view Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMapFormList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "submittedMapCell") as? SubmittedMapListCell
        
        if cell == nil {
            let nib  = Bundle.main.loadNibNamed("SubmittedMapListCell", owner: self, options: nil)
            cell = nib?[0] as? SubmittedMapListCell
        }
        
        cell?.labelId.text = (arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "map_id_display") as? String ?? ""
        cell?.labelDate.text = Utility.getDateFrom((arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "created_at") as? String, givenFormat: "yyyy-MM-dd hh:mm:ss", returnFormat: "dd MMM yyyy")
        
        if (arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "map_status")as? String == "1" {
            cell?.labelStatus.text = "Processing"
        }
        else if (arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "map_status")as? String == "2" {
            cell?.labelStatus.text = "Waiting for payment"
        }
        else if (arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "map_status")as? String == "3" {
            cell?.labelStatus.text = "cancelled"
        }
        else if (arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "map_status")as? String == "4" {
            cell?.labelStatus.text = "Completed"
        }
        else{
            cell?.labelStatus.text = ""
        }
        
        cell?.btnDelete.tag = indexPath.row
        cell?.btnDelete.addTarget(self, action: #selector(cellBtnDeleteClicked(_:)), for: .touchUpInside)
        
        cell?.btnPdf.tag = indexPath.row
        //cell?.btnPdf .setImage(UIImage(named: "pdf.png"), for: .normal)
        cell?.btnPdf.addTarget(self, action: #selector(cellBtnPdfClicked(_:)), for: .touchUpInside)
        
        cell?.btnShare.tag = indexPath.row
        //cell?.btnShare .setImage(UIImage(named: "share_map.png"), for: .normal)
        cell?.btnShare.addTarget(self, action: #selector(cellBtnShareClicked(_:)), for: .touchUpInside)
        
        if (arrayMapFormList[indexPath.row] as! NSDictionary).value(forKey: "document_required")as? String == "1" {
            cell?.viewRequired.isHidden = false
        }
        else{
            cell?.viewRequired.isHidden = true
        }
        
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let history = MapOrderDetailVC(nibName: "MapOrderDetailVC", bundle: nil)
        history.orderId = (arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "map_id_display")as! String
        history.mapId = (arrayMapFormList[indexPath.row]as! NSDictionary).value(forKey: "map_id")as? String ?? ""
        self.navigationController?.pushViewController(history, animated: true)
    }
    
    
    
    //MARK:- Web Service Call Methods
    
    func postDataOnWebserviceForMapFormList(){
        self.addLoadingIndicatiorOnFooterOnTableView()
        
        let completeURL = NSString(format:"%@%@", MainURL,getSubmittedMapListUrl) as String
        
        let pageNumber = "\(pageNum!)"
        
        var params:NSDictionary!
        
        let parameter = NSMutableDictionary()
        parameter.setValue(USERDEFAULT.value(forKey: "userID") as? String ?? "", forKey: "user_id")
        parameter.setValue(USERDEFAULT.value(forKey: "token") as? String ?? "", forKey: "token")
        parameter.setValue(Language_Type, forKey: "lang_type")
        parameter.setValue(pageNumber, forKey: "page")
        parameter.setValue(PAGINATION_LIMITE, forKey: "limit")
        
        
        params = parameter.mutableCopy() as! NSDictionary
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("Get Submitted map URL :",completeURL)
        print("Get Submitted map Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getSubmittedMapListUrlTag)
    }
    
    func postDataOnWebserviceDeleteMapForm(){
        //Registration/EditMapOrder
        
        let completeURL = NSString(format:"%@%@", MainURL,deleteSavedMapFormUrl) as String
        
        var params:NSDictionary!
        
        let parameter = NSMutableDictionary()
        parameter.setValue(USERDEFAULT.value(forKey: "userID") as? String ?? "", forKey: "user_id")
        parameter.setValue(USERDEFAULT.value(forKey: "token") as? String ?? "", forKey: "token")
        parameter.setValue(Language_Type, forKey: "lang_type")
        parameter.setValue("1", forKey: "is_delete")
        parameter.setValue(selectedMapFormId, forKey: "map_id")
        params = parameter.mutableCopy() as! NSDictionary
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("Delete saved map URL :",completeURL)
        print("Delete saved map Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: deleteSavedMapFormUrlTag)
    }
    
    //MARK:- Web Service Callback Methods
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
            
        case getSubmittedMapListUrlTag:
            let resultDict = responseObject as! NSDictionary;
            print("Saved map List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum == 1{
                    self.arrayMapFormList = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    arrayMapFormList.addObjects(from: myData as! [Any])
//                    print("Submitted Map List Response  : \(arrayMapFormList)")
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum > 0) {
                            self.pageNum = self.pageNum - 1
                        }
                        self.isLoading = false
                    }else{
                        self.isLoading = true
                    }
                }
                else{
                    self.isLoading = false
                    if (self.pageNum > 0) {
                        self.pageNum = self.pageNum - 1
                    }
                    
                    if arrayMapFormList.count == 0{
                        tableFormList.isHidden = true
                        labelNoDataFound.isHidden = false
                    }
                    else{
                        tableFormList.isHidden = false
                        labelNoDataFound.isHidden = true
                    }
                }
                
                tableFormList.reloadData()
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
            
            
        case deleteSavedMapFormUrlTag:
            
            let resultDict = responseObject as! NSDictionary;
            print("Delete map Form Response  : \(resultDict)")
            SVProgressHUD.dismiss()
            
            if resultDict.value(forKey: "status") as! String == "1"{
                arrayMapFormList.removeObject(at: deleteMapFormIndex)
                tableFormList.reloadData()
                
                if arrayMapFormList.count == 0{
                    tableFormList.isHidden = true
                    labelNoDataFound.isHidden = false
                }
                else{
                    tableFormList.isHidden = false
                    labelNoDataFound.isHidden = true
                }
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
            
        default:
            break
        }
    }
    
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case getSubmittedMapListUrlTag:
            self.isLoading = false
            if (self.pageNum > 0) {
                self.pageNum = self.pageNum - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
            
        case deleteSavedMapFormUrlTag:
            SVProgressHUD.dismiss()
            break
            
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    //MARK:-
    
    func downloadPdf(id:String, fileName:String, userID : String){
        // Create destination URL
        
        let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last as URL?)!
        let destinationFileUrl = documentsUrl.appendingPathComponent(fileName+".pdf")
        
        //Create URL to the source file you want to download
        
        let strUrl = SingleMapPDFurl+id+"&user_id="+userID
        let fileURL = URL(string: strUrl)
        
        print("file url -> \(String(describing: fileURL))")
        
        let isFileFound:Bool? = FileManager.default.fileExists(atPath: destinationFileUrl.path)
        if isFileFound == true
        {
            let viewer = UIDocumentInteractionController(url: URL(fileURLWithPath: destinationFileUrl.path))
            viewer.delegate = self
            viewer.presentPreview(animated: true)
        }
        else{
            SVProgressHUD.show(withStatus: "Downloading...")
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig)
            let request = URLRequest(url:fileURL!)
            
            let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
                if let tempLocalUrl = tempLocalUrl, error == nil {
                    // Success
                    if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                        SVProgressHUD.dismiss()
                        print("Successfully downloaded. Status code: \(statusCode)")
                    }
                    
                    do
                    {
                        if(FileManager.default.fileExists(atPath: destinationFileUrl.path))
                        {
                            try FileManager.default.removeItem(at: destinationFileUrl)
                            try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                            SVProgressHUD.dismiss()
                            self.showFileWithPath(path: destinationFileUrl.path)
                        }
                        else
                        {
                            try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                            self.showFileWithPath(path: destinationFileUrl.path)
                            SVProgressHUD.dismiss()
                        }
                    }
                    catch (let writeError)
                    {
                        print("Error creating a file \(destinationFileUrl) : \(writeError)")
                    }
                    
                } else {
                    SVProgressHUD.dismiss()
                    print("Error took place while downloading a file. Error description: %@", error?.localizedDescription ?? "Errorr.....");
                }
            }
            task.resume()
        }
    }
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    
    func showFileWithPath(path: String)
    {
        let isFileFound:Bool? = FileManager.default.fileExists(atPath: path)
        if isFileFound == true
        {
            let viewer = UIDocumentInteractionController(url: URL(fileURLWithPath: path))
            viewer.delegate = self
            viewer.presentPreview(animated: true)
        }
    }
}
