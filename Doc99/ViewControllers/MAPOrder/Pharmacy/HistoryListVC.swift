//
//  HistoryListVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 04/04/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class HistoryListVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIDocumentInteractionControllerDelegate {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var labelValue: UILabel!
    @IBOutlet weak var labelMsg: UILabel!
    
    @IBOutlet weak var btnDuePayment: UIButton!
    @IBOutlet weak var btnSettledPayment: UIButton!
    
    @IBOutlet weak var tableDuePayment: UITableView!
    @IBOutlet weak var tableSetPayment: UITableView!
    
    
    //MARK:- Variables
    var arrayDue = NSMutableArray()
    var arraySet = NSMutableArray()
    
    var pageDue : Int = 1
    var isLoadingDue : Bool = false
    
    var pageSet : Int = 1
    var isLoadingSet : Bool = false
    
    var duePayment = true
    
    
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
   
        self.btnDuePaymentClicked(btnDuePayment)
        
        tableDuePayment.register(UINib(nibName: "SellingHistoryCell", bundle: nil), forCellReuseIdentifier: "historyCell")
        tableSetPayment.register(UINib(nibName: "SellingHistoryCell", bundle: nil), forCellReuseIdentifier: "historyCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    //MARK:-
    
    func addLoadingIndicatiorOnFooterOnTableView(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        if duePayment {
            tableDuePayment.tableFooterView = spinner
        }
        else{
            tableSetPayment.tableFooterView = spinner
        }
    }
    
    func removeLoadingIndicatiorOnFooterOnTableView(){
        tableSetPayment.tableFooterView = nil
        tableDuePayment.tableFooterView = nil
    }
    
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let loginAlert = UIAlertController(title: messageT, message: title, preferredStyle:.alert)
        let actionOK = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        let actionLogin = UIAlertAction(title: "LOGIN", style: .default) { (actionLogin) in
            APPDELEGATE.logOutUserWithoutToken()
            let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        
        
        loginAlert.addAction(actionOK)
        loginAlert.addAction(actionLogin)
        
        if !(self.presentedViewController != nil) {
            self.present(loginAlert, animated: true, completion: nil)
        }
    }
    
    
    //MARK:- ScrollView Delegate Methdos
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == tableDuePayment {
            if isLoadingDue == true{
                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                    pageDue = pageDue + 1
                    print(pageDue)
                    isLoadingDue = false
                    
                    if Reachability.isConnectedToNetwork() == true {
                        self.addLoadingIndicatiorOnFooterOnTableView()
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDatagetDuePayment), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
            }
        }
        else if scrollView == tableSetPayment {
            if isLoadingSet == true{
                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                    pageSet = pageSet + 1
                    print(pageSet)
                    isLoadingSet = false
                    
                    if Reachability.isConnectedToNetwork() == true {
                        self.addLoadingIndicatiorOnFooterOnTableView()
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataGetSettledAmount), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
            }
        }
        
        
        
    }
    
    
    
    //MARK:- IBActions
    @IBAction func btnBackClicked(_ sender: Any) {
        APPDELEGATE.myTabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDuePaymentClicked(_ sender: UIButton) {
        
        labelValue.text = ""
        labelValue.textColor = UIColor.init(red: 233.0/255.0, green: 78.0/255.0, blue: 0.0/255.0, alpha: 1)
        labelMsg.text = "We owe you"
        btnDuePayment.setTitleColor(UIColor.init(red: 100.0/255.0, green: 208.0/255.0, blue: 233.0/255.0, alpha: 1), for: .normal)
        btnSettledPayment.setTitleColor(UIColor.init(red: 170.0/255.0, green: 170.0/255.0, blue: 170.0/255.0, alpha: 1), for: .normal)
        
        duePayment = true
        
        self.view.bringSubview(toFront: tableDuePayment)
        
        if Reachability.isConnectedToNetwork() == true {
            if arrayDue.count > 0{
                arrayDue.removeAllObjects()
                tableDuePayment.reloadData()
            }
            pageDue = 1
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDatagetDuePayment), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
    }
    
    @IBAction func btnSettledAmountClicked(_ sender: UIButton) {
        
        labelValue.text = ""
        labelValue.textColor = UIColor.init(red: 148.0/255.0, green: 190.0/255.0, blue: 71.0/255.0, alpha: 1)
        labelMsg.text = "Your total earnings"
        
        btnSettledPayment.setTitleColor(UIColor.init(red: 100.0/255.0, green: 208.0/255.0, blue: 233.0/255.0, alpha: 1), for: .normal)
        btnDuePayment.setTitleColor(UIColor.init(red: 170.0/255.0, green: 170.0/255.0, blue: 170.0/255.0, alpha: 1), for: .normal)
        
        duePayment = false
        
        self.view.bringSubview(toFront: tableSetPayment)
        
        if Reachability.isConnectedToNetwork() == true {
            if arraySet.count > 0{
                arraySet.removeAllObjects()
            }
            pageSet = 1
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataGetSettledAmount), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
    }
 
    @IBAction func duePdfClicked(_ sender : UIButton){
        let mapID = (arrayDue[sender.tag]as! NSDictionary).value(forKey: "map_id") as? String ?? ""
        let name = (arrayDue[sender.tag]as! NSDictionary).value(forKey: "map_id_display") as? String ?? ""
        let userID = USERDEFAULT.value(forKey: "userID")as? String ?? ""
        
        if !(mapID.isEmpty) && !(name.isEmpty) && !(userID.isEmpty){
            self.downloadPdf(id: mapID, fileName: name,userID: userID)
        }
        else{
            showAlert(Appname, title: "Can't download file.")
            print("Error while downloading file..cellBtnPdfClicked")
        }
    }
    
    @IBAction func setPdfClicked(_ sender : UIButton){
        let mapID = (arraySet[sender.tag]as! NSDictionary).value(forKey: "map_id") as? String ?? ""
        let name = (arraySet[sender.tag]as! NSDictionary).value(forKey: "map_id_display") as? String ?? ""
        let userID = USERDEFAULT.value(forKey: "userID")as? String ?? ""
        
        if !(mapID.isEmpty) && !(name.isEmpty) && !(userID.isEmpty){
            self.downloadPdf(id: mapID, fileName: name,userID: userID)
        }
        else{
            showAlert(Appname, title: "Can't download file.")
            print("Error while downloading file..cellBtnPdfClicked")
        }
    }
    
    
    //MARK:- TableView Delegate Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableDuePayment{
            return arrayDue.count
        }
        else if tableView == tableSetPayment{
            return arraySet.count
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableDuePayment{
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "historyCell") as? SellingHistoryCell
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("SellingHistoryCell", owner: self, options: nil)
                cell = nib?[0] as? SellingHistoryCell
            }
            
            cell?.labelID.text = (arrayDue[indexPath.row]as! NSDictionary).value(forKey: "map_id_display") as? String
            cell?.labelDate.text = Utility.getDateFrom((arrayDue[indexPath.row]as! NSDictionary).value(forKey: "created_at") as? String, givenFormat: "yyyy-MM-dd hh:mm:ss", returnFormat: "dd MMM yyyy")
            
            cell?.labelStatus.textColor = UIColor.init(red: 148.0/255.0, green: 190.0/255.0, blue: 71.0/255.0, alpha: 1)
            cell?.labelStatus.text = "Due"
            cell?.labelValue.text = "HK&\((arrayDue[indexPath.row]as! NSDictionary).value(forKey: "total_payment") as? String ?? "")"
            
            cell?.btnPdf.tag = indexPath.row
            cell?.btnPdf.addTarget(self, action: #selector(duePdfClicked(_:)), for: UIControlEvents.touchUpInside)
            
            return cell!
        }
        else{
            var cell = tableView.dequeueReusableCell(withIdentifier: "historyCell") as? SellingHistoryCell
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("SellingHistoryCell", owner: self, options: nil)
                cell = nib?[0] as? SellingHistoryCell
            }
            
            cell?.labelID.text = (arraySet[indexPath.row]as! NSDictionary).value(forKey: "map_id_display") as? String
            cell?.labelDate.text = Utility.getDateFrom((arraySet[indexPath.row]as! NSDictionary).value(forKey: "created_at") as? String, givenFormat: "yyyy-MM-dd hh:mm:ss", returnFormat: "dd MMM yyyy")
            
            cell?.labelStatus.textColor = UIColor.init(red: 233.0/255.0, green: 78.0/255.0, blue: 0.0/255.0, alpha: 1)
            cell?.labelStatus.text = "Settled"
            cell?.labelValue.text = "HK&\((arraySet[indexPath.row]as! NSDictionary).value(forKey: "total_payment") as? String ?? "")"
            
            cell?.btnPdf.tag = indexPath.row
            cell?.btnPdf.addTarget(self, action: #selector(setPdfClicked(_:)), for: .touchUpInside)
            
            return cell!
        }
    }
    
    
    //MARK:-
    func postDatagetDuePayment(){
        
        self.addLoadingIndicatiorOnFooterOnTableView()
        
        let completeURL = NSString(format:"%@%@", MainURL,getDuePaymentListUrl) as String
        var params:NSDictionary!
        
        let parameter = NSMutableDictionary()
        parameter.setValue(USERDEFAULT.value(forKey: "userID") as? String ?? "", forKey: "user_id")
        parameter.setValue(USERDEFAULT.value(forKey: "token") as? String ?? "", forKey: "token")
        parameter.setValue(Language_Type, forKey: "lang_type")
        parameter.setValue(pageDue, forKey: "page")
        parameter.setValue(PAGINATION_LIMITE, forKey: "limit")
        
        
        params = parameter.mutableCopy() as! NSDictionary
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("Get Due list URL :",completeURL)
        print("Get Due list Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getDuePaymentListUrlTag)
        
    }
    
    func postDataGetSettledAmount(){

        let completeURL = NSString(format:"%@%@", MainURL,getSettledPaymentListUrl) as String
        var params:NSDictionary!
        
        let parameter = NSMutableDictionary()
        parameter.setValue(USERDEFAULT.value(forKey: "userID") as? String ?? "", forKey: "user_id")
        parameter.setValue(USERDEFAULT.value(forKey: "token") as? String ?? "", forKey: "token")
        parameter.setValue(Language_Type, forKey: "lang_type")
        parameter.setValue(pageSet, forKey: "page")
        parameter.setValue(PAGINATION_LIMITE, forKey: "limit")
        
        
        params = parameter.mutableCopy() as! NSDictionary
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("Get Set list URL :",completeURL)
        print("Get Set list Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getSettledPaymentListUrlTag)
    }
 
    
    //MARK:- Web Service Callback Methods
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
            
        case getDuePaymentListUrlTag:
            let resultDict = responseObject as! NSDictionary;
            print("Due list Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageDue == 1{
                    self.arrayDue = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    arrayDue.addObjects(from: myData as! [Any])
                    //print("Due List Response  : \(arrayDue)")
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageDue > 0) {
                            self.pageDue = self.pageDue - 1
                        }
                        self.isLoadingDue = false
                    }else{
                        self.isLoadingDue = true
                    }
                }
                else{
                    self.isLoadingDue = false
                    if (self.pageDue > 0) {
                        self.pageDue = self.pageDue - 1
                    }
                    
//                    if arrayDue.count == 0{
//                        tableDuePayment.isHidden = true
//                        //labelNoDataFound.isHidden = false
//                    }
//                    else{
//                        tableDuePayment.isHidden = false
//                        //labelNoDataFound.isHidden = true
//                    }
                }
                
                tableDuePayment.reloadData()
                labelValue.text = "HK$\(resultDict.value(forKey: "total")as? String ?? "0")"
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
            
            
        case getSettledPaymentListUrlTag:
            let resultDict = responseObject as! NSDictionary;
//            print("settled list Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageSet == 1{
                    self.arraySet = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    arraySet.addObjects(from: myData as! [Any])
                    print("Due List Response  : \(arraySet)")
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageSet > 0) {
                            self.pageSet = self.pageSet - 1
                        }
                        self.isLoadingSet = false
                    }else{
                        self.isLoadingSet = true
                    }
                }
                else{
                    self.isLoadingSet = false
                    if (self.pageSet > 0) {
                        self.pageSet = self.pageSet - 1
                    }
                    
//                    if arraySet.count == 0{
//                        tableSetPayment.isHidden = true
//                        //labelNoDataFound.isHidden = false
//                    }
//                    else{
//                        tableSetPayment.isHidden = false
//                        //labelNoDataFound.isHidden = true
//                    }
                }
                tableSetPayment.reloadData()
                labelValue.text = "HK$\(resultDict.value(forKey: "total")as? String ?? "0")"
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
            
            
        default:
            break
            
        }
    }
    
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case getDuePaymentListUrlTag:
            self.isLoadingDue = false
            if (self.pageDue > 0) {
                self.pageDue = self.pageDue - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
            
        case getSettledPaymentListUrlTag:
            self.isLoadingSet = false
            if (self.pageSet > 0) {
                self.pageSet = self.pageSet - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
 
    //MARK:-
    
    func downloadPdf(id:String, fileName:String, userID : String){
        // Create destination URL
        
        let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last as URL?)!
        let destinationFileUrl = documentsUrl.appendingPathComponent(fileName+".pdf")
        
        //Create URL to the source file you want to download
        
        let strUrl = SingleMapPDFurl+id+"&user_id="+userID
        let fileURL = URL(string: strUrl)
        
        print("file url -> \(String(describing: fileURL))")
        
        let isFileFound:Bool? = FileManager.default.fileExists(atPath: destinationFileUrl.path)
        if isFileFound == true
        {
            let viewer = UIDocumentInteractionController(url: URL(fileURLWithPath: destinationFileUrl.path))
            viewer.delegate = self
            viewer.presentPreview(animated: true)
        }
        else{
            SVProgressHUD.show(withStatus: "Downloading...")
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig)
            let request = URLRequest(url:fileURL!)
            
            let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
                if let tempLocalUrl = tempLocalUrl, error == nil {
                    // Success
                    if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                        SVProgressHUD.dismiss()
                        print("Successfully downloaded. Status code: \(statusCode)")
                    }
                    
                    do
                    {
                        if(FileManager.default.fileExists(atPath: destinationFileUrl.path))
                        {
                            try FileManager.default.removeItem(at: destinationFileUrl)
                            try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                            SVProgressHUD.dismiss()
                            self.showFileWithPath(path: destinationFileUrl.path)
                        }
                        else
                        {
                            try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                            self.showFileWithPath(path: destinationFileUrl.path)
                            SVProgressHUD.dismiss()
                        }
                    }
                    catch (let writeError)
                    {
                        print("Error creating a file \(destinationFileUrl) : \(writeError)")
                    }
                    
                } else {
                    SVProgressHUD.dismiss()
                    print("Error took place while downloading a file. Error description: %@", error?.localizedDescription ?? "Errorr.....");
                }
            }
            task.resume()
        }
    }
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    
    func showFileWithPath(path: String)
    {
        let isFileFound:Bool? = FileManager.default.fileExists(atPath: path)
        if isFileFound == true
        {
            let viewer = UIDocumentInteractionController(url: URL(fileURLWithPath: path))
            viewer.delegate = self
            viewer.presentPreview(animated: true)
        }
    }
    
}
