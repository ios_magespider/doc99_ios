//
//  UserOrderDetailVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 02/04/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

//Used For Pharmacy user to review order given by normal user.


import UIKit
import SVProgressHUD

class UserOrderDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    //MARK:- IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var viewRequiredDoc: UIView!
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var viewSaveButton: UIView!
    @IBOutlet weak var btnReviewOrder: UIButton!
    //@IBOutlet weak var btnMakePayment: UIButton!
    
    @IBOutlet var viewDetail: UIView!
    @IBOutlet weak var labelOrderStatus: UILabel!
    
    @IBOutlet weak var viewOrderStatus: UIView!
    @IBOutlet weak var labelOrderId: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var viewRequiredDetail: UIView!
    
    
    @IBOutlet weak var labelInvoice: UILabel!
    @IBOutlet weak var tableProductList: UITableView!
    
    @IBOutlet weak var viewAmount: UIView!
    
    @IBOutlet weak var labelSubtTotal: UILabel!
    @IBOutlet weak var labelSubtTotalValue: UILabel!
    
    @IBOutlet weak var labelDiscount: UILabel!
    @IBOutlet weak var labelDiscountValue: UILabel!
    
    @IBOutlet weak var labelDeliveryCharge: UILabel!
    @IBOutlet weak var labelDeliveryChargeValue: UILabel!
    
    @IBOutlet weak var labelHandlingCharge: UILabel!
    @IBOutlet weak var labelHandlingChargeValue: UILabel!
    
    @IBOutlet weak var labelServiceTax: UILabel!
    @IBOutlet weak var labelServiceTaxValue: UILabel!
    
    @IBOutlet weak var labelTotalAmount: UILabel!
    @IBOutlet weak var labelTotalAmountValue: UILabel!
    
    //Change Status View
    @IBOutlet weak var viewUpdateStatus: UIView!
    @IBOutlet weak var btnProcessing: UIButton!
    @IBOutlet weak var imgProcessing: UIImageView!
    @IBOutlet weak var btnWaiting: UIButton!
    @IBOutlet weak var imgWaiting: UIImageView!
    @IBOutlet weak var btnCompleted: UIButton!
    @IBOutlet weak var imgCompleted: UIImageView!
    @IBOutlet weak var btnCancelled: UIButton!
    @IBOutlet weak var imgCancelled: UIImageView!
    
    
    //MARK:- Variables
    
    var mapId : String = ""
    var orderId : String = ""
    var mapDetail = NSMutableDictionary()
    
    var selectedStatus : String = ""
    
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = orderId
        labelOrderId.text = orderId
        
        
        self.adjustViewHeight()
        mainScrollView .addSubview(viewDetail)
        
        //orderReviewCell
        tableProductList.register(UINib(nibName: "MapOrderReviewCell", bundle: nil), forCellReuseIdentifier: "orderReviewCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(generalViewSettings), name: NSNotification.Name("refreshOrderDetail"), object: nil)
        self.generalViewSettings()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
   
    //MARK:-
    func generalViewSettings() {
        
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            self.postDataOnWebserviceForGetMapDetail()
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:
                NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
    }
    func adjustViewHeight() {
        
        tableProductList.frame = CGRect(x: 0, y: tableProductList.frame.origin.y, width: ScreenSize.SCREEN_WIDTH, height: tableProductList.contentSize.height)
        
        viewAmount.frame = CGRect(x: 0, y: tableProductList.frame.origin.y + tableProductList.frame.size.height, width: ScreenSize.SCREEN_WIDTH, height: viewAmount.frame.size.height)
        
        viewDetail.frame = CGRect(x: 0, y: viewDetail.frame.origin.y, width: viewDetail.frame.size.width, height: viewAmount.frame.origin.y + viewAmount.frame.size.height + 8)
        
        mainScrollView.contentSize = CGSize(width: ScreenSize.SCREEN_WIDTH, height: viewDetail.frame.size.height)
        //tableProductList.isScrollEnabled = true
    }

    
    func setStatus(status: String){
        
        if mapDetail.value(forKey: "map_status")as? String == "1" {
            labelStatus.text = "Processing"
        }
        else if mapDetail.value(forKey: "map_status")as? String == "2" {
            labelStatus.text = "Waiting for payment"
        }
        else if mapDetail.value(forKey: "map_status")as? String == "3" {
            labelStatus.text = "cancelled"
        }
        else if mapDetail.value(forKey: "map_status")as? String == "4" {
            labelStatus.text = "Completed"
        }
        else{
            labelStatus.text = ""
        }
        
    }
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let loginAlert = UIAlertController(title: messageT, message: title, preferredStyle:.alert)
        let actionOK = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        let actionLogin = UIAlertAction(title: "LOGIN", style: .default) { (actionLogin) in
            APPDELEGATE.logOutUserWithoutToken()
            let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        
        
        loginAlert.addAction(actionOK)
        loginAlert.addAction(actionLogin)
        
        if !(self.presentedViewController != nil) {
            present(loginAlert, animated: true, completion: nil)
        }
    }
    
    
    //MARK:- IBActions
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEditClicked(_ sender: Any) {
        
        if mapDetail.value(forKey: "map_status")as? String == "1" {
            btnSelectStatusClicked(btnProcessing)
        }
        else if mapDetail.value(forKey: "map_status")as? String == "2" {
            btnSelectStatusClicked(btnWaiting)
        }
        else if mapDetail.value(forKey: "map_status")as? String == "3" {
            btnSelectStatusClicked(btnCancelled)
        }
        else if mapDetail.value(forKey: "map_status")as? String == "4" {
            btnSelectStatusClicked(btnCompleted)
        }
        
        
        UIView.animate(withDuration: 0.3) {
            self.viewUpdateStatus.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        }
    }
    

    
    @IBAction func btnReviewOrderClicked(_ sender: Any) {
        let review = ReviewUserOrderVC(nibName: "ReviewUserOrderVC", bundle: nil)
        review.mapFormData = mapDetail
        self.navigationController?.pushViewController(review, animated: true)
    }
    
    
    @IBAction func btnSelectStatusClicked(_ sender: UIButton) {
        if sender.tag == 11 {
            imgProcessing.image = UIImage(named : "check_blue.png")
            imgWaiting.image = UIImage(named : "check_gray.png")
            imgCompleted.image = UIImage(named : "check_gray.png")
            imgCancelled.image = UIImage(named : "check_gray.png")
            
            selectedStatus = "1"
        }
        else if sender.tag == 12 {
            imgProcessing.image = UIImage(named : "check_gray.png")
            imgWaiting.image = UIImage(named : "check_blue.png")
            imgCompleted.image = UIImage(named : "check_gray.png")
            imgCancelled.image = UIImage(named : "check_gray.png")
            
            selectedStatus = "2"
        }
        else if sender.tag == 13 {
            imgProcessing.image = UIImage(named : "check_gray.png")
            imgWaiting.image = UIImage(named : "check_gray.png")
            imgCompleted.image = UIImage(named : "check_blue.png")
            imgCancelled.image = UIImage(named : "check_gray.png")
            
            selectedStatus = "4"
        }
        else if sender.tag == 14 {
            imgProcessing.image = UIImage(named : "check_gray.png")
            imgWaiting.image = UIImage(named : "check_gray.png")
            imgCompleted.image = UIImage(named : "check_gray.png")
            imgCancelled.image = UIImage(named : "check_blue.png")
            
            selectedStatus = "3"
        }
    }
    
    
    @IBAction func btnCancelClicked(_ sender: Any) {
        selectedStatus = ""
        UIView.animate(withDuration: 0.3) {
            self.viewUpdateStatus.frame = CGRect(x: 0, y: ScreenSize.SCREEN_HEIGHT, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        }
        imgProcessing.image = UIImage(named : "check_gray.png")
        imgWaiting.image = UIImage(named : "check_gray.png")
        imgCompleted.image = UIImage(named : "check_gray.png")
        imgCancelled.image = UIImage(named : "check_gray.png")
    }
    
    @IBAction func btnConfirmClicked(_ sender: Any) {
        
        if (selectedStatus.isEmpty) {
            showAlert(Appname, title: "Please select order status.")
        }
        else{
            if Reachability.isConnectedToNetwork() == true {
                if mapDetail.count > 0 {
                    SVProgressHUD.show(withStatus: "Loading..")
                    self.postDataUpdateOrderStatus()
                }
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:
                    NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            UIView.animate(withDuration: 0.3) {
                self.viewUpdateStatus.frame = CGRect(x: 0, y: ScreenSize.SCREEN_HEIGHT, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
            }
        }
    }
    
    
    //MARK:- TableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return APPDELEGATE.arrayMAPOredrDrug.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "orderReviewCell") as? MapOrderReviewCell
        
        if cell == nil {
            let nib  = Bundle.main.loadNibNamed("MapOrderReviewCell", owner: self, options: nil)
            cell = nib?[0] as? MapOrderReviewCell
        }
        
        
        cell?.labelDrugName.text = (APPDELEGATE.arrayMAPOredrDrug.object(at: indexPath.row) as? NSDictionary)?.value(forKey: "product_name") as? String ?? ""
        cell?.labelMg.text = "\((APPDELEGATE.arrayMAPOredrDrug.object(at: indexPath.row) as? NSDictionary)?.value(forKey: "weight") as? String ?? "0" + " mg")"
        cell?.labelPackageInfo.text = (APPDELEGATE.arrayMAPOredrDrug.object(at: indexPath.row) as? NSDictionary)?.value(forKey: "product_desc") as? String ?? ""
        cell?.labelQuantity.text = "\((APPDELEGATE.arrayMAPOredrDrug.object(at: indexPath.row) as? NSDictionary)?.value(forKey: "qty") as? String ?? "") qty"
        
        cell?.labelPrice.text = "HK$ \((APPDELEGATE.arrayMAPOredrDrug.object(at: indexPath.row) as? NSDictionary)?.value(forKey: "price") as? String ?? "")"
        
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        adjustViewHeight()
    }
    
    
    
    //MARK:- Web service call methods
    func postDataOnWebserviceForGetMapDetail(){
        
        let completeURL = NSString(format:"%@%@", MainURL,getMapFormDetailUrl) as String
        
        var params:NSDictionary!
        
        let parameter = NSMutableDictionary()
        parameter.setValue(USERDEFAULT.value(forKey: "userID") as? String ?? "", forKey: "user_id")
        parameter.setValue(USERDEFAULT.value(forKey: "token") as? String ?? "", forKey: "token")
        parameter.setValue(Language_Type, forKey: "lang_type")
        parameter.setValue(mapId, forKey: "map_id")
        
        params = parameter.mutableCopy() as! NSDictionary
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("get Map Detail URL :",completeURL)
        print("get Map Detail Parameter :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getMapFormDetailUrlTag)
    }
    
    func postDataUpdateOrderStatus() {
        let completeURL = NSString(format:"%@%@", MainURL,editSavedMapUrl) as String
        
        var params:NSDictionary!
        
        let parameter = NSMutableDictionary()
        parameter.setValue(USERDEFAULT.value(forKey: "userID") as? String ?? "", forKey: "user_id")
        parameter.setValue(USERDEFAULT.value(forKey: "token") as? String ?? "", forKey: "token")
        parameter.setValue(Language_Type, forKey: "lang_type")
        parameter.setValue(mapDetail.value(forKey: "map_id")as? String, forKey: "map_id")
        
        //Section A Parameters
        parameter.setValue(mapDetail.value(forKey: "sa_delievry_date")as? String, forKey: "sa_delievry_date")
        parameter.setValue(mapDetail.value(forKey: "sa_packaging")as? String, forKey: "sa_packaging")
        parameter.setValue(mapDetail.value(forKey: "sa_batch_no")as? String, forKey: "sa_batch_no")
        parameter.setValue(mapDetail.value(forKey: "sa_inbound_hk_awbno")as? String, forKey: "sa_inbound_hk_awbno")
        parameter.setValue(mapDetail.value(forKey: "sa_outbound_hk_date")as? String, forKey: "sa_outbound_hk_date")
        parameter.setValue(mapDetail.value(forKey: "sa_courier_ref")as? String, forKey: "sa_courier_ref")
        parameter.setValue(mapDetail.value(forKey: "sa_consignee_airport")as? String, forKey: "sa_consignee_airport")
        parameter.setValue(mapDetail.value(forKey: "sa_tracking_number")as? String, forKey: "sa_tracking_number")
        parameter.setValue(mapDetail.value(forKey: "sa_courier")as? String, forKey: "sa_courier")
        
        //Section B Parameters
        parameter.setValue(mapDetail.value(forKey: "sb_p_name")as? String, forKey: "sb_p_name")
        parameter.setValue(mapDetail.value(forKey: "sb_p_address")as? String, forKey: "sb_p_address")
        parameter.setValue(mapDetail.value(forKey: "sb_p_delivery_address")as? String, forKey: "sb_p_delivery_address")
        parameter.setValue(mapDetail.value(forKey: "sb_p_date")as? String, forKey: "sb_p_date")
        parameter.setValue(mapDetail.value(forKey: "sb_s_contact_name")as? String, forKey: "sb_s_contact_name")
        parameter.setValue(mapDetail.value(forKey: "sb_s_address")as? String, forKey: "sb_s_address")
        parameter.setValue(mapDetail.value(forKey: "sb_s_date")as? String, forKey: "sb_s_date")
        parameter.setValue(mapDetail.value(forKey: "sb_supply_date")as? String, forKey: "sb_supply_date")
        
        //Products
        parameter.setValue(mapDetail.value(forKey: "products")as? NSArray, forKey: "products")
        
        parameter.setValue(mapDetail.value(forKey: "document_required")as? String, forKey: "document_required")
        parameter.setValue(mapDetail.value(forKey: "delivery_charge")as? String, forKey: "delivery_charge")
        parameter.setValue(mapDetail.value(forKey: "handling_charge")as? String, forKey: "handling_charge")
        parameter.setValue(mapDetail.value(forKey: "freight_insurance")as? String, forKey: "freight_insurance")
        parameter.setValue(mapDetail.value(forKey: "vat")as? String, forKey: "vat")
        parameter.setValue(mapDetail.value(forKey: "width")as? String, forKey: "width")
        parameter.setValue(mapDetail.value(forKey: "length")as? String, forKey: "length")
        parameter.setValue(mapDetail.value(forKey: "is_draft")as? String, forKey: "is_draft")
        
        
        parameter.setValue(mapDetail.value(forKey: "store_id")as? String, forKey: "store_id")
        parameter.setValue(mapDetail.value(forKey: "store_owner_id")as? String, forKey: "store_owner_id")
        parameter.setValue(mapDetail.value(forKey: "sub_total")as? String, forKey: "sub_total")
        parameter.setValue(mapDetail.value(forKey: "total_payment")as? String, forKey: "total_payment")
        parameter.setValue(mapDetail.value(forKey: "totalpurchase")as? String, forKey: "totalpurchase")
        parameter.setValue(mapDetail.value(forKey: "unit_weight")as? String, forKey: "unit_weight")
        
        parameter.setValue(selectedStatus, forKey: "map_status")
        
        params = parameter.mutableCopy() as! NSDictionary
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("Update Order Status URL :",completeURL)
        print("Update Order Status Parameters :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: editSavedMapUrlTag)
    }
    
    
    
    //MARK: - Web Service call back Methods
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case getMapFormDetailUrlTag:
            
            let resultDict = responseObject as! NSDictionary;
            print("Get Single map detail Response  : \(resultDict)")
            SVProgressHUD.dismiss()
            if resultDict.value(forKey: "status") as! String == "1"{
                mapDetail = (resultDict.value(forKey: "data") as! NSDictionary).mutableCopy() as! NSMutableDictionary
                
                labelSubtTotalValue.text = mapDetail.value(forKey: "sub_total")as? String
                labelDiscountValue.text = mapDetail.value(forKey: "discount")as? String
                labelDeliveryChargeValue.text = mapDetail.value(forKey: "delivery_charge")as? String
                labelHandlingChargeValue.text = mapDetail.value(forKey: "handling_charge")as? String
                labelServiceTaxValue.text = mapDetail.value(forKey: "vat")as? String
                labelTotalAmountValue.text = mapDetail.value(forKey: "total_payment")as? String
                
                if mapDetail.value(forKey: "document_required")as? String == "1"
                {
                    viewRequiredDetail.isHidden = false
                }
                else{
                    viewRequiredDetail.isHidden = true
                }
                
                
                self.setStatus(status: selectedStatus)
                
                tableProductList.delegate = self;
                tableProductList.dataSource = self;
                
                APPDELEGATE.arrayMAPOredrDrug = (mapDetail.value(forKey: "products") as! NSArray).mutableCopy() as! NSMutableArray
                tableProductList.reloadData()
                adjustViewHeight()
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        case editSavedMapUrlTag:
            let resultDict = responseObject as! NSDictionary;
            print("Get Single map detail Response  : \(resultDict)")
            SVProgressHUD.dismiss()
            if resultDict.value(forKey: "status") as! String == "1"{
                mapDetail .setValue(selectedStatus, forKey: "map_status")
                self.setStatus(status: selectedStatus)
                NotificationCenter.default.post(name: NSNotification.Name("refreshUserOrderList"), object:nil)
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
            
        default:
            break
            
        }
        
    }
    
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
            
        case getMapFormDetailUrlTag:
            SVProgressHUD.dismiss()
            break
            
        case editSavedMapUrlTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""),
                  title: NSLocalizedString(FailureAlert,tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    
    
    
    
    
}
