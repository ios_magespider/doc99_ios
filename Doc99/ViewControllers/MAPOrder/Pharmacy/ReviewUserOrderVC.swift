//
//  ReviewUserOrderVC.swift
//  Doc99
//
//  Created by MS-MAC-009 on 02/04/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class ReviewUserOrderVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    //MARK:- Outlets
    
    @IBOutlet weak var scrollViewMain: UIScrollView!
    @IBOutlet weak var viewForButtonSave: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet var viewDetailForm: UIView!
    
    //--      Section-A Outlets      --//
    @IBOutlet weak var labelSectionA: UILabel!
    @IBOutlet weak var textViewNote: UITextView!
    @IBOutlet weak var viewSectionA: UIView!
    
    @IBOutlet weak var btnDeliveryDate: UIButton!
    @IBOutlet weak var txtDeliveryDate: UITextField!
    @IBOutlet weak var txtPackaging: UITextField!
    @IBOutlet weak var txtBatchNo: UITextField!
    @IBOutlet weak var txtInBoundAWB: UITextField!
    @IBOutlet weak var txtOutBoundAWB: UITextField!
    @IBOutlet weak var txtCourierReferance: UITextField!
    @IBOutlet weak var txtConsignee: UITextField!
    @IBOutlet weak var txtTrackingNo: UITextField!
    @IBOutlet weak var txtCourier: UITextField!
    
    //--      Packaging Outlets      --//
    
    @IBOutlet weak var viewPackagingDetail: UIView!
    @IBOutlet weak var txtLength: UITextField!
    @IBOutlet weak var txtWidth: UITextField!
    @IBOutlet weak var txtHeight: UITextField!
    @IBOutlet weak var txtUnitWeight: UITextField!
    
    //--      Product Table       --//
    @IBOutlet weak var tableProductCart: UITableView!
    
    //--   Charge view  Outlets   --//
    @IBOutlet weak var viewChargeDetail: UIView!
    
    @IBOutlet weak var txtDiscountAmount: UITextField!
    @IBOutlet weak var txtFreightInsaurance: UITextField!
    @IBOutlet weak var txtHandlingCharge: UITextField!
    @IBOutlet weak var txtServiesTax: UITextField!
    @IBOutlet weak var txtTotalAmount: UITextField!
    
    
    //--      Section-B Outlets      --//
    @IBOutlet weak var viewSectionB: UIView!
    @IBOutlet weak var txtPurchaserName: UITextField!
    @IBOutlet weak var btnPurchaserDate: UIButton!
    @IBOutlet weak var txtPurchaserDate: UITextField!
    @IBOutlet weak var txtPurchaserAddress: UITextField!
    @IBOutlet weak var txtDeliveryAddress: UITextField!
    @IBOutlet weak var txtShipperName: UITextField!
    @IBOutlet weak var txtShipperAddress: UITextField!
    @IBOutlet weak var btnShipperDate: UIButton!
    @IBOutlet weak var txtShipperDate: UITextField!
    @IBOutlet weak var btnSupplyDate: UIButton!
    @IBOutlet weak var txtSupplyDate: UITextField!
    
    @IBOutlet weak var viewRequire: UIView!
    @IBOutlet weak var imgRequire: UIImageView!
    
    //--      Date Picker Outlets      --//
    @IBOutlet weak var viewDatePicker: UIView!
    @IBOutlet weak var labelDateType: UILabel!
    @IBOutlet weak var btnCancelDatePicker: UIButton!
    @IBOutlet weak var btnDoneDatePicker: UIButton!
    @IBOutlet weak var myDatePicker: UIDatePicker!
    
    //MARK:- Variables
    var mapFormData = NSMutableDictionary()
    var orderId : String = ""
    var docRequired : String = "0"
    
    var activeField : UITextField!
    
    var purchaseDate : String = ""
    var deliveryDate : String = ""
    var shipperDate : String = ""
    var supplyDate : String = ""
    
    var numberToolbar : UIToolbar!
    
    var productIndex : Int!
    var obj = NSMutableDictionary()
    
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        tableProductCart.delegate = self
        tableProductCart.dataSource = self
        tableProductCart.reloadData()
        
        self.generalViewSettings()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK:-
    func generalViewSettings() {
        
        
        self.adjustViewHeight()
        self.scrollViewMain.addSubview(viewDetailForm)
        
        tableProductCart.register(UINib(nibName: "MapOrderReviewProductCell", bundle: nil), forCellReuseIdentifier: "reviewProductCell")
        
        self.setOrderDetails()
        NavBarNumberPad()
        
        myDatePicker.minimumDate = NSDate() as Date
        myDatePicker.datePickerMode = .date;
    }
    
    func adjustViewHeight() {
        
        tableProductCart.frame = CGRect(x: 0, y: viewPackagingDetail.frame.origin.y + viewPackagingDetail.frame.size.height + 18, width: tableProductCart.frame.size.width, height: tableProductCart.contentSize.height)
        tableProductCart.isScrollEnabled = false
        
        viewChargeDetail.frame = CGRect(x: 0, y:tableProductCart.frame.origin.y + tableProductCart.frame.size.height + 18, width: viewChargeDetail.frame.size.width, height: viewChargeDetail.frame.size.height)
        
        viewSectionB.frame = CGRect(x: 0, y: viewChargeDetail.frame.origin.y + viewChargeDetail.frame.size.height + 18, width: viewSectionB.frame.size.width, height: viewSectionB.frame.size.height)
        
        viewRequire.frame = CGRect(x: viewRequire.frame.origin.x, y: viewSectionB.frame.origin.y + viewSectionB.frame.size.height + 18, width: viewRequire.frame.size.width, height: viewRequire.frame.size.height)
        
        viewDetailForm.frame = CGRect(x: viewDetailForm.frame.origin.x, y: viewDetailForm.frame.origin.y, width: scrollViewMain.frame.size.width, height: viewRequire.frame.origin.y + viewRequire.frame.size.height + 18)
        
        self.scrollViewMain.contentSize = CGSize(width: ScreenSize.SCREEN_WIDTH, height: viewDetailForm.frame.size.height)
    }
    
    func setOrderDetails() {
        APPDELEGATE.arrayMAPOredrDrug = (mapFormData.value(forKey: "products") as? NSArray)?.mutableCopy() as! NSMutableArray
        tableProductCart.reloadData()
        
        txtDeliveryDate.text = Utility.getDateFrom(mapFormData.value(forKey: "sa_delievry_date")as? String, givenFormat: "yyyy-MM-dd HH:mm:ss", returnFormat: "MM/dd/yyyy")
        deliveryDate =  Utility.getDateFrom(mapFormData.value(forKey: "sa_delievry_date")as? String, givenFormat: "yyyy-MM-dd HH:mm:ss", returnFormat: "yyyy-MM-dd hh:mm:ss") ?? ""
        
        txtPackaging.text = mapFormData.value(forKey: "sa_packaging") as? String
        txtBatchNo.text = mapFormData.value(forKey: "sa_batch_no") as? String
        txtInBoundAWB.text = mapFormData.value(forKey: "sa_inbound_hk_awbno") as? String
        txtOutBoundAWB.text = mapFormData.value(forKey: "sa_outbound_hk_date") as? String
        txtCourierReferance.text = mapFormData.value(forKey: "sa_courier_ref") as? String
        txtConsignee.text = mapFormData.value(forKey: "sa_consignee_airport") as? String
        txtTrackingNo.text = mapFormData.value(forKey: "sa_tracking_number") as? String
        txtCourier.text = mapFormData.value(forKey: "sa_courier") as? String
        
        txtLength.text = mapFormData.value(forKey: "length") as? String
        txtWidth.text = mapFormData.value(forKey: "width") as? String
        txtHeight.text = mapFormData.value(forKey: "height") as? String
        txtUnitWeight.text = mapFormData.value(forKey: "unit_weight") as? String
        
        txtDiscountAmount.text = mapFormData.value(forKey: "discount") as? String
        txtFreightInsaurance.text = mapFormData.value(forKey: "freight_insurance") as? String
        txtHandlingCharge.text = mapFormData.value(forKey: "handling_charge") as? String
        txtServiesTax.text = mapFormData.value(forKey: "vat") as? String
        
        txtPurchaserName.text = mapFormData.value(forKey: "sb_p_name") as? String
        
        txtPurchaserDate.text = Utility.getDateFrom(mapFormData.value(forKey: "sb_p_date")as? String, givenFormat: "yyyy-MM-dd HH:mm:ss", returnFormat: "MM/dd/yyyy")
        purchaseDate = Utility.getDateFrom(mapFormData.value(forKey: "sb_p_date")as? String, givenFormat: "yyyy-MM-dd HH:mm:ss", returnFormat: "yyyy-MM-dd hh:mm:ss") ?? ""
        
        
        txtPurchaserAddress.text = mapFormData.value(forKey: "sb_p_address") as? String
        txtDeliveryAddress.text = mapFormData.value(forKey: "sb_p_delivery_address") as? String
        
        txtShipperName.text = mapFormData.value(forKey: "sb_s_contact_name") as? String
        txtShipperAddress.text = mapFormData.value(forKey: "sb_s_address") as? String
        
        txtShipperDate.text = Utility.getDateFrom(mapFormData.value(forKey: "sb_s_date")as? String, givenFormat: "yyyy-MM-dd HH:mm:ss", returnFormat: "MM/dd/yyyy")
        shipperDate = Utility.getDateFrom(mapFormData.value(forKey: "sb_s_date")as? String, givenFormat: "yyyy-MM-dd HH:mm:ss", returnFormat: "yyyy-MM-dd hh:mm:ss") ?? ""
        
        txtSupplyDate.text = Utility.getDateFrom(mapFormData.value(forKey: "sb_supply_date")as? String, givenFormat: "yyyy-MM-dd HH:mm:ss", returnFormat: "MM/dd/yyyy")
        supplyDate = Utility.getDateFrom(mapFormData.value(forKey: "sb_supply_date")as? String, givenFormat: "yyyy-MM-dd HH:mm:ss", returnFormat: "yyyy-MM-dd hh:mm:ss") ?? ""
        
        
        if mapFormData.value(forKey: "document_required") as? String == "1" {
            imgRequire.image = UIImage(named:"check_blue.png")
            docRequired = "1"
        }
        else{
            imgRequire.image = UIImage(named:"check_gray.png")
            docRequired = "0"
        }
        
    }
    
    
    
    //MARK:- IBActions
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnDeliveryDateClicked(_ sender: Any) {
        self.view.endEditing(true)
        labelDateType.text = "Select Delivery Date"
        UIView.animate(withDuration: 0.3) {
            self.viewDatePicker.frame = CGRect(x: 0,
                                               y: ScreenSize.SCREEN_HEIGHT - self.viewDatePicker.frame.size.height,
                                               width: self.viewDatePicker.frame.size.width,
                                               height: self.viewDatePicker.frame.size.height)
        }
    }
    
    @IBAction func btnShipperDateClicked(_ sender: Any) {
        self.view.endEditing(true)
        labelDateType.text = "Select Shipper Date"
        UIView.animate(withDuration: 0.3) {
            self.viewDatePicker.frame = CGRect(x: 0,
                                               y: ScreenSize.SCREEN_HEIGHT - self.viewDatePicker.frame.size.height,
                                               width: self.viewDatePicker.frame.size.width,
                                               height: self.viewDatePicker.frame.size.height)
        }
    }
    
    @IBAction func btnSupplyDateClicked(_ sender: Any) {
        self.view.endEditing(true)
        labelDateType.text = "Select Supply Date"
        UIView.animate(withDuration: 0.3) {
            self.viewDatePicker.frame = CGRect(x: 0,
                                               y: ScreenSize.SCREEN_HEIGHT - self.viewDatePicker.frame.size.height,
                                               width: self.viewDatePicker.frame.size.width,
                                               height: self.viewDatePicker.frame.size.height)
        }
    }
    
    @IBAction func btnRequireClicked(_ sender : UIButton){
        
        if docRequired == "0" {
            imgRequire.image = UIImage(named:"check_blue.png")
            docRequired = "1"
        }
        else{
            imgRequire.image = UIImage(named:"check_gray.png")
            docRequired = "0"
        }
    }
    
    @IBAction func btnSaveClicked(_ sender : UIButton){
        if(txtLength.text!.isEmpty){
            showAlert(Appname, title: "Please enter length.")
        }
        else if txtWidth.text!.isEmpty {
            showAlert(Appname, title: "Please enter width.")
        }
        else if txtHeight.text!.isEmpty {
            showAlert(Appname, title: "Please enter height.")
        }
        else if txtUnitWeight.text!.isEmpty {
            showAlert(Appname, title: "Please enter unit wight.")
        }
        else if txtShipperName.text!.isEmpty {
            showAlert(Appname, title: "Please enter shipper name.")
        }
        else if txtShipperAddress.text!.isEmpty {
            showAlert(Appname, title: "Please enter shipper address.")
        }
        else if txtShipperDate.text!.isEmpty {
            showAlert(Appname, title: "Please select shipper date.")
        }
        else if txtSupplyDate.text!.isEmpty {
            showAlert(Appname, title: "Please select supply date.")
        }
        else{
            if Reachability.isConnectedToNetwork() == true {
                //SVProgressHUD.show(withStatus: "Loading..")
                self.postDataUpdateUserOrder()
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:
                    NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
        }
        
    }
    
    
    //MARK:- Tableview Delegate Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return APPDELEGATE.arrayMAPOredrDrug.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "reviewProductCell") as? MapOrderReviewProductCell
        
        if cell == nil {
            let nib  = Bundle.main.loadNibNamed("MapOrderReviewProductCell", owner: self, options: nil)
            cell = nib?[0] as? MapOrderReviewProductCell
        }
        
        cell?.labelHeader.text = "Product \(indexPath.row + 1)"
        
        cell?.txtName.text = (APPDELEGATE.arrayMAPOredrDrug[indexPath.row] as! NSDictionary).value(forKey: "product_name") as? String
        cell?.txtDesc.text = (APPDELEGATE.arrayMAPOredrDrug[indexPath.row] as! NSDictionary).value(forKey: "product_desc") as? String
        cell?.txtQuantity.text = (APPDELEGATE.arrayMAPOredrDrug[indexPath.row] as! NSDictionary).value(forKey: "qty") as? String
        cell?.txtPackageUnit.text = (APPDELEGATE.arrayMAPOredrDrug[indexPath.row] as! NSDictionary).value(forKey: "packaging_unit") as? String
        cell?.txtCountry.text = (APPDELEGATE.arrayMAPOredrDrug[indexPath.row] as! NSDictionary).value(forKey: "m_country") as? String
        
        cell?.txtUnitPrice.text = (APPDELEGATE.arrayMAPOredrDrug[indexPath.row] as! NSDictionary).value(forKey: "price") as? String
        //cell?.txtTotalValue.text = (APPDELEGATE.arrayMAPOredrDrug[indexPath.row] as! NSDictionary).value(forKey: "total_price") as? String ?? ""
        
        print("total val --> \((APPDELEGATE.arrayMAPOredrDrug[indexPath.row] as! NSDictionary).value(forKey: "total_price") as? String ?? "")")
        
        
        let qt = ((APPDELEGATE.arrayMAPOredrDrug[indexPath.row] as! NSDictionary).value(forKey: "qty") as! NSString).integerValue
        let price = ((APPDELEGATE.arrayMAPOredrDrug[indexPath.row] as! NSDictionary).value(forKey: "price") as! NSString).integerValue
        cell?.txtTotalValue.text = String(qt * price)
        
        
        cell?.txtUnitPrice.delegate = self
        cell?.txtUnitPrice.tag = indexPath.row
        cell?.txtUnitPrice.addTarget(self, action: #selector(countPrice(_:)), for: .editingChanged)
        cell?.txtUnitPrice.inputAccessoryView = numberToolbar
        
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        adjustViewHeight()
    }

    
    //MARK:- Web service call Methods
    
    func postDataUpdateUserOrder() {
        let completeURL = NSString(format:"%@%@", MainURL,editSavedMapUrl) as String
        
        var params:NSDictionary!
        
        let parameter = NSMutableDictionary()
        parameter.setValue(USERDEFAULT.value(forKey: "userID") as? String ?? "", forKey: "user_id")
        parameter.setValue(USERDEFAULT.value(forKey: "token") as? String ?? "", forKey: "token")
        parameter.setValue(Language_Type, forKey: "lang_type")
        parameter.setValue(mapFormData.value(forKey: "map_id")as? String, forKey: "map_id")
        
        //Section A Parameters
        parameter.setValue(deliveryDate, forKey: "sa_delievry_date")
        parameter.setValue(txtPackaging.text!, forKey: "sa_packaging")
        parameter.setValue(txtBatchNo.text!, forKey: "sa_batch_no")
        parameter.setValue(txtInBoundAWB.text!, forKey: "sa_inbound_hk_awbno")
        parameter.setValue(txtOutBoundAWB.text!, forKey: "sa_outbound_hk_date")
        parameter.setValue(txtCourierReferance.text!, forKey: "sa_courier_ref")
        parameter.setValue(txtConsignee.text!, forKey: "sa_consignee_airport")
        parameter.setValue(txtTrackingNo.text!, forKey: "sa_tracking_number")
        parameter.setValue(txtCourier.text!, forKey: "sa_courier")
        
        //Packagning
        parameter.setValue(txtHeight.text!, forKey: "height")
        parameter.setValue(txtWidth.text!, forKey: "width")
        parameter.setValue(txtLength.text!, forKey: "length")
        parameter.setValue(txtUnitWeight.text!, forKey: "unit_weight")
        
        //Products
        parameter.setValue(APPDELEGATE.arrayMAPOredrDrug, forKey: "products")
        
        //Insaurance Charge
        parameter.setValue(txtDiscountAmount.text!, forKey: "discount")
        parameter.setValue(txtHandlingCharge.text!, forKey: "handling_charge")
        parameter.setValue(txtFreightInsaurance.text!, forKey: "freight_insurance")
        parameter.setValue(txtServiesTax.text!, forKey: "vat")
        
        //Section B Parameters
        parameter.setValue(mapFormData.value(forKey: "sb_p_name")as? String, forKey: "sb_p_name")
        parameter.setValue(mapFormData.value(forKey: "sb_p_address")as? String, forKey: "sb_p_address")
        parameter.setValue(mapFormData.value(forKey: "sb_p_delivery_address")as? String, forKey: "sb_p_delivery_address")
        parameter.setValue(mapFormData.value(forKey: "sb_p_date")as? String, forKey: "sb_p_date")
        parameter.setValue(txtShipperName.text!, forKey: "sb_s_contact_name")
        parameter.setValue(txtShipperAddress.text!, forKey: "sb_s_address")
        parameter.setValue(shipperDate, forKey: "sb_s_date")
        parameter.setValue(supplyDate, forKey: "sb_supply_date")
        
        parameter.setValue(docRequired, forKey: "document_required")
        
        //Other
        parameter.setValue(mapFormData.value(forKey: "delivery_charge")as? String, forKey: "delivery_charge")
        parameter.setValue(mapFormData.value(forKey: "is_draft")as? String, forKey: "is_draft")
        parameter.setValue(mapFormData.value(forKey: "store_id")as? String, forKey: "store_id")
        parameter.setValue(mapFormData.value(forKey: "store_owner_id")as? String, forKey: "store_owner_id")
        parameter.setValue(mapFormData.value(forKey: "sub_total")as? String, forKey: "sub_total")
        parameter.setValue(mapFormData.value(forKey: "total_payment")as? String, forKey: "total_payment")
        parameter.setValue(mapFormData.value(forKey: "totalpurchase")as? String, forKey: "totalpurchase")
        parameter.setValue(mapFormData.value(forKey: "map_status")as? String, forKey: "map_status")
        
        params = parameter.mutableCopy() as! NSDictionary
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("Update Order detail URL :",completeURL)
        print("Update Order detail Parameters :",finalParams)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: editSavedMapUrlTag)
    }
    
    
    //MARK: - Web Service call back Methods
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
            
        case editSavedMapUrlTag:
            let resultDict = responseObject as! NSDictionary;
            print("review map detail Response  : \(resultDict)")
            SVProgressHUD.dismiss()
            if resultDict.value(forKey: "status") as! String == "1"{
                
                NotificationCenter.default.post(name: NSNotification.Name("refreshUserOrderList"), object:nil)
                NotificationCenter.default.post(name: NSNotification.Name("refreshOrderDetail"), object:nil)
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        default:
            break
        }
    }
    
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
            
        case editSavedMapUrlTag:
            SVProgressHUD.dismiss()
            
        default:
            break
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""),
                  title: NSLocalizedString(FailureAlert,tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    //MARK:- DatePicker Methods
    @IBAction func btnCancelDateClicked(_ sender: Any) {
        UIView.animate(withDuration: 0.3) {
            self.viewDatePicker.frame = CGRect(x: 0,
                                               y: ScreenSize.SCREEN_HEIGHT,
                                               width: self.viewDatePicker.frame.size.width,
                                               height: self.viewDatePicker.frame.size.height)
        }
    }
    
    @IBAction func btnDoneDateClicked(_ sender: Any) {
        if labelDateType.text == "Select Shipper Date" {
            shipperDate = Utility.getStringFrom(myDatePicker.date, format: "yyyy-MM-dd hh:mm:ss")
            txtShipperDate.text = getStringDateFromDate(dateFormat: "MM/dd/yyyy", enterDate: myDatePicker.date)
        }
        else if labelDateType.text == "Select Supply Date" {
            supplyDate = Utility.getStringFrom(myDatePicker.date, format: "yyyy-MM-dd hh:mm:ss")
            txtSupplyDate.text = getStringDateFromDate(dateFormat: "MM/dd/yyyy", enterDate: myDatePicker.date)
        }
        else if labelDateType.text == "Select Delivery Date" {
            deliveryDate = Utility.getStringFrom(myDatePicker.date, format: "yyyy-MM-dd hh:mm:ss")
            txtDeliveryDate.text = getStringDateFromDate(dateFormat: "MM/dd/yyyy", enterDate: myDatePicker.date)
        }
        UIView.animate(withDuration: 0.3) {
            self.viewDatePicker.frame = CGRect(x: 0,
                                               y: ScreenSize.SCREEN_HEIGHT,
                                               width: self.viewDatePicker.frame.size.width,
                                               height: self.viewDatePicker.frame.size.height)
        }
    }
    
    
    //MARK:- TextField delegate methods
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeField = textField
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if !(activeField == txtDiscountAmount) &&
            !(activeField == txtFreightInsaurance) &&
            !(activeField == txtHandlingCharge) &&
            !(activeField == txtServiesTax) &&
            !(activeField == txtLength) &&
            !(activeField == txtWidth) &&
            !(activeField == txtHeight) &&
            !(activeField == txtUnitWeight) &&
            !(activeField == txtShipperName) &&
            !(activeField == txtShipperAddress) &&
            !(activeField == txtPackaging) &&
            !(activeField == txtBatchNo) &&
            !(activeField == txtInBoundAWB) &&
            !(activeField == txtOutBoundAWB) &&
            !(activeField == txtCourierReferance) &&
            !(activeField == txtConsignee) &&
            !(activeField == txtTrackingNo) &&
            !(activeField == txtCourier){
            
            tableProductCart.reloadData()
            adjustViewHeight()
        }
        
        
        if txtDiscountAmount.text!.isEmpty {
            txtDiscountAmount.text = "0"
        }
        else if txtFreightInsaurance.text!.isEmpty {
            txtFreightInsaurance.text = "0"
        }
        else if txtHandlingCharge.text!.isEmpty {
            txtHandlingCharge.text = "0"
        }
        else if txtServiesTax.text!.isEmpty {
            txtServiesTax.text = "0"
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.returnKeyType == .done {
            textField.resignFirstResponder()
        }
        else{
            let nextTage=textField.tag+1;
            let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
            if (nextResponder != nil){
                nextResponder?.becomeFirstResponder()
            }
            else
            {
                textField.resignFirstResponder()
            }
        }
        return false
    }
    
    @IBAction func count() {
        let disc:Int = (txtDiscountAmount.text! as NSString).integerValue
        let freight:Int = (txtFreightInsaurance.text! as NSString).integerValue
        let hand:Int = (txtHandlingCharge.text! as NSString).integerValue
        let service:Int = (txtServiesTax.text! as NSString).integerValue
        
        
        let total:Int? = disc + freight + hand + service
        txtTotalAmount.text = "\(total!)"
        
        
    }
    
    @IBAction func countPrice(_ textfield: UITextField){
        productIndex = textfield.tag
        obj = ((APPDELEGATE.arrayMAPOredrDrug.object(at: textfield.tag))as! NSDictionary).mutableCopy() as! NSMutableDictionary
        let qty = (obj.value(forKey: "qty") as! NSString).integerValue
        let total = qty * (textfield.text! as NSString).integerValue
        print("Total = \(total)")
        obj.setValue(textfield.text!, forKey: "price")
    }
    
    //MARK:- Numeric Keyboard Hide Method
    func NavBarNumberPad(){
        
        numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        numberToolbar.barStyle = .default
        
        let viewBack = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 45))
        viewBack.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        numberToolbar.addSubview(viewBack)
        
        let btnCancel = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelNumberPad))
        btnCancel.tintColor = UIColor.white
        
        let btnDone = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.doneWithNumberPad))
        btnDone.tintColor = UIColor.white
        
        numberToolbar.items = NSArray(objects: btnCancel,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),btnDone) as? [UIBarButtonItem]
        
        
        numberToolbar.sizeToFit()
        txtLength.inputAccessoryView = numberToolbar
        txtWidth.inputAccessoryView = numberToolbar
        txtHeight.inputAccessoryView = numberToolbar
        txtUnitWeight.inputAccessoryView = numberToolbar
        
        txtDiscountAmount.inputAccessoryView = numberToolbar
        txtFreightInsaurance.inputAccessoryView = numberToolbar
        txtHandlingCharge.inputAccessoryView = numberToolbar
        txtServiesTax.inputAccessoryView = numberToolbar
        
    }
    
    func cancelNumberPad(){
        self.view .endEditing(true)
    }
    
    func doneWithNumberPad(){
        self.view .endEditing(true)
        
        if !(activeField == txtDiscountAmount) &&
            !(activeField == txtFreightInsaurance) &&
            !(activeField == txtHandlingCharge) &&
            !(activeField == txtServiesTax) &&
            !(activeField == txtLength) &&
            !(activeField == txtWidth) &&
            !(activeField == txtHeight) &&
            !(activeField == txtUnitWeight) &&
            !(activeField == txtShipperName) &&
            !(activeField == txtShipperAddress) &&
            !(activeField == txtPackaging) &&
            !(activeField == txtBatchNo) &&
            !(activeField == txtInBoundAWB) &&
            !(activeField == txtOutBoundAWB) &&
            !(activeField == txtCourierReferance) &&
            !(activeField == txtConsignee) &&
            !(activeField == txtTrackingNo) &&
            !(activeField == txtCourier){
            
            APPDELEGATE.arrayMAPOredrDrug.replaceObject(at: productIndex, with: obj)
            print("textfield is ok - \(APPDELEGATE.arrayMAPOredrDrug)")
            
            tableProductCart.reloadData()
            adjustViewHeight()
        }
        
        if txtDiscountAmount.text!.isEmpty {
            txtDiscountAmount.text = "0"
        }
        else if txtFreightInsaurance.text!.isEmpty {
            txtFreightInsaurance.text = "0"
        }
        else if txtHandlingCharge.text!.isEmpty {
            txtHandlingCharge.text = "0"
        }
        else if txtServiesTax.text!.isEmpty {
            txtServiesTax.text = "0"
        }
        
    }
    
    //MARK:-
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let loginAlert = UIAlertController(title: messageT, message: title, preferredStyle:.alert)
        let actionOK = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        let actionLogin = UIAlertAction(title: "LOGIN", style: .default) { (actionLogin) in
            APPDELEGATE.logOutUserWithoutToken()
            let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
        
        
        loginAlert.addAction(actionOK)
        loginAlert.addAction(actionLogin)
        
        if !(self.presentedViewController != nil) {
            self.present(loginAlert, animated: true, completion: nil)
        }
    }
    
    
}

