//
//  FavouriteStoreVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 03/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class FavouriteStoreVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var tableViewStore:UITableView!
    @IBOutlet var tableViewDoctor:UITableView!
    
    @IBOutlet var lblUnderStore:UILabel!
    @IBOutlet var lblUnderDoctor:UILabel!
    
    @IBOutlet var btnStore:UIButton!
    @IBOutlet var btnDoctor:UIButton!
    
    @IBOutlet var viewStore:UIView!
    @IBOutlet var viewDoctor:UIView!
    
    @IBOutlet var scrView:UIScrollView!
    //FOR SCROLLING VIEW
    var currentPag:Int!
    var pageCount:Int!
    
    
    var favouriteStoreData = NSMutableArray()
    var favouriteStoreGlobalData = NSMutableArray()
    
    var favouriteDoctorData = NSMutableArray()
    var favouriteDoctorGlobalData = NSMutableArray()
    
    
    //For Pasignation
    var pageNum:Int!
    var isLoading:Bool?
    
    var pageNum1:Int!
    var isLoading1:Bool?
    
    
    var favouriteStoreID:String!
    var favouriteDoctorID:String!
    
    var index:Int!
    
    @IBOutlet var lblNoFavouriteStore:UILabel!
    @IBOutlet var lblNoFavouriteDoctor:UILabel!
    
    @IBOutlet var viewUnderScorllView:UIView!
    
    let screenWidth = Int(ScreenSize.SCREEN_WIDTH)
    
    @IBOutlet var viewSearch:UIView!
    @IBOutlet var txtSearch:UITextField!
    @IBOutlet var headerView:UIView!
    
    var isStore:Bool?

    
    var singleStoreData = NSDictionary()
    var singleDoctorData = NSDictionary()
    

    @IBOutlet var btnHeaderFavorite:UILabel!
    
    var refreshControl: UIRefreshControl!

    
    // FIXME: - VIEW CONTROLLER METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
        
        self.viewSearch.frame = CGRect(x: self.headerView.frame.origin.x+self.headerView.frame.size.width, y: self.headerView.frame.origin.y, width: self.viewSearch.frame.size.width, height: self.viewSearch.frame.size.height)

        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        
        headerView.addSubview(viewSearch)

        
        pageCount=2;
        currentPag=0;
        
        scrView.contentSize = CGSize(width: CGFloat(screenWidth * pageCount), height: scrView.frame.size.height)
        
        viewStore.frame = CGRect(x: 0.0, y: viewStore.frame.origin.y, width: viewStore.frame.size.width, height: viewStore.frame.size.height)
        scrView.addSubview(viewStore)
        
        
        viewDoctor.frame = CGRect(x: viewStore.frame.origin.x+viewStore.frame.size.width, y: viewDoctor.frame.origin.y, width: viewDoctor.frame.size.width, height: viewDoctor.frame.size.height)
        scrView.addSubview(viewDoctor)
        
        self.settingBtnBgColor(btnStore)

        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.favouriteStoreData = NSMutableArray()
        self.favouriteStoreGlobalData = NSMutableArray()
        
        if Reachability.isConnectedToNetwork() == true {
         self.addLoadingIndicatiorOnFooterOnTableViewStore()
           Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetFavourtiteStore), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        
        
        
        pageNum1=1;
        let indicator2 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator2.startAnimating()
        
        self.favouriteDoctorData = NSMutableArray()
        self.favouriteDoctorGlobalData = NSMutableArray()
        
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableViewDoctor()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetFavourtiteDoctor), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        
        self.setDiviceSupport()

        self.addTapGestureInOurView()
        self.setLocalizationText()
        self.addPullRefreshForDoctor()
        self.addPullRefreshForStore()
        
    }
    
    func addPullRefreshForStore(){
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.gray
        refreshControl.addTarget(self, action: #selector(self.refreshStoreData(sender:)), for: .valueChanged)
        tableViewStore.addSubview(refreshControl)
    }
    func addPullRefreshForDoctor(){
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.gray
        refreshControl.addTarget(self, action: #selector(self.refreshDoctorData(sender:)), for: .valueChanged)
        tableViewDoctor.addSubview(refreshControl)
    }
    
    func refreshStoreData(sender:AnyObject) {
        // Code to refresh table view
        
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.favouriteStoreData = NSMutableArray()
        self.favouriteStoreGlobalData = NSMutableArray()
        self.tableViewStore.reloadData()
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableViewStore()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetFavourtiteStore), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }

        
        refreshControl.endRefreshing()
    }
    func refreshDoctorData(sender:AnyObject) {
        // Code to refresh table view
        pageNum1=1;
        let indicator2 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator2.startAnimating()
        
        self.favouriteDoctorData = NSMutableArray()
        self.favouriteDoctorGlobalData = NSMutableArray()
        self.tableViewDoctor.reloadData()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableViewDoctor()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetFavourtiteDoctor), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        refreshControl.endRefreshing()
    }

    
    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        if viewTouched!.isKind(of: UIButton.self){
            
        }
        else{
            self.view.endEditing(true)
        }
    }

    
    func settingBtnBgColor(_ sender:UIButton){
        
        if sender == btnStore{
            
            btnStore.setTitleColor(UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0), for: .normal)
            btnDoctor.setTitleColor(UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0), for: .normal)
            
            lblUnderDoctor.isHidden = true
            lblUnderStore.isHidden = false
            
            isStore = true
            txtSearch.text = ""
            txtSearch.attributedPlaceholder = NSAttributedString(string:"Search Store",
                                                                        attributes:nil)

        }
        else if sender == btnDoctor{
            
            btnDoctor.setTitleColor(UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0), for: .normal)
            btnStore.setTitleColor(UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0), for: .normal)
            
            lblUnderDoctor.isHidden = false
            lblUnderStore.isHidden = true
            
            isStore = false
            txtSearch.text = ""
            txtSearch.attributedPlaceholder = NSAttributedString(string:"Search Doctor",
                                                                        attributes:nil)
        }
        
    }
    func addLoadingIndicatiorOnFooterOnTableViewStore(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableViewStore.tableFooterView = spinner
    }
    
    func addLoadingIndicatiorOnFooterOnTableViewDoctor(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableViewDoctor.tableFooterView = spinner
    }
    
    
    
    func removeLoadingIndicatiorOnFooterOnTableViewStore(){
        tableViewStore.tableFooterView = nil
    }
    func removeLoadingIndicatiorOnFooterOnTableViewDoctor(){
        tableViewDoctor.tableFooterView = nil
    }
    
    func setupScrollViewWithCurrentPage(currentPage:Int){
        scrView.contentOffset = CGPoint(x: currentPag * screenWidth, y: 0)
    }
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    func setDiviceSupport()
    {
        if DeviceType.IS_IPHONE_4_OR_LESS
        {
            
            viewStore.frame = CGRect(x: viewStore.frame.origin.x, y: viewStore.frame.origin.y, width: viewStore.frame.size.width, height: viewStore.frame.size.height - 88.0)
            
            viewDoctor.frame = CGRect(x: viewDoctor.frame.origin.x, y: viewDoctor.frame.origin.y, width: viewDoctor.frame.size.width, height: viewDoctor.frame.size.height - 88.0)
            
            
            tableViewStore.frame = CGRect(x: tableViewStore.frame.origin.x, y: tableViewStore.frame.origin.y, width: tableViewStore.frame.size.width, height: tableViewStore.frame.size.height - 88.0)
            
            tableViewDoctor.frame = CGRect(x: tableViewDoctor.frame.origin.x, y: tableViewDoctor.frame.origin.y, width: tableViewDoctor.frame.size.width, height: tableViewDoctor.frame.size.height - 88.0)
            
            scrView.frame = CGRect(x: scrView.frame.origin.x, y: scrView.frame.origin.y, width: scrView.frame.size.width, height: scrView.frame.size.height - 88.0)
            
            viewUnderScorllView.frame = CGRect(x: viewUnderScorllView.frame.origin.x, y: viewUnderScorllView.frame.origin.y, width: viewUnderScorllView.frame.size.width, height: viewUnderScorllView.frame.size.height - 88.0)
            
            
        }
    }
    
    func setLocalizationText(){
        
        btnHeaderFavorite.text = NSLocalizedString("favourite", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblNoFavouriteStore.text = NSLocalizedString("nostore", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblNoFavouriteDoctor.text = NSLocalizedString("nodoctor", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
        btnStore.setTitle(NSLocalizedString("store", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        btnDoctor.setTitle(NSLocalizedString("doctor", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        
    }
    
    
    // TODO: - Textfield DELEGATE METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        return false
    }
    
    //MARK: - ACTION SHEET DELEGATE
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
        }
    }

    // TODO: - DELEGATE METHODS
    
    //ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.tag == 50) {
            currentPag = Int(scrollView.contentOffset.x) / Int(scrollView.frame.size.width);
            
            if (currentPag == 0) {
                self.settingBtnBgColor(btnStore)
            }
            else if (currentPag == 1){
                self.settingBtnBgColor(btnDoctor)
                let btn = UIButton.init()
                btn.tag = 101
                
                self.btnHeaderClicked(btn)
                
            }
            
            scrView.contentOffset = CGPoint(x: scrView.contentOffset.x, y: 0.0)
            
            
        }
        else  if scrollView == tableViewStore {
            if isLoading == true{
                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                    pageNum = pageNum + 1
                    print(pageNum)
                    isLoading = false
                    
                    if Reachability.isConnectedToNetwork() == true {
                        self.addLoadingIndicatiorOnFooterOnTableViewStore()
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetFavourtiteStore), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
            }
            
        }
        else  if scrollView == tableViewDoctor {
            if isLoading1 == true{
                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                    pageNum1 = pageNum1 + 1
                    print(pageNum1)
                    isLoading1 = false
                    
                    if Reachability.isConnectedToNetwork() == true {
                        self.addLoadingIndicatiorOnFooterOnTableViewDoctor()
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetFavourtiteDoctor), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
            }
            
        }
        
        
    }
    
    
    //TableView Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 100 {
            return self.favouriteStoreData.count
        }else if tableView.tag == 101{
            return self.favouriteDoctorData.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 100{
            let identifier = "favouriteStoreCell"
            
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FvouriteStoreTableVC
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("FvouriteStoreTableVC", owner: self, options: nil)
                cell = nib?[0] as? FvouriteStoreTableVC
            }
            cell!.selectionStyle = .none;
            
            

            
            if let storename = (self.favouriteStoreData[indexPath.row] as AnyObject).value(forKey: "firstname") as? String{
                cell?.lblStoreName.text = storename
            }
            
            if let storeaddress = (self.favouriteStoreData[indexPath.row] as AnyObject).value(forKey: "address") as? String{
                cell?.lblStoreDescription.text = storeaddress
            }
            
            
            if let favour =  (self.favouriteStoreData[indexPath.row] as AnyObject).value(forKey: "is_favourite") as? NSNumber
            {
                let favourateArtical = "\(favour)"
                if favourateArtical == "1"{
                    cell?.imageViewFavourite.image = UIImage.init(named: "like.png")
                }
                else if favourateArtical == "0"{
                    cell?.imageViewFavourite.image = UIImage.init(named: "unlike.png")
                }
            }
            else if let favour =  (self.favouriteStoreData[indexPath.row] as AnyObject).value(forKey: "is_favourite") as? String
            {
                let favourateArtical = "\(favour)"
                if favourateArtical == "1"{
                    cell?.imageViewFavourite.image = UIImage.init(named: "like.png")
                }
                else if favourateArtical == "0"{
                    cell?.imageViewFavourite.image = UIImage.init(named: "unlike.png")
                }
            }
            
            cell?.btnFavourite.tag = indexPath.row
            cell?.btnFavourite.addTarget(self, action: #selector(self.btnFavouriteStoredClicked(_:)), for: .touchUpInside)
            
            
            return cell!
            
        }
        else {
            let identifier = "favouriteStoreCell"
            
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FvouriteStoreTableVC
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("FvouriteStoreTableVC", owner: self, options: nil)
                cell = nib?[0] as? FvouriteStoreTableVC
            }
            cell!.selectionStyle = .none;
            
            if let storename = (self.favouriteDoctorData[indexPath.row] as AnyObject).value(forKey: "first_name") as? String{
                cell?.lblStoreName.text = storename
            }
            
            if let storeaddress = (self.favouriteDoctorData[indexPath.row] as AnyObject).value(forKey: "address") as? String{
                cell?.lblStoreDescription.text = storeaddress
            }
            
            
            if let favour =  (self.favouriteDoctorData[indexPath.row] as AnyObject).value(forKey: "is_favourite") as? NSNumber
            {
                let favourateArtical = "\(favour)"
                if favourateArtical == "1"{
                    cell?.imageViewFavourite.image = UIImage.init(named: "like.png")
                }
                else if favourateArtical == "0"{
                    cell?.imageViewFavourite.image = UIImage.init(named: "unlike.png")
                }
            }
            else if let favour =  (self.favouriteDoctorData[indexPath.row] as AnyObject).value(forKey: "is_favourite") as? String
            {
                let favourateArtical = "\(favour)"
                if favourateArtical == "1"{
                    cell?.imageViewFavourite.image = UIImage.init(named: "like.png")
                }
                else if favourateArtical == "0"{
                    cell?.imageViewFavourite.image = UIImage.init(named: "unlike.png")
                }
            }
            
            cell?.btnFavourite.tag = indexPath.row
            cell?.btnFavourite.addTarget(self, action: #selector(self.btnFavouriteDoctorClicked(_:)), for: .touchUpInside)

            
            
            return cell!
            
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 100 {
            let storeDetailVC = StoreDetailVC(nibName: "StoreDetailVC", bundle: nil)
            storeDetailVC.storeDetailData = self.favouriteStoreData[indexPath.row] as! NSDictionary
            self.navigationController?.pushViewController(storeDetailVC, animated: true)

        }else if tableView.tag == 101{
            let doctorDetailVC = DoctorDetailVC(nibName: "DoctorDetailVC", bundle: nil)
            doctorDetailVC.doctorDetailData = self.favouriteDoctorData[indexPath.row] as! NSDictionary
            self.navigationController?.pushViewController(doctorDetailVC, animated: true)
            
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 100 {
            return 80.0
        }else if tableView.tag == 101{
            return 80.0
        }
        return 0.0
    }

    
    
    // TODO: - ACTION METHODS
    
    @IBAction func btnHeaderClicked(_ sender:UIButton){
        
        if sender.tag == 10 {
            currentPag=0;
            self.setupScrollViewWithCurrentPage(currentPage: currentPag)
            self.settingBtnBgColor(btnStore)
            
        }else if sender.tag == 11 {
            currentPag=1;
            self.setupScrollViewWithCurrentPage(currentPage: currentPag)
            self.settingBtnBgColor(btnDoctor)
        }
        
        
    }
    @IBAction func btnBackClicked(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnFavouriteStoredClicked(_ sender:UIButton){
        
        let ifFav = (self.favouriteStoreData[sender.tag] as AnyObject).value(forKey: "is_favourite") as? String
        favouriteStoreID = (self.favouriteStoreData[sender.tag] as AnyObject).value(forKey: "store_id") as? String
        
        if ifFav == "1"{
            let temp = (self.favouriteStoreData[sender.tag] as! NSDictionary).mutableCopy() as! NSMutableDictionary
            print(temp)
            temp.setValue("0", forKey: "is_favourite")
         //
         //   if (self.favouriteStoreData.count > sender.tag) {
         //       self.favouriteStoreData.replaceObject(at: sender.tag, with: temp)
         //       self.favouriteStoreGlobalData.replaceObject(at: sender.tag, with: temp)
         //       let indexPath = NSIndexPath(row: sender.tag, section: 0)
         //       tableViewStore.reloadRows(at: [indexPath as IndexPath], with: .none)
         //
         //   }
            
            singleStoreData = temp.mutableCopy() as! NSDictionary
            print("Single Store Data : ",singleStoreData)

            
            self.favouriteStoreData.removeObject(at: sender.tag)
            self.favouriteStoreGlobalData.removeObject(at: sender.tag)
            if self.favouriteStoreData.count == 0{
                self.tableViewStore.isHidden = true
                self.lblNoFavouriteStore.isHidden = false
            }
            else{
                self.tableViewStore.isHidden = false
                self.lblNoFavouriteStore.isHidden = true
            }
            tableViewStore.reloadData()

            if Reachability.isConnectedToNetwork() == true {
                self.performSelector(inBackground: #selector(self.postDataOnWebserviceForRemoveFavouriteStore), with: nil)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
        }
        else{
            
            let temp = (self.favouriteStoreData[sender.tag] as! NSDictionary).mutableCopy() as! NSMutableDictionary
            print(temp)
            temp.setValue("1", forKey: "is_favourite")
            
            if (self.favouriteStoreData.count > sender.tag) {
                self.favouriteStoreData.replaceObject(at: sender.tag, with: temp)
                self.favouriteStoreGlobalData.replaceObject(at: sender.tag, with: temp)
                let indexPath = NSIndexPath(row: sender.tag, section: 0)
                tableViewStore.reloadRows(at: [indexPath as IndexPath], with: .none)
            }
            
          //  if Reachability.isConnectedToNetwork() == true {
          //      self.performSelector(inBackground: #selector(self.postDataOnWebserviceForAddFavouriteStore), with: nil)
          //  }else{
          //      showAlert("Check Connection", title: "Internet is not available.")
          //  }
            
        }
    }
    
    @IBAction func btnFavouriteDoctorClicked(_ sender:UIButton){
        
        let ifFav = (self.favouriteDoctorData[sender.tag] as AnyObject).value(forKey: "is_favourite") as? String
        favouriteDoctorID = (self.favouriteDoctorData[sender.tag] as AnyObject).value(forKey: "d_id") as? String
        
        if ifFav == "1"{
            let temp = (self.favouriteDoctorData[sender.tag] as! NSDictionary).mutableCopy() as! NSMutableDictionary
            print(temp)
            temp.setValue("0", forKey: "is_favourite")
          
          //  if (self.favouriteDoctorData.count > sender.tag) {
          //      self.favouriteDoctorData.replaceObject(at: sender.tag, with: temp)
          //      self.favouriteDoctorGlobalData.replaceObject(at: sender.tag, with: temp)
          //      let indexPath = NSIndexPath(row: sender.tag, section: 0)
          //      tableViewDoctor.reloadRows(at: [indexPath as IndexPath], with: .none)
          //  }

            singleDoctorData = temp.mutableCopy() as! NSDictionary
            print("Single Doctor Data : ",singleDoctorData)

            
            self.favouriteDoctorData.removeObject(at: sender.tag)
            self.favouriteDoctorGlobalData.removeObject(at: sender.tag)
            
            if self.favouriteDoctorData.count == 0{
                self.tableViewDoctor.isHidden = true
                self.lblNoFavouriteDoctor.isHidden = false
            }
            else{
                self.tableViewDoctor.isHidden = false
                self.lblNoFavouriteDoctor.isHidden = true
            }
            tableViewDoctor.reloadData()
            
            
            if Reachability.isConnectedToNetwork() == true {
                self.performSelector(inBackground: #selector(self.postDataOnWebserviceForRemoveFavouriteDoctor), with: nil)
            }else{
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
        }
        else{
            let temp = (self.favouriteDoctorData[sender.tag] as! NSDictionary).mutableCopy() as! NSMutableDictionary
            print(temp)
            temp.setValue("1", forKey: "is_favourite")
            
            if (self.favouriteDoctorData.count > sender.tag) {
                self.favouriteDoctorData.replaceObject(at: sender.tag, with: temp)
                self.favouriteDoctorGlobalData.replaceObject(at: sender.tag, with: temp)
                let indexPath = NSIndexPath(row: sender.tag, section: 0)
                tableViewDoctor.reloadRows(at: [indexPath as IndexPath], with: .none)
            }
            
         //   if Reachability.isConnectedToNetwork() == true {
         //       self.performSelector(inBackground: #selector(self.postDataOnWebserviceForAddFavouriteDoctor), with: nil)
         //   }else{
         //       showAlert("Check Connection", title: "Internet is not available.")
         //   }
            
        }
    }
    
    @IBAction func btnSearchClicked(_ sender:UIButton){
        txtSearch.becomeFirstResponder()
        
        UIView.animate(withDuration: 0.3) {
            
            self.viewSearch.frame = CGRect(x: self.headerView.frame.origin.x, y: self.headerView.frame.origin.y, width: self.viewSearch.frame.size.width, height: self.viewSearch.frame.size.height)
            
        }
        
    }
    
    @IBAction func btnSearchCancelClicked(_ sender:UIButton){
        txtSearch.text = ""
        txtSearch.resignFirstResponder()
        
        UIView.animate(withDuration: 0.3) {
            
            self.viewSearch.frame = CGRect(x: self.headerView.frame.origin.x+self.headerView.frame.size.width, y: self.headerView.frame.origin.y, width: self.viewSearch.frame.size.width, height: self.viewSearch.frame.size.height)
            
        }
        
        if  isStore == true{
            favouriteStoreData = favouriteStoreGlobalData.mutableCopy() as! NSMutableArray
            if favouriteStoreData.count == 0 {
                lblNoFavouriteStore.isHidden = false
                tableViewStore.isHidden = true
            }
            else{
                tableViewStore.isHidden = false
                lblNoFavouriteStore.isHidden = true
            }
            tableViewStore.reloadData()
        }
        else{
            favouriteDoctorData = favouriteDoctorGlobalData.mutableCopy() as! NSMutableArray
            if favouriteDoctorData.count == 0 {
                lblNoFavouriteDoctor.isHidden = false
                tableViewDoctor.isHidden = true
            }
            else{
                tableViewDoctor.isHidden = false
                lblNoFavouriteDoctor.isHidden = true
            }
            tableViewDoctor.reloadData()
        }
        
        
    }
    
    @IBAction func searchFilter(){
        
        if  isStore == true{
            var predicate = String()
            
            
            if (txtSearch.text == "") {
                favouriteStoreData = favouriteStoreGlobalData.mutableCopy() as! NSMutableArray
            }else{
                predicate = String(format: "SELF['firstname'] contains[c] '%@'", txtSearch.text!)
                print(predicate)
                
                let myPredicate:NSPredicate = NSPredicate(format:predicate)
                let temp = self.favouriteStoreGlobalData.mutableCopy() as! NSArray
                self.favouriteStoreData = (temp.filtered(using: myPredicate) as NSArray).mutableCopy() as! NSMutableArray
                
                
            }
            
            if favouriteStoreData.count == 0 {
                lblNoFavouriteStore.isHidden = false
                tableViewStore.isHidden = true
            }
            else{
                tableViewStore.isHidden = false
                lblNoFavouriteStore.isHidden = true
            }
            tableViewStore.reloadData()
        }
        else{
            var predicate = String()
            if (txtSearch.text == "") {
                favouriteDoctorData = favouriteDoctorGlobalData.mutableCopy() as! NSMutableArray
            }else{
                predicate = String(format: "SELF['first_name'] contains[c] '%@'", txtSearch.text!)
                print(predicate)
                
                let myPredicate:NSPredicate = NSPredicate(format:predicate)
                let temp = self.favouriteDoctorGlobalData.mutableCopy() as! NSArray
                self.favouriteDoctorData = (temp.filtered(using: myPredicate) as NSArray).mutableCopy() as! NSMutableArray
                
                
            }
            
            if favouriteDoctorData.count == 0 {
                lblNoFavouriteDoctor.isHidden = false
                tableViewDoctor.isHidden = true
            }
            else{
                tableViewDoctor.isHidden = false
                lblNoFavouriteDoctor.isHidden = true
            }
            tableViewDoctor.reloadData()
        }
        
        
    }
    
    

    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForGetFavourtiteStore(){
        let completeURL = NSString(format:"%@%@", MainURL,getFavouriteStoreURL) as String
        
        let pageNumber = "\(pageNum!)"

        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "page":pageNumber,
            "limit":PAGINATION_LIMITE
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetFavourtiteStore API Parameter :",finalParams)
        print("GetFavourtiteStore API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getFavouriteStoreURLTag)
        
    }
    func postDataOnWebserviceForGetFavourtiteDoctor(){
        let completeURL = NSString(format:"%@%@", MainURL,getFavouriteDoctorURL) as String
        
        let pageNumber = "\(pageNum1!)"

        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "page":pageNumber,
            "limit":PAGINATION_LIMITE
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetFavourtiteDoctor API Parameter :",finalParams)
        print("GetFavourtiteDoctor API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getFavouriteDoctorURLTag)
        
    }
    
    func postDataOnWebserviceForRemoveFavouriteStore(){
        let completeURL = NSString(format:"%@%@", MainURL,removeFavouriteStoreURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "store_id":favouriteStoreID
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("RemoveFavouriteStore API Parameter :",finalParams)
        print("RemoveFavouriteStore API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: removeFavouriteStoreURLTag)
    }
    func postDataOnWebserviceForRemoveFavouriteDoctor(){
        let completeURL = NSString(format:"%@%@", MainURL,removeFavouriteDoctorURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "d_id":favouriteDoctorID
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("RemoveFavouriteStore API Parameter :",finalParams)
        print("RemoveFavouriteStore API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: removeFavouriteDoctorURLTag)
    }

    
    
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case getFavouriteStoreURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("GetFavourtiteStore Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum == 1{
                    self.favouriteStoreData = NSMutableArray()
                    self.favouriteStoreGlobalData = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    //                                self.dishGlobalData.addObjects(from: (myData) as! [Any])
                    for i in 0...myData.count - 1 {
                        self.favouriteStoreGlobalData.add(myData[i])
                    }
                    
                    for i in 0...myData.count - 1 {
                        self.favouriteStoreData.add(myData[i])
                    }
                    
                    
                    //                                self.searchStatusFilter()
                    
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum > 0) {
                            self.pageNum = self.pageNum - 1
                        }
                        self.isLoading = false
                    }else{
                        self.isLoading = true
                    }
                }
                else{
                    self.isLoading = false
                    if (self.pageNum > 0) {
                        self.pageNum = self.pageNum - 1
                    }
                    
                    if self.favouriteStoreData.count == 0{
                        self.tableViewStore.isHidden = true
                        self.lblNoFavouriteStore.isHidden = false
                    }
                    else{
                        self.tableViewStore.isHidden = false
                        self.lblNoFavouriteStore.isHidden = true
                    }
                 }
                
                self.tableViewStore.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableViewStore()

                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        case getFavouriteDoctorURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("GetFavourtiteDoctor Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum1 == 1{
                    self.favouriteDoctorData = NSMutableArray()
                    self.favouriteDoctorGlobalData = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    //                                self.dishGlobalData.addObjects(from: (myData) as! [Any])
                    for i in 0...myData.count - 1 {
                        self.favouriteDoctorGlobalData.add(myData[i])
                    }
                    
                    for i in 0...myData.count - 1 {
                        self.favouriteDoctorData.add(myData[i])
                    }
                    
                    
                    //                                self.searchStatusFilter()
                    
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum1 > 0) {
                            self.pageNum1 = self.pageNum1 - 1
                        }
                        self.isLoading1 = false
                    }else{
                        self.isLoading1 = true
                    }
                }
                else{
                    self.isLoading1 = false
                    if (self.pageNum1 > 0) {
                        self.pageNum1 = self.pageNum1 - 1
                    }
                    
                    if self.favouriteDoctorData.count == 0{
                        self.tableViewDoctor.isHidden = true
                        self.lblNoFavouriteDoctor.isHidden = false
                    }
                    else{
                        self.tableViewDoctor.isHidden = false
                        self.lblNoFavouriteDoctor.isHidden = true
                    }
                }
                
                self.tableViewDoctor.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableViewDoctor()
                
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            break
        case removeFavouriteStoreURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("RemoveFavouriteStore Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                
                let notificationName = Notification.Name("refreshFavouriteStoreData")
//                NotificationCenter.default.post(name: notificationName, object: nil)
                NotificationCenter.default.post(name: notificationName, object: nil, userInfo: self.singleStoreData as? [AnyHashable : Any])

                
//                let notificationName = Notification.Name("refreshStoreData")
//                // NotificationCenter.default.post(name: notificationName, object: nil)
//                NotificationCenter.default.post(name: notificationName, object: nil, userInfo: self.singleStoreData as? [AnyHashable : Any])


            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }

            
            break
        case removeFavouriteDoctorURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("RemoveFavouriteDoctor Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                let notificationName = Notification.Name("refreshFavouriteDoctorData")
                NotificationCenter.default.post(name: notificationName, object: nil, userInfo: self.singleDoctorData as? [AnyHashable : Any])

//                NotificationCenter.default.post(name: notificationName, object: nil)
                
//                let notificationName = Notification.Name("refreshDoctorData")
//                // NotificationCenter.default.post(name: notificationName, object: nil)
//                NotificationCenter.default.post(name: notificationName, object: nil, userInfo: self.singleDoctorData as? [AnyHashable : Any])

            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }

            
            break

        default:
            break
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case getFavouriteStoreURLTag:
            self.isLoading = false
            if (self.pageNum > 0) {
                self.pageNum = self.pageNum - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableViewStore()

            break
            
        case getFavouriteDoctorURLTag:
            self.isLoading1 = false
            if (self.pageNum1 > 0) {
                self.pageNum1 = self.pageNum1 - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableViewDoctor()
            
            break

        case removeFavouriteStoreURLTag:
            break
        case removeFavouriteDoctorURLTag:
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    

}
