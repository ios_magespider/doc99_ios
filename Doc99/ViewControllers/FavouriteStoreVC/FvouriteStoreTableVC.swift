//
//  FvouriteStoreTableVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 03/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class FvouriteStoreTableVC: UITableViewCell {

    @IBOutlet var lblStoreName:UILabel!
    @IBOutlet var lblStoreDescription:UILabel!
    @IBOutlet var imageViewFavourite:UIImageView!
    @IBOutlet var btnFavourite:UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
