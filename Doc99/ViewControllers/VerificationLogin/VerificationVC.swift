//
//  VerificationVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 06/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class VerificationVC: UIViewController,UITextFieldDelegate {

    @IBOutlet var txtVerificationCode:UITextField!
    @IBOutlet var lblEmailID:UILabel!
    @IBOutlet var scrMain:UIScrollView!

    var emailID:String!
    var isFrom:String!
    var ccCode:String!
    var timer = Timer()
    var byPassScreenName:String?
    var count : Int = 60

    
    @IBOutlet var lblDescription:UILabel!
    @IBOutlet var lblVerificationCode:UILabel!
    @IBOutlet var btnResendVarificationCode:UIButton!
    @IBOutlet var btnAlreadyAccount:UIButton!
    @IBOutlet var lblRequestCode:UILabel!
    @IBOutlet weak var lblTimer: UILabel!
    

   

    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblEmailID.text = "\(ccCode!) \(emailID!)"
        txtVerificationCode.delegate = self
        
            if #available(iOS 12.0, *) {
                txtVerificationCode.becomeFirstResponder()
                txtVerificationCode.textContentType = .oneTimeCode
            } else {
                // Fallback on earlier versions
            }
        self.startTimer()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // TODO: - OTHER METHODS
    func startTimer() {
        self.timer = Timer.scheduledTimer(timeInterval:1, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
    }
    func update() {
        count -= 1
        if count != 0 {
            lblTimer.text = "\(count)"
        }else{
            btnResendVarificationCode.isHidden = false
            lblTimer.isHidden = true
            timer.invalidate()
        }
        
     
    }
    func generalViewControllerSetting(){
        self.addTapGestureInOurView()
        //self.setLocalizationText()
    }
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        if viewTouched!.isKind(of: UIButton.self){
            
        }
        else{
            self.view.endEditing(true)
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        }
    }

    func setLocalizationText(){
        
        lblDescription.text = NSLocalizedString("verificationdescription", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblVerificationCode.text = NSLocalizedString("verificationcode", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblRequestCode.text = NSLocalizedString("requestcode", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
        btnAlreadyAccount.setTitle(NSLocalizedString("alreadyaccount", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        btnResendVarificationCode.setTitle(NSLocalizedString("resendverification", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)

    }
    
    
    // TODO: - DELEGATE METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
            
        }
        return false
    }
    @IBAction func countLength(_ sender: Any) {
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var YOffset:Int = 0
        
        if (textField.tag==4) {
            YOffset=20
        }
        if (DeviceType.IS_IPHONE_4_OR_LESS) {
            
            if (textField.tag==4) {
                YOffset=25
            }
        }
        scrMain.setContentOffset(CGPoint(x: 0, y: CGFloat(textField.tag * YOffset)), animated: true)
        return true
    }
    

    // TODO: - ACTION METHODS
    @IBAction func btnVerifyCodeClicked(_ sender:UIButton){
        if txtVerificationCode.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter verification code", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
        else{
            if isFrom == "ForgetPassword"{
                if Reachability.isConnectedToNetwork() == true {
                    SVProgressHUD.show(withStatus: "Loading..")
                    Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForVerifyCode), userInfo: nil, repeats: false)
                } else {
                    showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                }
            }
            else{
                if Reachability.isConnectedToNetwork() == true {
                    SVProgressHUD.show(withStatus: "Loading..")
                    Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForVerifyUser), userInfo: nil, repeats: false)
                } else {
                    showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                }

            }
        }
    }
    
    @IBAction func btnAlreadyAccountClicked(_ sender:UIButton){
        let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
        self.navigationController?.pushViewController(loginVC, animated: true)

    }
    
    @IBAction func btnResendCodeClicked(_ sender:UIButton){
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForResendCode), userInfo: nil, repeats: false)
           
            
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }

    }


    func postDataOnWebserviceForVerifyUser(){
        let completeURL = NSString(format:"%@%@", MainURL,verifyUserURL) as String
        
        let params:NSDictionary = [
            "phone" : emailID,
            "ccode": ccCode,
            "activation_code":txtVerificationCode.text!,
            "lang_type":Language_Type,
            "device_type":"1",
            "device_token":"54sfdsfdsf6d54f98s4df6sdf"
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("VerifyUser API Parameter :",finalParams)
        print("VerifyUser API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: verifyUserURLTag)
        
    }
    
    func postDataOnWebserviceForResendCode(){
        let completeURL = NSString(format:"%@%@", MainURL,resendCodeURL) as String
        
        let params:NSDictionary = [
            "phone" : emailID,
            "ccode" : ccCode,
            "lang_type":Language_Type
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("ResendCode API Parameter :",finalParams)
        print("ResendCode API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: resendCodeURLTag)
        
    }
    
    func postDataOnWebserviceForVerifyCode(){
         let completeURL = NSString(format:"%@%@", MainURL,verifyCodeURL) as String
         
         let params:NSDictionary = [
         "phone" : emailID,
         "ccode" : ccCode,
         "verify_code":txtVerificationCode.text!,
         "lang_type":Language_Type
         ]
         
         let finalParams:NSDictionary = [
         "data" : params
         ]
         
         print("ForgetPassword API Parameter :",finalParams)
         print("ForgetPassword API URL :",completeURL)
         
         let sampleProtocol = SyncManager()
         sampleProtocol.delegate = self
         sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: verifyCodeURLTag)
     
     }

    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case verifyUserURLTag:
            let resultDict = responseObject as! NSDictionary;
            
            if resultDict.value(forKey: "status") as! String == "1"{
                print("VerifyUser Response  : \(resultDict)")
               
                
                if isFrom == "ForgetPassword"{
                    let newPasswordVC = NewPasswordVC(nibName: "NewPasswordVC", bundle: nil)
                    newPasswordVC.emailID = emailID
                    self.navigationController?.pushViewController(newPasswordVC, animated: true)

                }
                else if isFrom == "FBLogin"{
                    
                    let dataDictionary = resultDict.value(forKey: "data") as! NSDictionary
                    
                    USERDEFAULT.setValue(dataDictionary.value(forKey: "user_id") as! String, forKey: "userID")
                    USERDEFAULT.synchronize()
                    
                    USERDEFAULT.setValue(dataDictionary.value(forKey: "token") as! String, forKey: "token")
                    USERDEFAULT.synchronize()
                    
                    USERDEFAULT.setValue(dataDictionary.value(forKey: "email") as! String, forKey: "emailID")
                    USERDEFAULT.synchronize()
                    
                    USERDEFAULT.setValue(dataDictionary.value(forKey: "fullname") as! String, forKey: "fullName")
                    USERDEFAULT.synchronize()
                    
                    USERDEFAULT.setValue(dataDictionary.value(forKey: "is_member") as! String, forKey: "isMember")
                    USERDEFAULT.synchronize()
                    
                    
                    let homeVC = TabBarVC(nibName: "TabBarVC", bundle: nil)
                    homeVC.selectedIndexOfMyTabbarController = 0
                    self.navigationController?.pushViewController(homeVC, animated: true)
                    
                }
                else{
                    
                        self.timer.invalidate()
                        let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                        loginVC.byPassScreenName = byPassScreenName
                        self.navigationController?.pushViewController(loginVC, animated: true)
                }
             
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
                btnResendVarificationCode.isHidden = false
                lblTimer.isHidden = true
            }
            SVProgressHUD.dismiss()
            break
        case resendCodeURLTag:
            let resultDict = responseObject as! NSDictionary;
            
            if resultDict.value(forKey: "status") as! String == "1"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
                btnResendVarificationCode.isHidden = true
                lblTimer.isHidden = false
                self.count = 60
                self.startTimer()
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
                btnResendVarificationCode.isHidden = false
                lblTimer.isHidden = true
            }
           
            SVProgressHUD.dismiss()
            break
        case verifyCodeURLTag:
             let resultDict = responseObject as! NSDictionary;
             print("VerifyCode Response  : \(resultDict)")
             
             if resultDict.value(forKey: "status") as! String == "1"{

            self.timer.invalidate()
             let newPasswordVC = NewPasswordVC(nibName: "NewPasswordVC", bundle: nil)
             newPasswordVC.emailID = emailID
             newPasswordVC.ccCode = self.ccCode
             self.navigationController?.pushViewController(newPasswordVC, animated: true)
                
             }
             else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
                btnResendVarificationCode.isHidden = false
                lblTimer.isHidden = true
                
             }
             SVProgressHUD.dismiss()
        break

            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case verifyUserURLTag:
            SVProgressHUD.dismiss()
            break
        case resendCodeURLTag:
            SVProgressHUD.dismiss()
            break
        case verifyCodeURLTag:
         SVProgressHUD.dismiss()
        break
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }

    

    

}
