//
//  OrderHistoryTableVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 12/07/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class OrderHistoryTableVC: UITableViewCell {

    @IBOutlet var lblTransactionNumber:UILabel!
    @IBOutlet var lblDate:UILabel!
    @IBOutlet var lblTicketNumber:UILabel!
    @IBOutlet var lblStatus:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
