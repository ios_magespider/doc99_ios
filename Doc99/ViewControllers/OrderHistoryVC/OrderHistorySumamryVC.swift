//
//  OrderHistorySumamryVC.swift
//  Doc99
//
//  Created by MacBookPro on 26/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD

class OrderHistorySumamryVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UIAlertViewDelegate,MKMapViewDelegate {

    // FIXME: - VARIABLE
    @IBOutlet var viewField:UIView!
    @IBOutlet var scrMain:UIScrollView!
    
    @IBOutlet var lblPatientName:UILabel!
    @IBOutlet var lblPatientGender:UILabel!
    @IBOutlet var lblPatientAddress:UILabel!

    @IBOutlet var lblStoreName:UILabel!
    @IBOutlet var lblStoreAddress:UILabel!
    @IBOutlet var lblConvenienceFee:UILabel!
    @IBOutlet var lblPaybleFee:UILabel!
    
    @IBOutlet var lblTransactionNumber:UILabel!
    @IBOutlet var lblTicketNumber:UILabel!
    @IBOutlet var lblTransactionDate:UILabel!

    
    @IBOutlet var collectionViewPrescriptionImages:UICollectionView!

    var preImagesData = NSMutableArray()

    let reuseIdentifierForImageUpload = "prescriptionImageCell"

    
    @IBOutlet var viewPreImagesInfo:UIView!
    @IBOutlet var viewStoreInfo:UIView!
    @IBOutlet var viewExtraInfo:UIView!
    @IBOutlet var viewBillSummaryInfo:UIView!
    @IBOutlet var viewTransactionInfo:UIView!
    @IBOutlet var viewFooter:UIView!

    
    @IBOutlet var lblStoreTitle:UILabel!
    @IBOutlet var lblExtraInfoTitle:UILabel!
    @IBOutlet var lblBillSummaryTitle:UILabel!
    @IBOutlet var lblPatientTitle:UILabel!
    @IBOutlet var lblUploadPrescription:UILabel!
    @IBOutlet var lblHeaderTitle:UILabel!
    @IBOutlet var lblPayment:UILabel!
    @IBOutlet var lblTransactionTitle:UILabel!

    var orderHistoryID:String!
    
    var orderHistoryData:NSDictionary!

    @IBOutlet var myMapView:MKMapView!
    var pinAnnotationView:MKPinAnnotationView!
    
    @IBOutlet var lblStatus1:UILabel!
    @IBOutlet var lblStatus2:UILabel!
    @IBOutlet var lblStatus3:UILabel!

    @IBOutlet var lblStatusTitle1:UILabel!
    @IBOutlet var lblStatusTitle2:UILabel!
    @IBOutlet var lblStatusTitle3:UILabel!
    
    @IBOutlet var lblStatusLine1:UILabel!
    @IBOutlet var lblStatusLine2:UILabel!
    
    
    // FIXME: - VIEW CONTROLLER METHODS

    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        
        self.setOrderHistorySummary(historyDetail: orderHistoryData)

//        if Reachability.isConnectedToNetwork() == true {
//            SVProgressHUD.show(withStatus: "Loading..")
//            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetOrderHistoryDetail), userInfo: nil, repeats: false)
//        } else {
//            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
//        }
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        
        lblStatus1.layer.cornerRadius = lblStatus1.frame.size.width / 2
        lblStatus1.layer.masksToBounds = true
        lblStatus2.layer.cornerRadius = lblStatus2.frame.size.width / 2
        lblStatus2.layer.masksToBounds = true
        lblStatus3.layer.cornerRadius = lblStatus3.frame.size.width / 2
        lblStatus3.layer.masksToBounds = true

        self.collectionViewPrescriptionImages.register(UINib(nibName: "UploadPreCollectionVC", bundle: nil), forCellWithReuseIdentifier: reuseIdentifierForImageUpload)

        
        scrMain.addSubview(viewField)
        scrMain.contentSize = CGSize(width: scrMain.frame.size.width, height: viewField.frame.size.height)
     
        
        
        self.setLocalizationText()
        
    }
    
    func setLocalizationText(){
        
        //lblPayment.text = NSLocalizedString("payment", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblStoreTitle.text = NSLocalizedString("storeaddress", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblExtraInfoTitle.text = NSLocalizedString("extrainfo", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblBillSummaryTitle.text = NSLocalizedString("billsummary", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblPatientTitle.text = NSLocalizedString("patient", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblUploadPrescription.text = NSLocalizedString("uploadpresciption", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblHeaderTitle.text = NSLocalizedString("billsummaryheader", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

    }
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    func setOrderHistorySummary(historyDetail:NSDictionary){
        print("Order History Detail Details",historyDetail)
        
        if let patientName = historyDetail.value(forKey: "patient_name") as? String{
            lblPatientName.text = patientName
        }
        
        var strGender:String!
        var strAge:String!
        
        if let gender = historyDetail.value(forKey: "patient_gender") as? String{
            strGender = gender
        }
        if let patientAge = historyDetail.value(forKey: "patient_age") as? String{
            strAge = patientAge
        }
        lblPatientGender.text = "\(strGender!) \(strAge!)"
        
        if let storeName = historyDetail.value(forKey: "store_name") as? String{
            lblStoreName.text = storeName
        }
        
        if let storeAddress = historyDetail.value(forKey: "store_address") as? String{
            lblStoreAddress.text = storeAddress
        }
        
        if let transaction_id = (historyDetail.value(forKey: "payment_info") as? NSDictionary)?.value(forKey: "transaction_id") as? String{
            lblTransactionNumber.text = "\(transaction_id)"
        }
        
        if let po_id = historyDetail.value(forKey: "po_id") as? String{
            lblTicketNumber.text = po_id
        }
        
        if let order_date = historyDetail.value(forKey: "order_date") as? String{
            let date = "\(order_date)"
//            let  finalDate = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: date, newFormat: "MMM d,yyyy HH:mm a")
//            lblTransactionDate.text = "\(finalDate)"
            let  finalDate = convertDateToDeviceTimeZone(myDateString: date, Strformat: "yyyy-MM-dd HH:mm:ss", returnFormat: "MMM d,yyyy HH:mm a")
            lblTransactionDate.text = "\(finalDate)"


        }
        

        
        if let preIamge = historyDetail.value(forKey: "images") as? String{
            let preImages = preIamge.components(separatedBy: ",") as NSArray
            print("preImagesData",preImagesData);
            preImagesData = preImages.mutableCopy() as! NSMutableArray
            collectionViewPrescriptionImages.reloadData()
            self.setupScrollForMainView()
        }
        
        
//        let flatNo = historyDetail.value(forKey: "flatNo") as? String
//        let streetName = historyDetail.value(forKey: "streetName") as? String
//        let landMark = historyDetail.value(forKey: "landMark") as? String
//        let area = historyDetail.value(forKey: "area") as? String
//        let city = historyDetail.value(forKey: "city") as? String
//        let postalCode = historyDetail.value(forKey: "postalCode") as? String
        
        lblPatientAddress.text = historyDetail.value(forKey: "patient_address") as? String
        
        let latitude = historyDetail.value(forKey: "store_latitude") as! String
        let longitude = historyDetail.value(forKey: "store_longtitude") as! String
        
        print("Latitude : Longitude ",latitude,longitude)
        
        self.recenterMapToPlacemark(strLatitude: latitude, strLongtitude: longitude, isChange: true)

        
    }
    
    //AlertDelegate Methods protocol
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }
    
    
    
    
    
    // TODO: - DELEGATE METHODS
    
    func recenterMapToPlacemark(strLatitude:String,strLongtitude:String,isChange:Bool){
        var latitudeAPI:Double!
        var longtitudeAPI:Double!
        
        if strLatitude != "" || strLongtitude != ""{
            latitudeAPI = Double(strLatitude)
            longtitudeAPI = Double(strLongtitude)
        }
        else{
            latitudeAPI = Double(0.0)
            longtitudeAPI = Double(0.0)
        }
        
        
        
        let myCurrentLocationCordinate:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitudeAPI, longtitudeAPI)
        
        myMapView.setCenter(myCurrentLocationCordinate, animated: true)
        
        //        myMapView.addAnnotation(myCurrentLocationCordinate as! MKAnnotation)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = myCurrentLocationCordinate
        annotation.title = lblStoreName.text
        annotation.subtitle = lblStoreAddress.text
        
        pinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "pin")
        myMapView.addAnnotation(pinAnnotationView.annotation!)
        
        
        
        // let region = MKCoordinateRegionMakeWithDistance(myCurrentLocationCordinate, 5000, 5000)
        //        let span = MKCoordinateSpanMake(0.05, 0.05)
        //        let region = MKCoordinateRegion(center: myCurrentLocationCordinate, span: span)
        //        myMapView.setRegion(region, animated: true)
        
        if isChange == true{
            let viewRegion = MKCoordinateRegionMakeWithDistance(myCurrentLocationCordinate, 5000, 5000)
            if(viewRegion.center.longitude == -180.00000000){
                print("Invalid region!")
                return;
            }
            
            let adjustedRegion: MKCoordinateRegion = myMapView.regionThatFits(viewRegion)
            
            if(adjustedRegion.center.longitude == -180.00000000){
                print("Invalid region!")
            }else{
                print("Valid region!")
                myMapView.setRegion(adjustedRegion, animated: true)
            }
        }
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        let annotationIdentifier = "pin"
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        if let annotationView = annotationView {
            
            annotationView.canShowCallout = true
            annotationView.image = UIImage(named: "pharmasist.png")
        }
        
        
        return annotationView
    }

    
    
    // MARK: - UICollectionViewDataSource protocol
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if preImagesData.count == 0{
            return 0
        }
        print(preImagesData.count)
        return preImagesData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierForImageUpload, for: indexPath) as! UploadPreCollectionVC
        
        
        if let upImage = preImagesData.object(at: indexPath.row) as? String
        {
            if upImage == ""
            {
                cell.imageViewPrescription.image = UIImage.init(named: "image_placeholder.png")
                cell.activityIndicatorForPrescriptionImages.isHidden = true

            }
            else{
                let imageUrl = upImage
                
                let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl) as String
                let url : NSString = fullUrl as NSString
                let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
                let searchURL : NSURL = NSURL(string: urlStr as String)!
                
                cell.activityIndicatorForPrescriptionImages.isHidden = false
                cell.activityIndicatorForPrescriptionImages.startAnimating()

                
                cell.imageViewPrescription.sd_setImage(with: searchURL as URL, completed: { (image:UIImage?, error:Error?, cacheType:SDImageCacheType!, imageURL:URL?) in
                    
                    if ((error) != nil) {
                        cell.imageViewPrescription.image = UIImage.init(named: "image_placeholder.png")
                    }
                    cell.activityIndicatorForPrescriptionImages.isHidden = true

                })
                
            }
        }
        else{
            cell.imageViewPrescription.image = UIImage.init(named: "image_placeholder.png")
            cell.activityIndicatorForPrescriptionImages.isHidden = true
        }
        
        
        cell.btnRemove.isHidden = true
        cell.imageViewRemove.isHidden = true
        
        return cell
        
        
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    
    
    // TODO: - ACTION METHODS
    @IBAction func btnFooterClicked(_ sender: UIButton) {
//        if Reachability.isConnectedToNetwork() == true {
//            SVProgressHUD.show(withStatus: "Loading..")
//            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetOrderHistory), userInfo: nil, repeats: false)
//        } else {
//            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
//        }

    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
       _ = self.navigationController?.popViewController(animated: true)
    }
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForGetOrderHistoryDetail(){
        
        let completeURL = NSString(format:"%@%@", MainURL,getOrderHistoryDetailURL) as String
//        let transactionDetail:NSDictionary = ["amount" : "15","transaction_id":"564984115"]
//        let timeStamp = Int(Date().timeIntervalSince1970)
//        let strTimeStamp = String(timeStamp)

        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "store_id":orderHistoryID,
        ]
        
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetOrderHistoryDetail API Parameter :",finalParams)
        print("GetOrderHistoryDetail API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getOrderHistoryDetailURLTag)
    }
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case getOrderHistoryDetailURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("GetOrderHistoryDetail Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                self.setOrderHistorySummary(historyDetail: resultDict.value(forKey: "data") as! NSDictionary)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }

            SVProgressHUD.dismiss()
            break
        
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case getOrderHistoryDetailURLTag:
            SVProgressHUD.dismiss()
            break
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    // TODO: - ScrollView Frame Setup METHODS

    func setupScrollForMainView(){
        
        self.collectionViewPrescriptionImages.reloadData()
        
        var myHeight:CGFloat = 0
        
        if (preImagesData.count % 3 == 0) {
            myHeight =  100 * CGFloat(preImagesData.count/3);
        }
        else{
            myHeight =  100 * CGFloat((preImagesData.count/3) + 1);
        }
        
        print("myHeight", myHeight)
        
        self.collectionViewPrescriptionImages.frame = CGRect(x: self.collectionViewPrescriptionImages.frame.origin.x, y: self.collectionViewPrescriptionImages.frame.origin.y, width: self.collectionViewPrescriptionImages.frame.size.width, height: myHeight)
        
        
        self.viewPreImagesInfo.frame = CGRect(x: self.viewPreImagesInfo.frame.origin.x, y: self.viewPreImagesInfo.frame.origin.y, width: self.viewPreImagesInfo.frame.size.width, height: self.collectionViewPrescriptionImages.frame.origin.y + self.collectionViewPrescriptionImages.frame.size.height+5.0)
        
        self.lblStoreTitle.frame = CGRect(x: self.lblStoreTitle.frame.origin.x, y: self.viewPreImagesInfo.frame.origin.y + self.viewPreImagesInfo.frame.size.height + 10.0, width: self.lblStoreTitle.frame.size.width, height: self.lblStoreTitle.frame.size.height)

        self.viewStoreInfo.frame = CGRect(x: self.viewStoreInfo.frame.origin.x, y: self.lblStoreTitle.frame.origin.y + self.lblStoreTitle.frame.size.height + 5.0, width: self.viewStoreInfo.frame.size.width, height: self.viewStoreInfo.frame.size.height)

        self.lblExtraInfoTitle.frame = CGRect(x: self.lblExtraInfoTitle.frame.origin.x, y: self.viewStoreInfo.frame.origin.y + self.viewStoreInfo.frame.size.height + 10.0, width: self.lblExtraInfoTitle.frame.size.width, height: self.lblExtraInfoTitle.frame.size.height)

        self.viewExtraInfo.frame = CGRect(x: self.viewExtraInfo.frame.origin.x, y: self.lblExtraInfoTitle.frame.origin.y + self.lblExtraInfoTitle.frame.size.height + 5.0, width: self.viewExtraInfo.frame.size.width, height: self.viewExtraInfo.frame.size.height)

        self.lblBillSummaryTitle.frame = CGRect(x: self.lblBillSummaryTitle.frame.origin.x, y: self.viewExtraInfo.frame.origin.y + self.viewExtraInfo.frame.size.height + 10.0, width: self.lblBillSummaryTitle.frame.size.width, height: self.lblBillSummaryTitle.frame.size.height)

        self.viewBillSummaryInfo.frame = CGRect(x: self.viewBillSummaryInfo.frame.origin.x, y: self.lblBillSummaryTitle.frame.origin.y + self.lblBillSummaryTitle.frame.size.height + 5.0, width: self.viewBillSummaryInfo.frame.size.width, height: self.viewBillSummaryInfo.frame.size.height)
        
//        self.viewFooter.frame = CGRect(x: self.viewFooter.frame.origin.x, y: self.viewBillSummaryInfo.frame.origin.y + self.viewBillSummaryInfo.frame.size.height + 5.0, width: self.viewFooter.frame.size.width, height: viewFooter.frame.size.height)


        self.viewField.frame = CGRect(x: self.viewField.frame.origin.x, y: self.viewField.frame.origin.y , width: self.viewField.frame.size.width, height: self.viewBillSummaryInfo.frame.size.height + self.viewBillSummaryInfo.frame.origin.y + 10.0)

        scrMain.contentSize = CGSize(width: scrMain.frame.size.width, height: viewField.frame.size.height)
        
        //        self.viewMainHeightConstant.constant = self.viewDishContent.frame.size.height
        // self.collectionViewImages.layoutIfNeeded()
        //        self.viewDishContent.layoutIfNeeded()
        //        self.viewDishField.layoutIfNeeded()
        
        
        
    }
    
    

    
    
}
