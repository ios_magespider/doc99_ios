//
//  OrderHistoryVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 12/07/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class OrderHistoryVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var tableViewCurrentOrder:UITableView!
    @IBOutlet var tableViewPreviousOrder:UITableView!
    
    @IBOutlet var lblUnderCurrentOrder:UILabel!
    @IBOutlet var lblUnderPreviousOrder:UILabel!
    
    @IBOutlet var btnCurrentOrder:UIButton!
    @IBOutlet var btnPreviousOrder:UIButton!
    
    @IBOutlet var viewCurrentOrder:UIView!
    @IBOutlet var viewPreviousOrder:UIView!
    
    @IBOutlet var scrView:UIScrollView!
    //FOR SCROLLING VIEW
    var currentPag:Int!
    var pageCount:Int!
    
    
    var currentOrderData = NSMutableArray()
    var currentOrderGlobalData = NSMutableArray()
    
    var previousOrderData = NSMutableArray()
    var previousOrderGlobalData = NSMutableArray()
    
    
    //For Pasignation
    var pageNum:Int!
    var isLoading:Bool?
    
    var pageNum1:Int!
    var isLoading1:Bool?
    
    
    var index:Int!
    
    @IBOutlet var lblNoCurrentOrder:UILabel!
    @IBOutlet var lblNoPreviousOrder:UILabel!
    
    @IBOutlet var viewUnderScorllView:UIView!
    
    let screenWidth = Int(ScreenSize.SCREEN_WIDTH)
    
//    @IBOutlet var viewSearch:UIView!
//    @IBOutlet var txtSearch:UITextField!
//    @IBOutlet var headerView:UIView!
    
    var isStore:Bool?
    
    
    var singleStoreData = NSDictionary()
    var singleDoctorData = NSDictionary()
    
    
    @IBOutlet var btnHeaderOrder:UILabel!
    
    var refreshControlCurrentOrder: UIRefreshControl!
    var refreshControlPreviousOrder: UIRefreshControl!
    
    
    // FIXME: - VIEW CONTROLLER METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
        
//        self.viewSearch.frame = CGRect(x: self.headerView.frame.origin.x+self.headerView.frame.size.width, y: self.headerView.frame.origin.y, width: self.viewSearch.frame.size.width, height: self.viewSearch.frame.size.height)
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        
      //  headerView.addSubview(viewSearch)
        
        
        pageCount=2;
        currentPag=0;
        
        scrView.contentSize = CGSize(width: CGFloat(screenWidth * pageCount), height: scrView.frame.size.height)
        
        viewCurrentOrder.frame = CGRect(x: 0.0, y: viewCurrentOrder.frame.origin.y, width: viewCurrentOrder.frame.size.width, height: viewCurrentOrder.frame.size.height)
        scrView.addSubview(viewCurrentOrder)
        
        
        viewPreviousOrder.frame = CGRect(x: viewCurrentOrder.frame.origin.x+viewCurrentOrder.frame.size.width, y: viewPreviousOrder.frame.origin.y, width: viewPreviousOrder.frame.size.width, height: viewPreviousOrder.frame.size.height)
        scrView.addSubview(viewPreviousOrder)
        
        self.settingBtnBgColor(btnCurrentOrder)
        
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.currentOrderData = NSMutableArray()
        self.currentOrderGlobalData = NSMutableArray()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableViewCurrentOrder()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetCurrentOrder), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        
        
        
        pageNum1=1;
        let indicator2 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator2.startAnimating()
        
        self.previousOrderData = NSMutableArray()
        self.previousOrderGlobalData = NSMutableArray()
        
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableViewPreviousOrder()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetPreviousOrder), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        
        self.setDiviceSupport()
        
        self.addTapGestureInOurView()
        self.setLocalizationText()
        self.addPullRefreshForPrevousOrderData()
        self.addPullRefreshForCurrentOrderData()
        
    }
    
    func addPullRefreshForCurrentOrderData(){
        refreshControlCurrentOrder = UIRefreshControl()
        refreshControlCurrentOrder.backgroundColor = UIColor.clear
        refreshControlCurrentOrder.tintColor = UIColor.gray
        refreshControlCurrentOrder.addTarget(self, action: #selector(self.refreshStoreData(sender:)), for: .valueChanged)
        tableViewCurrentOrder.addSubview(refreshControlCurrentOrder)
    }
    func addPullRefreshForPrevousOrderData(){
        refreshControlPreviousOrder = UIRefreshControl()
        refreshControlPreviousOrder.backgroundColor = UIColor.clear
        refreshControlPreviousOrder.tintColor = UIColor.gray
        refreshControlPreviousOrder.addTarget(self, action: #selector(self.refreshDoctorData(sender:)), for: .valueChanged)
        tableViewPreviousOrder.addSubview(refreshControlPreviousOrder)
    }
    
    func refreshStoreData(sender:AnyObject) {
        // Code to refresh table view
        
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.currentOrderData = NSMutableArray()
        self.currentOrderGlobalData = NSMutableArray()
        self.tableViewCurrentOrder.reloadData()
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableViewCurrentOrder()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetCurrentOrder), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        
        refreshControlCurrentOrder.endRefreshing()
    }
    func refreshDoctorData(sender:AnyObject) {
        // Code to refresh table view
        pageNum1=1;
        let indicator2 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator2.startAnimating()
        
        self.previousOrderData = NSMutableArray()
        self.previousOrderGlobalData = NSMutableArray()
        self.tableViewPreviousOrder.reloadData()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableViewPreviousOrder()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetPreviousOrder), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        refreshControlPreviousOrder.endRefreshing()
    }
    
    
    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        if viewTouched!.isKind(of: UIButton.self){
            
        }
        else{
            self.view.endEditing(true)
        }
    }
    
    
    func settingBtnBgColor(_ sender:UIButton){
        
        if sender == btnCurrentOrder{
            
            btnCurrentOrder.setTitleColor(UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0), for: .normal)
            btnPreviousOrder.setTitleColor(UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0), for: .normal)
            
            lblUnderPreviousOrder.isHidden = true
            lblUnderCurrentOrder.isHidden = false
            
            isStore = true
//            txtSearch.text = ""
//            txtSearch.attributedPlaceholder = NSAttributedString(string:"Search Store",
//                                                                 attributes:nil)
            
        }
        else if sender == btnPreviousOrder{
            
            btnPreviousOrder.setTitleColor(UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0), for: .normal)
            btnCurrentOrder.setTitleColor(UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0), for: .normal)
            
            lblUnderPreviousOrder.isHidden = false
            lblUnderCurrentOrder.isHidden = true
            
            isStore = false
//            txtSearch.text = ""
//            txtSearch.attributedPlaceholder = NSAttributedString(string:"Search Doctor",
//                                                                 attributes:nil)
        }
        
    }
    func addLoadingIndicatiorOnFooterOnTableViewCurrentOrder(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableViewCurrentOrder.tableFooterView = spinner
    }
    
    func addLoadingIndicatiorOnFooterOnTableViewPreviousOrder(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableViewPreviousOrder.tableFooterView = spinner
    }
    
    
    
    func removeLoadingIndicatiorOnFooterOnTableViewCurrentOrder(){
        tableViewCurrentOrder.tableFooterView = nil
    }
    func removeLoadingIndicatiorOnFooterOnTableViewPreviousOrder(){
        tableViewPreviousOrder.tableFooterView = nil
    }
    
    func setupScrollViewWithCurrentPage(currentPage:Int){
        scrView.contentOffset = CGPoint(x: currentPag * screenWidth, y: 0)
    }
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    func setDiviceSupport()
    {
        if DeviceType.IS_IPHONE_4_OR_LESS
        {
            
            viewCurrentOrder.frame = CGRect(x: viewCurrentOrder.frame.origin.x, y: viewCurrentOrder.frame.origin.y, width: viewCurrentOrder.frame.size.width, height: viewCurrentOrder.frame.size.height - 88.0)
            
            viewPreviousOrder.frame = CGRect(x: viewPreviousOrder.frame.origin.x, y: viewPreviousOrder.frame.origin.y, width: viewPreviousOrder.frame.size.width, height: viewPreviousOrder.frame.size.height - 88.0)
            
            
            tableViewCurrentOrder.frame = CGRect(x: tableViewCurrentOrder.frame.origin.x, y: tableViewCurrentOrder.frame.origin.y, width: tableViewCurrentOrder.frame.size.width, height: tableViewCurrentOrder.frame.size.height - 88.0)
            
            tableViewPreviousOrder.frame = CGRect(x: tableViewPreviousOrder.frame.origin.x, y: tableViewPreviousOrder.frame.origin.y, width: tableViewPreviousOrder.frame.size.width, height: tableViewPreviousOrder.frame.size.height - 88.0)
            
            scrView.frame = CGRect(x: scrView.frame.origin.x, y: scrView.frame.origin.y, width: scrView.frame.size.width, height: scrView.frame.size.height - 88.0)
            
            viewUnderScorllView.frame = CGRect(x: viewUnderScorllView.frame.origin.x, y: viewUnderScorllView.frame.origin.y, width: viewUnderScorllView.frame.size.width, height: viewUnderScorllView.frame.size.height - 88.0)
            
            
        }
    }
    
    func setLocalizationText(){
        
        btnHeaderOrder.text = NSLocalizedString("Order History", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblNoCurrentOrder.text = NSLocalizedString("Current Order Not Found", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblNoPreviousOrder.text = NSLocalizedString("Previous Order Not Found", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
        btnCurrentOrder.setTitle(NSLocalizedString("Current Orders", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        btnPreviousOrder.setTitle(NSLocalizedString("Previous Orders", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        
    }
    
    
    // TODO: - Textfield DELEGATE METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        return false
    }
    
    //MARK: - ACTION SHEET DELEGATE
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
        }
    }
    
    // TODO: - DELEGATE METHODS
    
    //ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.tag == 50) {
            currentPag = Int(scrollView.contentOffset.x) / Int(scrollView.frame.size.width);
            
            if (currentPag == 0) {
                self.settingBtnBgColor(btnCurrentOrder)
            }
            else if (currentPag == 1){
                self.settingBtnBgColor(btnPreviousOrder)
                let btn = UIButton.init()
                btn.tag = 101
                
                self.btnHeaderClicked(btn)
                
            }
            
            scrView.contentOffset = CGPoint(x: scrView.contentOffset.x, y: 0.0)
            
            
        }
        else  if scrollView == tableViewCurrentOrder {
            if isLoading == true{
                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                    pageNum = pageNum + 1
                    print(pageNum)
                    isLoading = false
                    
                    if Reachability.isConnectedToNetwork() == true {
                        self.addLoadingIndicatiorOnFooterOnTableViewCurrentOrder()
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetCurrentOrder), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
            }
            
        }
        else  if scrollView == tableViewPreviousOrder {
            if isLoading1 == true{
                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                    pageNum1 = pageNum1 + 1
                    print(pageNum1)
                    isLoading1 = false
                    
                    if Reachability.isConnectedToNetwork() == true {
                        self.addLoadingIndicatiorOnFooterOnTableViewPreviousOrder()
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetPreviousOrder), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
            }
            
        }
        
        
    }
    
    
    //TableView Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 100 {
            return self.currentOrderData.count
        }else if tableView.tag == 101{
            return self.previousOrderData.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 100{
            let identifier = "orderHistoryCell"
            
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderHistoryTableVC
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("OrderHistoryTableVC", owner: self, options: nil)
                cell = nib?[0] as? OrderHistoryTableVC
            }
            cell!.selectionStyle = .none;
            
            
            
            
            
            if let transaction_id = ((self.currentOrderData[indexPath.row] as AnyObject).value(forKey: "payment_info") as! NSDictionary).value(forKey: "transaction_id") as? String{
                cell?.lblTransactionNumber.text = "\(transaction_id)"
            }
            else if let transaction_id = ((self.currentOrderData[indexPath.row] as AnyObject).value(forKey: "payment_info") as! NSDictionary).value(forKey: "transaction_id") as? NSNumber{
                cell?.lblTransactionNumber.text = "\(transaction_id)"
            }
            
            if let order_date = (self.currentOrderData[indexPath.row] as AnyObject).value(forKey: "order_date") as? String{
                let date = "\(order_date)"
//                let  finalDate = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: date, newFormat: "MMM d,yyyy HH:mm a")
//                cell?.lblDate.text = "\(finalDate)"
                
                let  finalDate = convertDateToDeviceTimeZone(myDateString: date, Strformat: "yyyy-MM-dd HH:mm:ss", returnFormat: "MMM d,yyyy HH:mm a")
                cell?.lblDate.text = "\(finalDate)"
            }
            
            if let status = (self.currentOrderData[indexPath.row] as AnyObject).value(forKey: "status") as? String{
                let currentStatus = "\(status)"
                if currentStatus == "0"{
                    cell?.lblStatus.text = "Pending"
                }
                else if currentStatus == "1"{
                    cell?.lblStatus.text = "In Process"
                }
                else if currentStatus == "2"{
                    cell?.lblStatus.text = "Packed"
                }
                else if currentStatus == "3"{
                    cell?.lblStatus.text = "Shipped"
                }
                else if currentStatus == "4"{
                    cell?.lblStatus.text = "Done"
                }
            }
            else  if let status = (self.currentOrderData[indexPath.row] as AnyObject).value(forKey: "status") as? NSNumber{
                let currentStatus = "\(status)"
                
                if currentStatus == "0"{
                    cell?.lblStatus.text = "Pending"
                }
                else if currentStatus == "1"{
                    cell?.lblStatus.text = "In Process"
                }
                else if currentStatus == "2"{
                    cell?.lblStatus.text = "Packed"
                }
                else if currentStatus == "3"{
                    cell?.lblStatus.text = "Shipped"
                }
                else if currentStatus == "4"{
                    cell?.lblStatus.text = "Done"
                }
                
            }

            
            if let po_id = (self.currentOrderData[indexPath.row] as AnyObject).value(forKey: "po_id") as? String{
                let ticketNumber = "\(po_id)"
                cell?.lblTicketNumber.text = ticketNumber
            }
            else  if let po_id = (self.currentOrderData[indexPath.row] as AnyObject).value(forKey: "po_id") as? NSNumber{
                let ticketNumber = "\(po_id)"
                cell?.lblTicketNumber.text = ticketNumber
            }
            

            
            
            
            return cell!
            
        }
        else {
            let identifier = "orderHistoryCell"
            
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? OrderHistoryTableVC
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("OrderHistoryTableVC", owner: self, options: nil)
                cell = nib?[0] as? OrderHistoryTableVC
            }
            cell!.selectionStyle = .none;
            
            
            
            
            if let transaction_id = ((self.previousOrderData[indexPath.row] as AnyObject).value(forKey: "payment_info") as! NSDictionary).value(forKey: "transaction_id") as? String{
                cell?.lblTransactionNumber.text = "\(transaction_id)"
            }
            else if let transaction_id = ((self.previousOrderData[indexPath.row] as AnyObject).value(forKey: "payment_info") as! NSDictionary).value(forKey: "transaction_id") as? NSNumber{
                cell?.lblTransactionNumber.text = "\(transaction_id)"
            }
            
            if let order_date = (self.previousOrderData[indexPath.row] as AnyObject).value(forKey: "order_date") as? String{
                let date = "\(order_date)"
//                let  finalDate = getStringData(currentFormat: "yyyy-MM-dd HH:mm:ss", strDate: date, newFormat: "MMM d,yyyy HH:mm a")
//                cell?.lblDate.text = "\(finalDate)"
                let  finalDate = convertDateToDeviceTimeZone(myDateString: date, Strformat: "yyyy-MM-dd HH:mm:ss", returnFormat: "MMM d,yyyy HH:mm a")
                cell?.lblDate.text = "\(finalDate)"

            }
            
            if let status = (self.previousOrderData[indexPath.row] as AnyObject).value(forKey: "status") as? String{
                let currentStatus = "\(status)"
                if currentStatus == "0"{
                    cell?.lblStatus.text = "Pending"
                }
                else if currentStatus == "1"{
                    cell?.lblStatus.text = "In Process"
                }
                else if currentStatus == "2"{
                    cell?.lblStatus.text = "Packed"
                }
                else if currentStatus == "3"{
                    cell?.lblStatus.text = "Shipped"
                }
                else if currentStatus == "4"{
                    cell?.lblStatus.text = "Done"
                }
            }
            else  if let status = (self.previousOrderData[indexPath.row] as AnyObject).value(forKey: "status") as? NSNumber{
                let currentStatus = "\(status)"
                
                if currentStatus == "0"{
                    cell?.lblStatus.text = "Pending"
                }
                else if currentStatus == "1"{
                    cell?.lblStatus.text = "In Process"
                }
                else if currentStatus == "2"{
                    cell?.lblStatus.text = "Packed"
                }
                else if currentStatus == "3"{
                    cell?.lblStatus.text = "Shipped"
                }
                else if currentStatus == "4"{
                    cell?.lblStatus.text = "Done"
                }

            }

            
            if let po_id = (self.previousOrderData[indexPath.row] as AnyObject).value(forKey: "po_id") as? String{
                let ticketNumber = "\(po_id)"
                cell?.lblTicketNumber.text = ticketNumber
            }
            else  if let po_id = (self.previousOrderData[indexPath.row] as AnyObject).value(forKey: "po_id") as? NSNumber{
                let ticketNumber = "\(po_id)"
                cell?.lblTicketNumber.text = ticketNumber
            }
            
            
            
            return cell!
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 100 {
            let orderHistorySummaryVC = OrderHistorySumamryVC(nibName: "OrderHistorySumamryVC", bundle: nil)
            orderHistorySummaryVC.orderHistoryID = "1"
            orderHistorySummaryVC.orderHistoryData = self.currentOrderData[indexPath.row] as! NSDictionary
            self.navigationController?.pushViewController(orderHistorySummaryVC, animated: true)
        }
        else if tableView.tag == 101{
            let orderHistorySummaryVC = OrderHistorySumamryVC(nibName: "OrderHistorySumamryVC", bundle: nil)
            orderHistorySummaryVC.orderHistoryID = "1"
            orderHistorySummaryVC.orderHistoryData = self.previousOrderData[indexPath.row] as! NSDictionary
            self.navigationController?.pushViewController(orderHistorySummaryVC, animated: true)
        }
       

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 100 {
            return 70.0
        }else if tableView.tag == 101{
            return 70.0
        }
        return 0.0
    }
    
    
    
    // TODO: - ACTION METHODS
    
    @IBAction func btnHeaderClicked(_ sender:UIButton){
        
        if sender.tag == 10 {
            currentPag=0;
            self.setupScrollViewWithCurrentPage(currentPage: currentPag)
            self.settingBtnBgColor(btnCurrentOrder)
            
        }else if sender.tag == 11 {
            currentPag=1;
            self.setupScrollViewWithCurrentPage(currentPage: currentPag)
            self.settingBtnBgColor(btnPreviousOrder)
        }
        
        
    }
    @IBAction func btnBackClicked(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnSearchClicked(_ sender:UIButton){
//        txtSearch.becomeFirstResponder()
//        
//        UIView.animate(withDuration: 0.3) {
//            
//            self.viewSearch.frame = CGRect(x: self.headerView.frame.origin.x, y: self.headerView.frame.origin.y, width: self.viewSearch.frame.size.width, height: self.viewSearch.frame.size.height)
//            
//        }
        
    }
    
//    @IBAction func btnSearchCancelClicked(_ sender:UIButton){
//        txtSearch.text = ""
//        txtSearch.resignFirstResponder()
//        
//        UIView.animate(withDuration: 0.3) {
//            
//            self.viewSearch.frame = CGRect(x: self.headerView.frame.origin.x+self.headerView.frame.size.width, y: self.headerView.frame.origin.y, width: self.viewSearch.frame.size.width, height: self.viewSearch.frame.size.height)
//            
//        }
//        
//        if  isStore == true{
//            favouriteStoreData = favouriteStoreGlobalData.mutableCopy() as! NSMutableArray
//            if favouriteStoreData.count == 0 {
//                lblNoFavouriteStore.isHidden = false
//                tableViewStore.isHidden = true
//            }
//            else{
//                tableViewStore.isHidden = false
//                lblNoFavouriteStore.isHidden = true
//            }
//            tableViewStore.reloadData()
//        }
//        else{
//            favouriteDoctorData = favouriteDoctorGlobalData.mutableCopy() as! NSMutableArray
//            if favouriteDoctorData.count == 0 {
//                lblNoFavouriteDoctor.isHidden = false
//                tableViewDoctor.isHidden = true
//            }
//            else{
//                tableViewDoctor.isHidden = false
//                lblNoFavouriteDoctor.isHidden = true
//            }
//            tableViewDoctor.reloadData()
//        }
//        
//        
//    }
//    
//    @IBAction func searchFilter(){
//        
//        if  isStore == true{
//            var predicate = String()
//            
//            
//            if (txtSearch.text == "") {
//                favouriteStoreData = favouriteStoreGlobalData.mutableCopy() as! NSMutableArray
//            }else{
//                predicate = String(format: "SELF['firstname'] contains[c] '%@'", txtSearch.text!)
//                print(predicate)
//                
//                let myPredicate:NSPredicate = NSPredicate(format:predicate)
//                let temp = self.favouriteStoreGlobalData.mutableCopy() as! NSArray
//                self.favouriteStoreData = (temp.filtered(using: myPredicate) as NSArray).mutableCopy() as! NSMutableArray
//                
//                
//            }
//            
//            if favouriteStoreData.count == 0 {
//                lblNoFavouriteStore.isHidden = false
//                tableViewStore.isHidden = true
//            }
//            else{
//                tableViewStore.isHidden = false
//                lblNoFavouriteStore.isHidden = true
//            }
//            tableViewStore.reloadData()
//        }
//        else{
//            var predicate = String()
//            if (txtSearch.text == "") {
//                favouriteDoctorData = favouriteDoctorGlobalData.mutableCopy() as! NSMutableArray
//            }else{
//                predicate = String(format: "SELF['first_name'] contains[c] '%@'", txtSearch.text!)
//                print(predicate)
//                
//                let myPredicate:NSPredicate = NSPredicate(format:predicate)
//                let temp = self.favouriteDoctorGlobalData.mutableCopy() as! NSArray
//                self.favouriteDoctorData = (temp.filtered(using: myPredicate) as NSArray).mutableCopy() as! NSMutableArray
//                
//                
//            }
//            
//            if favouriteDoctorData.count == 0 {
//                lblNoFavouriteDoctor.isHidden = false
//                tableViewDoctor.isHidden = true
//            }
//            else{
//                tableViewDoctor.isHidden = false
//                lblNoFavouriteDoctor.isHidden = true
//            }
//            tableViewDoctor.reloadData()
//        }
//        
//        
//    }
    
    
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForGetCurrentOrder(){
        let completeURL = NSString(format:"%@%@", MainURL,getCurrentOrderURL) as String
        
        let pageNumber = "\(pageNum!)"
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "page":pageNumber,
            "limit":PAGINATION_LIMITE
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetCurrentOrder API Parameter :",finalParams)
        print("GetCurrentOrder API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getCurrentOrderURLTag)
        
    }
    func postDataOnWebserviceForGetPreviousOrder(){
        let completeURL = NSString(format:"%@%@", MainURL,getPreviousOrderURL) as String
        
        let pageNumber = "\(pageNum1!)"
        
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "page":pageNumber,
            "limit":PAGINATION_LIMITE
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetPreviousOrder API Parameter :",finalParams)
        print("GetPreviousOrder API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getPreviousOrderURLTag)
        
    }
    
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case getCurrentOrderURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("GetCurrentOrder Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum == 1{
                    self.currentOrderData = NSMutableArray()
                    self.currentOrderGlobalData = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    //                                self.dishGlobalData.addObjects(from: (myData) as! [Any])
                    for i in 0...myData.count - 1 {
                        self.currentOrderGlobalData.add(myData[i])
                    }
                    
                    for i in 0...myData.count - 1 {
                        self.currentOrderData.add(myData[i])
                    }
                    
                    
                    //                                self.searchStatusFilter()
                    
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum > 0) {
                            self.pageNum = self.pageNum - 1
                        }
                        self.isLoading = false
                    }else{
                        self.isLoading = true
                    }
                }
                else{
                    self.isLoading = false
                    if (self.pageNum > 0) {
                        self.pageNum = self.pageNum - 1
                    }
                    
                    if self.currentOrderData.count == 0{
                        self.tableViewCurrentOrder.isHidden = true
                        self.lblNoCurrentOrder.isHidden = false
                    }
                    else{
                        self.tableViewCurrentOrder.isHidden = false
                        self.lblNoCurrentOrder.isHidden = true
                    }
                }
                
                self.tableViewCurrentOrder.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableViewCurrentOrder()
                
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            break
            
        case getPreviousOrderURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("GetPreviousOrder Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                var myData = NSArray()
                
                if self.pageNum1 == 1{
                    self.previousOrderData = NSMutableArray()
                    self.previousOrderGlobalData = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    //                                self.dishGlobalData.addObjects(from: (myData) as! [Any])
                    for i in 0...myData.count - 1 {
                        self.previousOrderGlobalData.add(myData[i])
                    }
                    
                    for i in 0...myData.count - 1 {
                        self.previousOrderData.add(myData[i])
                    }
                    
                    
                    //                                self.searchStatusFilter()
                    
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum1 > 0) {
                            self.pageNum1 = self.pageNum1 - 1
                        }
                        self.isLoading1 = false
                    }else{
                        self.isLoading1 = true
                    }
                }
                else{
                    self.isLoading1 = false
                    if (self.pageNum1 > 0) {
                        self.pageNum1 = self.pageNum1 - 1
                    }
                    
                    if self.previousOrderData.count == 0{
                        self.tableViewPreviousOrder.isHidden = true
                        self.lblNoPreviousOrder.isHidden = false
                    }
                    else{
                        self.tableViewPreviousOrder.isHidden = false
                        self.lblNoPreviousOrder.isHidden = true
                    }
                }
                
                self.tableViewPreviousOrder.reloadData()
                self.removeLoadingIndicatiorOnFooterOnTableViewPreviousOrder()
                
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            break
            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case getCurrentOrderURLTag:
            self.isLoading = false
            if (self.pageNum > 0) {
                self.pageNum = self.pageNum - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableViewCurrentOrder()
            refreshControlCurrentOrder.endRefreshing()

            break
            
        case getPreviousOrderURLTag:
            self.isLoading1 = false
            if (self.pageNum1 > 0) {
                self.pageNum1 = self.pageNum1 - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableViewPreviousOrder()
            refreshControlPreviousOrder.endRefreshing()

            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
}
