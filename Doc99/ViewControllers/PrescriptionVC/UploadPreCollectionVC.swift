//
//  UploadPreCollectionVC.swift
//  Doc99
//
//  Created by MacBookPro on 26/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class UploadPreCollectionVC: UICollectionViewCell {

    @IBOutlet var imageViewPrescription:UIImageView!
    @IBOutlet var btnRemove:UIButton!
    @IBOutlet var imageViewRemove:UIImageView!
    @IBOutlet var activityIndicatorForPrescriptionImages:UIActivityIndicatorView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
