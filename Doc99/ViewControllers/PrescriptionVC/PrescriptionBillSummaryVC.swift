//
//  PrescriptionBillSummaryVC.swift
//  Doc99
//
//  Created by MacBookPro on 26/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD

class PrescriptionBillSummaryVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UIAlertViewDelegate,MKMapViewDelegate,PayPalPaymentDelegate {

    // FIXME: - VARIABLE
    @IBOutlet var viewField:UIView!
    @IBOutlet var scrMain:UIScrollView!
    
    @IBOutlet var lblPatientName:UILabel!
    @IBOutlet var lblPatientGender:UILabel!
    @IBOutlet var lblStoreName:UILabel!
    @IBOutlet var lblStoreAddress:UILabel!
    @IBOutlet var lblConvenienceFee:UILabel!
    @IBOutlet var lblPaybleFee:UILabel!
    
    @IBOutlet var collectionViewPrescriptionImages:UICollectionView!

    var preImagesData = NSMutableArray()

    let reuseIdentifierForImageUpload = "prescriptionImageCell"

    
    @IBOutlet var viewPreImagesInfo:UIView!
    @IBOutlet var viewStoreInfo:UIView!
    @IBOutlet var viewExtraInfo:UIView!
    @IBOutlet var viewBillSummaryInfo:UIView!
    @IBOutlet var viewMapInfo:UIView!

    @IBOutlet var lblStoreTitle:UILabel!
    @IBOutlet var lblExtraInfoTitle:UILabel!
    @IBOutlet var lblBillSummaryTitle:UILabel!
    @IBOutlet var lblPatientTitle:UILabel!
    @IBOutlet var lblUploadPrescription:UILabel!
    @IBOutlet var lblHeaderTitle:UILabel!
    @IBOutlet var lblPayment:UILabel!
    
    @IBOutlet var lblMapTitle:UILabel!
    @IBOutlet var lblPatientAddress:UILabel!
    @IBOutlet var lblDate:UILabel!

    @IBOutlet var myMapView:MKMapView!
    var pinAnnotationView:MKPinAnnotationView!

    // PAYPAL INTEGRATION
    var payPalConfig = PayPalConfiguration() // default
    var environment:String = PayPalEnvironmentSandbox {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    var amountValue:String!
    var transactionID:String!

    
    // FIXME: - VIEW CONTROLLER METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        self.paypalConfigurationSetting()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setPayPalEnvironment(environment: environment)
        amountValue = "15"

    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        

        self.collectionViewPrescriptionImages.register(UINib(nibName: "UploadPreCollectionVC", bundle: nil), forCellWithReuseIdentifier: reuseIdentifierForImageUpload)

        
        scrMain.addSubview(viewField)
        scrMain.contentSize = CGSize(width: scrMain.frame.size.width, height: viewField.frame.size.height)
        print("Prescription Details",APPDELEGATE.prescriptionData)
        
        if let patientName = APPDELEGATE.prescriptionData.value(forKey: "patientName") as? String{
            lblPatientName.text = patientName
        }
        
        var strGender:String!
        var strAge:String!

        if let gender = APPDELEGATE.prescriptionData.value(forKey: "patientGender") as? String{
            strGender = gender
        }
        if let patientAge = APPDELEGATE.prescriptionData.value(forKey: "patientAge") as? String{
            strAge = patientAge
        }
        lblPatientGender.text = "\(strGender!) \(strAge!)"
        
        if let storeName = APPDELEGATE.prescriptionData.value(forKey: "storeName") as? String{
            lblStoreName.text = storeName
        }
        
        if let storeAddress = APPDELEGATE.prescriptionData.value(forKey: "storeAddress") as? String{
            lblStoreAddress.text = storeAddress
        }
        
        if let preIamge = APPDELEGATE.prescriptionData.value(forKey: "prescriptionImages") as? String{
            let preImages = preIamge.components(separatedBy: ",") as NSArray
            print("preImagesData",preImagesData);
            preImagesData = preImages.mutableCopy() as! NSMutableArray
            collectionViewPrescriptionImages.reloadData()
            self.setupScrollForMainView()
        }
        
        
        lblDate.text = getStringDateFromDate(dateFormat: "dd MMMM yyyy", enterDate: Date())
        
        
        /*
        let addrDict = APPDELEGATE.prescriptionData.value(forKey: "AddressDetail") as? NSDictionary
        
        
        let flatNo = addrDict!.value(forKey: "flat_building") as? String
        //let streetName = addrDict!.value(forKey: "area") as? String
        
        let landMark:String
        if let landmark = addrDict!.value(forKey: "landmark") as? String{
            let land = "\(landmark)"
            if land != ""{
                landMark = land
            }
            else{
                landMark = ""
            }
        }
        else{
            landMark = ""
        }
        

        let area = addrDict!.value(forKey: "area") as? String
        let city = addrDict!.value(forKey: "city") as? String
        let postalCode = addrDict!.value(forKey: "postal_code") as? String

        
        
        lblPatientAddress.text = "\(flatNo!) \(streetName!) \(landMark) \(area!) \(city!) \(postalCode!)"
        */
        let latitude = APPDELEGATE.prescriptionData.value(forKey: "storeLatitude") as! String
        let longitude = APPDELEGATE.prescriptionData.value(forKey: "storeLongitude") as! String

        print("Latitude : Longitude ",latitude,longitude)
        
        self.recenterMapToPlacemark(strLatitude: latitude, strLongtitude: longitude, isChange: true)
        
        self.setLocalizationText()
        
    }
    
    func setLocalizationText(){
        
        lblPayment.text = NSLocalizedString("payment", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblStoreTitle.text = NSLocalizedString("storeaddress", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblExtraInfoTitle.text = NSLocalizedString("extrainfo", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblBillSummaryTitle.text = NSLocalizedString("billsummary", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblPatientTitle.text = NSLocalizedString("patient", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblUploadPrescription.text = NSLocalizedString("uploadpresciption", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblHeaderTitle.text = NSLocalizedString("billsummaryheader", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

    }
    
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    
    //AlertDelegate Methods protocol
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }
    
    
    func recenterMapToPlacemark(strLatitude:String,strLongtitude:String,isChange:Bool){
        var latitudeAPI:Double!
        var longtitudeAPI:Double!
        
        if strLatitude != "" || strLongtitude != ""{
            latitudeAPI = Double(strLatitude)
            longtitudeAPI = Double(strLongtitude)
        }
        else{
            latitudeAPI = Double(0.0)
            longtitudeAPI = Double(0.0)
        }
        
        
        
        let myCurrentLocationCordinate:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitudeAPI, longtitudeAPI)
        
        myMapView.setCenter(myCurrentLocationCordinate, animated: true)
        myMapView.isUserInteractionEnabled = false
        
//        myMapView.addAnnotation(myCurrentLocationCordinate as! MKAnnotation)

        let annotation = MKPointAnnotation()
        annotation.coordinate = myCurrentLocationCordinate
        annotation.title = lblStoreName.text
        annotation.subtitle = lblStoreAddress.text
        
        pinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "pin")
        myMapView.addAnnotation(pinAnnotationView.annotation!)

        
        
        // let region = MKCoordinateRegionMakeWithDistance(myCurrentLocationCordinate, 5000, 5000)
        //        let span = MKCoordinateSpanMake(0.05, 0.05)
        //        let region = MKCoordinateRegion(center: myCurrentLocationCordinate, span: span)
        //        myMapView.setRegion(region, animated: true)
        
        if isChange == true{
            let viewRegion = MKCoordinateRegionMakeWithDistance(myCurrentLocationCordinate, 5000, 5000)
            if(viewRegion.center.longitude == -180.00000000){
                print("Invalid region!")
                return;
            }
            
            let adjustedRegion: MKCoordinateRegion = myMapView.regionThatFits(viewRegion)
            
            if(adjustedRegion.center.longitude == -180.00000000){
                print("Invalid region!")
            }else{
                print("Valid region!")
                myMapView.setRegion(adjustedRegion, animated: true)
            }
        }
        
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        let annotationIdentifier = "pin"
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        if let annotationView = annotationView {
            
            annotationView.canShowCallout = true
            annotationView.image = UIImage(named: "pharmasist.png")
        }
        
        
        return annotationView
    }
    
    
    // TODO: - DELEGATE METHODS
    // MARK: - UICollectionViewDataSource protocol
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if preImagesData.count == 0{
            return 0
        }
        print(preImagesData.count)
        return preImagesData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierForImageUpload, for: indexPath) as! UploadPreCollectionVC
        
        
        if let upImage = preImagesData.object(at: indexPath.row) as? String
        {
            if upImage == ""
            {
                cell.imageViewPrescription.image = UIImage.init(named: "image_placeholder.png")
                cell.activityIndicatorForPrescriptionImages.isHidden = true

            }
            else{
                let imageUrl = upImage
                
                let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl) as String
                let url : NSString = fullUrl as NSString
                let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
                let searchURL : NSURL = NSURL(string: urlStr as String)!
                
                cell.activityIndicatorForPrescriptionImages.isHidden = false
                cell.activityIndicatorForPrescriptionImages.startAnimating()

                
                cell.imageViewPrescription.sd_setImage(with: searchURL as URL, completed: { (image:UIImage?, error:Error?, cacheType:SDImageCacheType!, imageURL:URL?) in
                    
                    if ((error) != nil) {
                        cell.imageViewPrescription.image = UIImage.init(named: "image_placeholder.png")
                    }
                    cell.activityIndicatorForPrescriptionImages.isHidden = true

                })
                
            }
        }
        else{
            cell.imageViewPrescription.image = UIImage.init(named: "image_placeholder.png")
            cell.activityIndicatorForPrescriptionImages.isHidden = true
        }
        
        
        cell.btnRemove.isHidden = true
        cell.imageViewRemove.isHidden = true
        
        return cell
        
        
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    // TODO: - PAYPAL METHODS
    
    func paypalConfigurationSetting() {
        // Set up payPalConfig
        payPalConfig.acceptCreditCards = false
        //        payPalConfig.merchantName = "Awesome Shirts, Inc."
        //        payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        //        payPalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        
        // Setting the languageOrLocale property is optional.
        //
        // If you do not set languageOrLocale, then the PayPalPaymentViewController will present
        // its user interface according to the device's current language setting.
        //
        // Setting languageOrLocale to a particular language (e.g., @"es" for Spanish) or
        // locale (e.g., @"es_MX" for Mexican Spanish) forces the PayPalPaymentViewController
        // to use that language/locale.
        //
        // For full details, including a list of available languages and locales, see PayPalPaymentViewController.h.
        
        payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
        
        // Setting the payPalShippingAddressOption property is optional.
        //
        // See PayPalConfiguration.h for details.
        
        self.environment = PayPalEnvironmentSandbox
        
        payPalConfig.payPalShippingAddressOption = .payPal
        payPalConfig.rememberUser = false
        
        print("PayPal iOS SDK Version: \(PayPalMobile.libraryVersion())")
        
    }
    
    func setPayPalEnvironment(environment:String) {
        //        self.environment = environment
        self.environment = environment
        
        PayPalMobile.preconnect(withEnvironment: self.environment)
    }
    
    func settingsPaypalMethod(myAmount:String){
        
        var subtotal = NSDecimalNumber(string: myAmount)
        
        let payment = PayPalPayment()
        
        
        payment.amount = subtotal;
        payment.currencyCode = "HKD";
        payment.shortDescription = "MEMBERSHIP";
        payment.paymentDetails = nil; // if not including payment details, then leave payment.paymentDetails as nil
        
        
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
            print("Payment not processalbe: \(payment)")
        }
        
    }
    
    
    // TODO: -  PayPalPaymentDelegate METHODS
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        
        self.sendCompletedPaymentToServer(completedPayment: completedPayment) //Payment was processed successfully send to server for verification and fulfillment
        
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            
        })
    }
    
    func sendCompletedPaymentToServer(completedPayment:PayPalPayment) {
        
        print("Here is your proof of payment:\n\n \(completedPayment.confirmation) \n\nSend this to your server for confirmation and fulfillment")
        
        let dict = NSMutableDictionary(dictionary: completedPayment.confirmation)
        print("Data : ",dict)
        
        transactionID = (dict.value(forKey: "response") as AnyObject).value(forKey: "id") as! String
        
        if Reachability.isConnectedToNetwork() == true {
            SVProgressHUD.show(withStatus: "Loading..")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForAddEprescriptionOrder), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
    }
    
    
    // TODO: - ACTION METHODS
    @IBAction func btnPaymentClicked(_ sender: UIButton) {
//        if Reachability.isConnectedToNetwork() == true {
//            SVProgressHUD.show(withStatus: "Loading..")
//            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForAddEprescriptionOrder), userInfo: nil, repeats: false)
//        } else {
//            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
//        }
        
        self.settingsPaypalMethod(myAmount: amountValue)

        
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
       _ = self.navigationController?.popViewController(animated: true)
    }
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForAddEprescriptionOrder(){
        
        print("amountValue Value : ",amountValue)
        print("transactionID : ",transactionID)

        let completeURL = NSString(format:"%@%@", MainURL,addEprescriptionOrderURL) as String
        let transactionDetail:NSDictionary = ["amount" : amountValue,"transaction_id":transactionID]
        let timeStamp = Int(Date().timeIntervalSince1970)
        let strTimeStamp = String(timeStamp)

        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "store_id":APPDELEGATE.prescriptionData.value(forKey: "storeID") as! String,
            "patient_id":APPDELEGATE.prescriptionData.value(forKey: "patientID") as! String,
            "fee":"15",
            "payment_info":transactionDetail,
            "images":APPDELEGATE.prescriptionData.value(forKey: "prescriptionImages") as! String,
            "timestamp":strTimeStamp,
            "patient_address":lblPatientAddress.text!
        ]
        
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("AddEprescriptionOrder API Parameter :",finalParams)
        print("AddEprescriptionOrder API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: addEprescriptionOrderURLTag)
    }
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case addEprescriptionOrderURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("AddEprescriptionOrder Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
                
                let homeVC = TabBarVC(nibName: "TabBarVC", bundle: nil)
                homeVC.selectedIndexOfMyTabbarController = 3
                self.navigationController?.pushViewController(homeVC, animated: true)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }

            SVProgressHUD.dismiss()
            break
        
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case addEprescriptionOrderURLTag:
            SVProgressHUD.dismiss()
            break
            
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }
    
    
    // TODO: - ScrollView Frame Setup METHODS

    func setupScrollForMainView(){
        
        self.collectionViewPrescriptionImages.reloadData()
        
        var myHeight:CGFloat = 0
        
        if (preImagesData.count % 3 == 0) {
            myHeight =  100 * CGFloat(preImagesData.count/3);
        }
        else{
            myHeight =  100 * CGFloat((preImagesData.count/3) + 1);
        }
        
        print("myHeight", myHeight)
        
        self.collectionViewPrescriptionImages.frame = CGRect(x: self.collectionViewPrescriptionImages.frame.origin.x, y: self.collectionViewPrescriptionImages.frame.origin.y, width: self.collectionViewPrescriptionImages.frame.size.width, height: myHeight)
        
        
        self.viewPreImagesInfo.frame = CGRect(x: self.viewPreImagesInfo.frame.origin.x, y: self.viewPreImagesInfo.frame.origin.y, width: self.viewPreImagesInfo.frame.size.width, height: self.collectionViewPrescriptionImages.frame.origin.y + self.collectionViewPrescriptionImages.frame.size.height+5.0)
        
        self.lblStoreTitle.frame = CGRect(x: self.lblStoreTitle.frame.origin.x, y: self.viewPreImagesInfo.frame.origin.y + self.viewPreImagesInfo.frame.size.height + 10.0, width: self.lblStoreTitle.frame.size.width, height: self.lblStoreTitle.frame.size.height)

        self.viewStoreInfo.frame = CGRect(x: self.viewStoreInfo.frame.origin.x, y: self.lblStoreTitle.frame.origin.y + self.lblStoreTitle.frame.size.height + 5.0, width: self.viewStoreInfo.frame.size.width, height: self.viewStoreInfo.frame.size.height)

        self.lblMapTitle.frame = CGRect(x: self.lblMapTitle.frame.origin.x, y: self.viewStoreInfo.frame.origin.y + self.viewStoreInfo.frame.size.height + 10.0, width: self.lblMapTitle.frame.size.width, height: self.lblMapTitle.frame.size.height)

        self.viewMapInfo.frame = CGRect(x: self.viewMapInfo.frame.origin.x, y: self.lblMapTitle.frame.origin.y + self.lblMapTitle.frame.size.height + 5.0, width: self.viewMapInfo.frame.size.width, height: self.viewMapInfo.frame.size.height)

        self.lblExtraInfoTitle.frame = CGRect(x: self.lblExtraInfoTitle.frame.origin.x, y: self.viewMapInfo.frame.origin.y + self.viewMapInfo.frame.size.height + 10.0, width: self.lblExtraInfoTitle.frame.size.width, height: self.lblExtraInfoTitle.frame.size.height)

        self.viewExtraInfo.frame = CGRect(x: self.viewExtraInfo.frame.origin.x, y: self.lblExtraInfoTitle.frame.origin.y + self.lblExtraInfoTitle.frame.size.height + 5.0, width: self.viewExtraInfo.frame.size.width, height: self.viewExtraInfo.frame.size.height)

        self.lblBillSummaryTitle.frame = CGRect(x: self.lblBillSummaryTitle.frame.origin.x, y: self.viewExtraInfo.frame.origin.y + self.viewExtraInfo.frame.size.height + 10.0, width: self.lblBillSummaryTitle.frame.size.width, height: self.lblBillSummaryTitle.frame.size.height)

        self.viewBillSummaryInfo.frame = CGRect(x: self.viewBillSummaryInfo.frame.origin.x, y: self.lblBillSummaryTitle.frame.origin.y + self.lblBillSummaryTitle.frame.size.height + 5.0, width: self.viewBillSummaryInfo.frame.size.width, height: self.viewBillSummaryInfo.frame.size.height)

        self.viewField.frame = CGRect(x: self.viewField.frame.origin.x, y: self.viewField.frame.origin.y , width: self.viewField.frame.size.width, height: self.viewBillSummaryInfo.frame.size.height + self.viewBillSummaryInfo.frame.origin.y + 10.0)

        scrMain.contentSize = CGSize(width: scrMain.frame.size.width, height: viewField.frame.size.height)
        
        //        self.viewMainHeightConstant.constant = self.viewDishContent.frame.size.height
        // self.collectionViewImages.layoutIfNeeded()
        //        self.viewDishContent.layoutIfNeeded()
        //        self.viewDishField.layoutIfNeeded()
        
    }

    
    
}
