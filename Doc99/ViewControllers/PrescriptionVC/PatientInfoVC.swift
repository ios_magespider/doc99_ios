//
//  PatientInfoVC.swift
//  Doc99
//
//  Created by MacBookPro on 26/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class PatientInfoVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate {
    
    // FIXME: - VARIABLE
    //For Pasignation
    var pageNum:Int!
    var isLoading:Bool?
    
    @IBOutlet var tableViewPatientInfo:UITableView!
    var patientData = NSMutableArray()
    var patientGlobalData = NSMutableArray()

    var patientID:String!
    
    @IBOutlet var btnAddNewPatient:UIButton!
    @IBOutlet var scrMain:UIScrollView!
    var boolArray:NSMutableArray!
    var patientIDData = NSMutableArray()
    var zeroTempDataArray:NSMutableArray!

    
    @IBOutlet var lblHeaderPationInfo:UILabel!
    @IBOutlet var lbluploadPrescription:UILabel!
    
    @IBOutlet var viewFooter:UIView!

    var isFromSettingPage:Bool?
    var refreshControl: UIRefreshControl!

    @IBOutlet var viewMain:UIView!
    
    // FIXME: - VIEW CONTROLLER METHODS
//    @IBOutlet var viewMainHeightConstant:NSLayoutConstraint!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Define identifier
        let notificationName1 = Notification.Name("refreshPatientListData")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(refreshPatientListData(notification:)), name: notificationName1, object: nil)
        
        
        
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
        
        if isFromSettingPage == true{
            viewFooter.isHidden = true
            viewMain.frame = CGRect(x: viewMain.frame.origin.x, y: viewMain.frame.origin.y, width: viewMain.frame.size.width, height: viewMain.frame.size.height + viewMain.frame.origin.y + viewFooter.frame.size.height)
            scrMain.frame = CGRect(x: scrMain.frame.origin.x, y: scrMain.frame.origin.y, width: scrMain.frame.size.width, height: scrMain.frame.size.height + scrMain.frame.origin.y + viewFooter.frame.size.height)

//            self.viewMainHeightConstant.constant = viewMain.frame.size.height
//            viewMain.layoutIfNeeded()
//            viewMain.updateConstraints()
        }
        else{
            viewMain.frame = CGRect(x: viewMain.frame.origin.x, y: viewMain.frame.origin.y, width: viewMain.frame.size.width, height: viewMain.frame.size.height)
            viewFooter.isHidden = false
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.patientData = NSMutableArray()
        self.patientGlobalData = NSMutableArray()
        tableViewPatientInfo.reloadData()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetPatientInfo), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        self.addTapGestureInOurView()
        self.setLocalizationText()
        self.addPullRefreshForPatientInfo()
    }
    
    
    func addPullRefreshForPatientInfo(){
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.gray
        refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: .valueChanged)
        tableViewPatientInfo.addSubview(refreshControl)
    }
    
    func refresh(sender:AnyObject) {
        
        // Code to refresh table view
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.patientData = NSMutableArray()
        self.patientGlobalData = NSMutableArray()
        tableViewPatientInfo.reloadData()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetPatientInfo), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
        refreshControl.endRefreshing()
    }
    
    

    
    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        if viewTouched!.isKind(of: UIButton.self){
        }
        else{
            self.view.endEditing(true)
        }
    }

    
    
    func refreshPatientListData(notification: NSNotification) {
        pageNum=1;
        let indicator1 = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator1.startAnimating()
        
        self.patientData = NSMutableArray()
        self.patientGlobalData = NSMutableArray()
        tableViewPatientInfo.reloadData()
        
        if Reachability.isConnectedToNetwork() == true {
            self.addLoadingIndicatiorOnFooterOnTableView()
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetPatientInfo), userInfo: nil, repeats: false)
        }else{
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
    }
    func addLoadingIndicatiorOnFooterOnTableView(){
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
        tableViewPatientInfo.tableFooterView = spinner
    }
    func removeLoadingIndicatiorOnFooterOnTableView(){
        tableViewPatientInfo.tableFooterView = nil
    }
    func showAlertForVarification(_ messageT:String,title:String,alertTag:Int){
        let alert:UIAlertView = UIAlertView(title: messageT, message: title, delegate: nil, cancelButtonTitle: NSLocalizedString("LOGIN", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), otherButtonTitles:NSLocalizedString("CANCEL", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        alert.show()
        alert.cancelButtonIndex = -1
        alert.tag = alertTag
        alert.delegate = self
    }
    
    
    func setLocalizationText(){
        
        lblHeaderPationInfo.text = NSLocalizedString("patientinfo", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lbluploadPrescription.text = NSLocalizedString("uploadpresciption", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

        btnAddNewPatient.setTitle(NSLocalizedString("addnewpatient", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        
    }
    
    
    // TODO: - DELEGATE METHODS
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch alertView.tag {
        case 1001:
            switch buttonIndex {
            case 0:
                APPDELEGATE.logOutUserWithoutToken()
                let loginVC = LoginVC(nibName: "LoginVC", bundle: nil)
                self.navigationController?.pushViewController(loginVC, animated: true)
                
                break
            case 1:
                print("OK")
                break
            default:
                break
            }
        default:
            break
            
            
        }
    }

    // TODO: - DELEGATE ScrollView
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == tableViewPatientInfo {
            if isLoading == true{
                if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
                    pageNum = pageNum + 1
                    print(pageNum)
                    isLoading = false
                    
                    if Reachability.isConnectedToNetwork() == true {
                        self.addLoadingIndicatiorOnFooterOnTableView()
                        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForGetPatientInfo), userInfo: nil, repeats: false)
                    } else {
                        showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                    }
                }
            }
            
        }
        
        
    }
    
    
    //TableView Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.patientData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let identifier = "patientInfoCell"
            
            var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? PatientInfoTableVC
            
            if cell == nil {
                let nib  = Bundle.main.loadNibNamed("PatientInfoTableVC", owner: self, options: nil)
                cell = nib?[0] as? PatientInfoTableVC
            }
            cell!.selectionStyle = .none;
            
            
            
            
            if let name = (self.patientData[indexPath.row] as AnyObject).value(forKey: "name") as? String{
                cell?.lblName.text = name
            }
            var gender1:String!
            var age1:String!
        
            if let gender = (self.patientData[indexPath.row] as AnyObject).value(forKey: "gender") as? String{
                gender1 = "\(gender)"
            }
            if let age = (self.patientData[indexPath.row] as AnyObject).value(forKey: "age") as? String{
                age1 = "\(age)"
            }

            cell?.lblGender.text = "\(gender1!) \(age1!)"
        
        
        if isFromSettingPage == false {
            
            if boolArray.object(at: indexPath.row) as! String == "1"{
                cell?.imageViewSelect.image = UIImage.init(named: "check_blue.png")
            }
            else{
                cell?.imageViewSelect.image = UIImage.init(named: "check_gray.png")
            }
        }
        
        cell?.btnSelect.tag = indexPath.row
        if isFromSettingPage == true {
            cell?.btnSelect.addTarget(self, action: #selector(self.btnDeleteClicked(_:)), for: .touchUpInside)
        }
        else{
            cell?.btnSelect.addTarget(self, action: #selector(self.btnSelectionClicked(_:)), for: .touchUpInside)
        }
        
        if isFromSettingPage == true{
            cell?.imageViewSelect.isHidden = false
            cell?.btnSelect.isHidden = false
            cell?.imageViewSelect.image = UIImage.init(named: "trash.png")
        }
        else{
            cell?.imageViewSelect.isHidden = false
            cell?.btnSelect.isHidden = false
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isFromSettingPage == true{
            self.patientID = (self.patientData[indexPath.row] as AnyObject).value(forKey: "patient_id") as? String
            let addPatientVC = AddPatientInfoVC(nibName: "AddPatientInfoVC", bundle: nil)
            addPatientVC.editPatientData = self.patientData[indexPath.row] as! NSDictionary
            addPatientVC.isFromEdit = true
            self.navigationController?.pushViewController(addPatientVC, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 55.0
    }
    @available(iOS 8.0, *)
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        if isFromSettingPage == true{
            
            let deleteAction:UITableViewRowAction = UITableViewRowAction(style: .normal, title: "Delete") { (action:UITableViewRowAction, indexPath:IndexPath) in
                print("Delete Clicked")
                
                self.patientID = (self.patientData[indexPath.row] as AnyObject).value(forKey: "patient_id") as? String
                
                self.patientData.removeObject(at: indexPath.row)
                self.patientGlobalData.removeObject(at: indexPath.row)
                self.tableViewPatientInfo.reloadData()
                self.setAddPatientBtnFrame()
                
                
                if Reachability.isConnectedToNetwork() == true {
                    //                SVProgressHUD.show(withStatus: "Loading..")
                    Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForDeletePatientInfo), userInfo: nil, repeats: false)
                } else {
                    showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                }
            }
            deleteAction.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
            return [deleteAction]
 
        }
        else{
            let editAction:UITableViewRowAction = UITableViewRowAction(style: .normal, title: "Edit") { (action:UITableViewRowAction, indexPath:IndexPath) in
                
                self.patientID = (self.patientData[indexPath.row] as AnyObject).value(forKey: "patient_id") as? String
                
                let addPatientVC = AddPatientInfoVC(nibName: "AddPatientInfoVC", bundle: nil)
                addPatientVC.editPatientData = self.patientData[indexPath.row] as! NSDictionary
                addPatientVC.isFromEdit = true
                self.navigationController?.pushViewController(addPatientVC, animated: true)
                
            }
            editAction.backgroundColor = UIColor.init(red: 108.0/255.0, green: 110.0/255.0, blue: 112.0/255.0, alpha: 1.0)
            
            let deleteAction:UITableViewRowAction = UITableViewRowAction(style: .normal, title: "Delete") { (action:UITableViewRowAction, indexPath:IndexPath) in
                print("Delete Clicked")
                
                self.patientID = (self.patientData[indexPath.row] as AnyObject).value(forKey: "patient_id") as? String
                
                self.patientData.removeObject(at: indexPath.row)
                self.patientGlobalData.removeObject(at: indexPath.row)
                self.tableViewPatientInfo.reloadData()
                self.setAddPatientBtnFrame()
                
                
                if Reachability.isConnectedToNetwork() == true {
                    //                SVProgressHUD.show(withStatus: "Loading..")
                    Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForDeletePatientInfo), userInfo: nil, repeats: false)
                } else {
                    showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                }
                
                
            }
            deleteAction.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
            return [editAction,deleteAction]
        }
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if isFromSettingPage == true {
            return false
        }
        else{
            return true
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    // TODO: - ACTION METHODS
    
    @IBAction func btnSelectionClicked(_ sender:UIButton){
//        if boolArray.object(at: sender.tag) as! String == "0"{
//            boolArray.replaceObject(at: sender.tag, with: "1")
//        }
//        else{
//            boolArray.replaceObject(at: sender.tag, with: "0")
//        }
//        
//      
//        
//        let indexPath = NSIndexPath(row: sender.tag, section: 0)
//        tableViewPatientInfo.reloadRows(at: [indexPath as IndexPath], with: .none)
//        
//        patientIDData = NSMutableArray.init()
//        
//        
//        for i in 0...patientData.count - 1 {
//            if boolArray.object(at: i) as! String == "1"{
//                patientIDData.add((self.patientData[i] as AnyObject).value(forKey: "patient_id") as! String)
//            }
//        }
//        print("PatientID Data",patientIDData)
        
        
        
        let indexPath = NSIndexPath(row: sender.tag, section: 0)
        
        let myIndexValue:Int = boolArray.index(of: "1")
        
        
        boolArray = zeroTempDataArray.mutableCopy() as! NSMutableArray
        
        let oneDataArray = zeroTempDataArray.mutableCopy() as! NSMutableArray
        
        oneDataArray.replaceObject(at: indexPath.row, with: "1")
        boolArray.replaceObject(at: indexPath.row, with: oneDataArray.object(at: indexPath.row))
        
        UIView.animate(withDuration: 0, animations: {
//            self.tableViewPatientInfo.performBatchUpdates({
//                let indexPath1 = IndexPath(row: indexPath.row, section: 0)
//                print("Indexpath1 \(indexPath1.row)")
//                self.collectionViewFilterDrug.reloadItems(at: [indexPath1])
//            }, completion: nil)
            
            let indexPath1 = IndexPath(row: indexPath.row, section: 0)
            print("Indexpath1 \(indexPath1.row)")
            self.tableViewPatientInfo.reloadRows(at: [indexPath1], with: .none)
            print("PatientID : ",((self.patientData[indexPath1.row] as AnyObject).value(forKey: "patient_id") as! String))

            
        })
        
        UIView.animate(withDuration: 0, animations: {
            
            if (myIndexValue<self.boolArray.count) {
                let indexPath2 = IndexPath(row: myIndexValue, section: 0)
                print("Indexpath2 \(indexPath2.row)")
                self.tableViewPatientInfo.reloadRows(at: [indexPath2], with: .none)
                print("PatientID : ",((self.patientData[indexPath2.row] as AnyObject).value(forKey: "patient_id") as! String))

            }

            
//            self.tableViewPatientInfo.performBatchUpdates({
//                if (myIndexValue<self.boolArray.count) {
//                    let indexPath2 = IndexPath(row: myIndexValue, section: 0)
//                    print("Indexpath2 \(indexPath2.row)")
//                    self.collectionViewFilterDrug.reloadItems(at: [indexPath2])
//                }
//            }, completion: nil)
        })
        
        
        patientIDData = NSMutableArray.init()
        
        
        for i in 0...patientData.count - 1 {
            if boolArray.object(at: i) as! String == "1"{
                patientIDData.add((self.patientData[i] as AnyObject).value(forKey: "patient_id") as! String)
                let patientID = (self.patientData[i] as AnyObject).value(forKey: "patient_id") as! String
                let patientname = (self.patientData[i] as AnyObject).value(forKey: "name") as! String
                let patientgender = (self.patientData[i] as AnyObject).value(forKey: "gender") as! String
                let patientage = (self.patientData[i] as AnyObject).value(forKey: "age") as! String

                APPDELEGATE.prescriptionData.setValue(patientID, forKey: "patientID")
                APPDELEGATE.prescriptionData.setValue(patientname, forKey: "patientName")
                APPDELEGATE.prescriptionData.setValue(patientgender, forKey: "patientGender")
                APPDELEGATE.prescriptionData.setValue(patientage, forKey: "patientAge")

            }
        }
        
        print("PatientID Data",patientIDData)
    }
    
    @IBAction func btnDeleteClicked(_ sender:UIButton){
        print("Sender.tag --> \(sender.tag)")
        self.patientID = (self.patientData[sender.tag] as AnyObject).value(forKey: "patient_id") as? String
        
        if Reachability.isConnectedToNetwork() == true {
            self.patientData.removeObject(at: sender.tag)
            self.patientGlobalData.removeObject(at: sender.tag)
            self.tableViewPatientInfo.reloadData()
            self.setAddPatientBtnFrame()
            
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForDeletePatientInfo), userInfo: nil, repeats: false)
        } else {
            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        
    }
    
    @IBAction func btnAddNewPatientClicked(_ sender: UIButton) {
        let addPatientVC = AddPatientInfoVC(nibName: "AddPatientInfoVC", bundle: nil)
        self.navigationController?.pushViewController(addPatientVC, animated: true)

    }
    @IBAction func btnUploadPrescriptionClicked(_ sender: UIButton) {
//        if patientIDData.count > 0{
//            let uploadPre = UploadPrescriptionVC(nibName: "UploadPrescriptionVC", bundle: nil)
//            self.navigationController?.pushViewController(uploadPre, animated: true)
//        }
//        else{
//            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please select patient", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
//        }
        
                if patientIDData.count > 0{
                    let patientAddressVC = AddressListVC(nibName: "AddressListVC", bundle: nil)
//                    patientAddressVC.patientID = (self.patientData[0] as AnyObject).value(forKey: "patient_id") as! String
                    patientAddressVC.isFromSetting = false
                    self.navigationController?.pushViewController(patientAddressVC, animated: true)
                }
                else{
                    showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please select patient", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
                }
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
       _ = self.navigationController?.popViewController(animated: true)
    }
    
    // TODO: - POST DATA METHODS
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForGetPatientInfo(){
        let completeURL = NSString(format:"%@%@", MainURL,getPatientInfoURL) as String
        
        let pageNumber = "\(pageNum!)"
        
        let params = [
                "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
                "token": USERDEFAULT.value(forKey: "token") as! String,
                "lang_type":Language_Type,
                "page":pageNumber,
                "limit":PAGINATION_LIMITE
            ] as [String : Any]
            
        
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("GetPatientInfo API Parameter :",finalParams)
        print("GetPatientInfo API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: getPatientInfoURLTag)
        
    }
    
    func postDataOnWebserviceForDeletePatientInfo(){
        let completeURL = NSString(format:"%@%@", MainURL,removePatientInfoURL) as String
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "patient_id":patientID
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("DeletePatientInfo API Parameter :",finalParams)
        print("DeletePatientInfo API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: removePatientInfoURLTag)
    }
    

    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case getPatientInfoURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("GetPatientInfo List Response  : \(resultDict)")
            
            if resultDict.value(forKey: "status") as! String == "1"{
                
                var myData = NSArray()
                
                if self.pageNum == 1{
                    self.patientData = NSMutableArray()
                    self.patientGlobalData = NSMutableArray()
                }
                
                myData = resultDict.value(forKey: "data") as! NSArray
                
                if (myData.count > 0) {
                    for i in 0...myData.count - 1 {
                        self.patientGlobalData.add(myData[i])
                    }
                    
                    for i in 0...myData.count - 1 {
                        self.patientData.add(myData[i])
                    }
                    
                    boolArray = NSMutableArray.init()
                    for i in 0...patientData.count - 1 {
                        boolArray.add("0")
                    }
                    
                    zeroTempDataArray = NSMutableArray.init()
                    for i in 0...patientData.count - 1 {
                        zeroTempDataArray.add("0")
                    }

                    
                    
                    if (myData.count < PAGINATION_LIMITE) {
                        if (self.pageNum > 0) {
                            self.pageNum = self.pageNum - 1
                        }
                        self.isLoading = false
                    }else{
                        self.isLoading = true
                    }
                }
                else{
                    self.isLoading = false
                    if (self.pageNum > 0) {
                        self.pageNum = self.pageNum - 1
                    }
                    
                }
                
                if self.patientData.count == 0{
                    self.tableViewPatientInfo.isHidden = true
                    
                    if isFromSettingPage == false{
                        let addPatientVC = AddPatientInfoVC(nibName: "AddPatientInfoVC", bundle: nil)
                        self.navigationController?.pushViewController(addPatientVC, animated: true)
                    }
                    

                    //self.lblNoArtical.isHidden = false
                }
                else{
                    self.tableViewPatientInfo.isHidden = false
                    //self.lblNoArtical.isHidden = true
                }
                
                
                self.tableViewPatientInfo.reloadData()
                
                
            }
            else if resultDict.value(forKey: "status") as! String == "0"{
                showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: resultDict.value(forKey: "message") as! String)
            }
            else if resultDict.value(forKey: "status") as! String == "2"{
                self.showAlertForVarification(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(ExpireTokenMessage, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), alertTag: 1001)
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            self.setAddPatientBtnFrame()
            break
            
        case removePatientInfoURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("Remove PatientInfo Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                
            }
            tableViewPatientInfo.reloadData()
            self.setAddPatientBtnFrame()

            
           // SVProgressHUD.dismiss()
            break
            

            
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case getPatientInfoURLTag:
            self.isLoading = false
            if (self.pageNum > 0) {
                self.pageNum = self.pageNum - 1
            }
            self.removeLoadingIndicatiorOnFooterOnTableView()
            break
        case removePatientInfoURLTag:
            //SVProgressHUD.dismiss()
            break

        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }

    func setAddPatientBtnFrame(){
        
//        btnAddNewPatient.frame = CGRect(x: btnAddNewPatient.frame.origin.x, y: tableViewPatientInfo.contentSize.height + tableViewPatientInfo.frame.origin.y + 10, width: btnAddNewPatient.frame.size.width, height:btnAddNewPatient.frame.size.height)
        
        tableViewPatientInfo.frame = CGRect(x: tableViewPatientInfo.frame.origin.x, y: btnAddNewPatient.frame.size.height + btnAddNewPatient.frame.origin.y + 10, width: tableViewPatientInfo.frame.size.width, height: tableViewPatientInfo.contentSize.height)
        
        scrMain.contentSize = CGSize(width: scrMain.frame.size.width, height: tableViewPatientInfo.frame.origin.y + tableViewPatientInfo.frame.size.height)
    }
    
}
