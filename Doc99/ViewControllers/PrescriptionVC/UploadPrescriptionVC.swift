//
//  UploadPrescriptionVC.swift
//  Doc99
//
//  Created by MacBookPro on 26/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class UploadPrescriptionVC: UIViewController,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UIAlertViewDelegate {
    
    // FIXME: - VARIABLE
    @IBOutlet var collectionViewImages:UICollectionView!
    
    let reuseIdentifierForImageUpload = "prescriptionImageCell"

    @IBOutlet var btnAttechReceipt:UIButton!
    @IBOutlet var imageViewCamera:UIImageView!

    @IBOutlet var scrMain:UIScrollView!
    var imagePicker:UIImagePickerController!

    // FIXME: - VIEW CONTROLLER METHODS
    var imageUploadData = NSMutableArray()
   // var imagesData = NSMutableArray()
    var imageName:String!
    var imageDictionary:NSMutableDictionary!
    var isImageSelected:Bool?

    @IBOutlet var lblHeader:UILabel!
    @IBOutlet var lblUploadPrescription:UILabel!
    @IBOutlet var lblBillSummary:UILabel!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        self.collectionViewImages.register(UINib(nibName: "UploadPreCollectionVC", bundle: nil), forCellWithReuseIdentifier: reuseIdentifierForImageUpload)

//        let temp = NSMutableDictionary()
//        temp.setValue("", forKey: "uploadImage")
//        self.imageUploadData.add(temp)
//        print("ImageData",self.imageUploadData)
        self.collectionViewImages.reloadData()
        self.setupScrollForMainView()
        isImageSelected = false

        self.setLocalizationText()
    }
    func openActionSheetForCamera() {
        let actionSheet = UIActionSheet(title: "Choose your photo", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Gallery","Camera")
        actionSheet.tag = 100
        actionSheet.show(in: self.view)
        
    }
    
    func setLocalizationText(){
        
        lblHeader.text = NSLocalizedString("Upload Prescription", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblUploadPrescription.text = NSLocalizedString("uploadpresciption", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblBillSummary.text = NSLocalizedString("billsummary", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")

        btnAttechReceipt.setTitle(NSLocalizedString("attachreceipt", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        
    }
    
    
    // TODO: - DELEGATE METHODS
    //MARK: - ACTION SHEET DELEGATE
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
        
        
        
        switch buttonIndex {
        case 0:
            print("Cancel")
        case 1:
            print("Gallery")
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            
            present(imagePicker, animated: true, completion: nil)
            
        case 2:
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            
            if Platform.isSimulator {
                imagePicker.sourceType = .photoLibrary
            }
            else {
                imagePicker.sourceType = .camera
            }
            
            present(imagePicker, animated: true, completion: nil)
            
            
            print("Camera")
        default:
            dismiss(animated: true, completion: nil)
            
        }
    }
    
    //MARK: - IMAGEPICKER CONTROLLER DELEGATE
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        let chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
        
        isImageSelected = true
        
        picker.dismiss(animated: true) {
            let temp = NSMutableDictionary()
            temp.setValue(chosenImage, forKey: "uploadImage")
            self.imageUploadData.add(temp)

//            if self.indexRow >= self.imageUploadData.count{
//                self.imageUploadData.add(temp)
//            }
//            else{
//                self.imageUploadData.replaceObject(at: self.indexRow, with: temp)
//                
//                if self.indexRow == self.imageUploadData.count - 1{
//                    let temp1 = NSMutableDictionary()
//                    temp1.setValue("", forKey: "uploadImage")
//                    self.imageUploadData.insert(temp1, at: self.indexRow + 1)
//                }
//            }
            
            print("UploadImages",self.imageUploadData)

            self.collectionViewImages.reloadData()
            self.setupScrollForMainView()
            
            
        }
        
        //        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - UICollectionViewDataSource protocol
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if imageUploadData.count == 0{
                return 0
            }
            print(imageUploadData.count)
            return imageUploadData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierForImageUpload, for: indexPath) as! UploadPreCollectionVC
            
            
            if let upImage = (imageUploadData.object(at: indexPath.row) as AnyObject).value(forKey: "uploadImage") as? UIImage
            {
                let uploadImage = upImage
                if uploadImage.isEqual(""){
                    cell.imageViewPrescription.image = UIImage.init(named: "upload-picture.png")
                    cell.btnRemove.isHidden = true
                    cell.imageViewRemove.isHidden = true

                }
                else{
                    cell.imageViewPrescription.image = upImage
                    cell.btnRemove.isHidden = false
                    cell.imageViewRemove.isHidden = false

                }
            }
            else{
                cell.imageViewPrescription.image = UIImage.init(named: "upload-picture.png")
                cell.btnRemove.isHidden = true
                cell.imageViewRemove.isHidden = true
            }
        
            
            cell.activityIndicatorForPrescriptionImages.isHidden = true
            cell.btnRemove.tag = indexPath.row
            cell.btnRemove.addTarget(self, action: #selector(self.btnImageDeleteClicked(_:)), for: .touchUpInside)
            
        
            return cell
        
        
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }

    
    
    // TODO: - ACTION METHODS
    @IBAction func btnAttechReceiptClicked(_ sender: UIButton) {
      //  indexRow = sender.tag
        self.openActionSheetForCamera()

    }
    @IBAction func btnBillSummaryClicked(_ sender: UIButton) {
        if self.isImageSelected == true {
            
            if Reachability.isConnectedToNetwork() == true {
                SVProgressHUD.show(withStatus: "Loading..")
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForImageUpload), userInfo: nil, repeats: false)
            } else {
                showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
            }
            
        }
        else{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please upload prescription receipt", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "") )
        }
       
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
       _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func btnImageDeleteClicked(_ sender:UIButton){
        
        print("sender.tag", sender.tag)
        
        if imageUploadData.count > 0{
            imageUploadData.removeObject(at: sender.tag)
            self.collectionViewImages.reloadData()
        }
        
//        if imagesData.count > 0{
//            imagesData.removeObject(at: sender.tag)
//        }
        
        print("imageUploadData", imageUploadData)
        
        self.setupScrollForMainView()
        
    }
    
    // TODO: - POST DATA METHODS
    
    func postDataOnWebserviceForImageUpload(){
        
        
        let completeURL = NSString(format:"%@",imageFileUploadUrl) as String
        let url = NSURL(string: completeURL)
        let request:ASIFormDataRequest = ASIFormDataRequest(url: url as URL!)
        request.delegate = self
        request.requestMethod = "POST"
        request.timeOutSeconds = 5000
        request.addHeader("Content-Type", value: "multipart/form-data")
        
        
        if imageUploadData.count > 0{
            for i in 0...imageUploadData.count - 1 {
                
                let imageData1:NSData = UIImagePNGRepresentation(Utility.resize((imageUploadData.object(at: i) as AnyObject).value(forKey: "uploadImage") as? UIImage))! as NSData
                let path1 = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
                let url1 = NSURL(fileURLWithPath: path1)
                let imageName:String = "image\(i).png"
                let filePath1 = url1.appendingPathComponent(imageName)?.path
                print(filePath1!)
                imageData1.write(toFile: filePath1!, atomically: true)
                let imageKey = "image\(i)"
                request.setFile(filePath1!, forKey: imageKey)
                
                
            }
            
        }
        else {
            return;
        }
        
        
        
        request.tag = 1010
        request.didFinishSelector = #selector(self.checkupdateFinished(request:))
        request.didFailSelector = #selector(self.checkupdateFailed(request:))
        request.startSynchronous()
    }
    
    func checkupdateFinished(request:ASIFormDataRequest) {
        
        let receivedString = request.responseString()
        print(receivedString!)
        
        if (request.responseData() != nil) {
            if request.tag == 1010{
                
                do{
                    
                    let json:Any = try JSONSerialization.jsonObject(with: request.responseData(), options: [])  // Check 2
                    if let resultDict = json as? NSDictionary {
                        print("Image Upload Response :-> ",resultDict)
                        
                        if String(format:"%@", resultDict.value(forKey: "status") as! CVarArg) == "1"
                        {
                          //  let imageData = (resultDict.value(forKey: "data") as! NSDictionary).value(forKey: "images") as! NSArray
                            
                            imageName = resultDict.value(forKey: "data") as! String
                            let billSummary = PrescriptionBillSummaryVC(nibName: "PrescriptionBillSummaryVC", bundle: nil)
                            APPDELEGATE.prescriptionData.setValue(imageName, forKey: "prescriptionImages")
                            self.navigationController?.pushViewController(billSummary, animated: true)

                            
//                            if imageData.count > 0 {
//                                
//                                imagesData = NSMutableArray()
//                                
//                                
//                                for i in 0..<imageData.count {
//                                    imageDictionary = NSMutableDictionary()
//                                    imageDictionary.setValue((imageData[i] as! NSDictionary).value(forKey: "imagename") as! String, forKey: "imagename")
//                                    imagesData.add(imageDictionary)
//                                }
//                                print("Images DATA : ",imagesData)
//                            }
                            
                        }
                        
                    }
                    else {
                        if let jsonString = NSString(data: request.responseData(), encoding: String.Encoding.utf8.rawValue) {
                            print("JSON String: \n\n \(jsonString)")
                        }
                    }
                }
                catch{
                    
                }
                SVProgressHUD.dismiss()
                
            }
        }
        
    }
    func checkupdateFailed(request:ASIFormDataRequest) {
        print(request.error)
        //  print("Error: " + error!.localizedDescription)
        
        SVProgressHUD.dismiss()
        //showAlert(Appname, title: FailureAlert)
    }
    
    
    
    func setupScrollForMainView(){
        
        self.collectionViewImages.reloadData()
        
        var myHeight:CGFloat = 0
        
        
       // let cellItem:Int = self.collectionViewImages.numberOfItems(inSection: 0)
        
        if (imageUploadData.count % 3 == 0) {
            myHeight =  100 * CGFloat(imageUploadData.count/3);
        }
        else{
            myHeight =  100 * CGFloat((imageUploadData.count/3) + 1);
        }
        
        print("myHeight", myHeight)
        
        self.collectionViewImages.frame = CGRect(x: self.collectionViewImages.frame.origin.x, y: self.collectionViewImages.frame.origin.y, width: self.collectionViewImages.frame.size.width, height: myHeight)
        
        
        btnAttechReceipt.frame = CGRect(x: btnAttechReceipt.frame.origin.x, y: collectionViewImages.frame.size.height + collectionViewImages.frame.origin.y + 10, width: btnAttechReceipt.frame.size.width, height:btnAttechReceipt.frame.size.height)
        imageViewCamera.frame = CGRect(x: imageViewCamera.frame.origin.x, y:  btnAttechReceipt.frame.origin.y + 13, width: imageViewCamera.frame.size.width, height:imageViewCamera.frame.size.height)

        
        scrMain.contentSize = CGSize(width: scrMain.frame.size.width, height: btnAttechReceipt.frame.origin.y + btnAttechReceipt.frame.size.height)
        
        
//        self.viewMainHeightConstant.constant = self.viewDishContent.frame.size.height
       // self.collectionViewImages.layoutIfNeeded()
//        self.viewDishContent.layoutIfNeeded()
//        self.viewDishField.layoutIfNeeded()
        
    }
    
    
}
