//
//  PatientInfoTableVC.swift
//  Doc99
//
//  Created by MacBookPro on 26/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit

class PatientInfoTableVC: UITableViewCell {

    @IBOutlet var lblName:UILabel!
    @IBOutlet var lblGender:UILabel!
    @IBOutlet var imageViewSelect:UIImageView!
    @IBOutlet var btnSelect:UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
