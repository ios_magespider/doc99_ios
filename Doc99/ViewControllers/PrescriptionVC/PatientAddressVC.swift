//
//  PatientAddressVC.swift
//  Doc99
//
//  Created by Pritesh Pethani on 13/07/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD

class PatientAddressVC: UIViewController {

    @IBOutlet var txtFlatNumber:UITextField!
    @IBOutlet var txtStreetName:UITextField!
    @IBOutlet var txtLandmark:UITextField!
    @IBOutlet var txtArea:UITextField!
    @IBOutlet var txtCity:UITextField!
    @IBOutlet var txtPostalCode:UITextField!
    
    @IBOutlet var lblHeader:UILabel!
    @IBOutlet var lblFlatNumber:UILabel!
    @IBOutlet var lblStreetName:UILabel!
    @IBOutlet var lblLandMark:UILabel!
    @IBOutlet var lblArea:UILabel!
    @IBOutlet var lblCity:UILabel!
    @IBOutlet var lblPostalCode:UILabel!
    @IBOutlet var lblUploadPrescription:UILabel!

    
//    var patientID:String!
    
    // FIXME: - VIEW CONTROLLER METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        self.addTapGestureInOurView()
        self.setLocalizationText()
    }
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        
        if viewTouched!.isKind(of: UIButton.self) && viewTouched!.isKind(of:UITextView.self){
            
        }
        else{
            self.view.endEditing(true)
            //            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
            
            
        }
    }
    
    func setLocalizationText(){
        
        lblHeader.text = NSLocalizedString("Patient Address", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblFlatNumber.text = NSLocalizedString("Flat number & building name", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblStreetName.text = NSLocalizedString("Street Name", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblLandMark.text = NSLocalizedString("Landmark (optional)", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblArea.text = NSLocalizedString("Area", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblCity.text = NSLocalizedString("City", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblPostalCode.text = NSLocalizedString("Postal Code", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        lblUploadPrescription.text = NSLocalizedString("UPLOAD PRESCRIPTION", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
    
    }
    
    
    // TODO: - TEXTFIELD DELEGATE METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        return false
    }
    
    // TODO: - DELEGATE METHODS
    
    
    
    // TODO: - ACTION METHODS
    @IBAction func btnUploadPrescriptionClicked(_ sender: UIButton) {
        
        if txtFlatNumber.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter flatnumber and building name", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        else if txtStreetName.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter street name", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        else if txtArea.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter area", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        else if txtCity.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter city", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        else if txtPostalCode.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter postalcode", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        else{
            
            APPDELEGATE.prescriptionData.setValue(txtFlatNumber.text!, forKey: "flatNo")
            APPDELEGATE.prescriptionData.setValue(txtStreetName.text!, forKey: "streetName")
            APPDELEGATE.prescriptionData.setValue(txtLandmark.text!, forKey: "landMark")
            APPDELEGATE.prescriptionData.setValue(txtArea.text!, forKey: "area")
            APPDELEGATE.prescriptionData.setValue(txtCity.text!, forKey: "city")
            APPDELEGATE.prescriptionData.setValue(txtPostalCode.text!, forKey: "postalCode")
            
            let uploadPre = UploadPrescriptionVC(nibName: "UploadPrescriptionVC", bundle: nil)
            self.navigationController?.pushViewController(uploadPre, animated: true)
        }
        
        
//        if Reachability.isConnectedToNetwork() == true {
//            SVProgressHUD.show(withStatus: "Loading..")
//            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForAddPatientAddress), userInfo: nil, repeats: false)
//        } else {
//            showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
//        }

    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }

    
    
    // TODO: - POST DATA METHODS
//    func postDataOnWebserviceForAddPatientAddress(){
//        let completeURL = NSString(format:"%@%@", MainURL,addPatientInfoURL) as String
//        
//        let landMark:String!
//        
//        if txtLandmark.text == ""{
//            landMark = ""
//        }
//        else{
//            landMark = txtLandmark.text!
//        }
//        
//        let params:NSDictionary = [
//            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
//            "token": USERDEFAULT.value(forKey: "token") as! String,
//            "lang_type":Language_Type,
//            "name":txtName.text!,
//            "gender":txtGender.text!,
//            "age":txtAge.text!,
//            "relation":relation
//        ]
//        
//        
//        
//        let finalParams:NSDictionary = [
//            "data" : params
//        ]
//        
//        print("AddPatientInfo API Parameter :",finalParams)
//        print("AddPatientInfo API URL :",completeURL)
//        
//        let sampleProtocol = SyncManager()
//        sampleProtocol.delegate = self
//        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: addPatientInfoURLTag)
//    }
//  
//    
//    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
//        switch tag {
//        case addPatientInfoURLTag:
//            let resultDict = responseObject as! NSDictionary;
//            print("AddPatientInfo Response  : \(resultDict)")
//            if resultDict.value(forKey: "status") as! String == "1"{
//                let uploadPre = UploadPrescriptionVC(nibName: "UploadPrescriptionVC", bundle: nil)
//                self.navigationController?.pushViewController(uploadPre, animated: true)
//            }
//            SVProgressHUD.dismiss()
//            break
//        default:
//            break
//            
//        }
//        
//    }
//    func syncFailure(_ error: Error!, withTag tag: Int) {
//        switch tag {
//        case addPatientInfoURLTag:
//            SVProgressHUD.dismiss()
//            break
//        default:
//        break
//            
//        }
//        print("syncFailure Error : ",error.localizedDescription)
//        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
//    }

}
