//
//  AddPatientInfoVC.swift
//  Doc99
//
//  Created by MacBookPro on 26/04/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

import UIKit
import SVProgressHUD
class AddPatientInfoVC: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    // FIXME: - VARIABLE

    @IBOutlet var txtName:UITextField!
    @IBOutlet var txtGender:UITextField!
    @IBOutlet var txtAge:UITextField!
    @IBOutlet var txtRelation:UITextField!
    @IBOutlet var btnGender:UIButton!

    
    @IBOutlet var viewPicker:UIView!
    @IBOutlet var pickerView:UIPickerView!
    var pickerSelIndx:Int!
    var pickOption:NSMutableArray!
    
    var isFromEdit:Bool?
    @IBOutlet var btnSave:UIButton!
    @IBOutlet var lblHeader:UILabel!

    var editPatientData = NSDictionary()
    
    @IBOutlet var lblPatientHeader:UILabel!
    @IBOutlet var btnCancel:UIButton!
    @IBOutlet var btnDone:UIButton!

    
    // FIXME: - VIEW CONTROLLER METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generalViewControllerSetting()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        APPDELEGATE.sideMenuController?.removeGesture()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        APPDELEGATE.myTabBarController?.tabBar.isHidden = true
        
        
        pickerSelIndx=0
        pickOption = NSMutableArray()
        pickOption = ["Male","Female"]
        pickerView.reloadAllComponents()
        
        viewPicker.frame = CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: viewPicker.frame.size.width, height: viewPicker.frame.size.height)
        
        
        
    }
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        self.NavBarNumberPad()
        self.addTapGestureInOurView()
        
        if isFromEdit == true{
            lblHeader.text = "Edit Patient"
            btnSave.setTitle("UPDATE", for: .normal)
            self.setEditPatientData(patientData: editPatientData)
        }
        else{
            lblHeader.text = "Add Patient"
            btnSave.setTitle("SAVE", for: .normal)
        }

        self.setLocalizationText()
    }
    // Numeric Keyboard Hide Method
    func NavBarNumberPad(){
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        numberToolbar.barStyle = .default
        
        let viewBack = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        viewBack.backgroundColor = UIColor.init(red: 85.0/255.0, green: 198.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        numberToolbar.addSubview(viewBack)
        
        let btnCancel = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelNumberPad))
        btnCancel.tintColor = UIColor.white
        
        let btnDone = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(self.doneWithNumberPad))
        btnDone.tintColor = UIColor.white
        
        numberToolbar.items = NSArray(objects: btnCancel,UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),btnDone) as? [UIBarButtonItem]
        
        numberToolbar.sizeToFit()
        txtAge.inputAccessoryView = numberToolbar
    }
    
    func cancelNumberPad(){
        txtAge.resignFirstResponder()
    }
    
    func doneWithNumberPad(){
        txtAge.resignFirstResponder()
    }
    
    
    func addTapGestureInOurView(){
        let tapRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTap(_:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    @IBAction func backgroundTap(_ sender:UITapGestureRecognizer){
        
        let point:CGPoint = sender.location(in: sender.view)
        let viewTouched = view.hitTest(point, with: nil)
        
        
        if viewTouched!.isKind(of: UIButton.self) && viewTouched!.isKind(of:UITextView.self){
            
        }
        else{
            self.view.endEditing(true)
            //            scrMain.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
            
            
            if (viewTouched!.superview!.superview == viewPicker) {
            }else if(viewTouched!.superview == viewPicker){
            }else if(viewTouched == viewPicker){
            }else{
                UIView.animate(withDuration: 0.3) {
                    
                    self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
                    
                    
                    
                }
                
            }
        }
    }
    
    func setEditPatientData(patientData:NSDictionary) {
        if let name = patientData.value(forKey: "name") as? String{
            txtName.text = name
        }
        if let age = patientData.value(forKey: "age") as? String{
            txtAge.text = age
        }
        if let gender = patientData.value(forKey: "gender") as? String{
            txtGender.text = gender
        }

        if let relation = patientData.value(forKey: "relation") as? String{
            txtRelation.text = relation
        }
        
    }
    
    
    
    func setLocalizationText(){
        
        lblPatientHeader.text = NSLocalizedString("addpatient", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: "")
        
        btnCancel.setTitle(NSLocalizedString("cancel", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        btnDone.setTitle(NSLocalizedString("done", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), for: .normal)
        
    }
    
    
    // TODO: - DELEGATE METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage=textField.tag+1;
        let nextResponder=textField.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        return false
    }

    
    // MARK: - PICKERVIEW DELEGATE
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickOption.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        // return (self.pickOption[row] as AnyObject).value(forKey: "title") as? String
        return self.pickOption[row] as? String
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50.0
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let label = UILabel()
        label.textAlignment = NSTextAlignment.center
        
        label.font = UIFont.systemFont(ofSize: 20.0)
        label.text = self.pickOption[row] as? String
        return label
    }
    
    // TODO: - ACTION METHODS
    @IBAction func btnGenderClicked(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y - self.viewPicker.frame.size.height, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
            
        })
    }
    @IBAction func cancelPicker(sender:AnyObject){
        UIView.animate(withDuration: 0.3) {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
        }
        
    }
    
    @IBAction func donePicker(sender:UIButton){
        
        pickerSelIndx = pickerView.selectedRow(inComponent: 0)
        
        
        
        print("Picker Index is  :-> \(pickerSelIndx) and value is \(pickOption[pickerSelIndx])")
         txtGender.text = self.pickOption[pickerSelIndx] as? String
        //dishID = ((self.dishData[pickerSelIndx] as AnyObject).value(forKey: "_id") as? String)!
       
        
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewPicker.frame=CGRect(x: self.view.frame.size.width - self.viewPicker.frame.size.width, y: self.view.frame.size.height + self.view.frame.origin.y, width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
            
        })
    }
    
    @IBAction func btnSaveClicked(_ sender: UIButton) {
        if txtName.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter name", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        else if txtGender.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please select gender", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        else if txtAge.text == ""{
            showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title:NSLocalizedString("Please enter age", tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
        }
        else{
            
            if isFromEdit == true{
                if Reachability.isConnectedToNetwork() == true {
                    SVProgressHUD.show(withStatus: "Loading..")
                    Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForUpdatePatientInfo), userInfo: nil, repeats: false)
                } else {
                    showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                }

            }
            else{
                if Reachability.isConnectedToNetwork() == true {
                    SVProgressHUD.show(withStatus: "Loading..")
                    Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.postDataOnWebserviceForAddPatientInfo), userInfo: nil, repeats: false)
                } else {
                    showAlert(NSLocalizedString(CheckConnection, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(InternetError, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
                }

            }
    }

    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
       _ = self.navigationController?.popViewController(animated: true)
    }
    
    // TODO: - POST DATA METHODS
    func postDataOnWebserviceForAddPatientInfo(){
        let completeURL = NSString(format:"%@%@", MainURL,addPatientInfoURL) as String
        
        let relation:String!
        
        if txtRelation.text == ""{
            relation = ""
        }
        else{
            relation = txtRelation.text
        }
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "name":txtName.text!,
            "gender":txtGender.text!,
            "age":txtAge.text!,
            "relation":relation
        ]
        
        
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("AddPatientInfo API Parameter :",finalParams)
        print("AddPatientInfo API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: addPatientInfoURLTag)
    }
    func postDataOnWebserviceForUpdatePatientInfo(){
        let completeURL = NSString(format:"%@%@", MainURL,editPatientInfoURL) as String
        
        let relation:String!
        
        if txtRelation.text == ""{
            relation = ""
        }
        else{
            relation = txtRelation.text
        }
        
        let params:NSDictionary = [
            "user_id" : USERDEFAULT.value(forKey: "userID") as! String,
            "token": USERDEFAULT.value(forKey: "token") as! String,
            "lang_type":Language_Type,
            "name":txtName.text!,
            "gender":txtGender.text!,
            "age":txtAge.text!,
            "relation":relation,
            "patient_id":editPatientData.value(forKey: "patient_id") as! String
        ]
        
        let finalParams:NSDictionary = [
            "data" : params
        ]
        
        print("UpdatePatientInfo API Parameter :",finalParams)
        print("UpdatePatientInfo API URL :",completeURL)
        
        let sampleProtocol = SyncManager()
        sampleProtocol.delegate = self
        sampleProtocol.webServiceCall(completeURL, withParams: finalParams as! [AnyHashable : Any], withTag: editPatientInfoURLTag)
    }
    
    
    func syncSuccess(_ responseObject: Any!, withTag tag: Int) {
        switch tag {
        case addPatientInfoURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("AddPatientInfo Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                
                let notificationName = Notification.Name("refreshPatientListData")
                NotificationCenter.default.post(name: notificationName, object: nil)
                
             //   showAlert(Appname, title: resultDict.value(forKey: "message") as! String)
               _ = self.navigationController?.popViewController(animated: true)

            }
            SVProgressHUD.dismiss()
            break
        case editPatientInfoURLTag:
            let resultDict = responseObject as! NSDictionary;
            print("Update PatientInfo Response  : \(resultDict)")
            if resultDict.value(forKey: "status") as! String == "1"{
                
                let notificationName = Notification.Name("refreshPatientListData")
                NotificationCenter.default.post(name: notificationName, object: nil)
                
                _ = self.navigationController?.popViewController(animated: true)
                
            }
            SVProgressHUD.dismiss()
            break
  
        default:
            break
            
        }
        
    }
    func syncFailure(_ error: Error!, withTag tag: Int) {
        switch tag {
        case addPatientInfoURLTag:
            SVProgressHUD.dismiss()
            break
        case editPatientInfoURLTag:
            SVProgressHUD.dismiss()
            break
        default:
            break
            
        }
        print("syncFailure Error : ",error.localizedDescription)
        showAlert(NSLocalizedString(Appname, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""), title: NSLocalizedString(FailureAlert, tableName: nil, bundle: APPDELEGATE.bundle, value: "", comment: ""))
    }

}
