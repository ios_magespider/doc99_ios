//
//  Doc99-Bridging-Header.h
//  Doc99
//
//  Created by Pritesh Pethani on 30/03/17.
//  Copyright © 2017 Pritesh Pethani. All rights reserved.
//

#ifndef Doc99_Bridging_Header_h
#define Doc99_Bridging_Header_h



#import "Reachability.h"
#import "DataManager.h"
#import "SyncManager.h"
#import "Utility.h"
//#import "AFNetworking/AFNetworking.h"
#import "UIAlertView+Blocks.h"
//Slider Menu
#import "MVYSideMenuController.h"
#import "MVYSideMenuOptions.h"

#import "EMCCountryPickerController.h"
#import "DiallingCode.h"

//#import "SVProgressHUD/SVProgressHUD.h"
//#import <SVProgressHUD/SVProgressHUD.h>

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import "QTree.h"
#import "DummyAnnotation.h"
#import "QCluster.h"
#import "ClusterAnnotationView.h"

#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "ASIDataDecompressor.h"

#import "TWMessageBarManager.h"

#import "PayPalMobile.h"
#import "FRHyperLabel.h"


//#import "SDWebImage/UIImageView+WebCache.h"
//#import <SDWebImage/SDImageCache.h>


#endif /* Doc99_Bridging_Header_h */




/*
 <key>UILaunchImages</key>
 <array>
 <dict>
 <key>UILaunchImageMinimumOSVersion</key>
 <string>8.0</string>
 <key>UILaunchImageName</key>
 <string>Default</string>
 <key>UILaunchImageOrientation</key>
 <string>Portrait</string>
 <key>UILaunchImageSize</key>
 <string>{320, 480}</string>
 </dict>
 <dict>
 <key>UILaunchImageMinimumOSVersion</key>
 <string>8.0</string>
 <key>UILaunchImageName</key>
 <string>Default-568h</string>
 <key>UILaunchImageOrientation</key>
 <string>Portrait</string>
 <key>UILaunchImageSize</key>
 <string>{320, 568}</string>
 </dict>
 <dict>
 <key>UILaunchImageMinimumOSVersion</key>
 <string>8.0</string>
 <key>UILaunchImageName</key>
 <string>Default-667h</string>
 <key>UILaunchImageOrientation</key>
 <string>Portrait</string>
 <key>UILaunchImageSize</key>
 <string>{375, 667}</string>
 </dict>
 <dict>
 <key>UILaunchImageMinimumOSVersion</key>
 <string>8.0</string>
 <key>UILaunchImageName</key>
 <string>Default-736h</string>
 <key>UILaunchImageOrientation</key>
 <string>Portrait</string>
 <key>UILaunchImageSize</key>
 <string>{414, 736}</string>
 </dict>
 </array>
 */
