
#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol DiallingCodeDelegate <NSObject>

@optional
- (void)didGetDiallingCode:(NSString *)diallingCode forCountry:(NSString *)countryCode;
- (void)didGetCountries:(NSArray *)countries forDiallingCode:(NSString *)diallingCode;

@required
- (void)failedToGetDiallingCode;

@end

@interface DiallingCode : NSObject <CLLocationManagerDelegate>

- (id)initWithDelegate:(id<DiallingCodeDelegate>)delegate;
- (void)getDiallingCodeForCurrentLocation;
- (void)getDiallingCodeForCountry:(NSString *)country;
- (void)getCountriesWithDiallingCode:(NSString *)diallingCode;

@property (nonatomic, assign) id<DiallingCodeDelegate> delegate;

@end
