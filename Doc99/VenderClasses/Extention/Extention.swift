//
//  Extention.swift
//  Doc99
//
//  Created by Web Migrates on 8/18/18.
//  Copyright © 2018 Pritesh Pethani. All rights reserved.
//

import UIKit
import Foundation


extension UIColor
{
    class func UIColorFromHex(hex:String, alpha:Double = 1.0) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(alpha)
        )
    }
}
extension Date {
    
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
        
        let currentCalendar = Calendar.current
        
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
        
        return end - start
    }
    
   /* func calculateTime(startDate:String,endDate:String) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let stdate : String = startDate
        let startDate = dateFormatter.date(from: stdate)!
        
        let currentDate = Date()
        
        let strTimer : String = startDate.offset(from: currentDate)
        if !strTimer.isEmpty {
            let stDay: String = "\((Int(strTimer)! % 31536000) / 86400)"
            let stHour: String = "\((Int(strTimer)! % 86400) / 3600)"
            let stMin: String = "\((Int(strTimer)! % 3600) / 60)"
            let stSec: String = "\(Int(strTimer)! % 60)"
            yourLabelOutlet.text = "Start In :\(stDay) Days \(stHour) Hours \(stMin) Minutes \(stSec) Seconds"
        }
        
    }*/
    
}
