//
//  localization+string.swift
//  My2 Property
//
//  Created by Developer on 13/03/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import Foundation
extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}
//
//func NSLocalizedString(_ key: String) -> String {
//    return NSLocalizedString(key, comment: "")
//}
//
//
//extension String {
//    static let Hello = NSLocalizedString("Hello!")
//    static let ImpressMe = NSLocalizedString("Impress Me!")
//    static let ThisApplicationIsCreated = NSLocalizedString("This application is created by the swifting.io team")
//    static let OpsNoFeature = NSLocalizedString("Oops! It looks like this feature haven't been implemented yet :(!")
//    static let OK = NSLocalizedString("OK")
//}

