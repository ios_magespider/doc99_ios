//
//  MVYMenuCustomCell.swift
//  SwiftTutorialWithXIB
//
//  Created by Pritesh Pethani on 24/06/16.
//  Copyright © 2016 Pritesh Pethani. All rights reserved.
//

import UIKit

class MVYMenuCustomCell: UITableViewCell {


   @IBOutlet var lblMenuName:UILabel!
    @IBOutlet var imageViewMenu:UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
