//
//  MVYMenuViewController.swift
//  SwiftTutorialWithXIB
//
//  Created by Pritesh Pethani on 24/06/16.
//  Copyright © 2016 Pritesh Pethani. All rights reserved.
//

import UIKit
import SDWebImage

class MVYMenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var tableView:UITableView!
    
    var appDelegate:AppDelegate?
    var selectedIndexpath:NSIndexPath?
    
    @IBOutlet var imageviewProfile:UIImageView!
    @IBOutlet var lblProfileCounter:UILabel!
    @IBOutlet var lblUsername:UILabel!
    @IBOutlet var lblEmailID:UILabel!

    @IBOutlet var activityIndicatorForProfileImage:UIActivityIndicatorView!


    var menuItems = [String]()
    var menuImages = [String]()

    // FIXME: - VIEW CONTROLLER METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.generalViewControllerSetting()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // TODO: - OTHER METHODS
    func generalViewControllerSetting(){
        
        self.setDiviceSupport()
        self.imageBorderSet();
        
        //        self.tableView.registerClass(MVYMenuCustomCell.self, forCellReuseIdentifier: "cell")
        self.tableView.register(UINib(nibName: "MVYMenuCustomCell", bundle: nil), forCellReuseIdentifier: "cell")
        //"Wallet" "wallet.png"
        
        if USERDEFAULT.value(forKey: "userType") as! String == "1"{
            menuItems = ["Dish","Orders","My Schedule","Scheduled Dishes","Reviews","Settings","Help","FAQ","Contact Us","Sign Out"]
            menuImages = ["dish.png","order.png","schedule.png","schedule.png","review.png","setting.png","help.png","faq.png","contact.png","logout.png"]
            
//            menuItems = ["Dish","Orders","Todays Schedule","Reviews","Settings","Help","FAQ","Contact Us","Sign Out"]
//            menuImages = ["dish.png","order.png","schedule.png","review.png","setting.png","help.png","faq.png","contact.png","logout.png"]
            
        }
        else if USERDEFAULT.value(forKey: "userType") as! String == "2"{
            menuItems = ["Home","Explore","Orders","Profile","Sign Out"]
            menuImages = ["dish.png","order.png","schedule.png","wallet.png","logout.png"]
        }

        
        // Define identifier
        let notificationName = Notification.Name("refreshingNotificationCount")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(notificationCounterRefresh(notification:)), name: notificationName, object: nil)
     
        
        self.setUserProfileData()
    }
    
    func setDiviceSupport(){
        
        if DeviceType.IS_IPHONE_4_OR_LESS {
            
            //            viewFooter.frame = CGRect(x: viewFooter.frame.origin.x, y: ScreenSize.SCREEN_HEIGHT - viewFooter.frame.size.height, width: viewFooter.frame.size.width ,height: viewFooter.frame.size.height)
            
            self.tableView.frame = CGRect(x: self.tableView.frame.origin.x, y: self.tableView.frame.origin.y, width: self.tableView.frame.size.width, height: self.tableView.frame.size.height - 88)
            
        }
    }
    
    func imageBorderSet(){
        imageviewProfile.layer.masksToBounds = true;
        imageviewProfile.layer.cornerRadius = imageviewProfile.frame.size.width / 2
        
        lblProfileCounter.layer.masksToBounds = true;
        lblProfileCounter.layer.cornerRadius = lblProfileCounter.frame.size.width / 2
        
    }

    func setUserProfileData(){
        
        if ((USERDEFAULT.value(forKey: "userName") as? String) != nil)
        {
            self.lblUsername.text = USERDEFAULT.value(forKey: "userName") as? String
        }
        
        if ((USERDEFAULT.value(forKey: "emailID") as? String) != nil)
        {
            self.lblEmailID.text = USERDEFAULT.value(forKey: "emailID") as? String
        }
        
        
        if ((USERDEFAULT.value(forKey: "userProfileImage") as? String) != nil) {
            let imageUrl = USERDEFAULT.value(forKey: "userProfileImage") as? String
            let fullUrl = NSString(format:"%@%@", ImageGetURL,imageUrl!) as String
            let url : NSString = fullUrl as NSString
            let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
            let searchURL : NSURL = NSURL(string: urlStr as String)!
            
           
            self.activityIndicatorForProfileImage.isHidden = false
            self.activityIndicatorForProfileImage.startAnimating()
            
            
            
            imageviewProfile.sd_setImage(with: searchURL as URL, completed: { (image:UIImage?, error:Error?, cacheType:SDImageCacheType!, imageURL:URL?) in
                
                if ((error) != nil) {
                    self.imageviewProfile.image = UIImage.init(named: "image_placeholder.png")
                }
                
                self.activityIndicatorForProfileImage.isHidden = true
                
            })

            
        }
        else{
            self.activityIndicatorForProfileImage.isHidden = true
            self.imageviewProfile.image = UIImage.init(named: "image_placeholder.png")

        }
        
    }
    
    func notificationCounterRefresh(notification: NSNotification) {
        
        
        
        if Reachability.isConnectedToNetwork() == true {
            let notificationCounter = notification.object as! String
            print("Notification Counter",notificationCounter)
            
            if notificationCounter == "0" || notificationCounter == ""{
                lblProfileCounter.isHidden = true
                lblProfileCounter.text = notificationCounter
            }
            else{
                lblProfileCounter.isHidden = false
                lblProfileCounter.text = notificationCounter
            }
            
    }
    
    }
    
    // TODO: - DELEGATE METHODS
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if menuItems.count == 0 {
            return 0;
        }
        return menuItems.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:MVYMenuCustomCell = self.tableView.dequeueReusableCell(withIdentifier: "cell") as! MVYMenuCustomCell
        
        //       let cell:MVYMenuCustomCell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MVYMenuCustomCell
        
        
        cell.lblMenuName.text = self.menuItems[indexPath.row] as String
        
        cell.imageViewMenu.image = UIImage.init(named: self.menuImages[indexPath.row])
        
       // cell.selectionStyle = .none
        
        let bgColorView = UIView.init()
        bgColorView.backgroundColor = UIColor(colorLiteralRed: 214.0/255.0, green: 239.0/255.0, blue: 215.0/255.0, alpha: 0.5)
        bgColorView.layer.masksToBounds = true
        cell.selectedBackgroundView = bgColorView
        self.tableView.backgroundColor = UIColor.clear
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if USERDEFAULT.value(forKey: "userType") as! String == "1"{
            
//            if indexPath.row == 0 {
//                let dishVC = DishViewController(nibName: "DishViewController", bundle: nil)
//                dishVC.isFirstTimeLogin = false
//                APPDELEGATE.navigationC = UINavigationController(rootViewController: dishVC)
//                APPDELEGATE.navigationC?.isNavigationBarHidden = true
//                self.sideMenuController().changeContentViewController(APPDELEGATE.navigationC, closeMenu: true)
//            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        tableView.backgroundColor = UIColor.white
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
    
    
    // TODO: - ACTION METHODS
    
    // TODO: - POST DATA METHODS
    

    
    

    
   
    
    
  
}
